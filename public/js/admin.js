$('#btn-imprimir').on('click', function(){
    data = $("#frmFiltrosPDF").serialize();
    var variables =  $("#rep_empresa").val()+
        ','+$("#rep_proyecto").val()+
        ','+$("#rep_gestion").val()+
        ','+$("#rep_selectMonto").val()+
        ','+$("#rep_monto").val()+
        ','+$("#rep_desde").val()+
        ','+$("#rep_hasta").val()+
        ','+$("#rep_columna").val()+
        ','+$("#rep_columna_buscar").val()+
        ','+$("#rep_tipo_solicitud").val()+',FIN';
    var values = $("input[name='exsol_id[]']")
          .map(function(){return $(this).val();}).get();
    if(values.length ==0){
        window.open('/imprimir/solicitudes/'+(-1)+'/'+variables);
    }else{
        window.open('/imprimir/solicitudes/'+values+'/'+variables);
    }
});