function verDocumento(id) {
    console.log(id);

    // jQuery.noConflict();
    var route = config.rutas[0].showCorrespon;
    route = route.replace(':id',id);
    var token = config.rutas[0].token;

    var storage = config.rutas[0].storage;

    $('#hrefDescargar').addClass("hidden");

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function (data){
            console.log(data);
            $('#lblCite').text(data.cite);
            $('#lblEmpresa').text(data.empresa.nombre);
            $('#lblFecha').text(data.fecha);
            $('#lblFechaLimite').text(data.fecha_limite);
            $('#lblOrigen').text(data.origen);
            $('#lblDestino').text(data.destino.nombres);

            var copias = '';
            if(data.tipo_id!=3){
                for(var i=0;i<data.rutas[0].empleados.length;i++){
                    // console.log(data.rutas[0].empleados[i]);
                    copias+=data.rutas[0].empleados[i].nombres+', ';
                }
            }

            /******************************************************DOCUMENTO/
             *
             */
            var documento = data.documento.nombre;
            var extension = documento.replace(/^.*\./, '');

            //ELIMINANDO DATOS DEL DOCUMENTO
            $('#liDocumento').children().remove();

            var routeDescarga = config.rutas[0].descargar;
            routeDescarga = routeDescarga.replace(':id', data.documento.id);

            console.log('Extension: '+extension);

            switch (extension){
                case 'pdf':
                    // console.log("PDF");
                    storage = storage.replace('archivo', data.documento.ubicacion);
                    console.log(storage);

                    var html = '<div class="row">'+
                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> '+
                        // '<div id="doc"></div>'+
                        '<iframe src="/js/ViewerJS/?zoom=page-width#../../storage/'+data.documento.ubicacion+'" width="100%" height="700" allowfullscreen webkitallowfullscreen></iframe>'+
                        '</div>'+
                        '</div>';

                    $('#liDocumento').append(html);
                    // PDFObject.embed(storage, "#doc", options);
                    // PDFObject.embed(storage, "#doc");
                    break;
                case "png":
                case "PNG":
                case "jpg":
                case "JPG":
                    // console.log("Imagen es");
                    storage = storage.replace('archivo', data.documento.ubicacion);

                    var html = '<div class="row">'+
                                    '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> '+
                                        '<img src="'+storage+'" style="width: 100%;">'+
                                    '</div>'+
                                '</div>';

                    $('#hrefDescargar').attr('href',routeDescarga);
                    $('#hrefDescargar').removeClass("hidden");

                    $('#liDocumento').append(html);

                    break;
                default:
                    console.log("OTROS");

                    var html = '<div class="row">'+
                        '<div class="media">'+
                        '<div class="media-left media-middle text-center">'+

                        '<img class="media-object" src="/images/archivo.png" width="180" height="180">'+

                        '</div>'+
                        '<div class="media-body">'+
                        '<h4 class="media-heading">'+data.documento.nombre+'</h4>'+
                        '<p><b>Referencia: </b>'+data.referencia+'</p>'+
                        '<p><b>Documento de: </b>'+data.documento.tipo.nombre+'</p>'+
                        '<p><b>Numero de hojas: </b>'+data.num_hojas+'</p>'+
                        '<p><b>Tipo de doc.: </b>'+data.tipo.nombre+'</p>'+
                        '<p><b>Copias: </b>'+copias+'</p>'+
                        '</div>'+
                        '</div>'+
                        '</div>';

                    $('#hrefDescargar').attr('href',routeDescarga);
                    $('#hrefDescargar').removeClass("hidden");

                    $('#liDocumento').append(html);
                    break;
            }
            $('#liRef').text(data.referencia);
            $('#liDocDe').text(data.documento.tipo.nombre);
            $('#liNumHojas').text(data.num_hojas);
            $('#liNombre').text(data.documento.nombre);
            $('#liTipoDoc').text(data.tipo.nombre);

            $('#lblCopias').text(copias);

            // $('#modalFooterCorrespon').children().remove();
            //
            // var footer = '';
            //
            // $('#modalFooterCorrespon').append();

            $('#modalCorrespondencia').modal('show');
        }
    });
}