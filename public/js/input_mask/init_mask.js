$(document).ready(function() {
	mascara();
});

function mascara () {
	$('.mask-cantidad').inputmask("decimal",{
		radixPoint: '.',
    	inputtype: "text",
        digits: "6,5",
        greedy: false,
        unmaskAsNumber: true,
        integerDigits: 6,
        groupSeparator: "",
        autoGroup: true,
        allowMinus: false,
        removeMaskOnSubmit: true,
    	onBeforePaste: function (pastedValue, opts) {
            return pastedValue;
        },    	
    });

    $('.mask-decimal').inputmask("decimal",{
    	radixPoint: '.',
    	inputtype: "text",
        digits: "8,5",
        greedy: false,
        unmaskAsNumber: true,
        integerDigits: 8,
        groupSeparator: "",
        autoGroup: true,
        removeMaskOnSubmit: true,
        onBeforePaste: function (pastedValue, opts) {
            return pastedValue;
        },        
    });

    $('.mask-fecha').inputmask("date",{
        placeholder: "dd/mm/aaaa",
    });

    
    $('.mask-factura').inputmask("numeric", {
        integerDigits: 15,
        digits: 0,
        allowMinus: false,
    });
}