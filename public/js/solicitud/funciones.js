/**
 * BEGIN: ISSUE 0001
 */
function verificaProyecto(usuario_id, usuario, proyecto_actual_id, nro_solicitud) {
    var proyecto_id = $("select.select_control option:selected").val();        
    var url_usuario = '/json/existe_proyecto/'+usuario_id+'/'+proyecto_id;        
    $.getJSON(url_usuario, function(cantidad) {
        if(parseInt(cantidad['valor']) === 0){
            var nombreProyecto = $("select.select_control option:selected").text();
            $("#nombre_proyecto").text(nombreProyecto);
            $("#nombre_usuario").text(usuario);
            $("#nro_solicitud").text("NRO." + nro_solicitud);
            $("#btn_añadirProyecto").attr('onclick', 'añadirProyectoUsuario('+proyecto_id+', '+usuario_id+')');                
            $('button.btn_modificar').attr('disabled', 'true');
            $("#advertenciaModal").modal('show');
            $("select.select_control").val(proyecto_actual_id).change();
        }else{                
            $('button.btn_modificar').removeAttr('disabled');
        }
    });
}

function añadirProyectoUsuario (proyecto_id, usuario_id) {
    $("#advertenciaModal").modal('hide');
    $.ajax({
        url: '/guardar-asignacion-proyecto-individual',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        data: {proyecto_id: proyecto_id,
            usuario_id: usuario_id
        }
    })
    .done(function(data) {
        $("select.select_control").val(data['proyecto_id']).change();
    })
    .fail(function(data) {
        console.log("error "+data);
        alert("No se pudo realizar la asignación");
    })
    .always(function(data) {
        console.log("complete "+ data['proyecto_id']);
    });
    
}
/**
 * END: ISSUE 0001
 */