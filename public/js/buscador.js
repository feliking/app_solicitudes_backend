/**
 * Created by djauregui on 8/11/2017.
 */
//***************************************BUSCADOR
$('#buscador').keyup(function () {
    $('#resultado').html('');
    var search = $('#buscador').val();
    var expression = new RegExp(search, 'i');

    var route = config.rutas[0].buscar;
    var token = config.rutas[0].token;

    route = route.replace(':texto', search);

    console.log(route);

    if(search.length > 2){
        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'GET',
            dataType: 'json',
            success: function (data){
                // console.log(data);
                // console.log(data.length);
                if(data.length > 0){

                    $.each(data, function (key, value) {
                        // console.log(value);
                        if(value.cite.search(expression)!=-1 || value.referencia.search(expression)!=-1 || value.origen.search(expression)!=-1 || value.empresa.search(expression)!=-1 || value.tag.search(expression)!=-1){
//                                $('#resultado').append('<li class="list-group-item">'+value.cite+' '+value.referencia+'</li>')
                            $('#resultado').append('<a onclick="javascript:textoAdd(this, '+value.id+');" style="cursor: pointer;" class="list-group-item">'+value.cite+' | '+value.referencia+' | '+value.origen+' | '+value.empresa+' | '+value.tag+'</a>');
                        }

                    });
                }else{
                    $('#resultado').html('');
                    $('#resultado').append('<a style="cursor: pointer;" class="list-group-item">No hay resultados</a>');
                }
            }
        });

    }else{
        actualizar_tabla = true;
        if(htmlTbodyTable!=null){
            $('#tbodyCorrespondencia').children().remove();
            $('#tbodyCorrespondencia').append(htmlTbodyTable);
        }
    }

});

var htmlTbodyTable = null;
function textoAdd(obj, id) {
    // console.log(obj);

    // console.log($(obj).text());
    $('#buscador').val($(obj).text());
    $('#resultado').html(''); //<---Resetea el buscador
    actualizar_tabla = false;

    var route = config.rutas[0].buscarId;
    var token = config.rutas[0].token;

    route = route.replace(':id',id);

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        success: function (data){
            // console.log(data);
            if(htmlTbodyTable==null)
                htmlTbodyTable = $('#tbodyCorrespondencia').children();

            $('#tbodyCorrespondencia').children().remove();

            //*************************************************TAGS DE VISTOS
            var auxVisto = 0;
            var auxVistoOtros = 0;
            $.each(data.rutas, function (key, value) {
                // console.log(value);
                if(value.visto_fecha==null && value.destino_id == config.rutas[0].uE){
                    auxVisto++;
                }else{
                    auxVistoOtros++;
                }
            });

            var tag_vistos = '<td></td>';
            if(auxVisto > 0){
                if(auxVistoOtros > 0){
                    tag_vistos = '<td><span class="label label-danger">'+auxVisto+'</span> <span class="label label-warning">'+auxVistoOtros+'</span></td>';
                }else {
                    tag_vistos = '<td><span class="label label-danger">'+auxVisto+'</span> <span class="label label-warning">0</span></td>';
                }
            }else{
                if(auxVistoOtros > 0){
                    tag_vistos = '<td><span class="label label-success">0</span> <span class="label label-warning">'+auxVistoOtros+'</span> </td>';
                }else {
                    tag_vistos = '<td><span class="label label-success">0</span> <span class="label label-warning">0</span></td>';
                }
            }
            //*************************************************

            //*************************************************TAGS DE ESTADOS
            var tag_estado = '<td></td>';
            switch (data.estado_id){
                case 1:
                    tag_estado = '<td><span class="label label-success">RECIBIDO</span></td>';
                    break;
                case 2:
                    tag_estado = '<td><span class="label label-warning">EN CURSO</span></td>';
                    break;
                case 3:
                    tag_estado = '<td><span class="label label-danger">CERRADO</span></td>';
                    break;

            }
            //*************************************************

            var fecha_limite = null;
            if(data.fecha_limite!=null){
                fecha_limite = '<td>'+data.fecha_limite+'</td>';
            }else{
                fecha_limite = '<td></td>';
            }

            var html = '<tr>' +
                '<td>'+data.cite+'</td>'+
                '<td>'+data.referencia+'</td>'+
                '<td>'+data.fecha+'</td>'+
                fecha_limite+
                '<td>'+data.num_hojas+'</td>'+
                '<td>'+data.empresa.nombre+'</td>'+
                '<td>'+data.origen+'</td>'+
                '<td>'+data.destino.nombres+'</td>'+
                tag_vistos+
                tag_estado+
                '<td>' +
                '<button onclick="javascript:verCorrespondencia('+data.id+')" class="btn btn-sm btn-default" title="Ver correspondencia '+data.cite+'"><i class="fa fa-search"></i></button>' +
                '<button onclick="javascript:verRuta('+data.id+','+data.estado_id+')" class="btn btn-sm btn-info-custom" title="Ver rutas '+data.cite+'"><i class="fa fa-random"></i></button>' +
                '</td>'+
                '</tr>';

            $('#tbodyCorrespondencia').append(html);
        }
    });
}