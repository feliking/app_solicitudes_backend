/**
 * Created by djauregui on 12/10/2017.
 */
var usuarios = [];

$( document ).ready(function() {
    console.log( "ready!" );
    getEmpleados();

    var users = config.rutas[0].usersGet;
    var token = config.rutas[0].token;

    $.ajax({
        url: users,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        success: function (data){
            var a = 0;
            $.each(data, function(key, value){
                // console.log(key+"=>"+value+"=>"+value['empleado_id']);
                usuarios[a] = [];
                usuarios[a]['empleado_id'] = value['empleado_id'];
                usuarios[a]['email'] = value['email'];
                a++;
            });
        }
    });

    $('#formRuta').submit(function(e, submit) {
        var error = "";

        $('select[name="selectDestinos[]"]').each(function () {
            var emp = this.value;
            console.log("Empleado");
            console.log(emp);

            var selectDestinoSeparado = emp.split('-');

            //*************************************************************VERIFICACION DE USUARIO
            var boolUsuario = false;
            var boolEmail = false;
            $.each(usuarios, function(key, value){

                if(selectDestinoSeparado[0]==value['empleado_id']){
                    boolUsuario = true;
                    if(value['email']!=null){
                        boolEmail = true;
                    }
                    return false;
                }else{
                    if(selectDestinoSeparado[0]<value['empleado_id']){
                        boolUsuario = false;
                        return false;
                    }else {
                        boolUsuario = false;
                    }
                }
            });

            if(boolUsuario){
                if(boolEmail){
                    // console.log("usuario y email");
                }else{
                    // console.log("usuario sin email");
                    error += "- "+selectDestinoSeparado[1]+" <i style='color: #8c8c8c;'>(tiene usuario pero no cuenta con email)</i><br>";
                }
            }else{
                console.log("no usuario");
                if(selectDestinoSeparado[2]==""){
                    error += "- "+selectDestinoSeparado[1]+" <i style='color: #8c8c8c;'>(no tiene usuario ni cuenta con email para ser registrado)</i><br>";
                }
            }
            //*************************************************************

        });


        //*************************************************************VERIFICACION DE COPIAS
        $('select#copias_id').find(':selected').map(function () {
            console.log(this.value);
            var copias = this.value.split('-');
            // console.log(copias);
            //*************************************************************VERIFICACION DE USUARIO
            var boolUsuario = false;
            var boolEmail = false;
            $.each(usuarios, function(key, value){

                if(copias[0]==value['empleado_id']){
                    boolUsuario = true;
                    if(value['email']!=null){
                        boolEmail = true;
                    }
                    return false;
                }else{
                    if(copias[0]<value['empleado_id']){
                        boolUsuario = false;
                        return false;
                    }else {
                        boolUsuario = false;
                    }
                }
            });

            if(boolUsuario){
                if(boolEmail){
                    // console.log("usuario y email");
                }else{
                    // console.log("usuario sin email");
                    error += "- "+copias[1]+" <i style='color: #8c8c8c;'>(tiene usuario pero no cuenta con email)</i><br>";
                }
            }else{
                console.log("no usuario");
                if(copias[2]==""){
                    error += "- "+copias[1]+" <i style='color: #8c8c8c;'>(no tiene usuario ni cuenta con email para ser registrado)</i><br>";
                }
            }
            //!*************************************************************
        });

        // console.log(error);
        if(error!=""){
            $('#lblConfirmación').html("Las siguientes personas no cuentan con un correo registrado por lo que el sistema le enviara un correo con los usuarios y " +
                "sus contraseñas para que <b>usted</b> se encargue de contactar con estas: <br><br>"+error);
            $('#modalConfirmacion').modal('show');
            if(!submit)
                e.preventDefault(e);
        }
    });

    $('#btnConfirmacionModal').click(function () {
        // console.log("Acepto...");
        $('#formRuta').trigger('submit', [true]);
    });
});

var empleados = null;

function getEmpleados(){
    $.ajax({
        url: 'http://rrhh.pragmainvest.com.bo/servicios/get.empleados',
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(e){
            empleados = null;

            $("select#copias_id").select2({
                allowClear: true,
                placeholder: "Cargando con copias a ...",
                width: '100%'
            }).val('').trigger('change');

            $('select#copias_id').prop('disabled', true);
        }
    }).done(function (response){

        $('#cargandoAlert').slideToggle();
        $('#timelineiconadd').attr('onclick','agregarRuta()');
        // console.log(response);
        empleados = response;

        var selectCopias = $('select#copias_id');
        selectCopias.empty();
        $.each(response['empleados'], function(key, value){
            selectCopias.append("<option value='"+value['id']+"-"+value['nombres']+" "+value['ap']+" "+value['am']+"-"+value['email_corporativo']+"'>"+value['nombres']+" "+value['ap']+" "+value['am']+"</option>");
        });
        $('select#copias_id').prop('disabled', false);

        $('select#copias_id').change();
        $("select#copias_id").select2({
            allowClear: true,
            placeholder: "Seleccione con copia a ...",
            width: '100%'
        }).val('').trigger('change');
    }).fail(function (response){
        console.log(response);
    });
}

var auxSelect = 0;
function agregarRuta() {
    console.log(empleados);

    if(empleados!=null){
        var selectOptions = '';

        for(var i=0; i<empleados.empleados.length;i++){
            selectOptions += '<option value="'+empleados.empleados[i].id+'-'+empleados.empleados[i].nombres+' '+empleados.empleados[i].ap+' '+empleados.empleados[i].am+'-'+empleados.empleados[i].email_corporativo+'">'+empleados.empleados[i].nombres+' '+empleados.empleados[i].ap+' '+empleados.empleados[i].am+'</option>';
        }

        var html =
            '<div class="timeline-centered-remove">'+
                '<article class="timeline-entry">'+
                    '<div class="timeline-entry-inner timeline-entry-inner-remove">'+
                        // '<div class="timeline-icon timeline-icon-remove bg-warning" onclick="javascript:eliminarRuta(this)">'+
                        '<div class="timeline-icon timeline-icon-remove bg-warning" onclick="javascript:eliminarRuta(this);">'+
                            '<i class="entypo-feather">-</i>'+
                        '</div>'+
                        '<div class="timeline-label">'+
                            '<p><b>Destino:</b></p><select id="selectDestinos'+auxSelect+'" name="selectDestinos[]" required>'+selectOptions+'</select>'+

                        '</div>'+
                    '</div>'+
                '</article>'+
            '</div>';


        $('#timelineentryadd').before(html);
        // $('#timelineentryadd').remove();

        $('#btnGuardarRuta').removeAttr('disabled');

        $("#selectDestinos"+auxSelect).select2({
            allowClear: true,
            placeholder: "Seleccione destino ...",
            width: '100%'
        }).val('').trigger('change');

        $("#selectCopias"+auxSelect).select2({
            allowClear: true,
            placeholder: "Seleccione copias ...",
            width: '100%'
        }).val('').trigger('change');

        auxSelect++;

    }else{
        alert("Cargando empleados...");
    }
}

function eliminarRuta(obj) {
    $(obj).parent();
    console.log($(obj).parent().parent());
    $(obj).parent().parent().remove();
}