$(document).ready(function () {
    $('#btn-logout').click(function(){
        console.log('salir');
        $('#logout-form').submit();
    });
});

var globalAlertas = 0;

function formatNumber(num) {
    if (!num || num == 'NaN') return '-';
    if (num == 'Infinity') return '&#x221e;';
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num + '.' + cents);
}

function harlem() {
    //javascript:
    (function () {
        function addStyleSheet() {
            var e = document.createElement("link");
            e.setAttribute("type", "text/css");
            e.setAttribute("rel", "stylesheet");
            e.setAttribute("href", styleSrc);
            e.setAttribute("class", markerClass);
            document.body.appendChild(e)
        }

        function removeAddedElements() {
            var e = document.getElementsByClassName(markerClass);
            for(var t = 0; t < e.length; t++){
                document.body.removeChild(e[t])
            }
        }

        function flashEffect(){
            var e = document.createElement("div");
            e.setAttribute("class", flashClass);
            document.body.appendChild(e);
            setTimeout(function(){
                document.body.removeChild(e)
            }, 100)
        }

        function getSize(e){
            return {
                height: e.offsetHeight,
                width: e.offsetWidth
            }
        }

        function isRightSize(i){
            var s = getSize(i);
            return s.height > minHeight && s.height < maxHeight
                &&
                s.width > minWidth && s.width < maxWidth
        }

        function heightFromTop(e) {
            var n = 0;
            while ( !! e) {
                n += e.offsetTop;
                e = e.offsetParent
            }
            return n
        }

        function getWindowHeight() {
            var e = document.documentElement;
            if ( !! window.innerWidth) {
                return window.innerHeight
            }
            else if (e && !isNaN(e.clientHeight)) {
                return e.clientHeight
            }
            return 0
        }

        function getPageYOffset() {
            if (window.pageYOffset) {
                return window.pageYOffset
            }
            return Math.max(document.documentElement.scrollTop, document.body.scrollTop)
        }

        function isOnScreen(e) {
            var t = heightFromTop(e);
            return t >= yOffset && t <= windowHeight + yOffset
        }

        function main() {
            var e = document.createElement("audio");
            e.setAttribute("class", markerClass);
            e.src = mp3Src;
            e.loop = false;
            e.innerHTML = " <p>If you are reading this, it is because your browser does not support the audio element. We recommend that you get a new browser.</p>";
            document.body.appendChild(e);
            e.addEventListener("ended", function () {
                removeAllShakeMeClasses();
                removeAddedElements()
            }, true);
            e.addEventListener("canplay", function () {
                setTimeout(function () {
                    addImFirstClass(startElement)
                }, 500);
                setTimeout(function () {
                    removeAllShakeMeClasses();
                    flashEffect();
                    for (var e = 0; e < O.length; e++) {
                        addShakeMeClass(O[e])
                    }
                }, 15500)
            }, true);
            e.play()
        }

        function addImFirstClass(e) {
            e.className += " " + shakeMeClass + " " + imFirstClass
        }

        function addShakeMeClass(e) {
            e.className += " " + shakeMeClass + " " + varClasses[Math.floor(Math.random() * varClasses.length)]
        }

        function removeAllShakeMeClasses() {
            var e = document.getElementsByClassName(shakeMeClass);
            var t = new RegExp("\\b" + shakeMeClass + "\\b");
            for (var n = 0; n < e.length;) {
                e[n].className = e[n].className.replace(t, "")
            }
        }

        var minHeight = 30;
        var minWidth = 30;
        var maxHeight = 350;
        var maxWidth = 350;
        var mp3Src = "//s3.amazonaws.com/moovweb-marketing/playground/harlem-shake.mp3";
        var shakeMeClass = "mw-harlem_shake_me";
        var imFirstClass = "im_first";
        var varClasses = ["im_drunk", "im_baked", "im_trippin", "im_blown"];
        var flashClass = "mw-strobe_light";
        var styleSrc = "//s3.amazonaws.com/moovweb-marketing/playground/harlem-shake-style.css";
        var markerClass = "mw_added_css";
        var windowHeight = getWindowHeight();
        var yOffset = getPageYOffset();
        var pageElements = document.getElementsByTagName("*");
        var startElement = null;

        // ----Starts here----
        for (var L = 0; L < pageElements.length; L++) {
            var A = pageElements[L];
            if (isRightSize(A) && isOnScreen(A)) {
                startElement = A;
                break
            }
        }
        if (startElement === null) {
            console.warn("Could not find a node of the right size. Please try a different page.");
            return
        } else{
            console.log("Found start element: ",A," with width "+getSize(A).width+", height "+getSize(A).height+", and a total Y offset of "+heightFromTop(A));
        }
        addStyleSheet();
        main();
        var O = [];
        for (var L = 0; L < pageElements.length; L++) {
            var A = pageElements[L];
            if (isRightSize(A)) {
                O.push(A)
            }
        }
    })()
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
        });
    return vars;
}

$('.proy_cols').hide();
function cambio_proy(){
    $('.proy_cols').toggle();
    $('.proy_simple').toggle();
}

/*  OLD VERSION
$(function() {
    console.log('funcion 1');
    $('#generarClave').click(function() {
        var alpha = "abcdefghijklmnopqrstuvwxyz";
        var alpha_upper = alpha.toUpperCase();
        var numeric = "0123456789";
        var special = "-+=_!@$#*%<>[]{}";
        var length = 14;

        var chars = "";
        var pw = "";

        chars += alpha;
        chars += numeric;
        chars += special;
        chars += alpha_upper;

        var len = chars.length;

        for (var i=0; i < length; i++){
            var numR = Math.random() * (len - 1);
            pw += chars.substring(numR, numR+1);
        }
        $('input[name="password"]').val(pw);
    });


    $('#btn-creafila').click(function() {
        creaFila();
    });

    $('#btn-eliminafila').click(function() {
        console.log('eli');
        eliminaFila();
    });

    $("input[name='unidad[]']").change(function(){
        console.log('cambio');
    });

     $('[id^=unidad]').change(function(){
        console.log('cambio123');
    });

    $('input[name^="unidad"]').change(function() {
        console.log("valor"+$(this).val());
    });

    $('input[name=unidad\\[\\]]').change(function() {
        console.log("valor123"+$(this).val());
    });

    function eliminaFila() {
        console.log('eliminar');
        //document.getElementById("solicitud-items").deleteRow(0);

//        var i=this.parentNode.parentNode.rowIndex;
//        document.getElementById('solicitud-items').deleteRow(i);
    }

    $( "#upload-imagen" ).click(function() {
        $('#imagen').click();
    });

    $( "#upload-doc" ).click(function() {
        $('#doc').click();
    });

});

(function($) {
    function opciones_menu_visibles(){
        $("#sidebar-opciones").find('ul').css('display', 'block');
        console.log('sidebar');
    }

    $(document).ready(function(){
        opciones_menu_visibles();
    });
    $(window).resize(function(){
        opciones_menu_visibles();
    });

    //Reloj para formulario Solicitud
    function getClock(){
        //Get Current Time
        d = new Date();
        str = prefixZero(d.getHours(), d.getMinutes(), d.getSeconds());
    }

    function prefixZero(hour, min, sec){
         var curTime;
         if(hour < 10)
            curTime = "0"+hour.toString();
         else
            curTime = hour.toString();

         if(min < 10)
            curTime += ":0"+min.toString();
         else
            curTime += ":"+min.toString();

         if(sec < 10)
            curTime += ":0"+sec.toString();
         else
            curTime += ":"+sec.toString();
        $('#reloj').val(curTime);
        return curTime;
    }
    setInterval(getClock, 1000);
})(jQuery);

$(document).ready(function () {
    $('#btn-logout').click(function(){
        console.log('salir');
        $('#logout-form').submit();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.addBtn').on('click', function () {
        addTableRow();
    });
    $('.addBtnRemove').click(function () {
        $(this).closest('tr').remove();
    })
    var i = 1;
    function addTableRow(){
        var tempTr = $('<tr><td><input type="text" id="userName_' + i + '" class="form-control"/></td><td><input type="text" id="email_' + i + '" class="form-control" /></td><td><input type="text" id="password_' + i + '" class="form-control" /></td><td><span class="glyphicon glyphicon-minus addBtnRemove" id="addBtn_' + i + '"></span></td></tr>').on('click', function () {
            $(this).closest('tr').remove();
            $(document.body).on('click', '.TreatmentHistoryRemove', function (e) {
                $(this).closest('tr').remove();
            });
        });
        $("#tableAddRow").append(tempTr)
        i++;
    }

    //Proyecto / Sub-proyecto
    $("input:radio[name=empresa]").click(function(){
        var value = $(this).attr("value");
        $('.logo-empresa').each(function(){
            $(this).css('display', 'none');
        });
        var logo = '#logo_'+value;
        $(logo).css('display', 'block');

        $('.div-sel-gestion').each(function(){
            $(this).css('display', 'none');
        });
        var logo_img = '#ges_empresa_'+value;
        $(logo_img).css('display', 'block');
    });

    $('.select_proyecto_padre').css('display', 'none');
    $("input:radio[name=opcion_sub]").click(function(){
        var valor = $(this).attr("value");
        if(valor == 1){
            $('.select_proyecto_padre').css('display', 'block');
        }else{
            $('.select_proyecto_padre').css('display', 'none');
        }
    });

    //Click para crear Item en solicitud
    $( "#btn-creafila1" ).click();

    //crea proyecto btn
    $('#crea-proyecto').click(function(){
        $('#form-crea-proyecto').click();
    });

    //Guardar e imprimir solicitud
    $('#btn-imprmir').click(function(){
        window.print();
        console.log('Guardar e Imprimir');
    });
});
 */

/**
 * BEGIN: ISSUE 0016
 */


/**
 * END: ISSUE 0016
 */