/**
 * Created by djauregui on 16/10/2017.
 */
$( document ).ready(function() {
    $(".js-placeholder-single").select2({
        allowClear: true,
        placeholder: "Seleccione ...",
        width: '100%'
    }).val('').trigger('change');
});

function editarUsuario(id){
    var route = config.rutas[0].editarUsuario;
    var token = config.rutas[0].token;

    route = route.replace(':id',id);

    console.log(route);
    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function (data){
            console.log(data);
            $('select#empleado_id').select2({
                allowClear: true,
                placeholder: "Seleccione ...",
                width: '100%'
            }).val('1541-Justina  Paredes').trigger('change');

            // jQuery.noConflict();
            $('#modalActualizarUsuario').modal('show');
        }
    });
}