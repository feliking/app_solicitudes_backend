//Numero a Literal
function basico(numero){
    var valor = ['UNO', 'DOS', 'TRES', 'CUATRO', 'CINCO', 'SEIS', 'SIETE', 'OCHO', 'NUEVE', 'DIEZ', 
        'ONCE', 'DOCE', 'TRECE', 'CATORCE', 'QUINCE', 'DIECISEIS', 'DIECISIETE', 'DIECIOCHO', 'DIECINUEVE', 'VEINTE',
        'VEINTIUNO', 'VEINTIDOS', 'VEINTITRES', 'VEINTICUATRO', 'VEINTICINCO', 'VEINTISÉIS', 'VEINTISIETE', 'VEINTIOCHO', 'VEINTINUEVE'];
    return valor[numero-1];
}

function decenas(n){
    var decenas  = [];

    decenas[30] = 'TREINTA';
    decenas[40] = 'CUARENTA';
    decenas[50] = 'CINCUENTA';
    decenas[60] = 'SESENTA';
    decenas[70] = 'SETENTA';
    decenas[80] = 'OCHENTA';
    decenas[90] = 'NOVENTA';
    if (n <= 29)
        return basico(n);
    var x = n % 10;
    if (x == 0) {
        return decenas[n];
    } else
        return decenas[n - x] + ' Y ' + basico(x);
}

var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

function centenas(n){
    var cientos  = [];
    cientos[100] = 'CIEN';
    cientos[200] = 'DOSCIENTOS';
    cientos[300] = 'TRECIENTOS';
    cientos[400] = 'CUATROCIENTOS';
    cientos[500] = 'QUINIENTOS';
    cientos[600] = 'SEISCIENTOS';
    cientos[700] = 'SETECIENTOS';
    cientos[800] = 'OCHOCIENTOS';
    cientos[900] = 'NOVECIENTOS';
    if (n >= 100) {
        if (n % 100 == 0) {
            return cientos[n];
        } else {
            u = parseInt(String(n).substring(0, 1));
            d = parseInt(String(n).substring(1));
            return ((u == 1) ? 'CIENTO' : cientos[u * 100]) + ' ' + decenas(d);
        }
    } else
        return decenas(n);
}

function miles(n){
    if (n > 999) {
        if (n == 1000) {
            return 'MIL';
        } else {
            l = String(n).length;
            c = parseInt(String(n).substring(0, l - 3));
            x = parseInt(String(n).substring(l - 3));
            if (c == 1) {
                cadena = 'MIL ' + centenas(x);
            } else if (x != 0) {
                cadena = centenas(c) + ' MIL ' + centenas(x);
            } else
                cadena = centenas(c) + ' MIL';
            return cadena;
        }
    } else
        return centenas(n);
}

function millones(n){
    if (n == 1000000) {
        return 'UN MILLÓN';
    } else {
        l = String(n).length;
        c = parseInt(String(n).substring(0, l - 6));
        x = parseInt(String(n).substring(l - 6));
        if (c == 1) {
            cadena = ' MILLÓN ';
        } else {
            cadena = ' MILLONES ';
        }
        return miles(c) + cadena + ((x > 0) ? miles(x) : '');
    }
}

function numeroLiteral(numero){
    var n = "";
    console.log("numero: "+numero);
    if(numero.indexOf(',') > -1){
        var numero_array = numero.split(',');
        for(i = 0; i < numero_array.length; i++){
           n += numero_array[i];
        }
    }else{
        n = numero;
    }
    n = parseInt(n);

    switch (true) {
        case ( n >= 1 && n <= 29 ) :
            return basico(n);
            break;
        case ( n >= 30 && n < 100 ) :
            return decenas(n);
            break;
        case ( n >= 100 && n < 1000 ) :
            return centenas(n);
            break;
        case ( n >= 1000 && n <= 999999 ):
            return miles(n);
            break;
        case ( n >= 1000000 ):
            return millones(n);
    }
}