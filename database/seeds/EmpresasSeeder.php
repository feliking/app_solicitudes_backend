<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class EmpresasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresas')->insert([
            'id' => 1,
            'nombre' => 'PRAGMA INVEST S.A.',
            'razon_social' => 'PRAGMA INVEST SOCIEDAD DE INVERSIONES',
            'nit' => '130275028',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/pragma_invest/pragma_invest.png',
            'slug' => 'pragma_invest',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 2,
            'nombre' => 'COLINA SRL',
            'razon_social' => 'COMPAÑIA DE LIMPIEZA E INGENIERIA AMBIENTAL',
            'nit' => '1029493021',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/colina/colina.jpg',
            'slug' => 'colina',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 3,
            'nombre' => 'TREBOL S.A.',
            'razon_social' => 'TRATAMIENTO DE RESIDUOS BOLIVIA',
            'nit' => '131173021',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/trebol/trebol.jpg',
            'slug' => 'trebol',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 4,
            'nombre' => 'LA PAZ LIMPIA',
            'razon_social' => 'LA PAZ LIMPIA',
            'nit' => '123',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/la_paz_limpia/la_paz_limpia.png',
            'slug' => 'la_paz_limpia',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 5,
            'nombre' => 'TEPCO SRL',
            'razon_social' => 'TECNOLOGIA PARA COMPRAR',
            'nit' => '198040023',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/tepco_srl/tepco_srl.jpg',
            'slug' => 'tepco_srl',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 6,
            'nombre' => 'HINO S.A',
            'razon_social' => 'HORMIGON INDUSTRIAL DEL NORTE',
            'nit' => '168482020',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/hino_sa/hino_sa.jpg',
            'slug' => 'hino_sa',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 7,
            'nombre' => 'TUNQUI LTDA',
            'razon_social' => 'EMPRESA CONSTRUCTURA TUNQUI LTDA',
            'nit' => '176346021',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/tunqui_ltda/tunqui_ltda.jpg',
            'slug' => 'tunqui_ltda',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 8,
            'nombre' => 'CASA',
            'razon_social' => 'CONSTRUCCION ALTEÑA SA',
            'nit' => '157522023',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/casa/casa.jpg',
            'slug' => 'casa',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 9,
            'nombre' => 'BOLIVIAN PROJECTS S.A.',
            'razon_social' => 'BOLIVIAN PROJECTS S.A.',
            'nit' => '131405023',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/bolivian_projects_sa/bolivian_projects_sa.jpg',
            'slug' => 'bolivian_projects_sa',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 10,
            'nombre' => 'MANOS ANDINAS SRL',
            'razon_social' => 'CONSTRUCTORA MANOS ANDINAS DEL NORTE S.R.L.',
            'nit' => '177136023',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/manos_andinas_srl/manos_andinas_srl.jpg',
            'slug' => 'manos_andinas_srl',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 11,
            'nombre' => 'PRARCO S.R.L.',
            'razon_social' => 'PRARCO S.R.L.',
            'nit' => '182584023',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/prarco_srl/prarco_srl.jpg',
            'slug' => 'prarco_srl',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 12,
            'nombre' => 'HUAYNA POTOSI',
            'razon_social' => 'HUAYNA POTOSI',
            'nit' => '190196026',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/huayna_potosi/huayna_potosi.jpg',
            'slug' => 'huayna_potosi',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 13,
            'nombre' => 'INFINIT S.A.',
            'razon_social' => 'INFINIT S.A.',
            'nit' => '129741026',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/infinit_sa/infinit_sa.jpg',
            'slug' => 'infinit_sa',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 14,
            'nombre' => 'CREA S.A',
            'razon_social' => 'COMUNIDAD DE RECICLAJE EL ALTO S.A.',
            'nit' => '168498029',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/crea_sa/crea_sa.jpg',
            'slug' => 'crea_sa',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 15,
            'nombre' => 'URBANIZA',
            'razon_social' => 'URBANIZA BIENES RAICES Y CONTRUCCIONES S.A.',
            'nit' => '131637029',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/urbaniza/urbaniza.png',
            'slug' => 'urbaniza',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 16,
            'nombre' => 'RIGSA',
            'razon_social' => 'RIGSA',
            'nit' => '111',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/rigsa/rigsa.jpg',
            'slug' => 'rigsa',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('empresas')->insert([
            'id' => 17,
            'nombre' => 'JACHASOL',
            'razon_social' => 'JACHASOL',
            'nit' => '222',
            'descripcion' => '',
            'telefono' => '',
            'direccion' => '',
            'email' => '',
            'logo' => 'empresas/jachasol/jachasol.jpg',
            'slug' => 'jachasol',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
