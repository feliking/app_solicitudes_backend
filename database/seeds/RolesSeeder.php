<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'nombre' => 'ADMINISTRADOR'
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'nombre' => 'USUARIO'
        ]);
        DB::table('roles')->insert([
            'id' => 3,
            'nombre' => 'AUTORIZADOR'
        ]);
        DB::table('roles')->insert([
            'id' => 4,
            'nombre' => 'REVISOR'
        ]);
        DB::table('roles')->insert([
            'id' => 5,
            'nombre' => 'APROBADOR'
        ]);
        DB::table('roles')->insert([
            'id' => 6,
            'nombre' => 'TESORERIA'
        ]);
        DB::table('roles')->insert([
            'id' => 7,
            'nombre' => 'APROBADOR RENDICIONES'
        ]);
        DB::table('roles')->insert([
            'id' => 8,
            'nombre' => 'ADMIN'
        ]);
    }
}
