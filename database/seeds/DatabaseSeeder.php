<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(NivelesAprobacionSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(EstadosSeeder::class);
        $this->call(EmpresasSeeder::class);
        $this->call(GestionesSeeder::class);
        $this->call(MonedasSeeder::class);
        $this->call(UnidadesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(BancosSeeder::class);
        $this->call(ModalidadesSeeder::class);
        $this->call(TipoCambioSeeder::class);
        $this->call(PrioridadesSeeder::class);
        $this->call(CuentasSeeder::class);
        $this->call(ProyectosSeeder::class);
        $this->call(ChequesEstadosSeeder::class);
        $this->call(ItemsTiposSeeder::class);
        $this->call(NivelAprobadorSeeder::class);
//        $this->call(Seeder::class);
    }
}
