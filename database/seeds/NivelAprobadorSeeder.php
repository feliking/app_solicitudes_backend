<?php

use Illuminate\Database\Seeder;

class NivelAprobadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 7,
            'nivel_id' => 1,
            'porcentaje' => 100
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 7,
            'nivel_id' => 2,
            'porcentaje' => 100
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 7,
            'nivel_id' => 3,
            'porcentaje' => 100
        ]);
        
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 8,
            'nivel_id' => 1,
            'porcentaje' => 35
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 8,
            'nivel_id' => 2,
            'porcentaje' => 30
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 8,
            'nivel_id' => 3,
            'porcentaje' => 25
        ]);

        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 9,
            'nivel_id' => 1,
            'porcentaje' => 45
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 9,
            'nivel_id' => 2,
            'porcentaje' => 40
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 9,
            'nivel_id' => 3,
            'porcentaje' => 30
        ]);

        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 10,
            'nivel_id' => 1,
            'porcentaje' => 50
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 10,
            'nivel_id' => 2,
            'porcentaje' => 45
        ]);
        DB::table('nivel_aprobador')->insert([
            'aprobador_id' => 10,
            'nivel_id' => 3,
            'porcentaje' => 35
        ]);
    }
}
