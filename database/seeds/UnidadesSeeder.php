<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class UnidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unidades')->insert([
            'codigo' => 'BARRA',
            'descripcion' => 'BARRA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'CJ',
            'descripcion' => 'CAJA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'CM',
            'descripcion' => 'CENTIMETROS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'DÍA',
            'descripcion' => 'DÍAS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'GAL',
            'descripcion' => 'GALONES',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'GLB',
            'descripcion' => 'GAS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'HR',
            'descripcion' => 'HORAS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'JG',
            'descripcion' => 'JUEGOS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'KG',
            'descripcion' => 'KILOGRAMOS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'LT',
            'descripcion' => 'LITROS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'M',
            'descripcion' => 'METROS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'M2',
            'descripcion' => 'METROS CUADRADOS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'M3',
            'descripcion' => 'METROS CUBICOS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'PZA',
            'descripcion' => 'PIEZAS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'TN',
            'descripcion' => 'TONELADAS',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('unidades')->insert([
            'codigo' => 'UN',
            'descripcion' => 'UNIDAD',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
