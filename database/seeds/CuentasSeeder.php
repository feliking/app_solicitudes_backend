<?php

use Illuminate\Database\Seeder;

class CuentasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cuenta_bancarias')->insert([
            'numero' => '700',
            'monto_inicial' => 7000000,
            'saldo' => 0.0,
            'moneda_id' => 1,
            'empresa_id' => 1,
            'banco_id' => 1
        ]);
        DB::table('cuenta_bancarias')->insert([
            'numero' => '701',
            'monto_inicial' => 7001000,
            'saldo' => 0.0,
            'moneda_id' => 1,
            'empresa_id' => 1,
            'banco_id' => 2
        ]);
        DB::table('cuenta_bancarias')->insert([
            'numero' => '710',
            'monto_inicial' => 71000,
            'saldo' => 0.0,
            'moneda_id' => 2,
            'empresa_id' => 1,
            'banco_id' => 1
        ]);   
    }
}
