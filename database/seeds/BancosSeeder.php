<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class BancosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bancos')->insert([
            'codigo' => 'BU',
            'nombre' => 'BANCO UNION',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('bancos')->insert([
            'codigo' => 'BISA',
            'nombre' => 'BANCO BISA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('bancos')->insert([
            'codigo' => 'MSC',
            'nombre' => 'BANCO MERCANTIL SANTA CRUZ',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('bancos')->insert([
            'codigo' => 'BNB',
            'nombre' => 'BANCO NACIONAL DE BOLIVIA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
