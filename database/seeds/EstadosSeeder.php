<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SOLICITUD CREADA (1) [1]
        DB::table('estados')->insert([
            'id' => 10,
            'codigo' => 'SOL',
            'orden' => 10,
            'descripcion' => 'SOLICITUD CREADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 11,
            'codigo' => 'SOL-M',
            'orden' => 11,
            'descripcion' => 'SOLICITUD MODIFICADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 12,
            'codigo' => 'SOL-A',
            'orden' => 12,
            'descripcion' => 'SOLICITUD ANULADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //AUTORIZADOR (3) [2, 3, 4]
        DB::table('estados')->insert([
            'id' => 20,
            'codigo' => 'AUT',
            'orden' => 20,
            'descripcion' => 'SOLICITUD AUTORIZADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 21,
            'codigo' => 'AUT-R',
            'orden' => 21,
            'descripcion' => 'SOLICITUD RECHAZADA POR AUTORIZADOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 22,
            'codigo' => 'AUT-O',
            'orden' => 22,
            'descripcion' => 'SOLICITUD OBSERVADA POR AUTORIZADOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 23,
            'codigo' => 'AUT-M',
            'orden' => 23,
            'descripcion' => 'SOLICITUD MODIFICADA POR AUTORIZADOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //CONTROLLER (3) [5, 6, 7]
        DB::table('estados')->insert([
            'id' => 30,
            'codigo' => 'REV',
            'orden' => 30,
            'descripcion' => 'SOLICITUD AUTORIZADA REVISOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 31,
            'codigo' => 'REV-R',
            'orden' => 31,
            'descripcion' => 'SOLICITUD RECHAZADA POR REVISOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 32,
            'codigo' => 'REV-O',
            'orden' => 32,
            'descripcion' => 'SOLICITUD OBSERVADA POR REVISOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 33,
            'codigo' => 'REV-M',
            'orden' => 33,
            'descripcion' => 'SOLICITUD MODIFICADA POR REVISOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //APROBADOR (3) [8, 9, 10]
        DB::table('estados')->insert([
            'id' => 40,
            'codigo' => 'APR',
            'orden' => 40,
            'descripcion' => 'SOLICITUD APROBADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 41,
            'codigo' => 'APR-R',
            'orden' => 41,
            'descripcion' => 'SOLICITUD RECHAZDA POR APROBADOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 42,
            'codigo' => 'APR-O',
            'orden' => 42,
            'descripcion' => 'SOLICITUD OBSERVADA POR APROBADOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 43,
            'codigo' => 'APR-M',
            'orden' => 43,
            'descripcion' => 'SOLICITUD MODIFICADA POR APROBADOR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 44,
            'codigo' => 'APR-P',
            'orden' => 44,
            'descripcion' => 'SOLICITUD APROBADA PARCIALMENTE',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //TESORERIA (3) [11, 12, 13]
        DB::table('estados')->insert([
            'id' => 50,
            'codigo' => 'TES',
            'orden' => 50,
            'descripcion' => 'APROBADA TESORERIA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 51,
            'codigo' => 'TES-R',
            'orden' => 51,
            'descripcion' => 'SOLICITUD RECHAZADA POR TESORERIA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 52,
            'codigo' => 'TES-O',
            'orden' => 52,
            'descripcion' => 'SOLICITUD OBSERVADA POR TESORERIA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 53,
            'codigo' => 'TES-M',
            'orden' => 53,
            'descripcion' => 'SOLICITUD MODIFICADA POR TESORERIA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //RENDICIONES (1) [14]
        DB::table('estados')->insert([
            'id' => 60,
            'codigo' => 'REN',
            'orden' =>60,
            'descripcion' => 'SOLICITUD A RENDIR',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 61,
            'codigo' => 'REN-P',
            'orden' =>61,
            'descripcion' => 'SOLICITUD RENDICION PARCIAL',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 62,
            'codigo' => 'REN-F',
            'orden' =>62,
            'descripcion' => 'SOLICITUD RENDIDICION FINALIZADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //APROBACION RENDICION (5) [15, 16, 17, 18, 19]
        DB::table('estados')->insert([
            'id' => 70,
            'codigo' => 'REN-A',
            'orden' => 70,
            'descripcion' => 'RENDICIÓN APROBADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 71,
            'codigo' => 'REN-R',
            'orden' => 71,
            'descripcion' => 'RENDICIÓN RECHAZADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 72,
            'codigo' => 'REN-OA',
            'orden' => 72,
            'descripcion' => 'SOLICITUD OBSERVADA EN RENDICIÓN A',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 73,
            'codigo' => 'REN-OB',
            'orden' => 73,
            'descripcion' => 'SOLICITUD OBSERVADA EN RENDICIÓN B',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('estados')->insert([
            'id' => 74,
            'codigo' => 'REN-OC',
            'orden' => 74,
            'descripcion' => 'SOLICITUD OBSERVADA EN RENDICIÓN C',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //SOLICITUD VENCIDA
        DB::table('estados')->insert([
            'id' => 80,
            'codigo' => 'SOL-V',
            'orden' => 80,
            'descripcion' => 'SOLICITUD VENCIDA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //SOLICITUD FINALIZADA (1) [20]
        DB::table('estados')->insert([
            'id' => 90,
            'codigo' => 'FIN',
            'orden' => 90,
            'descripcion' => 'SOLICITUD FINALIZADA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
