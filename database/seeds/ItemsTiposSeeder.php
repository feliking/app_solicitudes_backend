<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ItemsTiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items_tipos')->insert([
            'codigo' => 'AF',
            'descripcion' => 'ACTIVO FIJO',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('items_tipos')->insert([
            'codigo' => 'GO',
            'descripcion' => 'GASTO OPERATIVO',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('items_tipos')->insert([
            'codigo' => 'RE',
            'descripcion' => 'REPUESTO',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('items_tipos')->insert([
            'codigo' => 'OB',
            'descripcion' => 'OBRA',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('items_tipos')->insert([
            'codigo' => 'IM',
            'descripcion' => 'IMPUESTO',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('items_tipos')->insert([
            'codigo' => 'PR',
            'descripcion' => 'PRESTAMO',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
