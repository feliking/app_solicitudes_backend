<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class NivelesAprobacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('niveles_aprobacion')->insert([
            'nivel' => '1',
            'desde' => 100000,
            'hasta' => 200000,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('niveles_aprobacion')->insert([
            'nivel' => '2',
            'desde' => 200001,
            'hasta' => 500000,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('niveles_aprobacion')->insert([
            'nivel' => '3',
            'desde' => 500001,
            'hasta' => 1000000,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
