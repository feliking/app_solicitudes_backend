<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ProyectosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Nuevos proyectos Sistema Nuevo
        //1 pragma
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACION',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'CONTABILIDAD',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'LEGAL',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'ACTIVOS FIJOS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'RECURSOS HUMANOS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'GESTIÓN DE CREDITOS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'PROYECTOS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'SISTEMAS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //2 colina
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'CYM KK',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'TYDF KK',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'CIERRE TECNICO KK',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'D9 AGUA POTABLE CBBA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'TACACHIRA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'CYM VILLA INGENIO',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'TYDF VILLA INGENIO',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'ASEO URBANO Y TRATAMIENTO YACUIBA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'YPFB REST. AMBIENTL YACUIBA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'OTB VALVERDE CBBA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'PEDREGAL - SUCRE',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'CHUA COCANI',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'LPL MEJORAMIENTO DE VIA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //3 trebol
        DB::table('proyectos')->insert([
            'nombre' => 'EL ALTO',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'LA PAZ',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //5 tepco
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'PULPERIA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'INSUMOS EN GENERAL',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'VENTA DE VEHICULOS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'SERVICIOS E INSUMOS DE TECNOLOGÍA DE INFORMACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'TEPCO COCHABAMBA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        //6 hino
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'BROCALES Y TAPAS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'LOSETAS',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        //7 tunqui
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'ASEO YACUIBA',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        //8 casa
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        
        //9 bolivian project
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 9,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //10 manos andinas
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'ENTEL',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('proyectos')->insert([
            'nombre' => 'ESTABILIZACIÓN BASE LPL',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 10,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //13 infinit
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 13,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //14 crea
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 14,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //15 urbaniza
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 15,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //16 rigsa
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 16,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //17 jachasol
        DB::table('proyectos')->insert([
            'nombre' => 'ADMINISTRACIÓN',
            'descripcion' => '',
            //'plan' => true,
            'padre_id' => NULL,
            'empresa_id' => 17,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
