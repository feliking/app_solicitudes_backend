<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => 'ROOT',
            'usuario' => 'root',
            'password' => bcrypt('123456'),
            'empleado_id' => NULL,
            'rol_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'ERICK ALVAREZ VELASQUEZ',
            'usuario' => 'Eralvarez',
            'password' => bcrypt('123456'),
            //'email' => 'eralvarez@pragmainvest.com.bo',
            'empleado_id' => 643,
            'rol_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'ABNEL LOPEZ CHOQUE',
            'usuario' => 'Alopez',
            'password' => bcrypt('123456'),
            //'email' => 'alopez@pragmainvest.com.bo',
            'empleado_id' => 629,
            'rol_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'JORGE BRAVO',
            'usuario' => 'Jbravo',
            'password' => bcrypt('123456'),
            //'email' => 'jbravo@pragmainvest.com.bo',
            'empleado_id' => 652,
            'rol_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'AUTORIZADOR',
            'usuario' => 'autorizador',
            'password' => bcrypt('123456'),
            //'email' => 'autorizador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'REVISOR',
            'usuario' => 'revisor',
            'password' => bcrypt('123456'),
            //'email' => 'autorizador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'APROBADOR',
            'usuario' => 'aprobador',
            'password' => bcrypt('123456'),
            //'email' => 'aprobador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'APROBADOR A',
            'usuario' => 'aprobador a',
            'password' => bcrypt('123456'),
            //'email' => 'aprobador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'APROBADOR B',
            'usuario' => 'aprobador b',
            'password' => bcrypt('123456'),
            //'email' => 'aprobador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'APROBADOR C',
            'usuario' => 'aprobador c',
            'password' => bcrypt('123456'),
            //'email' => 'aprobador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'TESORERIA',
            'usuario' => 'tesoreria',
            'password' => bcrypt('123456'),
            //'email' => 'tesoreria@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'APR RENDICIONES',
            'usuario' => 'apr rendicion',
            'password' => bcrypt('123456'),
            'empleado_id' => NULL,
            'rol_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'MICAELA',
            'usuario' => 'Micaela',
            'password' => bcrypt('123456'),
            //'email' => 'jbravo@pragmainvest.com.bo',
            'empleado_id' => 652,
            'rol_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'MICAELA (AUTORIZADOR)',
            'usuario' => 'Micaela Aut',
            'password' => bcrypt('123456'),
            //'email' => 'autorizador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'MICAELA (REVISOR)',
            'usuario' => 'micaela rev',
            'password' => bcrypt('123456'),
            //'email' => 'autorizador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'MICAELA (APROBADOR)',
            'usuario' => 'Micaela Apr',
            'password' => bcrypt('123456'),
            //'email' => 'aprobador@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'nombre' => 'MICAELA (TESORERIA)',
            'usuario' => 'Micaela Tes',
            'password' => bcrypt('123456'),
            //'email' => 'tesoreria@pragmainvest.com.bo',
            'empleado_id' => NULL,
            'rol_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        //Usuarios Externos
        DB::table('users')->insert([
            'id' => 1001,
            'nombre' => 'USUARIO VIAJES',
            'usuario' => 'uprviajes',
            'password' => bcrypt('123456'),
            'empleado_id' => NULL,
            'rol_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'id' => 1002,
            'nombre' => 'USUARIO TEPCO PEDIDOS',
            'usuario' => 'utppedidos',
            'password' => bcrypt('123456'),
            'empleado_id' => NULL,
            'rol_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
