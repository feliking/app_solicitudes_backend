<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transferencias', function (Blueprint $table) {
            $table->increments('id');

            $table->float('monto', 15, 2);
            $table->float('factor', 6, 4);

            $table->integer('cuenta_origen_id')->unsigned();
            $table->foreign('cuenta_origen_id')->references('id')->on('cuenta_bancarias');
            $table->integer('cuenta_destino_id')->unsigned();
            $table->foreign('cuenta_destino_id')->references('id')->on('cuenta_bancarias');

            $table->integer('tipo_transferencia_id')->unsigned();
            $table->foreign('tipo_transferencia_id')->references('id')->on('items_tipos');

            $table->integer('solicitud_id')->unsigned();
            $table->foreign('solicitud_id')->references('id')->on('solicitudes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transferencias');
    }
}
