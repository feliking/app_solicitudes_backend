<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblItemsRendicion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_rendiciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad')->unsigned;
            $table->float('costo', 15, 2);
            $table->integer('num_factura');
            $table->text('detalle');
            $table->text('direccion');  //documento
            $table->boolean('activo_fijo')->default(false);
            $table->integer('item_id')->unsigned()->nullable();
            $table->integer('unidad_id')->unsigned();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->integer('solicitud_id')->unsigned();
            $table->foreign('solicitud_id')->references('id')->on('solicitudes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_rendiciones');
    }
}
