<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('nombre');
            $table->string('razon_social');
            $table->string('nit')->unique();
            $table->text('descripcion')->nullable();
            $table->text('telefono')->nullable();
            $table->text('direccion')->nullable();
            $table->text('email')->nullable();
            $table->string('logo');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresas');
    }
}
