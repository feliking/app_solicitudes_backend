<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCuentaBancaria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuenta_bancarias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero');
            $table->float('monto_inicial', 15, 2);
            $table->float('saldo', 15, 2);
            $table->integer('moneda_id')->unsigned()->required();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->integer('empresa_id')->unsigned()->required();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('banco_id')->unsigned()->required();
            $table->foreign('banco_id')->references('id')->on('bancos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuenta_bancarias');
    }
}
