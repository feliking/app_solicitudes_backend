<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFondorotativos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fondorotativos', function (Blueprint $table) {
            $table->decimal('monto', 15, 5)->after('descripcion')->default(0);
            $table->boolean('status')->after('monto')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fondorotativos', function (Blueprint $table) {
            $table->dropColumn(['monto', 'status']);
        });
    }
}
