<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblMovimientosBancarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos_bancarios', function (Blueprint $table) {
            $table->increments('id');
            $table->float('monto', 15, 2);
            $table->integer('cuenta_ori_id')->unsigned()->nullable();
            $table->foreign('cuenta_ori_id')->references('id')->on('cuenta_bancarias');
            $table->integer('cuenta_des_id')->unsigned()->nullable();
            $table->foreign('cuenta_des_id')->references('id')->on('cuenta_bancarias');
            $table->integer('tipo_movimiento_id')->unsigned();
            $table->foreign('tipo_movimiento_id')->references('id')->on('tipo_movimientos_bancarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos_bancarios');
    }
}
