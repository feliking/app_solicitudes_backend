<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMonedaIdToTiposCambioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipos_cambio', function (Blueprint $table) {
            /**
             * BEGIN: ISSUE 0002
             */
            $table->integer('moneda_id')->after('cambio')->unsigned()->nullable();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            /**
             * END: ISSUE 0002
             */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipos_cambio', function (Blueprint $table) {
            /**
             * BEGIN: ISSUE 0002
             */
            $table->dropForeign(['moneda_id']);
            $table->dropColumn(['moneda_id']);
            /**
             * END: ISSUE 0002
             */
        });
    }
}
