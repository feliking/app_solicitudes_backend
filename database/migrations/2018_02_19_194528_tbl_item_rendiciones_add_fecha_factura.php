<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblItemRendicionesAddFechaFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_rendiciones', function (Blueprint $table) {
            $table->date('fecha_factura')->after('costo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_rendiciones', function (Blueprint $table) {
            $table->dropColumn('fecha_factura');
        });
    }
}
