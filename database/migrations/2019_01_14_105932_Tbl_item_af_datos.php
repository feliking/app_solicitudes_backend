<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblItemAfDatos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_af_datos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('descripcion_especifica');
            $table->text('numero_serie');
            $table->text('asignacion');
            $table->text('actividad_especifica');
            $table->boolean('nuevo');
            $table->boolean('reemplazo');
            $table->boolean('garantia');
            $table->integer('item_rendido_id')->unsigned();
            $table->foreign('item_rendido_id')->references('id')->on('item_rendiciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_af_datos');
    }
}
