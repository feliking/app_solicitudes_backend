<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSolicitudesChangeFrtipoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropForeign(['frtipo_id']);

            $table->foreign('frtipo_id')->references('id')->on('fondorotativos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropForeign(['frtipo_id']);
            
            $table->foreign('frtipo_id')->references('id')->on('fondorotativos');
        });
    }
}
