<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactoresTipoCambioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factores_tipo_cambio', function (Blueprint $table) {
            $table->increments('id');
            $table->float('factor', 6, 4);

            $table->integer('moneda_origen_id')->unsigned();
            $table->foreign('moneda_origen_id')->references('id')->on('monedas');
            $table->integer('moneda_destino_id')->unsigned();
            $table->foreign('moneda_destino_id')->references('id')->on('monedas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factores_tipo_cambio');
    }
}
