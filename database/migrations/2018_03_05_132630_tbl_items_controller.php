<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblItemsController extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_controller', function (Blueprint $table) {
            $table->increments('id');
            $table->string('detalle');
            $table->integer('cantidad')->unsigned;
            $table->float('costo', 15, 2);
            $table->integer('numero_partida')->unsigned()->nullable();
            $table->string('nombre_partida')->nullable();
            $table->integer('unidad_id')->unsigned();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->integer('solicitud_id')->unsigned();
            $table->foreign('solicitud_id')->references('id')->on('solicitudes');
            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('items_tipos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_controller');
    }
}
