<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblItemRendicionesAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_rendiciones', function (Blueprint $table) {
            $table->string('proyecto', 255)->after('activo_fijo')->default(null)->nullable();
            $table->string('proveedor', 255)->after('proyecto')->default(null)->nullable();
            $table->enum('tipo', ['FACTURA', 'RECIBO', 'SIN ESPECIFICAR'])->after('proveedor')->default('SIN ESPECIFICAR')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_rendiciones', function (Blueprint $table) {
            $table->dropColumn(['proyecto', 'proveedor', 'tipo']);
        });
    }
}
