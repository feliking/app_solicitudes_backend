<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModalidadesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modalidades_users', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('modalidad_id')
                    ->unsigned();
            $table->foreign('modalidad_id')->references('id')->on('modalidades');

            $table->integer('user_id')
                    ->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modalidades_users');
    }
}
