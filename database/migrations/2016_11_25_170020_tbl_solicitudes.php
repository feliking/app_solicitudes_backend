<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero')->unsigned();
            $table->string('desembolso');
            $table->text('referencia')->nullable();
            $table->text('observacion')->nullable();
            $table->date('fecha_limite')->nullable();
            $table->float('total', 15, 2);
            $table->float('total_rendido', 15, 2)->nullable();
            $table->integer('empleado_id')->unsigned();
            $table->integer('modalidad_id')->unsigned();
            $table->foreign('modalidad_id')->references('id')->on('modalidades');
            $table->integer('moneda_id')->unsigned();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->integer('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyectos');
            $table->integer('gestion_id')->unsigned();
            $table->foreign('gestion_id')->references('id')->on('gestiones');
            $table->integer('prioridad_id')->unsigned()->nullable();
            $table->foreign('prioridad_id')->references('id')->on('prioridades');
            $table->integer('cambio_id')->unsigned();
            $table->foreign('cambio_id')->references('id')->on('tipos_cambio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
