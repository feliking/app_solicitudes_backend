<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group([
    'middleware' => 'guest:api',
    'prefix' => 'v1'
], function () {
    Route::post('login', 'Api\V1\Auth\LoginController@login');
    Route::post('loginf', 'Api\V1\Auth\LoginController@loginFingerPrint');
    Route::post('login/refresh', 'Api\V1\Auth\LoginController@refresh');
});

Route::group([
    'middleware' => ['jwt'],
    'prefix' => 'v1'
], function() {
    // ***********************Rutas protegidas de JWT******************************
    Route::post('logout', 'Api\V1\Auth\LoginController@logout');
    Route::get('me', 'Api\V1\Auth\LoginController@me');

    // ***********************Rutas protegidas del sistema*************************

    // ***********************Ayudantes de usuario*********************************
    Route::post('user_get_revisor', 'Api\V1\UserController@getRevisor');

    // ***********************Rutas para la aplicación*****************************
    Route::get('/seleccion-empresa', 'Api\V1\EmpresaController@seleccion_empresa');
    Route::post('solicitudes', 'Api\V1\SolicitudController@index')->name('solicitudes');
    Route::get('rendiciones/{empresa}/{gestion}', 'Api\V1\SolicitudController@rendiciones')->name('rendiciones');
    // Route::get('solicitudes/{empresa}', 'Api\V1\SolicitudController@index')->name('solicitudes');
    Route::post('solicitud-autorizar', array('uses' => 'Api\V1\SolicitudController@autorizar'));
    Route::get('solicitud-autorizacion/{id_sol}/{obs}/{platform}', array('uses' => 'Api\V1\SolicitudController@guardarAutorizacion'));
    Route::get('solicitudes-disponibles', 'Api\V1\SolicitudController@disponibles')->name('solicitudes.disponibles');
    Route::post('solicitudes-disponibles', 'Api\V1\SolicitudController@disponibles')->name('solicitudes.disponibles');
    Route::post('solicitud-observacion', array('uses' => 'Api\V1\SolicitudController@observacion'));
    Route::post('solicitud-rechazo', array('uses' => 'Api\V1\SolicitudController@rechazo'));

    Route::get('rendir/solicitud/{solicitud_id}', 'Api\V1\SolicitudController@rendir')->name('rendir/solicitud');

    Route::get('json/cantidad/estados/{empresa_id}/{gestion_id}', 'Api\V1\JsonController@getCantidadEstados');
    Route::get('json/datos_solicitud/{solicitud_id}', 'Api\V1\JsonController@getDatosSolicitud');
    Route::get('json/documento/eliminar/{documento_id}/{gestion}', 'Api\V1\DocumentosController@eliminarDoc');
    Route::get('json/proyectos/empresa/{empresa_id}', 'Api\V1\JsonController@getProyectosEmpresa');
    // Route::resource('solicitud', 'Api\V1\SolicitudController');
    Route::get('solicitudes/estados/{id}', 'Api\V1\SolicitudController@getEstados')->name('solicitudes.getEstados');
    Route::get('solicitud/create/{empresa_id}', 'Api\V1\SolicitudController@create');
    Route::post('solicitud', 'Api\V1\SolicitudController@store');
    Route::put('solicitud/{id}', 'Api\V1\SolicitudController@update');
    // Rutas RESTfull
    Route::resource('unidad', 'Api\V1\UnidadController');
    Route::resource('item-tipo', 'Api\V1\ItemTipoController');
    Route::resource('gestion', 'Api\V1\GestionController');
    Route::resource('empresa', 'Api\V1\EmpresaController');

    // REVISOR
    Route::get('solicitud-controller', 'Api\V1\SolicitudController@controller');
    Route::post('solicitud-controller', 'Api\V1\SolicitudController@controller');
    Route::get('solicitud-revision/{id_sol}/{obs}/{platform}', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionController'));
    Route::get('solicitudes-controller/cambio/pendiente', 'Api\V1\SolicitudController@controllerSolicitudPendiente');
    Route::post('solicitud-controller-buscador', 'Api\V1\SolicitudController@buscadorController');

    // APROBADOR
    Route::post('solicitud-aprobar', 'Api\V1\SolicitudController@aprobar');
    Route::get('solicitud-aprobar', 'Api\V1\SolicitudController@aprobar');
    Route::put('solicitudes/modificar/aprobador', 'Api\V1\SolicitudController@cambiarAprobadorSolicitud');

    // RENDICIÓN
    Route::get('solicitud-aprobar-rendicion', array('uses' => 'Api\V1\SolicitudController@aprobarRendicionSolicitud'));

    // PRESIDENCIA
    Route::post('solicitudes/admin', 'Api\V1\SolicitudController@solicitudesAdmin');
    Route::get('solicitudes/admin', 'Api\V1\SolicitudController@solicitudesAdmin');
    Route::get('solicitudes/admin/aprobadas', 'Api\V1\SolicitudController@solicitudesAdminAprobadas');
    Route::post('solicitudes-admin/creadas', 'Api\V1\SolicitudController@adminCreadas');
    Route::get('solicitudes-admin/creadas', 'Api\V1\SolicitudController@adminCreadas');
    Route::post('solicitudes-admin/autorizadas', 'Api\V1\SolicitudController@adminAutorizadas');
    Route::get('solicitudes-admin/autorizadas', 'Api\V1\SolicitudController@adminAutorizadas');
    Route::post('solicitudes-admin/transf_tesoreria', 'Api\V1\SolicitudController@adminTransferencias');
    Route::get('solicitudes-admin/transf_tesoreria', 'Api\V1\SolicitudController@adminTransferencias');
    Route::post('solicitudes-admin/pendientes1', 'Api\V1\SolicitudController@adminPendientes');
    Route::get('solicitudes-admin/pendientes1', 'Api\V1\SolicitudController@adminPendientes');
    Route::post('solicitudes-admin/pendientes2', 'Api\V1\SolicitudController@adminPendientes2');
    Route::get('solicitudes-admin/pendientes2', 'Api\V1\SolicitudController@adminPendientes2');
    Route::post('solicitudes-admin/pendientes3', 'Api\V1\SolicitudController@adminPendientes3');
    Route::get('solicitudes-admin/pendientes3', 'Api\V1\SolicitudController@adminPendientes3');
    Route::post('solicitudes-admin/pendientes4', 'Api\V1\SolicitudController@adminPendientes4');
    Route::get('solicitudes-admin/pendientes4', 'Api\V1\SolicitudController@adminPendientes4');
    Route::post('solicitudes-admin/pendientes5', 'Api\V1\SolicitudController@adminPendientes5');
    Route::get('solicitudes-admin/pendientes5', 'Api\V1\SolicitudController@adminPendientes5');
    Route::post('solicitudes-admin/tesoreria', 'Api\V1\SolicitudController@adminTesoreria');
    Route::get('solicitudes-admin/tesoreria', 'Api\V1\SolicitudController@adminTesoreria');
    Route::post('solicitudes-admin/rechazadas', 'Api\V1\SolicitudController@adminRechazadas');
    Route::get('solicitudes-admin/rechazadas', 'Api\V1\SolicitudController@adminRechazadas');
    Route::post('solicitudes-admin/rechazadas-tesoreria', 'Api\V1\SolicitudController@adminRechazadasTesoreria');
    Route::get('solicitudes-admin/rechazadas-tesoreria', 'Api\V1\SolicitudController@adminRechazadasTesoreria');
    Route::post('solicitudes-admin/todas', 'Api\V1\SolicitudController@adminTodas');
    Route::get('solicitudes-admin/todas', 'Api\V1\SolicitudController@adminTodas');

    Route::get('solicitudes-admin/cambio/pendiente', 'Api\V1\SolicitudController@adminSolicitudPendiente');
    Route::get('solicitudes-admin/cambio/pendiente2', 'Api\V1\SolicitudController@adminSolicitudPendiente2');
    Route::get('solicitudes-admin/cambio/pendiente3', 'Api\V1\SolicitudController@adminSolicitudPendiente3');
    Route::get('solicitudes-admin/cambio/pendiente4', 'Api\V1\SolicitudController@adminSolicitudPendiente4');
    Route::get('solicitudes-admin/cambio/pendiente5', 'Api\V1\SolicitudController@adminSolicitudPendiente5');
    Route::get('solicitudes-admin/aprobar/flash', 'Api\V1\SolicitudController@adminSolicitudAprobadas');

    Route::get('solicitud-aprobacion-creada', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionCreada'));
    Route::get('solicitud-aprobacion-autorizada', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionAutorizada'));
    Route::get('solicitud-aprobacion', array('uses' => 'Api\V1\SolicitudController@guardarAprobacion'));
    Route::get('solicitud-aprobacion-pendiente', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionPendiente'));
    Route::get('solicitud-aprobacion-pendiente2', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionPendiente2'));
    Route::get('solicitud-aprobacion-pendiente3', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionPendiente3'));
    Route::get('solicitud-aprobacion-pendiente4', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionPendiente4'));
    Route::get('solicitud-aprobacion-pendiente5', array('uses' => 'Api\V1\SolicitudController@guardarAprobacionPendiente5'));

    Route::post('solicitud-admin-observacion-creada', array('uses' => 'Api\V1\SolicitudController@adminGuardarObservacionCreada'));
    Route::post('solicitud-admin-rechazo', array('uses' => 'Api\V1\SolicitudController@adminGuardarRechazo'));
    Route::post('solicitud-admin-rechazo-creada', array('uses' => 'Api\V1\SolicitudController@adminGuardarRechazoCreada'));
    Route::post('solicitud-admin-rechazo-revisada', array('uses' => 'Api\V1\SolicitudController@adminGuardarRechazoRevisada'));
    Route::post('solicitud-admin-observacion', array('uses' => 'Api\V1\SolicitudController@adminGuardarObservacion'));
    Route::post('solicitud-admin-observacion-creada', array('uses' => 'Api\V1\SolicitudController@adminGuardarObservacionCreada'));
    Route::post('solicitud-admin-observacion-revisada', array('uses' => 'Api\V1\SolicitudController@adminGuardarObservacionRevisada'));

    // GLOBAL
    /**
     * Global view
     */
    // Autorizar
    Route::get('global/autorizar', 'Api\V1\SolicitudGlobalController@autorizar')->name('global.autorizar');
    Route::post('global/autorizar', 'Api\V1\SolicitudGlobalController@autorizar')->name('global.autorizar');
    Route::get('global/autorizar/disponibles', 'Api\V1\SolicitudGlobalController@autorizar_disponibles')->name('global.autorizar.disponibles');
    Route::post('global/autorizar/disponibles', 'Api\V1\SolicitudGlobalController@autorizar_disponibles')->name('global.autorizar.disponibles');
    // Aprobar
    Route::get('global/aprobar', 'Api\V1\SolicitudGlobalController@aprobar')->name('global.aprobar');
    Route::post('global/aprobar', 'Api\V1\SolicitudGlobalController@aprobar')->name('global.aprobar');
    Route::get('global/aprobar/disponibles', 'Api\V1\SolicitudGlobalController@aprobar_disponibles')->name('global.aprobar.disponibles');
    Route::post('global/aprobar/disponibles', 'Api\V1\SolicitudGlobalController@aprobar_disponibles')->name('global.aprobar.disponibles');
    // Aprobar rendiciones
    Route::get('global/aprobar_rendiciones', 'Api\V1\SolicitudGlobalController@aprobar_rendiciones')->name('global.aprobar_rendiciones');
    Route::post('global/aprobar_rendiciones', 'Api\V1\SolicitudGlobalController@aprobar_rendiciones')->name('global.aprobar_rendiciones');
    Route::get('global/aprobar_rendiciones/disponibles', 'Api\V1\SolicitudGlobalController@aprobar_rendiciones_disponibles')->name('global.aprobar_rendiciones.disponibles');
    Route::post('global/aprobar_rendiciones/disponibles', 'Api\V1\SolicitudGlobalController@aprobar_rendiciones_disponibles')->name('global.aprobar_rendiciones.disponibles');
    // Pendientes
    Route::get('global/pendientes', 'Api\V1\SolicitudGlobalController@pendientes')->name('global.pendientes');
    Route::post('global/pendientes', 'Api\V1\SolicitudGlobalController@pendientes')->name('global.pendientes');
    Route::get('global/pendientes2', 'Api\V1\SolicitudGlobalController@pendientes2')->name('global.pendientes2');
    Route::post('global/pendientes2', 'Api\V1\SolicitudGlobalController@pendientes2')->name('global.pendientes2');
    Route::get('global/pendientes3', 'Api\V1\SolicitudGlobalController@pendientes3')->name('global.pendientes3');
    Route::post('global/pendientes3', 'Api\V1\SolicitudGlobalController@pendientes3')->name('global.pendientes3');
    Route::get('global/pendientes4', 'Api\V1\SolicitudGlobalController@pendientes4')->name('global.pendientes4');
    Route::post('global/pendientes4', 'Api\V1\SolicitudGlobalController@pendientes4')->name('global.pendientes4');
    Route::get('global/pendientes5', 'Api\V1\SolicitudGlobalController@pendientes5')->name('global.pendientes5');
    Route::post('global/pendientes5', 'Api\V1\SolicitudGlobalController@pendientes5')->name('global.pendientes5');
    // Revisor
    Route::get('global/revisor/sujeto_rendicion', 'SolicitudGlobalController@revisorSujetoRendicion')->name('global.revisor_sujeto_rendicion');
    Route::post('global/revisor/sujeto_rendicion', 'SolicitudGlobalController@revisorSujetoRendicion')->name('global.revisor_sujeto_rendicion');
    Route::get('global/revisor/gasto_directo', 'SolicitudGlobalController@revisorGastoDirecto')->name('global.revisor_gasto_directo');
    Route::post('global/revisor/gasto_directo', 'SolicitudGlobalController@revisorGastoDirecto')->name('global.revisor_gasto_directo');
    Route::get('global/revisor/fondo_rotativo', 'SolicitudGlobalController@revisorFondoRotativo')->name('global.revisor_fondo_rotativo');
    Route::post('global/revisor/fondo_rotativo', 'SolicitudGlobalController@revisorFondoRotativo')->name('global.revisor_fondo_rotativo');
    Route::get('global/revisor/reposicion', 'SolicitudGlobalController@revisorReposicion')->name('global.revisor_reposicion');
    Route::post('global/revisor/reposicion', 'SolicitudGlobalController@revisorReposicion')->name('global.revisor_reposicion');
    // Mis solicitudes
    Route::get('json/cantidad/estados.global', 'Api\V1\JsonController@getCantidadEstadosGlobal');
    Route::get('global/proyectos/{empresa_id}', 'Api\V1\SolicitudGlobalController@getProyectos')->name('global.get_proyectos');
    Route::get('global/crear_solicitud', 'Api\V1\SolicitudGlobalController@create')->name('global.create');
    Route::post('global/crear_solicitud', 'Api\V1\SolicitudGlobalController@store')->name('global.store');
    Route::put('global/solicitud/{id}', 'Api\V1\SolicitudGlobalController@update');
    // En curso
    Route::get('global/en_curso', 'Api\V1\SolicitudGlobalController@en_curso')->name('global.en_curso');
    Route::post('global/en_curso', 'Api\V1\SolicitudGlobalController@en_curso')->name('global.en_curso');
    // Por rendir
    Route::get('global/por_rendir', 'Api\V1\SolicitudGlobalController@por_rendir')->name('global.por_rendir');
    Route::post('global/por_rendir', 'Api\V1\SolicitudGlobalController@por_rendir')->name('global.por_rendir');
    // Observadas
    Route::get('global/observadas', 'Api\V1\SolicitudGlobalController@observadas')->name('global.observadas');
    Route::post('global/observadas', 'Api\V1\SolicitudGlobalController@observadas')->name('global.observadas');
    // Rechazadas
    Route::get('global/rechazadas', 'Api\V1\SolicitudGlobalController@rechazadas')->name('global.rechazadas');
    Route::post('global/rechazadas', 'Api\V1\SolicitudGlobalController@rechazadas')->name('global.rechazadas');
    // Rendidas
    Route::get('global/rendidas', 'Api\V1\SolicitudGlobalController@rendidas')->name('global.rendidas');
    Route::post('global/rendidas', 'Api\V1\SolicitudGlobalController@rendidas')->name('global.rendidas');
    // Finalizadas
    Route::get('global/finalizadas', 'Api\V1\SolicitudGlobalController@finalizadas')->name('global.finalizadas');
    Route::post('global/finalizadas', 'Api\V1\SolicitudGlobalController@finalizadas')->name('global.finalizadas');
    
    // ***********************Reutilización de rutas JSON**************************
    Route::get('json/datos/solicitud/{solicitud_id}', 'JsonController@getDocumentosSolicitud');
    Route::get('json/verifica/solicitud/completa/{solicitud_id}', 'JsonController@solicitudCompleta');
    Route::get('json/items_solicitud/{solicitud_id}', 'JsonController@getItemsSolicitud');
    Route::get('json/items_tiene_antiguos/{solicitud_id}', 'JsonController@getCantidadItemsAntiguosSolicitud');
    Route::get('json/items_antiguos_solicitud/{solicitud_id}', 'JsonController@getItemsAntiguosSolicitud');
    Route::get('json/items_rendidos/{solicitud_id}', 'JsonController@getItemsRendidosSolicitud');
    Route::get('json/documentos_solicitud/{solicitud_id}', 'JsonController@getDocumentosSolicitud');
    Route::get('json/documentos_rendicion/{solicitud_id}', 'JsonController@getDocumentosRendicion');
    Route::get('json/proyecto/fr/{proyecto_id}/', 'JsonController@getFrProyecto');
    Route::get('json/existe_proyecto/{usuario_id}/{proyecto_id}', 'JsonController@existeProyecto');
});