<?php

use Illuminate\Support\Facades\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    Auth::logout();
    Session::flush();
    
    return redirect()->route('login');
});

Auth::routes();

Route::group(['middleware' => ['Solicitudes']], function(){
    Route::get('/seleccion-empresa', 'EmpresaController@seleccion_empresa');

    //Gestiones
    Route::get('gestion/{gestion_id}', 'GestionesController@actualizaGestion')->name('gestion');
    
    //Empresas
    Route::resource('empresa', 'EmpresaController');
    //Route::get('/empresas/listado', 'EmpresaController@listado')->name('empresas/listado');

    //Usuarios
    Route::resource('usuario', 'UsuarioController');
    Route::get('clave/usuario', 'UsuarioController@UsuarioClave')->name('clave/usuario');
    Route::put('clave/usuario/guardar', 'UsuarioController@UsuarioClaveUpdate')->name('clave/usuario/guardar');
    Route::get('empresa/get.empleados/{empresaId}', 'EmpresaController@getEmpleadosPorEmpresa')
        ->name('empresa.getEmpleados');

    //Usuario-Proyecto
    Route::get('asignar/proyectos', 'UsuarioController@UsuarioProyecto');
    Route::post('guardar-asignacion-proyectos', 'UsuarioController@UsuarioProyectoStore')->name('guardar-asignacion-proyecto');
    Route::post('guardar-asignacion-proyecto-individual', 'UsuarioController@usuarioProyectoStoreIndividual')->name('guardar-asignacion-proyecto-individual');

    //Autorizador-Usuario-Proyecto
    Route::get('autorizador/usuario/proyecto/lista', 'UsuarioController@AutorizadorUsuarioProyectoLista');
    Route::get('autorizador/usuario/proyecto/{autorizador_id}', 'UsuarioController@AutorizadorUsuarioProyecto'); //ruta sin usar
    Route::post('autorizador/usuario/proyecto/guardar', 'UsuarioController@AutorizadorUsuarioProyectoStore');

    //Autorizador-Usuario
    Route::post('autorizador/usuario/guardar', 'UsuarioController@AutorizadorUsuarioStore')->name('autorizador/usuario/guardar');

    //Apr Rendicion Empresa
    Route::get('aprorendicion/empresa/lista', 'UsuarioController@AprorendicionEmpresaLista');
    Route::post('aprorendicion/emrpesa/guardar', 'UsuarioController@AprorendicionEmpresaStore')->name('aprorendicion/empresa/guardar');

    //Solicitudes
    Route::resource('solicitud', 'SolicitudController');
    Route::post('solicitudes/admin', 'SolicitudController@solicitudesAdmin')->name('solicitudes.admin');
    Route::get('solicitudes/admin', 'SolicitudController@solicitudesAdmin')->name('solicitudes.admin');
    Route::get('solicitudes/admin/aprobadas', 'SolicitudController@solicitudesAdminAprobadas')->name('solicitudes.admin.aprobadas');
    Route::post('solicitudes', 'SolicitudController@index')->name('solicitudes');
    Route::get('solicitudes', 'SolicitudController@index')->name('solicitudes');
    Route::post('solicitud-rechazar', 'SolicitudController@rechazarSolicitud')->name('solicitud-rechazar');
    Route::get('solicitud-rechazar', 'SolicitudController@rechazarSolicitud')->name('solicitud-rechazar');
    Route::get('solicitud-express', 'SolicitudController@express')->name('solicitud-express');
    Route::get('solicitudes/estados/{id}', 'SolicitudController@getEstados')->name('solicitudes.getEstados');
    Route::get('solicitudes/estado/{idEstado}', 'SolicitudController@getSolicitudesEstado')->name('solicitud.getSolicitudesEstado');
    Route::get('solicitudes-disponibles', 'SolicitudController@disponibles')->name('solicitudes.disponibles');
    Route::post('solicitudes-disponibles', 'SolicitudController@disponibles')->name('solicitudes.disponibles');
    Route::put('solicitudes/proyecto/guardar', 'SolicitudController@cambiarProyectoSolicitud')->name('solicitudes.proyecto.guardar');
    Route::put('solicitudes/modificar/aprobador', 'SolicitudController@cambiarAprobadorSolicitud')->name('solicitudes.modificar.aprobador');
    Route::get('solicitudes/modificar/aprobador', 'SolicitudController@cambiarAprobadorSolicitud')->name('solicitudes.modificar.aprobador');

    Route::get('solicitudes/item/rendidos/eliminar', 'SolicitudController@eliminarItemRendido');

    Route::post('solicitudes-admin/creadas', 'SolicitudController@adminCreadas');
    Route::get('solicitudes-admin/creadas', 'SolicitudController@adminCreadas');
    Route::post('solicitudes-admin/autorizadas', 'SolicitudController@adminAutorizadas');
    Route::get('solicitudes-admin/autorizadas', 'SolicitudController@adminAutorizadas');
    Route::post('solicitudes-admin/transf_tesoreria', 'SolicitudController@adminTransferencias');
    Route::get('solicitudes-admin/transf_tesoreria', 'SolicitudController@adminTransferencias');
    Route::post('solicitudes-admin/pendientes', 'SolicitudController@adminPendientes');
    Route::get('solicitudes-admin/pendientes', 'SolicitudController@adminPendientes');
    Route::post('solicitudes-admin/pendientes2', 'SolicitudController@adminPendientes2');
    Route::get('solicitudes-admin/pendientes2', 'SolicitudController@adminPendientes2');
    Route::post('solicitudes-admin/pendientes3', 'SolicitudController@adminPendientes3');
    Route::get('solicitudes-admin/pendientes3', 'SolicitudController@adminPendientes3');
    Route::post('solicitudes-admin/pendientes4', 'SolicitudController@adminPendientes4');
    Route::get('solicitudes-admin/pendientes4', 'SolicitudController@adminPendientes4');
    Route::post('solicitudes-admin/pendientes5', 'SolicitudController@adminPendientes5');
    Route::get('solicitudes-admin/pendientes5', 'SolicitudController@adminPendientes5');
    Route::post('solicitudes-admin/tesoreria', 'SolicitudController@adminTesoreria');
    Route::get('solicitudes-admin/tesoreria', 'SolicitudController@adminTesoreria');
    Route::post('solicitudes-admin/rechazadas', 'SolicitudController@adminRechazadas');
    Route::get('solicitudes-admin/rechazadas', 'SolicitudController@adminRechazadas');
    Route::post('solicitudes-admin/rechazadas-tesoreria', 'SolicitudController@adminRechazadasTesoreria');
    Route::get('solicitudes-admin/rechazadas-tesoreria', 'SolicitudController@adminRechazadasTesoreria');
    Route::post('solicitudes-admin/todas', 'SolicitudController@adminTodas');
    Route::get('solicitudes-admin/todas', 'SolicitudController@adminTodas');

    Route::get('solicitudes-admin/cambio/pendiente', 'SolicitudController@adminSolicitudPendiente');
    Route::get('solicitudes-admin/cambio/pendiente2', 'SolicitudController@adminSolicitudPendiente2')->name('pendiente2');
    Route::get('solicitudes-admin/cambio/pendiente3', 'SolicitudController@adminSolicitudPendiente3')->name('pendiente3');
    Route::get('solicitudes-admin/cambio/pendiente4', 'SolicitudController@adminSolicitudPendiente4')->name('pendiente4');
    Route::get('solicitudes-admin/cambio/pendiente5', 'SolicitudController@adminSolicitudPendiente5')->name('pendiente5');
    Route::get('solicitudes-admin/aprobar/flash', 'SolicitudController@adminSolicitudAprobadas');

    Route::get('solicitudes-controller/cambio/pendiente', 'SolicitudController@controllerSolicitudPendiente');

    Route::get('solicitud-lastEstado/{solicitud_id}', 'SolicitudController@getLastEstado')->name('solicitud-lastEstado');
    Route::get('solicitud-lastEstadoCodigo/{solicitud_id}', 'SolicitudController@getLastEstadoCodigo')->name('solicitud-lastEstado');
    Route::get('UsuarioPorEstado/{estado_id}/{solicitud_id}', 'SolicitudController@getUsuarioPorEstado')->name('UsuarioPorEstado');

    Route::post('solicitud-autorizar', array('uses' => 'SolicitudController@autorizar'));
    Route::get('solicitud-autorizar', 'SolicitudController@autorizar')
        ->name('solicitudes.autorizar');//***REVISAR

    Route::get('solicitud-autorizacion', array('uses' => 'SolicitudController@guardarAutorizacion'));

    Route::post('solicitud-controller-buscador', 'SolicitudController@buscadorController');
    // Route::get('solicitud-controller', 'SolicitudController@controller')
    //     ->name('solicitudes.revisar');

    // Route::get('solicitud-controller-sr', 'SolicitudController@controller_sr')
    //     ->name('controller.sr');
    // Route::get('solicitud-controller-gd', 'SolicitudController@controller_gd')
    //     ->name('controller.gd');
    // Route::get('solicitud-controller-fra', 'SolicitudController@controller_fra')
    //     ->name('controller.fra');
    // Route::get('solicitud-controller-fro', 'SolicitudController@controller_fro')
    //     ->name('controller.fro');

    Route::get('solicitud-controller', 'SolicitudController@controller')
        ->name('controller.sr');
    Route::post('solicitud-controller', 'SolicitudController@controller');
    
    Route::get('solicitud-controller-store', array('uses' => 'SolicitudController@guardarController'));
    Route::get('solicitud-controller-editar/{solicitud_id}/{controller}', array('uses' => 'SolicitudController@modificarController'))->name('solicitud.controller.editar');

    Route::get('solicitud-apr-rendiciones-editar/{solicitud_id}/{aprobador}', array('uses' => 'SolicitudController@modificarAprRendicion'))->name('solicitud.apr.rendiciones.editar');

    Route::get('solicitud-aprobacion-controller', array('uses' => 'SolicitudController@guardarAprobacionController'));

    Route::post('solicitud-aprobar', 'SolicitudController@aprobar');
    Route::get('solicitud-aprobar', 'SolicitudController@aprobar')
        ->name('solicitudes.aprobar');

    Route::get('solicitud-aprobacion', array('uses' => 'SolicitudController@guardarAprobacion'));
    Route::post('solicitud-admin-rechazo', array('uses' => 'SolicitudController@adminGuardarRechazo'));
    Route::post('solicitud-admin-rechazo', array('uses' => 'SolicitudController@adminGuardarRechazo'));
    Route::post('solicitud-admin-rechazo-creada', array('uses' => 'SolicitudController@adminGuardarRechazoCreada'));
    Route::post('solicitud-admin-rechazo-revisada', array('uses' => 'SolicitudController@adminGuardarRechazoRevisada'));
    Route::post('solicitud-admin-observacion', array('uses' => 'SolicitudController@adminGuardarObservacion'));
    Route::post('solicitud-admin-observacion-creada', array('uses' => 'SolicitudController@adminGuardarObservacionCreada'));
    Route::post('solicitud-admin-observacion-revisada', array('uses' => 'SolicitudController@adminGuardarObservacionRevisada'));

    Route::post('solicitud-consultor', 'SolicitudController@buscadorConsultor');
    Route::get('solicitud-consultor', 'SolicitudController@consultor');
    Route::get('solicitud-aprobar-rendicion', array('uses' => 'SolicitudController@aprobarRendicionSolicitud'));

    Route::get('solicitud-aprobacion-creada', array('uses' => 'SolicitudController@guardarAprobacionCreada'));
    Route::get('solicitud-aprobacion-autorizada', array('uses' => 'SolicitudController@guardarAprobacionAutorizada'));
    Route::get('solicitud-aprobacion-pendiente', array('uses' => 'SolicitudController@guardarAprobacionPendiente'));
    Route::get('solicitud-aprobacion-pendiente2', array('uses' => 'SolicitudController@guardarAprobacionPendiente2'));
    Route::get('solicitud-aprobacion-pendiente3', array('uses' => 'SolicitudController@guardarAprobacionPendiente3'));
    Route::get('solicitud-aprobacion-pendiente4', array('uses' => 'SolicitudController@guardarAprobacionPendiente4'));
    Route::get('solicitud-aprobacion-pendiente5', array('uses' => 'SolicitudController@guardarAprobacionPendiente5'));

    Route::get('solicitud-aprobacion-transferencia', array('uses' => 'SolicitudController@guardarAprobacionTransferencia'));

    Route::post('solicitud-rechazo', array('uses' => 'SolicitudController@rechazo'));
    Route::post('solicitud-observacion', array('uses' => 'SolicitudController@observacion'));

    Route::post('solicitud-tesoreria', 'SolicitudController@buscadorTesoreria');
    Route::get('solicitud-tesoreria', 'SolicitudController@tesoreria')
        ->name('solicitudes-tesoreria');
    Route::get('registro-tesoreria/{solicitud_id}', 'SolicitudController@registroTesoreria');

    Route::post('aprobador-rendiciones', 'SolicitudController@aprobadorRendicion');
    Route::get('aprobador-rendiciones', 'SolicitudController@aprobadorRendicion');

    //Aprobador parcial
    Route::get('aprobador/solicitudes/parciales', 'SolicitudController@aprParcialSolicitudes');

    //Pdf
    Route::post('/download-pdf', 'PdfController@generar');
    Route::get('solicitud-imprimir', 'PdfController@imprimirSolicitud')->name('solicitud-imprimir');
    Route::get('solicitud-imprimir/completa', 'PdfController@imprimirSolicitudCompleta')->name('solicitud-imprimir.completa');

    Route::get('solicitud-imprimir/rendicion', 'PdfController@imprimirRendicionSolicitud')->name('solicitud-imprimir.rendicion');

    Route::get('solicitud-imprimir/formulario/af', 'PdfController@imprimirFormularioAF')->name('solicitud-imprimir.formulario.af');
    Route::get('solicitud-imprimir/formulario/items-af', 'PdfController@imprimirFormularioItemsAF')->name('solicitud-imprimir.formulario.itemsaf');

    Route::get('solicitud-imprimir-transferencia', 'PdfController@imprimirSolicitudTransferencia')->name('solicitud-imprimir.transferencia');

    //Proyectos
    Route::resource('proyectos', 'ProyectosController');
    Route::post('/proyectos-form', function(){
        return view('proyectos.form');
    })->name('proyectos-form');

    Route::post('solicitud-guardar-archivo', array('uses' => 'SolicitudController@guardarArchivo'));

    //EmpresaUsuario
    Route::post('/asignar-empresa', 'UsuarioController@UsuarioEmpresa')->name('asignar-empresa');
    Route::post('/guardar-asignacion-empresa', 'UsuarioController@UsuarioEmpresaStore')->name('guardar-asignacion-empresa');

    //Estados
    Route::resource('estados', 'EstadosController');

    //Roles
    Route::resource('roles', 'RolesController');

    //Bancos
    Route::resource('bancos', 'BancosController');

    //Cuentas
    Route::resource('cuentas', 'BancoCuentasController');

    //******* JSON *******
    Route::get('json/tipo_cambio', 'JsonController@getCambio');
    Route::get('json/usuario_proyectos/{usuario_id}', 'JsonController@getUsuarioProyectos');
    Route::get('json/documentos_solicitud/{solicitud_id}', 'JsonController@getDocumentosSolicitud');
    Route::get('json/documentos_rendicion/{solicitud_id}', 'JsonController@getDocumentosRendicion')
        ->name('json.documentos_rendicion');
    Route::get('json/items_solicitud/{solicitud_id}', 'JsonController@getItemsSolicitud');
    Route::get('json/items_tiene_antiguos/{solicitud_id}', 'JsonController@getCantidadItemsAntiguosSolicitud');
    Route::get('json/items_antiguos_solicitud/{solicitud_id}', 'JsonController@getItemsAntiguosSolicitud');
    Route::get('json/items_rendidos/{solicitud_id}', 'JsonController@getItemsRendidosSolicitud');
    Route::get('json/datos_solicitud/{solicitud_id}', 'JsonController@getDatosSolicitud');
    Route::get('json/asignacion_autorizador/{autorizador_id}', 'JsonController@getDatosAutorizadorAsignacion');
    Route::get('json/datos_item/{item_id}', 'JsonController@getDatosItem');
    Route::get('json/cantidad/estados/{empresa_id}', 'JsonController@getCantidadEstados');
    Route::get('json/cantidad/empresas/estados', 'JsonController@getCantidadEstadosEmpresas');
    Route::get('json/estados/solicitud', 'JsonController@getEstadosSolicitud');
    Route::get('json/porcentaje/aprobador/{aprobador_id}/{solicitud_id}', 'JsonController@getPorcentajeAprobador');
    Route::get('json/datos/cheque/{cheque_id}', 'JsonController@getDatosCheque');
    Route::get('json/proyectos/empresa/{empresa_id}', 'JsonController@getProyectosEmpresa');
    Route::get('json/datos/solicitud/{solicitud_id}', 'JsonController@getDocumentosSolicitud');
    Route::get('json/verifica/solicitud/completa/{solicitud_id}', 'JsonController@solicitudCompleta');
    Route::get('json/cuentas/empresa/{empresa_id}/{banco_id}', 'JsonController@getCuentasEmpresa');
    Route::get('json/bancos/empresa/{empresa_id}', 'JsonController@getBancosEmpresa');
    Route::get('json/tipos/compra', 'JsonController@getTiposCompra');
    Route::get('json/empresas', 'JsonController@getEmpresas');   
    Route::get('json/existe_proyecto/{usuario_id}/{proyecto_id}', 'JsonController@existeProyecto');
    Route::get('json/proyectos_usuario/{proyecto_id}/{usuario_id}', 'JsonController@proyectosUsuario');
    Route::get('json/item/rendicion/af/{solicitud_id}', 'JsonController@getDatosItemsAF');
    Route::get('json/item/formularios/af/{solicitud_id}', 'JsonController@getFormuItemsAF');
    Route::get('json/asignacion_aprorendicion/{usuario_id}', 'JsonController@getDatosAprorendicionAsignacion');
    Route::get('json/proyecto/fr/{proyecto_id}/', 'JsonController@getFrProyecto');

    //Password Generator
    Route::get('generador', 'JsonController@passGenerator');

    //Cheques
    Route::get('cheques/anulados', 'ChequesController@chequesAnulados')->name('cheques/anulados');
    Route::get('cheques/registrados', 'ChequesController@chequesRegistrados')->name('cheques/registrados');
    Route::resource('cheques', 'ChequesController');
    Route::post('cheque', 'ChequesController@index')->name('cheque');
    Route::get('cheque', 'ChequesController@index')->name('cheque');

    Route::get('cheque-lastEstado/{cheque_id}', 'ChequesController@getLastEstadoCheque')->name('cheque-lastEstado');
    Route::get('cheque/anular', array('uses' => 'ChequesController@anularCheque'));
    Route::get('cheque/anular/solicitud', array('uses' => 'ChequesController@anularSolicitudCheque'));

    //Configuraciones
    Route::resource('configuraciones', 'ConfiguracionesController');
    Route::get('configuracion/vidautil', 'ConfiguracionesController@solicitudDuracion')->name('configuracion/vidautil');

    //Rendiciones
    Route::get('rendiciones', 'SolicitudController@rendiciones')->name('rendiciones');
    Route::get('rendir/solicitud/{solicitud_id}', 'SolicitudController@rendir')->name('rendir/solicitud');
    Route::post('rendir/solicitud/item/', 'SolicitudController@itemRendicionStore')->name('rendir.solicitud.item');
    Route::post('rendir/solicitud/devolucion/', 'SolicitudController@itemDevolucionStore')->name('rendir.solicitud.devolucion');
    Route::post('finalizar/solicitud/', 'SolicitudController@finalizarSolicitudStore')->name('finalizar.solicitud');
    Route::get('solicitud-rendiciones', 'SolicitudController@solicitudRendiciones')->name('solicitud-rendiciones');
    Route::get('solicitud-aprobar-rendicion/{solicitud_id}', 'SolicitudController@solicitudAprobarRendicion')->name('solicitud-aprobar-rendicion');
    Route::post('solicitud-aprobar-rendicion-guardar/', array('uses' => 'SolicitudController@solicitudAprobarRendicionStore'))->name('solicitud.aprobar.rendicion.guardar');
    Route::post('solicitud-documentos/rendicion/guardar/', array('uses' => 'SolicitudController@solicitudDocumentoRendicionStore'))->name('solicitud.documentos.rendicion.guardar');

    //Servicios
    Route::get('servicios/empresas', 'ServiciosController@getEmpresas')->middleware('Bypass');

    //Documentos
    Route::get('/documentoshow/{id}',[
        'uses' => 'DocumentosController@getDocumentoShow',
        'as'=> 'documentos.showcr'
    ]);

    Route::get('json/documento/eliminar/{documento_id}', 'DocumentosController@eliminarDoc');

    Route::get('descargar/{id}', [
        'uses' => 'DocumentosController@getDocumento',
        'as'=> 'doc.descargar'
    ]);

    // ViewerJS
    Route::any('ViewerJS/{all?}', function(){
        return View::make('ViewerJS.index');
    });

    //Libreta Bancaria
    Route::get('libreta', 'LibretaBancariaController@index');
    Route::get('transferencia', 'LibretaBancariaController@transferencia')->name('transferencia');
    Route::post('transferencia/guardar', array('uses' => 'LibretaBancariaController@transferenciaStore'))->name('transferencia.store');
    Route::get('movimiento/ingreso', 'LibretaBancariaController@ingreso')->name('movimiento.ingreso');
    Route::post('movimiento/ingreso/guardar', array('uses' => 'LibretaBancariaController@ingresoStore'))->name('ingreso.store');
    Route::get('movimiento/egreso', 'LibretaBancariaController@egreso')->name('movimiento.egreso');
    Route::post('movimiento/egreso/guardar', array('uses' => 'LibretaBancariaController@egresoStore'))->name('egreso.store');

    //Reportes
    Route::get('reporte/general', 'ReportesController@reporteGeneral')->name('reporte.general');
    Route::get('reporte/general/usuario', 'ReportesController@reporteGeneralUsuario')->name('reporte.general.usuario');
    Route::post('reporte/general/usuario', 'ReportesController@reporteGeneralUsuario')->name('reporte.general.usuario');
    Route::get('reporte/revisor', 'ReportesController@reporteRevisor')->name('reporte.revisor');
    Route::post('reporte/revisor', 'ReportesController@reporteRevisor')->name('reporte.revisor');
    Route::get('reporte/apr_rendiciones', 'ReportesController@reporteAprRendiciones')->name('reporte.aprrendiciones');
    Route::post('reporte/apr_rendiciones', 'ReportesController@reporteAprRendiciones')->name('reporte.aprrendiciones');

    Route::get('reporte/solicitudes/rendidas', 'ReportesController@reporteSolicitudesRendidas');
    Route::get('reporte/cantidad/solicitudes/proyectos', 'ReportesController@reporteSolicitudesProyecto');
    Route::get('reporte/empresa/proyectos/usuarios', 'ReportesController@reporteSolicitudesProyectoEmpresa');

    Route::get('reporte/activos_fijos', 'ReportesController@reporteActivosFijos')
        ->name('reportes.activos_fijos');
    Route::post('reporte/activos_fijos', 'ReportesController@reporteActivosFijos')
        ->name('reportes.activos_fijos');

    //Excel
    Route::get('exportar/excel', 'ExcelController@generarExcel')->name('reporte.excel');
    Route::post('exportar/excel/revisor', 'ExcelController@generarExcelRevisor')->name('reporte.excel.revisor');
    Route::get('exportar/excel/apr_rendiciones', 'ExcelController@generarExcelAprRendiciones')->name('reporte.excel.aprrendiciones');
    Route::get('exportar/excel/cantidades', 'ExcelController@generarExcelCantidades')->name('reporte.excel.cantidades');
    // Excel - Importar
    Route::post('items/importar', 'ExcelController@importExcel')->name('importar.excel');

    // PDF admin
    Route::get('imprimir/solicitudes/{solicitudes_ids}/{variables}', 'PdfController@imprimirSolicitudes')->name('reporte.solicitudes');

    // Download File
    Route::get('/public/{file}', function ($file='') {
        return response()->download(public_path($file));
    });

    Route::get('test', array('uses' => 'LibretaBancariaController@egresoStore'));

    /**
     * Global view
     */
    // Autorizar
    Route::get('global/autorizar', 'SolicitudGlobalController@autorizar')->name('global.autorizar');
    Route::post('global/autorizar', 'SolicitudGlobalController@autorizar')->name('global.autorizar');
    Route::get('global/autorizar/disponibles', 'SolicitudGlobalController@autorizar_disponibles')->name('global.autorizar.disponibles');
    Route::post('global/autorizar/disponibles', 'SolicitudGlobalController@autorizar_disponibles')->name('global.autorizar.disponibles');
    // Aprobar
    Route::get('global/aprobar', 'SolicitudGlobalController@aprobar')->name('global.aprobar');
    Route::post('global/aprobar', 'SolicitudGlobalController@aprobar')->name('global.aprobar');
    Route::get('global/aprobar/disponibles', 'SolicitudGlobalController@aprobar_disponibles')->name('global.aprobar.disponibles');
    Route::post('global/aprobar/disponibles', 'SolicitudGlobalController@aprobar_disponibles')->name('global.aprobar.disponibles');
    // Aprobar rendiciones
    Route::get('global/aprobar_rendiciones', 'SolicitudGlobalController@aprobar_rendiciones')->name('global.aprobar_rendiciones');
    Route::post('global/aprobar_rendiciones', 'SolicitudGlobalController@aprobar_rendiciones')->name('global.aprobar_rendiciones');
    Route::get('global/aprobar_rendiciones/disponibles', 'SolicitudGlobalController@aprobar_rendiciones_disponibles')->name('global.aprobar_rendiciones.disponibles');
    Route::post('global/aprobar_rendiciones/disponibles', 'SolicitudGlobalController@aprobar_rendiciones_disponibles')->name('global.aprobar_rendiciones.disponibles');
    // Pendientes
    Route::get('global/pendientes', 'SolicitudGlobalController@pendientes')->name('global.pendientes');
    Route::post('global/pendientes', 'SolicitudGlobalController@pendientes')->name('global.pendientes');
    Route::get('global/pendientes2', 'SolicitudGlobalController@pendientes2')->name('global.pendientes2');
    Route::post('global/pendientes2', 'SolicitudGlobalController@pendientes2')->name('global.pendientes2');
    Route::get('global/pendientes3', 'SolicitudGlobalController@pendientes3')->name('global.pendientes3');
    Route::post('global/pendientes3', 'SolicitudGlobalController@pendientes3')->name('global.pendientes3');
    Route::get('global/pendientes4', 'SolicitudGlobalController@pendientes4')->name('global.pendientes4');
    Route::post('global/pendientes4', 'SolicitudGlobalController@pendientes4')->name('global.pendientes4');
    Route::get('global/pendientes5', 'SolicitudGlobalController@pendientes5')->name('global.pendientes5');
    Route::post('global/pendientes5', 'SolicitudGlobalController@pendientes5')->name('global.pendientes5');
    // Revisor
    Route::get('global/revisor/sujeto_rendicion', 'SolicitudGlobalController@revisorSujetoRendicion')->name('global.revisor_sujeto_rendicion');
    Route::post('global/revisor/sujeto_rendicion', 'SolicitudGlobalController@revisorSujetoRendicion')->name('global.revisor_sujeto_rendicion');
    Route::get('global/revisor/gasto_directo', 'SolicitudGlobalController@revisorGastoDirecto')->name('global.revisor_gasto_directo');
    Route::post('global/revisor/gasto_directo', 'SolicitudGlobalController@revisorGastoDirecto')->name('global.revisor_gasto_directo');
    Route::get('global/revisor/fondo_rotativo', 'SolicitudGlobalController@revisorFondoRotativo')->name('global.revisor_fondo_rotativo');
    Route::post('global/revisor/fondo_rotativo', 'SolicitudGlobalController@revisorFondoRotativo')->name('global.revisor_fondo_rotativo');
    Route::get('global/revisor/reposicion', 'SolicitudGlobalController@revisorReposicion')->name('global.revisor_reposicion');
    Route::post('global/revisor/reposicion', 'SolicitudGlobalController@revisorReposicion')->name('global.revisor_reposicion');
    // Mis solicitudes
    Route::get('json/cantidad/estados.global', 'JsonController@getCantidadEstadosGlobal');
    Route::get('global/proyectos/{empresa_id}', 'SolicitudGlobalController@getProyectos')->name('global.get_proyectos');
    Route::get('global/crear_solicitud', 'SolicitudGlobalController@create')->name('global.create');
    Route::post('global/crear_solicitud', 'SolicitudGlobalController@store')->name('global.store');
    // En curso
    Route::get('global/en_curso', 'SolicitudGlobalController@en_curso')->name('global.en_curso');
    Route::post('global/en_curso', 'SolicitudGlobalController@en_curso')->name('global.en_curso');
    // Por rendir
    Route::get('global/por_rendir', 'SolicitudGlobalController@por_rendir')->name('global.por_rendir');
    Route::post('global/por_rendir', 'SolicitudGlobalController@por_rendir')->name('global.por_rendir');
    // Observadas
    Route::get('global/observadas', 'SolicitudGlobalController@observadas')->name('global.observadas');
    Route::post('global/observadas', 'SolicitudGlobalController@observadas')->name('global.observadas');
    // Rechazadas
    Route::get('global/rechazadas', 'SolicitudGlobalController@rechazadas')->name('global.rechazadas');
    Route::post('global/rechazadas', 'SolicitudGlobalController@rechazadas')->name('global.rechazadas');
    // Rendidas
    Route::get('global/rendidas', 'SolicitudGlobalController@rendidas')->name('global.rendidas');
    Route::post('global/rendidas', 'SolicitudGlobalController@rendidas')->name('global.rendidas');
    // Finalizadas
    Route::get('global/finalizadas', 'SolicitudGlobalController@finalizadas')->name('global.finalizadas');
    Route::post('global/finalizadas', 'SolicitudGlobalController@finalizadas')->name('global.finalizadas');

    /**
     * Dashboard
     */
    Route::get('dashboard', 'DashboardController@index')
        ->name('dashboard.index');

    /**
     * Tesorería
     */
    Route::get('solicitud_tesoreria/create', 'TransferenciaController@create')
        ->name('solicitud.tesoreria.transferencia.create');
    Route::post('solicitud_tesoreria/create', 'TransferenciaController@store')
        ->name('solicitud.tesoreria.transferencia.store');

    Route::get('get_cuentas_moneda', 'TransferenciaController@getCuentasBancariasPorMoneda')
        ->name('solicitud.tesoreria.transferencia.getCuentaMoneda');
    Route::get('get_cuentas_empresa', 'TransferenciaController@getCuentasBancariasPorEmpresa')
        ->name('solicitud.tesoreria.transferencia.getCuentaEmpresa');
    Route::get('get_factor', 'TransferenciaController@getFactor')
        ->name('solicitud.tesoreria.transferencia.getFactor');
    Route::get('reporte_transferencias', 'TransferenciaController@report')
        ->name('solicitud.tesoreria.transferencia.report');
    Route::post('reporte_transferencias', 'TransferenciaController@report')
        ->name('solicitud.tesoreria.transferencia.report');

    Route::get('reporte_transferencias_excel', 'TransferenciaController@reportExcel')
        ->name('solicitud.tesoreria.transferencia.report_excel');

    Route::get('reporte_transferencias_pdf', 'TransferenciaController@reportPdf')
        ->name('solicitud.tesoreria.transferencia.report_pdf');
});