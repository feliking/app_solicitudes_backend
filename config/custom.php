<?php

/*
 * This file is part of customize options for Laravel 5.3.
 *
 * (c) Felix Mamani <felixddxd@gmail.com>
 *
 */

return [

    'fingerprint_key' => env('FINGERPRINT_AUTH')

];
