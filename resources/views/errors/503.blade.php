<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistema de Solicitudes</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div>
            {{-- <img width="164px" height="60px" src="{{ asset('images/tepco_srl.png') }}"> --}}
        </div>
        <div style="color: darkorange" class="title m-b-md">
            Http Error 500 - Error de conexión con el servidor
        </div>

        {{--<div class="links">--}}
            {{--<p style="color: #209a33;font-weight: 700;">Por favor, haga <a style="color: #085436;text-decoration: underline;font-weight: bold;" href="http://tepcopedidos.pragmainvest.com.bo">click aquí</a> para ir a la página de <strong>TEPCO PEDIDOS</strong></p>--}}
        {{--</div>--}}

        <div style="color: orange; font-weight: 600" class="links">
            <p>No es posible conectarse con el servidor, favor intente más tarde... Disculpe las molestias.</p>
        </div>
    </div>
</div>
</body>
</html>