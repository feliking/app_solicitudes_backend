@extends('main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default no-print">
                    <div class="panel-heading">
                        <h2>Advertencia:</h2>
                    </div>
                    <div class="panel-body">
                        @if ($exception->getMessage() == 'edit') 
                        <h4>No está autorizado para editar esta solicitud.</h4>
                        @elseif($exception->getMessage() == 'unauthorized-apr')
                        <h4>No está autorizado para aprobar la solicitud. Favor comuníquese con el Departamento de TIC's.</h4>
                        @elseif($exception->getMessage() == 'banned')
                        <h4>No está autorizado para realizar este proceso. Su cuenta se encuentra bloqueada.</h4>
                        @elseif($exception->getMessage() == 'year')
                        <h4>No puede realizar esta acción. Debe seleccionar la gestión actual.</h4>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    
</script>
{{ Html::script('/js/solicitud/funciones.js') }}
@endsection