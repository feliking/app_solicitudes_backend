@extends('main')

@section('content')
<?php include_once(app_path().'/functions.php')?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>
                        Empresas
                        <div class="crear_empresa pull-right">
                            <a class="btn btn-success" href="{{ route('empresa.create') }}">Adicionar Empresa</a>
                        </div>
                    </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Razón Social</th>
                                <th>NIT</th>
                                <th>Descripción</th>
                                <th>Proyectos</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <?php $contador = 1;?>
                        <tbody>
                            @foreach($empresas as $empresa)
                            <tr>
                                <td>
                                    <?php
                                    echo $contador++;
                                    ?>
                                </td>
                                <td>{{$empresa->nombre}}</td>
                                <td>{{$empresa->razon}}</td>
                                <td>{{$empresa->nit}}</td>
                                <td style="width: 25%">{{cantPalabras($empresa->descripcion, 10)}}</td>
                                <td>
                                </td>
                                <td>
                                    {!! Form::open(array('route' => 'proyectos-form','method'=>'POST', 'class'=>'form-horizontal')) !!}
                                        {!! Form::hidden('id', $empresa->id, array('placeholder' => '','class' => 'form-control')) !!}
                                        {!! Form::submit('Crear Proyecto', ['class' => 'btn btn-success']) !!}
                                    {!! Form::close() !!}
                                    <a class="btn btn-primary" href="{{ route('empresa.edit', $empresa->id) }}">Editar</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
