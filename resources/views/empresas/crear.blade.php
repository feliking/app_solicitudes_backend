@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Registrar Empresa</h2></div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'empresa.store','method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
                        {{ csrf_field() }}
                        @include('empresas.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection