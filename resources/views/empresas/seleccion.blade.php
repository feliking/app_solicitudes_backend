@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        {{-- <div class="col-md-6 col-md-offset-3"> --}}
        <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-12">
            <div class="panel panel-default login-panel">
                <div class="panel-heading">Elegir Empresa
                    <div class="pull-right">
                        <a class="btn btn-danger" href="{{ url('/logout') }}" role="button" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
                         <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
                <div class="panel-body login-empresa">
                    @if(count($empresas) == 0)
                        <h2>No cuenta con empresa asignada, contactese con el administrador, Gracias!!!</h2>
                    @else
                        {!! Form::open(array('route' => 'solicitudes','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            @foreach($empresas as $empresa)
                            <div class="form-group select-empresa" style="margin-bottom: 0px;">
                                <input type="radio" name="empresa" id="empresa_{{ $loop->iteration }}" data-id="{{ $empresa->id }}" value="{{ $empresa->id }}"{{ ($loop->iteration == 1)?'checked':'' }}>
                                <label for="empresa_{{ $loop->iteration }}">{{ $loop->iteration }} {{ $empresa->nombre }}</label>
                            </div>
                            @endforeach
                            <div class="form-group">
                                <div class="col-md-12{{ $errors->has('gestion') ? ' has-error' : '' }}">
                                    <label for="gestion" class="control-label">GESTIÓN</label>
                                    <select class="form-control" name="gestion" id="gestion">
                                    @foreach($gestiones as $gestion)
                                        <option value="{{ $gestion->id }}">{{ $gestion->gestion }}</option>
                                    @endforeach
                                        <option value="{{ $ultimas_gestiones[1]->id.'|'.$ultimas_gestiones[0]->id }}">{{ $ultimas_gestiones[1]->gestion.' y '.$ultimas_gestiones[0]->gestion }}</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="btn-seleccion-empresa">
                                {!! Form::submit('Siguiente', ['class' => 'btn btn-primary', 'id' => 'btn-sel-empresa']) !!}
                            </div>                            
                        {!! Form::close() !!}
                        <div class="logo hidden-xs">
                            <div class="inner-logo" style="">
                                @foreach($empresas as $empresa)
                                    <img src="{{$empresa->logo}}" class="logo-empresa" id="{{ 'logo_'.$empresa->id }}" style="{{ ($loop->iteration == 1)?'display: block;':'display: none;' }} height: auto; width: 200px;">
                                @endforeach
                            </div>
                            <div class="clearfix">
                                <table style="width: 200px;">
                                    <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center" style="font-weight: bold;">RESUMEN DE SOLICITUDES</h2>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody id="datos-adicionales"></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
<script language="javascript" type="text/javascript">
    $("input:radio[name=empresa]").click(function(){
        var value = $(this).attr("value");
        $('.logo-empresa').each(function(){
            $(this).css('display', 'none');
        });
        var logo = '#logo_'+value;
        $(logo).css('display', 'block');
        datosResumen($(this).attr('data-id'));
    });

    var radioSel = document.querySelector('input[name="empresa"]:checked').value;
    datosResumen(radioSel);
    
    function datosResumen(id){
        var url = "/json/cantidad/estados/" + id;
        var total_items = 0;
        var fila = "";
        $.getJSON(url, function(data) {
            $("#datos-adicionales").empty();
            fila = "<tr style='color: green;'>";
            fila += "<td>En Curso</td>";
            fila += "<td style='text-align: right;'>"+data['curso']+"</td>";
            fila += "</tr>";
            fila += "<tr style='color: orange;'>";
            fila += "<td>Observadas</td>";
            fila += "<td style='text-align: right;'>"+data['observadas']+"</td>";
            fila += "</tr>";
            fila += "<tr style='color: red;'>";
            fila += "<td>Rechazadas</td>";
            fila += "<td style='text-align: right;'>"+data['rechazadas']+"</td>";
            fila += "</tr>";
            fila += "<tr style='color: DodgerBlue;'>";
            fila += "<td>Rendidas</td>";
            fila += "<td style='text-align: right;'>"+data['rendidas']+"</td>";
            fila += "</tr>";
            fila += "<tr style='color: blue;'>";
            fila += "<td>Finalizadas</td>";
            fila += "<td style='text-align: right;'>"+data['finalizadas']+"</td>";
            fila += "</tr>";
            if(data['rol_id'] > 2 && data['rol_id'] < 7){
                var titulo = "";
                var cadListos = "";
                var cadPor = "";
                var cantidadListos = 0;
                var cantidadPor = 0;
                var entero = parseInt(data['rol_id']);
                switch(entero){
                    case 3:
                        titulo = "Autorizador";
                        cadListos = "Autorizadas";
                        cadPor = "Por Autorizar";
                        cantidadListos = data['autorizadas'];
                        cantidadPor = data['porautorizar'];
                        break;
                    case 4:
                        titulo = "Revisor";
                        cadListos = "Revisadas";
                        cadPor = "Por Revisar";
                        cantidadListos = data['revisadas'];
                        cantidadPor = data['porrevisar'];
                        break;
                    case 5:
                        titulo = "Aprobador";
                        cadListos = "Aprobadas";
                        cadPor = "Por Aprobar";
                        cantidadListos = data['aprobadas'];
                        cantidadPor = data['poraprobar'];
                        break;
                    case 6:
                        titulo = "Tesoreria";
                        cadListos = "Reg. Tesoreria Realizados";
                        cadPor = "Reg. Tesoreria Faltantes";
                        cantidadListos = data['tesoreria'];
                        cantidadPor = data['portesoreria'];
                        break;
                }                
                fila += "<tr style='color: gray;'>";
                fila += "<td style='text-align: center;' colspan='2'>"+titulo+"</td>";
                fila += "</tr>";
                fila += "<tr style='color: green;'>";
                fila += "<td>"+cadPor+"</td>";
                fila += "<td style='text-align: right;'>"+cantidadPor+"</td>";
                fila += "</tr>";
                fila += "<tr style='color: DodgerBlue;'>";
                fila += "<td>"+cadListos+"</td>";
                fila += "<td style='text-align: right;'>"+cantidadListos+"</td>";
                fila += "</tr>";
            }
            $("#datos-adicionales").append(fila);
        });
    }
</script>
@endsection