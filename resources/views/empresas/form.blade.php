<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
    <label for="nombre" class="col-md-4 control-label">Nombre Empresa</label>

    <div class="col-md-6">
        <input id="nombre" style='text-transform:uppercase' type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" placeholder="Nombre Empresa" required autofocus>

        @if ($errors->has('nombre'))
            <span class="help-block">
                <strong>{{ $errors->first('nombre') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('razon') ? ' has-error' : '' }}">
    <label for="razon" class="col-md-4 control-label">Razón Social</label>

    <div class="col-md-6">
        <input id="razon" style='text-transform:uppercase' type="text" class="form-control" name="razon" value="{{ old('razon') }}" placeholder="Razón Social" required>

        @if ($errors->has('razon'))
            <span class="help-block">
                <strong>{{ $errors->first('razon') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('nit') ? ' has-error' : '' }}">
    <label for="nit" class="col-md-4 control-label">NIT</label>

    <div class="col-md-6">
        <input id="nit" type="number" class="form-control" name="nit" value="{{ old('nit') }}" placeholder="NÚMERO DE IDENTIFICACIÓN TRIBUTARIA" required>

        @if ($errors->has('nit'))
            <span class="help-block">
                <strong>{{ $errors->first('nit') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
    <label for="descripcion" class="col-md-4 control-label">Descripción</label>

    <div class="col-md-6">
        <textarea id="descripcion" style='text-transform:uppercase'  class="form-control" name="descripcion" placeholder="Descripción...">{{ old('descripcion') }}</textarea>

        @if ($errors->has('descripcion'))
            <span class="help-block">
                <strong>{{ $errors->first('descripcion') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('imagen') ? ' has-error' : '' }}">
    <label for="imagen" class="col-md-4 control-label">Logo Empresa</label>

    <div class="col-md-6">
        <a class="btn btn-warning" id="upload-imagen" onclick="elegirImagen()">
            <i class="fa fa-upload"></i> Subir Imagen
        </a>
        <p style="margin-top: 7px; margin-bottom: 0px;">* Formato de la imagen PNG o JPG</p>
        <input id="imagen" type="file" class="" name="imagen" style="display: none">
        @if ($errors->has('imagen'))
            <span class="help-block">
                <strong>{{ $errors->first('imagen') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Registrar Empresa
        </button>
    </div>
</div>

@section('footerScripts')
@parent
<script>
    function elegirImagen(){
        document.getElementById('imagen').click();
    }
</script>
@endsection