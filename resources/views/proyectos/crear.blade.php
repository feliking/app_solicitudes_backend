@extends('main')
@php
if(sizeof($errors)>0){
    print_r($errors);
}
@endphp
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Nuevo Proyecto</h2></div>
                <div class="panel-body">
                    @php
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    {!! Form::open(array('route' => 'proyectos.store','method'=>'POST', 'class'=>'form-horizontal', 'name' => 'formProyecto')) !!}
                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            <label for="nombre" class="col-md-4 control-label">Nombre del Proyecto</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nombre" placeholder="Nombre del Proyecto" style="text-transform: uppercase">
                                @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <label for="descripcion" class="col-md-4 control-label">Descripción del Proyecto</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="descripcion" placeholder="Descripción..." style="text-transform: uppercase">{{ old('descripcion') }}</textarea>
                                @if ($errors->has('descripcion'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('opcion_sub') ? ' has-error' : '' }}" @php
                            if(sizeof($proy_array_padres)==0){
                                echo 'style="display: none;"';
                            }
                        @endphp>
                            <label for="opcion_sub" class="col-md-4 control-label">¿Es Sub-Proyecto?</label>
                            <div class="col-md-6">
                                <input type="radio" name="opcion_sub" id="no" value="0" checked>
                                <label for="no"> No</label>&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="opcion_sub" id="si" value="1">
                                <label for="si"> Si</label>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('proyecto') ? ' has-error' : '' }} select_proyecto_padre hidden" id='sel_proyecto'>
                            <label for="proyecto" class="col-md-4 control-label">Elija Proyecto</label>
                            <div class="col-md-6">
                                <select class="form-control" id="id_padre" name="id_padre">
                                    @foreach($proy_array_padres as $proy_pa)
                                        <option value="{{ $proy_pa->id }}">
                                            @if($proy_pa->padre_id!=0)
                                                {{ '--' }}
                                            @endif
                                            {{ $proy_pa->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                {!! Form::submit('Crear Proyecto', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
<script language="javascript" type="text/javascript">
    var frm = document.getElementById('sel_proyecto');
    var rad = document.formProyecto.opcion_sub;
    for(var i = 0; i < rad.length; i++) {
        rad[i].onclick = function() {
            if(this.id == 'si'){
                frm.classList.remove('hidden');
            }else{
                frm.classList.add('hidden');
            }
        };
    }
</script>
@endsection