@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="crear_empresa">
                        <a class="btn btn-success pull-right" href="{{ route('proyectos.create') }}">Nuevo Proyecto</a>
                    </div>
                    <h2>Proyectos</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Opciones</th>
                                <th>#</th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($proyectos as $proyecto)
                            <tr>
                                <td></td>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $proyecto->nombre }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection