<?php
include_once(app_path().'/functions.php');
?>
@extends('pdfMain')

@section('headerScripts')
<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 12px;
        background: white;
        color: black;
    }
    img.logo{
        height: 100px;
    }
    h1 {
        font-size: 2.4em;
        padding: 0.5em;
    }
    table{
        border-collapse: collapse;
        margin: auto;
        width: 100%;
        font-size: 12px;
    }
    table tr td h1{
        text-align: center;
    }
    table tr td h2{
        text-align: center;
    }
    table tr td .number{
        font-size: 28px;
        text-align: right;
    }
    #table-items tr th{
        background: #e6e6e6;
        color: black;
        padding: 5px 5px;
    }
    #table-items tr td{
        padding: 3px 5px;
        font-size: 9px;
    }
    .panel-default{
        border: 2px solid black;
        margin-bottom: 10px;
    }
    .panel-heading{
        font-size: 1.5em;
        font-weight: bold;
        padding-top: 3px;
        padding-bottom: 3px;
        background: #fff;
        border-bottom: 2px solid black!important;
    }
    .panel-solicitante{
        width: 55%;
    }
    .panel-solicitante .panel-heading{
        font-size: 0.9em;
    }
    .panel-solicitante .panel-body{
        margin: 0px;
        padding: 0px;
    }
    .panel-solicitante .panel-body .table tr td{
        padding-bottom: 3px;
        padding-top: 3px;
    }
    #table-contacto{
        margin: 5px 0px;
        font-size: 0.9em;
    }
    #table-contacto td{
        border: none!important;
    }

    #table-contacto tr{
        background: none;
    }

    #table-firmas{
        margin-bottom: 0px;
    }
    #table-firmas th{
        background: #fff;
        height: 70px;
    }
    #table-firmas tr td{
        width: 20%;
    }
    #table-firmas tr td p{
        font-size: 10px;
        text-align: center;
    }
    .p-total{
        margin-bottom: 0px;
        padding-bottom: 0px;
    }

    .table-print tr td{
        height: 15px!important;
        padding-bottom: 0px!important;
        padding-top: 0px!important;
    }

</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <table>
            <tr>
                <td style="width: 160px">
                    <img src="{{ public_path($solicitud->proyecto->empresa->logo) }}" alt="{{ session('empresa_nombre') }}" class="logo"/>
                </td>
                <td>
                    <h1>{{ $solicitud->proyecto->empresa->nombre }}</h1>
                    <h2>RENDICIÓN SOLICITUD DE FONDOS</h2>
                </td>
                <td style="width: 160px">
                    <div class="number">
                        <br/><br/>
                        <strong>Nro. {{ $solicitud->numero }}</strong>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="panel-body">
        <div class="panel panel-default ">
            <div class="panel-heading">INFORMACIÓN SOLICITUD</div>
            <div class="panel-body">
                <table class="table table-print" id="reporte-solicitud">
                    <tr>
                        <td style="width: 130px;"><label for="pago">PROYECTO: </label></td>
                        <td colspan="7">
                            @if($solicitud->proyecto->nivel==3)
                                {{ $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre }}
                            @endif
                            @if($solicitud->proyecto->nivel==2)
                                {{ $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre }}
                            @endif
                            @if($solicitud->proyecto->nivel==1)
                                {{ $solicitud->proyecto->nombre }}
                            @endif
                        </td>
                        <td style="width: 120px"><label for="turno">FECHA LIMITE: </label></td>
                        <td style="width: 120px">{{ $solicitud->fecha_limite }}</td>
                    </tr>
                    <tr>
                        <td><label for="ruta">SOLICITANTE: </label></td>
                        <td colspan="9">{{ $solicitud->usuario->nombre }}</td>
                    </tr>
                    <tr>
                        <td><label for="grupo">DESEMBOLSO A: </label></td>
                        <td colspan="9">{{ $solicitud->desembolso }}</td>
                    </tr>
                    <tr>
                        <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                        <td>{{ $solicitud->tipo_cambio->cambio }}</td>
                        <td style="text-align: right;"><label for="grupo">MONEDA: </label></td>
                        <td colspan="3">
                            {{ $solicitud->moneda->nombre }}
                        </td>
                        <td style="text-align: right;"><label for="grupo">MODALIDAD: </label></td>
                        <td colspan="3">
                            {{ $solicitud->modalidad->nombre }}
                        </td>
                    </tr>
                    <tr>
                        <td><label for="grupo">REFERENCIA: </label></td>
                        <td colspan="9">{{ $solicitud->referencia }}</td>
                    </tr>
                    <tr>
                        <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                        <td colspan="9">{{ $solicitud->observacion }}</td>
                    </tr>
                    @if ($solicitud->observacionRendicion != null)
                    <tr>
                        <td><label for="grupo">OBSERVACIÓN: </label></td>
                        <td colspan="9">{{ $solicitud->observacionRendicion->observacion }}</td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading">ITEMS RENDIDOS</div>
            <div class="panel-body panel-items">
                <table class="table table-bordered" id="table-items">
                    <tr>
                        <th class="text-center" style="width:20px;">#</th>
                        <th class="text-center" style="width:70px;">FACTURA</th>
                        <th class="text-center" style="width:70px;">FECHA</th>
                        <th class="text-center" style="width:70px;">UNIDAD</th>
                        <th class="text-center">DETALLE</th>
                        <th class="text-center" style="width:50px;">CANT.</th>
                        <th class="text-center" style="width:90px;">P. UNITARIO</th>
                        <th class="text-center" style="width:90px;">SUB TOTAL</th>
                    </tr>
                    @php
                        $total = 0;
                        $total_items = 0;
                        $num_fila = 0;
                        foreach($solicitud->items as $item_sol){
                            $total_items += $item_sol->costo * $item_sol->cantidad;
                        }
                    @endphp
                    @foreach($solicitud->itemsRendidos as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $item->num_factura }}</td>
                            <td class="text-center">{{ $item->fecha_factura }}</td>
                            <td class="text-center">{{ $item->unidad->codigo }}</td>
                            <td>{{ $item->detalle }}</td>
                            <td class="text-center">{{ $item->cantidad }}</td>
                            <td class="text-right">{{ number_format((float)$item->costo, 2, '.', ',') }}</td>
                            <td class="text-right">{{ number_format((float)($item->cantidad * $item->costo), 2, '.', ',') }}</td>
                        </tr>
                        @php
                            $total += $item->cantidad * $item->costo;
                            $num_fila = $loop->iteration;
                        @endphp
                    @endforeach
                    @if(count($solicitud->devoluciones) > 0)
                        <tr>
                            <td colspan="8" class="text-center"><b>DEVOLUCIONES</b></td>
                        </tr>
                        @foreach($solicitud->devoluciones as $devolucion)
                            <tr>
                                <td>{{ ($num_fila + $loop->iteration) }}</td>
                                <td colspan="6">{{ $devolucion->detalle }}</td>
                                <td class="text-right">{{ number_format((float)($devolucion->monto), 2, '.', ',') }}</td>
                            </tr>
                            @php
                                $total += $devolucion->monto;
                            @endphp
                        @endforeach
                    @endif
                    <tr>
                        <td colspan="5" style="border:0!important;"></td>
                        <td colspan="2" class="text-center"><strong>TOTAL RENDIDO</strong></td>
                        <td class="text-right"><strong>{{ number_format((float)$total, 2, '.', ',') }}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="border:0!important;"></td>
                        <td colspan="2" class="text-center"><strong>TOTAL SOLICITUD</strong></td>
                        <td class="text-right"><strong>{{ number_format((float)$total_items, 2, '.', ',') }}</strong></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="border:0!important;"></td>
                        <td colspan="2" class="text-center"><strong>SALDO</strong></td>
                        <td class="text-right"><strong>{{ number_format((float)($total - $total_items), 2, '.', ',') }}</strong></td>
                    </tr>
                </table>
                @php
                //***REVISAR CENTAVOS
                $p_decimal = '00';
                $total = number_format((float)$total, 2, '.', ',');
                if(strpos($solicitud->total, '.')){
                    $total = explode('.', $total);
                    $p_entera = $total[0];
                    $p_decimal = $total[1];
                }else{
                    $p_entera = $total;
                }
                @endphp
                <p class="p-total"><strong>SON:</strong> {{ numeroLiteral (str_replace(',', '', $p_entera)) }} CON {{ $p_decimal }}/100 {{ $solicitud->moneda->nombre }}</p>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading">FIRMAS</div>
            <div class="panel-body">
                <table class="table" id="table-firmas">
                    <tr>
                        <th style="width: 50%;"></th>
                        <th style="width: 50%;"></th>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                {{ ucwords(strtolower($solicitud->usuario->nombre)) }}
                                <br>
                                {{ $solicitud->updated_at }}
                                <br>
                                Solicitante
                            </p>
                        </td>
                        <td>
                            <p>
                                {{ ucwords(strtolower($solicitud->autorizador)) }}
                                <br>
                                {{ $solicitud->autorizadorHora }}
                                <br>
                                Autorizador
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="panel panel-default panel-solicitante">
            <div class="panel-heading">CONTACTO SOLICITANTE</div>
            <div class="panel-body">
                <table class="table" id="table-contacto">
                    <tr>
                        <td>
                            <strong>Nombre: </strong>
                        </td>
                        <td>
                            {{ ucwords(strtolower($solicitud->usuario->nombre)) }}
                        </td>
                        <td>
                            <strong>Fecha Solicitud: </strong>
                        </td>
                        <td>
                            {{ $solicitud->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Telefono: </strong>
                        </td>
                        <td>

                        </td>
                        <td>
                            <strong>Hora  Solicitud: </strong>
                        </td>
                        <td>
                            {{ $solicitud->time }}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <strong>Fecha Rendición: </strong>
                        </td>
                        <td>
                            @if(count($solicitud->itemsRendidos) > 0)
                                {{ date_format(date_create($solicitud->itemsRendidos()->orderBy('item_rendiciones.created_at', 'DESC')->first()->created_at), 'd-m-Y') }}
                            @else
                                {{ date_format(date_create($solicitud->devoluciones()->orderBy('devoluciones.created_at', 'DESC')->first()->created_at), 'd-m-Y') }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <strong>Hora Rendición: </strong>
                        </td>
                        <td>
                            @if(count($solicitud->itemsRendidos) > 0)
                                {{ date_format(date_create($solicitud->itemsRendidos()->orderBy('item_rendiciones.created_at', 'DESC')->first()->created_at), 'H:i:s') }}
                            @else
                                {{ date_format(date_create($solicitud->devoluciones()->orderBy('devoluciones.created_at', 'DESC')->first()->created_at), 'H:i:s') }}
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection