<?php
include_once(app_path().'/functions.php');
?>
@extends('pdfMain')

@section('headerScripts')
<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 12px;
        background: white;
        color: black;
    }
    img.logo{
        height: 100px;
    }
    h1 {
        font-size: 2.4em;
        padding: 0.5em;
    }
    table{
        border-collapse: collapse;
        margin: auto;
        width: 100%;
        font-size: 12px;
    }
    table tr td h1{
        text-align: center;
        font-weight: bolder;
    }
    table tr td h2{
        text-align: center;
        font-weight: bolder;
    }
    table tr td .number{
        font-size: 28px;
        text-align: right;
    }
    #table-items tr th{
        background: #e6e6e6;
        color: black;
        padding: 5px 5px;
    }
    #table-items tr td{
        padding: 3px 5px;
        font-size: 9px;
    }
    .panel-default{
        border: 2px solid black;
        margin-bottom: 10px;
    }
    .panel-heading{
        font-size: 1.5em;
        font-weight: bold;
        padding-top: 3px;
        padding-bottom: 3px;
        background: #fff;
        border-bottom: 2px solid black!important;
    }
    .panel-solicitante{
        width: 55%;
    }
    .panel-solicitante .panel-heading{
        font-size: 0.9em;
    }
    .panel-solicitante .panel-body{
        margin: 0px;
        padding: 0px;
    }
    .panel-solicitante .panel-body .table tr td{
        padding-bottom: 3px;
        padding-top: 3px;
    }
    #table-contacto{
        margin: 5px 0px;
        font-size: 0.9em;
    }
    #table-contacto td{
        border: none!important;
    }

    #table-contacto tr{
        background: none;
    }
    
    #table-firmas{
        margin-bottom: 0px;
    }
    #table-firmas th{
        background: #fff;
        height: 70px;
    }
    #table-firmas tr td{
        width: 20%;
    }
    #table-firmas tr td p{
        font-size: 10px;
        text-align: center;
    }
    .p-total{
        margin-bottom: 0px;
        padding-bottom: 0px;
    }

    .table-print tr td{
        height: 15px!important;
        padding-bottom: 0px!important;
        padding-top: 0px!important;
    }

</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <table>
            <tr>
                <td style="width: 160px">
                    <img src="{{ public_path($solicitud->proyecto->empresa->logo) }}" alt="{{ session('empresa_nombre') }}" class="logo"/>
                </td>
                <td>
                    <h1>{{ $solicitud->proyecto->empresa->nombre }}</h1>
                    <h2>SOLICITUD DE TRANSFERENCIA</h2>
                </td>
                <td style="width: 160px">
                    <div class="number">
                        <br/><br/>
                        <strong>Nro. {{ $solicitud->numero }}</strong>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="panel-body">
        <div class="panel panel-default ">
            <div class="panel-heading" style="text-align: center;">INFORMACIÓN SOLICITUD DE TRANSFERENCIA</div>
            <div class="panel-body">
                <table class="table table-print" id="reporte-solicitud">
                    <tr>
                        <td style="width: 130px;"><label for="pago">PROYECTO: </label></td>
                        <td colspan="9">
                            @if($solicitud->proyecto->nivel==3)
                                {{ $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre }}
                            @endif
                            @if($solicitud->proyecto->nivel==2)
                                {{ $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre }}
                            @endif
                            @if($solicitud->proyecto->nivel==1)
                                {{ $solicitud->proyecto->nombre }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><label for="ruta">SOLICITANTE: </label></td>
                        <td colspan="4">{{ $solicitud->usuario->nombre }}</td>
                        <td><label for="grupo">DESEMBOLSO A: </label></td>
                        <td colspan="4">{{ $solicitud->desembolso }}</td>
                    </tr>
                    @if($solicitud->moneda->nombre == 'BOLIVIANOS')
                        <tr>
                            <td style="width: 120px"><label for="turno">FECHA LIMITE: </label></td>
                            <td style="width: 120px" colspan="4">{{ $solicitud->fecha_limite }}</td>
                            <td style="text-align: left;"><label for="grupo">MONEDA: </label></td>
                            <td colspan="4">
                                {{ $solicitud->moneda->nombre }}
                            </td>
                        </tr>
                    @else
                        <tr>
                            <td style="width: 120px"><label for="turno">FECHA LIMITE: </label></td>
                            <td style="width: 120px" colspan="3">{{ $solicitud->fecha_limite }}</td>
                            <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                            <td colspan="2">{{ $solicitud->tipo_cambio->cambio }}</td>
                            <td style="text-align: right;"><label for="grupo">MONEDA: </label></td>
                            <td colspan="2">
                                {{ $solicitud->moneda->nombre }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td><label for="grupo">MODALIDAD: </label></td>
                        <td colspan="9">
                            {{ $solicitud->modNombre }}
                        </td>
                    </tr>
                    <tr>
                        <td><label for="grupo">REFERENCIA: </label></td>
                        <td colspan="9">{{ $solicitud->referencia }}</td>
                    </tr>
                    <tr>
                        <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                        <td colspan="9">{{ $solicitud->observacion }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading" style="text-align: center;">DETALLE DE TRANSFERENCIA</div>
            <div class="panel-body panel-items">
                <table class="table table-bordered" id="table-items">
                    <tr>
                        <th class="text-center">CUENTA BANCARIA ORIGEN</th>
                        <th class="text-center">TIPO DE TRANSACCIÓN</th>
                        <th class="text-center">MONTO DE TRANSFERENCIA</th>
                    </tr>
                    <tr>
                        <td class="text-center">{{ $solicitud->transferencia->cuentaOrigen->banco->nombre }} (CTA. {{ $solicitud->transferencia->cuentaOrigen->numero }})</td>
                        <td class="text-center">[{{ $solicitud->transferencia->tipoTransferencia->codigo }}] {{ $solicitud->transferencia->tipoTransferencia->descripcion }}</td>
                        <td class="text-right">{{ number_format($solicitud->transferencia->monto, 2, '.', ',') }}</td>
                    </tr>
                    <tr>
                        <th class="text-center">EMPRESA DESTINO</th>
                        <th class="text-center">CUENTA BANCARIA DESTINO</th>
                        <th class="text-center">FACTOR DE CONVERSIÓN</th>
                    </tr>
                    <tr>
                        <td class="text-center">{{ $solicitud->transferencia->cuentaDestino->empresa->nombre }}</td>
                        <td class="text-center">{{ $solicitud->transferencia->cuentaDestino->banco->nombre }} (CTA. {{ $solicitud->transferencia->cuentaDestino->numero }})</td>
                        <td class="text-right">{{ number_format($solicitud->transferencia->factor, 2, '.', ',') }}</td>
                    </tr>
                </table>
                
                @php
                //***REVISAR CENTAVOS
                $p_decimal = '00';
                $total = number_format((float)$solicitud->total, 2, '.', ',');
                if(strpos($solicitud->total, '.')){
                    $total = explode('.', $total);
                    $p_entera = $total[0];
                    $p_decimal = $total[1];
                }else{
                    $p_entera = $total;
                }
                @endphp

                <p class="p-total"><strong>SON:</strong> {{ numeroLiteral (str_replace(',', '', $p_entera)) }} CON {{ $p_decimal }}/100 {{ $solicitud->moneda->nombre }}</p>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading" style="text-align: center;">FIRMAS</div>
            <div class="panel-body">
                <table class="table" id="table-firmas">
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                {{ ucwords(strtolower($solicitud->usuario->nombre)) }}
                                <br>
                                {{ $solicitud->updated_at }}
                                <br>
                                Solicitante
                            </p>
                        </td>
                        <td>
                            <p>
                                {{ ucwords(strtolower($solicitud->autorizador)) }}
                                <br>
                                {{ $solicitud->autorizadorHora }}
                                <br>
                                Autorizador
                            </p>
                        </td>
                        <td>
                            <p>
                                {{ ucwords(strtolower($solicitud->controller)) }}
                                <br>
                                {{ $solicitud->controllerHora }}
                                <br>
                                Revisor
                            </p>
                        </td>
                        <td>
                            <p>
                                {{ ucwords(strtolower($solicitud->aprobador)) }}
                                @foreach($solicitud->aprobadores as $aprobador)
                                    {{ ucwords(strtolower($aprobador)) }}<br>
                                @endforeach
                                @if(count($solicitud->aprobadores) == 1)
                                    Aprobador<br>
                                @else
                                    Aprobadores<br>
                                @endif
                                @if($solicitud->apr_estado != "")
                                    {{ $solicitud->apr_estado }}
                                @endif
                            </p>
                        </td>
                        <td>
                            <p>
                                {{ ucwords(strtolower($solicitud->tesoreria)) }}
                                <br>
                                {{ $solicitud->tesoreriaHora }}
                                <br>
                                Tesoreria
                            </p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="panel panel-default panel-solicitante">
            <div class="panel-heading">CONTACTO SOLICITANTE</div>
            <div class="panel-body">
                <table class="table" id="table-contacto">
                    <tr>
                        <td>
                            <strong>Nombre: </strong>
                        </td>
                        <td>
                            {{ ucwords(strtolower($solicitud->usuario->nombre)) }}
                        </td>
                        <td>
                            <strong>Fecha Solicitud: </strong>
                        </td>
                        <td>
                            {{ $solicitud->created_at }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Telefono: </strong>
                        </td>
                        <td>
                            
                        </td>
                        <td>
                            <strong>Hora  Solicitud: </strong>
                        </td>
                        <td>
                            {{ $solicitud->time }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection