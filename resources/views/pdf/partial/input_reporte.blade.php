@php
	$colorBoton = "";
	if($tipo_solicitud == "APROBAR SOLICITUDES"){
		$colorBoton = "btn-default";
	}else{
		$colorBoton = "btn-success";
	}
@endphp

<div class="btn {{ $colorBoton }} pull-right" id="btn-imprimir"><span class="fa fa-file-pdf-o"></span> PDF</div>
<form id="frmFiltrosPDF">
    <input type="hidden" id="rep_empresa" name="rep_empresa" value="{{ isset($variables['empresa_id']) ? $variables['empresa_id'] : 'TODAS' }}">
    <input type="hidden" id="rep_proyecto" name="rep_proyecto" value="{{ isset($variables['proyecto_id']) ? $variables['proyecto_id'] : 'TODAS' }}">
    <input type="hidden" id="rep_gestion" name="rep_gestion" value="{{ isset($variables['gestion_id']) ? $variables['gestion_id'] : 'TODAS' }}">
    <input type="hidden" id="rep_selectMonto" name="rep_selectMonto" value="{{ isset($variables['selectMonto']) ? $variables['selectMonto'] : 'NINGUNO' }}">
    <input type="hidden" id="rep_monto" name="rep_monto" value="{{ isset($variables['monto']) ? $variables['monto'] : 'NINGUNO' }}">
    <input type="hidden" id="rep_desde" name="rep_desde" value="{{ isset($variables['fecha_ini']) ? $variables['fecha_ini'] : '0' }}">
    <input type="hidden" id="rep_hasta" name="rep_hasta" value="{{ isset($variables['fecha_fin']) ? $variables['fecha_fin'] : '0' }}">
    <input type="hidden" id="rep_columna" name="rep_columna" value="{{ isset($variables['campo']) ? $variables['campo'] : 'NINGUNO' }}">
    <input type="hidden" id="rep_columna_buscar" name="rep_columna_buscar" value="{{ isset($variables['texto']) ? $variables['texto'] : 'NINGUNO' }}">
    <input type="hidden" id="rep_tipo_solicitud" name="rep_tipo_solicitud" value="{{ isset($tipo_solicitud) ? $tipo_solicitud : '' }}">
</form>