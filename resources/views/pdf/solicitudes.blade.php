<?php
include_once(app_path().'/functions.php');
?>
@extends('pdfMain')

@section('headerScripts')
<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 12px;
        background: white;
        color: black;
    }
    img.logo{
        height: 100px;
    }
    h1 {
        font-size: 2.4em;
        padding: 0.5em;
    }
    table{
        border-collapse: collapse;
        margin: auto;
        width: 100%;
        font-size: 10px;

        page-break-inside: avoid;
    }
    table tr{
        page-break-inside: avoid;
    }
    table tr td h1{
        text-align: center;
    }
    table tr td h2{
        text-align: center;
    }
    table tr td .number{
        font-size: 28px;
        text-align: right;
    }
    #table-items tr th{
        background: #e6e6e6;
        color: black;
        padding: 5px 5px;
    }
    #table-items tr td{
        padding: 3px 5px;
    }
    .panel-default{
        border: 2px solid black;
        margin-bottom: 10px;
    }
    .panel-heading{
        font-size: 1.5em;
        font-weight: bold;
        padding-top: 3px;
        padding-bottom: 3px;
        background: #fff;
        border-bottom: 2px solid black!important;
    }
    .panel-solicitante{
        width: 45%;
    }
    .panel-solicitante .panel-heading{
        font-size: 0.9em;
    }
    .panel-solicitante .panel-body{
        margin: 0px;
        padding: 0px;
    }
    .panel-solicitante .panel-body .table tr td{
        padding-bottom: 3px;
        padding-top: 3px;
    }
    #table-contacto{
        margin: 5px 0px;
        font-size: 0.9em;
    }
    #table-contacto td{
        border: none!important;
    }

    #table-contacto tr{
        background: none;
    }

    #table-firmas{
        margin-bottom: 0px;
    }
    #table-firmas th{
        background: #fff;
        height: 70px;
    }
    #table-firmas tr td{
        width: 20%;
    }
    #table-firmas tr td p{
        font-size: 10px;
        text-align: center;
    }
    .p-total{
        margin-bottom: 0px;
        padding-bottom: 0px;
    }

    .table-print tr td{
        height: 15px!important;
        padding-bottom: 0px!important;
        padding-top: 0px!important;
    }

</style>
@endsection

@section('content')
<div class="panel panel-default">
    @if (isset($solicitudes))
        <div class="panel-heading">
            <table>
                <tr>
                    <td style="width: 160px"></td>
                    <td>
                        <h2>SOLICITUDES</h2>
                    </td>
                    <td style="width: 160px">
                        <div class="number"></div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="panel-body">
            <div class="panel panel-default ">
                <div class="panel-heading">{{ $tipo_solicitud }}</div>
                <div class="panel-body">
                    <table class="table table-condensed">
                        <tbody>
                            <tr>
                                <td><strong>EMPRESA</strong></td>
                                <td>{{ $empresa }}</td>
                                <td><strong>PROYECTO</strong></td>
                                <td>{{ $proyecto }}</td>
                                <td><strong>GESTIÓN</strong></td>
                                <td>{{ $gestion }}</td>
                                <td><strong>MONTO</strong> </td>
                                <td>{{ $monto_filtro }}</td>
                            </tr>
                            <tr>
                                @if ($desde == 0)
                                    <td colspan="4"><strong>SIN RANGO DE FECHA</strong></td>
                                @else
                                    <td><strong>DESDE</strong></td>
                                    <td>{{ $desde }}</td>
                                    <td><strong>HASTA</strong></td>
                                    <td>{{ $hasta }}</td>
                                @endif
                                <td><strong>COLUMNA</strong> </td>
                                <td>{{ $columna }}</td>
                                <td colspan="2">{{ $texto }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th >NRO. SOL</th>
                    <th style="width: 6%;">FECHA</th>
                    <th >ESTADO</th>
                    <th >MODALIDAD</th>
                    <th >MONTO</th>
                    <th >MONEDA</th>
                    <th >EMPRESA</th>
                    <th >PROYECTO</th>
                    <th >SOLICITANTE</th>
                    <th >REFERENCIA</th>
                    <th >JUSTIFICACIÓN</th>
                    <th >TIPO DE CAMBIO</th>
                    <th >DESEMBOLSO</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($solicitudes as $solicitud)
                <tr>
                    <td>{{ $solicitud->numero }}</td>
                    <td>{{ $solicitud->created_at }}</td>
                    <td>{{ $solicitud->estadoCodigo }}</td>
                    <td>{{ $solicitud->modalidad->nombre }}</td>
                    <td>{{ number_format($solicitud->total, '2', '.', ',') }}</td>
                    <td>{{ $solicitud->moneda->sigla }}</td>
                    <td>{{ $solicitud->proyecto->empresa->nombre }}</td>
                    <td>{{ $solicitud->nom_completo_proyecto }}</td>
                    <td>{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
                    <td>{{ $solicitud->referencia }}</td>
                    <td>{{ $solicitud->observacion }}</td>
                    <td>{{ $solicitud->tipo_cambio->cambio }}</td>
                    <td>{{ $solicitud->desembolso }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="panel-heading">
            <table>
                <tr>
                    <td style="width: 160px"></td>
                    <td>
                        <h2>NINGUNA SOLICITUD PARA MOSTRAR, INTENTE CON OTROS FILTROS</h2>
                    </td>
                    <td style="width: 160px">
                        <div class="number"></div>
                    </td>
                </tr>
            </table>
        </div>
    @endif
</div>
@endsection