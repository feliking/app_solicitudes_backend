<?php
include_once(app_path().'/functions.php');
?>
@extends('pdfMain')

@section('headerScripts')
<style type="text/css">
    body {
        font-family: sans-serif;
        font-size: 12px;
        background: white;
        color: black;
    }
    img.logo{
        height: 100px;
    }
    h1 {
        font-size: 2.4em;
        padding: 0.5em;
    }
    table{
        border-collapse: collapse;
        margin: auto;
        width: 100%;
        font-size: 12px;
    }
    table tr td h1{
        text-align: center;
    }
    table tr td h2{
        text-align: center;
    }
    table tr td .number{
        font-size: 28px;
        text-align: right;
    }
    #table-items tr th{
        background: #e6e6e6;
        color: black;
        padding: 5px 5px;
    }
    #table-items tr td{
        padding: 3px 5px;
    }
    .panel-default{
        border: 2px solid black;
        margin-bottom: 10px;
    }
    .panel-heading{
        font-size: 1.5em;
        font-weight: bold;
        padding-top: 3px;
        padding-bottom: 3px;
        background: #fff;
        border-bottom: 2px solid black!important;
    }
    .panel-solicitante{
        width: 45%;
    }
    .panel-solicitante .panel-heading{
        font-size: 0.9em;
    }
    .panel-solicitante .panel-body{
        margin: 0px;
        padding: 0px;
    }
    .panel-solicitante .panel-body .table tr td{
        padding-bottom: 3px;
        padding-top: 3px;
    }
    #table-contacto{
        margin: 5px 0px;
        font-size: 0.9em;
    }
    #table-contacto td{
        border: none!important;
    }

    #table-contacto tr{
        background: none;
    }

    #table-firmas{
        margin-bottom: 0px;
    }
    #table-firmas th{
        background: #fff;
        height: 70px;
    }
    #table-firmas tr td{
        width: 20%;
    }
    #table-firmas tr td p{
        font-size: 10px;
        text-align: center;
    }
    .p-total{
        margin-bottom: 0px;
        padding-bottom: 0px;
    }

    .table-print tr td{
        height: 15px!important;
        padding-bottom: 0px!important;
        padding-top: 0px!important;
    }

</style>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <table>
            <tr>
                <td style="width: 160px"></td>
                <td>
                    <h2>FORMULARIO DE ACTIVO FIJO <br>{{ $solicitud->proyecto->empresa->nombre }}</h2>
                </td>
                <td style="width: 160px">
                    <div class="number"></div>
                </td>
            </tr>
        </table>
    </div>
    <div class="panel-body" style="padding: 14px;">
        <div class="panel panel-default ">
            <div class="panel-heading">INFORMACIÓN SOLICITUD #{{ $solicitud->numero }}</div>
            <div class="panel-body">
                <div class="panel-body" style="padding: 7px;">
                    <table class="table table-bordered" id="reporte-solicitud">
                        @php
                        if($solicitud->proyecto->padre_id == null){
                            $proyecto = $solicitud->proyecto->nombre;
                        }elseif($solicitud->proyecto->padre->padre_id == null){
                            $proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                        }else{
                            $proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                        }
                        @endphp
                        <tr>
                            <td><strong>PROYECTO: </strong></td>
                            <td colspan="5" id='modal-proyecto'>{{ $proyecto }}</td>
                            <td><strong>FECHA LÍMITE: </strong></td>
                            <td id="modal-fecha-lim">{{ $solicitud->fecha_limite }}</td>
                        </tr>
                        <tr>
                            <td><strong>SOLICITANTE: </strong></td>
                            <td colspan="9" id="modal-solicitante">{{ $solicitud->usuario->nombre }}</td>
                        </tr>
                        @if($solicitud->moneda->nombre == 'BOLIVIANOS')
                            <tr id="tr_sin_tipo">
                                <td><strong>MODALIDAD: </strong></td>
                                <td colspan="5" id='modal-modalidad-sin'>{{ $solicitud->modalidad->nombre }}</td>
                                <td><strong>MONEDA: </strong></td>
                                <td colspan="5" id='modal-moneda-sin'>{{ $solicitud->moneda->nombre }}</td>
                            </tr>
                        @else
                            <tr id="tr_con_tipo">
                                <td><strong>TIPO DE CAMBIO: </strong></td>
                                <td id='modal-cambio'>{{ $solicitud->tipo_id }}</td>
                                <td><strong>MODALIDAD: </strong></td>
                                <td colspan="3" id='modal-modalidad'>{{ $solicitud->modalidad->nombre }}</td>
                                <td><strong>MONEDA: </strong></td>
                                <td colspan="3" id='modal-moneda'>{{ $solicitud->moneda->nombre }}</td>
                            </tr>
                        @endif
                        <tr>
                            <td style="width: 100px"><strong>REFERENCIA: </strong></td>
                            <td id='modal-referencia'>{{ $solicitud->referencia }}</td>
                        </tr>
                        <tr>
                            <td><strong>JUSTIFICACIÓN: </strong></td>
                            <td id='modal-observacion'>{{ $solicitud->observacion }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default ">
            <div class="panel-heading">INFORMACIÓN ACTIVO FIJO</div>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                        @php
                            $item_id = 0;
                            $item_iteration = 0;
                            $item_sub_iteration = 1;
                        @endphp
                        @foreach($item as $item_af)
                            {{-- {{ dd($item_af->item_rendido) }} --}}
                            @if($item_id != $item_af->item_rendido->id)
                            @php
                                $item_iteration += 1;
                            @endphp
                            <tr>
                                <th style="border: 1px solid #000 !important;">Nº</th>
                                <th style="border: 1px solid #000 !important;">UNIDAD</th>
                                <th style="border: 1px solid #000 !important;">TIPO COMPRA</th>
                                <th style="border: 1px solid #000 !important;">DETALLE</th>
                                <th style="border: 1px solid #000 !important;">CANTIDAD</th>
                                <th style="border: 1px solid #000 !important;">P. UNITARIO</th>
                                <th style="border: 1px solid #000 !important;">FECHA FACTURA</th>
                                <th style="border: 1px solid #000 !important;">NRO. FACTURA</th>
                                <th style="border: 1px solid #000 !important;">SUB TOTAL</th>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #000 !important;">{{ $item_iteration }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->item_rendido->unidad->descripcion }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->item_rendido->tipo_compra->descripcion }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->item_rendido->detalle }}</td>
                                <td style="border: 1px solid #000 !important;">{{ number_format($item_af->item_rendido->cantidad, '2', '.', '') }}</td>
                                <td style="border: 1px solid #000 !important;">{{ number_format($item_af->item_rendido->costo, '2', '.', '') }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->item_rendido->fecha_factura }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->item_rendido->num_factura }}</td>
                                <td style="border: 1px solid #000 !important;">{{ number_format($item_af->item_rendido->costo * $item_af->item_rendido->cantidad, '2', '.', '') }}</td>
                            </tr>
                            <tr>
                                <th style="text-align: center; border: 1px solid #000 !important;" colspan="9">FORMULARIO DE ACTIVO FIJO</th>
                            </tr>
                            <tr>
                                <th style="border: 1px solid #000 !important;"><label>Nº</label></th>
                                <th style="border: 1px solid #000 !important;" colspan="2"><label>DESCRIPCIÓN ESPECÍFICA</label></th>
                                <th style="border: 1px solid #000 !important;"><label>Nº SERIE / MODELO / CÓDIGO / PLACA / CHASIS</label></th>
                                <th style="border: 1px solid #000 !important;"><label>ASIGNACIÓN</label></th>
                                <th style="border: 1px solid #000 !important;"><label>ACTIVIDAD ESPECIFICA</label></th>
                                <th style="border: 1px solid #000 !important;"><label>NUEVO</label></th>
                                <th style="border: 1px solid #000 !important;"><label>REEMPLAZO</label></th>
                                <th style="border: 1px solid #000 !important;"><label>GARANTÍA</label></th>
                            </tr>
                            @php
                                $item_id = $item_af->item_rendido->id;
                                $item_sub_iteration = 1;
                            @endphp
                            @endif
                            <tr>
                                <td style="border: 1px solid #000 !important;">{{ $item_iteration.'.'.$item_sub_iteration }}</td>
                                <td style="border: 1px solid #000 !important;" colspan="2">{{ $item_af->descripcion_especifica }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->numero_serie }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->asignacion }}</td>
                                <td style="border: 1px solid #000 !important;">{{ $item_af->actividad_especifica }}</td>
                                <td style="border: 1px solid #000 !important;">{{ ($item_af->nuevo == true)?'SI':'NO' }}</td>
                                <td style="border: 1px solid #000 !important;">{{ ($item_af->reemplazo == true)?'SI':'NO' }}</td>
                                <td style="border: 1px solid #000 !important;">{{ ($item_af->garantia == true)?'SI':'NO' }}</td>
                            </tr>
                            @php
                                // $item_iteration += 1;
                                $item_sub_iteration += 1;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection