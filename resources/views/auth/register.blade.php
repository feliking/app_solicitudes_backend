@extends('layouts.menu')
<?php
if(sizeof($errors)>0){
    print_r($errors);
}
?>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Registrar Usuario</h2></div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'usuario.store','method'=>'POST', 'class'=>'form-horizontal')) !!}
                        {{ csrf_field() }}
                        @include('usuarios.form')
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar Usuario
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection