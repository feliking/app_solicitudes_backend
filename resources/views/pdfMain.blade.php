<!DOCTYPE html>
<html>
<head>
	{{-- 
		BEGIN: ISSUE 0013 
	--}}
	@include('parts.header_pdf')
	{{-- 
		END: ISSUE 0013 
	 --}}
	@yield('headerScripts')
</head>
<body>
	<div class="container body">
	    <div class="main_container">
	        @yield('content')
	    </div>
	</div>
	
	@yield("footerScripts")
</body>
</html>
