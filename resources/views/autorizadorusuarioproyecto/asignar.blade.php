@extends('main')

@section('content')
<?php include_once(app_path().'/functions.php')?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>ASIGNACIÓN DE USUARIO A AUTORIZADOR</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <?php $contador = 1;?>
                    {!! Form::open(array('action' => 'UsuarioController@AutorizadorUsuarioProyectoStore', 'method'=>'POST', 'class'=>'form-inline')) !!}
                    @foreach($proyectos as $proyecto)
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2">Proyecto: {{ $proyecto['nombre'] }}</th>
                                    <th>Asignar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($proyecto['usuarios'] as $usuario)
                                <tr>
                                    <td style="width: 10px; text-align: center;">{{ $contador++ }}</td>
                                    <td>{{ $usuario['nombre'] }}</td>
                                    <td style="width: 50px; text-align: center;">
                                        @if(in_array($proyecto['id']."-,-".$usuario['id'], $autorizaciones))
                                            {!! Form::checkbox('usuario_proyecto[]', $proyecto['id']."-,-".$usuario['id'], true); !!}
                                        @else
                                            {!! Form::checkbox('usuario_proyecto[]', $proyecto['id']."-,-".$usuario['id'], false); !!}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endforeach
                    {!! Form::hidden('autorizador_id', $autorizador_id) !!}
                    {!! Form::submit('GUARDAR ASIGNACIÓN', ['class' => 'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection