@extends('main')

@section('content')
@php
include_once(app_path().'/functions.php');
$contador = 1;
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>LISTA AUTORIZADORES</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Asignar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($autorizadores as $autorizador)
                            <tr>
                                <td style="width: 10px; text-align: center;">{{ $contador++."(".$autorizador['id'].")" }}</td>
                                <td>{{ $autorizador['nombre'] }}</td>
                                <td style="width: 50px; text-align: center;">
                                    <a id="{{ $autorizador['id'] }}" class="fa fa-edit icono-opciones icono-green" onclick="autorizadorModal(this)"></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
            <!-- MODAL -->
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#autorizadorAsignarUsuarioModal" style="visibility: hidden;">
                Modal solicitud
            </button>
            <div class="modal fade" id="autorizadorAsignarUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="autorizadorAsignarUsuarioModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="autorizadorAsignarUsuarioModalLabel"><strong>AUTORIZADOR <span id="modal-num"></span></strong></h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('route' => 'autorizador/usuario/guardar','method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
                            {{ Form::hidden('autorizador_id', 'secret', array('id' => 'autorizador')) }}
                            {{ Form::hidden('empresa_id', session('empresa'), array('id' => 'autorizador')) }}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>PROYECTOS/USUARIOS ASIGNADOS</h2></div>
                                <div class="panel-body">
                                    <div class="panel-group proy_user_accordion" id="accordion"></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer no-print">
                            {!! Form::submit('GUARDAR ASIGNACIÓN', ['class' => 'btn btn-success']) !!}
                            {!! Form::close() !!}
                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            <span class="pull-right" id="print-span"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
<script language="javascript" type="text/javascript">
    function autorizadorModal(e){
        $('#btn-modal-solicitud').click();
        $('#autorizador').val(e.id);
        var url_autorizador = '/json/asignacion_autorizador/'+e.id;
        $('.proy_user_accordion').empty();
        $.getJSON(url_autorizador, function(proyectos){
            var contador = 1;
            $.each(proyectos, function(i, usuarios){
                $panel = "<div class ='panel panel-default'>";
                $panel += "  <div class='panel-heading'>";
                $panel += "      <h4 class='panel-title'><a data-toggle='collapse' data-parent='#accordion' href='#collapse"+contador+"'>"+i+"</a><span class='pull-right'><input type='checkbox' id='proyecto_"+contador+"' onclick='allProyUsers(this)'></span></h4>";
                $panel += "  </div>";
                if(contador == 1){
                    $panel += "  <div id='collapse"+contador+"' class='panel-collapse collapse in'>";
                }else{
                    $panel += "  <div id='collapse"+contador+"' class='panel-collapse collapse '>";
                }
                $panel += "      <div class='panel-body' id='usuarios_"+contador+"'>";
                var fila = 0;
                $.each(usuarios, function(j, usuario){
                    fila++;
                    if(fila % 2 ==0){
                        fila_color = "#DFDFDF";
                    }else{
                        fila_color = "#FFF";
                    }
                    $panel += "     <div class='checkbox' style='background: "+fila_color+"'>";
                    if(usuario.checkbox){
                        valor = "checked='checked'";
                    }else{
                        valor = "";
                    }
                    $panel += "         <label style='display: inline;'><div style='display: inline-block;'>"+usuario.nombre+"</div><span class='pull-right' style='display: inline;'><input type='checkbox' name='proyectos_usuario[]' class='padre_"+contador+"' value='"+usuario.proyecto_id+"_"+usuario.id+"' "+valor+"></span></label>";
                    $panel += "     </div>";
                });
                $panel += "      </div>";
                $panel += "  </div>";
                $panel += "</div>";
                contador++;
                $(".proy_user_accordion").append($panel);
            });
        });
    }

    function allProyUsers(e){
        var array_data = (e.id).split('_');
        var padre = ".padre_"+array_data[1];
        var bool_check = false;
        bool_check = ($('#proyecto_'+array_data[1]).is(':checked'))?true:false;
        $(padre).each(function(){
            $(this).prop('checked', bool_check);
        });
    }
</script>
@endsection