<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="icon" href="/favicon.ico">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>Solicitudes </title>

{{ Html::style('/css/style.css') }}
<link href="http://localhost:7002/bootstrap.css" rel="stylesheet">
<!-- Bootstrap -->
{{ Html::style('/css/bootstrap.min.css') }}

<!-- Fontawesome -->
{{ Html::style('/css/font-awesome.min.css') }}

<!-- bootstrap-daterangepicker -->
{{ Html::style('/css/daterangepicker.css') }}

<!-- iCheck -->
{{ Html::style('/css/iCheck/skins/flat/green.css') }}

<!-- bootstrap-progressbar -->
{{ Html::style('/css/custom.min.css') }}
