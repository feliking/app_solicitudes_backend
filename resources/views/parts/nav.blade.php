<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/solicitud" class="site_title">
                @php
                $req_id_empresa = session('empresa');
                $gestiones = App\Gestiones::all();
                @endphp
                @if($req_id_empresa)
                    @php
                        $tit_emp = App\Empresas::find(session('empresa'))
                    @endphp
                    <img src="{{ asset($tit_emp->logo) }}">
                @endif
            </a>
        </div>
        <div class="clearfix"></div>
        <!-- /menu profile quick info -->
        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <!-- ADMINISTRADOR -->
            @php
            $user = auth()->user();
            $acceso_usuario = $user->rol_id;
            @endphp

            @if($acceso_usuario == '')
                //REDIRECCIONAR INDEX
            @elseif($acceso_usuario == 1 )
                <div class="menu_section">
                    <h3>Administración</h3>
                    <ul class="nav side-menu">
                        <li>
                            <a><i class="fa fa-group"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/usuario">Listado</a></li>
                                <li><a href="/usuario/create">Crear</a></li>
                                <li><a href="/asignar/proyectos">Asignar Proyectos</a></li>
                                <li><a href="/autorizador/usuario/proyecto/lista">Autorizador Usuarios</a></li>
                                <li><a href="/aprorendicion/empresa/lista">Apr. Rendiciones Empresas</a></li>
                                <li><a href="#">Buscar</a></li>
                            </ul>
                        </li>
                        <li>
                            <a><i class="fa fa-institution"></i> Empresas <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/empresa">Listado</a></li>
                                <li><a href="/empresa/create">Crear</a></li>
<!--                                            <li><a href="/asignar-empresa">Asignar</a></li>-->
                            </ul>
                        </li>
                        <li>
                            <a><i class="fa fa-building-o"></i> Proyectos <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/proyectos">Listado</a></li>
                                @if($user->rol_id == 1 || $user->rol_id >= 3)
                                    <li><a href="{{ route('proyectos.create') }}">Crear</a></li>
                                @endif
                                <li><a href="#">Buscar</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            @endif
            <!-- END ADMINISTRADOR -->
            <div class="menu_section">
                <h3>MENU GENERAL</h3>
                <ul class="nav side-menu">
                    @if ($user->global_view != 1 && $user->rol_id != 8)
                    <li>
                        <a href="/seleccion-empresa"><i class="fa fa-institution"></i> Empresas</a>
                    </li>
                    @endif
                    @if ($user->global_view != 1 && $user->rol_id != 8)
                    <li><a><i class="fa fa-calendar-o"></i> Gestiones <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @foreach($gestiones as $gestion)
                                <li>
                                    <a href="/gestion/{{ $gestion->id }}" style="display: block">{{ $gestion->gestion }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    @endif
                    <!-- Menu Admin -->
                    @if($user->rol_id == 8)
                    <li>
                        <a href="{{ route('solicitudes.admin') }}">
                            <i class="fa fa-edit"></i> Solicitudes
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('solicitud.tesoreria.transferencia.report') }}">
                            <i class="fa fa-exchange"></i> Transferencias
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.index') }}">
                            <i class="fa fa-bar-chart"></i> Dashboard
                        </a>
                    </li>
                    @endif
                    <!-- Menu Admin -->
                    <li>
                        @if ($user->rol_id != 8)
                        <a><i class="fa fa-edit"></i> Solicitudes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                        @endif
                            @if ($user->rol_id != 8 && $user->global_view != 1)
                                <li>
                                    <a href="/solicitudes" style="display: block">Usuario</a>
                                </li>
                            @elseif ($user->rol_id != 8)
                            <li>
                                <a href="{{ route('global.en_curso') }}" style="display: block">Usuario</a>
                            </li>
                            <li>
                                <a href="{{ route('global.autorizar') }}" style="display: block">Autorización</a>
                            </li>
                            <li>
                                <a href="{{ route('global.aprobar') }}" style="display: block">Aprobador parcial</a>
                            </li>
                            <li>
                                <a href="{{ route('global.aprobar_rendiciones') }}" style="display: block">Aprobar rendiciones</a>
                            </li>
                            <li id="global_pendientes">
                                <a href="{{ route('global.pendientes') }}" style="display: block">Pendientes</a>
                            </li>
                            <li>
                                <a href="{{ route('global.revisor_sujeto_rendicion') }}" style="display: block">Revisor</a>
                            </li>
                            @endif
                            @if($user->rol_id == 3 && $user->global_view != 1)
                                <li>
                                    <a href="/solicitud-autorizar?tabs=autorizador" style="display: block">Autorización</a>
                                </li>
                            @endif
                            @if(($user->rol_id == 4 || $user->revisor) && $user->global_view != 1)
                                <li>
                                    @if(Auth::user()->revisa->first()->id == 3)
                                        <a href="/solicitud-controller?tabs=revisor&tipo_fr={{ Auth::user()->revisa_fr->first()->id }}" style="display: block">Revisor</a>
                                    @else
                                        <a href="/solicitud-controller?tabs=revisor&modalidad={{ Auth::user()->revisa->first()->id }}" style="display: block">Revisor</a>
                                    @endif
                                </li>
                            @endif
                            @if($user->rol_id == 5 && $user->global_view != 1)
                                <li>
                                    <a href="/solicitud-aprobar?tabs=aprobador" style="display: block">Aprobador</a>
                                </li>
                            @endif
                            @if($user->apr_parcial && $user->global_view != 1)
                                <li>
                                    <a href="/solicitud-aprobar?tabs=aprobador&id_empresa={{ session('empresa') }}" style="display: block">Aprobador Parcial</a>
                                </li>
                            @endif
                            {{-- 
                            @if($user->apr_parcial && $user->global_view != 1)
                                <li>
                                    <a href="/aprobador/solicitudes/parciales?tabs=aprobador&id_empresa={{ session('empresa') }}" style="display: block">Sol. Apr. Parciales</a>
                                </li>
                            @endif
                            --}}
                            @if($user->rol_id == 6 && $user->global_view != 1)
                                <li>
                                    <a href="/solicitud-tesoreria?tabs=tesoreria" style="display: block">Tesorería</a>
                                </li>
                                <li>
                                    <a href="{{ route('solicitud.tesoreria.transferencia.create') }}" style="display: block;">Transferencia</a>
                                </li>
                            @endif
                            @if($user->apr_rendiciones && $user->global_view != 1)
                                @php
                                $array_empresas = array();
                                foreach($user->aprorendicion_empresas as $empresa_id){
                                    $array_empresas[] = $empresa_id->id;
                                }
                                @endphp
                                {{-- @if(in_array(session('empresa'), $array_empresas)) --}}
                                @if($user->apr_rendiciones)
                                    <li>
                                        <a href="/aprobador-rendiciones?tabs=aprrendiciones" style="display: block">Aprobar Rendiciones</a>
                                    </li>
                                @endif
                            @endif
                            @if($user->consultor)
                                <li>
                                    <a href="/solicitud-consultor?tabs=consultor&id_empresa={{ session('empresa') }}" style="display: block">Consultar Rendiciones</a>
                                </li>
                            @endif
                        @if ($user->rol_id != 8)
                        </ul>
                        @endif
                    </li>
                    @if($user->rol_id != 8)
                    <li>
                        <a><i class="fa fa-file-text"></i> Reportes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li>
                                @if ($user->id != 32)
                                <a href="/reporte/general/usuario?id_empresa={{ session('empresa') }}" style="display: block">Reporte General</a>
                                @else
                                <a href="/reporte/general/usuario" style="display: block">Reporte General</a>
                                @endif
                            </li>
                            @if ($user->rol_id == 6)
                            <li>
                                <a href="{{ route('solicitud.tesoreria.transferencia.report') }}" style="display: block">Reporte de Transferencias</a>
                            </li>
                            @endif
                            @if($user->rol_id == 4 || $user->revisor)
                                <li>
                                    <a href="/reporte/revisor?tabs=revisor&id_empresa={{ session('empresa') }}" style="display: block">Reporte Revisor</a>
                                </li>
                            @endif
                            @if($user->apr_rendiciones == true)
                                <li>
                                    <a href="/reporte/apr_rendiciones" style="display: block">Reporte Apr. Rendiciones</a>
                                </li>
                            @endif
                            @if($user->global_view == 1 || $user->id == 99 || $user->id == 100) {{-- Usuarios de Jesus Segovia y Gerardo Cazas --}}
                                <li>
                                    <a href="{{ route('reportes.activos_fijos') }}" style="display: block">Reporte Activos Fijos</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if($user->rol_id == 1 || $user->rol_id == 6)
                        <li>
                            <a><i class="fa fa-edit"></i> Bancos <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/bancos">Listado</a></li>
<!--                                <li><a href="/bancos/create">Crear</a></li>-->
                                <li><a href="/cuentas">Cuentas</a></li>
                            </ul>
                        </li>
                    @endif
                    @if($user->rol_id == 1 || $user->rol_id == 6)
                        <li>
                            <a><i class="fa fa-money"></i> Libreta Bancaria <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/libreta">Libreta</a></li>
                                <li><a href="/transferencia">Transferencia de fondos</a></li>
                                <li><a href="/movimiento/ingreso">Ingreso</a></li>
                                <li><a href="/movimiento/egreso">Egreso</a></li>
                            </ul>
                        </li>
                        <li>
                            <a><i class="fa fa-money"></i> Cheques <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="/cheques">Listado</a></li>
<!--                                <li><a href="/cheques/anulados">Anulados</a></li>
                                <li><a href="/cheques/registrados">Registrados</a></li>-->
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top"></a>
            <a data-toggle="tooltip" data-placement="top"></a>
            <a data-toggle="tooltip" data-placement="top" title="SELECCIONAR EMPRESA" href="/seleccion-empresa">
                <span class="fa fa-institution" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="CERRAR SESIÓN" id="btn-logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<!-- top navigation -->
<div class="top_nav no-print">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <div class="nav_nombre_empresa">
                @if(FALSE !== session('empresa') && session('empresa') != '')
                    {{ "&nbsp;&nbsp;&nbsp;" }}<i class='fa fa-institution'></i> Empresa: <span id='nom_empresa'>{{ $tit_emp->nombre }} Gestión: {{ session('gestion') }}</span>
                @elseif($user->rol_id == 8 || $user->global_view == 1)
                    {{ "&nbsp;&nbsp;&nbsp;" }}<i class='fa fa-institution'></i>{{ " TODAS LAS EMPRESAS" }}
                @else
                    {{ "SIN EMPRESAS!!!" }}
                @endif
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        {{ ucfirst(Auth::user()->nombre).'&nbsp;&nbsp;'.ucfirst(Auth::user()->ap).'&nbsp;&nbsp;'.ucfirst(Auth::user()->am) }}&nbsp;&nbsp;
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="/clave/usuario"> Cambio de contraseña</a></li>
                        <li>
                            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out pull-right"></i>  Cerrar Sesión
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->

@section('footerScripts')
@parent
<script>
    @if(\Auth::user()->rol_id == 8)
    $(document).ready(function (){
        // Collapse Nav
        $('body')
            .addClass('nav-sm')
            .removeClass('nav-md');

        $('li.active').addClass('active-sm')
            .removeClass('active');

        $('ul.child_menu').css('display', 'none');
    });
    @endif
</script>
@endsection