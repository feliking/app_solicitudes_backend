<!-- jQuery -->
{{ Html::script('/js/jquery.min.js') }}

<!-- Bootstrap -->
{{ Html::script('/js/bootstrap.min.js') }}

<!-- FastClick fastclick.js-->
{{ Html::script('/js/fastclick.js') }}

<!-- NProgress http://ricostacruz.com/nprogress/-->
{{ Html::script('/js/nprogress.js') }}

<!-- iCheck -->
{{ Html::script('/js/icheck.min.js') }}


<!-- bootstrap-daterangepicker https://momentjs.com/ | http://www.daterangepicker.com/ -->
{{ Html::script('/js/moment.min.js') }}
{{ Html::script('/js/daterangepicker.js') }}

<!-- Mi formato de date picker -->
{{ Html::script('/js/fecha-picker-format.js') }}

<!-- Custom Theme Scripts - SIRVE PARA LA BARRA DE NAVEGACION -->
{{ Html::script('/js/custom.min.js') }}
{{--<script src="/menu/build/js/custom.min.js"></script>--}}

{{-- 
	BEGIN: ISSUE 0016
 --}}
{{ Html::script('/js/input_mask/dist/jquery.inputmask.bundle.js') }}
{{ Html::script('/js/input_mask/init_mask.js') }}
{{-- 
	END: ISSUE 0016
 --}}
{{ Html::script('/js/main.js') }}


