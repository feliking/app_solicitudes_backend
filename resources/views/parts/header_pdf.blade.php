<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="icon" href="/favicon.ico">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<title>Solicitudes </title>

<link rel="stylesheet" href="{{ public_path('css/style.css') }}">

<!-- Bootstrap -->
<link rel="stylesheet" href="{{ public_path('css/bootstrap.min.css') }}">

<!-- Fontawesome -->
<link rel="stylesheet" href="{{ public_path('css/font-awesome.min.css') }}">
<!-- bootstrap-daterangepicker -->
<link rel="stylesheet" href="{{ public_path('css/daterangepicker.css') }}">

<!-- iCheck -->
<link rel="stylesheet" href="{{ public_path('css/iCheck/skins/flat/green.css') }}">

<!-- bootstrap-progressbar -->
<link rel="stylesheet" href="{{ public_path('css/custom.min.css') }}">