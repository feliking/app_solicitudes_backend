@extends('main')
<?php
if(sizeof($errors)>0){
    print_r($errors);
}
?>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Asignación de empresa</h2></div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'guardar-asignacion-empresa','method'=>'POST', 'class'=>'form-horizontal')) !!}
                    <input type="hidden" name="usuario_id" value="<?=$_POST['id_usuario']?>"/>
                    <?php
                    foreach($empresas as $empresa){
                        ?>
                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input type="checkbox" name="empresa[]" value="<?=$empresa->id?>" <?=($empresa->checked ? 'checked="checked"' : '');?>"> <?=$empresa->nombre?><br>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                        <div class="col-md-6 col-md-offset-4">
                            {!! Form::submit('Guardar Asignación', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection