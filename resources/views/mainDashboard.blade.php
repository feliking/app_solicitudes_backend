<!DOCTYPE html>
<html lang="en">
<head>
    @include('parts.header')
    @yield('headerScripts')
</head>

@php
$userData = auth()->user();
@endphp

<body class="nav-md">
    <div class="container body">
        <div class="main_container">

            @include('parts.nav')

            <!-- page content -->
            <div class="right_col">
                @include('parts.messages')
                
                @yield('content')
                <div class="clearfix"></div>
            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="text-center">
                    <a href="#">&COPY; 2020 Pragma Invest S.A</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>
    <input type="hidden" name="empresa" id="emp_id" data-id="{{ session('empresa') }}" value="{{ session('empresa') }}">
    <input type="hidden" name="user_rol_id" id="user_rol_id" data-id="{{ $userData->rol_id }}" value="{{ $userData->rol_id }}">
    
    @include('parts.javascript')
    
    @yield('footerScripts')

    <script language="javascript" type="text/javascript">
        
    </script>
</body>
</html>