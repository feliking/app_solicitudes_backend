@extends('main')

@section('content')
<?php include_once(app_path().'/functions.php')?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Estados</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Cantidad</th>
                                <th>Monto $us</th>
                                <th>Monto Bs</th>
                            </tr>
                        </thead>
                        <?php $contador = 1?>
                        <tbody>
                            @foreach($estados as $estado)
                            <tr>
                                <td><?=$contador++;?></td>
                                <td><?=$estado->codigo;?></td>
                                <td><?=$estado->descripcion;?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @endforeach
                            <tr>
                                <td style="text-align: right;" colspan="4">Total</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

