@extends('main')

@section('content')
@php
include_once(app_path().'/functions.php');
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Cambio de Contraseña: {{ $usuario->nombre }}</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    {!! Form::model($usuario, array('route' => ['clave/usuario/guardar', $usuario->id], 'method'=>'PUT', 'class'=>'form-horizontal')) !!}
                        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                            <label for="old_password" class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                {!! Form::password('old_password', array('placeholder' => 'CONTRASEÑA ACTUAL','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                {!! Form::password('password', array('placeholder' => 'NUEVA CONTRASEÑA','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password_confirmation" class="col-md-4 control-label">Confirmar Contraseña</label>
                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', array('placeholder' => 'CONFIRMAR CONTRASEÑA','class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Guardar Cambios
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

