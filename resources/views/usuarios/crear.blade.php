@extends('main')

@section('headerScripts')
    @parent
    {!! Html::style('/css/select2.min.css') !!}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Registrar Usuario</h2>
                </div>

                <div class="panel-body">
                    {!! Form::open(array('route' => 'usuario.store', 'method'=>'POST', 'class'=>'form-horizontal')) !!}
                        @include('usuarios.form')

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                {!! Form::text('password', null, array('placeholder' => 'Contraseña','class' => 'form-control', 'required' => 'required')) !!}
                                <a id="generarClave" class="btn btn-info">Generar Contraseña</a>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('rendiciones') ? ' has-error' : '' }}">
                            <label for="rendiciones" class="col-md-4 control-label">Aprobar Rendiciones</label>
                            <div class="col-md-6">
                                {!! Form::checkbox('rendiciones', 'true', '', ['style' => 'vertical-align:middle; margin-top: 12px;']) !!}
                            </div>
                        </div>
                        {{-- 
                            BEGIN: ISSUE 0001
                         --}}
                        <div class="form-group{{ $errors->has('asignar_proyecto') ? ' has-error' : '' }}">
                            <label for="asignar_proyecto" class="col-md-4 control-label">Asignador de proyectos</label>
                            <div class="col-md-6">
                                {!! Form::checkbox('asignar_proyecto', 'true', '', ['style' => 'vertical-align:middle; margin-top: 12px;']) !!}
                            </div>
                        </div>
                        {{-- 
                            END: ISSUE 0001
                         --}}
                        <div id="niveles_aprobador" class="col-md-4 col-md-offset-4" style="display: none;">
                            <table class="table table-bordered" style="width: 300px;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style="text-align: center;">NIVELES DE APROBACION</th>
                                    </tr>
                                    <tr>
                                        <th>NIVEL</th>
                                        <th>DESDE</th>
                                        <th>HASTA</th>
                                        <th>PORCENTAJE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($niveles as $nivel)
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center;">{{ $nivel->nivel }}</td>
                                        <td style="vertical-align: middle;">{{ number_format($nivel->desde, '2', '.', ',') }}</td>
                                        <td style="vertical-align: middle;">{{ number_format($nivel->hasta, '2', '.', ',') }}</td>
                                        <td>
                                            {!! Form::hidden('nivel_id[]', $nivel->id, []) !!}
                                            {!! Form::number('nivel[]', null, array('placeholder' => 'Porcentaje','class' => 'form-control validar_porcentaje', 'min' => 0, 'required' => 'true', 'disabled' =>'true')) !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                <button type="submit" class="btn btn-primary">
                                    Registrar Usuario
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- 
    BEGIN: ISSUE 0007
--}}
@section('footerScripts')
@parent
<script>
$('#cambiar_clave').click(function(e){
    $('.hide').each(function(){
        $('input[name=password]').val('');
        $(this).removeClass('hide');
    });
    $(this).addClass('hide');
});
</script>
@endsection
{{-- 
    END: ISSUE 0007
--}}