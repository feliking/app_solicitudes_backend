@extends('main')

@section('content')
@php
include_once(app_path().'/functions.php');
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="crear_empresa">
                        <a class="btn btn-success pull-right" href="{{ route('usuario.create') }}">Adicionar Usuario</a>
                    </div>
                    <h2>Usuarios</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>E-mail</th>
                                <th>Acceso</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usuario)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$usuario->nombre}}</td>
                                <td>{{$usuario->usuario}}</td>
                                <td>{{$usuario->email}}</td>
                                <td>{{$usuario->rol->nombre}}</td>
                                <td>
                                    <a class="fa fa-edit icono-opciones icono-green" href="{{ route('usuario.edit', $usuario->id) }}"></a>
                                    {!! Form::open(['method' => 'DELETE','route' => ['usuario.destroy', $usuario->id],'style'=>'display:inline']) !!}
                                        <i class="fa fa-times-circle-o icono-opciones icono-red" onClick="javascript:this.form.submit();"></i>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

