@extends('main')

@section('headerScripts')
    @parent
    {!! Html::style('/css/select2.min.css') !!}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Modificar Datos Usuario</h2>
                </div>

                <div class="panel-body">
                    {!! Form::model($usuario, array('route' => ['usuario.update', $usuario->id], 'method'=>'PUT', 'class'=>'form-horizontal')) !!}
                        @include('usuarios.form')

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="button" class="btn btn-primary" id="cambiar_clave">
                                    Cambiar Contraseña
                                </button>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} hide">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                {{-- 
                                    BEGIN: ISSUE 0031
                                 --}}
                                {!! Form::password('password', array('placeholder' => 'NUEVA CONTRASEÑA','class' => 'form-control', 'disabled' => 'true', 'id' => 'password', 'required' => 'true')) !!}
                                {{-- 
                                    END: ISSUE 0031
                                 --}}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('confirm_confirm_password') ? ' has-error' : '' }} hide">
                            <label for="confirm_password" class="col-md-4 control-label">Confirmar Contraseña</label>
                            <div class="col-md-6">
                                {{-- 
                                    BEGIN: ISSUE 0031
                                 --}}
                                {!! Form::password('confirm_password', array('placeholder' => 'CONFIRMAR CONTRASEÑA','class' => 'form-control', 'disabled' => 'true', 'id' => 'confirm_password', 'required' => 'true')) !!}
                                {{-- 
                                    END: ISSUE 0031
                                 --}}
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('rendiciones') ? ' has-error' : '' }}">
                            <label for="rendiciones" class="col-md-4 control-label">Aprobar Rendiciones</label>
                            <div class="col-md-6">
                                {!! Form::checkbox('rendiciones', 'true', $usuario->apr_rendiciones, ['style' => 'vertical-align:middle; margin-top: 12px;']) !!}
                            </div>
                        </div>
                        {{-- 
                            BEGIN: ISSUE 0001
                         --}}
                        <div class="form-group{{ $errors->has('asignar_proyecto') ? ' has-error' : '' }}">
                            <label for="asignar_proyecto" class="col-md-4 control-label">Asignador de proyectos</label>
                            <div class="col-md-6">
                                {!! Form::checkbox('asignar_proyecto', 'true', $usuario->asignador_proyecto , ['style' => 'vertical-align:middle; margin-top: 12px;']) !!}
                            </div>
                        </div>
                        {{-- 
                            END: ISSUE 0001
                         --}}
                        <div id="niveles_aprobador" class="col-md-6 col-md-offset-4" style="display: none;">
                            <table class="table table-bordered" style="width: 300px;">
                                <thead>
                                    <tr>
                                        <th colspan="4" style="text-align: center;">NIVELES DE APROBACION</th>
                                    </tr>
                                    <tr>
                                        <th>NIVEL {{ $usuario->id }}</th>
                                        <th>DESDE</th>
                                        <th>HASTA</th>
                                        <th>PORCENTAJE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- 
                                        BEGIN: ISSUE 0028, 0031
                                     --}}                                     
                                    @forelse ($usuario->nivel_aprobaciones as $nivel)
                                        <tr>
                                            <td style="vertical-align: middle; text-align: center;">{{ $nivel->nivel }}</td>
                                            <td style="vertical-align: middle;">{{ number_format($nivel->desde, '2', '.', ',') }}</td>
                                            <td style="vertical-align: middle;">{{ number_format($nivel->hasta, '2', '.', ',') }}</td>
                                            <td>
                                                {!! Form::hidden('nivel_id[]', $nivel->id, []) !!}
                                                {!! Form::number('nivel[]', $nivel->pivot->porcentaje, array('placeholder' => 'Porcentaje','class' => 'form-control validar_porcentaje', 'min' => 0, 'required' => 'true')) !!}
                                                {!! Form::hidden('nivel_aprobador_id[]', $nivel->pivot->id, []) !!}
                                            </td>
                                        </tr>
                                    @empty
                                         @foreach($niveles as $nivel)
                                            <tr>
                                                <td style="vertical-align: middle; text-align: center;">{{ $nivel->nivel }}</td>
                                                <td style="vertical-align: middle;">{{ number_format($nivel->desde, '2', '.', ',') }}</td>
                                                <td style="vertical-align: middle;">{{ number_format($nivel->hasta, '2', '.', ',') }}</td>
                                                {!! Form::hidden('nivel_id[]', $nivel->id, []) !!}
                                                <td>{!! Form::number('nivel[]', null, array('placeholder' => 'Porcentaje','class' => 'form-control validar_porcentaje', 'min' => 0, 'required' => 'true', 'disabled' =>'true')) !!}</td>
                                            </tr>
                                        @endforeach
                                    @endforelse
                                    {{-- 
                                        END: ISSUE 0028, 0031
                                     --}}
                                </tbody>
                            </table>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                <button type="submit" class="btn btn-primary">
                                    Guardar Cambios
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
@parent
<script>
$('#cambiar_clave').click(function(e){
    $('.hide').each(function(){
        $('input[name=password]').val('');
        $(this).removeClass('hide');
    });
    $(this).addClass('hide');
    $('#confirm_password').removeAttr('disabled');
    $('#password').removeAttr('disabled');
});

/**
 * BEGIN: ISSUE 0007
 */
$(document).ready(function (){
    $('select[name="empresa_id"]').val({{ $usuario->empleado->empresa->id }});
    $('select[name="empresa_id"]').change();

    $('select[name="acceso"]').val({{ $usuario->rol_id }});
    $('select[name="acceso"]').change();

    /**
     * BEGIN: issue 0028
     */
    $('select[name="empresa_id"]').attr('disabled', 'true');
    $('select[name="empleado_id"]').attr('disabled', 'true');
    /**
     * END: ISSUE 0028
     */
});
/**
 * BEGIN: ISSUE 0007
 */
</script>
@endsection