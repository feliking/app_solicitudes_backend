<div class="form-group{{ $errors->has('empresa') ? ' has-error' : '' }}">
    <div class="col-lg- ">
        <label for="empresa_id" class="col-md-4 control-label">Empresa </label>
        <div class="col-md-6">
            {!! Form::select('empresa_id', $empresas, null, ['class' => 'form-control select2 js-placeholder-single', 'required' => 'required', 'placeholder' => 'Seleccione una empresa']) !!}
            @if ($errors->has('empresa_id'))
                <span class="help-block">
                    <strong>{{ $errors->first('empresa_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>

<div class="form-group{{ $errors->has('empresa_id') ? ' has-error' : '' }}">
        <div class="col-lg- ">
            <label for="empleado_id" class="col-md-4 control-label">Empleado </label>
            <div class="col-md-6">
                {!! Form::select('empleado_id', [], null, ['class' => 'form-control select2 js-placeholder-single', 'required' => 'required', 'placeholder' => 'Seleccione un empleado']) !!}
                @if ($errors->has('empleado_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('empleado_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

<div class="form-group{{ $errors->has('acceso') ? ' has-error' : '' }}">
    <label for="acceso" class="col-md-4 control-label">Rol</label>
    <div class="col-md-6">
        <select class="form-control" id="acceso" name="acceso" onchange="selectRol(this)">
        @foreach($roles as $rol)
            <option value='{{ $rol->id }}'>{{ $rol->nombre }}</option>"
        @endforeach
        </select>
    </div>
</div>

<div class="form-group{{ $errors->has('usuario') ? ' has-error' : '' }}">
    <label for="usuario" class="col-md-4 control-label">Usuario</label>
    <div class="col-md-6">
        {!! Form::text('usuario', null, array('placeholder' => 'Nombre de usuario','class' => 'form-control', 'required' => 'required')) !!}
    </div>
</div>

@section('footerScripts')
@parent
{{ Html::script('/js/select2.min.js') }}
<script type="text/javascript">
    $( document ).ready(function() {
        $('select.select2').select2();
    });

    /**
     * BEGIN: ISSUE 0007
     */
    // Ajax getEmpleadosPorEmpresa <- Json
    $('select[name="empresa_id"]').on('change', function(event){
        var idEmpresa = $(this).val();

        var url = '{{ route('empresa.getEmpleados', 'empresaId') }}';
        url = url.replace('empresaId', idEmpresa);
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function(e){
                /**
                 * BEGIN: ISSUE 0028
                 */
                // $('select[name="empleado_id"]').prop('disabled', true);
                /**
                 * END: ISSUE 0028
                 */
                $('select[name="empleado_id"]').empty()
                    .append("<option selected='selected' value=''>Seleccione un empleado</option>");
            }
        }).done(function (response){
            if(response['empleados'] != null){
                $.each(response['empleados'], function(key, value){
                    $('select[name="empleado_id"]').append("<option value='"+value['id']+"'>"+value['apellido_1']+" "+value['apellido_2']+" "+value['nombres']+"</option>");
                });
                /**
                 * BEGIN: ISSUE 0028
                 */
                // $('select[name="empleado_id"]').prop('disabled', false);
                /**
                 * END: ISSUE 0028
                 */
                @if(isset($usuario->empleado_id))
                // Empleado Actual
                var idEmpleadoActual = {{ $usuario->empleado_id }};
                $('select[name="empleado_id"]').val(idEmpleadoActual);
                $('select[name="empleado_id"]').change();
                @endif

                @if(old('empleado_id'))
                // Error de validación
                $('select[name="empleado_id"]').val({{ old('empleado_id') }});
                $('select[name="empleado_id"]').change();
                @endif
            }
        }).fail(function (response){

        });
        url = url.replace(idEmpresa, 'empresaId');
    });
    // Ajax getEmpleadosPorEmpresa <- Json
    /**
     * END: ISSUE 0007
     */

    function selectRol(ele){
        var e = document.getElementById(ele.id);
        var valor = e.options[e.selectedIndex].value;
        if(valor == 5){
            document.getElementById('niveles_aprobador').style.display = 'inline-block';
            /**
             * BEGIN: ISSUE 0031
             */            
            $('.validar_porcentaje').removeAttr('disabled');
            /**
             * END: ISSUE 0031
             */
        }else{
            document.getElementById('niveles_aprobador').style.display = 'none';
            /**
             * BEGIN: ISSUE 0031
             */            
            $('.validar_porcentaje').attr('disabled', 'true');            
            /**
             * END: ISSUE 0031
             */
        }
    }
</script>
@endsection