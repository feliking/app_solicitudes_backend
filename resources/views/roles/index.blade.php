@extends('main')

@section('content')
<?php include_once(app_path().'/functions.php')?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Roles</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <?php $contador = 1?>
                        <tbody>
                            @foreach($roles as $rol)
                            <tr>
                                <td><?=$contador++;?></td>
                                <td><?=$rol->id;?></td>
                                <td><?=$rol->nombre;?></td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection