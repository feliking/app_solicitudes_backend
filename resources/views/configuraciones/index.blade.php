@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>Configuraciones</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="panel-group proy_user_accordion" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed">DÍAS LÍMITE PARA RENDICIÓN</a>
                                </h4>
                            </div>
                            {!! Form::open(array('route' => ['configuraciones.update', 'dias_limite'], 'method'=>'PUT', 'class'=>'form-horizontal', 'files' => true)) !!}
                            <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body" id="usuarios_1">
                                    <div class="checkbox" style="background: #FFF">
                                        <div class="form-group wrapper-variable{{ $errors->has('variable') ? ' has-error' : '' }}">
                                            {!! Form::label('variable', 'DÍAS', ['class' => 'col-md-2 col-md-offset-1 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::input('text', 'variable', $configuraciones['dias_limite'], ['class' => 'form-control', 'placeholder' => 'DÍAS LÍMITE PARA RENDICIONES']) !!}
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('variable') }}</strong>
                                                </span>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                {{ Form::submit('ACTUALIZAR DÍAS', array('class' => 'btn btn-success')) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false">UFV'S</a>
                                </h4>
                            </div>
                            {!! Form::open(array('route' => ['configuraciones.update', 'ufv'], 'method'=>'PUT', 'class'=>'form-horizontal', 'files' => true)) !!}
                            <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body" id="usuarios_1">
                                    <div class="checkbox" style="background: #FFF">
                                        <div class="form-group wrapper-variable{{ $errors->has('variable') ? ' has-error' : '' }}">
                                            {!! Form::label('variable', 'UFVs', ['class' => 'col-md-2 col-md-offset-1 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::input('text', 'variable', $configuraciones['ufv'], ['class' => 'form-control', 'placeholder' => 'valor ufv']) !!}
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('variable') }}</strong>
                                                </span>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                {{ Form::submit('ACTUALIZAR VALOR UFV', array('class' => 'btn btn-success')) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false">DÍAS RENDICIÓN</a>
                                </h4>
                            </div>
                            {!! Form::open(array('route' => ['configuraciones.update', 'dias_rendicion'], 'method'=>'PUT', 'class'=>'form-horizontal', 'files' => true)) !!}
                            <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body" id="usuarios_1">
                                    <div class="checkbox" style="background: #FFF">
                                        <div class="form-group wrapper-variable{{ $errors->has('variable') ? ' has-error' : '' }}">
                                            {!! Form::label('variable', 'DÍAS RENDICIÓN', ['class' => 'col-md-2 col-md-offset-1 control-label']) !!}
                                            <div class="col-md-8">
                                                {!! Form::input('text', 'variable', $configuraciones['dias_rendicion'], ['class' => 'form-control', 'placeholder' => 'valor dias_rendicion']) !!}
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('variable') }}</strong>
                                                </span>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                {{ Form::submit('ACTUALIZAR VALOR', array('class' => 'btn btn-success')) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection