@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection
@section('content')
<?php
if(!isset($array_addquery)){
    $array_addquery = '';
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-default no-print">
                <div class="panel-heading">
                    <div class="col-md-3">
                        <h2>Solicitudes</h2>
                    </div>
                    <!-- SEARCH ENGINE -->
                    <?php
                    $contador = 1;
                    ?>
                    <div class="col-md-6">
                        {!! Form::open(array('action' => 'SolicitudController@index', 'method'=>'POST', 'class'=>'form-inline')) !!}
                            {!! Form::label('buscar', 'BUSCAR: ') !!}
                            {!! Form::text('buscar', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase')) !!}
                            {!! Form::select('campo', ['TODOS', 'NRO. DE SOLICITUD', 'MONTO', 'PROYECTO', 'FECHA', 'HORA', 'SOLICITANTE', 'DESEMBOLSO', 'TIPO DE CAMBIO', 'MODALIDAD', 'MONEDA', 'REFERENCIA', 'OBSERVACIÓN'], 0, ['class' => 'form-control']) !!}
                            {!! Form::submit('BUSCAR', ['class' => 'btn btn-success']) !!}
                        {!! Form::close() !!}
                        <?php
                        if(isset($_GET['ordenar'])){
                            ?>
                            <p style="font-size:12px; margin-bottom: 0px; padding-bottom: 0px; text-align: center;">ORDENADO POR: <?=strtoupper($_GET['ordenar'])?> (<?=$orden_type?>)</p>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-md-3">
                        <div class="crear_empresa">
                            <a class="btn btn-success" href="{{ route('solicitud.create') }}">NUEVA SOLICITUD</a>
                        </div>
                    </div>
                    <!-- END SEARCH ENGINE -->
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered table-hover table-solicitudes">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>NRO.<br>SOL.</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>ESTADO</th>
                                <th>MONTO</th>
                                <th>PROYECTO</th>
                                <th>SOLICITANTE</th>
                                <th>DESEMBOLSO</th>
                                <th>TIPO DE<br>CAMBIO</th>
                                <th>MODALIDAD</th>
                                <th>MONEDA</th>
                                <th>REFERENCIA</th>
                                <th>JUSTIFICACIÓN</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($solicitudes as $solicitud)
                                <tr id="sol_fila_<?=$solicitud->id;?>" ondblclick="seleccion_solicitud(this)">
                                    <td>
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        @php
                                            $usuario = auth()->user();
                                        @endphp
                                        @if($solicitud->estado == 'SOLICITUD CREADA' && $usuario->id == $solicitud->usuario_id)
                                            <a class="fa fa-edit icono-opciones-files icono-green" href="{{ route('solicitud.edit', $solicitud->id) }}"></a>
                                        @endif
                                    </td>
                                    <td id="solicitud_<?=$solicitud->id;?>"><?=$solicitud->numero;?></td>
                                        <span id="nom_proy_<?=$solicitud->id;?>" style="display:none;"></span>
                                        <span id="nom_subproy_<?=$solicitud->id;?>" style="display:none;"></span>
                                    </td>
                                    <td>
                                        <a id="<?=$solicitud->id;?>" onclick="subir_docs(this)"><i class="fa fa-cloud-upload icono-opciones-files icono-green"></i></a>&nbsp;
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="<?=$solicitud->id;?>" onclick="descarga_docs(this)"><i class="fa fa-cloud-download icono-opciones-files icono-red"></i></a>
                                        @endif
                                    </td>
                                    <td>{{ $solicitud->estadoCodigo }}</td>

                                    <td id="total_<?=$solicitud->id;?>" style="text-align: right;"><?=number_format($solicitud->total, '2', '.', '');?></td>
                                    <td id="proyecto_<?=$solicitud->id;?>">{{$solicitud->proyecto->nombre}}</td>

                                    <td id="usuario_<?=$solicitud->id;?>">
                                        <?=$solicitud->usuario->nombre." ".$solicitud->usuario->ap;?>
                                    </td>
                                    <td id="desembolso_<?=$solicitud->id;?>"><?=$solicitud->desembolso;?></td>
                                    <td id="cambio_<?=$solicitud->id;?>"><?=$solicitud->tipo_cambio->cambio;?></td>
                                    <td id="tipo_<?=$solicitud->id;?>">{{$solicitud->modalidad->nombre}}</td>
                                    <td id="moneda_<?=$solicitud->id;?>">{{$solicitud->moneda->nombre}}</td>
                                    <td id="referencia_<?=$solicitud->id;?>"><?=$solicitud->referencia;?></td>
                                    <td id="obs_<?=$solicitud->id;?>"><?=$solicitud->observacion;?></td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->links() }}
                </div>
            </div>
            <!-- MODAL -->
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>

            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            @include('solicitudes.partial.docsUp')
            <!-- END MODAL -->
            <script>
                function imprimirSolicitud(e){
                    var id = e.id;
                    window.location.href = '/solicitud-imprimir?id_sol='+id;
                }
                function subir_docs(e){
                    //llenar datos necesarios para documentos en la solicitud deseada
                    document.getElementById('id_sol_subir').value = e.id;

                    var btn_modal_sub = document.getElementById('btn-modal-subir');
                    btn_modal_sub.click();
                }
                function descarga_docs(e){
                    var url = "/json/documentos_solicitud/" + e.id;
                    $.getJSON(url, function(data) {
                        $("#doc_table tbody").empty();
                        docs = data;
                        $.each(data, function (i, object) {
                            $fila = "<tr>";
                            $fila += "<td>"+object.id+"</td>";
                            $fila += "<td>"+object.direccion+"</td>";
                            $fila += "<td>";
                            $fila += "<a href='/empresas/"+object.empresa_slug+"/"+object.gestion+"/solicitud/"+object.direccion+"' download='"+object.direccion+"'><i class='fa fa-download icono-opciones icono-blue' title='Descargar Documento'></i></a>";
                            $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-file-text icono-opciones icono-green' onclick='ver_documento(this)' data='"+object.direccion+"' title='Ver Documento'></a>";
                            $fila += "</td>";
                            $fila += "</tr>";
                            $("#doc_table tbody").append($fila);
                        });
                    });
                    var btn_modal_des = document.getElementById('btn-modal-descarga');
                    btn_modal_des.click();
                }

                function ver_documento(e){
                }

                function seleccion_solicitud(e){
                    var mod_tam = (e.id).length;
                    var id_fila = (e.id).substring(9, mod_tam);
                    document.getElementById('modal-solicitante').innerHTML = document.getElementById('usuario_'+id_fila).innerHTML;
                    document.getElementById('modal-proyecto').innerHTML = document.getElementById('proyecto_'+id_fila).innerHTML;
                    document.getElementById('modal-subproyecto').innerHTML = document.getElementById('nom_subproy_'+id_fila).innerHTML;
                    document.getElementById('modal-desembolso').innerHTML = document.getElementById('desembolso_'+id_fila).innerHTML;
                    document.getElementById('modal-cambio').innerHTML = document.getElementById('cambio_'+id_fila).innerHTML;
                    document.getElementById('modal-num').innerHTML = document.getElementById('solicitud_'+id_fila).innerHTML;
                    document.getElementById('modal-num-2').innerHTML = document.getElementById('solicitud_'+id_fila).innerHTML;
                    document.getElementById('modal-cambio').innerHTML = document.getElementById('cambio_'+id_fila).innerHTML;
                    document.getElementById('modal-modalidad').innerHTML = document.getElementById('tipo_'+id_fila).innerHTML;
                    document.getElementById('modal-moneda').innerHTML = document.getElementById('moneda_'+id_fila).innerHTML;
                    document.getElementById('modal-referencia').innerHTML = document.getElementById('referencia_'+id_fila).innerHTML;
                    document.getElementById('modal-observacion').innerHTML = document.getElementById('obs_'+id_fila).innerHTML;

                    var total = document.getElementById('total_'+id_fila).innerHTML;
                    var res = total.split(".");
                    var p_entera = res[0];
                    var p_decimal = res[1];
                    var str_decimal = '00';

                    //var str_literal = string_literal_conversion(p_entera);
                    var str_literal = numeroLiteral(p_entera);
                    document.getElementById('modal-literal').innerHTML = str_literal.toUpperCase();

                    if(p_decimal>0 && p_decimal <10){
                        str_decimal = '0'+str_decimal;
                    }else if(p_decimal > 9){
                        str_decimal = res[1];
                    }
                    document.getElementById('modal-decimal').innerHTML = str_decimal;
                    //str_items += "<tr><td class='text-right'>" + (cont_items) + "</td><td class='text-right'>" + td_can + "</td><td>" + td_uni + "</td><td>" + td_det + "</td><td class='text-right'>" + td_pre + "</td><td class='text-right'>" + td_sub + "</td></tr>";
                    var fila_sol = document.getElementById('solicitud_'+id_fila).innerHTML;

                    //item_table.style('display: block');
                    var url = "/json/items_solicitud/" + id_fila;
                    var total_items = 0;
                    $.getJSON(url, function(data) {
                        $("#table_items tbody").empty();
                        $.each(data, function (i, object) {
                            $fila = "<tr>";
                            $fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                            $fila += "<td>"+object.unidad_descripcion+"</td>";
                            $fila += "<td>"+object.detalle+"</td>";
                            $fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                            $fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                            $fila += "<td style='text-align:right;'>"+object.subtotal+"</td>";
                            $fila += "</tr>";
                            $("#table_items tbody").append($fila);
                            total_items += parseFloat(object.subtotal);
                        });
                        $fila = "<tr>";
                        $fila += "<td colspan='5' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                        $fila += "<td style='text-align:right;'>"+total_items.toFixed(2)+"</td>";
                        $fila += "</tr>";
                        $("#table_items tbody").append($fila);
                    });
                    var btn_modal = document.getElementById('btn-modal-solicitud');
                    btn_modal.click();
                    var span_p = document.getElementById('print-span');
                    span_p.innerHTML = "";
                    var file = document.createElement("button");
                    file.setAttribute("type", "button");
                    file.setAttribute("class", "btn btn-primary");
                    file.setAttribute("id", id_fila);
                    file.innerHTML = "IMPRIMIR";
                    file.setAttribute("onclick", "imprimirSolicitud(this)");
                    span_p.appendChild(file);
                }
            </script>
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
    {{ Html::script('/js/main.js') }}
    <script type="text/javascript">
        var config = {
            rutas:[
                {
                    showCorrespon: "{{route('documentos.showcr', ['id'=>':id'])}}",
                    descargar: "{{ route('doc.descargar', ['id'=>':id']) }}",
                    storage: "{{ asset('storage/archivo') }}",
                    token: "{{Session::token()}}"
                }
            ]
        };
    </script>
    {{Html::script('/js/docs/documento_solicitud.js')}}

@endsection
