@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #F39C12">
                    <div class="col-md-4">
                        <h2 style='color: #FFF'>Consultas Solicitudes Rendidas</h2>
                    </div>
                    @php
                        $array_docs_adjuntos = array();
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    <div class="col-md-8">
                        {!! Form::open(array('action' => 'SolicitudController@buscadorConsultor', 'method'=>'POST', 'class'=>'form-inline')) !!}
                            @include('solicitudes.partial.buscador')
                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered table-solicitudes table-hover">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>@sortablelink('estado', 'ESTADO')</th>
                                <th>@sortablelink('total', 'MONTO')</th>
                                <th>@sortablelink('proyecto_id', 'PROYECTO')</th>
                                <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                                <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $fecha_actual = new DateTime(\Carbon\Carbon::now());
                            @endphp
                            @foreach($solicitudes as $solicitud)
                            @php
                                $fecha_sol = new DateTime($solicitud->created_at);
                                $fecha_diff = $fecha_sol->diff($fecha_actual)->format("%a");
                                if($fecha_diff <= 7){
                                    $color = "#99ffbb";
                                }elseif($fecha_diff > 7 && $fecha_diff <=14){
                                    $color = "#ffffcc";
                                }elseif($fecha_diff > 14 && $fecha_diff <=30){
                                    $color = "#ffe6cc";
                                }elseif($fecha_diff > 30){
                                    $color = "#ffb3b3";
                                }
                            @endphp
                            <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)" style="background-color: {{ $color }}">
                                <td>
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                    <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud"></a>
                                    @if($solicitud->formulariosAF)
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text-o icono-opciones-files icono-green" onclick="formularios_solicitud(this)" title="Ver Formularios AF"></a>
                                    @endif
                                </td>
                                <td id="solicitud_{{ $solicitud->id }}">{{ $solicitud->numero }}</td>
                                <td>
                                    @if(sizeof($solicitud->documentos) > 0)
                                        <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                    @endif
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                        {{ $solicitud->estadoCodigo }}
                                    </span>
                                </td>
                                <td id="total_{{ $solicitud->id }}">{{ number_format($solicitud->total, 2, '.', ',') }}</td>
                                <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}"><i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i></td>
                                <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre }}</td>
                                <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                <td id="cambio_{{ $solicitud->id }}" style="text-align: right;">
                                    @if($solicitud->tipo_cambio->cambio != 1)
                                    {{ $solicitud->tipo_cambio->cambio }}
                                    @endif
                                </td>
                                <td id="moneda_{{ $solicitud->id }}">{{ $solicitud->moneda->nombre }}</td>
                                <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-solicitud-af" class="btn btn-success btn-block" data-toggle="modal" data-target="#formulariosSolicitud" style="display:none;">
                Modal solicitud
            </button>
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL -->
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL FORMULARIO AF-->
            @include('solicitudes.partial.formAF')
            <!-- END MODAL -->
        </div>
    </div>
</div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    function imprimirSolicitud(e){
        var id = e.id;
        //window.location.href = '/solicitud-imprimir?id_sol='+id;
    }
    </script>
@endsection