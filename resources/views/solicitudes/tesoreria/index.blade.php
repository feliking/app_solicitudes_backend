@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
@endsection

@section('content')
<div class="container" style="overflow-x: scroll;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background: #F39C12">
                    <div class="col-md-3">
                        <h2 style='color: #FFF'>Tesoreria</h2>
                    </div>
                    @php
                        $array_docs_adjuntos = array();
                        $contador = 1;
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    <div class="col-md-8">
                        {!! Form::open(array('action' => 'SolicitudController@buscadorTesoreria', 'method'=>'POST', 'class'=>'form-inline')) !!}
                            @include('solicitudes.partial.buscador')
                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered table-solicitudes table-hover">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>@sortablelink('estado', 'ESTADO')</th>
                                <th>@sortablelink('total', 'MONTO')</th>
                                <th>@sortablelink('proyecto_id', 'PROYECTO')</th>
                                <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $fecha_actual = new DateTime(\Carbon\Carbon::now());
                            @endphp
                            @foreach($solicitudes as $solicitud)
                            @php
                                $fecha_sol = new DateTime($solicitud->created_at);
                                $fecha_diff = $fecha_sol->diff($fecha_actual)->format("%a");
                                if($fecha_diff <= 7){
                                    $color = "#99ffbb";
                                }elseif($fecha_diff > 7 && $fecha_diff <=14){
                                    $color = "#ffffcc";
                                }elseif($fecha_diff > 14 && $fecha_diff <=30){
                                    $color = "#ffe6cc";
                                }elseif($fecha_diff > 30){
                                    $color = "#ffb3b3";
                                }
                            @endphp
                            <tr style="background-color: {{ $color }}">
                                <td>
                                    @if ($solicitud->modalidad_id == 5)
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud_transferencia(this)" title="Ver Solicitud"></a>
                                    @else
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                    @endif

                                    <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud" title="Historial Solicitud"></a>

                                    @if ($solicitud->modalidad_id != 5)
                                    <a class="fa fa-edit icono-opciones-files icono-green" href="{{ route('solicitud.edit', $solicitud->id) }}" title="Modificar Solicitud"></a>
                                    @endif

                                    @if ($solicitud->modalidad_id == 5)
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-check icono-opciones-files icono-green" onclick="registrar_solicitud_transferencia(this)" data-toggle="modal" data-id="{{ $solicitud->id }}" title="Registrar Solicitud"></a>
                                    @else
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-check icono-opciones-files icono-green" onclick="registrar_solicitud(this)" data-toggle="modal" data-id="{{ $solicitud->id }}" title="Registrar Solicitud"></a>
                                    @endif

                                    <i class="fa fa-times-circle-o icono-opciones-files icono-red" data-toggle="modal" data-target="#rechazoModal" onclick="datosRechazoModal({{ $solicitud->id }})" title="Rechazar Solicitud"></i>
                                </td>
                                <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                <td>
                                    @if(sizeof($solicitud->documentos) > 0)
                                        <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                    @endif
                                </td>

                                @php
                                    $label_color = 'label-default';
                                    if ($solicitud->modalidad_id == 5)
                                    {
                                        $label_color = 'label-info';
                                    }
                                @endphp
                                <td style="text-align: center;">
                                    <span class="label {{ $label_color }} label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                        {{ $solicitud->estadoCodigo }}
                                    </span>
                                </td>

                                <td id="total_{{ $solicitud->id }}" style="text-align: right;">{{ number_format($solicitud->total, 2, '.', ',') }}</td>
                                <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}"><i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i></td>
                                <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre }}</td>
                                <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                <td id="cambio_{{ $solicitud->id }}" style="text-align: right;">
                                    @if($solicitud->tipo_cambio->cambio != 1)
                                    {{ $solicitud->tipo_cambio->cambio }}
                                    @endif
                                </td>
                                <td id="moneda_{{ $solicitud->id }}"><b>{{ $solicitud->moneda->nombre }}</b></td>
                                <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-solicitud-transferencia" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModalTransferencia" style="display:none">
                    Modal solicitud transferecnia
                </button>
            <button type="button" id="btn-modal-aprobar" class="btn btn-success btn-block" data-toggle="modal" data-target="#autorizarModal" style="display:none;">
                Modal aprobar
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL -->
            @include('solicitudes.partial.datosSolicitud')
            @include('solicitudes.partial.datosSolicitudTransferencia')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL RECHAZO-->
            <div class="modal fade" id="rechazoModal" tabindex="-1" role="dialog" aria-labelledby="rechazoModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><strong>PROYECTO: </strong></td>
                                            <td colspan="7" id='modalrechazo-proyecto'></td>
                                            <td><strong>FECHA LÍMITE: </strong></td>
                                            <td id="modalrechazo-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><strong>SOLICITANTE: </strong></td>
                                            <td colspan="9" id="modalrechazo-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><strong>DESEMBOLSO A: </strong></td>
                                            <td colspan="9" id='modalrechazo-desembolso'></td>
                                        </tr>
                                        <tr id="tr_con_tipo_rec">
                                            <td><strong>TIPO DE CAMBIO: </strong></td>
                                            <td id='modalrechazo-cambio'></td>
                                            <td><strong>MODALIDAD: </strong></td>
                                            <td colspan="3" id='modalrechazo-modalidad'></td>
                                            <td><strong>MONEDA: </strong></td>
                                            <td colspan="3" id='modalrechazo-moneda'></td>
                                        </tr>
                                        <tr id="tr_sin_tipo_rec">
                                            <td><label >MODALIDAD: </label></td>
                                            <td colspan="4" id='modalrechazo-modalidad-sin'></td>
                                            <td><label >MONEDA: </label></td>
                                            <td colspan="4" id='modalrechazo-moneda-sin'></td>
                                        </tr>
                                        <tr>
                                            <td><strong>REFERENCIA: </strong></td>
                                            <td colspan="9" id='modalrechazo-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><strong>JUSTIFICACIÓN: </strong></td>
                                            <td colspan="9" id='modalrechazo-observacion'></td>
                                        </tr>
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-total'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>ITEMS SOLICITUD</h4>
                                        <table class="table table-bordered table-items" id="table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th style="width:20px">TIPO COMPRA</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal-rechazo"></span> CON <span id="modal-decimal-rechazo"></span>/100 <span id="modalrechazo-literal-moneda"></span></p>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_id')) !!}
                                    {!! Form::hidden('tipo', 'tesoreria', array('placeholder' => '','class' => 'form-control')) !!}
                                    
                                    {!! Form::textarea('motivo', null, ['placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform:uppercase', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-primary" onclick="rechazo()">RECHAZAR SOLICITUD</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- AUTORIZAR MODAL -->
            <div class="modal fade" id="autorizarModal" tabindex="-1" role="dialog" aria-labelledby="autorizarModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>DETALLE SOLICITUD123</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default " style="overflow-x: scroll;">
                                <table class="table table-bordered" id="aprobar-solicitud">
                                    <tr>
                                        <td><strong>PROYECTO: </strong></td>
                                        <td colspan="7" id='modalautorizar-proyecto'></td>
                                        <td><strong for="turno">FECHA LÍMITE: </strong></td>
                                        <td id="modalautorizar-fecha-lim"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>SOLICITANTE: </strong></td>
                                        <td colspan="9" id="modalautorizar-solicitante"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>DESEMBOLSO A: </strong></td>
                                        <td colspan="9" id='modalautorizar-desembolso'></td>
                                    </tr>
                                    <tr>
                                        <td><strong>TIPO DE CAMBIO: </strong></td>
                                        <td id='modalautorizar-cambio'></td>
                                        <td><strong>MODALIDAD: </strong></td>
                                        <td colspan="3" id='modalautorizar-modalidad'></td>
                                        <td><strong>MONEDA: </strong></td>
                                        <td colspan="3" id='modalautorizar-moneda'></td>
                                    </tr>
                                    <tr>
                                        <td><strong>REFERENCIA: </strong></td>
                                        <td colspan="9" id='modalautorizar-referencia'></td>
                                    </tr>
                                    <tr>
                                        <td><strong>JUSTIFICACIÓN: </strong></td>
                                        <td colspan="9" id='modalautorizar-observacion'></td>
                                    </tr>
                                    <tr class="no-print">
                                        <td class="text-right"><strong>TOTAL</strong></td>
                                        <td colspan="9" id='modalautorizar-total'></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ITEMS SOLICITUD</h2></div>
                                <div class="panel-body panel-items">
                                    @foreach($items as $key => $item_table)
                                        <table class="table table-hover table-bordered" id="items_aprob_{{ $key }}" style="display:none">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            @php
                                                $con_filas = 1;
                                                $suma_total = 0.00;
                                            @endphp
                                            @foreach($item_table as $curr_sol)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $curr_sol['tipo'] }}</td>
                                                    <td>{{ $curr_sol['descripcion'] }}</td>
                                                    <td style="text-align: right;">{{ $curr_sol['cantidad'] }}</td>
                                                    <td style="text-align: right;">{{ $curr_sol['costo'] }}</td>
                                                    <td style="text-align: right;">{{ $curr_sol['subtotal'] }}</td>
                                                </tr>
                                                @php
                                                    $suma_total +=$curr_sol['subtotal'];
                                                    $con_filas++;
                                                @endphp
                                            @endforeach
                                            <tr>
                                                <td colspan="5" style="text-align: right;"><strong>TOTAL</strong></td>
                                                <td style="text-align: right;">
                                                    {{ number_format($suma_total, 2, '.', ',') }}
                                                </td>
                                            </tr>
                                            </body>
                                        </table>
                                    @endforeach
                                    <p><strong>SON:</strong> <span id="modal-literal-aprobar"></span> CON <span id="modal-decimal-aprobar"></span>/100 BOLIVIANOS</p>
                                </div>
                            </div>
                            <div class="modal-footer"  id="modalautorizaropciones">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->

            <!-- MODAL SUBIR DOCS-->
            <div class="modal fade" id="subirDocsModal" tabindex="-1" role="dialog" aria-labelledby="subirDocsModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>SUBIR DOCUMENTOS SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                                <div class="panel-body">
                                    {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                                        {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                                        <table class="table-modal-subir-archivo">
                                            <tr>
                                                <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                                <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                            </tr>
                                        </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
        </div>
    </div>
</div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    function registroTesoreria(e){
        var id = e.id;
        //window.location.href = '/solicitud-imprimir?id_sol='+id;
    }
    function datosRechazoModal(fila){
        document.getElementById('tr_con_tipo_rec').style.display = 'contents';
        document.getElementById('tr_sin_tipo_rec').style.display = 'none';

        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalrechazo-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalrechazo-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalrechazo-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalrechazo-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalrechazo-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        if(document.getElementById('moneda_'+fila).innerHTML == 'BOLIVIANOS'){
            document.getElementById('tr_con_tipo_rec').style.display = 'none';
            document.getElementById('tr_sin_tipo_rec').style.display = 'contents';
        }
        document.getElementById('modalrechazo-modalidad-sin').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda-sin').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalrechazo-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalrechazo-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('rechazoModalLabel').innerHTML = "<strong>RECHAZAR SOLICITUD# "+numeroSolicitud+"</strong>";
        document.getElementById('sol_id').value = fila;
        document.getElementById('modalrechazo-literal-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        var url = "/json/items_solicitud/" + fila;

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';
        if(p_decimal){
            str_decimal = p_decimal;
        }
        document.getElementById('modal-decimal-rechazo').innerHTML = str_decimal;

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-rechazo').innerHTML = str_literal.toUpperCase();

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalrechazo-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        $('textarea[name="motivo"]').val('');
    }
    function autorizarModal(fila){
        document.getElementById('modalautorizar-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalautorizar-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
        document.getElementById('modalautorizar-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalautorizar-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalautorizar-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalautorizar-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalautorizar-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalautorizar-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;

        var span_p = document.getElementById('modalautorizaropciones');
        $("#modalautorizaropciones").empty();
        var btnCerrar = document.createElement("button");
        btnCerrar.setAttribute("type", "button");
        btnCerrar.setAttribute("class", "btn btn-default");
        btnCerrar.setAttribute("data-dismiss", "modal");
        btnCerrar.innerHTML="CERRAR";
        span_p.appendChild(btnCerrar);

        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "AUTORIZAR";
        file.setAttribute("onclick", "autorizarSolicitud(this)");
        span_p.appendChild(file);

        for (i = 0; i < 5000; i++) {
            if(document.getElementById('items_aprob_'+i)){
                document.getElementById('items_aprob_'+i).style.display = "none";
            }
        }
        var fila_sol = document.getElementById('solicitud_'+fila).innerHTML;
        document.getElementById('items_aprob_'+fila).setAttribute('style', 'display: inline-table');

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        //var str_literal = string_literal_conversion(p_entera);
        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-aprobar').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-aprobar').innerHTML = str_decimal;

    }

    function registrar_solicitud(e){
        var mod_tam = (e.id).length;
        var fila = (e.id).substring(9, mod_tam);
        var id_fila = fila;
        document.getElementById('modal-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modal-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modal-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modal-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modal-numero').innerHTML = document.getElementById('solicitud_'+fila).innerHTML;
        document.getElementById('modal-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modal-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modal-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modal-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        seleccion_solicitud(e);

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        //var str_literal = string_literal_conversion(p_entera);
        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal').innerHTML = str_literal.toUpperCase();

        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal').innerHTML = str_decimal;

        var url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        var span_p = document.getElementById('print-span');
        span_p.innerHTML = "";
        var file = document.createElement("a");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "REGISTRO TESORERIA";
        file.setAttribute("href", "registro-tesoreria/"+fila);
        span_p.appendChild(file);
    }

    function registrar_solicitud_transferencia(e){
        var mod_tam = (e.id).length;
        var fila = (e.id).substring(9, mod_tam);
        var id_fila = fila;
        document.getElementById('modal-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modal-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modal-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modal-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modal-numero').innerHTML = document.getElementById('solicitud_'+fila).innerHTML;
        document.getElementById('modal-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modal-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modal-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modal-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        
        seleccion_solicitud_transferencia(e);

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        //var str_literal = string_literal_conversion(p_entera);
        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal').innerHTML = str_literal.toUpperCase();

        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal').innerHTML = str_decimal;

        var span_p = document.getElementById('print-trs-span');
        span_p.innerHTML = "";
        var file = document.createElement("a");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "REGISTRO TESORERIA";
        file.setAttribute("href", "registro-tesoreria/"+fila);
        span_p.appendChild(file);
    }
    
    function rechazo(){
    }
</script>
@endsection