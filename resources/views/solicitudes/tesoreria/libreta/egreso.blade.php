@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h2>EGRESO</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'egreso.store', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
                    {{ Form::hidden('movimiento_id', 1) }}
                    <div class="col-md-12">
                        <h2>Cuenta Origen</h2>
                        <div class="col-md-4">
                            <label>Empresa:</label>
                            <select id="origen_empresa" name="origen_empresa" class="form-control form-admin" onchange="bancosOrigen()">
                                @foreach($empresas as $key => $empresa)
                                    <option value='{{ $key }}'>{{ $empresa }}</option>
                                @endforeach
                                <option value='0'>OTROS</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Banco:</label>
                            <select id="origen_banco" name="origen_banco" class="form-control" onchange="cuentasOrigen()"></select>
                        </div>
                        <div class="col-md-4">
                            <label>Cuenta:</label>
                            <select id="origen_cuenta" name="origen_cuenta" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Fecha</label>
                            <input id="fecha" type="date" class="form-control" name="fecha" required>
                        </div>
                        <div class="col-md-3">
                            <label>Monto</label>
                            {{ Form::number('monto', null, array('id' => 'monto', 'placeholder' => 'MONTO DE EGRESO', 'class' => 'form-control', 'required' => 'true', 'step' => '0.01')) }}
                        </div>
                        <div class="col-md-6">
                            <label>Concepto</label>
                            {{ Form::text('concepto', null, array('id' => 'concepto', 'placeholder' => 'CONCEPTO', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase')) }}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Tipo de cambio</label>
                            <input id="cambio" type="number" class="form-control" name="cambio" step="0.01" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4 col-md-offset-4">
                            {{ Form::button('ACEPTAR', array('class' => 'btn btn-primary btn-block', 'onclick' => 'modalTransferencia()')) }}
                            {!! Form::submit('REALIZAR TRANSFERENCIA', ['class' => 'btn btn-success btn-block', 'id' => 'btn-registro-solicitud', 'style' => 'display:none']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL PREVIEW DOCS-->
<div class="modal fade" id="transferenciaModal" tabindex="-1" role="dialog" aria-labelledby="transferenciaModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success"><strong>CONFIRMAR DATOS TRANSFERENCIA</strong></h4>
            </div>
            <div class="modal-body">
                <div id="transferenciaDiv"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                <button type="button" class="btn btn-primary" onclick="guardarTransferencia()">REALIZAR TRANSFERENCIA</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
$(document).ready ( function(){
    var url = "/json/tipo_cambio";
    var cad = "";
    $.getJSON(url, function(data) {
        document.getElementById('cambio').value = data.cambio;
    });
});
function modalTransferencia(){
    var monto = document.getElementById('monto').value;
    if(monto > 0){
        $('#transferenciaModal').modal('toggle');
        var tabla = document.getElementById('transferenciaDiv');
        var ele = document.getElementById('origen');
        var origen = ele.options[ele.selectedIndex].innerHTML;
        ele = document.getElementById('destino');
        var destino = ele.options[ele.selectedIndex].innerHTML;

        var cadena = "<table class='table'><tr>";
        cadena += "<td>ORIGEN</td>";
        cadena += "<td>"+origen+"</td>";
        cadena += "</tr><tr>";
        cadena += "<td>DESTINO</td>";
        cadena += "<td>"+destino+"</td>";
        cadena += "</tr><tr>";
        cadena += "<td>MONTO</td>";
        cadena += "<td>"+monto+"</td>";
        cadena += "</tr></table";
        tabla.innerHTML = cadena;
    }else{

    }
}

function guardarTransferencia(){
    document.getElementById('btn-registro-solicitud').click();
}

function bancosOrigen(){
    var e = document.getElementById("origen_empresa");
    var valor = e.options[e.selectedIndex].value;

    if(valor != 0){
        var url = "/json/bancos/empresa/" + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('origen_banco').innerHTML = '';
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+">"+object.nombre+"</option>";
            });
            document.getElementById('origen_banco').innerHTML = cad;
        });

        document.getElementById("origen_banco").disabled = false;
    }else{
        document.getElementById("origen_banco").disabled = true;
        document.getElementById('origen_banco').innerHTML = "<option value='0'>NINGUNO</option>";
    }
}

function cuentasOrigen(){
    var e = document.getElementById("origen_banco");
    var valor = e.options[e.selectedIndex].value;

    if(valor != 0){
        var url = "/json/cuentas/empresa/" + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('origen_cuenta').innerHTML = '';
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+">"+object.numero+" ["+object.moneda_texto+"]</option>";
            });
            document.getElementById('origen_cuenta').innerHTML = cad;
        });

        document.getElementById("origen_cuenta").disabled = false;
    }else{
        document.getElementById("origen_cuenta").disabled = true;
        document.getElementById('origen_cuenta').innerHTML = "<option value='0'>NINGUNO</option>";
    }
}

function bancosDestino(){
    var e = document.getElementById("destino_empresa");
    var valor = e.options[e.selectedIndex].value;

    if(valor != 0){
        var url = "/json/bancos/empresa/" + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('destino_banco').innerHTML = '';
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+">"+object.nombre+"</option>";
            });
            document.getElementById('destino_banco').innerHTML = cad;
        });

        document.getElementById("destino_banco").disabled = false;
    }else{
        document.getElementById("destino_banco").disabled = true;
        document.getElementById('destino_banco').innerHTML = "<option value='0'>NINGUNO</option>";
    }
}

function cuentasDestino(){
    var e = document.getElementById("destino_banco");
    var valor = e.options[e.selectedIndex].value;

    if(valor != 0){
        var url = "/json/cuentas/empresa/" + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('destino_cuenta').innerHTML = '';
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+">"+object.numero+" ["+object.moneda_texto+"]</option>";
            });
            document.getElementById('destino_cuenta').innerHTML = cad;
        });

        document.getElementById("destino_cuenta").disabled = false;
    }else{
        document.getElementById("destino_cuenta").disabled = true;
        document.getElementById('destino_cuenta').innerHTML = "<option value='0'>NINGUNO</option>";
    }
}
</script>
@endsection