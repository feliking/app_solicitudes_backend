@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h2>TRANSFERENCIA</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'transferencia.store', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
                    {{ Form::hidden('movimiento_id', 1) }}
                    <div class="col-md-12">
                        <h2>Cuenta Origen</h2>
                        <div class="col-md-4">
                            <label>Empresa:</label>
                            <select id="origen_empresa" name="origen_empresa" class="form-control form-admin" onchange="bancosOrigen()">
                                <option value="">SELECCIONE UNA EMPRESA</option>
                                @foreach($empresas as $key => $empresa)
                                    <option value='{{ $key }}'>{{ $empresa }}</option>
                                @endforeach
                                <option value='0'>OTROS</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Banco:</label>
                            <select id="origen_banco" name="origen_banco" class="form-control" onchange="cuentasOrigen()"></select>
                        </div>
                        <div class="col-md-4">
                            <label>Cuenta:</label>
                            <select id="origen_cuenta" name="origen_cuenta" class="form-control" onchange="cuentaOrigenSelect()"></select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h2>Cuenta Destino</h2>
                        <div class="col-md-4">
                            <label>Empresa:</label>
                            <select id="destino_empresa" name="destino_empresa" class="form-control form-admin" onchange="bancosDestino()">
                                <option value="">SELECCIONE UNA EMPRESA</option>
                                @foreach($empresas as $key => $empresa)
                                    <option value='{{ $key }}'>{{ $empresa }}</option>
                                @endforeach
                                <option value='0'>OTROS</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Banco:</label>
                            <select id="destino_banco" name="destino_banco" class="form-control" onchange="cuentasDestino()"></select>
                        </div>
                        <div class="col-md-4">
                            <label>Cuenta:</label>
                            <select id="destino_cuenta" name="destino_cuenta" class="form-control" onchange="cuentaDestinoSelect()"></select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h2>Descripción</h2>
                        <div class="col-md-4">
                            <label>Fecha</label>
                            <input id="fecha" type="date" class="form-control" name="fecha" required>
                        </div>
                        <div class="col-md-4">
                            <label>Monto</label>
                            {{ Form::number('monto', null, array('id' => 'monto', 'placeholder' => 'MONTO DE TRANSFERENCIA', 'class' => 'form-control', 'required' => 'true', 'step' => '0.01')) }}
                        </div>
                        <div id="tipo_de_cambio" class="col-md-4" style="display: none;">
                            <label>Tipo de cambio</label>
                            <input id="cambio" type="number" class="form-control" name="cambio" step="0.01" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <label>Concepto</label>
                            {{ Form::text('concepto', null, array('id' => 'concepto', 'placeholder' => 'CONCEPTO', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase')) }}
                        </div>
                        <div class="col-md-4">
                            <br>
                            {{ Form::button('ACEPTAR', array('class' => 'btn btn-primary btn-block', 'onclick' => 'modalTransferencia()')) }}
                            {!! Form::submit('REALIZAR TRANSFERENCIA', ['class' => 'btn btn-success btn-block', 'id' => 'btn-registro-solicitud', 'style' => 'display:none']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL PREVIEW DOCS-->
<div class="modal fade" id="transferenciaModal" tabindex="-1" role="dialog" aria-labelledby="transferenciaModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success"><strong>CONFIRMAR DATOS TRANSFERENCIA</strong></h4>
            </div>
            <div class="modal-body">
                <div id="transferenciaDiv"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                <button type="button" class="btn btn-primary" onclick="guardarTransferencia()">REALIZAR TRANSFERENCIA</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
$(document).ready ( function(){
    var url = "/json/tipo_cambio";
    var cad = "";
    $.getJSON(url, function(data) {
        document.getElementById('cambio').value = data.cambio;
    });
});

var moneda_origen = 0;
var moneda_destino = 0;

function cuentaOrigenSelect(){
    var e = document.getElementById("origen_cuenta");
    moneda_origen = e.options[e.selectedIndex].dataset.moneda;
}

function cuentaDestinoSelect(){
    var e = document.getElementById("destino_cuenta");
    moneda_destino = e.options[e.selectedIndex].dataset.moneda;

    if(moneda_origen == moneda_destino){
        $('div#tipo_de_cambio').css('display', 'none');
    }else{
        var url = "/json/tipo_cambio?monedaId=" + moneda_destino;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('cambio').value = data.cambio;
        }).done(function(data){
            $('div#tipo_de_cambio').css('display', 'block');
        });
    }
}

function modalTransferencia(){
    var monto = document.getElementById('monto').value;
    if(monto > 0){
        $('#transferenciaModal').modal('toggle');
        var tabla = document.getElementById('transferenciaDiv');
        var ele = document.getElementById('origen_banco');
        var banco_origen = ele.options[ele.selectedIndex].innerHTML;
        ele = document.getElementById('origen_empresa');
        var empresa_origen = ele.options[ele.selectedIndex].innerHTML;
        ele = document.getElementById('origen_cuenta');
        var cuenta_origen = ele.options[ele.selectedIndex].innerHTML;

        ele = document.getElementById('destino_banco');
        var banco_destino = ele.options[ele.selectedIndex].innerHTML;
        ele = document.getElementById('destino_empresa');
        var empresa_destino = ele.options[ele.selectedIndex].innerHTML;
        ele = document.getElementById('destino_cuenta');
        var cuenta_destino = ele.options[ele.selectedIndex].innerHTML;

        var fecha = document.getElementById('fecha').value;
        var concepto = document.getElementById('concepto').value.toUpperCase();

        var cadena = "<table class='table'><tr>";
        cadena += "<td><strong>ORIGEN</strong></td>";
        cadena += "<td>"+empresa_origen+"</td>";
        cadena += "<td>"+banco_origen+"</td>";
        cadena += "<td>"+cuenta_origen+"</td>";
        cadena += "</tr><tr>";
        cadena += "<td><strong>DESTINO</strong></td>";
        cadena += "<td>"+empresa_destino+"</td>";
        cadena += "<td>"+banco_destino+"</td>";
        cadena += "<td>"+cuenta_destino+"</td>";
        cadena += "</tr><tr>";
        cadena += "<td><strong>FECHA</strong></td>";
        cadena += "<td>"+fecha+"</td>";
        cadena += "<td><strong>MONTO<strong></td>";
        cadena += "<td>"+monto+"</td>";
        cadena += "</tr><tr>";
        cadena += "<td><strong>CONCEPTO</strong></td>";
        cadena += "<td colspan=3>"+concepto+"</td>";
        cadena += "</tr></table>";
        tabla.innerHTML = cadena;
    }else{

    }
}

function guardarTransferencia(){
    document.getElementById('btn-registro-solicitud').click();
}

var empresaOrigenSelect = 0;

function bancosOrigen(){
    var e = document.getElementById("origen_empresa");
    var valor = e.options[e.selectedIndex].value;

    empresaOrigenSelect = valor;

    if(valor != 0){
        var url = "/json/bancos/empresa/" + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('origen_banco').innerHTML = '';
            cad += "<option value=''>SELECCIONE UN BANCO</option>";
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+">"+object.nombre+"</option>";
            });
            document.getElementById('origen_banco').innerHTML = cad;
        });

        document.getElementById("origen_banco").disabled = false;
    }else{
        document.getElementById("origen_banco").disabled = true;
        document.getElementById('origen_banco').innerHTML = "<option value='0'>NINGUNO</option>";
        document.getElementById("origen_cuenta").disabled = true;
    }
}

function cuentasOrigen(){
    var e = document.getElementById("origen_banco");
    var valor = e.options[e.selectedIndex].value;

    if(valor != 0){
        var url = "/json/cuentas/empresa/" + empresaOrigenSelect + '/' + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('origen_cuenta').innerHTML = '';
            cad += "<option value=''>SELECCIONE UNA CUENTA</option>";
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+" data-moneda="+object.moneda.id+">"+object.numero+" ["+object.moneda_texto+"]</option>";
            });
            document.getElementById('origen_cuenta').innerHTML = cad;
        });

        document.getElementById("origen_cuenta").disabled = false;
    }else{
        document.getElementById("origen_cuenta").disabled = true;
        document.getElementById('origen_cuenta').innerHTML = "<option value='0'>NINGUNO</option>";
    }
}

var empresaDestinoSelect = 0;

function bancosDestino(){
    var e = document.getElementById("destino_empresa");
    var valor = e.options[e.selectedIndex].value;

    empresaDestinoSelect = valor;

    if(valor != 0){
        var url = "/json/bancos/empresa/" + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('destino_banco').innerHTML = '';
            cad += "<option value=''>SELECCIONE UN BANCO</option>";
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+">"+object.nombre+"</option>";
            });
            document.getElementById('destino_banco').innerHTML = cad;
        });

        document.getElementById("destino_banco").disabled = false;
    }else{
        document.getElementById("destino_banco").disabled = true;
        document.getElementById('destino_banco').innerHTML = "<option value='0'>NINGUNO</option>";
        document.getElementById("destino_cuenta").disabled = true;
    }
}

function cuentasDestino(){
    var e = document.getElementById("destino_banco");
    var valor = e.options[e.selectedIndex].value;

    if(valor != 0){
        var url = "/json/cuentas/empresa/" + empresaDestinoSelect + '/' + valor;
        var cad = "";
        $.getJSON(url, function(data) {
            document.getElementById('destino_cuenta').innerHTML = '';
            cad += "<option value=''>SELECCIONE UNA CUENTA</option>";
            $.each(data, function (i, object) {
                cad += "<option value="+object.id+" data-moneda="+object.moneda.id+">"+object.numero+" ["+object.moneda_texto+"]</option>";
            });
            document.getElementById('destino_cuenta').innerHTML = cad;
        });

        document.getElementById("destino_cuenta").disabled = false;
    }else{
        document.getElementById("destino_cuenta").disabled = true;
        document.getElementById('destino_cuenta').innerHTML = "<option value='0'>NINGUNO</option>";
    }
}
</script>
@endsection