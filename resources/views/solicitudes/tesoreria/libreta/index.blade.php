@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-6">
                        <h2>Libreta Bancaria - Cuentas</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    @foreach($bancos as $banco)
                    <div class="col-md-4">
                        <div class="panel panel-default panel-cuenta-resumen">
                            <div class="panel-heading panel-cuenta-resumen-header">{{ $banco['nombre'] }}</div>
                            <div class="panel-body">
                                @foreach($banco['cuentas'] as $cuenta)
                                    <div class="panel-cuenta-resumen-body">
                                        <h2>Nro. de cuenta:{{ $cuenta['numero'] }}</h2>
                                        <h3>Moneda: {{ ucfirst(strtolower($cuenta['moneda'])) }}</h3>
                                        <div class="container">
                                            <table class="table table-hover">
                                                <tbody>
                                                    <tr>
                                                        <td>CHEQUES EMITIDOS</td>
                                                        <td style="text-align: center;">{{ $cuenta['cantidad_cheques'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>INGRESOS</td>
                                                        <td style="text-align: right;">{{ number_format($cuenta['monto_ingreso'], '2', '.', ',') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>EGRESOS</td>
                                                        <td style="text-align: right;">{{ number_format($cuenta['monto_egreso'], '2', '.', ',') }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>SALDOS</td>
                                                        <td style="text-align: right;">{{ number_format(($cuenta['monto_inicial'] + $cuenta['monto_ingreso'] - $cuenta['monto_egreso']), '2', '.', ',') }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">

</script>
@endsection