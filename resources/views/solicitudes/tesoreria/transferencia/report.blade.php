@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>
                            Reporte Consolidado de Transferencias
                            @php
                            if(isset( $filters['origen_destino_empresa_id']))
                                $odempresa = $filters['origen_destino_empresa_id'];
                            else
                                $odempresa = 0;

                            if(isset( $filters['empresa_id']))
                                $fempresa = $filters['empresa_id'];
                            else
                                $fempresa = session('empresa_nombre');
                            @endphp
                            <input type="text" name="exsol_id" value="{{ $odempresa }}" style="display: none;">
                            <input type="text" name="emp_id" value="{{ $fempresa }}" style="display: none;">
                            <div class="crear_empresa pull-right">
                                <a class="btn btn-success" id="btn-excel"><span class="fa fa-file-excel-o"></span> EXPORTAR</a>
                                <a target="_blank" class="btn btn-primary" id="btn-pdf"><span class="fa fa-file-pdf-o"></span> EXPORTAR</a>
{{--                                <a class="btn btn-success" href="{{ route('solicitud.tesoreria.transferencia.report_excel') }}">Exportar Excel</a>--}}
{{--                                <a class="btn btn-danger" href="{{ route('solicitud.tesoreria.transferencia.report_excel') }}">Exportar PDF</a>--}}
                            </div>
                        </h2>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    
                    {!! Form::open(['action' => 'TransferenciaController@report', 'method'=>'POST', 'class'=>'form-inline', 'id' => 'frm_sol_admin']) !!}
                        @include('solicitudes.tesoreria.transferencia.filtros')
                    {!! Form::close() !!}

                    <div class="div-solicitudes" style="overflow-x: scroll;">
                        {{-- Total --}}
                        @if (session('empresa_nombre') != null || (isset($filters['empresa_id']) && $filters['empresa_id'] != 0))
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            @if (isset($filters['empresa_id']) && $filters['empresa_id'] != 0)
                                            <th colspan="7" style="font-size: 20px; text-align: center;">{{ \App\Empresas::find($filters['empresa_id'])->nombre }}</th>
                                            @else
                                            <th colspan="7" style="font-size: 20px; text-align: center;">{{ session('empresa_nombre') }}</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th>TOTAL INGRESOS</th>
                                            <th style="width: 5%;">BS</th>
                                            <td id="total_in_0" style="width: 15%; text-align: right;"></td>
                                            <th style="width: 5%;">$US</th>
                                            <td id="total_in_1" style="width: 15%; text-align: right;"></td>
                                            <th style="width: 5%;">€</th>
                                            <td id="total_in_2" style="width: 15%; text-align: right;"></td>
                                        </tr>
                                        <tr>
                                            <th>TOTAL EGRESOS</th>
                                            <th style="width: 5%;">BS</th>
                                            <td id="total_out_0" style="width: 15%; text-align: right;"></td>
                                            <th style="width: 5%;">$US</th>
                                            <td id="total_out_1" style="width: 15%; text-align: right;"></td>
                                            <th style="width: 5%;">€</th>
                                            <td id="total_out_2" style="width: 15%; text-align: right;"></td>
                                        </tr>
                                        <tr>
                                            <th>TOTAL GENERAL</th>
                                            <th style="width: 5%;">BS</th>
                                            <td id="total_general_0" style="width: 15%; text-align: right;"></td>
                                            <th style="width: 5%;">$US</th>
                                            <td id="total_general_1" style="width: 15%; text-align: right;"></td>
                                            <th style="width: 5%;">€</th>
                                            <td id="total_general_2" style="width: 15%; text-align: right;"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif

                        @php
                            $total_ingresos_global = array(0, 0, 0); // Bs, $ y €
                            $total_egresos_global = array(0, 0, 0); // Bs, $ y €
                        @endphp
                        @foreach ($cuentas_disponibles->chunk(2) as $chunks)
                        <div class="row">
                        @foreach ($chunks as $cuenta_origen)
                        @php
                            $total_egresos = 0;
                            $total_ingresos = 0;
                        @endphp
                        <div class="col-md-6">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th colspan="5" style="font-size: 20px; text-align: center;">
                                        {{ $cuenta_origen->banco->nombre }} - CTA. {{ $cuenta_origen->numero }} [{{ $cuenta_origen->moneda->sigla }}]
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th colspan="5" style="text-align: center; font-size: 18px;">Ingresos</th>
                                </tr>
                                <tr>
                                    <th style="width: 10%;">#</th>
                                    <th style="width: 15%;">Tipo</th>
                                    <th style="width: 15%;">Monto</th>
                                    <th colspan="2" style="width: 60%;">Origen</th>
                                </tr>
                                @php
                                    if (isset($filters['origen_destino_empresa_id']) && $filters['origen_destino_empresa_id'] != 0)
                                    {
                                        $transferencias = \App\Transferencia::join('cuenta_bancarias', 'transferencias.cuenta_origen_id', '=', 'cuenta_bancarias.id')
                                            ->where([
                                                ['transferencias.aprobado', '=', 1],
                                                ['transferencias.cuenta_destino_id', '=', $cuenta_origen->id],
                                                ['cuenta_bancarias.empresa_id', '=', $filters['origen_destino_empresa_id']]
                                            ])
                                            ->select('transferencias.*')
                                            ->orderBy('transferencias.created_at', 'ASC')
                                            ->get();
                                    }
                                    else
                                    {
                                        $transferencias = \App\Transferencia::where([
                                            ['aprobado', '=', 1],
                                            ['cuenta_destino_id', '=', $cuenta_origen->id]
                                        ])
                                            ->orderBy('created_at', 'ASC')
                                            ->get();
                                    }
                                @endphp

                                @if (count($transferencias) > 0)
                                @foreach ($transferencias as $transferencia)
                                @php
                                    $cuenta_o = $transferencia->cuentaOrigen;
                                    $banco_origen = $cuenta_o->banco;

                                    $empresa_origen = $cuenta_o->empresa;

                                    $total_ingresos += $transferencia->monto * $transferencia->factor;

                                    switch ($cuenta_origen->moneda_id) {
                                        case 1:
                                            // Bolivianos
                                            $total_ingresos_global[0] += $total_ingresos;
                                            break;
                                        case 2:
                                            // Dólares
                                            $total_ingresos_global[1] += $total_ingresos;
                                            break;
                                        case 3:
                                            // Euros
                                            $total_ingresos_global[2] += $total_ingresos;
                                            break;
                                    }
                                @endphp
                                <tr>
                                    <td style="width: 10%;">{{ $loop->iteration }}</td>
                                    <td style="width: 15%;">{{ $transferencia->tipoTransferencia->descripcion }}</td>
                                    <td style="width: 15%;">{{ $cuenta_origen->moneda->sigla }} {{ number_format($transferencia->monto * $transferencia->factor, 2, '.', ',') }}</td>
                                    <td style="width: 30%;">{{ $empresa_origen->nombre }}</td>
                                    <td style="width: 30%;">{{ $banco_origen->nombre }} - CTA. {{ $cuenta_o->numero }} [{{ $cuenta_o->moneda->sigla }}]</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5">Sin movimientos registrados</td>
                                </tr>
                                @endif

                                <tr>
                                    <th colspan="5" style="text-align: center; font-size: 18px;">Egresos</th>
                                </tr>
                                <tr>
                                    <th style="width: 10%;">#</th>
                                    <th style="width: 15%;">Tipo</th>
                                    <th style="width: 15%;">Monto</th>
                                    <th colspan="2" style="width: 60%;">Destino</th>
                                </tr>
                                @php
                                    if (isset($filters['origen_destino_empresa_id']) && $filters['origen_destino_empresa_id'] != 0)
                                    {
                                        $transferencias = \App\Transferencia::join('cuenta_bancarias', 'transferencias.cuenta_destino_id', '=', 'cuenta_bancarias.id')
                                            ->where([
                                                ['transferencias.aprobado', '=', 1],
                                                ['transferencias.cuenta_origen_id', '=', $cuenta_origen->id],
                                                ['cuenta_bancarias.empresa_id', '=', $filters['origen_destino_empresa_id']]
                                            ])
                                            ->select('transferencias.*')
                                            ->orderBy('transferencias.created_at', 'ASC')
                                            ->get();
                                    }
                                    else
                                    {
                                        $transferencias = \App\Transferencia::where([
                                            ['aprobado', '=', 1],
                                            ['cuenta_origen_id', '=', $cuenta_origen->id]
                                        ])
                                            ->orderBy('created_at', 'ASC')
                                            ->get();
                                    }
                                @endphp

                                @if (count($transferencias) > 0)
                                @foreach ($transferencias as $transferencia)
                                @php
                                    $cuenta_destino = $transferencia->cuentaDestino;
                                    $banco_destino = $cuenta_destino->banco;
                                    $empresa_destino = $cuenta_destino->empresa;

                                    $total_egresos += $transferencia->monto;

                                    switch ($cuenta_origen->moneda_id) {
                                        case 1:
                                            // Bolivianos
                                            $total_egresos_global[0] += $total_egresos;
                                            break;
                                        case 2:
                                            // Dólares
                                            $total_egresos_global[1] += $total_egresos;
                                            break;
                                        case 3:
                                            // Euros
                                            $total_egresos_global[2] += $total_egresos;
                                            break;
                                    }
                                @endphp
                                <tr>
                                    <td style="width: 10%;">{{ $loop->iteration }}</td>
                                    <td style="width: 15%;">{{ $transferencia->tipoTransferencia->descripcion }}</td>
                                    <td style="width: 15%;">{{ $cuenta_origen->moneda->sigla }} {{ number_format($transferencia->monto, 2, '.', ',') }}</td>
                                    <td style="width: 30%;">{{ $empresa_destino->nombre }}</td>
                                    <td style="width: 30%;">{{ $banco_destino->nombre }} - CTA. {{ $cuenta_destino->numero }} [{{ $cuenta_destino->moneda->sigla }}]</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="5">Sin movimientos registrados</td>
                                </tr>
                                @endif

                                {{-- TOTALES --}}
                                <tr>
                                    <th colspan="3">TOTAL INGRESOS</th>
                                    <td colspan="2">{{ $cuenta_origen->moneda->sigla }} {{ number_format($total_ingresos, 2, '.', ',') }}</td>
                                </tr>
                                <tr>
                                    <th colspan="3">TOTAL EGRESOS</th>
                                    <td colspan="2">{{ $cuenta_origen->moneda->sigla }} {{ number_format($total_egresos, 2, '.', ',') }}</td>
                                </tr>
                                <tr>
                                    <th colspan="3">TOTAL</th>
                                    <td colspan="2">{{ $cuenta_origen->moneda->sigla }} {{ number_format($total_ingresos - $total_egresos, 2, '.', ',') }}</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        @endforeach
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    $(document).ready(function (){
        $('td#total_in_0').html("{{ number_format($total_ingresos_global[0], 2, '.', ',') }}");
        $('td#total_in_1').html("{{ number_format($total_ingresos_global[1], 2, '.', ',') }}");
        $('td#total_in_2').html("{{ number_format($total_ingresos_global[2], 2, '.', ',') }}");

        $('td#total_out_0').html("{{ number_format($total_egresos_global[0], 2, '.', ',') }}");
        $('td#total_out_1').html("{{ number_format($total_egresos_global[1], 2, '.', ',') }}");
        $('td#total_out_2').html("{{ number_format($total_egresos_global[2], 2, '.', ',') }}");

        $('td#total_general_0').html("{{ number_format(($total_ingresos_global[0] - $total_egresos_global[0]), 2, '.', ',') }}");
        $('td#total_general_1').html("{{ number_format(($total_ingresos_global[1] - $total_egresos_global[1]), 2, '.', ',') }}");
        $('td#total_general_2').html("{{ number_format(($total_ingresos_global[2] - $total_egresos_global[2]), 2, '.', ',') }}");
    });
    $('#btn-excel').on('click', function(){
        var values = $("input[name='exsol_id']")
            .map(function(){return $(this).val();}).get();
        var values_empresa = $("input[name='emp_id']")
            .map(function(){return $(this).val();}).get();

        console.log(values);
        console.log(values_empresa);
        if(values_empresa != 0){
            console.log("ENTRO AQU");
            window.location.href = '/reporte_transferencias_excel?origen_destino_empresa_id='+values+'&empresa_id='+values_empresa;
        }

    });

    $('#btn-pdf').on('click', function(){
        var values = $("input[name='exsol_id']")
            .map(function(){return $(this).val();}).get();
        var values_empresa = $("input[name='emp_id']")
            .map(function(){return $(this).val();}).get();

        if(values_empresa != 0)
        window.open('/reporte_transferencias_pdf?origen_destino_empresa_id='+values+'&empresa_id='+values_empresa , '_blank');
    });

</script>
@endsection