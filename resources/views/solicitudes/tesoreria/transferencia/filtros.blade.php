<div class="col-md-12">
    
    @if (\Auth::user()->rol_id == 8)
    <div class="col-md-4 col-md-12">
        <div class="form-group row col-md-12">
            {!! Form::label('empresa_id', 'EMPRESA: ', ['class' => 'col-md-3 col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                <select id="empresa_id" name="empresa_id" class="form-control form-admin">
                    <option value=''>SELECCIONE UNA EMPRESA</option>
                    @foreach($empresas as $empresa)
                        @if(isset($filters['empresa_id']) && $filters['empresa_id'] == $empresa['id'])
                        <option value='{{ $empresa['id'] }}' selected='selected'>{{ $empresa['nombre'] }}</option>
                        @else
                        <option value='{{ $empresa['id'] }}' >{{ $empresa['nombre'] }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    @endif

    <div class="col-md-4 col-md-12">
        <div class="form-group row col-md-12">
            {!! Form::label('origen_destino_empresa_id', 'ORIGEN/DESTINO: ', ['class' => 'col-md-3 col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                <select id="origen_destino_empresa_id" name="origen_destino_empresa_id" class="form-control form-admin">
                    <option value='0'>TODAS LAS EMPRESAS</option>
                    @foreach($empresas as $empresa)
                        @if(isset($filters['origen_destino_empresa_id']) && $filters['origen_destino_empresa_id'] == $empresa['id'])
                        <option value='{{ $empresa['id'] }}' selected='selected'>{{ $empresa['nombre'] }}</option>
                        @else
                        <option value='{{ $empresa['id'] }}' >{{ $empresa['nombre'] }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>

</div>

<br><br>

<div class="col-md-2 col-md-offset-5">
    <div class="center-block" style="margin-top: 10px;">
        {!! Form::submit('FILTRAR', ['class' => 'btn btn-success form-control btn-block btn-admin']) !!}
    </div>
</div>