@extends('main')

@section('headerScripts')
    <script src="/js/literal.js"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Guardar -->
                {!! Form::open(array('route' => 'solicitud.store', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true, 'onkeypress' => 'return event.keyCode != 13;', 'id' => 'frm')) !!}
                <!-- Enviar a Controller -->
                <div class="panel panel-default no-print">
                    <div class="panel-heading"><h2>Información Solicitud de Transferencia</h2></div>
                    <div class="panel-body">
                        @include('solicitudes.partial.form')
                    </div>
                </div>
                <div class="panel panel-default no-print">
                    <div class="panel-heading">
                        <div class="col-md-8">
                            <h2>Detalle de la transferencia</h2>
                            <span style="color:green; font-weight: bold;">(Especifique la cuenta de origen y la cuenta de destino)</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="solicitud-items">
                                <tbody>
                                    <tr>
                                        <th style="width: 40%;">Cuenta Bancaria Origen</th>
                                        <th style="width: 40%;">Tipo de Transferencia</th>
                                        <th style="width: 20%;">Monto de Transferencia</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="cuenta_origen" id="cuenta_origen" class="form-control">
                                                <option value="">SELECCIONE UNA CUENTA</option>
                                                @foreach ($cuentas_bancarias_origen as $cuenta_origen)
                                                    <option value="{{ $cuenta_origen->id }}">
                                                        {{ $cuenta_origen->banco->nombre }} (CTA. {{ $cuenta_origen->numero }}) [{{ $cuenta_origen->moneda->sigla }}]
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="tipo_transferencia" id="tipo_transferencia" class="form-control">
                                                <option value="">SELECCIONE UN TIPO</option>
                                                @foreach ($tipos_transferencia as $tipo_transferencia)
                                                    <option value="{{ $tipo_transferencia->id }}">
                                                        [{{ $tipo_transferencia->codigo }}] {{ $tipo_transferencia->descripcion }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="monto" id="monto" class="form-control mask-decimal" style="text-transform: uppercase; text-align: right;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Empresa Destino</th>
                                        <th>Cuenta Bancaria Destino</th>
                                        <th>Factor de conversión</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="empresa_destino" id="empresa_destino" class="form-control">
                                                <option value="">SELECCIONE UNA EMPRESA</option>
                                                @foreach ($empresas as $empresa)
                                                    <option value="{{ $empresa->id }}">{{ $empresa->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="cuenta_destino" id="cuenta_destino" class="form-control" disabled>
                                                <option value="">SELECCIONE UNA CUENTA</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="tipo_cambio" id="tipo_cambio" class="form-control mask-decimal" style="text-transform: uppercase; text-align: right;"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <span id="table-items" class="help-block" style="color: #a94442;">
                                <strong></strong>
                            </span>

                            <table class="table table-hover" id="solicitud-items-total" style="margin-bottom: 20px;">
                                <tbody>
                                    <tr style="border: 0;">
                                        <td colspan="3"></td>
                                        <td style="width: 10%; text-align: right; padding-top: 15px;"><strong>TOTAL: </strong></td>
                                        <td style="width: 10%;"><a class="btn btn-info" id="total" style="width: 100%; margin: 0px; text-align:right;">0.00</a></td>
                                        <td colspan="2" style="text-align:left; width: 5%; padding-top: 15px;"><strong id="monedaSiglaTag"> Bs.</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        {!! Form::hidden('referencia', 'REFERENCIA', array('class' => 'form-control', 'readonly' => 'true', 'id' => 'referencia')) !!}

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('observacion') ? ' has-error' : '' }}">
                                <label for="observacion" class="col-md-2 control-label">JUSTIFICACIÓN </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('observacion', null, ['placeholder' => 'JUSTIFIQUE LA TRANSFERENCIA','class' => 'form-control', 'id' => 'observacion', 'style' => 'text-transform:uppercase', 'rows' => 4]) !!}

                                    @if ($errors->has('observacion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('observacion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                {!! Form::hidden('imprimir', '0', array('id' => 'imprimir')) !!}
                                {!! Form::submit('REALIZAR SOLICITUD OCULTO', ['class' => 'btn btn-success btn-block', 'id' => 'btn-registro-solicitud', 'style' => 'display:none']) !!}
<!--                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#verificarModal" onclick="datosModal()">-->
                                <button type="button" class="btn btn-success" onclick="datosTransferencia()">
                                    REALIZAR SOLICITUD DE TRANSFERENCIA
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- MODAL -->
                @include('solicitudes.partial.modalVerificarTransferencia')
                <!-- END MODAL -->

                {!! Form::close() !!}
                <!-- MODAL DOCS PREVIEW-->
                <div class="modal fade" id="previewDocsModal" tabindex="-1" role="dialog" aria-labelledby="previewDocsModalLabel">
                    <div class="modal-dialog modal-lg " role="document">
                        <div class="modal-content">
                            <br>
                            <div class="modal-header no-print">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title text-center text-success" id="previewDocsModalLabel"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel panel-default ">
                                    <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                                    <div class="panel-body">
                                        {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                                            {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                                            <table class="table-modal-subir-archivo">
                                                <tr>
                                                    <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                                    <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                                </tr>
                                            </table>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->
            </div>
        </div>
    </div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    
    var contador;
    $(document).ready(function (e){
        //Filas Tabla Items
        contador = 0;
    });

    document.getElementById('correo_envio').style.display = 'none';
    
    function enviar_solicitud(){
        if(document.getElementById('correo').checked){
            document.getElementById('correo_envio').style.display = 'block';
        }else{
            document.getElementById('correo_envio').style.display = 'none';
        }
    }

    // Cambio de moneda
    $('input[name="moneda"]').on('click', function() {
        var moneda_id = $(this).val();

        var moneda_origen = $('input[name="moneda"]:checked').val();
        var moneda_destino = $('select#cuenta_destino option:selected').attr('data-moneda');

        $.ajax({
            url: '{{ route('solicitud.tesoreria.transferencia.getCuentaMoneda') }}',
            headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]')},
            type: 'GET',
            dataType: 'json',
            data: 'moneda_id=' + moneda_id,
            beforeSend: function (e){
                $('select#cuenta_origen').attr('disabled', true);
            },
            success: function (data){
                $('select#cuenta_origen > option').remove();
                $('select#cuenta_origen').removeAttr('disabled').append(new Option('SELECCIONE UNA CUENTA', ''));

                $.each(data, function (key, value) {
                    $('select#cuenta_origen').append(new Option(value.banco.nombre + ' (CTA. ' + 
                    value.numero + ') ' + '[' + value.moneda.sigla + ']', value.id));
                });

                factor();
            }
        });
    });

    // Cambio de empresa
    $('select#empresa_destino').on('change', function() {
        var empresa_id = $(this).val();
        
        $.ajax({
            url: '{{ route('solicitud.tesoreria.transferencia.getCuentaEmpresa') }}',
            headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]')},
            type: 'GET',
            dataType: 'json',
            data: 'empresa_id=' + empresa_id,
            beforeSend: function (e){
                $('select#cuenta_destino').attr('disabled', true);
            },
            success: function (data){
                $('select#cuenta_destino > option').remove();
                $('select#cuenta_destino').removeAttr('disabled').append(new Option('SELECCIONE UNA CUENTA', ''));

                $.each(data, function (key, value) {
                    var option = new Option(value.banco.nombre + ' (CTA. ' + 
                    value.numero + ') ' + '[' + value.moneda.sigla + ']', value.id);
                    option.setAttribute('data-moneda', value.moneda.id);
                    option.setAttribute('data-sigla', value.moneda.sigla);
                    $('select#cuenta_destino').append(option);
                });
            }
        });
    });

    // Cambio cuenta destino
    $('select#cuenta_destino').on('change', function() {
        factor();
    });

    // Cambio en monto
    $('input#monto').on('keyup', function() {
        total();
    });

    // Cambio en factor
    $('input#tipo_cambio').on('keyup', function() {
        total();
    });

    // Cambio en tipo de transferncia
    $('select#tipo_transferencia').on('change', function() {
        var tipo = $(this).val();
        if (tipo == 9) // TRASPASO EN LA MISMA EMPRESA
        {
            $('select#empresa_destino option:selected').removeAttr('selected').change();
            $('select#empresa_destino option:not([value="{{ session('empresa') }}"])').attr('disabled', true);

            $('select#empresa_destino').val('{{ session('empresa') }}').change();
        }
        else
        {
            $('select#empresa_destino option').removeAttr('disabled');
        }
    });

    // Factor
    function factor ()
    {
        var moneda_origen = $('input[name="moneda"]:checked').val();
        var moneda_destino = $('select#cuenta_destino option:selected').attr('data-moneda');
        var moneda_destino_sigla = $('select#cuenta_destino option:selected').attr('data-sigla');

        console.log('MO: ' + moneda_origen + ' MD: ' + moneda_destino);
        if (moneda_destino != undefined)
        {
            $.ajax({
                url: '{{ route('solicitud.tesoreria.transferencia.getFactor') }}',
                headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]')},
                type: 'GET',
                dataType: 'json',
                data: 'moneda_origen_id=' + moneda_origen + '&moneda_destino_id=' + moneda_destino,
                beforeSend: function (e){
                    
                },
                success: function (data){
                    $('input[name=tipo_cambio]').val(data.factor);

                    total();
                }
            });
        }
        else
        {
            $('input[name=tipo_cambio]').val('1');
        }

        $('strong#monedaSiglaTag').html(moneda_destino_sigla);
    }

    // Calcular total
    function total ()
    {
        var monto = $('input#monto').val();
        var tipo_cambio = $('input#tipo_cambio').val();

        var total = parseFloat(monto) * parseFloat(tipo_cambio);
        
        console.log('MONTO: ' + monto + ' TC: ' + tipo_cambio + ' TOTAL: ' + total);

        if (!isNaN(total))
        {
            $('#total').html(total.toFixed(2));
        }
        else
        {
            $('#total').html('0.00');
        }
    }

    // Validación
    function datosTransferencia ()
    {
        $('span#table-items strong').html('');

        var cerrarModal = false;

        // Modalidad
        var mod_sel = 'TRANSFERENCIA';

        // Empreas
        var mod_empresa = document.getElementById('nom_empresa').innerHTML;

        // Solicitante
        var mod_solicitante = '<div class="text-danger"><strong>NOMBRE DE USUARIO</strong></div>';
        if (document.getElementById('usuario').value) {
            mod_solicitante = document.getElementById('usuario').value;
        }

        // Proyecto
        var mod_proyecto = '<div class="text-danger"><strong>SIN PROYECTO</strong></div>';
        var ele = document.getElementById('proyecto');
        mod_proyecto = ele.options[ele.selectedIndex].innerHTML;

        // Fecha Limite
        var mod_fecha_lim = '<div class="text-danger"><strong>SIN FECHA LIMITE</strong></div>';
        var fecha_limite = new Date(document.getElementById('fecha_lim').value);
        var fecha_actual = new Date('{{ $date }}');

        if (fecha_limite && !isNaN(fecha_limite)) {
            mod_fecha_lim = document.getElementById('fecha_lim').value;

            if(fecha_limite < fecha_actual){
                @if(isset($solicitud))
                @if(Auth::user()->revisor != 1)
                $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
                $('input[name="fecha_lim"]').siblings('span.help-block').children().html('La fecha límite debe ser posterior a la fecha actual');
                cerrarModal=true;
                @endif
                @else
                $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
                $('input[name="fecha_lim"]').siblings('span.help-block').children().html('La fecha límite debe ser posterior a la fecha actual');
                cerrarModal=true;
                @endif
            }else{
                $('input[name="fecha_lim"]').parent().parent().removeClass('has-error');
                $('input[name="fecha_lim"]').siblings('span.help-block').children().html('');
            }
        }else{
            $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
            $('input[name="fecha_lim"]').siblings('span.help-block').children().html('Debe especificar la fecha límite de la solicitud');
            cerrarModal=true;
        }

        // Modalidad
        var rad_modalidad = document.getElementById('modalidad');
        var rate_value = $('input[name="modalidad"]').val();

        var mod_modalidad = 'TRANSFERENCIA';

        // Tipo de cambio
        var mod_tipo_cambio = '{{ $cambio["cambio"] }}';

        // Moneda
        var rad_moneda = document.getElementsByName('moneda');
        var mod_moneda;
        for (var i = 0; i < rad_moneda.length; i++) {
            if (rad_moneda[i].checked) {
                var selector = 'label[for=' + rad_moneda[i].id + ']';
                var label = document.querySelector(selector);
                mod_moneda = label.innerHTML;
            }
        }

        // Desembolso
        var mod_desembolso = '<div class="text-danger"><strong>SIN DESEMBOLSO</strong></div>';
        mod_desembolso = (document.getElementById('desembolso').value).toUpperCase();
        $('input[name="desembolso"]').parent().parent().removeClass('has-error');
        $('input[name="desembolso"]').siblings('span.help-block').children().html('');

        // Referencia
        var mod_referencia = '<div class="text-danger"><strong>SIN REFERENCIA</strong></div>';
        if (document.getElementById('referencia').value) {
            mod_referencia = (document.getElementById('referencia').value).toUpperCase();
        }

        // Observación
        var mod_observacion = '<div class="text-danger"><strong>SIN JUSTIFICACIÓN</strong></div>';
        if (document.getElementById('observacion').value) {
            mod_observacion = (document.getElementById('observacion').value).toUpperCase();
        }

        // Documentos
        var mod_documento = '<div class="text-danger"><strong>SIN DOCUMENTO</strong></div>';
        var str_docs = '';
        var aux_docs;
        for (i = 1; i < 30; i++) {
            element = (document.getElementById('doc' + i));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                aux_docs = document.getElementById('doc' + i).value;
                if (aux_docs) {
                    var startIndex = (aux_docs.indexOf('\\') >= 0 ? aux_docs.lastIndexOf('\\') : aux_docs.lastIndexOf('/'));
                    var filename = aux_docs.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    str_docs += filename + "<br>";
                }
            }
        }

        mod_documento = str_docs;

        $('span#documentos_error strong').html('');

        // Total
        var verifica_total = parseFloat(document.getElementById('total').innerHTML);
        if(verifica_total == 0 ){
            if($('span#table-items strong').html() == ""){
                $('span#table-items strong').html('El monto total de la solicitud debe ser mayor a cero');
            }
            cerrarModal = true;
        }else if($('span#table-items strong').html() == ""){
            $('span#table-items strong').html('');
        }

        console.log(verifica_total + ' ----- ' + cerrarModal + '////' + $('span#table-items strong').html());

        // Cuenta Origen
        var mod_cuenta_origen = '';
        var v_cuenta_origen = $('select#cuenta_origen option:selected').val();
        if (v_cuenta_origen == '')
        {
            $('span#table-items strong').html('Debe seleccionar una cuenta de origen');
            cerrarModal = true;
        }
        else if (cerrarModal == false)
        {
            $('span#table-items strong').html('');
            mod_cuenta_origen = $('select#cuenta_origen option:selected').text().trim();
        }

        console.log(v_cuenta_origen + ' ----- ' + cerrarModal + '////' + $('span#table-items strong').html());

        // Tipo de transferencia
        var mod_tipo_transferencia = '';
        var v_tipo_transferencia = $('select#tipo_transferencia option:selected').val();
        if (v_tipo_transferencia == '')
        {
            $('span#table-items strong').html('Debe seleccionar un tipo de transferencia');
            cerrarModal = true;
        }
        else if (cerrarModal == false)
        {
            $('span#table-items strong').html('');
            mod_tipo_transferencia = $('select#tipo_transferencia option:selected').text().trim();
        }

        console.log(v_tipo_transferencia + ' ----- ' + cerrarModal + '////' + $('span#table-items strong').html());

        // Monto
        var mod_monto = '';
        var mod_literal = '';
        var mod_decimal = '';
        var v_monto = parseFloat($('input#monto').val());
        if (v_monto > 0)
        {
            $('span#table-items strong').html('');
            mod_monto = v_monto;
            // Literal
            mod_literal = numeroLiteral($('input#monto').val());
            mod_decimal = numeroParteDecimal($('input#monto').val());
        }
        else if (cerrarModal == false)
        {
            $('span#table-items strong').html('Debe ingresar un monto');
            cerrarModal = true;
        }

        console.log(v_monto + ' ----- ' + cerrarModal + '////' + $('span#table-items strong').html());

        // Empresa destino
        var mod_empresa_destino = '';
        var v_empresa_destino = $('select#empresa_destino option:selected').val();
        if (v_empresa_destino == '')
        {
            $('span#table-items strong').html('Debe seleccionar una empresa de destino');
            cerrarModal = true;
        }
        else if (cerrarModal == false)
        {
            $('span#table-items strong').html('');
            mod_empresa_destino = $('select#empresa_destino option:selected').text().trim();
        }

        console.log(v_empresa_destino + ' ----- ' + cerrarModal + '////' + $('span#table-items strong').html());

        // Cuenta destino
        var mod_cuenta_destino = '';
        var v_cuenta_destino = $('select#cuenta_destino option:selected').val();
        if (v_cuenta_destino == '')
        {
            $('span#table-items strong').html('Debe seleccionar una cuenta de destino');
            cerrarModal = true;
        }
        else if (cerrarModal == false)
        {
            $('span#table-items strong').html('');
            mod_cuenta_destino = $('select#cuenta_destino option:selected').text().trim();
        }

        console.log(v_cuenta_destino + ' ----- ' + cerrarModal + '////' + $('span#table-items strong').html());

        // Factor
        var mod_factor = $('input#tipo_cambio').val();

        // Mostramos la infor en Modal
        $('#modal-proyecto').html(mod_proyecto);
        $('#modal-fecha-lim').html(mod_fecha_lim);
        $('#modal-solicitante').html(mod_solicitante);
        $('#modal-desembolso').html(mod_desembolso);
        $('#modal-cambio').html(mod_tipo_cambio);
        $('#modal-modalidad').html(mod_modalidad);
        $('#modal-moneda').html(mod_moneda);
        $('#modal-referencia').html(mod_referencia);
        $('#modal-observacion').html(mod_observacion);
        $('#modal-documentos').html(mod_documento);

        $('#modal-cuenta_origen').html(mod_cuenta_origen);
        $('#modal-tipo_transaccion').html(mod_tipo_transferencia);
        $('#modal-monto_transferencia').html(mod_monto);
        $('#modal-empresa_destino').html(mod_empresa_destino);
        $('#modal-cuenta_destino').html(mod_cuenta_destino);
        $('#modal-tipo_cambio').html(mod_factor);

        $('#modal-literal').html(mod_literal);
        $('#modal-decimal').html(mod_decimal);

        console.log(cerrarModal);

        if(!cerrarModal){
            $('#verificarModal').modal('show');
        }else{
            $('#reviewModal').modal('show');
        }
    }

    // Validación form
    function verificarFormTransferencia(e){
        $('span#table-items strong').html('');

        e.disabled = true;

        document.getElementById('btn-registro-solicitud').click();
    }
</script>

@endsection