<?php
include_once(app_path() . '/functions.php');
?>
@extends('pdfMain')

@section('headerScripts')
    <style type="text/css">
        body {
            font-family: sans-serif;
            font-size: 12px;
            background: white;
            color: black;
        }

        img.logo {
            height: 100px;
        }

        h1 {
            font-size: 2.4em;
            padding: 0.5em;
        }

        table {
            border-collapse: collapse;
            margin: auto;
            width: 100%;
            font-size: 12px;
        }

        table tr td h1 {
            text-align: center;
            font-weight: bolder;
        }

        table tr td h2 {
            text-align: center;
            font-weight: bolder;
        }

        table tr td .number {
            font-size: 28px;
            text-align: right;
        }

        #table-items tr th {
            background: #e6e6e6;
            color: black;
            padding: 5px 5px;
        }

        #table-items tr td {
            padding: 3px 5px;
            font-size: 9px;
        }

        .table-transferencia tr th {
            background: #e6e6e6;
            color: black;
            padding: 5px 5px;
        }

        .table-transferencia tr td {
            padding: 3px 5px;
            font-size: 9px;
        }


        .panel-default {
            border: 2px solid black;
            margin-bottom: 10px;
        }

        .panel-heading {
            font-size: 1.5em;
            font-weight: bold;
            padding-top: 3px;
            padding-bottom: 3px;
            background: #fff;
            border-bottom: 2px solid black !important;
        }

        .panel-solicitante {
            width: 55%;
        }

        .panel-solicitante .panel-heading {
            font-size: 0.9em;
        }

        .panel-solicitante .panel-body {
            margin: 0px;
            padding: 0px;
        }

        .panel-solicitante .panel-body .table tr td {
            padding-bottom: 3px;
            padding-top: 3px;
        }

        #table-contacto {
            margin: 5px 0px;
            font-size: 0.9em;
        }

        #table-contacto td {
            border: none !important;
        }

        #table-contacto tr {
            background: none;
        }

        #table-firmas {
            margin-bottom: 0px;
        }

        #table-firmas th {
            background: #fff;
            height: 70px;
        }

        #table-firmas tr td {
            width: 20%;
        }

        #table-firmas tr td p {
            font-size: 10px;
            text-align: center;
        }

        .p-total {
            margin-bottom: 0px;
            padding-bottom: 0px;
        }

        .table-print tr td {
            height: 15px !important;
            padding-bottom: 0px !important;
            padding-top: 0px !important;
        }

    </style>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <table>
                <tr>
                    <td style="width: 160px">
                        <img src="{{ public_path($empresa_origen_o->logo) }}" alt="{{ session('empresa_nombre') }}"
                             class="logo"/>
                    </td>
                    <td>
                        <h1>{{ $empresa_origen_o->nombre }}</h1>
                        <h2>Reporte Consolidado de Transferencias</h2>
                    </td>
                    {{--                    <td style="width: 160px">--}}
                    {{--                        <div class="number">--}}
                    {{--                            <br/><br/>--}}
                    {{--                            <strong>Nro. {{ $solicitud->numero }}</strong>--}}
                    {{--                        </div>--}}
                    {{--                    </td>--}}
                </tr>
            </table>
        </div>
        <div class="panel-body">

            <div class="panel panel-default ">
                <div class="panel-heading" style="text-align: center;">TOTALES</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="table-items">
                        <tbody>
                            <tr>
                                <th>TOTAL INGRESOS</th>
                                <th style="width: 5%;">BS</th>
                                <td id="total_in_0" style="width: 15%; text-align: right;"></td>
                                <th style="width: 5%;">$US</th>
                                <td id="total_in_1" style="width: 15%; text-align: right;"></td>
                                <th style="width: 5%;">€</th>
                                <td id="total_in_2" style="width: 15%; text-align: right;"></td>
                            </tr>
                            <tr>
                                <th>TOTAL EGRESOS</th>
                                <th style="width: 5%;">BS</th>
                                <td id="total_out_0" style="width: 15%; text-align: right;"></td>
                                <th style="width: 5%;">$US</th>
                                <td id="total_out_1" style="width: 15%; text-align: right;"></td>
                                <th style="width: 5%;">€</th>
                                <td id="total_out_2" style="width: 15%; text-align: right;"></td>
                            </tr>
                            <tr>
                                <th>TOTAL GENERAL</th>
                                <th style="width: 5%;">BS</th>
                                <td id="total_general_0" style="width: 15%; text-align: right;"></td>
                                <th style="width: 5%;">$US</th>
                                <td id="total_general_1" style="width: 15%; text-align: right;"></td>
                                <th style="width: 5%;">€</th>
                                <td id="total_general_2" style="width: 15%; text-align: right;"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            @php
                $total_ingresos_global = array(0, 0, 0); // Bs, $ y €
                $total_egresos_global = array(0, 0, 0); // Bs, $ y €
            @endphp
            @foreach ($cuentas_disponibles->chunk(2) as $chunks)

                @foreach ($chunks as $cuenta_origen)
                    @php
                        $total_egresos = 0;
                        $total_ingresos = 0;
                    @endphp


                    @php
                        if (isset($filters['origen_destino_empresa_id']) && $filters['origen_destino_empresa_id'] != 0)
                        {
                            $transferencias = \App\Transferencia::join('cuenta_bancarias', 'transferencias.cuenta_origen_id', '=', 'cuenta_bancarias.id')
                                ->where([
                                    ['transferencias.aprobado', '=', 1],
                                    ['transferencias.cuenta_destino_id', '=', $cuenta_origen->id],
                                    ['cuenta_bancarias.empresa_id', '=', $filters['origen_destino_empresa_id']]
                                ])
                                ->select('transferencias.*')
                                ->orderBy('transferencias.created_at', 'ASC')
                                ->get();

                            $transferencias2 = \App\Transferencia::join('cuenta_bancarias', 'transferencias.cuenta_destino_id', '=', 'cuenta_bancarias.id')
                                    ->where([
                                        ['transferencias.aprobado', '=', 1],
                                        ['transferencias.cuenta_origen_id', '=', $cuenta_origen->id],
                                        ['cuenta_bancarias.empresa_id', '=', $filters['origen_destino_empresa_id']]
                                    ])
                                    ->select('transferencias.*')
                                    ->orderBy('transferencias.created_at', 'ASC')
                                    ->get();
                        }
                        else
                        {
                            $transferencias = \App\Transferencia::where([
                                ['aprobado', '=', 1],
                                ['cuenta_destino_id', '=', $cuenta_origen->id]
                            ])
                                ->orderBy('created_at', 'ASC')
                                ->get();

                            $transferencias2 = \App\Transferencia::where([
                                    ['aprobado', '=', 1],
                                    ['cuenta_origen_id', '=', $cuenta_origen->id]
                                ])
                                    ->orderBy('created_at', 'ASC')
                                    ->get();
                        }

                    @endphp

                    @if (count($transferencias) > 0 || count($transferencias2) > 0)
                        <div class="panel panel-default">
                            <div class="panel-heading" style="text-align: center;">{{ $cuenta_origen->banco->nombre }} -
                                CTA. {{ $cuenta_origen->numero }} [{{ $cuenta_origen->moneda->sigla }}]
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered" id="table-items">
                                    <tbody>
                                    <tr>
                                        <th colspan="5" style="text-align: center; font-size: 18px;">Ingresos</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 10%;">#</th>
                                        <th style="width: 15%;">Tipo</th>
                                        <th style="width: 15%;">Monto</th>
                                        <th colspan="2" style="width: 60%;">Origen</th>
                                    </tr>
                                    @if (count($transferencias) > 0)
                                    @foreach ($transferencias as $transferencia)
                                        @php
                                            $cuenta_o = $transferencia->cuentaOrigen;
                                            $banco_origen = $cuenta_o->banco;

                                            $empresa_origen = $cuenta_o->empresa;

                                            $total_ingresos += $transferencia->monto * $transferencia->factor;

                                            switch ($cuenta_origen->moneda_id) {
                                                case 1:
                                                    // Bolivianos
                                                    $total_ingresos_global[0] += $total_ingresos;
                                                    break;
                                                case 2:
                                                    // Dólares
                                                    $total_ingresos_global[1] += $total_ingresos;
                                                    break;
                                                case 3:
                                                    // Euros
                                                    $total_ingresos_global[2] += $total_ingresos;
                                                    break;
                                            }
                                        @endphp
                                        <tr>
                                            <td style="width: 10%;">{{ $loop->iteration }}</td>
                                            <td style="width: 15%;">{{ $transferencia->tipoTransferencia->descripcion }}</td>
                                            <td style="width: 15%;">{{ $cuenta_origen->moneda->sigla }} {{ number_format($transferencia->monto * $transferencia->factor, 2, '.', ',') }}</td>
                                            <td style="width: 30%;">{{ $empresa_origen->nombre }}</td>
                                            <td style="width: 30%;">{{ $banco_origen->nombre }} -
                                                CTA. {{ $cuenta_o->numero }} [{{ $cuenta_o->moneda->sigla }}]
                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">Sin movimientos registrados</td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <th colspan="5" style="text-align: center; font-size: 18px;">Egresos</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 10%;">#</th>
                                        <th style="width: 15%;">Tipo</th>
                                        <th style="width: 15%;">Monto</th>
                                        <th colspan="2" style="width: 60%;">Destino</th>
                                    </tr>
                                    @php
                                        if (isset($filters['origen_destino_empresa_id']) && $filters['origen_destino_empresa_id'] != 0)
                                        {
                                            $transferencias = \App\Transferencia::join('cuenta_bancarias', 'transferencias.cuenta_destino_id', '=', 'cuenta_bancarias.id')
                                                ->where([
                                                    ['transferencias.aprobado', '=', 1],
                                                    ['transferencias.cuenta_origen_id', '=', $cuenta_origen->id],
                                                    ['cuenta_bancarias.empresa_id', '=', $filters['origen_destino_empresa_id']]
                                                ])
                                                ->select('transferencias.*')
                                                ->orderBy('transferencias.created_at', 'ASC')
                                                ->get();
                                        }
                                        else
                                        {
                                            $transferencias = \App\Transferencia::where([
                                                ['aprobado', '=', 1],
                                                ['cuenta_origen_id', '=', $cuenta_origen->id]
                                            ])
                                                ->orderBy('created_at', 'ASC')
                                                ->get();
                                        }
                                    @endphp

                                    @if (count($transferencias2) > 0)
                                        @foreach ($transferencias2 as $transferencia)
                                            @php
                                                $cuenta_destino = $transferencia->cuentaDestino;
                                                $banco_destino = $cuenta_destino->banco;
                                                $empresa_destino = $cuenta_destino->empresa;

                                                $total_egresos += $transferencia->monto;

                                                switch ($cuenta_origen->moneda_id) {
                                                    case 1:
                                                        // Bolivianos
                                                        $total_egresos_global[0] += $total_egresos;
                                                        break;
                                                    case 2:
                                                        // Dólares
                                                        $total_egresos_global[1] += $total_egresos;
                                                        break;
                                                    case 3:
                                                        // Euros
                                                        $total_egresos_global[2] += $total_egresos;
                                                        break;
                                                }
                                            @endphp
                                            <tr>
                                                <td style="width: 10%;">{{ $loop->iteration }}</td>
                                                <td style="width: 15%;">{{ $transferencia->tipoTransferencia->descripcion }}</td>
                                                <td style="width: 15%;">{{ $cuenta_origen->moneda->sigla }} {{ number_format($transferencia->monto, 2, '.', ',') }}</td>
                                                <td style="width: 30%;">{{ $empresa_destino->nombre }}</td>
                                                <td style="width: 30%;">{{ $banco_destino->nombre }} -
                                                    CTA. {{ $cuenta_destino->numero }}
                                                    [{{ $cuenta_destino->moneda->sigla }}]
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">Sin movimientos registrados</td>
                                        </tr>
                                    @endif

                                    {{-- TOTALES --}}
                                    <tr>
                                        <th colspan="3">TOTAL INGRESOS</th>
                                        <td colspan="2">{{ $cuenta_origen->moneda->sigla }} {{ number_format($total_ingresos, 2, '.', ',') }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="3">TOTAL EGRESOS</th>
                                        <td colspan="2">{{ $cuenta_origen->moneda->sigla }} {{ number_format($total_egresos, 2, '.', ',') }}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="3">TOTAL</th>
                                        <td colspan="2">{{ $cuenta_origen->moneda->sigla }} {{ number_format($total_ingresos - $total_egresos, 2, '.', ',') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                        @endforeach

                        @endforeach
        </div>
    </div>
@endsection
@section('footerScripts')
    @parent
    <script language="javascript" type="text/javascript">
        document.getElementById('total_in_0').innerHTML = "{{ number_format($total_ingresos_global[0], 2, '.', ',') }}";
        document.getElementById('total_in_1').innerHTML = "{{ number_format($total_ingresos_global[1], 2, '.', ',') }}";
        document.getElementById('total_in_2').innerHTML = "{{ number_format($total_ingresos_global[2], 2, '.', ',') }}";

        document.getElementById('total_out_0').innerHTML = "{{ number_format($total_egresos_global[0], 2, '.', ',') }}";
        document.getElementById('total_out_1').innerHTML = "{{ number_format($total_egresos_global[1], 2, '.', ',') }}";
        document.getElementById('total_out_2').innerHTML = "{{ number_format($total_egresos_global[2], 2, '.', ',') }}";

        document.getElementById('total_general_0').innerHTML = "{{ number_format(($total_ingresos_global[0] - $total_egresos_global[0]), 2, '.', ',') }}";
        document.getElementById('total_general_1').innerHTML = "{{ number_format(($total_ingresos_global[1] - $total_egresos_global[1]), 2, '.', ',') }}";
        document.getElementById('total_general_2').innerHTML = "{{ number_format(($total_ingresos_global[2] - $total_egresos_global[2]), 2, '.', ',') }}";
    </script>
@endsection