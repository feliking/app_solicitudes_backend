@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection
@section('content')
@php
    include_once(app_path().'/functions.php');
@endphp
<div class="panel panel-default ">
    <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD #{{ $solicitud->numero }}</h2></div>
    <div class="panel-body">
        <table class="table table-bordered" id="reporte-solicitud">
            <tr>
                <td><strong>PROYECTO: </strong></td>
                <td colspan="3">{{ $proyecto['proyecto'] }}</td>
                <td><strong>SUB PROYECTO: </strong></td>
                <td colspan="3">{{ $proyecto['subproyecto'] }}</td>
                <td><strong>Fecha Limite: </strong></td>
                <td>{{ $solicitud->fecha_limite }}</td>
            </tr>
            <tr>
                <td><strong>SOLICITANTE: </strong></td>
                <td colspan="9">{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
            </tr>
            <tr>
                <td><strong>DESEMBOLSO A: </strong></td>
                <td colspan="9">{{ $solicitud->desembolso }}</td>
            </tr>
            <tr>
                <td><strong>TIPO DE CAMBIO: </strong></td>
                <td>{{ $solicitud->tipo_cambio->cambio }}</td>
                <td><strong>MODALIDAD: </strong></td>
                <td colspan="3">{{ $proyecto['modalidad'] }}</td>
                <td><strong>MONEDA: </strong></td>
                <td colspan="3">{{ $solicitud->moneda->nombre }}</td>
            </tr>
            <tr>
                <td><strong>REFERENCIA: </strong></td>
                <td colspan="9">{{ $solicitud->referencia }}</td>
            </tr>
            <tr>
                <td><strong>JUSTIFICACIÓN: </strong></td>
                <td colspan="9">{{ $solicitud->observacion }}</td>
            </tr>
            <tr>
                <td><strong>AUTORIZADOR: </strong></td>
                <td colspan="9">
                    {{ $solicitud->autorizador }}
                </td>
            </tr>
            <tr>
                <td><strong>REVISOR: </strong></td>
                <td colspan="9">
                    {{ $solicitud->controller }}
                </td>
            </tr>
            <tr>
                <td><strong>APROBADOR: </strong></td>
                <td colspan="9">
                    {{ $solicitud->aprobador }}
                </td>
            </tr>
        </table>
    </div>
</div>

@if ($solicitud->modalidad_id != 5)
<div class="panel panel-default ">
    <div class="panel-heading"><h2>ITEMS SOLICITUD</h2></div>
    <div class="panel-body panel-items">
        <table class="table table-bordered table-items" id="table_items">
            <thead>
                <tr>
                    <th style="width:10px">#</th>
                    <th style="width:20px">UNIDAD</th>
                    <th>DETALLE</th>
                    <th style="width:20px">CANTIDAD</th>
                    <th style="width:20px">P. UNITARIO</th>
                    <th style="width:20px">SUB TOTAL</th>
                </tr>
            </thead>
            <tbody id='modal-items'>
                @php
                    $total = 0;
                @endphp
                @foreach($solicitud->items as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->unidad->descripcion }}</td>
                        <td>{{ $item->detalle }}</td>
                        <td style="text-align: center;">{{ $item->cantidad }}</td>
                        <td style="text-align: right;">{{ number_format($item->costo, 2, '.', ',') }}</td>
                        <td style="text-align: right;">{{ number_format($item->costo*$item->cantidad, 2, '.', ',') }}</td>
                    </tr>
                    @php
                        $total += number_format($item->costo*$item->cantidad, 2, '.', '')
                    @endphp
                @endforeach
                    <tr>
                        <td style="text-align: right;" colspan="5">TOTAL</td>
                        <td style="text-align: right;">{{ number_format($total, 2, '.', ',') }}</td>
                    </tr>
            </tbody>
        </table>
        <p>SON: <strong>{{ numeroLiteral($total) }}</strong> <span id="modal-literal"></span> CON <strong>{{ numeroDecimal($total) }}</strong>/100 {{ $solicitud->moneda->nombre }}</p>
    </div>
</div>
@else
@php
    $total = $solicitud->total;
@endphp
@endif

<div class="panel panel-default ">
    <div class="panel-heading"><h2>REGISTRO DE TESORERIA</h2></div>
    {!! Form::open(array('route' => 'cheques.store','method'=>'POST', 'class'=>'form-horizontal', 'id' => 'registro_tesoreria_form')) !!}
    <div class="panel-body panel-items">
        <div>
            <div class="container">
                <table id="solicitud-cheques" class=" table order-list">
                <thead>
                    <tr>
                        <td>NRO. CUENTA</td>
                        <td>BENEFICIARIO</td>
                        <td>MODO DE PAGO</td>
                        <td>NRO. CHEQUE</td>
                        <td>FECHA</td>
                        <td>MONTO</td>
                        <td>CONCEPTO</td>
                        <td>OBSERVACIONES</td>
                    </tr>
                </thead>
                <tbody></tbody>
                @if ($solicitud->modalidad_id != 5)
                <tfoot>
                    <tr>
                        <td colspan="8" style="text-align: left;">
                            <input type="button" class="btn btn-lg btn-block btn-primary" id="addrow" value="AÑADIR CHEQUE" onclick="creaFila()"/>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </tfoot>
                @endif
            </table>
            </div>
        </div>
        <div class="form-group{{ $errors->has('monto') ? ' has-error' : '' }} monto">
            <label for="monto" class="col-md-3 control-label">MONTO SOLICITUD:</label>
            <div class="col-md-3">
                <input class="form-control" name="monto" id="monto" placeholder="monto..." style="font-weight: bold; background: #ffffb3;" value="{{ number_format($total, 2, '.', '') }}" readonly="true"/>
                @if ($errors->has('monto'))
                    <span class="help-block">
                        <strong>{{ $errors->first('monto') }}</strong>
                    </span>
                @endif
            </div>
            <label for="monto_real" class="col-md-3 control-label">MONTO DESEMBOLSO:</label>
            <div class="col-md-3">
                <input class="form-control" name="monto_real" id="monto_real" placeholder="monto_real..." style="font-weight: bold; " value="0" onchange="actualiza_literal()" onkeyup="actualiza_literal()"/>
                @if ($errors->has('monto_real'))
                    <span class="help-block">
                        <strong>{{ $errors->first('monto_real') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4">
            {{ Form::hidden('solicitud_id', $solicitud->id, array('id' => 'solicitud_id', 'data-type' => 'sol_id')) }}
            {{ Form::hidden('moneda_id', $solicitud->moneda_id, array('id' => 'moneda_id')) }}
            {{ Form::hidden('cuenta_id', '', array('id' => 'cuenta_id', 'data-type' => 'cuenta_id')) }}
            <button type="button" class="btn btn-success btn-block" onclick="datosModalRegistro()">
                REALIZAR REGISTRO
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<!-- MODAL REVIEW -->
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="reviewModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-danger"><strong>VERIFIQUE LA INFORMACIÓN</strong></h4>
            </div>
            <div class="modal-body">
                <span id="mensaje_error">La información que intenta enviar cuenta con algunas observaciones, por favor verifique la misma.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection

@section('footerScripts')
<script>
    var contador;
    $(document).ready(function (e){
        //Filas Tabla Items
        contador = 0;
        creaFila();

        // Si es Transferencia
        @if ($solicitud->modalidad_id == 5)
        @php
            $transferencia = $solicitud->transferencia;
            $cuenta_destino = $transferencia->cuentaDestino;
            $empresa_destino = $cuenta_destino->empresa;
            $banco_destino = $cuenta_destino->banco;
        @endphp
        $('select#cuenta_0').val('{{ $transferencia->cuenta_origen_id }}');
        $('select#pago_0').val('2').change();
        $('input#monto_cheque_0').val('{{ $solicitud->total }}');
        $('input#beneficiario_0').val('{{ $empresa_destino->nombre }} - {{ $banco_destino->nombre }} - CTA. {{ $cuenta_destino->numero }}');
        $('input#monto_real').val('{{ $solicitud->total }}');

        $('select#cuenta_0 option:not(:selected)').remove();
        @endif
    });

    function creaFila() {
        var tabla = document.getElementById("solicitud-cheques").tBodies[0];
        var fila = document.createElement("TR");
        fila.setAttribute("data-fila", contador);
        fila.setAttribute("align", "center");

        var counter = document.createElement("INPUT");
        counter.setAttribute("type", "hidden");
        counter.setAttribute("value", contador);
        counter.setAttribute("name", "counter");
        fila.appendChild(counter);

        //CELDA CUENTA
        var celda1 = document.createElement("TD");
        var cuenta = document.createElement("SELECT");
        cuenta.setAttribute("size", "1");
        cuenta.setAttribute("class", "form-control");
        cuenta.setAttribute("id", "cuenta_" + contador);
        cuenta.setAttribute("name", "cuenta[]");
        <?php
        echo "opcioncur = document.createElement('OPTION');";
        echo "opcioncur.innerHTML = 'SELECCIONE CUENTA';";
        echo "opcioncur.value = '';";
        echo "cuenta.appendChild(opcioncur);";
        foreach ($cuentas as $cuenta) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $cuenta->banco->nombre." ".$cuenta->numero."(".$cuenta->moneda->sigla.")" . "';";
            echo "opcioncur.value = '" . $cuenta->id . "';";
            echo "cuenta.appendChild(opcioncur);";
        }
        ?>
        celda1.appendChild(cuenta);

        //CELDA BENEFICIARIO
        var celda2 = document.createElement("TD");
        var beneficiario = document.createElement("INPUT");
        beneficiario.setAttribute("type", "text");
        beneficiario.setAttribute("min", "0");
        beneficiario.setAttribute("id", "beneficiario_" + contador);
        beneficiario.setAttribute("class", "form-control mask-beneficiario");
        beneficiario.setAttribute("name", "beneficiario[]");
        <?php echo "var desem = '".$solicitud->desembolso."';"?>
        beneficiario.setAttribute("value", desem);
        celda2.appendChild(beneficiario);

        //CELDA PAGO
        var celda3 = document.createElement("TD");
        var pago = document.createElement("SELECT");
        pago.setAttribute("size", "1");
        pago.setAttribute("class", "form-control");
        pago.setAttribute("id", "pago_" + contador);
        pago.setAttribute("name", "pago[]");
        pago.setAttribute("onchange", "blockCheque(this.id)");

        var opcioncur = document.createElement('OPTION');
        opcioncur.innerHTML = 'CHEQUE';
        opcioncur.value = '1';
        pago.appendChild(opcioncur);
        opcioncur = document.createElement('OPTION');
        opcioncur.innerHTML = 'DEBITO AUTOMATICO';
        opcioncur.value = '2';
        pago.appendChild(opcioncur);
        opcioncur = document.createElement('OPTION');
        opcioncur.innerHTML = 'EFECTIVO';
        opcioncur.value = '3';
        pago.appendChild(opcioncur);
        opcioncur = document.createElement('OPTION');
        opcioncur.innerHTML = 'OTRO';
        opcioncur.value = '4';
        pago.appendChild(opcioncur);

        celda3.appendChild(pago);

        //CELDA NRO DE CHEQUE
        var celda4 = document.createElement("TD");
        var nro_cheque = document.createElement("INPUT");
        nro_cheque.setAttribute("type", "text");
        nro_cheque.setAttribute("placeholder", "NRO DE CHEQUE");
        nro_cheque.setAttribute("class", "form-control");
        nro_cheque.setAttribute("size", "1");
        nro_cheque.setAttribute("id", "nro_cheque_" + contador);
        nro_cheque.setAttribute("name", "nro_cheque[]");
        nro_cheque.setAttribute("style", "text-transform:uppercase;");

        celda4.appendChild(nro_cheque);

        //CELDA FECHA
        var celda5 = document.createElement("TD");
        var fecha = document.createElement("INPUT");
        fecha.setAttribute("type", "date");
        fecha.setAttribute("class", "form-control");
        fecha.setAttribute("id", "fecha_" + contador);
        fecha.setAttribute("name", "fecha[]");
        fecha.setAttribute("style", "text-transform:uppercase;");
        celda5.appendChild(fecha);

        //CELDA MONTO
        var celda51 = document.createElement("TD");
        var monto_cheque = document.createElement("INPUT");
        monto_cheque.setAttribute("type", "text");
        monto_cheque.setAttribute("id", "monto_cheque_" + contador);
        monto_cheque.setAttribute("class", "form-control mask-decimal");
        monto_cheque.setAttribute("name", "monto_cheque[]");
        monto_cheque.setAttribute("style", "text-transform:uppercase; text-align: right");
        monto_cheque.setAttribute("onchange", "subtotal_monto()");
        monto_cheque.setAttribute("onkeyup", "subtotal_monto()");
        celda51.appendChild(monto_cheque);

        //CELDA CONCEPTO
        var celda6 = document.createElement("TD");
        var concepto = document.createElement("INPUT");
        concepto.setAttribute("type", "text");
        concepto.setAttribute("min", "0");
        concepto.setAttribute("id", "concepto_" + contador);
        concepto.setAttribute("class", "form-control");
        concepto.setAttribute("name", "concepto[]");
        <?php echo "var desem = '".$solicitud->referencia."';"?>
        concepto.setAttribute("value", desem);
        celda6.appendChild(concepto);

        //CELDA OBSERVACION
        var celda7 = document.createElement("TD");
        var observacion = document.createElement("INPUT");
        observacion.setAttribute("type", "text");
        observacion.setAttribute("min", "0");
        observacion.setAttribute("id", "observacion_" + contador);
        observacion.setAttribute("class", "form-control");
        observacion.setAttribute("name", "observacion[]");
        celda7.appendChild(observacion);

        //BOTON ELIMINAR FILA
        var celda8 = document.createElement('TD');
        var tipoboton = document.createElement('div');
        tipoboton.setAttribute('class', 'botoneliminar');
        var eliminar = document.createElement('i');
        eliminar.setAttribute('class', 'fa fa-times-circle-o icono-opciones icono-red');
        eliminar.setAttribute('value', 'Quitar');
        tipoboton.appendChild(eliminar);
        tipoboton.onclick = function () {
            borrarFila(this);
            subtotal_monto();
        }
        celda8.appendChild(tipoboton);

        fila.appendChild(celda1);
        fila.appendChild(celda2);
        fila.appendChild(celda3);
        fila.appendChild(celda4);
        fila.appendChild(celda5);
        fila.appendChild(celda51);
        fila.appendChild(celda6);
        fila.appendChild(celda7);
        fila.appendChild(celda8);
        tabla.appendChild(fila);
        contador = contador + 1;
        mascara();
    }

    function borrarFila(button) {
        var fila = button.parentNode.parentNode;
        var tabla = document.getElementById('solicitud-cheques').getElementsByTagName('tbody')[0];
        tabla.removeChild(fila);
    }

    function calculateRow(row) {
        var price = +row.find('input[name^="price"]').val();

    }

    function calculateGrandTotal() {
        var grandTotal = 0;
        $("table.order-list").find('input[name^="price"]').each(function () {
            grandTotal += +$(this).val();
        });
        $("#grandtotal").text(grandTotal.toFixed(2));
    }

    $(document).ready(function() {
        $('#lbl_cheque').css("display", "block");
        $("#div_cheque").css("display", "block");
        $("#div_cheque input").attr("required", "true");
        $("#cuenta_id").val($("#numero_cuenta").val());
//        actualizar_moneda();
        actualiza_literal();
    });
    $("#pago").on('change', function(){
        if(this.value == 1){
            $('#lbl_cheque').css("display", "block");
            $("#div_cheque").css("display", "block");
            $("#div_cheque input").attr("required", "true");
        }else{
            $('#lbl_cheque').css("display", "none");
            $("#div_cheque").css("display", "none");
            $("#div_cheque input").removeAttr("required");
        }

    });

    $("#numero_cuenta").on('change', function(){
        $("#cuenta_id").val(this.value);
    });

    function actualizar_moneda(){
        var valor = $("#numero_cuenta").find(':selected').attr("data-tipo");
        $("#literal").empty();
        $("#literal").append(valor+": ");
        if(valor == "DOLARES"){
            $("#monto_real").val(($("#monto").val()/$("#tipo_cambio").val()).toFixed(2));
            actualiza_literal();
        }else{
            $("#monto_real").val($("#monto").val());
            actualiza_literal();
        }

    }

    function actualiza_literal(){
        $("#literal").val(numeroLiteral($("#monto_real").val()));
    }


    //Conversion Literal
    function mod(dividendo, divisor)
    {
        resDiv = dividendo / divisor;
        parteEnt = Math.floor(resDiv);            // Obtiene la parte Entera de resDiv
        parteFrac = resDiv - parteEnt;      // Obtiene la parte Fraccionaria de la divisi�n
        modulo = Math.round(parteFrac * divisor);  // Regresa la parte fraccionaria * la divisi�n (modulo)
        return modulo;
    }
    function ObtenerParteEntDiv(dividendo, divisor)
    {
        resDiv = dividendo / divisor;
        parteEntDiv = Math.floor(resDiv);
        return parteEntDiv;
    }
    function fraction_part(dividendo, divisor)
    {
        resDiv = dividendo / divisor;
        f_part = Math.floor(resDiv);
        return f_part;
    }

    function subtotal_monto(){
        var rows = document.getElementById("solicitud-cheques").tBodies[0].getElementsByTagName("tr").length;
        var fila = document.getElementById("solicitud-cheques").tBodies[0].rows[ document.getElementById("solicitud-cheques").tBodies[0].rows.length - 1 ].getAttribute('data-fila');
        var monto_total = 0;
        for(var i = 0; i <= fila; i++){
            if(document.getElementById("monto_cheque_"+i) && document.getElementById("monto_cheque_"+i).value != ""){
                monto_total += parseFloat(document.getElementById("monto_cheque_"+i).value);
            }
        }

        document.getElementById("monto_real").value = monto_total;
    }

    function blockCheque(e){
        var x1 = document.getElementById(e).selectedIndex;
        var y1 = document.getElementById(e).options;
        var ele = e.split("_");
        if(y1[x1].value != 1){
            document.getElementById("nro_cheque_"+ele[1]).value = 0;
            document.getElementById("nro_cheque_"+ele[1]).style.visibility = 'hidden';
        }else{
            document.getElementById("nro_cheque_"+ele[1]).style.visibility = 'visible';
            document.getElementById("nro_cheque_"+ele[1]).value = "";
        }
        console.log(y1[x1].value);
    }

    function datosModalRegistro() {
        $('span#table-items strong').html('');
        var cerrarModal = false;
        var mod_sel = document.getElementById("modalidad");

        //Tabla Items
        var total;
        total = 0;
        var element;
        var str_items = "";
        var cont_items = 0;
        var td_can;
        var td_uni;
        var td_tip;
        var td_det;
        var td_pre;
        var td_sub;
        var td_total = (document.getElementById('monto_real')).value;

        var tableReg = document.getElementById("solicitud-cheques").tBodies[0];
        var filas_tabla = document.getElementById('solicitud-cheques').tBodies[0].rows.length;

        for (i = 0; i < filas_tabla; i++) {
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            referenciaFila = cellsOfRow[1].children[0].id;
            var fila_array = referenciaFila.split('_');
            element = parseFloat(document.getElementById('monto_cheque_'+ fila_array[1]).value);
            console.log("element: "+element);
            if (isNaN(element)){
                tableReg.deleteRow(i);
                i--;
                filas_tabla--;
            }else{
                var cad = "";
                var monto = document.getElementById('monto_cheque_' + fila_array[1]);
                var td_fecha_factura = (document.getElementById('fecha_' + fila_array[1])).value;
                var sel_tipo = document.getElementById('pago_' + fila_array[1]);
                var td_seltip = sel_tipo.options[sel_tipo.selectedIndex].innerHTML;
                if(td_seltip == 'CHEQUE'){
                    var nro_cheque = (document.getElementById('nro_cheque_' + fila_array[1])).value;
                    if(nro_cheque == ""){
                        cad +='Verifique que todos los ítems que sean cheques cuenten con Número de cheque';
                        cerrarModal = true;
                    }
                }
                var ele = document.getElementById('cuenta_' + fila_array[1]);
                td_tip = ele.options[ele.selectedIndex].innerHTML;
                if(td_tip == 'SELECCIONE CUENTA' || monto < 1 || td_fecha_factura.length < 1){
                    if(td_tip == 'SELECCIONE CUENTA'){
                        if(cad != ""){
                            cad += ", ";
                        }
                        cad +='Verifique que todos los ítems de la solicitud tengan Cuenta seleccionada';
                    }else if(monto > 0){
                        if(cad != ""){
                            cad += ", ";
                        }
                        cad += 'Verifique que todos los ítems de la solicitud tengan Monto';
                    }else if(td_fecha_factura !== 'undefined' || td_fecha_factura.length < 1 || !isDate(td_fecha_factura)){
                        if(cad != ""){
                            cad += ", ";
                        }
                        cad += 'Verifique que todos los movimientos tengan especificado la fecha';
                    }
                    cerrarModal = true;
                    break;
                }
                cont_items++;
            }
        }

        if(td_total <= 0){
            if($('span#table-items strong').html() == ""){
                $('span#table-items strong').html('La solicitud no debe tener un total menor o igual a cero, favor verifique la información de los ítems');
            }
            cerrarModal = true;
        }else if($('span#table-items strong').html() == ""){
            $('span#table-items strong').html('');
        }

        var mod_literal = numeroLiteral(td_total);
        var mod_decimal = numeroParteDecimal(td_total);

        //fin
        if(!cerrarModal){
            document.getElementById("registro_tesoreria_form").submit();
        }else{
            document.getElementById('mensaje_error').innerHTML = cad;
            $('#reviewModal').modal('show');
        }
    }

    function isDate(ExpiryDate) {
    var objDate,  // date object initialized from the ExpiryDate string
        mSeconds, // ExpiryDate in milliseconds
        day,      // day
        month,    // month
        year;     // year
    // date length should be 10 characters (no more no less)
    if (ExpiryDate.length !== 10) {
        return false;
    }
    // third and sixth character should be '/'
    if (ExpiryDate.substring(2, 3) !== '/' || ExpiryDate.substring(5, 6) !== '/') {
        return false;
    }
    // extract month, day and year from the ExpiryDate (expected format is mm/dd/yyyy)
    // subtraction will cast variables to integer implicitly (needed
    // for !== comparing)
    month = ExpiryDate.substring(0, 2) - 1; // because months in JS start from 0
    day = ExpiryDate.substring(3, 5) - 0;
    year = ExpiryDate.substring(6, 10) - 0;
    // test year range
    if (year < 1000 || year > 3000) {
        return false;
    }
    // convert ExpiryDate to milliseconds
    mSeconds = (new Date(year, month, day)).getTime();
    // initialize Date() object from calculated milliseconds
    objDate = new Date();
    objDate.setTime(mSeconds);
    // compare input date and parts from Date() object
    // if difference exists then date isn't valid
    if (objDate.getFullYear() !== year ||
        objDate.getMonth() !== month ||
        objDate.getDate() !== day) {
        return false;
    }
    // otherwise return true
    return true;
}

</script>
@endsection