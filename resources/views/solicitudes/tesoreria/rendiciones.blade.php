@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-3">
                        <h2>Tesoreria</h2>
                    </div>
                    <?php
                    $array_docs_adjuntos = array();
                        if(!isset($array_addquery)){
                            $array_addquery = '';
                        }
                        $orden_type = "DESC";
                        if(isset($_GET['ordenar'])){

                            if($_GET['tipo_ord'] == 'DESC'){
                                $orden_type = 'ASC';
                            }else{
                                $orden_type = 'DESC';
                            }
                            switch ($_GET['ordenar']){
                                case 'id_solicitud':
                                    $orden = "sol.id_solicitud ".$orden_type;
                                    break;
                                case 'total':
                                    $orden = "sol.total ".$orden_type;
                                    break;
                                case 'proyecto':
                                    $orden = "pro.nombre ".$orden_type;
                                    break;
                                case 'fecha':
                                    $orden = "sol.fecha_solicitud ".$orden_type;
                                    break;
                                case 'hora':
                                    $orden = "sol.hora_solicitud ".$orden_type;
                                    break;
                                case 'usuario':
                                    $orden = "sol.id_usuario ".$orden_type;
                                    break;
                                case 'desembolso':
                                    $orden = "sol.desembolso ".$orden_type;
                                    break;
                                case 'cambio':
                                    $orden = "sol.tipo_cambio ".$orden_type;
                                    break;
                                case 'tipo_solicitud':
                                    $orden = "sol.tipo_solicitud ".$orden_type;
                                    break;
                                case 'moneda':
                                    $orden = "sol.moneda ".$orden_type;
                                    break;
                                case 'referencia':
                                    //$orden = "sol.id_solicitud ASC";
                                    $orden = "fecha_solicitud ".$orden_type;
                                    break;
                                case 'observacion':
                                    $orden = "sol.observacion ".$orden_type;
                                    break;
                            }
                        }else{
                            $orden_type = "DESC";
                            $orden = "fecha_solicitud ".$orden_type;
                        }
                        $contador = 1;
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                        ?>
                    <div class="col-md-6">
                        {!! Form::open(array('action' => 'SolicitudController@buscadorAutorizar', 'method'=>'POST', 'class'=>'form-inline')) !!}
                            {!! Form::label('buscar', 'BUSCAR: ') !!}
                            {!! Form::text('buscar', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase')) !!}
                            {!! Form::select('campo', ['TODOS', 'NRO. DE SOLICITUD', 'MONTO', 'PROYECTO', 'FECHA', 'HORA', 'SOLICITANTE', 'DESEMBOLSO', 'TIPO DE CAMBIO', 'MODALIDAD', 'MONEDA', 'REFERENCIA', 'OBSERVACIÓN'], 0, ['class' => 'form-control']) !!}
                            {!! Form::submit('BUSCAR', ['class' => 'btn btn-success']) !!}
                        {!! Form::close() !!}
                        <?php
                        if(isset($_GET['ordenar'])){
                            ?>
                            <p style="font-size:12px; margin-bottom: 0px; padding-bottom: 0px; text-align: center;">ORDENADO POR: <?=strtoupper($_GET['ordenar'])?> (<?=$orden_type?>)</p>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered table-solicitudes table-hover">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>#</th>
                                <th style="width: 60px;">DOC.</th>
                                <th><a href="<?=Request::url()?>?ordenar=id_solicitud&tipo_ord={{ $orden_type }}">NRO.<br>SOL. <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=total&tipo_ord={{ $orden_type }}">MONTO <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=proyecto&tipo_ord={{ $orden_type }}">PROYECTO <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=usuario&tipo_ord={{ $orden_type }}">SOLICITANTE <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=desembolso&tipo_ord={{ $orden_type }}">DESEMBOLSO <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=cambio&tipo_ord={{ $orden_type }}">TIPO DE <br>CAMBIO <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=tipo_solicitud&tipo_ord={{ $orden_type }}">MODALIDAD <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=moneda&tipo_ord={{ $orden_type }}">MONEDA <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=referencia&tipo_ord={{ $orden_type }}">REFERENCIA <i class="fa fa-sort"></i></a></th>
                                <th><a href="<?=Request::url()?>?ordenar=observacion&tipo_ord={{ $orden_type }}">OBSERVACION <i class="fa fa-sort"></i></a></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($solicitudes as $solicitud)
                            <tr>
                                <td>
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                    <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-target="#historialSolicitud" data-toggle="modal"></a>
                                    <i id="sol_fila_{{ $solicitud->id }}" class="fa fa-check-circle-o icono-opciones-files icono-green" data-toggle="modal" data-target="#getTicket" data-id="{{ $solicitud->id }}" onclick="seleccion_solicitud(this)" title="Aceptar Rendición"></i>
                                </td>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @if(sizeof($solicitud->documentos) > 0)
                                        <a id="<?=$solicitud->id;?>" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                    @endif
                                </td>
                                <td id="solicitud_{{ $solicitud->id }}">{{ $solicitud->numero }}</td>
                                <td id="total_{{ $solicitud->id }}">{{ $solicitud->total }}</td>
                                <td id="proyecto_{{ $solicitud->id }}">{{ $solicitud->proyecto->nombre }}</td>
                                <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre }}</td>
                                <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                <td id="cambio_{{ $solicitud->id }}">{{ $solicitud->tipo_cambio->cambio }}</td>
                                <td id="moneda_{{ $solicitud->id }}">{{ $solicitud->moneda->sigla }}</td>
                                <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <script language="javascript" type="text/javascript">
                        function registroTesoreria(e){
                            var id = e.id;
                            //window.location.href = '/solicitud-imprimir?id_sol='+id;
                        }
                        function datosRechazoModal(fila){
                            document.getElementById('modalrechazo-proyecto').innerHTML = document.getElementById('proyecto_'+fila).innerHTML;
                            //document.getElementById('modalrechazo-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
                            document.getElementById('modalrechazo-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
                            document.getElementById('modalrechazo-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
                            document.getElementById('modalrechazo-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
                            document.getElementById('modalrechazo-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
                            document.getElementById('modalrechazo-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
                            document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
                            document.getElementById('modalrechazo-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
                            document.getElementById('modalrechazo-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
                            document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
                        }
                        function autorizarModal(fila){
                            document.getElementById('modalautorizar-proyecto').innerHTML = document.getElementById('proyecto_'+fila).innerHTML;
                            //document.getElementById('modalautorizar-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
                            document.getElementById('modalautorizar-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
                            document.getElementById('modalautorizar-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
                            document.getElementById('modalautorizar-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
                            document.getElementById('modalautorizar-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
                            document.getElementById('modalautorizar-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
                            document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
                            document.getElementById('modalautorizar-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
                            document.getElementById('modalautorizar-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
                            document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;

                            var span_p = document.getElementById('modalautorizaropciones');
                            $("#modalautorizaropciones").empty();
                            //<button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            var btnCerrar = document.createElement("button");
                            btnCerrar.setAttribute("type", "button");
                            btnCerrar.setAttribute("class", "btn btn-default");
                            btnCerrar.setAttribute("data-dismiss", "modal");
                            btnCerrar.innerHTML="CERRAR";
                            span_p.appendChild(btnCerrar);

                            var file = document.createElement("button");
                            file.setAttribute("type", "button");
                            file.setAttribute("class", "btn btn-primary");
                            file.setAttribute("id", fila);
                            file.innerHTML = "AUTORIZAR";
                            file.setAttribute("onclick", "autorizarSolicitud(this)");
                            span_p.appendChild(file);

                            for (i = 0; i < 5000; i++) {
                                if(document.getElementById('items_aprob_'+i)){
                                    document.getElementById('items_aprob_'+i).style.display = "none";
                                }
                            }
                            var fila_sol = document.getElementById('solicitud_'+fila).innerHTML;
                            document.getElementById('items_aprob_'+fila).setAttribute('style', 'display: inline-table');

                            var total = document.getElementById('total_'+fila).innerHTML;
                            var res = total.split(".");
                            var p_entera = res[0];
                            var p_decimal = res[1];
                            var str_decimal = '00';

                            //var str_literal = string_literal_conversion(p_entera);
                            var str_literal = numeroLiteral(p_entera);
                            document.getElementById('modal-literal-aprobar').innerHTML = str_literal.toUpperCase();
                            if(p_decimal>0 && p_decimal <10){
                                str_decimal = '0'+str_decimal;
                            }else if(p_decimal > 9){
                                str_decimal = res[1];
                            }
                            document.getElementById('modal-decimal-aprobar').innerHTML = str_decimal;

                        }

                        function subir_docs(e){
                            //llenar datos necesarios para documentos en la solicitud deseada
                            document.getElementById('id_sol_subir').value = e.id;

                            var btn_modal_sub = document.getElementById('btn-modal-subir');
                            btn_modal_sub.click();
                        }

                        function descarga_docs(e){
                            var url = "/json/documentos_solicitud/" + e.id;
                            $.getJSON(url, function(data) {
                                $("#doc_table tbody").empty();
                                docs = data;
                                $.each(data, function (i, object) {
                                    $fila = "<tr>";
                                    $fila += "<td>"+object.id+"</td>";
                                    $fila += "<td>"+object.direccion+"</td>";
                                    $fila += "<td>";
                                    $fila += "<a href='/empresas/"+object.empresa_slug+"/"+object.gestion+"/solicitud/"+object.direccion+"' download='"+object.direccion+"'><i class='fa fa-download icono-opciones icono-blue' title='Descargar Documento'></i></a>";
                                    $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-file-text icono-opciones icono-green' onclick='ver_documento(this)' data='"+object.direccion+"' title='Ver Documento'></a>";
                                    $fila += "</td>";
                                    $fila += "</tr>";
                                    $("#doc_table tbody").append($fila);
                                });
                            });
                            var btn_modal_des = document.getElementById('btn-modal-descarga');
                            btn_modal_des.click();
                            var div_doc = document.getElementById('ver_doc');
                            div_doc.style.display = 'none';
                            var numeroSolicitud = e.getAttribute('data-numero');
                            document.getElementById('descargarDocsModalLabel').innerHTML = "<strong>DESCARGAR DOCUMENTOS SOLICITUD #"+numeroSolicitud+"</strong>";
                        }

                        function seleccion_solicitud(e){
                            var mod_tam = (e.id).length;
                            var fila = (e.id).substring(9, mod_tam);
                            document.getElementById('modal-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
                            document.getElementById('modal-proyecto').innerHTML = document.getElementById('proyecto_'+fila).innerHTML;
                            //document.getElementById('modal-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
                            document.getElementById('modal-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
                            document.getElementById('modal-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
                            document.getElementById('modal-num').innerHTML = document.getElementById('solicitud_'+fila).innerHTML;
                            document.getElementById('modal-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
                            document.getElementById('modal-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
                            document.getElementById('modal-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
                            document.getElementById('modal-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;

                            //var total = document.getElementById('total_'+fila).innerHTML;
                            var total = document.getElementById('suma_total').innerHTML;
                            var res = total.split(".");
                            var p_entera = res[0];
                            var p_decimal = res[1];
                            var str_decimal = '00';

                            //var str_literal = string_literal_conversion(p_entera);
                            var str_literal = numeroLiteral(p_entera);
                            document.getElementById('modal-literal').innerHTML = str_literal.toUpperCase();

                            if(p_decimal>0 && p_decimal <10){
                                str_decimal = '0'+str_decimal;
                            }else if(p_decimal > 9){
                                str_decimal = res[1];
                            }
                            document.getElementById('modal-decimal').innerHTML = str_decimal;

                            //str_items += "<tr><td class='text-right'>" + (cont_items) + "</td><td class='text-right'>" + td_can + "</td><td>" + td_uni + "</td><td>" + td_det + "</td><td class='text-right'>" + td_pre + "</td><td class='text-right'>" + td_sub + "</td></tr>";
                            var fila_sol = document.getElementById('solicitud_'+fila).innerHTML;
                            for (i = 0; i < 5000; i++) {
                                if(document.getElementById('items_'+i)){
                                    document.getElementById('items_'+i).style.display = "none";
                                }
                            }
                            document.getElementById('items_'+fila).style.display = "inline-table";
                            var btn_modal = document.getElementById('btn-modal-solicitud');
                            btn_modal.click();

                            var span_p = document.getElementById('print-span');
                            span_p.innerHTML = "";
                            var file = document.createElement("a");
                            file.setAttribute("class", "btn btn-primary");
                            file.setAttribute("id", fila);
                            file.innerHTML = "APROBAR RENDICIÓN";
                            file.setAttribute("href", "solicitud-aprobar-rendicion/"+fila);
                            span_p.appendChild(file);
                        }
                    </script>
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#favoritesModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-aprobar" class="btn btn-success btn-block" data-toggle="modal" data-target="#autorizarModal" style="display:none;">
                Modal aprobar
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <!-- MODAL -->
            <div class="modal fade" id="favoritesModal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="favoritesModalLabel"><strong>DATOS DE LA SOLICITUD #<span id="modal-num"></span></strong></h4>
                        </div>
                        <div class="modal-body">
                            <br>
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="3" id='modal-proyecto'></td>
                                            <td><label for="pago">SUB PROYECTO: </label></td>
                                            <td colspan="3" id='modal-subproyecto'></td>
                                            <td><label for="turno">Fecha Limite: </label></td>
                                            <td id="modal-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modal-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modal-desembolso'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                            <td id='modal-cambio'></td>
                                            <td><label for="grupo">MODALIDAD: </label></td>
                                            <td colspan="3" id='modal-modalidad'></td>
                                            <td><label for="grupo">MONEDA: </label></td>
                                            <td colspan="3" id='modal-moneda'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">REFERENCIA: </label></td>
                                            <td colspan="9" id='modal-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                                            <td colspan="9" id='modal-observacion'></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ITEMS RENDIDOS SOLICITUD</h2></div>
                                <div class="panel-body panel-items">
                                    @foreach($items as $key => $item_table)
                                        <table class="table table-hover table-bordered" id="items_{{$key}}" style="display:none">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                                <?php $suma_total = 0.00; ?>
                                                @foreach($item_table as $curr_sol)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $curr_sol['unidad'] }}</td>
                                                        <td>{{ $curr_sol['descripcion'] }}</td>
                                                        <td style="text-align: right;">{{ $curr_sol['cantidad'] }}</td>
                                                        <td style="text-align: right;">{{ number_format($curr_sol['costo'], 2, '.', '') }}</td>
                                                        <td style="text-align: right;">{{ number_format($curr_sol['cantidad'] * $curr_sol['costo'], 2, '.', '') }}</td>
                                                    </tr>
                                                    <?php $suma_total += $curr_sol['cantidad'] * $curr_sol['costo']; ?>
                                                @endforeach
                                                <tr>
                                                    <td colspan="5" style="text-align: right;"><strong>TOTAL</strong></td>
                                                    <td style="text-align: right;" id="suma_total">
                                                        {{ number_format($suma_total, 2, '.', '') }} <!-- ***REVISAR DECIMALES ENTEROS-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    @endforeach
                                    <p><strong>SON:</strong> <span id="modal-literal"></span> CON <span id="modal-decimal"></span>/100 BOLIVIANOS</p>
                                </div>
                            </div>
                            <div class="modal-footer no-print"><!-- ELIMINAR DISPLAY NONE -->
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                <span class="pull-right" id="print-span"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- MODAL RECHAZO-->
            <div class="modal fade" id="rechazoModal" tabindex="-1" role="dialog" aria-labelledby="rechazoModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="3" id='modalrechazo-proyecto'></td>
                                            <td><label for="pago">SUB PROYECTO: </label></td>
                                            <td colspan="3" id='modalrechazo-subproyecto'></td>
                                            <td><label for="turno">FECHA LÍMITE: </label></td>
                                            <td id="modalrechazo-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modalrechazo-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modalrechazo-desembolso'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                            <td id='modalrechazo-cambio'></td>
                                            <td><label for="grupo">MODALIDAD: </label></td>
                                            <td colspan="3" id='modalrechazo-modalidad'></td>
                                            <td><label for="grupo">MONEDA: </label></td>
                                            <td colspan="3" id='modalrechazo-moneda'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">REFERENCIA: </label></td>
                                            <td colspan="9" id='modalrechazo-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                                            <td colspan="9" id='modalrechazo-observacion'></td>
                                        </tr>
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-total'></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::hidden('tipo', 'autorizar', array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::textarea('motivo', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...')) !!}
                                </div>
                            </div>
                            <div class="text-center">
                                <input type="checkbox" name="correo" id="correo" value="correo" checked="checked">&nbsp;&nbsp;<label for='correo'>¿ENVIAR AL CORREO?</label>
                            </div>
                            {!! Form::close() !!}
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="button" class="btn btn-primary" onclick="rechazo()">RECHAZAR SOLICITUD</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                        </div>
                    </div>
                    <script>
                        function rechazo(){
                            document.getElementById('id_solicitud').click();
                        }
                    </script>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- AUTORIZAR MODAL -->
            <div class="modal fade" id="autorizarModal" tabindex="-1" role="dialog" aria-labelledby="autorizarModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>DETALLE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <table class="table table-bordered" id="aprobar-solicitud">
                                    <tr>
                                        <td><label for="pago">PROYECTO: </label></td>
                                        <td colspan="3" id='modalautorizar-proyecto'></td>
                                        <td><label for="pago">SUB PROYECTO: </label></td>
                                        <td colspan="3" id='modalautorizar-subproyecto'></td>
                                        <td><label for="turno">FECHA LÍMITE: </label></td>
                                        <td id="modalautorizar-fecha-lim"></td>
                                    </tr>
                                    <tr>
                                        <td><label for="ruta">SOLICITANTE: </label></td>
                                        <td colspan="9" id="modalautorizar-solicitante"></td>
                                    </tr>
                                    <tr>
                                        <td><label for="grupo">DESEMBOLSO A: </label></td>
                                        <td colspan="9" id='modalautorizar-desembolso'></td>
                                    </tr>
                                    <tr>
                                        <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                        <td id='modalautorizar-cambio'></td>
                                        <td><label for="grupo">MODALIDAD: </label></td>
                                        <td colspan="3" id='modalautorizar-modalidad'></td>
                                        <td><label for="grupo">MONEDA: </label></td>
                                        <td colspan="3" id='modalautorizar-moneda'></td>
                                    </tr>
                                    <tr>
                                        <td><label for="grupo">REFERENCIA: </label></td>
                                        <td colspan="9" id='modalautorizar-referencia'></td>
                                    </tr>
                                    <tr>
                                        <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                                        <td colspan="9" id='modalautorizar-observacion'></td>
                                    </tr>
<!--                                    <tr class="no-print">
                                        <td class="text-right"><strong>DOCUMENTO(S)<br>ADJUNTO(S)</strong></td>
                                        <td colspan="9" id='modalautorizar-documentos'></td>
                                    </tr>-->
                                    <tr class="no-print">
                                        <td class="text-right"><strong>TOTAL</strong></td>
                                        <td colspan="9" id='modalautorizar-total'></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ITEMS SOLICITUD</h2></div>
                                <div class="panel-body panel-items">
                                    <?php
                                    foreach($items as $key => $item_table){
                                        ?>
                                        <table class="table table-hover table-bordered" id="items_aprob_<?=$key?>" style="display:none">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                        <?php
                                        $con_filas = 1;
                                        $suma_total = 0.00;
                                        foreach($item_table as $curr_sol){
                                            ?>
                                            <tr>
                                                <td><?=$con_filas?></td>
                                                <td><?=$curr_sol['unidad']?></td>
                                                <td><?=$curr_sol['descripcion']?></td>
                                                <td style="text-align: right;"><?=$curr_sol['cantidad']?></td>
                                                <td style="text-align: right;"><?=$curr_sol['costo']?></td>
                                                <td style="text-align: right;"><?=$curr_sol['subtotal']?></td>
                                            </tr>
                                            <?php
                                            $suma_total +=$curr_sol['subtotal'];
                                            $con_filas++;
                                        }?>
                                            <tr>
                                                <td colspan="5" style="text-align: right;"><strong>TOTAL</strong></td>
                                                <td style="text-align: right;">
                                                    <?=$suma_total; //***REVISAR DECIMALES ENTEROS?>
                                                </td>
                                            </tr>
                                        <?php
                                        echo "</body></table>";
                                    }
                                    ?>
                                    <p><strong>SON:</strong> <span id="modal-literal-aprobar"></span> CON <span id="modal-decimal-aprobar"></span>/100 BOLIVIANOS</p>
                                </div>
                            </div>
                            <div class="modal-footer"  id="modalautorizaropciones">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            <div class="modal fade" id="subirDocsModal" tabindex="-1" role="dialog" aria-labelledby="subirDocsModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>SUBIR DOCUMENTOS SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                                <div class="panel-body">
                                    {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                                        {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                                        <table class="table-modal-subir-archivo">
                                            <tr>
                                                <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                                <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                            </tr>
                                        </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
        </div>
    </div>
</div>
@stop