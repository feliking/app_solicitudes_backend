@extends('main')

@section('headerScripts')

@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-default no-print">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>APROBAR RENDICIÓN SOLICITUD #{{ $solicitud->numero }}</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>PROYECTO</td>
                                <td>{{ ($solicitud->padre_id)?$solicitud->padre->nombre:$solicitud->proyecto->nombre }}</td>
                                <td>SUB PROYECTO</td>
                                <td>{{ ($solicitud->padre_id)?$solicitud->proyecto->nombre:'s/n' }}</td>
                            </tr>
                            <tr>
                                <td>SOLICITANTE</td>
                                <td>{{ $solicitud->usuario->nombre }}</td>
                                <td>FECHA</td>
                                <td>{{ $solicitud->created_at->format('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <td>DESEMBOLSO A</td>
                                <td>{{ $solicitud->desembolso }}</td>
                                <td>MODALIDAD</td>
                                <td>{{ $solicitud->modalidad->nombre }}</td>
                            </tr>
                            <tr>
                                <td>MONEDA</td>
                                <td>{{ $solicitud->moneda->nombre }}</td>
                                <td>TIPO DE CAMBIO</td>
                                <td>{{ $solicitud->tipo_cambio->cambio }}</td>
                            </tr>
                            <tr>
                                <td>REFERENCIA</td>
                                <td colspan="3">{{ $solicitud->referencia }}</td>
                            </tr>
                            <tr>
                                <td>JUSTIFICACIÓN</td>
                                <td colspan="3">{{ $solicitud->observacion }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>INFORMACIÓN RENDICIÓN</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td style=" font-weight: bold;">Rendido por</td>
                                <td>{{ $solicitud->usuario->nombre }}</td>
                                <td style=" font-weight: bold;">Fecha</td>
                                <td>{{ $solicitud->created_at->format('d-m-Y') }}</td>
                            </tr>
                            <tr>
                                <td style=" font-weight: bold;">Proyecto</td>
                                <td>{{ ($solicitud->padre_id)?$solicitud->padre->nombre:$solicitud->proyecto->nombre }}</td>
                                <td style=" font-weight: bold;">Sub Proyecto</td>
                                <td>{{ ($solicitud->padre_id)?$solicitud->proyecto->nombre:'' }}</td>
                            </tr>
                            <tr>
                                <td style=" font-weight: bold;">Monto Desembolso</td>
                                <td style="color: red; font-weight: bold;">{{ number_format($extras['total_desembolso'], '2', '.', '') }}</td>
                                <td style=" font-weight: bold;">Monto Rendido</td>
                                <td style="color: green; font-weight: bold;">{{ number_format($extras['total_rendido'], '2', '.', '') }}</td>
                            </tr>
                            <tr>
                                <td style=" font-weight: bold;">Monto favor del solicitante</td>
                                <td>{{ number_format($extras['favor_solicitante'], '2', '.', '') }}</td>
                                <td style=" font-weight: bold;">Monto favor de la empresa</td>
                                <td>{{ number_format($extras['favor_empresa'], '2', '.', '') }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <h3>ITEMS RENDIDOS</h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>FACTURA</th>
                                <th>DETALLE</th>
                                <th>CANTIDAD</th>
                                <th>PRECIO</th>
                                <th>SUBTOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total = 0;
                            @endphp
                            @foreach($items_rendidos as $itemRendido)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ ($itemRendido->num_factura != 0)? $itemRendido->num_factura: 'Sin Factura' }}</td>
                                <td>{{ $itemRendido->detalle }}</td>
                                <td style="text-align: right;">{{ $itemRendido->cantidad }}</td>
                                <td style="text-align: right;">{{ number_format($itemRendido->costo, '2', '.', '') }}</td>
                                <td style="text-align: right;">{{ number_format($itemRendido->costo * $itemRendido->cantidad, '2', '.', '') }}</td>
                            </tr>
                            @php
                                $total += $itemRendido->costo * $itemRendido->cantidad;
                            @endphp
                            @endforeach
                            <tr>
                                <td colspan="5" style="text-align: right; font-weight: bold;">
                                    TOTAL
                                </td>
                                <td style="text-align: right; font-weight: bold;">
                                    {{ number_format($total, '2', '.', '') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div>
                        {!! Form::open(array('route' => 'solicitud.aprobar.rendicion.guardar', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
                            <input type="hidden" name="solicitud_id" value="{{ $solicitud->id }}"/>
                            <input class='btn btn-primary' type='submit' value='APROBAR RENDICIÓN'>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="rendirItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">VERIFICAR DATOS RENDICIÓN ITEM</h4>
            </div>
            {!! Form::open(array('route' => 'rendir.solicitud.item', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
            <div class="modal-body">
                <table id="table_items" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 45px;">ID</th>
                            <th style="width: 100px;">Nº FACTURA</th>
                            <th>DETALLE</th>
                            <th style="width: 70px;">CANT.</th>
                            <th style="width: 90px;">COSTO</th>
                            <th style="width: 105px;">SUB TOTAL</th>
                        </tr>
                    </thead>
                    <tbody id="item_info"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div id="solicitud_rendir"></div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
@endsection

@section('footerScripts')
<script language="javascript" type="text/javascript">
    
</script>
@endsection
