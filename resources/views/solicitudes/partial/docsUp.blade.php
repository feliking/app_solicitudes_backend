<div class="modal fade" id="subirDocsModal" tabindex="-1" role="dialog" aria-labelledby="subirDocsModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="subirDocsModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default ">
                    <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                    <div class="panel-body">
                        {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                            {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                            <table class="table-modal-subir-archivo">
                                <tr>
                                    <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                    <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                </tr>
                                <tr>
                                    <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                </tr>
                            </table>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                </div>
        </div>
    </div>
</div>

@section('footerScripts')
@parent
<script>
    function subir_docs(e){
        //llenar datos necesarios para documentos en la solicitud deseada
        document.getElementById('id_sol_subir').value = e.id;

        var btn_modal_sub = document.getElementById('btn-modal-subir');
        btn_modal_sub.click();
        var numeroSolicitud = e.getAttribute('data-numero');
        document.getElementById('subirDocsModalLabel').innerHTML = "<strong>SUBIR DOCUMENTOS A LA SOLICITUD #"+numeroSolicitud+"</strong>";
    }   
</script>
@endsection
