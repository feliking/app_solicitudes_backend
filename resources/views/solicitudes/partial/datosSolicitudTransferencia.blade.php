<div class="modal fade" id="solicitudModalTransferencia" tabindex="-1" role="dialog" aria-labelledby="solicitudModalTransferenciaLabel">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header no-print">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center text-success" id="solicitudModalTransferenciaLabel">
                        <strong><span id="modal-trs-nom-empresa"></span><br>DATOS DE LA SOLICITUD DE TRANSFERENCIA # <span id="modal-trs-numero"></span></strong>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default ">
                        <div class="panel-heading"><h1>INFORMACIÓN DE SOLICITUD DE TRANSFERENCIA</h1></div>
                        <div class="panel-body" style="overflow-x: scroll;">
                            <table class="table table-bordered" id="reporte-trs-solicitud">
                                <tr>
                                    <td><strong>PROYECTO: </strong></td>
                                    <td colspan="9" id='modal-trs-proyecto'></td>
                                </tr>
                                <tr>
                                    <td><strong>SOLICITANTE: </strong></td>
                                    <td colspan="9" id="modal-trs-solicitante"></td>
                                </tr>
                                <tr id="tr_trs_con_tipo">
                                    <td><strong>MODALIDAD: </strong></td>
                                    <td colspan="9" id='modal-trs-modalidad'></td>
                                </tr>
                                <tr id="tr_trs_sin_tipo">
                                    <td><strong>MODALIDAD: </strong></td>
                                    <td colspan="9" id='modal-trs-modalidad-sin'></td>
                                </tr>
                                <tr id="tr_trs_con_moneda">
                                    <td><strong>MONEDA: </strong></td>
                                    <td id='modal-trs-moneda'></td>
                                    <td><strong>TIPO DE CAMBIO: </strong></td>
                                    <td id='modal-trs-cambio'></td>
                                    <td><strong>FECHA LIMITE: </strong></td>
                                    <td id="modal-trs-con-fecha-lim"></td>
                                </tr>
                                <tr id="tr_trs_sin_moneda">
                                    <td><strong>MONEDA: </strong></td>
                                    <td id='modal-trs-moneda-sin'></td>
                                    <td><strong>FECHA LIMITE: </strong></td>
                                    <td id="modal-trs-sin-fecha-lim"></td>
                                </tr>
                            </table>
                            <div >
                                <div style="border: 1px solid orange; padding: 5px; border-radius: 5px; background: #ffd5cd;">
                                    <div id="div_trs_items_solicitud" style="overflow-x: scroll;">
                                        <h4 style="text-align: center;">DETALLE DE TRANSFERENCIA</h4>
                                        <table class="table table-bordered table-items" id="table_trs_items">
                                            <tbody id='modal-trs-items'>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <p><strong>SON:</strong> <span id="modal-trs-literal"></span> CON <span id="modal-trs-decimal"></span>/100 <span id="modal-trs-literal-moneda"></span></p>
                                <table class="table table-bordered" id="a">
                                    <tr>
                                        <td style="width: 100px"><strong>REFERENCIA: </strong></td>
                                        <td id='modal-trs-referencia'></td>
                                    </tr>
                                    <tr>
                                        <td><strong>JUSTIFICACIÓN: </strong></td>
                                        <td id='modal-trs-observacion'></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-print"><!-- ELIMINAR DISPLAY NONE -->
                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                    <span class="pull-right" id="print-trs-span"></span>
                </div>
            </div>
        </div>
    </div>
    
    @section('footerScripts')
    @parent
    <script>
        function seleccion_solicitud_transferencia(e){
            var mod_tam = (e.id).length;
            var id_fila = (e.id).substring(9, mod_tam);
            $("#table_trs_items tbody").empty();
            $("#table_trs_items_rendidos tbody").empty();
    
            var fila_sol = document.getElementById('solicitud_'+id_fila).innerHTML;
            var url_solicitud = '/json/datos_solicitud/'+id_fila;
            var modalidad_id = 0;
            var last_estado_id = 0;
            var go_cad = "";
    
            $.getJSON(url_solicitud, function(datos){
                document.getElementById('tr_trs_con_tipo').style.display = 'contents';
                document.getElementById('tr_trs_sin_tipo').style.display = 'none';
    
                document.getElementById('modal-trs-sin-fecha-lim').innerHTML = datos.fecha_limite;
                document.getElementById('modal-trs-con-fecha-lim').innerHTML = datos.fecha_limite;
    
                document.getElementById('modal-trs-solicitante').innerHTML = datos.usuario;
                document.getElementById('modal-trs-proyecto').innerHTML = datos.proyecto;
                document.getElementById('modal-desembolso').innerHTML = datos.desembolso;
                document.getElementById('modal-trs-cambio').innerHTML = datos.tipo_cambio;
                document.getElementById('modal-trs-numero').innerHTML = datos.numero;
    
                if(datos.moneda == 'BOLIVIANOS'){
                    document.getElementById('tr_trs_con_tipo').style.display = 'none';
                    document.getElementById('tr_trs_sin_tipo').style.display = '';
                    document.getElementById('tr_trs_con_moneda').style.display = 'none';
                    document.getElementById('tr_trs_sin_moneda').style.display = '';
                }else{
                    document.getElementById('tr_trs_con_tipo').style.display = '';
                    document.getElementById('tr_trs_sin_tipo').style.display = 'none';
                    document.getElementById('tr_trs_con_moneda').style.display = '';
                    document.getElementById('tr_trs_sin_moneda').style.display = 'none';
                }
                document.getElementById('modal-trs-modalidad').innerHTML = datos.modalidad;
                document.getElementById('modal-trs-moneda').innerHTML = datos.moneda;
    
                document.getElementById('modal-trs-modalidad-sin').innerHTML = datos.modalidad;
                go_cad = datos.modalidad;

                $("#table_trs_items tbody").empty();
                fila_th = "<tr>";
                    fila_th += "<th>Cuenta Bancaria Origen</th>";
                    fila_th += "<th>Tipo de Transacción</th>";
                    fila_th += "<th>Monto de Transferencia</th>";
                fila_th += "</tr>";

                fila_th += "<tr>";
                    fila_th += "<td>" + datos.banco_origen.nombre + " (CTA. " + datos.cuenta_origen.numero + ")</td>";
                    fila_th += "<td>[" + datos.tipo_transferencia.codigo + "] " + datos.tipo_transferencia.descripcion + "</td>";
                    fila_th += "<td>" + datos.monto + "</td>";
                fila_th += "</tr>";

                fila_th += "<tr>";
                    fila_th += "<th>Empresa Destino</th>";
                    fila_th += "<th>Cuenta Bancaria Destino</th>";
                    fila_th += "<th>Factor de conversión</th>";
                fila_th += "</tr>";

                fila_th += "<tr>";
                    fila_th += "<td>" + datos.empresa_destino.nombre + "</td>";
                    fila_th += "<td>" + datos.banco_destino.nombre + " (CTA. " + datos.cuenta_destino.numero + ")</td>";
                    fila_th += "<td>" + datos.factor + "</td>";
                fila_th += "</tr>";
                $("#table_trs_items tbody").append(fila_th);

                document.getElementById('modal-trs-moneda-sin').innerHTML = datos.moneda;
                document.getElementById('modal-trs-referencia').innerHTML = datos.referencia;
                document.getElementById('modal-trs-observacion').innerHTML = datos.observacion;
                document.getElementById('modal-trs-nom-empresa').innerHTML = datos.empresa;
    
                document.getElementById('modal-trs-literal-moneda').innerHTML = datos.moneda;
    
                modalidad_id = datos.modalidad_id;
    
                last_estado_id = datos.last_estado_id;
            });
    
            var verifica = {};
            $.ajax({
                url: "/json/verifica/solicitud/completa/"+id_fila,
                async: false,
                dataType: 'json',
                success: function(data) {
                    verifica = data;
                }
            });
    
            var total = document.getElementById('total_'+id_fila).innerHTML;
            var res = total.split(".");
            var p_entera = res[0];
            var p_decimal = parseInt(res[1]);
            var str_decimal = '0';
    
            if(p_entera != ""){
                var str_literal = numeroLiteral(p_entera);
                document.getElementById('modal-trs-literal').innerHTML = str_literal.toUpperCase();
            }
    
            if(p_decimal>0 && p_decimal <10){
                str_decimal = '0'+p_decimal;
            }else if(p_decimal > 9){
                str_decimal = res[1];
            }
            document.getElementById('modal-trs-decimal').innerHTML = str_decimal;
    
            var btn_modal = document.getElementById('btn-modal-solicitud-transferencia');
            btn_modal.click();

            var span_p = document.getElementById('print-trs-span');
            span_p.innerHTML = "";

            var file = document.createElement("button");
            file.setAttribute("type", "button");
            file.setAttribute("class", "btn btn-primary");
            file.setAttribute("id", id_fila);
            file.innerHTML = "IMPRIMIR";
            file.setAttribute("onclick", "imprimirSolicitudTransferencia(this)");
            span_p.appendChild(file);
    
            var url_lastEstadoCodigo = "/solicitud-lastEstadoCodigo/"+id_fila;
            var div_rendidos = document.getElementById('div_items_rendidos');
            div_rendidos.setAttribute('style', 'display: none;');
        }
    
        function imprimirSolicitudTransferencia(e){
            var id = e.id;
            window.open('/solicitud-imprimir-transferencia?id_sol='+id);
        }
    
        function imprimirSolicitudCompleta(e){
            var id = e.id;
            window.open('/solicitud-imprimir/completa?id_sol='+id);
        }
    </script>
    
    @endsection