@php
	$usuario = auth()->user();
@endphp
<div class="modal fade" id="advertenciaModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">&times;</button>
				<h4 class="modal-title text-center text-success"><strong>ADVERTENCIA</strong></h4>
			</div>
			<div class="modal-body text-justify">
				<span>NO SE PUEDE ASIGNAR EL PROYECTO <strong id="nombre_proyecto"></strong>, A LA SOLICITUD <strong id="nro_solicitud"></strong>, YA QUE EL USUARIO <strong id="nombre_usuario"></strong></span> NO ESTA ASIGNADO AL PROYECTO.
				<br><br>
				@if ($usuario->asignador_proyecto)
					<p class="text-center">¿DESEA ASIGNAR EL PROYECTO AL USUARIO?</p>
				@else
					<p>COMUNIQUESE CON EL DEPARTAMENTO DE SISTEMAS PARA GESTIONAR LA ASIGNACION DEL PROYECTO AL USUARIO.</p>
				@endif
			</div>
			<div class="modal-footer">
				@if ($usuario->asignador_proyecto)
					<button type="button" class="btn btn-primary" id="btn_añadirProyecto">Añadir</button>
				@endif
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>