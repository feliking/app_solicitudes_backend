<div class="modal fade" id="formulariosSolicitud" tabindex="-1" role="dialog" aria-labelledby="formulariosSolicitud">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>FORMULARIOS AF</strong></h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-items" id="table_items">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>DESCRIPCIÓN ESPECIFICA</th>
                            <th>Nº DE SERIE/CÓDIGO</th>
                            <th>ASIGNACIÓN</th>
                            <th>ACTIVIDAD ESPECIFICA</th>
                            <th>NUEVO</th>
                            <th>REEMPLAZO</th>
                            <th>GARANTÍA</th>
                        </tr>
                    </thead>
                    <tbody id='modal-items-af'>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer" id="modal-footer-af">
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    function imprimirSolicitudItemAF(e){
        var id = e.id;
        //window.location.href = '/solicitud-imprimir/formulario/items-af?id_sol='+id;
        window.open('/solicitud-imprimir/formulario/items-af?id_sol='+id,'_blank');
    }

    function formularios_solicitud(e){
        var mod_tam = (e.id).length;
        var id_fila = (e.id).substring(9, mod_tam);
        $("#modal-items-af").empty();
        $("#table_items_rendidos tbody").empty();

        var span_p = document.getElementById('modal-footer-af');
        span_p.innerHTML = "";

        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-default");
        file.setAttribute("data-dismiss", "modal");
        file.innerHTML = "CERRAR";
        span_p.appendChild(file);

        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", id_fila);
        file.innerHTML = "IMPRIMIR";
        file.setAttribute("onclick", "imprimirSolicitudItemAF(this)");
        span_p.appendChild(file);

        $("#table_items thead").empty();
        let fila_th ="<tr>";
        fila_th +="<th>#</th>";
        fila_th +="<th>DESCRIPCIÓN ESPECIFICA</th>";
        fila_th +="<th>Nº DE SERIE/CÓDIGO</th>";
        fila_th +="<th>ASIGNACIÓN</th>";
        fila_th +="<th>ACTIVIDAD ESPECIFICA</th>";
        fila_th +="<th>NUEVO</th>";
        fila_th +="<th>REEMPLAZO</th>";
        fila_th +="<th>GARANTÍA</th>";
        fila_th +="</tr>";
        $("#table_items thead").append(fila_th);

        var url_solicitud = '/json/item/formularios/af/'+id_fila;

        $.getJSON(url_solicitud, function(data){
            $.each(data, function (i, object) {
                fila = "<tr>";
                fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                fila += "<td>"+object.descripcion_especifica+"</td>";
                fila += "<td>"+object.numero_serie+"</td>";
                fila += "<td>"+object.asignacion.toUpperCase()+"</td>";
                fila += "<td>"+(object.actividad_especifica == "" ? "" : object.actividad_especifica)+"</td>";
                fila += "<td style='text-align:right;'>"+(object.nuevo == 1 ? "Si" : "No")+"</td>";
                fila += "<td style='text-align:right;'>"+(object.reemplazo == 1 ? "Si" : "No")+"</td>";
                fila += "<td style='text-align:right;'>"+(object.garantia == 1 ? "Si" : "No")+"</td>";
                fila += "</tr>";
                $("#modal-items-af").append(fila);
            });
        });
        var btn_modal = document.getElementById('btn-modal-solicitud-af');
        btn_modal.click();
    }
    </script>
@endsection