@if(count($solicitudes) > 0)
    <h3><i class="fa fa-btn fa-cog"></i>En proceso</h3>
    <table class="table tablesorter" id="table_enproceso">
        <thead>
        <tr>
            <th>#</th>
            <th>Doc.</th>
            <th>Proyecto</th>
            <th>Solicitante</th>
            <th>Desembolso</th>
            <th>Referencia</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($solicitudes as $solicitud)
        <tr>
            <td>{{ $solicitud->id }}</td>
            <td>{{ $solicitud->nombre_cliente }}</td>
            <td>{{ $solicitud->asignaciones[0]->nombre }}</td>
            <td>
                <button class="btn btn-primary" data-toggle="modal" data-target="#asignarUsuario" data-id="{{ $solicitud->id }}"><i class="fa fa-btn fa-long-arrow-right"></i>Reasignar</button>
            </td>
            <td>{{ $solicitud->direccion }}</td>
            <td>
                @if($solicitud->prioridad->id == 1)
                <span class="label label-danger">Alta</span>
                @elseif($solicitud->prioridad->id == 2)
                <span class="label label-warning">Media</span>
                @elseif($solicitud->prioridad->id == 3)
                <span class="label label-info">Baja</span>
                @else
                <span class="label label-primary">{{ $solicitud->prioridad->descripcion }}</span>
                @endif
            </td>
            <td>{{ $solicitud->created_at->diffForHumans(null, true) }}</td>
            <td>
                <div class="btn-group" role="group">
                    <button class="btn btn-default" data-toggle="modal" data-target="#getTicket" data-id="{{ $solicitud->id }}" title="Ver Ticket"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-default" data-toggle="modal" data-target="#getEventos" data-id="{{ $solicitud->id }}" title="Progreso del Ticket"><i class="fa fa-tasks"></i></button>
                    <button class="btn btn-default" data-toggle="modal" data-target="#cerrarTicket" data-id="{{ $solicitud->id }}" title="Cerrar Ticket"><i class="fa fa-times"></i></button>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-warning" role="alert">
        <i class="fa fa-btn fa-ticket"></i>
        <strong>Mensaje:</strong> No cuenta con solicitudes creadas
    </div>
@endif
