<style>
    .row_fr{
        display: none;
    }
</style>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-md-4">
            <div class="btn btn-purple btn-block" style="margin-bottom: 5px;">
                <strong>GESTIÓN {{ (isset($solicitud->gestion_id))?$solicitud->gestion->gestion:session('gestion') }}</strong>
            </div>
        </div>
        @if (isset($factores))
        <div class="col-md-4 col-md-offset-4">
            <table class="table table-bordered" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th colspan="3">Factores de conversión</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($factores as $factor)
                    @if ($loop->iteration % 3 == 1)
                    <tr>
                    @endif
                        <td>1 {{ $factor->monedaOrigen->sigla }} = {{ $factor->factor }} {{ $factor->monedaDestino->sigla }}</td>
                    @if ($loop->iteration % 3 == 0 || $loop->last)
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="col-md-4">
            <div class="btn btn-orange btn-block">
                <strong id="tipo_cambio_moneda">CAMBIO: $us {{ $cambio['cambio'] }}</strong>
            </div>
        </div>
        <div class="col-md-4">
            <div class="btn btn-orange btn-block">
                <strong id="tipo_cambio_moneda">CAMBIO: € {{ $cambio_euro['cambio'] }}</strong>
            </div>
        </div>
        @endif
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('proyecto') ? ' has-error' : '' }}">
            <label for="nombre" class="col-md-2 control-label">PROYECTO</label>
            <div class="col-md-10">
                @if($controller)
                <select id="proyecto" name="proyecto" class="form-control">
                    @foreach($proyectos as $proyecto)
                        @if($proyecto->padre_id > 0)
                            @if(isset($solicitud->proyecto_id))
                                @if($solicitud->proyecto_id == $proyecto['id'])
                                    <option value='{{ $proyecto['id'] }}' selected>{{ $proyecto['nombre'] }}</option>
                                @else
                                    <option value='{{ $proyecto['id'] }}'>{{ $proyecto['nombre'] }}</option>
                                @endif
                            @endif
                        @endif
                    @endforeach
                </select>
                @else
                <select id="proyecto" name="proyecto" class="form-control select_control" onchange="frProyecto(this)">
                    @foreach($proyectos as $proyecto)
                        @if($proyecto->padre_id > 0)
                            @if(isset($solicitud->proyecto_id))
                                @if($solicitud->proyecto_id == $proyecto->id)
                                    <option value='{{ $proyecto['id'] }}' selected>{{ $proyecto['nombre'] }}</option>
                                @else
                                    <option value='{{ $proyecto['id'] }}'>{{ $proyecto['nombre'] }}</option>
                                @endif
                            @else
                                <option value='{{ $proyecto['id'] }}'>{{ $proyecto['nombre'] }}</option>
                            @endif
                        @endif
                    @endforeach
                </select>
                @endif
                <span class="help-block">
                    <strong>{{ $errors->first('proyecto') }}</strong>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('fecha_lim') ? ' has-error' : '' }}">
            <label for="fecha_lim" class="col-md-4 control-label">FECHA LIMITE</label>

            <div class="col-md-8">
                @if(isset($solicitud->fecha_limite))
                    <input id="fecha_lim" type="date" class="form-control mask-fecha" name="fecha_lim" value="{{ $solicitud->fecha_limite }}" {{($controller)?'readonly=true':''}} max="3000-12-31">
                @else
                    <input id="fecha_lim" type="date" class="form-control mask-fecha" name="fecha_lim" value="{{ old('fecha_lim') }}" {{($controller)?'readonly=true':''}} required="required" max="3000-12-31">
                @endif
                <span class="help-block">
                    <strong>{{ $errors->first('fecha_lim') }}</strong>
                </span>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group{{ $errors->has('usuario') ? ' has-error' : '' }}">
            <label for="usuario" class="col-md-3 control-label">SOLICITANTE</label>

            <div class="col-md-9">
                {!! Form::text('usuario', (isset($solicitud))?$solicitud->usuario->nombre:$user->nombre, array('placeholder' => 'Solicitante','class' => 'form-control', 'readonly' => 'true', 'id' => 'usuario')) !!}

                @if ($errors->has('usuario'))
                    <span class="help-block">
                        <strong>{{ $errors->first('usuario') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('moneda') ? ' has-error' : '' }}">
            <label for="moneda" class="col-md-4 control-label">MONEDA</label>
            <div class="col-md-8">
                <div class="form-control">
                @foreach($monedas as $moneda)
                    @if(isset($solicitud))
                        <input type="radio" id="m1{{($loop->iteration-1)}}" name="moneda" value="{{ $moneda->id }}" {{($loop->iteration==$solicitud->moneda_id)?'checked': ''}} onchange="cambioMoneda(this)" data-id="{{ $moneda->id }}" data-nombre="{{$moneda->nombre}}" data-sigla="{{$moneda->sigla}}"/>
                    @else
                        <input type="radio" id="m1{{($loop->iteration-1)}}" name="moneda" value="{{ $moneda->id }}" {{($loop->iteration==1)?'checked': ''}} onchange="cambioMoneda(this)" data-id="{{ $moneda->id }}" data-nombre="{{$moneda->nombre}}" data-sigla="{{$moneda->sigla}}"/>
                    @endif
                    <label for="m1{{($loop->iteration-1)}}" title="5">{{ $moneda->nombre }}</label>
                @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group{{ $errors->has('modalidad') ? ' has-error' : '' }}">
            <label for="modalidad" class="col-md-3 control-label">MODALIDAD</label>
            <div class="col-md-9">
                @if(isset($solicitud) && ($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4))
                    {!! Form::hidden('modalidad', $solicitud->modalidad_id, array('class' => 'form-control', 'readonly' => 'true', 'id' => 'modalidad')) !!}
                    {!! Form::text('modalidad_nombre', $solicitud->modalidad->nombre, array('class' => 'form-control', 'readonly' => 'true', 'id' => 'modalidad')) !!}
                @else
                    @if(isset($modalidad_transferencia))
                        {!! Form::hidden('modalidad', $modalidad_transferencia->id, array('class' => 'form-control', 'readonly' => 'true', 'id' => 'modalidad')) !!}
                        {!! Form::text('modalidad_nombre', $modalidad_transferencia->nombre, array('class' => 'form-control', 'readonly' => 'true', 'id' => 'modalidad_nombre')) !!}
                    @else
                    <select id="modalidad" name="modalidad" class="form-control select_control" onchange="select_modalidad(this)">
                        <option value="">SELECCIONE UNA MODALIDAD</option>
                        @foreach($modalidades as $mod)
                            @if(isset($solicitud->modalidad_id))
                                @if($solicitud->modalidad_id == $mod->id)
                                    <option value='{{ $mod->id }}' selected>{{ $mod->nombre }}</option>
                                @else
                                    <option value='{{ $mod->id }}'>{{ $mod->nombre }}</option>
                                @endif
                            @else
                                <option value='{{ $mod->id }}'>{{ $mod->nombre }}</option>
                            @endif
                        @endforeach
                        </select>
                    @endif
                @endif
                <span class="help-block">
                    <strong>{{ $errors->first('modalidad') }}</strong>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-md-offset-6" id="div_empresa_fr" style="{{ (isset($solicitud) && $solicitud->modalidad_id == 3)?'display: block;':'display: none;' }}">
        <div class="form-group{{ $errors->has('empresa_fr') ? ' has-error' : '' }}">
            <label for="empresa_fr" class="col-md-3 control-label">FONDO</label>
            <div class="col-md-9">
                <select id="empresa_fr" name="empresa_fr" class="form-control select_control"></select>
                <span class="help-block">
                    <strong>{{ $errors->first('empresa_fr') }}</strong>
                </span>
            </div>
        </div>
    </div>
</div>

@if (!isset($modalidad_transferencia))
<div class="row">
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('desembolso') ? ' has-error' : '' }}">
            <label for="usuario" class="col-md-2 control-label">DESEMBOLSO A </label>

            <div class="col-md-10">
                {!! Form::text('desembolso', null, array('placeholder' => 'NOMBRE COMPLETO O NOMBRE DE LA EMPRESA A QUIEN VA DIRIGIDO EL DESEMBOLSO','class' => 'form-control', 'id' => 'desembolso', 'style' => 'text-transform:uppercase', 'required' => 'true', 'maxlength' => '200')) !!}
                <span class="help-block">
                    <strong>{{ $errors->first('desembolso') }}</strong>
                </span>
            </div>
        </div>
    </div>
</div>
@else
{!! Form::hidden('desembolso', 'TRANSFERENCIA', array('class' => 'form-control', 'readonly' => 'true', 'id' => 'desembolso')) !!}
@endif

<div class="col-md-12">
    <div class="form-group">
        <div class="col-md-4">
            <label for="doc" class="custom-file-upload" style="display: inline;">
                DOCUMENTO(S) ADJUNTO(S)
                <i class="fa fa-plus-circle icono-opciones-files icono-green" id="adjuntar-archivo" onclick="addFileInput()" style="display: inline;"></i>
            </label>
            <span id="documentos_error" class="help-block" style="color: #a94442;">
                <strong></strong>
            </span>
            <div id="moreUploadsLink"></div>
        </div>
        <div id="documentos_sol" class="col-md-8">
            @if(isset($documentos))
            <ul style="list-style-type: none; padding-left: 0px;">
                @foreach($documentos as $documento)
                    <li style="font-weight: bold; font-size: 14px;" id="doc_{{ $documento->id }}" documento-nombre="{{ $documento->direccion }}">{{ $documento->direccion }} <a class="fa fa-times-circle-o icono-opciones-files icono-red" onclick="borrarDocumentoModal({{ $documento->id }})"></a></li>
                @endforeach
            </ul>
            @endif
            {{ form::file('doc1', ["class" => 'inputfile', "value" => " old('doc') ", "id"=>'doc1', 'onchange' => 'cambio_input(this)', 'style' => 'display:none']) }}
            <input type="file" name="doc1" id="doc1" onchange="cambio_input(this)" style="display:none" />

            <div id="moreUploads"></div>
        </div>
        @if ($errors->has('doc'))
            <span class="help-block">
                <strong>{{ $errors->first('doc') }}</strong>
            </span>
        @endif

    </div>
</div>
{{ Form::hidden('cambio_id', $cambio['id'], array('id' => 'cambio_id')) }}

<button type="button" id="btn-modal-mensaje" data-toggle="modal" data-target="#fechaLimiteModal" style="display:none;">Modal Mensaje</button>
<div id="fechaLimiteModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">MENSAJE DEL SISTEMA</h4>
            </div>
            <div class="modal-body">
                <h2 id="mensaje-fecha-invalida">FECHA INVALIDA</h2>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>

<!-- MODAL REVIEW -->
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="reviewModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-danger"><strong>VERIFIQUE LA INFORMACIÓN</strong></h4>
            </div>
            <div class="modal-body">
                <span>La información que intenta enviar cuenta con algunas observaciones, por favor verifique la misma.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<!-- MODAL PREVIEW DOCS-->
<div class="modal fade" id="previewDocModal" tabindex="-1" role="dialog" aria-labelledby="previewDocModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success"><strong>VISTA PREVIA DOCUMENTO</strong></h4>
            </div>
            <div class="modal-body">
                <div id="previewDocDiv"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
<!-- MODAL ELIMINAR DOC-->
<div class="modal fade" id="eliminarDocModal" tabindex="-1" role="dialog" aria-labelledby="eliminarDocModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success"><strong>ELIMINAR DOCUMENTO</strong></h4>
            </div>
            <div class="modal-body">
                <div id="eliminarDocDiv"></div>
                <div id="ver_doc"></div>
            </div>
            <div class="modal-footer" id="eliminarDocModalFooter">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->


<!-- MODAL IMPORT EXCEL ERRORS -->
<div class="modal fade" id="excelErrorModal" tabindex="-1" role="dialog" aria-labelledby="excelErrorModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center text-success"><strong>OBSERVACIONES EN EL ARCHIVO DE EXCEL</strong></h4>
            </div>
            <div class="modal-body">
                ERROR
            </div>
            <div class="modal-footer" id="excelErrorModalFooter">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL IMPORT EXCEL ERRORS -->

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    $(document).ready(function(){
        var proy_init = document.getElementById("proyecto");
        frProyecto(proy_init);

        cambioMoneda(this);

        @if(isset($solicitud) && ($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4))
            $('div#seccion_excel').css('display', 'block');

            $('.row_fr').css('display', 'table-cell');

            $('table#solicitud-items').css('width', '1800px', 'important');
            $('table#solicitud-items-total').css('width', '1800px', 'important');

            document.getElementById('empresa_fr').selectedIndex = {{ $solicitud->modalidad_id }};

            $('select[id^="tipo_compra_"] option[value=1]').removeAttr('selected').hide().attr('disabled', 'disabled');
            $('select[id^="tipo_compra_"] option[value=7]').removeAttr('selected').hide().attr('disabled', 'disabled');
        @endif
    });

    // Verificación si la modalidad es Fondo Rotativo
    var strModalidad = 0;
    function select_modalidad(ele){
        var e = document.getElementById(ele.id);
        strModalidad = e.options[e.selectedIndex].value;
        console.log(strModalidad);
        if(strModalidad == 3 || $('input[name="modalidad"]').val() == 3 ||
            strModalidad == 4 || $('input[name="modalidad"]').val() == 4){
            $('div#seccion_excel').css('display', 'block');
            
            if(strModalidad == 3 || $('input[name="modalidad"]').val() == 3){
                $('div#div_empresa_fr').css('display', 'block');
            }else{
                $('div#div_empresa_fr').css('display', 'none');
            }

            $('.row_fr').css('display', 'table-cell');

            $('select[id^="tipo_compra_"] option[value=1]').removeAttr('selected').hide().attr('disabled', 'disabled');
            $('select[id^="tipo_compra_"] option[value=7]').removeAttr('selected').hide().attr('disabled', 'disabled');

            $('table#solicitud-items').css('width', '1800px', 'important');
            $('table#solicitud-items-total').css('width', '1800px', 'important');
        }else{
            $('div#seccion_excel').css('display', 'none');
            
            $('div#div_empresa_fr').css('display', 'none');

            $('.row_fr').css('display', 'none');

            $('select[id^="tipo_compra_"] option[value=1]').show();
            $('select[id^="tipo_compra_"] option[value=7]').show();

            $('table#solicitud-items').css('width', '100%', 'important');
            $('table#solicitud-items-total').css('width', '100%', 'important');
        }
    }

    //Subimos primer archivo
    function cambio_input(e){
        if(e.id == 'doc1'){
            var fullPath;
            fullPath = document.getElementById(e.id).value;
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            document.getElementById('elegir-archivo').style.display = 'none';
            var primer_archivo = document.getElementById('primer-archivo');
            primer_archivo.innerHTML = '<span style="font-size:14px; font-weight: bold;">'+filename+'</span>'+' <i class="fa fa-times-circle-o icono-opciones icono-red" onclick="borrarArchivo(this)" id="eli1"></i>';
        }else{
            fullPath = document.getElementById(e.id).value;
            if(fullPath){
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            var doc_tam = (e.id).length;
            var doc_val = (e.id).substr(3, doc_tam);
            document.getElementById('archivo'+doc_val).style.display = 'none';
            var archivos = document.getElementById('docnro'+doc_val);
            var a_tag = document.createElement('a');
            a_tag.setAttribute("class", "fa fa-times-circle-o icono-opciones-files icono-red");
            a_tag.setAttribute("onclick","borrarArchivo(this)");
            a_tag.setAttribute("id", "eli"+doc_val);

            var preview_btn = document.createElement('a');
            preview_btn.setAttribute("class", "fa fa-search icono-opciones-files icono-green");
            preview_btn.setAttribute("onclick","previewDoc(this)");
            preview_btn.setAttribute("id", "prev"+doc_val);

            var span_tag = document.createElement('span');
            span_tag.setAttribute("style", "font-size:14px; font-weight: bold;");
            span_tag.innerHTML = filename+" ";
            span_tag.appendChild(a_tag);
            archivos.appendChild(span_tag);
        }
    }

    //Añadir archivos
    var upload_number = 2;

    function addFileInput() {
        var d = document.createElement("div");
        d.setAttribute('id', 'docnro'+upload_number);
        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "doc" + upload_number);
        file.setAttribute("id", "doc" + upload_number);
        file.setAttribute("onchange", "cambio_input(this)");
        file.setAttribute("style", "display:none");
        d.appendChild(file);

        var file_add = document.createElement('div');
        file_add.setAttribute('id', 'archivo' + upload_number);
        file_add.setAttribute('value', upload_number);
        file_add.setAttribute('style', 'display:none;');
        file_add.innerHTML = "<i class='fa fa-upload icono-opciones icono-blue'></i> ";
        file_add.setAttribute('onclick', 'elegirArchivo(this)');
        d.appendChild(file_add);
        document.getElementById("moreUploads").appendChild(d);
        document.getElementById("archivo"+upload_number).click();
        upload_number++;
    }
    //Eliminar Archivo
    function borrarArchivo(eli) {
        var doc_tam = (eli.id).length;
        var doc_val = (eli.id).substring(3, doc_tam);

        //***HACER
        if(doc_val == 1){
            var element = document.getElementById("primer-archivo");
        }else{
            var element = document.getElementById("docnro"+doc_val);
        }
        element.parentNode.removeChild(element);
    }

    //borrar Documento
    function borrarDocumentoModal(documento_id){
        var doc = document.getElementById('ver_doc');
        $('#eliminarDocModal').modal('toggle');
        var doc_nombre = document.getElementById('doc_'+documento_id).getAttribute('documento-nombre');

        //eliminarDocDiv
        document.getElementById('eliminarDocDiv').innerHTML = "<p style='font-weight: bold;'>EL DOCUMENTO '"+doc_nombre+"' SERA ELIMINADO<br> NO PODRA SER RECUPERADO AL PRESIONAR EL BOTON DE ELIMINAR!!!</p>";

        var documento = doc_nombre;
        var array_doc = documento.split('.');
        var url;
        if(array_doc.length == 2){
            switch(array_doc[1]){
                case 'jpg':
                    url = "/empresas/{{session('empresa_slug')}}/{{session('gestion')}}/solicitud/"+documento;
                    break;
                case 'jpeg':
                    url = "/empresas/{{session('empresa_slug')}}/{{session('gestion')}}/solicitud/"+documento;
                    break;
                case 'png':
                    url = "/empresas/{{session('empresa_slug')}}/{{session('gestion')}}/solicitud/"+documento;
                    break;
                case 'bmp':
                    url = "/empresas/{{session('empresa_slug')}}/{{session('gestion')}}/solicitud/"+documento;
                    break;
                case 'pdf':
                    url = "/ViewerJS/#../empresas/{{session('empresa_slug')}}/{{session('gestion')}}/solicitud/"+documento;
                    break;
                default:
                    url = "";
            }
        }
        if(url != ""){
            doc.innerHTML = "<iframe src='"+url+"' width='100%' height='500px'></iframe>";
        }else{
            doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
        }
        doc.style.display = 'block';

        var d = document.createElement("eliminarDocModalFooter");
        $("#eliminarDocModalFooter").empty();

        var btn_eliminar = document.createElement("button");
        btn_eliminar.setAttribute("type", "button");
        btn_eliminar.setAttribute("type", "button");
        btn_eliminar.setAttribute("class", "btn btn-primary");
        btn_eliminar.setAttribute("data-dismiss", "modal");
        btn_eliminar.setAttribute("onclick", "borrarDocumento("+documento_id+")");
        btn_eliminar.innerHTML = "ELIMINAR";
        d.appendChild(btn_eliminar);

        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        d.appendChild(btn_cerrar);

        document.getElementById("eliminarDocModalFooter").appendChild(d);

    }

    function borrarDocumento(documento_id){
        $("#doc_"+documento_id).remove();

        var url = "/json/documento/eliminar/" + documento_id;
        $.ajax({
          url: url,
          data: {'file' : " "},
          success: function (response) {
             // do something
          },
          error: function () {
             // do something
          }
        });
    }

    //Fila Archivos
    var adjuntarArchivo = document.getElementById('adjuntar-archivo');
    var cont_archivos = 0;
    function elegirArchivo(el){
        var btn_elegir_imagen;
        if(el.id == 'elegir-archivo'){
            btn_elegir_imagen = document.getElementById('doc1');
            btn_elegir_imagen.click();
        }else{
            var tam = (el.id).length;
            var valor = (el.id).substring(7, tam);
            btn_elegir_imagen = document.getElementById('doc'+valor);
            btn_elegir_imagen.click();
        }
    }

    function validaFechaLim(e){
        var today = moment().format('YYYY-MM-DD');
        var fecha_sel = document.getElementById("fecha_lim").value;
        var anio_valido = parseInt(fecha_sel.substring(0,3));
        if(fecha_sel.length == 10 && anio_valido >= 201){
            if(fecha_sel < today){
                document.getElementById('fecha_lim').value = "";
                var btn_modal = document.getElementById('btn-modal-mensaje');
                btn_modal.click();
                document.getElementById('mensaje-fecha-invalida').innerHTML = "<i style='color: red; font-weight: bold'>Fecha "+fecha_sel+" no válida, la fecha límite debe ser posterior o igual a la fecha actual ("+today+")</i>";
            }
        }
    }

    function cambioMoneda(e){
        var valorId = document.querySelector('input[name="moneda"]:checked').id;
        var Sigla = document.getElementById(valorId).getAttribute('data-sigla');
        document.getElementById("monedaSiglaTag").innerHTML = Sigla;

        var monedaId = document.getElementById(valorId).getAttribute('data-id');
    }

    function creaFila() {
        var tabla = document.getElementById("solicitud-items").tBodies[0];
        var fila = document.createElement("TR");
        fila.setAttribute("align", "center");

        var counter = document.createElement("INPUT");
        counter.setAttribute("type", "hidden");
        counter.setAttribute("value", contador);
        counter.setAttribute("name", "counter");
        fila.appendChild(counter);

        //CELDA NUMERO
        var celda1 = document.createElement("TD");
        celda1.setAttribute("style", "padding-top: 7px!important;");
        var numero = document.createElement("spam");
        numero.setAttribute("style", "text-align:center; font-size:14px;");
        var indice = contador + 1;
        var numero1 = document.createTextNode(indice);

        numero.appendChild(numero1);
        celda1.appendChild(numero);

        //CELDA CANTIDAD
        var celda2 = document.createElement("TD");
        var cantidad = document.createElement("INPUT");
        cantidad.setAttribute("type", "text");
        cantidad.setAttribute("min", "0");
        cantidad.setAttribute("step", "0.00001");
        cantidad.setAttribute("id", "cantidad_" + contador);
        cantidad.setAttribute("class", "form-control mask-cantidad");
        cantidad.setAttribute("name", "cantidad[]");
        cantidad.setAttribute("onchange", "subtotal_cantidad(this.id)");
        cantidad.setAttribute("onkeyup", "subtotal_cantidad(this.id)");
        cantidad.setAttribute("onkeypress", "return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode != 44)");
        celda2.appendChild(cantidad);

        //CELDA UNIDAD
        var celda3 = document.createElement("TD");
        var unidad = document.createElement("SELECT");
        unidad.setAttribute("size", "1");
        unidad.setAttribute("class", "form-control");
        unidad.setAttribute("id", "unidad_" + contador);
        unidad.setAttribute("name", "unidad[]");
        <?php
        $unidades = App\Unidades::all();
        foreach ($unidades as $unidad) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $unidad->descripcion . "';";
            echo "opcioncur.value = '" . $unidad->id . "';";
            echo "unidad.appendChild(opcioncur);";
        }
        ?>
        celda3.appendChild(unidad);

        //CELDA TIPO COMPRA
        var celda4 = document.createElement("TD");
        var tipo_compra = document.createElement("SELECT");
        tipo_compra.setAttribute("size", "1");
        tipo_compra.setAttribute("class", "form-control");
        tipo_compra.setAttribute("id", "tipo_compra_" + contador);
        tipo_compra.setAttribute("name", "tipo_compra[]");
        <?php
        $tipo_compraes = App\ItemsTipos::orderBy('descripcion')->get();
        echo "opcioncur = document.createElement('OPTION');";
        echo "opcioncur.innerHTML = 'SELECCIONE TIPO DE COMPRA';";
        echo "opcioncur.value = '';";
        echo "tipo_compra.appendChild(opcioncur);";
        foreach ($tipo_compraes as $tipo_compra) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $tipo_compra->descripcion . "';";
            echo "opcioncur.value = '" . $tipo_compra->id . "';";
            echo "tipo_compra.appendChild(opcioncur);";
        }
        ?>
        celda4.appendChild(tipo_compra);

        //CELDA DETALLE
        var celda5 = document.createElement("TD");
        var detalle = document.createElement("INPUT");
        detalle.setAttribute("type", "text");
        detalle.setAttribute("placeholder", "Detalle del item");
        detalle.setAttribute("class", "form-control");
        detalle.setAttribute("size", "40"); //22
        detalle.setAttribute("maxlength", "70");
        detalle.setAttribute("id", "detalle_" + contador);
        detalle.setAttribute("name", "detalle[]");
        detalle.setAttribute("style", "text-transform:uppercase;");
        celda5.appendChild(detalle);

        //CELDA Nro. Factura
        var celda51 = document.createElement('td');
        celda51.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda51.setAttribute('style', 'display: table-cell');
        }

        var nro_factura = document.createElement('input');
        nro_factura.setAttribute('type', 'number');
        nro_factura.setAttribute("class", "form-control");
        nro_factura.setAttribute("min", "0");
        nro_factura.setAttribute("id", "nrofactura_" + contador);
        nro_factura.setAttribute("name", "nrofactura[]");
        celda51.appendChild(nro_factura);

        // CELDA Fecha Factura
        var celda52 = document.createElement('td');
        celda52.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda52.setAttribute('style', 'display: table-cell');
        }

        var fecha_factura = document.createElement('input');
        fecha_factura.setAttribute('type', 'date');
        fecha_factura.setAttribute("class", "form-control mask-fecha");
        fecha_factura.setAttribute("id", "fechafactura_" + contador);
        fecha_factura.setAttribute("name", "fechafactura[]");
        celda52.appendChild(fecha_factura);

        //CELDA Proyecto
        var celda53 = document.createElement('td');
        celda53.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda53.setAttribute('style', 'display: table-cell');
        }

        var proyecto = document.createElement('input');
        proyecto.setAttribute('type', 'text');
        proyecto.setAttribute("class", "form-control");
        proyecto.setAttribute("id", "itemproyecto_" + contador);
        proyecto.setAttribute("name", "itemproyecto[]");
        proyecto.setAttribute("style", "text-transform:uppercase;");
        celda53.appendChild(proyecto);

        //CELDA Proveedor
        var celda54 = document.createElement('td');
        celda54.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda54.setAttribute('style', 'display: table-cell');
        }

        var proveedor = document.createElement('input');
        proveedor.setAttribute('type', 'text');
        proveedor.setAttribute("class", "form-control");
        proveedor.setAttribute("id", "itemproveedor_" + contador);
        proveedor.setAttribute("name", "itemproveedor[]");
        proveedor.setAttribute("style", "text-transform:uppercase;");
        celda54.appendChild(proveedor);

        //CELDA TIPO
        var celda55 = document.createElement("td");
        celda55.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda55.setAttribute('style', 'display: table-cell');
        }

        var tipo = document.createElement("select");
        tipo.setAttribute("size", "1");
        tipo.setAttribute("class", "form-control");
        tipo.setAttribute("id", "itemtipo_" + contador);
        tipo.setAttribute("name", "itemtipo[]");

        // TIPO opciones
        var opt1 = document.createElement('option');
        opt1.value = 'FACTURA';
        opt1.innerHTML = 'FACTURA';
        tipo.appendChild(opt1);

        var opt2 = document.createElement('option');
        opt2.value = 'RECIBO';
        opt2.innerHTML = 'RECIBO';
        tipo.appendChild(opt2);

        celda55.appendChild(tipo);

        console.log(celda55);

        //CELDA P-UNITARIO
        var celda6 = document.createElement("TD");
        var punitario = document.createElement("INPUT");
        punitario.setAttribute("type", "text");
        punitario.setAttribute("id", "punitario_" + contador);
        punitario.setAttribute("class", "form-control mask-decimal");
        punitario.setAttribute("name", "punitario[]");
        punitario.setAttribute("style", "text-transform:uppercase; text-align: right");
        punitario.setAttribute("onchange", "subtotal_precio(this.id)");
        punitario.setAttribute("onkeyup", "subtotal_precio(this.id)");
        celda6.appendChild(punitario);

        //CELDA SUB-TOTAL
        var celda7 = document.createElement("TD");
        var subtotal = document.createElement("INPUT");
        subtotal.setAttribute("type", "text");
        subtotal.setAttribute("class", "form-control");
        subtotal.setAttribute("size", "6");
        subtotal.setAttribute("maxlength", "10");
        subtotal.setAttribute("name", "subtotal[]");
        subtotal.setAttribute("style", "text-transform:uppercase; text-align: right");
        subtotal.setAttribute("id", "subtotal_" + contador);
        subtotal.setAttribute("ReadOnly", true);
        subtotal.setAttribute("onchange", "total_precio()");
        celda7.appendChild(subtotal);

        //BOTON ELIMINAR FILA
        var celda8 = document.createElement('TD');
        var tipoboton = document.createElement('div');
        tipoboton.setAttribute('class', 'botoneliminar');
        var eliminar = document.createElement('i');
        eliminar.setAttribute('class', 'fa fa-times-circle-o icono-opciones icono-red');
        eliminar.setAttribute('value', 'Quitar');
        tipoboton.appendChild(eliminar);
        tipoboton.onclick = function () {
            borrarFila(this);
            total_precio();
        }
        celda8.appendChild(tipoboton);

        fila.appendChild(celda1);
        fila.appendChild(celda2);
        fila.appendChild(celda3);
        fila.appendChild(celda4);
        fila.appendChild(celda5);
        fila.appendChild(celda51);
        fila.appendChild(celda52);
        fila.appendChild(celda53);
        fila.appendChild(celda54);
        fila.appendChild(celda55);
        fila.appendChild(celda6);
        fila.appendChild(celda7);
        fila.appendChild(celda8);
        tabla.appendChild(fila);
        contador = contador + 1;
        mascara();

        $('select[id^="tipo_compra_"] option[value=8]').removeAttr('selected').hide().attr('disabled', 'disabled');
        $('select[id^="tipo_compra_"] option[value=9]').removeAttr('selected').hide().attr('disabled', 'disabled');
        if(strModalidad == 3 ||strModalidad == 4){
            $('select[id^="tipo_compra_"] option[value=1]').removeAttr('selected').hide().attr('disabled', 'disabled');
            $('select[id^="tipo_compra_"] option[value=7]').removeAttr('selected').hide().attr('disabled', 'disabled');
        }else if($('input[name="modalidad"]').val() == 3 || $('input[name="modalidad"]').val() == 4){
            $('select[id^="tipo_compra_"] option[value=1]').removeAttr('selected').hide().attr('disabled', 'disabled');
            $('select[id^="tipo_compra_"] option[value=7]').removeAttr('selected').hide().attr('disabled', 'disabled');
        }else{
            $('select[id^="tipo_compra_"] option[value=1]').show();
            $('select[id^="tipo_compra_"] option[value=7]').show();
        }
    }

    function creaFilaDatos(oCantidad, oUnidad, oTipoCompra, oDetalle, oNroFactura, oFechaFactura, oProyecto, oProveedor, oTipo, oPrecioUnitario) {
        var tabla = document.getElementById("solicitud-items").tBodies[0];

        var fila = document.createElement("TR");
        fila.setAttribute("align", "center");

        var counter = document.createElement("INPUT");
        counter.setAttribute("type", "hidden");
        counter.setAttribute("value", contador);
        counter.setAttribute("name", "counter");
        fila.appendChild(counter);

        //CELDA NUMERO
        var celda1 = document.createElement("TD");
        celda1.setAttribute("style", "padding-top: 7px!important;");
        var numero = document.createElement("spam");
        numero.setAttribute("style", "text-align:center; font-size:14px;");
        var indice = contador + 1;
        var numero1 = document.createTextNode(indice);

        numero.appendChild(numero1);
        celda1.appendChild(numero);

        //CELDA CANTIDAD
        var celda2 = document.createElement("TD");
        var cantidad = document.createElement("INPUT");
        cantidad.setAttribute("type", "text");
        cantidad.setAttribute("min", "0");
        cantidad.setAttribute("step", "0.00001");
        cantidad.setAttribute("id", "cantidad_" + contador);
        cantidad.setAttribute("class", "form-control mask-cantidad");
        cantidad.setAttribute("name", "cantidad[]");
        cantidad.setAttribute("onchange", "subtotal_cantidad(this.id)");
        cantidad.setAttribute("onkeyup", "subtotal_cantidad(this.id)");
        cantidad.setAttribute("onkeypress", "return ((event.charCode >= 48 && event.charCode <= 57) || event.charCode != 44)");
        cantidad.value = oCantidad;
        celda2.appendChild(cantidad);


        //CELDA UNIDAD
        var celda3 = document.createElement("TD");
        var unidad = document.createElement("SELECT");
        unidad.setAttribute("size", "1");
        unidad.setAttribute("class", "form-control");
        unidad.setAttribute("id", "unidad_" + contador);
        unidad.setAttribute("name", "unidad[]");
        <?php
        $unidades = App\Unidades::all();
        foreach ($unidades as $unidad) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $unidad->descripcion . "';";
            echo "opcioncur.value = '" . $unidad->id . "';";
            echo "unidad.appendChild(opcioncur);";
        }
        ?>
        unidad.value = oUnidad;
        celda3.appendChild(unidad);

        //CELDA TIPO COMPRA
        var celda4 = document.createElement("TD");
        var tipo_compra = document.createElement("SELECT");
        tipo_compra.setAttribute("size", "1");
        tipo_compra.setAttribute("class", "form-control");
        tipo_compra.setAttribute("id", "tipo_compra_" + contador);
        tipo_compra.setAttribute("name", "tipo_compra[]");
        <?php
        $tipo_compraes = App\ItemsTipos::orderBy('descripcion')->get();
        echo "opcioncur = document.createElement('OPTION');";
        echo "opcioncur.innerHTML = 'SELECCIONE TIPO DE COMPRA';";
        echo "opcioncur.value = '';";
        echo "tipo_compra.appendChild(opcioncur);";
        foreach ($tipo_compraes as $tipo_compra) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $tipo_compra->descripcion . "';";
            echo "opcioncur.value = '" . $tipo_compra->id . "';";
            echo "tipo_compra.appendChild(opcioncur);";
        }
        ?>
        tipo_compra.value = oTipoCompra;
        celda4.appendChild(tipo_compra);

        //CELDA DETALLE
        var celda5 = document.createElement("TD");
        var detalle = document.createElement("INPUT");
        detalle.setAttribute("type", "text");
        detalle.setAttribute("placeholder", "Detalle del item");
        detalle.setAttribute("class", "form-control");
        detalle.setAttribute("size", "40"); //22
        detalle.setAttribute("maxlength", "70");
        detalle.setAttribute("id", "detalle_" + contador);
        detalle.setAttribute("name", "detalle[]");
        detalle.setAttribute("style", "text-transform:uppercase;");
        detalle.value = oDetalle;
        celda5.appendChild(detalle);

        //CELDA Nro. Factura
        var celda51 = document.createElement('td');
        celda51.setAttribute('class', 'row_fr');

        if($('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 || $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4){
            celda51.setAttribute('style', 'display: table-cell');
        }

        var nro_factura = document.createElement('input');
        nro_factura.setAttribute('type', 'number');
        nro_factura.setAttribute("class", "form-control");
        nro_factura.setAttribute("min", "0");
        nro_factura.setAttribute("id", "nrofactura_" + contador);
        nro_factura.setAttribute("name", "nrofactura[]");
        nro_factura.value = oNroFactura;
        celda51.appendChild(nro_factura);

        // CELDA Fecha Factura
        var celda52 = document.createElement('td');
        celda52.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 || $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda52.setAttribute('style', 'display: table-cell');
        }

        var fecha_factura = document.createElement('input');
        fecha_factura.setAttribute('type', 'date');
        fecha_factura.setAttribute("class", "form-control mask-fecha");
        fecha_factura.setAttribute("id", "fechafactura_" + contador);
        fecha_factura.setAttribute("name", "fechafactura[]");

        fecha_factura.value = oFechaFactura.split('-')[0] +'-'+ oFechaFactura.split('-')[1] +'-'+ oFechaFactura.split('-')[2].split(' ')[0];

        celda52.appendChild(fecha_factura);

        //CELDA Proyecto
        var celda53 = document.createElement('td');
        celda53.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda53.setAttribute('style', 'display: table-cell');
        }

        var proyecto = document.createElement('input');
        proyecto.setAttribute('type', 'text');
        proyecto.setAttribute("class", "form-control");
        proyecto.setAttribute("id", "itemproyecto_" + contador);
        proyecto.setAttribute("name", "itemproyecto[]");
        proyecto.setAttribute("style", "text-transform:uppercase;");

        proyecto.value = oProyecto;

        celda53.appendChild(proyecto);

        //CELDA Proveedor
        var celda54 = document.createElement('td');
        celda54.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda54.setAttribute('style', 'display: table-cell');
        }

        var proveedor = document.createElement('input');
        proveedor.setAttribute('type', 'text');
        proveedor.setAttribute("class", "form-control");
        proveedor.setAttribute("id", "itemproveedor_" + contador);
        proveedor.setAttribute("name", "itemproveedor[]");
        proveedor.setAttribute("style", "text-transform:uppercase;");

        proveedor.value = oProveedor;

        celda54.appendChild(proveedor);

        //CELDA TIPO
        var celda55 = document.createElement("td");
        celda55.setAttribute('class', 'row_fr');

        if(
            $('#modalidad option:selected').val() == 3 || $('#modalidad option:selected').val() == 4 ||
            $('input#modalidad').val() == 3 || $('input#modalidad').val() == 4
        ){
            celda55.setAttribute('style', 'display: table-cell');
        }

        var tipo = document.createElement("select");
        tipo.setAttribute("size", "1");
        tipo.setAttribute("class", "form-control");
        tipo.setAttribute("id", "itemtipo_" + contador);
        tipo.setAttribute("name", "itemtipo[]");

        // TIPO opciones
        var opt1 = document.createElement('option');
        opt1.value = 'FACTURA';
        opt1.innerHTML = 'FACTURA';
        tipo.appendChild(opt1);

        var opt2 = document.createElement('option');
        opt2.value = 'RECIBO';
        opt2.innerHTML = 'RECIBO';
        tipo.appendChild(opt2);

        tipo.value = oTipo;

        celda55.appendChild(tipo);

        //CELDA P-UNITARIO
        var celda6 = document.createElement("TD");
        var punitario = document.createElement("INPUT");
        punitario.setAttribute("type", "text");
        punitario.setAttribute("id", "punitario_" + contador);
        punitario.setAttribute("class", "form-control mask-decimal");
        punitario.setAttribute("name", "punitario[]");
        punitario.setAttribute("style", "text-transform:uppercase; text-align: right");
        punitario.setAttribute("onchange", "subtotal_precio(this.id)");
        punitario.setAttribute("onkeyup", "subtotal_precio(this.id)");
        punitario.value = oPrecioUnitario;
        celda6.appendChild(punitario);

        //CELDA SUB-TOTAL
        var celda7 = document.createElement("TD");
        var subtotal = document.createElement("INPUT");
        subtotal.setAttribute("type", "text");
        subtotal.setAttribute("class", "form-control");
        subtotal.setAttribute("size", "6");
        subtotal.setAttribute("maxlength", "10");
        subtotal.setAttribute("name", "subtotal[]");
        subtotal.setAttribute("style", "text-transform:uppercase; text-align: right");
        subtotal.setAttribute("id", "subtotal_" + contador);
        subtotal.setAttribute("ReadOnly", true);
        subtotal.setAttribute("onchange", "total_precio()");

        celda7.appendChild(subtotal);

        //BOTON ELIMINAR FILA
        var celda8 = document.createElement('TD');
        var tipoboton = document.createElement('div');
        tipoboton.setAttribute('class', 'botoneliminar');
        var eliminar = document.createElement('i');
        eliminar.setAttribute('class', 'fa fa-times-circle-o icono-opciones icono-red');
        eliminar.setAttribute('value', 'Quitar');
        tipoboton.appendChild(eliminar);
        tipoboton.onclick = function () {
            borrarFila(this);
            total_precio();
        }
        celda8.appendChild(tipoboton);

        fila.appendChild(celda1);
        fila.appendChild(celda2);
        fila.appendChild(celda3);
        fila.appendChild(celda4);
        fila.appendChild(celda5);

        fila.appendChild(celda51);
        fila.appendChild(celda52);
        fila.appendChild(celda53);
        fila.appendChild(celda54);
        fila.appendChild(celda55);

        fila.appendChild(celda6);
        fila.appendChild(celda7);
        fila.appendChild(celda8);

        tabla.appendChild(fila);

        subtotal_precio("punitario_" + contador);


        contador = contador + 1;
        mascara();

        $('select[id^="tipo_compra_"] option[value=8]').removeAttr('selected').hide().attr('disabled', 'disabled');
        $('select[id^="tipo_compra_"] option[value=9]').removeAttr('selected').hide().attr('disabled', 'disabled');
    }

    function mayuscula_detalle(id) {
        var detalle_str = document.getElementById(id);
        var detalle = detalle_str.value;
        detalle = detalle.toUpperCase();
        detalle_str.value = detalle;
    }

    function subtotal_cantidad(id) {
        var res_array = id.split('_');
        var sub;
        var cant;
        var precio = document.getElementById('punitario_' + res_array[1]);
        if (precio.value != '') {
            sub = document.getElementById('subtotal_' + res_array[1]);
            cant = document.getElementById('cantidad_' + res_array[1]);
            sub.value = ((cant.value) * (precio.value)).toFixed(2);
            total_precio();
        }
    }

    function subtotal_precio(id) {
        var res_array = id.split('_');
        var sub;
        var cant;
        var precio = document.getElementById('cantidad_' + res_array[1]);
        if (precio.value != '') {
            sub = document.getElementById('subtotal_' + res_array[1]);
            cant = document.getElementById('punitario_' + res_array[1]);

            sub.value = ((cant.value) * (precio.value)).toFixed(2);
            total_precio();
        }
    }

    function total_precio() {
        var total;
        total = 0;
        var element;
        //***REVISAR CONTADOR
        for (i = 0; i < 300; i++) {
            element = (document.getElementById('subtotal_' + i));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                total += parseFloat(element.value);
            }
        }
        total = total.toFixed(2);
        document.getElementById("total").innerHTML = total;
    }

    function borrarFila(button) {
        var fila = button.parentNode.parentNode;
        var tabla = document.getElementById('solicitud-items').getElementsByTagName('tbody')[0];
        tabla.removeChild(fila);
    }

    function datosModal() {
        $('span#table-items strong').html('');
        var cerrarModal = false;
        var mod_sel = document.getElementById("modalidad");
        if(mod_sel.tagName === 'SELECT') {
            var x = document.getElementById(mod_sel.id).selectedIndex;
            var y = document.getElementById(mod_sel.id).options;
            if (parseInt(y[x].value) == 3){
                var proy_init = document.getElementById("empresa_fr");
                var x1 = document.getElementById(proy_init.id).selectedIndex;
                var y1 = document.getElementById(proy_init.id).options;
                if(y1[x1].value == 0){
                    $('select[name="empresa_fr"]').parent().parent().addClass('has-error');
                    $('select[name="empresa_fr"]').siblings('span.help-block').children().html('Debe agregar un fondo rotativo al proyecto');
                    cerrarModal = true;
                }else{
                    $('select[name="empresa_fr"]').parent().parent().removeClass('has-error');
                    $('select[name="empresa_fr"]').siblings('span.help-block').children().html('');
                }
            }
        }else{
            var rate_value = $('input[name="modalidad"]').val();
            var proy_init = document.getElementById("empresa_fr");
            var x1 = document.getElementById(proy_init.id).selectedIndex;
            var y1 = document.getElementById(proy_init.id).options;
            if(y1[x1].value == 0 && rate_value == 3){
                $('select[name="empresa_fr"]').parent().parent().addClass('has-error');
                $('select[name="empresa_fr"]').siblings('span.help-block').children().html('Debe agregar un fondo rotativo al proyecto');
                cerrarModal = true;
            }
        }

        //Nombre empresa
        @if (isset($solicitud))
        var mod_empresa = '{{ $solicitud->proyecto->empresa->nombre }}';
        @elseif (\Auth::user()->global_view == 0)
        var mod_empresa = document.getElementById('nom_empresa').innerHTML;
        @else
        var mod_empresa = 'PRAGMA INVEST S.A.';
        @endif

        //Solicitante
        var mod_solicitante = '<div class="text-danger"><strong>NOMBRE DE USUARIO</strong></div>';
        if (document.getElementById('usuario').value) {
            mod_solicitante = document.getElementById('usuario').value;
        }

        //Proyecto
        var mod_proyecto = '<div class="text-danger"><strong>SIN PROYECTO</strong></div>';
        var ele = document.getElementById('proyecto');
        mod_proyecto = ele.options[ele.selectedIndex].innerHTML;

        //Fecha Limite
        var mod_fecha_lim = '<div class="text-danger"><strong>SIN FECHA LIMITE</strong></div>';
        var fecha_limite = new Date(document.getElementById('fecha_lim').value);
        // var fecha_comparar = new Date(fecha_limite.split('/').reverse().join('-'));
        var fecha_actual = new Date('{{ $date }}');

        
        if (fecha_limite && !isNaN(fecha_limite)) {
            mod_fecha_lim = document.getElementById('fecha_lim').value;

            if(fecha_limite < fecha_actual){
                @if(isset($solicitud))
                @if(Auth::user()->revisor != 1)
                $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
                $('input[name="fecha_lim"]').siblings('span.help-block').children().html('La fecha límite debe ser posterior a la fecha actual');
                cerrarModal=true;
                @endif
                @else
                $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
                $('input[name="fecha_lim"]').siblings('span.help-block').children().html('La fecha límite debe ser posterior a la fecha actual');
                cerrarModal=true;
                @endif
            }else{
                $('input[name="fecha_lim"]').parent().parent().removeClass('has-error');
                $('input[name="fecha_lim"]').siblings('span.help-block').children().html('');
            }
        }else{
            $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
            $('input[name="fecha_lim"]').siblings('span.help-block').children().html('Debe especificar la fecha límite de la solicitud');
            cerrarModal=true;
        }

       //Modalidad
        var rad_modalidad = document.getElementById('modalidad');
        console.log(rad_modalidad);
        if(rad_modalidad.tagName === 'SELECT') {
            var mod_modalidad;
            var rate_value = $('select[name="modalidad"] :selected').val();

            if(rate_value != ''){
                $('select[name="modalidad"]').parent().parent().removeClass('has-error');
                $('select[name="modalidad"]').siblings('span.help-block').children().html('');
            }else{
                $('select[name="modalidad"]').parent().parent().addClass('has-error');
                $('select[name="modalidad"]').siblings('span.help-block').children().html('Debe seleccionar una modalidad');

                cerrarModal = true;
            }
        }else{
            var rate_value = $('input[name="modalidad"]').val();
            var proy_init = document.getElementById("empresa_fr");
            var x1 = document.getElementById(proy_init.id).selectedIndex;
            var y1 = document.getElementById(proy_init.id).options;
            
            if(y1[x1].value == 0 && rate_value == 3){
                $('select[name="empresa_fr"]').parent().parent().addClass('has-error');
                $('select[name="empresa_fr"]').siblings('span.help-block').children().html('Debe agregar un fondo rotativo al proyecto');

                cerrarModal = true;
            }
        }
        

        if(rate_value == undefined){
            rate_value = $('input[name="modalidad"]').val();

            if (rate_value == 3){
                var fr_cc = $('select[name="empresa_fr"] :selected').val();

                if (fr_cc == 0){
                    $('select[name="empresa_fr"]').parent().parent().addClass('has-error');
                    $('select[name="empresa_fr"]').siblings('span.help-block').children().html('Debe agregar un fondo rotativo al proyecto');

                    cerrarModal = true;
                }else{
                    $('select[name="empresa_fr"]').parent().parent().removeClass('has-error');
                    $('select[name="empresa_fr"]').siblings('span.help-block').children().html('');
                }
            }
        }

        console.log('rate_value: ' + rate_value);

        switch (rate_value) {
            case '1':
                mod_modalidad = 'Sujeto a Rendición';
                break;

            case '2':
                mod_modalidad = 'Gasto Directo';
                break;

            case '3':
                mod_modalidad = 'Fondo Rotativo';
                break;

            case '4':
                mod_modalidad = 'Reposición';
                break;

            default:
                mod_modalidad = '<div class="text-danger"><strong>SIN MODALIDAD</strong></div>';
        }

        //Tipo de cambio
        var mod_tipo_cambio = '{{ $cambio["cambio"] }}';

        //Moneda
        var rad_moneda = document.getElementsByName('moneda');
        var mod_moneda;
        for (var i = 0; i < rad_moneda.length; i++) {
            if (rad_moneda[i].checked) {
                var selector = 'label[for=' + rad_moneda[i].id + ']';
                var label = document.querySelector(selector);
                mod_moneda = label.innerHTML;
            }
        }

        //Desembolso
        var mod_desembolso = '<div class="text-danger"><strong>SIN DESEMBOLSO</strong></div>';
        if (document.getElementById('desembolso').value) {
            mod_desembolso = (document.getElementById('desembolso').value).toUpperCase();
            $('input[name="desembolso"]').parent().parent().removeClass('has-error');
            $('input[name="desembolso"]').siblings('span.help-block').children().html('');
        }else{
            $('input[name="desembolso"]').parent().parent().addClass('has-error');
            $('input[name="desembolso"]').siblings('span.help-block').children().html('Debe especificar el nombre a quien va dirigido el desembolso');
            cerrarModal = true;
        }

        //Referencia
        var mod_referencia = '<div class="text-danger"><strong>SIN REFERENCIA</strong></div>';
        if (document.getElementById('referencia').value) {
            mod_referencia = (document.getElementById('referencia').value).toUpperCase();
        }

        //Observación
        var mod_observacion = '<div class="text-danger"><strong>SIN JUSTIFICACIÓN</strong></div>';
        if (document.getElementById('observacion').value) {
            mod_observacion = (document.getElementById('observacion').value).toUpperCase();
        }

        //Documentos
        var mod_documento = '<div class="text-danger"><strong>SIN DOCUMENTO</strong></div>';
        var str_docs = '';
        var aux_docs;
        for (i = 1; i < 30; i++) {
            element = (document.getElementById('doc' + i));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                aux_docs = document.getElementById('doc' + i).value;
                if (aux_docs) {
                    var startIndex = (aux_docs.indexOf('\\') >= 0 ? aux_docs.lastIndexOf('\\') : aux_docs.lastIndexOf('/'));
                    var filename = aux_docs.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    str_docs += filename + "<br>";
                }
            }
        }
        if (str_docs.length > 0) {
            mod_documento = str_docs;

            $('span#documentos_error strong').html('');
        }else{
            if((rate_value == 3 || rate_value == 4) && $('div#documentos_sol ul').children().length == 0){
                if($('div#moreUploads').children().length == 0){
                    $('span#documentos_error strong').html('Debe subir al menos un archivo');

                    cerrarModal = true;
                }
            }else{
                $('span#documentos_error strong').html('');
            }
        }

        var verifica_total = parseFloat(document.getElementById('total').innerHTML);
        if(verifica_total == 0 ){
            if($('span#table-items strong').html() == ""){
                $('span#table-items strong').html('El monto total de la solicitud debe ser mayor a cero');
            }
            cerrarModal = true;
        }else if($('span#table-items strong').html() == ""){
            $('span#table-items strong').html('');
        }

        //Tabla Items
        var total;
        total = 0;
        var element;
        var str_items = "";
        var cont_items = 1;
        var td_can;
        var td_uni;
        var td_tip;
        var td_det;
        var td_pre;
        var td_sub;
        var td_total = (document.getElementById('total')).innerHTML;
        var tableReg = document.getElementById('solicitud-items');
        var filas_tabla = document.getElementById('solicitud-items').rows.length;
        for (i = 1; i < filas_tabla; i++) {
            cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
            referenciaFila = cellsOfRow[1].children[0].id;
            var fila_array = referenciaFila.split('_');
            element = (document.getElementById('subtotal_' + fila_array[1]));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                td_can = (document.getElementById('cantidad_' + fila_array[1])).value;
                var ele = document.getElementById('unidad_' + fila_array[1]);
                td_uni = ele.options[ele.selectedIndex].innerHTML;
                var ele = document.getElementById('tipo_compra_' + fila_array[1]);
                td_tip = ele.options[ele.selectedIndex].innerHTML;
                td_det = ((document.getElementById('detalle_' + fila_array[1])).value).toUpperCase();
                //asdf
                if(rate_value == '3'){
                    td_nro_factura = (document.getElementById('nrofactura_' + fila_array[1])).value;
                    td_fecha_factura = (document.getElementById('fechafactura_' + fila_array[1])).value;
                    td_proyecto = (document.getElementById('itemproyecto_' + fila_array[1])).value;
                    td_proveedor = (document.getElementById('itemproveedor_' + fila_array[1])).value;
                }else{
                    td_nro_factura = 0;
                    td_fecha_factura = 0;
                    td_proyecto = "";
                    td_proveedor = "";
                }
                
                td_pre = (document.getElementById('punitario_' + fila_array[1])).value;
                td_sub = (document.getElementById('subtotal_' + fila_array[1])).value;
                if(td_sub == 0 || td_tip == 'SELECCIONE TIPO DE COMPRA' || td_det.length < 1 || (rate_value == 3 && (td_nro_factura.length < 1 || td_fecha_factura < 1 || td_proyecto.length < 1 || td_proveedor < 1))){
                    if(td_sub == 0){
                        if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan un Sub-Total distitnto de cero');
                        }
                    }else if(td_tip == 'SELECCIONE TIPO DE COMPRA'){
                        if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan seleccionado el Tipo de Compra');
                        }
                    }else if(td_det.length < 1){
                        if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan especificado el Detalle');
                        }
                    }else if(td_nro_factura.length < 1){
                        if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan especificado el Nro. de factura');
                        }
                    }else if(td_fecha_factura.length < 1){
                        if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan especificado la Fecha de factura');
                        }
                    }else if(td_proyecto.length < 1){
                        if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan especificado el Proyecto');
                        }
                    }else if(td_proveedor.length < 1){
                        if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan especificado el Proveedor');
                        }
                    }

                    cerrarModal = true;
                    break;
                }
                str_items += "<tr><td class='text-right'>" + (cont_items) + "</td><td class='text-right'>" + td_can + "</td><td>" + td_uni + "</td><td>" + td_tip + "</td><td>" + td_det + "</td><td class='text-right'>" + td_pre + "</td><td class='text-right'>" + td_sub + "</td></tr>";
                cont_items++;
            }else{
                tableReg.deleteRow(i);
                i--;
                filas_tabla--;
            }
        }

        if(td_total <= 0){
            if($('span#table-items strong').html() == ""){
                $('span#table-items strong').html('La solicitud no debe tener un total menor o igual a cero, favor verifique la información de los ítems');
            }
            cerrarModal = true;
        }else if($('span#table-items strong').html() == ""){
            $('span#table-items strong').html('');
        }

        if (str_items == '') {
            str_items = "<tr><td colspan='7'><div class='text-danger text-center'><strong>SIN ITEMS</strong></div></td></tr>";
            if($('span#table-items strong').html() == ""){
                $('span#table-items strong').html('La solicitud no tiene ítems, favor agregue alguno(s)');
            }

            cerrarModal = true;
        } else if($('span#table-items strong').html() == ""){
            str_items += "<tr><td colspan='6'><div class='text-right'><strong>TOTAL: </strong></div></td><td class='text-right'>" + td_total + "</td></tr>";

            $('span#table-items strong').html('');
        }
        var mod_literal = numeroLiteral(td_total);
        var mod_decimal = numeroParteDecimal(td_total);
        if(cerrarModal){
            $('#verificarModal').modal('hide');
            //document.getElementById("verificarModal").showModal();
        }

        document.getElementById('modal-solicitante').innerHTML = mod_solicitante;
        document.getElementById('modal-empresa').innerHTML = mod_empresa;
        document.getElementById('modal-contacto').innerHTML = mod_solicitante;
        document.getElementById('modal-proyecto').innerHTML = mod_proyecto;
        document.getElementById('modal-fecha-lim').innerHTML = mod_fecha_lim;

        document.getElementById('modal-modalidad').innerHTML = mod_modalidad;
        document.getElementById('modal-cambio').innerHTML = mod_tipo_cambio;
        document.getElementById('modal-moneda').innerHTML = mod_moneda;
        document.getElementById('modal-desembolso').innerHTML = mod_desembolso;
        document.getElementById('modal-referencia').innerHTML = mod_referencia;

        document.getElementById('modal-observacion').innerHTML = mod_observacion;
        document.getElementById('modal-documentos').innerHTML = mod_documento;
        document.getElementById('modal-items').innerHTML = str_items;
        document.getElementById('modal-literal').innerHTML = mod_literal;
        document.getElementById('modal-decimal').innerHTML = mod_decimal;
        var valorId = document.querySelector('input[name="moneda"]:checked').id;
        var monedaNombre = document.getElementById(valorId).getAttribute('data-nombre');
        document.getElementById('monedaTag').innerHTML = monedaNombre;

        //fin
        if(!cerrarModal){
            $('#verificarModal').modal('show');
        }else{
            $('#reviewModal').modal('show');
        }
    }

    function guardarImprimir(){
        var imprimir_valor = document.getElementById('imprimir').value;
        document.getElementById('imprimir').value = 1;
        document.getElementById('btn-registro-solicitud').click();
    }

    function verificarForm(e){
        $('span#table-items strong').html('');

        e.disabled = true;

        var desembolso = document.getElementById('desembolso').value;
        var tableReg = document.getElementById('solicitud-items');
        var cantidad_items = document.getElementById('solicitud-items').rows.length - 1;

        var cerrarModal = false;

        var today = moment().format('YYYY-MM-DD');
        var fecha_sel = document.getElementById("fecha_lim").value;

        var date_regex = /(19|20)\d{2}$\/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])/ ;
        if(!(date_regex.test(fecha_sel))){
            var anio_valido = parseInt(fecha_sel.substring(0,3));
            if(fecha_sel.length == 10 && anio_valido > 200){
                if(fecha_sel < today){
                    @if(isset($solicitud))
                    @if(Auth::user()->revisor != 1)
                    $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
                    $('input[name="fecha_lim"]').siblings('span.help-block').children().html('Debe especificar una fecha límite posterior a la fecha actual');
                    cerrarModal = true;
                    @endif
                    @else
                    $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
                    $('input[name="fecha_lim"]').siblings('span.help-block').children().html('Debe especificar una fecha límite posterior a la fecha actual');
                    cerrarModal = true;
                    @endif
                }
            }
        }else{
            $('input[name="fecha_lim"]').parent().parent().addClass('has-error');
            $('input[name="fecha_lim"]').siblings('span.help-block').children().html('La fecha límite no cumple el formato establecido');
            cerrarModal = true;
        }

        if(desembolso == ''){
            $('input[name="desembolso"]').parent().parent().addClass('has-error');
            $('input[name="desembolso"]').siblings('span.help-block').children().html('Debe especificar el nombre a quien va dirigido el desembolso');
            cerrarModal = true;
            document.getElementById('desembolso').focus();
        }else if(cantidad_items == 0){
            if($('span#table-items strong').html() == ""){
                $('span#table-items strong').html('La solictud carece de ítems, por favor ingrese alguno(s)');
            }
            cerrarModal = true;
        }else{
            for (i = 1; i <= cantidad_items && cerrarModal == false; i++) {
                cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
                if(cellsOfRow[6].children[0] != null){
                    referenciaFila = cellsOfRow[6].children[0].id;
                    if(document.getElementById(referenciaFila).value != ''){
                        if(x == 0){
                            if($('span#table-items strong').html() == ""){
                                $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan un Sub-Total distinto de cero');
                            }
                            cerrarModal = true;
                            break;
                        }else if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('');
                        }
                    }
                    tipoFila = cellsOfRow[3].children[0].id;
                    var x = document.getElementById(tipoFila).selectedIndex;

                    if(parseFloat(cellsOfRow[6].children[0].value) > 0){
                        if(cellsOfRow[4].children[0].value == ''){
                            if($('span#table-items strong').html() == ""){
                                $('span#table-items strong').html('Verifique que todos los ítems de la solicitud tengan especificado el Detalle');
                            }
                            cerrarModal = true;
                            break;
                        }else if($('span#table-items strong').html() == ""){
                            $('span#table-items strong').html('');
                            var id_opcion_compra = cellsOfRow[3].children[0].id;
                            var opcion_compra = document.getElementById(id_opcion_compra);
                            var sel_opcion_compra = opcion_compra.options[opcion_compra.selectedIndex].value;
                        }
                    }
                }else{
                    console.log("undef");
                }
            }
        }

        if(cerrarModal){
            $('#verificarModal').modal('hide');
        }else{
            //submit
           document.getElementById('btn-registro-solicitud').click();
        }
    }

    $('form input').keydown(function (e){
        if(e.keyCode == 13){ // TECLA: ENTER
            e.preventDefault();
            return false;
        }
    });

    function frProyecto(e){
        var x = document.getElementById(e.id).selectedIndex;
        var y = document.getElementById(e.id).options;

        var url = "/json/proyecto/fr/" + y[x].value;
        $.getJSON(url, function(data) {
            fila="";
            $.each(data, function (i, object) {
                @if(isset($solicitud) && $solicitud->modalidad_id == 3)
                    if({{ $solicitud->frtipo_id }} == object.id){
                        fila += "<option value='"+object.id+"' selected>"+object.nombre+"</option>";
                    }else{
                        fila += "<option value='"+object.id+"'>"+object.nombre+"</option>";
                    }
                @else
                    fila += "<option value='"+object.id+"'>"+object.nombre+"</option>";
                @endif
            });
            if(fila == ""){
                fila = "<option value='0'>SIN FONDOS ROTATIVOS - CAJAS CHICAS</option>";
            }
            $("#empresa_fr").empty();
            $("#empresa_fr").append(fila);
        });
    }
</script>
@endsection