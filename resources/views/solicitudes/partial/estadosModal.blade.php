<div class="modal fade" id="estadosSolModal" tabindex="-1" role="dialog" aria-labelledby="estadosSolModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success">Estados de Solicitud</h4>
            </div>
            <div class="modal-body">
                <table id="table_estados" class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Código</th>
                            <th>Descripción</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>

@section('footerScripts')
@parent
<script>
function estadosSolicitud(est){
    var url = "/json/estados/solicitud?estado="+est;
    var total_items = 0;
    $("#table_estados tbody").empty();
    $.getJSON(url, function(data) {
        $.each(data, function (i, object) {
            if(object.codigo == est){
                font_color = "font-weight: bold; color: #008000";
            }else{
                font_color = "font-weight: initial; color: #000";
            }
            fila = "<tr>";
            fila += "<td style='text-align:center; color: "+font_color+"''>"+(i+1)+"</td>";
            fila += "<td style='"+font_color+"'>"+object.codigo+"</td>";
            fila += "<td style='"+font_color+"'>"+object.descripcion+"</td>";
            fila += "</tr>";
            $("#table_estados tbody").append(fila);
            total_items += parseFloat(object.subtotal);
        });
    });
}
</script>

@endsection