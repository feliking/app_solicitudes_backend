<div class="modal fade" id="historialSolicitud" tabindex="-1" role="dialog" aria-labelledby="historialSolicitudLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="historialModalLabel"></h4>
            </div>
            <div class="modal-body">
                 <!-- Loading -->
                <div class="alert alert-warning" role="alert" id="msg-estados">
                    <i class="fa fa-btn fa-spin fa-refresh"></i>
                    <strong>Cargando!!!</strong> Un momento por favor...
                </div>

                <table id="estados-solicitud" class="table">
                    <tr>
                        <th>Fecha</th>
                        <th>Usuario</th>
                        <th>Estado</th>
                        <th>Evento</th>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
@section('footerScripts')
@parent
<script>
    // Limpiar Tabla
    function limpiarEventos(){
        $('span#id').text('');
        $('table#estados-solicitud').text('');
    }

    // getHistorial - Json
    $(document).on('click', 'a[data-target="#historialSolicitud"]', function(e){
        limpiarEventos();
        var idSolicitud = $(this).attr('data-id');
        var numeroSolicitud = $('#sol_hist_'+idSolicitud).attr('data-numero');
        document.getElementById('historialModalLabel').innerHTML = "<strong>HISTORIAL DE ESTADOS SOLICITUD #"+numeroSolicitud+"</strong>";
        var url = '{{ route('solicitudes.getEstados', 'Texto') }}';
        url = url.replace('Texto', idSolicitud);
        $.ajax({
            url: url,
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            method: 'GET',
            dataType: 'JSON',
            beforeSend: function(e){
                $('#msg-estados').css('display', 'block');
                $('#estados-solicitud').css('display', 'none');
            }
        }).done(function (response){
            if(response['solicitud'] != ''){
                $('span#id').text(response['solicitud']['id']);

                if(response['estados'] != ''){
                    $('table#estados-solicitud').append('<tbody><tr><th>Fecha</th><th>Usuario</th><th>Codigo</th><th>Descripción</th></tr></tbody>');
                    // $.each(response['estados'], function(value, key){
                    //     estado = value.split('|');
                    //     $('table#estados-solicitud').append('<tr><td>' + new Date(key.date).toLocaleString() + '</td><td>' + estado[0] + '</td><td>' + estado[1] + '</td><td>' + estado[2] + '</td></tr>');
                    // });
                    $.each(response['estados'], function(key, value){
                        estado = value.estado.split('|');
                        $('table#estados-solicitud').append('<tr><td>' + new Date(value.created_at).toLocaleString() + '</td><td>' + estado[0] + '</td><td>' + estado[1] + '</td><td>' + estado[2] + '</td></tr>');
                    });
                }

                $('#msg-estados').css('display', 'none');
                $('#estados-solicitud').css('display', 'table');
            }
        }).fail(function (response){
        });
        url = url.replace(idSolicitud, 'Texto');
    });
</script>
@endsection