<div class="modal fade" id="solicitudModal" tabindex="-1" role="dialog" aria-labelledby="solicitudModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="solicitudModalLabel">
                    <strong><span id="modal-nom-empresa"></span><br>DATOS DE LA SOLICITUD # <span id="modal-numero"></span></strong>
                </h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default ">
                    <div class="panel-heading"><h1>INFORMACIÓN DE SOLICITUD</h1></div>
                    <div class="panel-body" style="overflow-x: scroll;">
                        <table class="table table-bordered" id="reporte-solicitud">
                            <tr>
                                <td><strong>PROYECTO: </strong></td>
                                <td colspan="9" id='modal-proyecto'></td>
                            </tr>
                            <tr>
                                <td><strong>SOLICITANTE: </strong></td>
                                <td colspan="9" id="modal-solicitante"></td>
                            </tr>
                            <tr id="tr_con_tipo">
                                <td><strong>MODALIDAD: </strong></td>
                                <td colspan="9" id='modal-modalidad'></td>
                            </tr>
                            <tr id="tr_sin_tipo">
                                <td><strong>MODALIDAD: </strong></td>
                                <td colspan="9" id='modal-modalidad-sin'></td>
                            </tr>
                            <tr id="tr_con_moneda">
                                <td><strong>MONEDA: </strong></td>
                                <td id='modal-moneda'></td>
                                <td><strong>TIPO DE CAMBIO: </strong></td>
                                <td id='modal-cambio'></td>
                                <td><strong>FECHA LIMITE: </strong></td>
                                <td id="modal-con-fecha-lim"></td>
                            </tr>
                            <tr id="tr_sin_moneda">
                                <td><strong>MONEDA: </strong></td>
                                <td id='modal-moneda-sin'></td>
                                <td><strong>FECHA LIMITE: </strong></td>
                                <td id="modal-sin-fecha-lim"></td>
                            </tr>
                        </table>
                        <div >
                            <div id="div_items_antiguos" style="overflow-x: scroll;">
                                <table>
                                    <tr>
                                        <td><strong>DESEMBOLSO ANTIGUO A: </strong></td>
                                        <td id='modal-desembolso-antiguo'></td>
                                    </tr>
                                </table>
                                <h4 style="text-align: center;">ITEMS SOLICITUD ORIGINAL</h4>
                                <table class="table table-bordered table-items table-items-antiguos" id="table_items_antiguos">
                                    <thead id='modal-items-antiguos-th'>
                                        <tr>
                                            <th>#</th>
                                            <th>UN.</th>
                                            <th>T. COMPRA</th>
                                            <th>DETALLE</th>
                                            <th>CANTIDAD</th>
                                            <th>P. UNI</th>
                                            <th>SUB TOT</th>
                                        </tr>
                                    </thead>
                                    <tbody id='modal-items-antiguos'>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border: 1px solid green; padding: 5px; border-radius: 5px; background: #ecf9ec;">
                                <table>
                                    <tr>
                                        <td><strong>DESEMBOLSO A: </strong></td>
                                        <td id='modal-desembolso'></td>
                                    </tr>
                                </table>
                                <div id="div_items_solicitud" style="overflow-x: scroll;">
                                    <h4 style="text-align: center;">ITEMS SOLICITUD</h4>
                                    <table class="table table-bordered table-items" id="table_items">
                                        <thead id='modal-items-th'>    
                                        </thead>
                                        <tbody id='modal-items'>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="div_items_rendidos" style="overflow-x: scroll;">
                                    <h4 style="text-align: center;">ITEMS RENDIDOS</h4>
                                    <table class="table table-bordered" id="table_items_rendidos">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>UN.</th>
                                                <th>T. COMPRA</th>
                                                <th>DETALLE</th>
                                                <th>CANTIDAD</th>
                                                <th>P. UNI</th>
                                                <th>FECHA FACTURA</th>
                                                <th>Nº FAC</th>
                                                <th>SUB TOT</th>
                                            </tr>
                                        </thead>
                                        <tbody id='modal-items'>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <p><strong>SON:</strong> <span id="modal-literal"></span> CON <span id="modal-decimal"></span>/100 <span id="modal-literal-moneda"></span></p>
                            <table class="table table-bordered" id="a">
                                <tr>
                                    <td style="width: 100px"><strong>REFERENCIA: </strong></td>
                                    <td id='modal-referencia'></td>
                                </tr>
                                <tr>
                                    <td><strong>JUSTIFICACIÓN: </strong></td>
                                    <td id='modal-observacion'></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-print"><!-- ELIMINAR DISPLAY NONE -->
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                <span class="pull-right" id="print-span"></span>
            </div>
        </div>
    </div>
</div>

@section('footerScripts')
@parent
<script>
    function seleccion_solicitud(e){
        var mod_tam = (e.id).length;
        var id_fila = (e.id).substring(9, mod_tam);
        $("#table_items tbody").empty();
        $("#table_items_rendidos tbody").empty();

        var fila_sol = document.getElementById('solicitud_'+id_fila).innerHTML;
        var url_solicitud = '/json/datos_solicitud/'+id_fila;
        var modalidad_id = 0;
        var last_estado_id = 0;
        var go_cad = "";

        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('tr_con_tipo').style.display = 'contents';
            document.getElementById('tr_sin_tipo').style.display = 'none';

            var des_antiguo = datos.desembolso_antiguo;
            $('#modal-desembolso-antiguo').append(des_antiguo);
            document.getElementById('modal-sin-fecha-lim').innerHTML = datos.fecha_limite;
            document.getElementById('modal-con-fecha-lim').innerHTML = datos.fecha_limite;

            document.getElementById('modal-solicitante').innerHTML = datos.usuario;
            document.getElementById('modal-proyecto').innerHTML = datos.proyecto;
            document.getElementById('modal-desembolso').innerHTML = datos.desembolso;
            document.getElementById('modal-cambio').innerHTML = datos.tipo_cambio;
            document.getElementById('modal-numero').innerHTML = datos.numero;

            if(datos.moneda == 'BOLIVIANOS'){
                document.getElementById('tr_con_tipo').style.display = 'none';
                document.getElementById('tr_sin_tipo').style.display = '';
                document.getElementById('tr_con_moneda').style.display = 'none';
                document.getElementById('tr_sin_moneda').style.display = '';
            }else{
                document.getElementById('tr_con_tipo').style.display = '';
                document.getElementById('tr_sin_tipo').style.display = 'none';
                document.getElementById('tr_con_moneda').style.display = '';
                document.getElementById('tr_sin_moneda').style.display = 'none';
            }
            document.getElementById('modal-modalidad').innerHTML = datos.modalidad;
            document.getElementById('modal-moneda').innerHTML = datos.moneda;

            document.getElementById('modal-modalidad-sin').innerHTML = datos.modalidad;
            go_cad = datos.modalidad;
            $("#table_items thead").empty();
            let fila_th = "";
            if(go_cad.includes("FONDO ROTATIVO")){
                fila_th ="<tr>";
                fila_th +="<th>#</th>";
                fila_th +="<th>UN.</th>";
                fila_th +="<th>T. COMPRA</th>";
                fila_th +="<th>DETALLE</th>";
                fila_th +="<th>PROYECTO/PROVEEDOR</th>";
                fila_th +="<th>F. FACTURA</th>";
                fila_th +="<th>Nº FACTURA</th>";
                fila_th +="<th>CANTIDAD</th>";
                fila_th +="<th>P. UNI</th>";
                fila_th +="<th>SUB TOT</th>";
                fila_th +="</tr>";
            }else{
                fila_th ="<tr>";
                fila_th +="<th>#</th>";
                fila_th +="<th>UN.</th>";
                fila_th +="<th>T. COMPRA</th>";
                fila_th +="<th>DETALLE</th>";
                fila_th +="<th>CANT.</th>";
                fila_th +="<th>P. UNI</th>";
                fila_th +="<th>SUB TOT</th>";
                fila_th +="</tr>";
            }
            $("#table_items thead").append(fila_th);
            document.getElementById('modal-moneda-sin').innerHTML = datos.moneda;
            document.getElementById('modal-referencia').innerHTML = datos.referencia;
            document.getElementById('modal-observacion').innerHTML = datos.observacion;
            document.getElementById('modal-nom-empresa').innerHTML = datos.empresa;

            document.getElementById('modal-literal-moneda').innerHTML = datos.moneda;

            modalidad_id = datos.modalidad_id;

            last_estado_id = datos.last_estado_id;
        });

        var verifica = {};
        $.ajax({
            url: "/json/verifica/solicitud/completa/"+id_fila,
            async: false,
            dataType: 'json',
            success: function(data) {
                verifica = data;
            }
        });

        //item_table.style('display: block');
        var url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $.each(data, function (i, object) {
                if(go_cad.includes("FONDO ROTATIVO")){
                    fila = "<tr>";
                    fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                    fila += "<td>"+object.unidad_descripcion+"</td>";
                    fila += "<td>"+object.tipocompra+"</td>";
                    fila += "<td>"+object.detalle.toUpperCase()+"</td>";
                    fila += "<td>"+object.proyecto+" - "+object.proveedor+"</td>";
                    fila += "<td>"+object.fecha_factura+"</td>";
                    fila += "<td>"+object.num_factura+"</td>";
                    fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                    fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                    fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                    fila += "</tr>";
                }else{
                    fila = "<tr>";
                    fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                    fila += "<td>"+object.unidad_descripcion+"</td>";
                    fila += "<td>"+object.tipocompra+"</td>";
                    fila += "<td>"+object.detalle.toUpperCase()+"</td>";
                    fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                    fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                    fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                    fila += "</tr>";
                }
                $("#table_items tbody").append(fila);
                total_items += parseFloat(object.subtotal);
            });
            if(go_cad.includes("FONDO ROTATIVO")){
                fila = "<tr>";
                fila += "<td colspan='9' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
                fila += "</tr>";
            }else{
                fila = "<tr>";
                fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
                fila += "</tr>";
            }
            $("#table_items tbody").append(fila);
        });

        var total = document.getElementById('total_'+id_fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = parseInt(res[1]);
        var str_decimal = '0';

        if(p_entera != ""){
            var str_literal = numeroLiteral(p_entera);
            document.getElementById('modal-literal').innerHTML = str_literal.toUpperCase();
        }

        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+p_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal').innerHTML = str_decimal;

        var btn_modal = document.getElementById('btn-modal-solicitud');
        btn_modal.click();

        var span_p = document.getElementById('print-span');
        span_p.innerHTML = "";
        
        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", id_fila);
        file.innerHTML = "IMPRIMIR COMPLETO";
        file.setAttribute("onclick", "imprimirSolicitudCompleta(this)");
        if(verifica){
            span_p.appendChild(file);
        }

        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", id_fila);
        file.innerHTML = "IMPRIMIR";
        file.setAttribute("onclick", "imprimirSolicitud(this)");
        span_p.appendChild(file);

        var url_antiguos = "/json/items_tiene_antiguos/"+id_fila;
        var div_antiguos = document.getElementById('div_items_antiguos');
        div_antiguos.setAttribute('style', 'display: none;');
        var cantidad = 0;
        $.getJSON(url_antiguos, function(data){
            cantidad = data;
            if(cantidad > 0){
                var div_antiguos = document.getElementById('div_items_antiguos');
                div_antiguos.setAttribute('style', 'display: block; border: 1px solid orange; padding: 5px; margin: 15px 0px; border-radius: 5px; background: #fff5e6;');
                var url_items_antiguos = "/json/items_antiguos_solicitud/"+id_fila;
                $.getJSON(url_items_antiguos, function(data) {
                    $("#table_items_antiguos tbody").empty();
                    total_items = 0;
                    $.each(data, function (i, object) {
                        fila = "<tr>";
                        fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                        fila += "<td>"+object.unidad_descripcion+"</td>";
                        fila += "<td>"+object.tipocompra+"</td>";
                        fila += "<td>"+object.detalle.toUpperCase()+"</td>";
                        fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                        fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                        fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                        fila += "</tr>";
                        $("#table_items_antiguos tbody").append(fila);
                        total_items += parseFloat(object.subtotal);
                    });
                    fila = "<tr>";
                    fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                    fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
                    fila += "</tr>";
                    $("#table_items_antiguos tbody").append(fila);
                });
            }
        });

        var url_lastEstadoCodigo = "/solicitud-lastEstadoCodigo/"+id_fila;
        var div_rendidos = document.getElementById('div_items_rendidos');
        div_rendidos.setAttribute('style', 'display: none;');

        $.get(url_lastEstadoCodigo, function(data) {
            if(last_estado_id > 60){
                var url_rendidos = "/json/items_rendidos/"+id_fila;
                var cantidad = 0;

                $.getJSON(url_rendidos, function(data){
                    cantidad = data.length;
                    if(cantidad > 0){
                        var div_rendidos = document.getElementById('div_items_rendidos');
                        div_rendidos.setAttribute('style', 'display: initial;');
                        total_items = 0;
                        
                        $.each(data, function (i, object) {
                            fila = "<tr>";
                            fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                            fila += "<td>"+object.unidad_descripcion+"</td>";
                            var cad_af = "";
                            if(object.tipocompra == 'ACTIVO FIJO'){
                                cad_af = "<a id='sol_fila_"+object.id+"' class='fa fa-file-o icono-opciones-files icono-green' href='/solicitud-imprimir/formulario/af?id_item_rendido="+object.id+"' target='blank' title='Formulario Activo Fijo'></a>";
                            }
                            fila += "<td>"+object.tipocompra+" "+cad_af+"</a></td>";
                            fila += "<td>"+object.detalle.toUpperCase()+"</td>";
                            fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                            fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                            fila += "<td style='text-align:left;'>"+object.fecha_factura+"</td>";
                            fila += "<td style='text-align:left;'>"+object.num_factura+"</td>";
                            fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                            fila += "</tr>";
                            $("#table_items_rendidos tbody").append(fila);
                            total_items += parseFloat(object.subtotal);
                        });
                        fila = "<tr>";
                        fila += "<td colspan='8' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                        fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
                        fila += "</tr>";
                        $("#table_items_rendidos tbody").append(fila);
                    }
                });
            }
        });
    }

    function imprimirSolicitud(e){
        var id = e.id;
        window.open('/solicitud-imprimir?id_sol='+id);
    }

    function imprimirSolicitudCompleta(e){
        var id = e.id;
        window.open('/solicitud-imprimir/completa?id_sol='+id);
    }
</script>

@endsection