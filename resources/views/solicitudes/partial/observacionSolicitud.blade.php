<div class="modal fade" id="observacionModal" tabindex="-1" role="dialog" aria-labelledby="observacionModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="observarModalLabel"><strong>OBSERVAR SOLICITUD # <span id="modal-num"></span></strong></h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default ">
                    <div class="panel-heading">Datos Solicitud</div>
                    <div class="panel-body" style="overflow-x: scroll;">
                        <table class="table table-bordered" id="aprobar-solicitud">
                            <tr>
                                <td><label for="pago">PROYECTO: </label></td>
                                <td colspan="5" id='modalobservacion-proyecto'></td>
                                <td><label for="turno">FECHA LÍMITE: </label></td>
                                <td id="modalobservacion-fecha-lim"></td>
                            </tr>
                            <tr>
                                <td><label for="ruta">SOLICITANTE: </label></td>
                                <td colspan="9" id="modalobservacion-solicitante"></td>
                            </tr>
                            <tr>
                                <td><label for="grupo">DESEMBOLSO A: </label></td>
                                <td colspan="9" id='modalobservacion-desembolso'></td>
                            </tr>
                            <tr id="tr_con_tipo_obs">
                                <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                <td id='modalobservacion-cambio'></td>
                                <td><label for="grupo">MODALIDAD: </label></td>
                                <td colspan="3" id='modalobservacion-modalidad'></td>
                                <td><label for="grupo">MONEDA: </label></td>
                                <td colspan="3" id='modalobservacion-moneda'></td>
                            </tr>
                            <tr id="tr_sin_tipo_obs">
                                <td><label for="grupo">MODALIDAD: </label></td>
                                <td colspan="3" id='modalobservacion-modalidad-sin'></td>
                                <td><label for="grupo">MONEDA: </label></td>
                                <td colspan="3" id='modalobservacion-moneda-sin'></td>
                            </tr>
                            <tr>
                                <td><label for="grupo">REFERENCIA: </label></td>
                                <td colspan="9" id='modalobservacion-referencia'></td>
                            </tr>
                            <tr>
                                <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                                <td colspan="9" id='modalobservacion-observacion'></td>
                            </tr>
                            <tr class="no-print">
                                <td class="text-right"><strong>TOTAL</strong></td>
                                <td colspan="9" id='modalobservacion-total'></td>
                            </tr>
                        </table>
                        <div>
                            <h4>ITEMS SOLICITUD</h4>
                            <table class="table table-bordered table-items" id="table_items">
                                <thead>
                                    <tr>
                                        <th style="width:10px">#</th>
                                        <th style="width:20px">UNIDAD</th>
                                        <th style="width:20px">TIPO COMPRA</th>
                                        <th>DETALLE</th>
                                        <th style="width:20px">CANTIDAD</th>
                                        <th style="width:20px">P. UNITARIO</th>
                                        <th style="width:20px">SUB TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody id='modal-items'>
                                </tbody>
                            </table>
                            <p><strong>SON:</strong> <span id="modal-literal-obs"></span> CON <span id="modal-decimal-obs"></span>/100 <span id="modal-moneda-obs"></span></p>
                        </div>
                    </div>
                </div>
                {!! Form::open(array('action' => 'SolicitudController@observacion','method'=>'POST', 'class'=>'form-horizontal')) !!}
                <div class="panel panel-default ">
                    <div class="panel-heading"><h2>OBSERVACIÓN</h2></div>
                    <div class="panel-body">
                        {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'id_sol')) !!}
                        {!! Form::hidden('id_rol', null, array('placeholder' => '','class' => 'form-control', 'id' => 'id_rol')) !!}
                        {!! Form::textarea('motivo', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA EL MOTIVO DE LA OBSERVACIÓN...', 'style' => 'text-transform:uppercase', 'required' => 'required')) !!}
                    </div>
                </div>
                <div class="modal-footer"  id="modalobservacionopciones">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                </div>
                {!! Form::close() !!}
            </div>   
        </div>
    </div>
</div>