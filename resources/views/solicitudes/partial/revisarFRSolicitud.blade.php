<div class="modal fade" id="revisarModal" tabindex="-1" role="dialog" aria-labelledby="revisarModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 1500px;">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="revisarModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-default ">
                            <div class="panel-heading" id="revisarModalTitulo">
                                <h2>INFORMACIÓN DE LA SOLICITUD DE FONDO ROTATIVO</h2>
                            </div>
                            <div class="panel-body" style="overflow-x: scroll;">
                                <table class="table table-bordered" id="aprobar-solicitud">
                                    <tr>
                                        <td><strong>PROYECTO: </strong></td>
                                        <td colspan="5" id='modalautorizar-proyecto'></td>
                                        <td><strong>FECHA LÍMITE: </strong></td>
                                        <td id="modalautorizar-fecha-lim"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>SOLICITANTE: </strong></td>
                                        <td colspan="9" id="modalautorizar-solicitante"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>DESEMBOLSO A: </strong></td>
                                        <td colspan="9" id='modalautorizar-desembolso'></td>
                                    </tr>
                                    <tr id="tr_con_tipo_apr">
                                        <td><strong>TIPO DE CAMBIO: </strong></td>
                                        <td id='modalautorizar-cambio'></td>
                                        <td><strong>MODALIDAD: </strong></td>
                                        <td colspan="3" id='modalautorizar-modalidad'></td>
                                        <td><strong>MONEDA: </strong></td>
                                        <td colspan="3" id='modalautorizar-moneda'></td>
                                    </tr>
                                    <tr id="tr_sin_tipo_apr">
                                        <td><strong>MODALIDAD: </strong></td>
                                        <td colspan="4" id='modalautorizar-modalidad-sin'></td>
                                        <td><strong>MONEDA: </strong></td>
                                        <td colspan="4" id='modalautorizar-moneda-sin'></td>
                                    </tr>
                                    <tr>
                                        <td><strong>REFERENCIA: </strong></td>
                                        <td colspan="9" id='modalautorizar-referencia'></td>
                                    </tr>
                                    <tr>
                                        <td><strong>JUSTIFICACIÓN: </strong></td>
                                        <td colspan="9" id='modalautorizar-observacion'></td>
                                    </tr>
                                    <tr class="no-print">
                                        <td class="text-right"><strong>TOTAL</strong></td>
                                        <td colspan="9" id='modalautorizar-total'></td>
                                    </tr>
                                </table>
                                <div>
                                    <div id="div_items_rendidos">
                                        <h4>ITEMS RENDIDOS</h4>
                                        <table class="table table-bordered table-items" id="table_items_rendidos" style="font-size:11px;">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th style="width:20px">T. COMPRA</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANT.</th>
                                                    <th style="width:20px">P. UNIT.</th>
                                                    <th style="width:20px">FECHA FACT/REC</th>
                                                    <th style="width:20px">NRO. FACT/REC</th>
                                                    <th style="width:20px">PROY.</th>
                                                    <th style="width:20px">PROV.</th>
                                                    <th style="width:20px">TIPO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                    </div>
                                    <p><strong>SON: </strong> <span id="modal-literal-aprobar"></span> CON <span id="modal-decimal-aprobar"></span>/100 <span id="modal-moneda-aprobar"></span></p>
                                </div>
                                <div class="text-center">
                                    <input type="checkbox" name="observa" id="observa" onclick="verificar()">&nbsp;&nbsp;<label for='correo'>¿DESEA REALIZAR OBSERVACIÓN?</label>
                                    
                                    {!! Form::textarea('des_observacion', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA SUS OBSERVACIONES...', 'style' => 'text-transform:uppercase', 'id' => 'des_observacion')) !!}
        
                                    <span class="help-block">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-default ">
                            <div class="panel-heading"><h2>ARCHIVOS ADJUNTOS</h2></div>
                            <div class="panel-body">
                                <table class="table table-hover table-bordered" id="doc_table_revisor">
                                    <thead>
                                        <tr>
                                            <th style="width:10px">#</th>
                                            <th>NOMBRE ARCHIVO</th>
                                            <th style="width:20px">DESCARGA</th>
                                        </tr>
                                    </thead>
                                    <tbody id="modal-items">
                                    </tbody>
                                </table>
                                <div id="ver_doc_revisor"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer"  id="modalautorizaropciones">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                </div>
            </div>
        </div>
    </div>
</div>
@section('footerScripts')
@parent
<script>
    document.getElementById('des_observacion').style.display = 'none';
    function verificar(){
        if(document.getElementById('observa').checked){
            document.getElementById('des_observacion').style.display = 'block';
        }else{
            document.getElementById('des_observacion').style.display = 'none';
        }
    }
</script>
@endsection