<div class="modal fade" id="descargaDocsModal" tabindex="-1" role="dialog" aria-labelledby="descargaDocsModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="descargarDocsModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default ">
                    <div class="panel-heading"><h2>ARCHIVOS ADJUNTOS</h2></div>
                    <div class="panel-body">
                        <table class="table table-hover table-bordered" id="doc_table">
                            <thead>
                                <tr>
                                    <th style="width:10px">#</th>
                                    <th>NOMBRE ARCHIVO</th>
                                    <th style="width:20px">DESCARGA</th>
                                </tr>
                            </thead>
                            <tbody id="modal-items">
                            </tbody>
                        </table>
                        <div id="ver_doc"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>

@section('footerScripts')
@parent
<script>
    function ver_documento(e, empresa, gestion){
        var doc = document.getElementById('ver_doc');
        var documento = e.getAttribute('data');
        var array_doc = documento.split('.');

        var url = "";
        if(array_doc.length >= 2){
            var doc_id = array_doc[0].split('_');
            url = "/empresas/"+ empresa +"/"+ gestion +"/solicitud/"+documento;
        }
        if(array_doc[array_doc.length-1] == 'pdf'){
            PDFObject.embed(url, "#ver_doc");
            var docFrame = document.getElementById('ver_doc');
            docFrame.setAttribute("style", "width: 100%; height: 500px;");
        }else{
            if(url != ""){
                switch(array_doc[1]){
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'bmp':
                        url = "/empresas/"+ empresa +"/"+ gestion +"/solicitud/"+documento;
                        break;
                    default:
                        url = "";
                }
                if(url == ""){
                    doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
                }else{
                    doc.innerHTML = "<iframe src='"+url+"' width='100%' height='500px'></iframe>";
                }
            }else{
                doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
            }
        }
        
        var div_doc = document.getElementById('ver_doc');
        div_doc.style.display = 'block';
    }

    function ver_documento_revisor(e, empresa, gestion){
        var doc = document.getElementById('ver_doc_revisor');
        var documento = e.getAttribute('data');
        var array_doc = documento.split('.');

        var url = "";
        if(array_doc.length >= 2){
            var doc_id = array_doc[0].split('_');
            url = "/empresas/"+ empresa +"/"+ gestion +"/solicitud/"+documento;
        }
        if(array_doc[array_doc.length-1] == 'pdf'){
            PDFObject.embed(url, "#ver_doc_revisor");
            var docFrame = document.getElementById('ver_doc_revisor');
            docFrame.setAttribute("style", "width: 100%; height: 500px;");
        }else{
            if(url != ""){
                switch(array_doc[1]){
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'bmp':
                        url = "/empresas/"+ empresa +"/"+ gestion +"/solicitud/"+documento;
                        break;
                    default:
                        url = "";
                }
                if(url == ""){
                    doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
                }else{
                    doc.innerHTML = "<iframe src='"+url+"' width='100%' height='500px'></iframe>";
                }
            }else{
                doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
            }
        }
        
        var div_doc = document.getElementById('ver_doc_revisor');
        div_doc.style.display = 'block';
    }

    function ver_documento_rendicion(e, empresa, gestion){
        var doc = document.getElementById('ver_doc');
        var documento = e.getAttribute('data');
        var array_doc = documento.split('.');

        var url = "";
        if(array_doc.length >= 2){
            var doc_id = array_doc[0].split('_');
            url = "/empresas/"+ empresa +"/"+ gestion +"/rendicion/"+documento;
        }
        if(array_doc[array_doc.length-1] == 'pdf'){
            PDFObject.embed(url, "#ver_doc");
            var docFrame = document.getElementById('ver_doc');
            docFrame.setAttribute("style", "width: 100%; height: 500px;");
        }else{
            if(url != ""){
                switch(array_doc[1]){
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'bmp':
                        url = "/empresas/"+ empresa +"/"+ gestion +"/rendicion/"+documento;
                        break;
                    default:
                        url = "";
                }
                if(url == ""){
                    doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
                }else{
                    doc.innerHTML = "<iframe src='"+url+"' width='100%' height='500px'></iframe>";
                }
            }else{
                doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
            }
        }

        var div_doc = document.getElementById('ver_doc');
        div_doc.style.display = 'block';
    }

    function descarga_docs(e){
        var url = "/json/documentos_solicitud/" + e.id;
        $.getJSON(url, function(data) {
            $("#doc_table tbody").empty();
            docs = data;
            $.each(data, function (i, object) {
                $fila = "<tr>";
                $fila += "<td>"+object.id+"</td>";
                $fila += "<td>"+object.direccion+"</td>";
                $fila += "<td>";
                $fila += "<a href='/empresas/"+object.empresa_slug+"/"+object.gestion+"/solicitud/"+object.direccion+"' download='"+object.direccion+"'><i class='fa fa-download icono-opciones icono-blue' title='Descargar Documento'></i></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-file-text icono-opciones icono-green' onclick='ver_documento(this, \""+object.empresa_slug+"\", "+ object.gestion +")' data='"+object.direccion+"' title='Ver Documento'></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-eye-slash icono-opciones icono-green' onclick='cerrar_vista(this)' data='"+object.direccion+"' title='Cerrar Vista Previa'></a>";
                $fila += "</td>";
                $fila += "</tr>";
                $("#doc_table tbody").append($fila);
            });
        });
        var btn_modal_des = document.getElementById('btn-modal-descarga');
        btn_modal_des.click();
        var div_doc = document.getElementById('ver_doc');
        div_doc.style.display = 'none';
        var numeroSolicitud = e.getAttribute('data-numero');
        document.getElementById('descargarDocsModalLabel').innerHTML = "<strong>DESCARGAR DOCUMENTOS SOLICITUD #"+numeroSolicitud+"</strong>";
    }

    function descarga_docs_rendidos(e){
        var url = "/json/documentos_rendicion/" + e.id;
        $.getJSON(url, function(data) {
            $("#doc_table tbody").empty();
            docs = data;
            $.each(data, function (i, object) {
                $fila = "<tr>";
                $fila += "<td>"+object.id+"</td>";
                $fila += "<td>"+object.direccion+"</td>";
                $fila += "<td>";
                $fila += "<a href='/empresas/"+object.empresa_slug+"/"+object.gestion+"/rendicion/"+object.direccion+"' download='"+object.direccion+"'><i class='fa fa-download icono-opciones icono-blue' title='Descargar Documento'></i></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-file-text icono-opciones icono-green' onclick='ver_documento_rendicion(this, \""+object.empresa_slug+"\", "+ object.gestion +")' data='"+object.direccion+"' title='Ver Documento'></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-eye-slash icono-opciones icono-green' onclick='cerrar_vista(this)' data='"+object.direccion+"' title='Cerrar Vista Previa'></a>";
                $fila += "</td>";
                $fila += "</tr>";
                $("#doc_table tbody").append($fila);
            });
        });
        var btn_modal_des = document.getElementById('btn-modal-descarga');
        btn_modal_des.click();
        var div_doc = document.getElementById('ver_doc');
        div_doc.style.display = 'none';
        var numeroSolicitud = e.getAttribute('data-numero');
        document.getElementById('descargarDocsModalLabel').innerHTML = "<strong>DESCARGAR DOCUMENTOS SOLICITUD #"+numeroSolicitud+"</strong>";
    }

    function cerrar_vista(e){
        var doc = document.getElementById('ver_doc');
        doc.setAttribute("style", "width: 0%; height: 0%;");
        doc.innerHTML = "";
    }

    function cerrar_vista_revisor(e){
        var doc = document.getElementById('ver_doc_revisor');
        doc.setAttribute("style", "width: 0%; height: 0%;");
        doc.innerHTML = "";
    }
</script>

@endsection