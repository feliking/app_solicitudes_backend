<div class="modal fade" id="rechazoAutModal" tabindex="-1" role="dialog" aria-labelledby="rechazoAutModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="rechazoAutModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default " style="overflow-x: scroll;">
                    <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                    <div class="panel-body">
                        <table class="table table-bordered" id="reporte-solicitud">
                            <tr>
                                <td><label for="pago">PROYECTO: </label></td>
                                <td colspan="5" id='modalrechazo-proyecto'></td>
                                <td><label for="turno">FECHA LÍMITE: </label></td>
                                <td id="modalrechazo-fecha-lim"></td>
                            </tr>
                            <tr>
                                <td><label for="ruta">SOLICITANTE: </label></td>
                                <td colspan="9" id="modalrechazo-solicitante"></td>
                            </tr>
                            <tr>
                                <td><label >DESEMBOLSO A: </label></td>
                                <td colspan="9" id='modalrechazo-desembolso'></td>
                            </tr>
                            <tr id="tr_con_tipo_rec">
                                <td><label >TIPO DE CAMBIO: </label></td>
                                <td id='modalrechazo-cambio'></td>
                                <td><label >MODALIDAD: </label></td>
                                <td colspan="3" id='modalrechazo-modalidad'></td>
                                <td><label >MONEDA: </label></td>
                                <td colspan="3" id='modalrechazo-moneda'></td>
                            </tr>
                            <tr id="tr_sin_tipo_rec">
                                <td><label >MODALIDAD: </label></td>
                                <td colspan="4" id='modalrechazo-modalidad-sin'></td>
                                <td><label >MONEDA: </label></td>
                                <td colspan="4" id='modalrechazo-moneda-sin'></td>
                            </tr>
                            <tr>
                                <td><label >REFERENCIA: </label></td>
                                <td colspan="9" id='modalrechazo-referencia'></td>
                            </tr>
                            <tr>
                                <td><label >JUSTIFICACIÓN: </label></td>
                                <td colspan="9" id='modalrechazo-observacion'></td>
                            </tr>
                            <tr class="no-print">
                                <td class="text-right"><strong>TOTAL</strong></td>
                                <td colspan="9" id='modalrechazo-total'></td>
                            </tr>
                        </table>
                        <div>
                            <h4>ITEMS SOLICITUD</h4>
                            <table class="table table-bordered table-items" id="table_items">
                                <thead>
                                    <tr>
                                        <th style="width:10px">#</th>
                                        <th style="width:20px">UNIDAD</th>
                                        <th style="width:20px">TIPO COMPRA</th>
                                        <th>DETALLE</th>
                                        <th style="width:20px">CANTIDAD</th>
                                        <th style="width:20px">P. UNITARIO</th>
                                        <th style="width:20px">SUB TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody id='modal-items'>
                                </tbody>
                            </table>
                            <p><strong>SON:</strong> <span id="modal-literal-rechazo"></span> CON <span id="modal-decimal-rechazo"></span>/100 <span id="modalrechazo-literal-moneda"></span></p>
                        </div>
                    </div>
                </div>
                {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                <div class="panel panel-default ">
                    <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                    <div class="panel-body">
                        {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_id')) !!}
                        {!! Form::hidden('tipo', 'autorizar', array('placeholder' => '','class' => 'form-control')) !!}
                        {!! Form::textarea('motivo', null, ['placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform:uppercase', 'required' => 'required']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                    <span class="pull-right">
                        <button type="submit" class="btn btn-primary" onclick="rechazo()">RECHAZAR SOLICITUD</button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>