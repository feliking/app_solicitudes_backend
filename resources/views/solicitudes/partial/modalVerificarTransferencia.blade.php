<div class="modal fade" id="verificarModal" tabindex="-1" role="dialog" aria-labelledby="verificarModalLabel">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header no-print">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center text-success" id="verificarModalLabel"><strong>POR FAVOR VERIFIQUE LOS DATOS PARA REALIZAR LA SOLICITUD DE TRANSFERENCIA</strong></h4>
                </div>
                <div class="modal-body">
                    <h1 class="text-center" id="modal-empresa"></h1>
                    <h2 class="text-center">SOLICITUD DE TRANSFERENCIA</h2>
                    <div class="panel panel-default ">
                        <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD DE TRANSFERENCIA</h2></div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="reporte-solicitud">
                                <tr>
                                    <td><label for="pago">Proyecto: </label></td>
                                    <td colspan="3" id='modal-proyecto'></td>
                                    <td><label for="turno">Fecha Limite: </label></td>
                                    <td id="modal-fecha-lim"></td>
                                </tr>
                                <tr>
                                    <td><label for="ruta">Solicitante: </label></td>
                                    <td colspan="5" id="modal-solicitante"></td>
                                </tr>
                                <tr>
                                    <td><label for="grupo">Desembolso A: </label></td>
                                    <td colspan="5" id='modal-desembolso'></td>
                                </tr>
                                <tr>
                                    <td><label for="grupo">Tipo de Cambio: </label></td>
                                    <td id='modal-cambio'></td>
                                    <td><label for="grupo">Modalidad: </label></td>
                                    <td id='modal-modalidad'></td>
                                    <td><label for="grupo">Moneda: </label></td>
                                    <td id='modal-moneda'></td>
                                </tr>
                                <tr>
                                    <td><label for="grupo">Referencia: </label></td>
                                    <td colspan="5" id='modal-referencia'></td>
                                </tr>
                                <tr>
                                    <td><label for="grupo">Justificación: </label></td>
                                    <td colspan="5" id='modal-observacion'></td>
                                </tr>
                                <tr class="no-print">
                                    <td class="text-right"><strong>Documento(s)<br>Adjunto(s)</strong></td>
                                    <td colspan="9" id='modal-documentos'></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-heading"><h2>DETALLE DE LA TRANSFERENCIA</h2></div>
                        <div class="panel-body panel-items">
                            <table class="table table-hover table-bordered tbl-modal-items">
                                <tbody id='modal-items'>
                                    <tr>
                                        <th>Cuenta Bancaria Origen</th>
                                        <th>Tipo de Transferencia</th>
                                        <th>Monto de Transferencia</th>
                                    </tr>
                                    <tr>
                                        <td id="modal-cuenta_origen"></td>
                                        <td id="modal-tipo_transaccion"></td>
                                        <td id="modal-monto_transferencia"></td>
                                    </tr>
                                    <tr>
                                        <th>Empresa Destino</th>
                                        <th>Cuenta Bancaria Destino</th>
                                        <th>Factor de conversión</th>
                                    </tr>
                                    <tr>
                                        <td id="modal-empresa_destino"></td>
                                        <td id="modal-cuenta_destino"></td>
                                        <td id="modal-tipo_cambio"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <p><strong>SON:</strong> <span id="modal-literal"></span> CON <span id="modal-decimal"></span>/100 <span id="monedaTag"></span></p>
                        </div>
                    </div>
                    <div class="panel panel-default panel-solicitante" style="display:none">
                        <div class="panel-heading"><h2>CONTACTO SOLICITANTE</h2></div>
                        <div class="panel-body">
                            <table class="table" id="table-contacto">
                                <tr>
                                    <td>
                                        <strong>Nombre: </strong><span id="modal-contacto"></span><br>
                                        <strong>Telefono: </strong><span id="modal-telefono"></span><br>
                                        <strong>Fecha y hora de Solicitud: </strong><span id="modal-fecha"></span> - <span id="modal-hora"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
    
                <div class="modal-footer no-print">
                    @if(!isset($solicitud))
                    <div class="col-md-6 col-md-offset-3">
                        <div class="text-center">
                            <input type="checkbox" name="correo" id="correo" value="correo" onclick="enviar_solicitud()">&nbsp;&nbsp;<label for='correo'>¿Enviar Solicitud a Correo?</label>
                            {!! Form::text('correo_envio', null, array('placeholder' => '','class' => 'form-control', 'placeholder' => 'Ingrese correo...', 'style' => 'margin-bottom: 15px;', 'id' => 'correo_envio')) !!}
                        </div>
                    </div>
                    @endif
                    <div class="col-md-12">
                        <span class="pull-right">
                            <button id="guardar_btn" type="button" class="btn btn-primary btn_modificar" onclick="verificarFormTransferencia(this)">GUARDAR SOLICITUD</button>
                        </span>
                        <span class="pull-right">
                            {{-- <button type="button" class="btn btn-default" data-dismiss="modal">MODIFICAR DATOS</button> --}}
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{ isset($solicitud)?'CERRAR':'MODIFICAR DATOS' }}</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>