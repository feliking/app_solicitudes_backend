<div class="modal fade" id="aprobarModalTransferencia" tabindex="-1" role="dialog" aria-labelledby="aprobarModalTransferenciaLabel">
        <div class="modal-dialog modal-lg " role="document">
            <div class="modal-content">
                <br>
                <div class="modal-header no-print">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center text-success" id="aprobarModalTransferenciaLabel"></h4>
                </div>
                <div class="modal-body">
                    <div class="panel panel-default ">
                        <div class="panel-heading" id="aprobarModalTransferenciaTitulo"></div>
                        <div class="panel-body" style="overflow-x: scroll;">
                            <table class="table table-bordered" id="aprobar-solicitud">
                                <tr>
                                    <td><strong>PROYECTO: </strong></td>
                                    <td colspan="5" id='modalautorizar-trs-proyecto'></td>
                                    <td><strong>FECHA LÍMITE: </strong></td>
                                    <td id="modalautorizar-trs-fecha-lim"></td>
                                </tr>
                                <tr>
                                    <td><strong>SOLICITANTE: </strong></td>
                                    <td colspan="9" id="modalautorizar-trs-solicitante"></td>
                                </tr>
                                <tr>
                                    <td><strong>DESEMBOLSO A: </strong></td>
                                    <td colspan="9" id='modalautorizar-trs-desembolso'></td>
                                </tr>
                                <tr id="tr_trs_con_tipo_apr">
                                    <td><strong>TIPO DE CAMBIO: </strong></td>
                                    <td id='modalautorizar-trs-cambio'></td>
                                    <td><strong>MODALIDAD: </strong></td>
                                    <td colspan="3" id='modalautorizar-trs-modalidad'></td>
                                    <td><strong>MONEDA: </strong></td>
                                    <td colspan="3" id='modalautorizar-trs-moneda'></td>
                                </tr>
                                <tr id="tr_trs_sin_tipo_apr">
                                    <td><strong>MODALIDAD: </strong></td>
                                    <td colspan="4" id='modalautorizar-trs-modalidad-sin'></td>
                                    <td><strong>MONEDA: </strong></td>
                                    <td colspan="4" id='modalautorizar-trs-moneda-sin'></td>
                                </tr>
                                <tr>
                                    <td><strong>REFERENCIA: </strong></td>
                                    <td colspan="9" id='modalautorizar-trs-referencia'></td>
                                </tr>
                                <tr>
                                    <td><strong>JUSTIFICACIÓN: </strong></td>
                                    <td colspan="9" id='modalautorizar-trs-observacion'></td>
                                </tr>
                                <tr class="no-print">
                                    <td class="text-right"><strong>TOTAL</strong></td>
                                    <td colspan="9" id='modalautorizar-trs-total'></td>
                                </tr>
                            </table>
                            <div>
                                <div id="div_items_trs_solicitud">
                                    <h4>DETALLE DE LA TRANSFERENCIA</h4>
                                    <table class="table table-bordered table-items" id="table_trs_items">
                                        <tbody id='modal-trs-items'>
                                        </tbody>
                                    </table>
                                </div>
                                <p><strong>SON: </strong> <span id="modal-trs-literal-aprobar"></span> CON <span id="modal-trs-decimal-aprobar"></span>/100 <span id="modal-trs-moneda-aprobar"></span></p>
                            </div>
                            <div class="text-center">
                                <input type="checkbox" name="observa" id="observa" onclick="verificar()">&nbsp;&nbsp;<label for='correo'>¿DESEA REALIZAR OBSERVACIÓN?</label>
                                
                                {!! Form::textarea('des_observacion', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA SUS OBSERVACIONES...', 'style' => 'text-transform:uppercase', 'id' => 'des_observacion')) !!}
    
                                <span class="help-block">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"  id="modalautorizar-trs-opciones">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('footerScripts')
    @parent
    <script>
        document.getElementById('des_observacion').style.display = 'none';
        function verificar(){
            if(document.getElementById('observa').checked){
                document.getElementById('des_observacion').style.display = 'block';
            }else{
                document.getElementById('des_observacion').style.display = 'none';
            }
        }
    </script>
    
    @endsection