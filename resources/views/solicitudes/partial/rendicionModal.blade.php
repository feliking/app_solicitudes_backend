<div class="modal fade" id="rendicionModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success">
                    <strong>{{ session('empresa_nombre') }}</strong>
                </h4>
                <h4 class="modal-title text-center text-success" id="rendicionSolicitudModalLabel">
                    <strong>DATOS DE LA SOLICITUD # <span id="modal-num-rendir"></span></strong>
                </h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default ">
                    <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                    <div class="panel-body">
                        <table class="table table-bordered" id="reporte-solicitud">
                            <tr>
                                <td><label>PROYECTO: </label></td>
                                <td colspan="5" id='modal-proyecto-rendir'></td>
                                <td><label>Fecha Limite: </label></td>
                                <td id="modal-fecha-lim-rendir"></td>
                            </tr>
                            <tr>
                                <td><label>SOLICITANTE: </label></td>
                                <td colspan="9" id="modal-solicitante-rendir"></td>
                            </tr>
                            <tr>
                                <td><label>TIPO DE CAMBIO: </label></td>
                                <td id='modal-cambio-rendir'></td>
                                <td><label>MODALIDAD: </label></td>
                                <td colspan="3" id='modal-modalidad-rendir'></td>
                                <td><label>MONEDA: </label></td>
                                <td colspan="3" id='modal-moneda-rendir'></td>
                            </tr>
                        </table>
                        <div >
                            <div id="div_items_antiguos_rendir">
                                <table>
                                    <tr>
                                        <td><label>DESEMBOLSO ANTIGUO A: </label></td>
                                        <td id='modal-desembolso-antiguo'></td>
                                    </tr>
                                </table>
                                <h4>ITEMS SOLICITUD ANTIGUOS</h4>
                                <table class="table table-bordered table-items table-items-antiguos" id="table_items_rendir">
                                    <thead>
                                        <tr>
                                            <th style="width:10px">#</th>
                                            <th style="width:20px">UNIDAD</th>
                                            <th style="width:20px">TIPO COMPRA</th>
                                            <th>DETALLE</th>
                                            <th style="width:20px">CANTIDAD</th>
                                            <th style="width:20px">P. UNITARIO</th>
                                            <th style="width:20px">SUB TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody id='modal-items-antiguos'>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border: 2px solid green; padding: 5px; border-radius: 5px; background: #c6ecc6;">
                                <table>
                                    <tr>
                                        <td><label>DESEMBOLSO A: </label></td>
                                        <td id='modal-desembolso-rendir'></td>
                                    </tr>
                                </table>
                                <div id="div_items_solicitud">
                                    <h4>ITEMS SOLICITUD</h4>
                                    <table class="table table-bordered table-items" id="table_items">
                                        <thead>
                                            <tr>
                                                <th style="width:10px">#</th>
                                                <th style="width:20px">UNIDAD</th>
                                                <th style="width:20px">TIPO COMPRA</th>
                                                <th>DETALLE</th>
                                                <th style="width:20px">CANTIDAD</th>
                                                <th style="width:20px">P. UNITARIO</th>
                                                <th style="width:20px">SUB TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody id='modal-items'>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="div_items_rendidos_rendir">
                                    <h4>ITEMS RENDIDOS</h4>
                                    <table class="table table-bordered table-items" id="table_items_rendidos">
                                        <thead>
                                            <tr>
                                                <th style="width:10px">#</th>
                                                <th style="width:20px">UNIDAD</th>
                                                <th style="width:20px">TIPO COMPRA</th>
                                                <th>DETALLE</th>
                                                <th style="width:20px">CANTIDAD</th>
                                                <th style="width:20px">P. UNITARIO</th>
                                                <th style="width:20px">SUB TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody id='modal-items'>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <p><strong>SON:</strong> <span id="modal-literal-rendir"></span> CON <span id="modal-decimal-r"></span>/100 BOLIVIANOS</p>
                            <table class="table table-bordered" id="a">
                                <tr>
                                    <td style="width: 100px"><label>REFERENCIA: </label></td>
                                    <td id='modal-referencia-rendir'></td>
                                </tr>
                                <tr>
                                    <td><label>JUSTIFICACIÓN: </label></td>
                                    <td id='modal-observacion-rendir'></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-print"><!-- ELIMINAR DISPLAY NONE -->
                <div id="solicitud_rendir"></div>
            </div>
        </div>
    </div>
</div>
@section('footerScripts')
@parent

<script language="javascript" type="text/javascript">
    function rendirModal(e){
        var url_solicitud = '/json/datos_solicitud/'+e;
        var url_solicitud_items = '/json/items_solicitud/'+e;
        var numeroSolicitud = $('#sol_hist_'+e).attr('data-numero');
        document.getElementById('rendicionSolicitudModalLabel').innerHTML = "<strong>SOLICITUD A RENDIR # "+numeroSolicitud+"</strong>";
        $('#solicitud_info').empty();
        $('#solicitud_rendir').empty();

        document.getElementById('modal-solicitante-rendir').innerHTML = document.getElementById('usuario_'+e).innerHTML;
        document.getElementById('modal-proyecto-rendir').innerHTML = document.getElementById('proyecto_td_'+e).title;
        document.getElementById('modal-desembolso-rendir').innerHTML = document.getElementById('desembolso_'+e).innerHTML;
        document.getElementById('modal-cambio-rendir').innerHTML = document.getElementById('cambio_'+e).innerHTML;
        document.getElementById('modal-fecha-lim-rendir').innerHTML = document.getElementById('fecha_'+e).innerHTML;
        //document.getElementById('modal-num-rendir').innerHTML = document.getElementById('solicitud_'+e).innerHTML;
        document.getElementById('modal-cambio-rendir').innerHTML = document.getElementById('cambio_'+e).innerHTML;
        document.getElementById('modal-modalidad-rendir').innerHTML = document.getElementById('tipo_'+e).innerHTML;
        document.getElementById('modal-moneda-rendir').innerHTML = document.getElementById('moneda_'+e).innerHTML;
        document.getElementById('modal-referencia-rendir').innerHTML = document.getElementById('referencia_'+e).innerHTML;
        document.getElementById('modal-observacion-rendir').innerHTML = document.getElementById('obs_'+e).innerHTML;
        $("#table_items tbody").empty();
        $("#table_items_rendidos tbody").empty();
        $("#table_items_rendir tbody").empty();

        var total = document.getElementById('total_'+e).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        if(p_entera != ""){
            var str_literal = numeroLiteral(p_entera);
            document.getElementById('modal-literal-rendir').innerHTML = str_literal.toUpperCase();
        }

        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-r').innerHTML = str_decimal;
        //str_items += "<tr><td class='text-right'>" + (cont_items) + "</td><td class='text-right'>" + td_can + "</td><td>" + td_uni + "</td><td>" + td_det + "</td><td class='text-right'>" + td_pre + "</td><td class='text-right'>" + td_sub + "</td></tr>";
        var fila_sol = document.getElementById('solicitud_'+e).innerHTML;

        var url_solicitud = '/json/datos_solicitud/'+e;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            var des_antiguo = datos.desembolso_antiguo;
            $('#modal-desembolso-antiguo').append(des_antiguo);
        });

        //item_table.style('display: block');
        var url = "/json/items_solicitud/" + e;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $.each(data, function (i, object) {
                $fila = "<tr>";
                $fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                $fila += "<td>"+object.unidad_descripcion+"</td>";
                $fila += "<td>"+object.tipocompra+"</td>";
                $fila += "<td>"+object.detalle+"</td>";
                $fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                $fila += "<td style='text-align:right;' step='0.00001'>"+object.costo+"</td>";
                $fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                $fila += "</tr>";
                $("#table_items tbody").append($fila);
                total_items += parseFloat(object.subtotal);
            });
            $fila = "<tr>";
            $fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            $fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            $fila += "</tr>";
            $("#table_items tbody").append($fila);
        });

        var span_p = document.getElementById('print-span');
        span_p.innerHTML = "";

        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", e);
        file.innerHTML = "IMPRIMIR";
        file.setAttribute("onclick", "imprimirSolicitud(this)");
        span_p.appendChild(file);

        var url_antiguos = "json/items_tiene_antiguos/"+e;
        var div_antiguos = document.getElementById('div_items_antiguos_rendir');
        div_antiguos.setAttribute('style', 'display: none;');
        var cantidad = 0;
        $.getJSON(url_antiguos, function(data){
            cantidad = parseInt(data);
            if(cantidad > 0){
                var div_antiguos = document.getElementById('div_items_antiguos_rendir');
                div_antiguos.setAttribute('style', 'display: block; border: 2px solid orange; padding: 5px; margin: 15px 0px; border-radius: 5px; background: #ffebcc;');
                var url_items_antiguos = "/json/items_antiguos_solicitud/"+e;
                $.getJSON(url_items_antiguos, function(data) {
                    $("#table_items_antiguos tbody").empty();
                    total_items = 0;
                    $.each(data, function (i, object) {
                        fila = "<tr>";
                        fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                        fila += "<td>"+object.unidad_descripcion+"</td>";
                        fila += "<td>"+object.tipocompra+"</td>";
                        fila += "<td>"+object.detalle+"</td>";
                        fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                        fila += "<td style='text-align:right;' step='0.00001'>"+object.costo+"</td>";
                        fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                        fila += "</tr>";
                        $("#table_items_rendir tbody").append(fila);
                        total_items += parseFloat(object.subtotal);
                    });
                    fila = "<tr>";
                    fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                    fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
                    fila += "</tr>";
                    $("#table_items_rendir tbody").append(fila);
                });
            }
        });

        var url_lastEstadoCodigo = "/solicitud-lastEstadoCodigo/"+e;
        var div_antiguos = document.getElementById('div_items_rendidos_rendir');
        div_antiguos.setAttribute('style', 'display: none;');

        $.get(url_lastEstadoCodigo, function(data) {
            if(data == "REN-F"){
                var url_rendidos = "json/items_rendidos/"+e;
                var cantidad = 0;

                $.getJSON(url_rendidos, function(data){
                    cantidad = data.length;
                    if(cantidad > 0){
                        var div_antiguos = document.getElementById('div_items_rendidos');
                        div_antiguos.setAttribute('style', 'display: initial;');
                        total_items = 0;

                        $.each(data, function (i, object) {
                            fila = "<tr>";
                            fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                            fila += "<td>"+object.unidad_descripcion+"</td>";
                            fila += "<td>"+object.tipocompra+"</td>";
                            fila += "<td>"+object.detalle+"</td>";
                            fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                            fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                            fila += "<td style='text-align:right;'>"+parseFloat(object.subtotal).toFixed(2)+"</td>";
                            fila += "</tr>";
                            $("#table_items_rendidos tbody").append(fila);
                            total_items += parseFloat(object.subtotal);
                        });
                        fila = "<tr>";
                        fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                        fila += "<td style='text-align:right;'>"+parseFloat(total_items).toFixed(2)+"</td>";
                        fila += "</tr>";
                        $("#table_items_rendidos tbody").append(fila);
                    }
                });
            }
        });

        //Rendir solicitud
        var btn_rendir = "<button type='button' class='btn btn-default' data-dismiss='modal'>CERRAR</button>";
        btn_rendir += "<a href='/rendir/solicitud/"+e+"' class='btn btn-primary'>REALIZAR RENDICIÓN</a>";
        $('#solicitud_rendir').append(btn_rendir);
    }

    function imprimirSolicitud(e){
        var id = e.id;
        window.open('/solicitud-imprimir?id_sol='+id);
    }
</script>

@endsection