@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
@endsection
@section('content')
@php
if(!isset($array_addquery))
    $array_addquery = '';
@endphp
<div class="container" style="overflow-x: scroll;">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-default no-print">
                <div class="panel-heading" style="background: #1ABB9C">
                    <div class="col-md-4">
                        <h2 style='color: #FFF'>Solicitudes Disponibles</h2>
                    </div>
                    <div class="col-md-8">
                        @php
                        $array_data_get = ['SolicitudController@disponibles'];
                        if(isset($_GET['tabs'])){
                            $array_data_get['tabs'] = $_GET['tabs'];
                        }
                        @endphp
                        {!! Form::open(array('action' => $array_data_get, 'method'=>'POST', 'class'=>'form-inline')) !!}
                            @php
                            if(!isset($extras)){
                                $extras = ['FFF','D5D5D5'];
                            }
                            @endphp
                            @if(isset($_GET['tabs']))
                                {!! Form::hidden('tabs', $_GET['tabs'], ['class' => 'form-control', 'id' => 'tabs']) !!}
                            @endif
                            @if(isset($_GET['page']))
                                {!! Form::hidden('page', $_GET['page'], ['class' => 'form-control', 'id' => 'f-page']) !!}
                            @endif
                            @if(isset($_GET['conq']))
                                {!! Form::hidden('conq', $_GET['conq'], ['class' => 'form-control', 'id' => 'f-conq']) !!}
                            @endif
                            {!! Form::label('buscar', 'BUSCAR: ', ['id' => 'lblBuscar', 'style' => 'color: #'.$extras[0]]) !!}
                            {!! Form::text('buscar', null, ['placeholder' => 'TEXTO A BUSCAR', 'class' => 'form-control', 'style' => 'text-transform:uppercase', 'id' => 'buscar']) !!}
                            {!! Form::number('numero', null, ['placeholder' => 'INTRODUZCA CANTIDAD', 'class' => 'form-control', 'style' => 'display: none;', 'id' => 'numero']) !!}
                            {!! Form::date('fechaIni', null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'text-transform:uppercase; display: none; width: 135px; padding: 6px;', 'id' => 'fechaIni']) !!}
                            {!! Form::label('fechaFin', 'HASTA: ', ['id' => 'lblFechaFin', 'style' => 'display: none;']) !!}
                            {!! Form::date('fechaFin', null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'text-transform:uppercase; display: none; width: 135px; padding: 6px;', 'id' => 'fechaFin']) !!}
                            <div class="input-group" style="margin-bottom: 0px; width: 230px;">
                                {!! Form::select('selectOpcion', ['TODOS', 'NRO. DE SOLICITUD', 'MONTO', 'PROYECTO', 'FECHA', 'SOLICITANTE', 'DESEMBOLSO', 'REFERENCIA', 'JUSTIFICACIÓN'], 0, ['class' => 'form-control', 'onchange' => 'selOpcion()', 'id' => 'selectOpcion', 'style' => 'padding: 6px;']) !!}
                                <span class="input-group-btn" style="width:0;">
                                    {!! Form::submit('BUSCAR', ['class' => 'btn btn-default']) !!}
                                </span>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    {{ $solicitudes->appends(['conq' => $conq])->links() }}
                    <table class="table table-bordered table-hover table-solicitudes">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>@sortablelink('estado', 'ESTADO')</th>
                                <th>@sortablelink('total', 'MONTO')</th>
                                <th class="proy_simple"><a class="fa fa-arrow-circle-right icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> @sortablelink('proyecto_id', 'PROYECTO')</th>
                                <th class="proy_cols"><a class="fa fa-arrow-circle-left icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> CENTRO DE COSTO</th>
                                <th class="proy_cols">GENERADOR DE COSTO</th>
                                <th class="proy_cols">ACTIVIDAD</th>
                                <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($solicitudes as $solicitud)
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)">
                                    <td>
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-target="#historialSolicitud" data-toggle="modal"></a>
                                        @php
                                            $usuario = auth()->user();
                                        @endphp
                                        @if($solicitud->estado == 'SOLICITUD CREADA' && $usuario->id == $solicitud->usuario_id)
                                            <a class="fa fa-edit icono-opciones-files icono-green" href="{{ route('solicitud.edit', $solicitud->id) }}" title="Modificar Solicitud"></a>
                                        @endif
                                        @if(count($solicitud->itemsRendidos) > 0 || count($solicitud->devoluciones) > 0)
                                            <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-o icono-opciones-files icono-green" href="/solicitud-imprimir/rendicion?id_sol={{ $solicitud->id }}" target="blank" title="Ver Rendición"></a>
                                        @endif
                                    </td>
                                    <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                        <span id="nom_proy_{{ $solicitud->id }}" style="display:none;"></span>
                                        <span id="nom_subproy_{{ $solicitud->id }}" style="display:none;"></span>
                                    </td>
                                    <td>
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                        @endif
                                        @if(sizeof($solicitud->documentosRendidos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs_rendidos(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-orange" title="Descargar Documento(s)"></i></a>
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                            {{ $solicitud->estadoCodigo }}
                                        </span>
                                    </td>
                                    <td id="total_{{ $solicitud->id }}" style="text-align: right;">{{ number_format($solicitud->total, '2', '.', ',') }}</td>
                                    <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}" class="proy_simple">
                                        <i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i>
                                    </td>
                                    <td class="proy_cols">
                                     @php
                                        switch($solicitud->proyecto->nivel){
                                           case 1:
                                              echo $solicitud->proyecto->nombre;
                                              break;
                                           case 2:
                                              echo $solicitud->proyecto->padre->nombre;
                                              break;
                                           case 3:
                                              echo $solicitud->proyecto->padre->padre->nombre;
                                              break;
                                           default:
                                              echo "";
                                              break;
                                        }
                                     @endphp
                                    </td>
                                    <td class="proy_cols">
                                     @php
                                        switch($solicitud->proyecto->nivel){
                                           case 2:
                                              echo $solicitud->proyecto->nombre;
                                              break;
                                           case 3:
                                              echo $solicitud->proyecto->padre->nombre;
                                              break;
                                            default:
                                              echo "";
                                              break;
                                        }
                                     @endphp
                                    </td>
                                    <td class="proy_cols">{{ ($solicitud->proyecto->nivel == 3)?$solicitud->proyecto->nombre:"" }}</td>
                                    <td id="usuario_{{ $solicitud->id }}">
                                        {{ $solicitud->usuario->nombre }}
                                    </td>
                                    <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                    <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                    <td id="cambio_{{ $solicitud->id }}">{{ $solicitud->tipo_cambio->cambio }}</td>
                                    <td id="tipo_{{ $solicitud->id }}">{{$solicitud->modalidad->nombre}}</td>
                                    <td id="moneda_{{ $solicitud->id }}">{{$solicitud->moneda->nombre}}</td>
                                    <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->appends(['conq' => $conq])->links() }}
                </div>
            </div>
            <!-- MODAL -->
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>

            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            @include('solicitudes.partial.docsUp')
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <script>
                function imprimirSolicitud(e){
                    var id = e.id;
                    window.location.href = '/solicitud-imprimir?id_sol='+id;
                }
            </script>
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
@parent
    {{ Html::script('/js/main.js') }}
    <script type="text/javascript">
        var config = {
            rutas:[
                {
                    showCorrespon: "{{route('documentos.showcr', ['id'=>':id'])}}",
                    descargar: "{{ route('doc.descargar', ['id'=>':id']) }}",
                    storage: "{{ asset('storage/archivo') }}",
                    token: "{{Session::token()}}"
                }
            ]
        };
    </script>
    {{Html::script('/js/docs/documento_solicitud.js')}}

    <script type="text/javascript">
        function selOpcion(){
            var selId = document.getElementById('selectOpcion');
            var lblBuscar = document.getElementById('lblBuscar');
            var inputBuscar = document.getElementById('buscar');
            var inputNumero = document.getElementById('numero');
            var inputFechaIni = document.getElementById('fechaIni');
            var lblFechaFin = document.getElementById('lblFechaFin');
            var inputFechaFin = document.getElementById('fechaFin');
            var selOpcion = document.getElementById('selectOpcion');
            
            var x = document.getElementById("selectOpcion").selectedIndex;
            var y = document.getElementById("selectOpcion").options;
            if(y[x].value == 0 || y[x].value == 5 || y[x].value == 6 || y[x].value == 7 || y[x].value == 8){
                //0 Todos
                //5 Solicitante
                //6 Desembolso
                //7 Referencia
                //8 Observacion
                lblBuscar.innerHTML = "BUSCAR: ";
                inputBuscar.style.display = "initial";
                inputNumero.style.display = "none";
                inputFechaIni.style.display = "none";
                lblFechaFin.style.display = "none";
                inputFechaFin.style.display = "none";

            }
            if(y[x].value == 1 || y[x].value == 2){
                //1 Numero de solicitud
                //2 Monto
                lblBuscar.innerHTML = "BUSCAR: ";
                inputBuscar.style.display = "none";
                inputNumero.style.display = "initial";
                inputFechaIni.style.display = "none";
                lblFechaFin.style.display = "none";
                inputFechaFin.style.display = "none";
            }
            if(y[x].value == 3){
                //3 Proyecto
                lblBuscar.innerHTML = "BUSCAR: ";
                inputBuscar.style.display = "initial";
                inputNumero.style.display = "none";
                inputFechaIni.style.display = "none";
                lblFechaFin.style.display = "none";
                inputFechaFin.style.display = "none";
            }
            if(y[x].value == 4){
                //4 Fecha
                lblBuscar.innerHTML = "DESDE: ";
                inputBuscar.style.display = "none";
                inputNumero.style.display = "none";
                inputFechaIni.style.display = "initial";
                lblFechaFin.style.display = "initial";
                inputFechaFin.style.display = "initial";
            }
        }
    </script>
@endsection
