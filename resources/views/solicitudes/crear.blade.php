@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
@php
use App\Proyectos;
$user = auth()->user();

$id_proys = array();
$str_proys = '';
if(sizeof($proyectos) == 0){
    echo "<script>window.location.href = '/solicitud';</script>";
    exit();
}
$ids_proys = array();
foreach ($proyectos as $proye) {
    $ids_proys[] = $proye['id'];
    $str_proys .= $proye['id'];
    if ($proye !== end($proyectos)) {
        $str_proys .= ',';
    }
}
if (substr($str_proys, -1) == ',') {
    $str_proys = substr($str_proys, 0, strlen($str_proys) - 1);
}
$consulta_proy = '';
if(!isset($ids_proys)){
//    echo "<script>window.location.href = '/solicitud';</script>";
//    exit();
    //Mostrar redireccionar y mostrar mensaje
}
$resp_proy = $proyectos;
@endphp

    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Guardar -->
                {!! Form::open(array('route' => 'solicitud.store', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true, 'onkeypress' => 'return event.keyCode != 13;', 'id' => 'frm')) !!}
                <!-- Enviar a Controller -->
                <div class="panel panel-default no-print">
                    <div class="panel-heading"><h2>Información Solicitud</h2></div>
                    <div class="panel-body">
                        @include('solicitudes.partial.form')
                    </div>
                </div>
                <div class="panel panel-default no-print">
                    <div class="panel-heading">
                        <div class="col-md-8">
                            <h2>Items Para Solicitud</h2>
                            <span style="color:green; font-weight: bold;">(Introduzca cantidad y precios unitarios correspondientes)</span>
                        </div>
                        <div id="seccion_excel" class="col-md-4 text-right" style="display: none;">
                            <div class="btn-group" role="group">
                                <a class="btn btn-default" href='{{ url("public") }}/fr_excel.xlsx'>
                                    <i class="fa fa-download"></i>  Descargar Plantilla Excel
                                </a>
                            </div>
                            <br/><br/>
                            <div class="btn-group" role="group">
                                {!! Form::file('file_excel', ['style' => 'display: none;']) !!}
                                <button id="btn_upload_excel" class="btn btn-info" type="button" onclick="document.getElementsByName('file_excel')[0].click()">
                                    <i class="fa fa-file-excel-o"></i> Seleccionar Plantilla Excel
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="solicitud-items">
                                <thead>
                                    <tr>
                                        <th style="width: 1%">#</th>
                                        <th style="width: 5%">Cantidad</th>
                                        <th style="width: 9%">Unidad</th>
                                        <th style="width: 9%">Tipo Compra</th>
                                        <th style="width: 15%">Detalle</th>
                                        <th style="width: 5%" class="row_fr">Nro. Factura / Recibo</th>
                                        <th style="width: 5%" class="row_fr">Fecha Factura / Recibo</th>
                                        <th style="width: 10%" class="row_fr">Proyecto</th>
                                        <th style="width: 10%" class="row_fr">Proveedor</th>
                                        <th style="width: 10%" class="row_fr">Tipo</th>
                                        <th style="width: 8%">P. Unitario</th>
                                        <th style="width: 8%">Sub-Total</th>
                                        <th style="width: 5%">Opción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Filas de Items en la solicitud -->
                                </tbody>
                            </table>

                            <span id="table-items" class="help-block" style="color: #a94442;">
                                <strong></strong>
                            </span>

                            <table class="table table-hover" id="solicitud-items-total" style="margin-bottom: 20px;">
                                <tbody>
                                    <tr style="border: 0;">
                                        <td colspan="3"><a id="btn-creafila1" class="btn btn-primary" onclick="creaFila()"><i class="fa fa-plus-circle icono-opciones"></i> AGREGAR NUEVO ITEM</a></td>
                                        <td style="width: 10%; text-align: right; padding-top: 15px;"><strong>TOTAL: </strong></td>
                                        <td style="width: 10%;"><a class="btn btn-info" id="total" style="width: 100%; margin: 0px; text-align:right;">0</a></td>
                                        <td colspan="2" style="text-align:left; width: 5%; padding-top: 15px;"><strong id="monedaSiglaTag"> Bs.</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('referencia') ? ' has-error' : '' }}">
                                <label for="referencia" class="col-md-2 control-label">REFERENCIA </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('referencia', null, ['placeholder' => 'DESCRIBA LOS ITEMS QUE SE COMPRARAN CON LA SOLICITUD','class' => 'form-control', 'id' => 'referencia', 'style' => 'text-transform:uppercase', 'rows' => 4]) !!}

                                    @if ($errors->has('referencia'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('referencia') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('observacion') ? ' has-error' : '' }}">
                                <label for="observacion" class="col-md-2 control-label">JUSTIFICACIÓN </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('observacion', null, ['placeholder' => 'DESCRIBA EL USO QUE SE LE DARA EN EL PROYECTO','class' => 'form-control', 'id' => 'observacion', 'style' => 'text-transform:uppercase', 'rows' => 4]) !!}

                                    @if ($errors->has('observacion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('observacion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                {!! Form::hidden('imprimir', '0', array('id' => 'imprimir')) !!}
                                {!! Form::submit('REALIZAR SOLICITUD OCULTO', ['class' => 'btn btn-success btn-block', 'id' => 'btn-registro-solicitud', 'style' => 'display:none']) !!}
<!--                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#verificarModal" onclick="datosModal()">-->
                                <button type="button" class="btn btn-success" onclick="datosModal()">
                                    REALIZAR SOLICITUD
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- MODAL -->
                @include('solicitudes.partial.modalVerificar')
                <!-- END MODAL -->

                {!! Form::close() !!}
                <!-- MODAL DOCS PREVIEW-->
                <div class="modal fade" id="previewDocsModal" tabindex="-1" role="dialog" aria-labelledby="previewDocsModalLabel">
                    <div class="modal-dialog modal-lg " role="document">
                        <div class="modal-content">
                            <br>
                            <div class="modal-header no-print">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title text-center text-success" id="previewDocsModalLabel"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="panel panel-default ">
                                    <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                                    <div class="panel-body">
                                        {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                                            {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                                            <table class="table-modal-subir-archivo">
                                                <tr>
                                                    <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                                    <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                                </tr>
                                            </table>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->
            </div>
        </div>
    </div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    
    var contador;
    $(document).ready(function (e){
        //Filas Tabla Items
        console.log("123456");
        contador = 0;
        creaFila();
        console.log("asdf");
    });

    document.getElementById('correo_envio').style.display = 'none';
    
    function enviar_solicitud(){
        if(document.getElementById('correo').checked){
            document.getElementById('correo_envio').style.display = 'block';
        }else{
            document.getElementById('correo_envio').style.display = 'none';
        }
    }

    // Procesa el Excel
    // $('button#btn_process_excel').on('click', function(event){
    $('input[name="file_excel"]').on('change', function(event){
        // $('form#frm_excel').submit();
        
        var data = new FormData();
        data.append('file', $('input[name="file_excel"]').prop('files')[0]);

        $.ajax({
            url: '{{ route('importar.excel') }}',
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            // data: frm_excel.serialize(),
            data: data,
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
        }).done(function (response){
            if(!response.error){
                $.each(response['items'], function(key, value){
                    creaFilaDatos(value.cantidad, value.unidad_id, value.tipo_id, value.detalle, value.nro_factura, value.fecha_factura, value.proyecto, value.proveedor, value.tipo, value.costo);
                });
            }else{
                // alert(response.error);
                $('div#excelErrorModal .modal-body').html('<span>'+ response.error +'</span><br/><br/><span>No se importaron las datos.</span>')
                $('div#excelErrorModal').modal('show');
            }

            $('input[name="file_excel"]').val('');
        });
    });
</script>

@endsection