<div class="modal fade" id="aprobarModal" tabindex="-1" role="dialog" aria-labelledby="aprobarModalLabel">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <br>
            <div class="modal-header no-print">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center text-success" id="aprobarModalLabel"></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('action' => 'SolicitudController@cambiarAprobadorSolicitud', 'method'=>'PUT', 'class'=>'form-inline', 'files' => true)) !!}
                <div class="panel panel-default ">
                    <div class="panel-heading" id="aprobarModalTitulo"></div>
                    <div class="panel-body">
                        <table class="table table-bordered" id="aprobar-solicitud">
                            {!! Form::hidden('modalAprobar_solicitud_id', null, array('id' => 'modalAprobar_solicitud_id')) !!}
                            {!! Form::hidden('modalAprobar_empresa_id', session('empresa'), array('id' => 'modalAprobar_empresa_id')) !!}
                            <tr>
                                <td><strong>PROYECTO: </strong></td>
                                <td colspan="5" id='modalautorizar-proyecto'>
                                    <select class="select_control" name="selectProyectos" id="selectProyectos" onchange="cambiarBoton()" style="width: 100%;"></select>
                                </td>
                                <td><strong>FECHA LÍMITE: </strong></td>
                                <td id="modalautorizar-fecha-lim"></td>
                            </tr>
                            <tr>
                                <td><strong>SOLICITANTE: </strong></td>
                                <td colspan="9" id="modalautorizar-solicitante"></td>
                            </tr>
                            <tr>
                                <td><strong>DESEMBOLSO A: </strong></td>
                                <td colspan="9" id='modalautorizar-desembolso'></td>
                            </tr>
                            <tr>
                                <td><strong>TIPO DE CAMBIO: </strong></td>
                                <td id='modalautorizar-cambio'></td>
                                <td><strong>MODALIDAD: </strong></td>
                                <td colspan="3" id='modalautorizar-modalidad'></td>
                                <td><strong>MONEDA: </strong></td>
                                <td colspan="3" id='modalautorizar-moneda'></td>
                            </tr>
                            <tr>
                                <td><strong>REFERENCIA: </strong></td>
                                <td colspan="9" id='modalautorizar-referencia'></td>
                            </tr>
                            <tr>
                                <td><strong>JUSTIFICACIÓN: </strong></td>
                                <td colspan="9" id='modalautorizar-observacion'></td>
                            </tr>
                            <tr class="no-print">
                                <td class="text-right"><strong>TOTAL</strong></td>
                                <td colspan="9" id='modalautorizar-total'></td>
                            </tr>
                        </table>
                        <div>
                            <div id="div_items_solicitud">
                                <h4>ITEMS SOLICITUD</h4>
                                <table class="table table-bordered table-items" id="table_items">
                                    <thead>
                                        <tr>
                                            <th style="width:10px">#</th>
                                            <th style="width:20px">UNIDAD</th>
                                            <th style="width:20px">TIPO COMPRA</th>
                                            <th>DETALLE</th>
                                            <th style="width:20px">CANTIDAD</th>
                                            <th style="width:20px">P. UNITARIO</th>
                                            <th style="width:20px">SUB TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody id='modal-items'>
                                    </tbody>
                                </table>
                            </div>
                            <div id="div_items_rendidos" style="display: none;">
                                <h4>ITEMS RENDIDOS</h4>
                                <table class="table table-bordered table-items" id="table_items_rendidos">
                                    <thead>
                                        <tr>
                                            <th style="width:10px">#</th>
                                            <th style="width:20px">UNIDAD</th>
                                            <th style="width:20px">TIPO COMPRA</th>
                                            <th>DETALLE</th>
                                            <th style="width:20px">CANTIDAD</th>
                                            <th style="width:20px">P. UNITARIO</th>
                                            <th style="width:20px">SUB TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody id='modal-items'>
                                    </tbody>
                                </table>
                            </div>
                            <p><strong>SON: </strong> <span id="modal-literal-aprobar"></span> CON <span id="modal-decimal-aprobar"></span>/100 <span id="modal-moneda-aprobar"></span></p>
                        </div>
                        <div class="text-center">
                            <input type="checkbox" name="observa" id="observa" onclick="verificar()">&nbsp;&nbsp;<label for='correo'>¿DESEA REALIZAR OBSERVACIÓN?</label>
                            {!! Form::textarea('des_observacion', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlenght' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA SUS OBSERVACIONES...', 'style' => 'text-transform: uppercase; width: 100%;', 'id' => 'des_observacion')) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer"  id="modalautorizaropciones">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@section('footerScripts')
@parent
<script>    
    document.getElementById('des_observacion').style.display = 'none';
    
    function verificar(){
        if(document.getElementById('observa').checked){
            document.getElementById('des_observacion').style.display = 'block';

            document.getElementById('des_observacion').setAttribute('required', 'required');
        }else{
            document.getElementById('des_observacion').style.display = 'none';

            document.getElementById('des_observacion').removeAttribute('required');
        }
    }

</script>

<script language="javascript" type="text/javascript">

    /**
     * BEGIN: ISSUE 0040
     */
    // Obtiene los proyectos del usuario, que puede cambiar en la solicitud
    @if (\Auth::user()->global_view == 0)
    var empresa_proyecto_select = {{ session('empresa') }};
    var url = "/json/proyectos/empresa/"+empresa_proyecto_select;
    cad = "";

    $.getJSON(url, function(data) {
        $.each(data, function (i, object) {
            cad += "<option value="+object.id+">"+object.nombre+"</option>";
        });
        $("#selectProyectos").append(cad);
        cad = "";
    });
    @endif

    // Obtiene los tipos de compra de los ítems
    var urlTipos = "/json/tipos/compra";
    var tiposC;
    tiposC = "";
    $.getJSON(urlTipos, function(data1) {
        $.each(data1, function (i1, object1) {
            tiposC += "<option value='"+object1+"'>"+i1+"</option>";
        });
    });
    /**
     * END: ISSUE 0040
     */

    function aprobarModal(fila){
        $('#observa').prop('checked', false);
        document.getElementById('des_observacion').style.display = 'none';
        
        document.getElementById('des_observacion').removeAttribute('required');

        var sol_id = document.getElementById('sol_id').value;
        var apr_id = document.getElementById('apr_id').value;
        var url;
        var id_fila = fila;

        $('#modalAprobar_solicitud_id').val(fila);

        /**
         * BEGIN: ISSUE 0040
         */
        @if(\Auth::user()->global_view == 0)
        $("#selectProyectos").val(document.getElementById('proyecto_actual_id_' + fila).value);
        @endif
        /**
         * END: ISSUE 0040
         */

        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalautorizar-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalautorizar-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalautorizar-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalautorizar-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalautorizar-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalautorizar-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('aprobarModalLabel').innerHTML = "<strong>APROBAR SOLICITUD # "+numeroSolicitud+"</strong>";
        document.getElementById('modal-moneda-aprobar').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        /**
         * BEGIN: ISSUE 0040
         */
        document.getElementById('observa').checked = false;
        document.getElementById('des_observacion').value = '';
        document.getElementById('des_observacion').style.display = 'none';
         /**
         * END: ISSUE 0040
         */

        var span_p = document.getElementById('modalautorizaropciones');
        span_p.innerHTML = '';
        var file = document.createElement("button");
        file.setAttribute("type", "button");
        /**
         * BEGIN: ISSUE 0001
         */
        file.setAttribute("class", "btn btn-primary btn_modificar");
        /**
         * END: ISSUE 0001
         */
        file.setAttribute("name", "btn-guardarA");
        file.setAttribute("id", 'btn_guar');
        file.innerHTML = "APROBAR";
        file.setAttribute("onclick", "submit()");
        span_p.appendChild(file);
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        var url_solicitud = '/json/datos_solicitud/'+id_fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalautorizar-fecha-lim').innerHTML = datos.fecha_limite;

            @if (\Auth::user()->global_view == 1)
            var url = "/json/proyectos/empresa/"+datos.empresa_id;
            cad = "";

            $("#selectProyectos").empty();
            $.getJSON(url, function(data) {
                $.each(data, function (i, object) {
                    cad += "<option value="+object.id+">"+object.nombre+"</option>";
                });
                $("#selectProyectos").append(cad);
                cad = "";
                
                $("#selectProyectos").val(datos.proyecto_id);
            });

            @endif
        });

        url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            //REVISAR TIPOS COMPRA ACA DEBE IR LOS TIPOS DE COMPRA
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td><select name='tipocompra["+object.id+"]' onchange='cambiarBoton()'>";
                // id_fila += "<option value='"+object.tipo_id+"' selected>"+object.tipocompra+"</option>";
                id_fila += tiposC;
                id_fila += "</select></td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);

                /**
                 * BEGIN: ISSUE 0040
                 */
                $('select[name="tipocompra['+ object.id +']"]').val(object.tipo_id);
                /**
                 * END: ISSUE 0040
                 */

                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-aprobar').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-aprobar').innerHTML = str_decimal;
    }

    function cambiarBoton(){
        document.getElementById('btn_guar').innerHTML = "GUARDAR CAMBIOS";
        // console.log(btn_guardar);
        /**
         * BEGIN: ISSUE 0001
         */
        var solicitud_id = $("#modalAprobar_solicitud_id").val();
        var proyecto_actual_id = $("#proyecto_actual_id_"+solicitud_id).val();
        var usuario_id = $("#usuario_id_"+solicitud_id).val();
        var nombre_usuario = $("#usuario_"+solicitud_id).text();
        var numeroSolicitud = $('#sol_hist_'+solicitud_id).attr('data-numero');
        verificaProyecto(usuario_id, nombre_usuario, proyecto_actual_id, numeroSolicitud);
        /**
         * END: ISSUE 0001
         */
    }
</script>
@endsection