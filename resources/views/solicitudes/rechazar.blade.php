@extends('main')

@section('content')
<?php include_once(app_path().'/functions.php')?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-5">
                        <h2>Rechazar Solicitud</h2>
                    </div>
                    <?php
                        $contador = 1;
                        if($solicitud->padre_id != 0 ){
                            $proyecto_nombre = $solicitud->proyecto->nombre;
                            $subproy_nombre = "";
                        }else{

                            $proyecto_nombre = $solicitud->proyecto->nombre;
                            $subproy_nombre = "";
                        }
                        ?>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    <div class="modal-body">
                        <div class="panel panel-default ">
                            <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD #<?=$solicitud->numero?></h2></div>
                            <div class="panel-body">
                                <table class="table table-bordered" id="reporte-solicitud">
                                    <tr>
                                        <td><label>PROYECTO: </label></td>
                                        <td colspan="3" id='modal-proyecto'><?=$padre;?></td>
                                        <td><label>SUB PROYECTO: </label></td>
                                        <td colspan="3" id='modal-subproyecto'><?=$hijo;?></td>
                                        <td><label>Fecha Limite: </label></td>
                                        <td id="modal-fecha-lim"></td>
                                    </tr>
                                    <tr>
                                        <td><label>SOLICITANTE: </label></td>
                                        <td colspan="9" id="modal-solicitante"><?=$solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am;?></td>
                                    </tr>
                                    <tr>
                                        <td><label for="grupo">DESEMBOLSO A: </label></td>
                                        <td colspan="9" id='modal-desembolso'><?=$solicitud->desembolso?></td>
                                    </tr>
                                    <tr>
                                        <td><label>TIPO DE CAMBIO: </label></td>
                                        <td id='modal-cambio'><?=$solicitud->tipo_cambio?></td>
                                        <td><label>MODALIDAD: </label></td>
                                        <td colspan="3" id='modal-modalidad'><?=modalidad($solicitud->modalidad)?></td>
                                        <td><label>MONEDA: </label></td>
                                        <td colspan="3" id='modal-moneda'><?=moneda($solicitud->moneda)?></td>
                                    </tr>
                                    <tr>
                                        <td><label>REFERENCIA: </label></td>
                                        <td colspan="9" id='modal-referencia'><?=$solicitud->referencia?></td>
                                    </tr>
                                    <tr>
                                        <td><label>JUSTIFICACIÓN: </label></td>
                                        <td colspan="9" id='modal-observacion'><?=$solicitud->observacion?></td>
                                    </tr>
                                    <tr class="no-print">
                                        <td class="text-right"><strong>Documento(s)<br>Adjunto(s)</strong></td>
                                        <td colspan="9" id='modal-documentos'></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="panel panel-default ">
                            <div class="panel-heading"><h2>ITEMS SOLICITUD</h2></div>
                            <div class="panel-body panel-items">
                                    <table class="table table-bordered table-items">
                                        <thead>
                                            <tr>
                                                <th style="width:10px">#</th>
                                                <th style="width:20px">UNIDAD</th>
                                                <th>DETALLE</th>
                                                <th style="width:20px">CANTIDAD</th>
                                                <th style="width:20px">P. UNITARIO</th>
                                                <th style="width:20px">SUB TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody id='modal-items'>
                                    <?php
                                    $con_filas = 1;
                                    $suma_total = 0.00;
                                    if(count($items)>0){
                                        foreach($items as $item){ ?>
                                            <tr>
                                                <td><?=$con_filas++?></td>
                                                <td><?=$item->unidad_id;?></td>
                                                <td><?=$item->detalle;?></td>
                                                <td style="text-align: center;"><?=$item->cantidad;?></td>
                                                <td style="text-align: right;"><?=$item->costo;?></td>
                                                <td style="text-align: right;"><?=$item->subtotal;?></td>
                                            </tr>
                                            <?php
                                            $suma_total +=$item->subtotal;
                                        }?>
                                            <tr>
                                                <td colspan="5" style="text-align: right;"><strong>TOTAL</strong></td>
                                                <td style="text-align: right;"><?=$suma_total?></td>
                                            </tr>
                                        <?php
                                    }else{
                                        echo "<tr><td>Sin Items</td></tr>";
                                    }

                                    echo "</body></table>";
                                ?>
                                <p><strong>SON:</strong> <span id="modal-literal"><?=numeroLiteral ($suma_total)?></span> CON <span id="modal-decimal"></span>/100 BOLIVIANOS</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer no-print" style="text-align:center">
                        <h2>Motivo de rechazo</h2>
                        {!! Form::open(['action' => ['SolicitudController@guardarAutorizacion', 'id_sol='.$solicitud->id]]) !!}
                            {!! Form::textarea('observaciones', null, ['style' => 'width:100%; margin-bottom: 15px;', 'rows' => '3', 'required' => 'required']) !!}
                            {!! Form::hidden('id_sol', $solicitud->id, array('class' => 'form-control', 'readonly' => 'true')) !!}
                            {!! Form::submit('RECHAZAR', ['class' => 'btn btn-success', 'id' => 'solicitud'.($contador-1)]) !!}
                            <a href="/solicitud-autorizar" class="btn btn-default">VOLVER</a>
                        {!! Form::close() !!}
                        <span class="pull-right" id="print-span"></span>
                    </div>
                </div>
            </div>
            <script>
                //document.getElementById('modal-literal').innerHTML = str_literal.toUpperCase();
                //document.getElementById('modal-decimal').innerHTML = str_decimal;
                function descarga_docs(e){
                    for (i = 0; i < 5000; i++) {
                        if(document.getElementById('doc_table_'+i)){
                            document.getElementById('doc_table_'+i).style.display = "none";
                        }
                    }
                    document.getElementById('doc_table_'+e.id).style.display = "inline-table";
                    var btn_modal_des = document.getElementById('btn-modal-descarga');
                    btn_modal_des.click();
                }
                
                //Conversion Literal
                function mod(dividendo, divisor)
                {
                    resDiv = dividendo / divisor;
                    parteEnt = Math.floor(resDiv);              // Obtiene la parte Entera de resDiv
                    parteFrac = resDiv - parteEnt;              // Obtiene la parte Fraccionaria de la divisi�n
                    modulo = Math.round(parteFrac * divisor);   // Regresa la parte fraccionaria * la divisi�n (modulo)
                    return modulo;
                }
                function ObtenerParteEntDiv(dividendo, divisor)
                {
                    resDiv = dividendo / divisor;
                    parteEntDiv = Math.floor(resDiv);
                    return parteEntDiv;
                }
                function fraction_part(dividendo, divisor)
                {
                    resDiv = dividendo / divisor;
                    f_part = Math.floor(resDiv);
                    return f_part;
                }
                function string_literal_conversion(number)
                {
                    centenas = ObtenerParteEntDiv(number, 100);

                    number = mod(number, 100);

                    decenas = ObtenerParteEntDiv(number, 10);
                    number = mod(number, 10);

                    unidades = ObtenerParteEntDiv(number, 1);
                    number = mod(number, 1);
                    string_hundreds = "";
                    string_tens = "";
                    string_units = "";

                    if (centenas == 1) {
                        string_hundreds = "ciento ";
                    }

                    if (centenas == 2) {
                        string_hundreds = "doscientos ";
                    }

                    if (centenas == 3) {
                        string_hundreds = "trescientos ";
                    }

                    if (centenas == 4) {
                        string_hundreds = "cuatrocientos ";
                    }

                    if (centenas == 5) {
                        string_hundreds = "quinientos ";
                    }

                    if (centenas == 6) {
                        string_hundreds = "seiscientos ";
                    }

                    if (centenas == 7) {
                        string_hundreds = "setecientos ";
                    }

                    if (centenas == 8) {
                        string_hundreds = "ochocientos ";
                    }

                    if (centenas == 9) {
                        string_hundreds = "novecientos ";
                    }

                    if (decenas == 1) {
                        if (unidades == 1) {
                            string_tens = "once";
                        }

                        if (unidades == 2) {
                            string_tens = "doce";
                        }

                        if (unidades == 3) {
                            string_tens = "trece";
                        }

                        if (unidades == 4) {
                            string_tens = "catorce";
                        }

                        if (unidades == 5) {
                            string_tens = "quince";
                        }

                        if (unidades == 6) {
                            string_tens = "dieciseis";
                        }

                        if (unidades == 7) {
                            string_tens = "diecisiete";
                        }

                        if (unidades == 8) {
                            string_tens = "dieciocho";
                        }

                        if (unidades == 9) {
                            string_tens = "diecinueve";
                        }
                    }

                    if (decenas == 2) {
                        string_tens = "veinti";
                    }

                    if (decenas == 3) {
                        string_tens = "treinta";
                    }

                    if (decenas == 4) {
                        string_tens = "cuarenta";
                    }

                    if (decenas == 5) {
                        string_tens = "cincuenta";
                    }

                    if (decenas == 6) {
                        string_tens = "sesenta";
                    }

                    if (decenas == 7) {
                        string_tens = "setenta";
                    }

                    if (decenas == 8) {
                        string_tens = "ochenta";
                    }

                    if (decenas == 9) {
                        string_tens = "noventa";
                    }

                    if (decenas == 1){
                        string_units = "";
                    }else{
                        if (unidades == 1) {
                            string_units = "un";
                        }
                        if (unidades == 2) {
                            string_units = "dos";
                        }
                        if (unidades == 3) {
                            string_units = "tres";
                        }
                        if (unidades == 4) {
                            string_units = "cuatro";
                        }
                        if (unidades == 5) {
                            string_units = "cinco";
                        }
                        if (unidades == 6) {
                            string_units = "seis";
                        }
                        if (unidades == 7) {
                            string_units = "siete";
                        }
                        if (unidades == 8) {
                            string_units = "ocho";
                        }
                        if (unidades == 9) {
                            string_units = "nueve";
                        }
                    }
                    if (centenas == 1 && decenas == 0 && unidades == 0) {
                        string_hundreds = "cien ";
                    }
                    if (decenas == 1 && unidades == 0) {
                        string_tens = "diez ";
                    }
                    if (decenas == 2 && unidades == 0) {
                        string_tens = "veinte ";
                    }
                    if (decenas >= 3 && unidades >= 1) {
                        string_tens = string_tens + " y ";
                    }
                    final_string = string_hundreds + string_tens + string_units;
                    return final_string;
                }

                function covertirNumLetras(number){
                    number1 = number;
                    cent = number1.split('.');
                    centavos = cent[1];
                    numerparchado = cent[0];
                    if (centavos == 0 || centavos == undefined) {
                        centavos = "00";
                    }

                    if (number == 0 || number == "") {
                        centenas_final_string = " cero ";
                    } else {

                        millions = ObtenerParteEntDiv(number, 1000000);
                        number = mod(numerparchado, 1000000);

                        if (millions != 0) {
                            if (millions == 1) {
                                descriptor = " millon ";
                            } else {
                                descriptor = " millones ";
                            }
                        } else {
                            descriptor = " ";
                        }
                        millions_final_string = string_literal_conversion(millions) + descriptor;

                        thousands = ObtenerParteEntDiv(number, 1000);
                        number = mod(number, 1000);
                        if (thousands != 1) {
                            thousands_final_string = string_literal_conversion(thousands) + " mil ";
                        }
                        if (thousands == 1) {
                            thousands_final_string = " mil ";
                        }
                        if (thousands < 1) {
                            thousands_final_string = " ";
                        }
                        centenas = number;
                        centenas_final_string = string_literal_conversion(centenas);

                    }

                    cad = millions_final_string + thousands_final_string + centenas_final_string;

                    /* Convierte la cadena a May�sculas*/
                    cad = cad.toUpperCase();

                    if (centavos.length > 2){
                        if (centavos.substring(2, 3) >= 5) {
                            centavos = centavos.substring(0, 1) + (parseInt(centavos.substring(1, 2)) + 1).toString();
                        } else {
                            centavos = centavos.substring(0, 2);
                        }
                    }

                    /* Concatena a los centavos la cadena "/100" */
                    if (centavos.length == 1){
                        centavos = centavos + "0";
                    }
                    centavos = centavos + "/100";

                    /* Asigna el tipo de moneda, para 1 = PESO, para distinto de 1 = PESOS*/
                    if (number == 1){
                        moneda = " BOLIVIANO ";
                    } else {
                        moneda = " BOLIVIANOS ";
                    }
                    /* Regresa el n�mero en cadena entre par�ntesis y con tipo de moneda y la fase M.N.*/
                    //resultado = ( cad+moneda+centavos+" Pesos MX ");
                    resultado = (cad + " " + cent[1] + "/100");
                    return resultado;
                }
            </script>
        </div>
    </div>
</div>
@stop