@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<?php
use App\Proyectos;
$user = auth()->user();

$id_proys = array();
$str_proys = '';
if(sizeof($proyectos) == 0){
    echo "<script>window.location.href = '/solicitud';</script>";
    exit();
}
$ids_proys = array();
foreach ($proyectos as $proye) {
    $ids_proys[] = $proye['id'];
    $str_proys .= $proye['id'];
    if ($proye !== end($proyectos)) {
        $str_proys .= ',';
    }
}
if (substr($str_proys, -1) == ',') {
    $str_proys = substr($str_proys, 0, strlen($str_proys) - 1);
}
$consulta_proy = '';
if(!isset($ids_proys)){
//    echo "<script>window.location.href = '/solicitud';</script>";
//    exit();
    //Mostrar redireccionar y mostrar mensaje
}
$resp_proy = $proyectos;
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Guardar -->
                {!! Form::model($solicitud, array('route' => ['solicitud.update', $solicitud->id], 'method'=>'PATCH', 'class'=>'form-horizontal', 'files' => true)) !!}
                <!-- Enviar a Controller -->
                <div class="panel panel-default no-print">
                    <div class="panel-heading"><h2>Modificar Información Solicitud #{{ $solicitud->numero }} (Controller)</h2></div>
                    <div class="panel-body">
                        @include('solicitudes.partial.form')
                    </div>
                </div>
                <div class="panel panel-default no-print">
                    <div class="panel-heading">
                        <h2>Items Para Solicitud</h2>
                        <span style="color:green; font-weight: bold;"></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover table-bordered" id="solicitud-items">
                            <thead>
                                <tr>
                                    <th style="width: 3%">#</th>
                                    <th style="width: 7%">Cantidad</th>
                                    <th style="width: 10%">Unidad</th>
                                    <th>Tipo de Compra</th>
                                    <th style="width: 55%">Detalle</th>
                                    <th style="width: 10%">P. Unitario</th>
                                    <th style="width: 10%">Sub-Total</th>
                                    <th style="width: 5%">Opción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($solicitud->items as $item)
                                <tr>
                                    <input type="hidden" value="{{ $loop->iteration-1 }}" name="counter">
                                    <input type="hidden" value="true" name="controller">
                                    <td>
                                        <spam style="text-align:center;">{{ $loop->iteration }}</spam>
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="number" min="0" id="cantidad_{{ $loop->iteration-1 }}" value="{{ $item->cantidad }}" class="form-control" step="0.00001" size="3" maxlength="12" name="cantidad[]" readonly="readonly" />
                                    </td>
                                    <td>
                                        {!! Form::select('unidad[]', $unidades, $item->unidad->id, ['id' => 'unidad_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                    </td>
                                    <td>
                                        {!! Form::select('tipo_compra[]', $tipocompra, $item->tipo_compra->descripcion, ['id' => 'tipo_compra_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" size="40" maxlength="70" id="detalle_{{ $loop->iteration-1 }}" name="detalle[]" style="text-transform:uppercase;" value="{{ $item->detalle }}">
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="number" min="0" id="punitario_{{ $loop->iteration-1 }}" class="form-control" step="0.01" size="6" maxlength="10" name="punitario[]" style="text-transform:uppercase; text-align: right" onchange="subtotal_precio(this.id)" onkeyup="subtotal_precio(this.id)" value="{{ number_format($item->costo, '2', '.', '') }}">
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" class="form-control" size="6" maxlength="10" name="subtotal[]" style="text-transform:uppercase; text-align: right" id="subtotal_{{ $loop->iteration-1 }}" readonly="true" onchange="total_precio()" value="{{ number_format(($item->costo * $item->cantidad), '2', '.', '') }}">
                                    </td>
                                    <td style="text-align: center;">
                                        <div class="botonelimianr" onclick="borrarFila(this)">
                                            <i class="fa fa-times-circle-o icono-opciones icono-red" value='Quitar'></i>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <table class="table table-hover" id="solicitud-items-total" style="margin-bottom: 20px;">
                            <tbody>
                                <tr style="border: 0;">
                                    <td colspan="3" style="width: 75%;text-align: center;"><a id="btn-creafila1" class="btn btn-primary" onclick="creaFila()"><i class="fa fa-plus-circle icono-opciones"></i> AGREGAR NUEVO ITEM</a></td>
                                    <td style="width: 10%; text-align: right; padding-top: 15px;"><strong>TOTAL: </strong></td>
                                    <td style="width: 10%;"><a class="btn btn-info" id="total" style="width: 100%; margin: 0px; text-align:right;">0</a></td>
                                    <td colspan="2" style="text-align:left; width: 5%; padding-top: 15px;"><strong> Bs.</strong></td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('referencia') ? ' has-error' : '' }}">
                                <label for="referencia" class="col-md-2 control-label">REFERENCIA </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('referencia', null, ['placeholder' => 'referencia','class' => 'form-control', 'id' => 'referencia', 'style' => 'text-transform:uppercase', 'rows' => 4, 'readonly' => ($controller)?'true':'false']) !!}

                                    @if ($errors->has('referencia'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('referencia') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('observacion') ? ' has-error' : '' }}">
                                <label for="observacion" class="col-md-2 control-label">JUSTIFICACIÓN </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('observacion', null, ['placeholder' => 'justificación','class' => 'form-control', 'id' => 'observacion', 'style' => 'text-transform:uppercase', 'rows' => 4, 'readonly' => ($controller)?'true':'false']) !!}

                                    @if ($errors->has('observacion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('observacion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                {!! Form::hidden('imprimir', '0', array('id' => 'imprimir')) !!}
                                {!! Form::submit('GUARDAR CAMBIOS OCULTO', ['class' => 'btn btn-success btn-block', 'id' => 'btn-registro-solicitud', 'style' => 'display:none']) !!}
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#favoritesModal" onclick="datosModal()">
                                    GUARDAR CAMBIOS
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <!-- MODAL -->
                <div class="modal fade" id="favoritesModal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
                    <div class="modal-dialog modal-lg " role="document">
                        <div class="modal-content">
                            <div class="modal-header no-print">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title text-center text-success" id="favoritesModalLabel"><strong>POR FAVOR VERIFIQUE LOS DATOS PARA REALIZAR LA SOLICITUD</strong></h4>
                            </div>
                            <div class="modal-body">
                                <h1 class="text-center" id="modal-empresa"></h1>
                                <h2 class="text-center">SOLICITUD DE FONDOS</h2>
                                <div class="panel panel-default ">
                                    <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                    <div class="panel-body">
                                        <table class="table table-bordered" id="reporte-solicitud">
                                            <tr>
                                                <td><label for="pago">Proyecto: </label></td>
                                                <td colspan="3" id='modal-proyecto'></td>
                                                <td><label for="turno">Fecha Limite: </label></td>
                                                <td id="modal-fecha-lim"></td>
                                            </tr>
                                            <tr>
                                                <td><label for="ruta">Solicitante: </label></td>
                                                <td colspan="5" id="modal-solicitante"></td>
                                            </tr>
                                            <tr>
                                                <td><label for="grupo">Desembolso A: </label></td>
                                                <td colspan="5" id='modal-desembolso'></td>
                                            </tr>
                                            <tr>
                                                <td><label for="grupo">Tipo de Cambio: </label></td>
                                                <td id='modal-cambio'></td>
                                                <td><label for="grupo">Modalidad: </label></td>
                                                <td id='modal-modalidad'></td>
                                                <td><label for="grupo">Moneda: </label></td>
                                                <td id='modal-moneda'></td>
                                            </tr>
                                            <tr>
                                                <td><label for="grupo">Referencia: </label></td>
                                                <td colspan="5" id='modal-referencia'></td>
                                            </tr>
                                            <tr>
                                                <td><label for="grupo">Justificación: </label></td>
                                                <td colspan="5" id='modal-observacion'></td>
                                            </tr>
                                            <tr class="no-print">
                                                <td class="text-right"><strong>Documento(s)<br>Adjunto(s)</strong></td>
                                                <td colspan="9" id='modal-documentos'></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel panel-default ">
                                    <div class="panel-heading"><h2>ITEMS SOLICITUD</h2></div>
                                    <div class="panel-body panel-items">
                                        <table class="table table-hover table-bordered tbl-modal-items">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Cantidad</th>
                                                    <th>Unidad</th>
                                                    <th>Detalle</th>
                                                    <th>P. Unitario</th>
                                                    <th>Sub-Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'></tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal"></span> CON 00/100 BOLIVIANOS</p>
                                    </div>
                                </div>
                                <div class="panel panel-default panel-solicitante" style="display:none">
                                    <div class="panel-heading"><h2>CONTACTO SOLICITANTE</h2></div>
                                    <div class="panel-body">
                                        <table class="table" id="table-contacto">
                                            <tr>
                                                <td>
                                                    <strong>Nombre: </strong><span id="modal-contacto"></span><br>
                                                    <strong>Telefono: </strong><span id="modal-telefono"></span><br>
                                                    <strong>Fecha y hora de Solicitud: </strong><span id="modal-fecha"></span> - <span id="modal-hora"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer no-print">
<!--                                <div class="text-center">
                                    <input type="checkbox" name="correo" id="correo" value="correo" checked="checked">&nbsp;&nbsp;<label for='correo'>¿Enviar Solicitud a Correo?</label>
                                </div>-->
                                <span class="pull-right">
                                    <button type="button" class="btn btn-primary" onclick="document.getElementById('btn-registro-solicitud').click()">GUARDAR CAMBIOS SOLICITUD</button>
                                </span>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->
            </div>
        </div>
    </div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    //Subimos primer archivo
    total_precio();
    function eliFila(){
        borrarFila(this);
        total_precio();
    }
    function cambio_input(e){
        if(e.id == 'doc1'){
            var fullPath;
            document.getElementById('moreUploadsLink').style.display = 'inline-block';
            fullPath = document.getElementById(e.id).value;
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            document.getElementById('elegir-archivo').style.display = 'none';
            var primer_archivo = document.getElementById('primer-archivo');
            primer_archivo.innerHTML = '<span style="font-size:14px; font-weight: bold;">'+filename+'</span>'+' <i class="fa fa-times-circle-o icono-opciones icono-red" onclick="borrarArchivo(this)" id="eli1"></i>';
        }else{
            fullPath = document.getElementById(e.id).value;
            if(fullPath){
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            var doc_tam = (e.id).length;
            var doc_val = (e.id).substr(3, doc_tam);
            document.getElementById('archivo'+doc_val).style.display = 'none';
            var archivos = document.getElementById('docnro'+doc_val);
            //archivos.innerHTML = filename;
            var a_tag = document.createElement('a');
            a_tag.setAttribute("class", "fa fa-times-circle-o icono-opciones icono-red");
            a_tag.setAttribute("onclick","borrarArchivo(this)");
            a_tag.setAttribute("id", "eli"+doc_val);
            var span_tag = document.createElement('span');
            span_tag.setAttribute("style", "font-size:14px; font-weight: bold;");
            span_tag.innerHTML = filename+" ";
            span_tag.appendChild(a_tag);
            archivos.appendChild(span_tag);
        }
    }

    //Añadir archivos
    var upload_number = 2;
    function addFileInput() {
        var d = document.createElement("div");
        d.setAttribute('id', 'docnro'+upload_number);
        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "doc" + upload_number);
        file.setAttribute("id", "doc" + upload_number);
        file.setAttribute("onchange", "cambio_input(this)");
        file.setAttribute("style", "display:none");
        d.appendChild(file);

        var file_add = document.createElement('div');
        file_add.setAttribute('id', 'archivo' + upload_number);
        file_add.setAttribute('value', upload_number);
        file_add.innerHTML = "<i class='fa fa-upload icono-opciones icono-blue'></i> ";
        file_add.setAttribute('onclick', 'elegirArchivo(this)');
        d.appendChild(file_add);
        document.getElementById("moreUploads").appendChild(d);
        upload_number++;
    }
    //Eliminar Archivo
    function borrarArchivo(eli) {
        var doc_tam = (eli.id).length;
        var doc_val = (eli.id).substring(3, doc_tam);

        //***HACER
        if(doc_val == 1){
            var element = document.getElementById("primer-archivo");
        }else{
            var element = document.getElementById("docnro"+doc_val);
        }
        element.parentNode.removeChild(element);
    }

    //Fila Archivos
    adjuntarArchivo = document.getElementById('adjuntar-archivo');
    var cont_archivos = 0;
    function elegirArchivo(el){
        var btn_elegir_imagen;
        if(el.id == 'elegir-archivo'){
            btn_elegir_imagen = document.getElementById('doc1');
            btn_elegir_imagen.click();
        }else{
            var tam = (el.id).length;
            var valor = (el.id).substring(7, tam);
            btn_elegir_imagen = document.getElementById('doc'+valor);
            btn_elegir_imagen.click();
        }
    }
    //Filas Tabla Items
    var contador = {{ count($solicitud->items) }};
    function creaFila() {
        var tabla = document.getElementById("solicitud-items").tBodies[0];
        var fila = document.createElement("TR");
        fila.setAttribute("align", "center");

        var counter = document.createElement("INPUT");
        counter.setAttribute("type", "hidden");
        counter.setAttribute("value", contador);
        counter.setAttribute("name", "counter");
        fila.appendChild(counter);

        //CELDA NUMERO
        var celda1 = document.createElement("TD");
        var numero = document.createElement("spam");
        numero.setAttribute("style", "text-align:center;");
        var indice = contador + 1;
        var numero1 = document.createTextNode(indice);

        numero.appendChild(numero1);
        celda1.appendChild(numero);

        //CELDA CANTIDAD
        var celda2 = document.createElement("TD");
        var cantidad = document.createElement("INPUT");
        cantidad.setAttribute("type", "number");
        cantidad.setAttribute("min", "0");
        cantidad.setAttribute("id", "cantidad_" + contador);
        cantidad.setAttribute("class", "form-control");
        cantidad.setAttribute("step", "0.01");
        cantidad.setAttribute("size", "3");//4
        cantidad.setAttribute("maxlength", "12");
        cantidad.setAttribute("name", "cantidad[]");
        cantidad.setAttribute("onchange", "subtotal_cantidad(this.id)");
        cantidad.setAttribute("onkeyup", "subtotal_cantidad(this.id)");
        celda2.appendChild(cantidad);


        //CELDA UNIDAD
        var celda3 = document.createElement("TD");
        var unidad = document.createElement("SELECT");
        unidad.setAttribute("size", "1");
        unidad.setAttribute("class", "form-control");
        unidad.setAttribute("id", "unidad_" + contador);
        unidad.setAttribute("name", "unidad[]");
        <?php
        $unidades = App\Unidades::all();
        foreach ($unidades as $unidad) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $unidad->descripcion . "';";
            echo "opcioncur.value = '" . $unidad->id . "';";
            echo "unidad.appendChild(opcioncur);";
        }
        ?>
        celda3.appendChild(unidad);

        //CELDA DETALLE
        var celda5 = document.createElement("TD");
        var detalle = document.createElement("INPUT");
        detalle.setAttribute("type", "text");
        detalle.setAttribute("class", "form-control");
        detalle.setAttribute("size", "40"); //22
        detalle.setAttribute("maxlength", "70");
        detalle.setAttribute("id", "detalle_" + contador);
        detalle.setAttribute("name", "detalle[]");
        detalle.setAttribute("style", "text-transform:uppercase;");
        celda5.appendChild(detalle);

        //CELDA P-UNITARIO
        var celda6 = document.createElement("TD");
        var punitario = document.createElement("INPUT");
        punitario.setAttribute("type", "number");
        punitario.setAttribute("min", "0");
        punitario.setAttribute("id", "punitario_" + contador);
        punitario.setAttribute("class", "form-control");
        punitario.setAttribute("step", "0.01");
        punitario.setAttribute("size", "6");
        punitario.setAttribute("maxlength", "10");
        punitario.setAttribute("name", "punitario[]");
        punitario.setAttribute("style", "text-transform:uppercase; text-align: right");
        punitario.setAttribute("onchange", "subtotal_precio(this.id)");
        punitario.setAttribute("onkeyup", "subtotal_precio(this.id)");
        celda6.appendChild(punitario);

        //CELDA SUB-TOTAL
        var celda7 = document.createElement("TD");
        var subtotal = document.createElement("INPUT");
        subtotal.setAttribute("type", "text");
        subtotal.setAttribute("class", "form-control");
        subtotal.setAttribute("size", "6");
        subtotal.setAttribute("maxlength", "10");
        subtotal.setAttribute("name", "subtotal[]");
        subtotal.setAttribute("style", "text-transform:uppercase; text-align: right");
        subtotal.setAttribute("id", "subtotal_" + contador);
        subtotal.setAttribute("ReadOnly", true);
        subtotal.setAttribute("onchange", "total_precio()");
        celda7.appendChild(subtotal);

        //BOTON ELIMINAR FILA
        var celda8 = document.createElement('TD');
        var tipoboton = document.createElement('div');
        tipoboton.setAttribute('class', 'botoneliminar');
        var eliminar = document.createElement('i');
        eliminar.setAttribute('class', 'fa fa-times-circle-o icono-opciones icono-red');
        eliminar.setAttribute('value', 'Quitar');
        tipoboton.appendChild(eliminar);
        tipoboton.onclick = function () {
            borrarFila(this);
        }
        celda8.appendChild(tipoboton);

        fila.appendChild(celda1);
        fila.appendChild(celda2);
        fila.appendChild(celda3);
        fila.appendChild(celda5);
        fila.appendChild(celda6);
        fila.appendChild(celda7);
        fila.appendChild(celda8);

        tabla.appendChild(fila);

        contador = contador + 1;
    }

    function mayuscula_detalle(id) {
        var detalle_str = document.getElementById(id);
        var detalle = detalle_str.value;
        detalle = detalle.toUpperCase();
        detalle_str.value = detalle;
    }

    function subtotal_cantidad(id) {
        var res_array = id.split('_');
        var sub;
        var cant;
        var precio = document.getElementById('punitario_' + res_array[1]);
        if (precio.value != '') {
            sub = document.getElementById('subtotal_' + res_array[1]);
            cant = document.getElementById('cantidad_' + res_array[1]);
            sub.value = (cant.value) * (precio.value);
            total_precio();
        }
    }

    function subtotal_precio(id) {
        var res_array = id.split('_');
        var sub;
        var cant;
        var precio = document.getElementById('cantidad_' + res_array[1]);
        if (precio.value != '') {
            sub = document.getElementById('subtotal_' + res_array[1]);
            cant = document.getElementById('punitario_' + res_array[1]);

            sub.value = ((cant.value) * (precio.value)).toFixed(2);
            total_precio();
        }
    }

    function total_precio() {
        var total;
        total = 0;
        var element;
        //***REVISAR CONTADOR
        for (i = 0; i < 50; i++) {
            element = (document.getElementById('subtotal_' + i));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                total += parseFloat(element.value);
            }
        }
        total = total.toFixed(2);
        document.getElementById("total").innerHTML = total;
    }

    function borrarFila(button) {
        var fila = button.parentNode.parentNode;
        var tabla = document.getElementById('solicitud-items').getElementsByTagName('tbody')[0];
        tabla.removeChild(fila);
        total_precio();
    }
    function datosModal() {
        //Nombre empresa
        var mod_empresa = '{{ $solicitud->proyecto->empresa->nombre }}';

        //Solicitante
        var mod_solicitante = '<div class="text-danger"><strong>NOMBRE DE USUARIO</strong></div>';
        if (document.getElementById('usuario').value) {
            mod_solicitante = document.getElementById('usuario').value;
        }

        //Proyecto
        var mod_proyecto = '<div class="text-danger"><strong>SIN PROYECTO</strong></div>';
        var ele = document.getElementById('proyecto');
        mod_proyecto = ele.options[ele.selectedIndex].innerHTML;


        //Fecha Limite
        var mod_fecha_lim = '<div class="text-danger"><strong>SIN FECHA LIMITE</strong></div>';
        if (document.getElementById('fecha_lim').value) {
            mod_fecha_lim = document.getElementById('fecha_lim').value;
        }

        //Modalidad
        var rad_modalidad = document.getElementsByName('modalidad');
        var mod_modalidad;
        var rate_value;
        for (var i = 0; i < rad_modalidad.length; i++) {
            if (rad_modalidad[i].checked) {
                rate_value = rad_modalidad[i].value;
            }
        }
        switch (rate_value) {
            case '1':
                mod_modalidad = 'Sujeto a Rendición';
                break;

            case '2':
                mod_modalidad = 'Gasto Directo';
                break;

            default:
                mod_modalidad = '<div class="text-danger"><strong>SIN MODALIDAD</strong></div>';
        }

        //Tipo de cambio
        var mod_tipo_cambio = {{$cambio['cambio']}};

        //Moneda
        var rad_moneda = document.getElementsByName('moneda');
        var mod_moneda;
        for (var i = 0; i < rad_moneda.length; i++) {
            if (rad_moneda[i].checked) {
                var selector = 'label[for=' + rad_moneda[i].id + ']';
                var label = document.querySelector(selector);
                mod_moneda = label.innerHTML;
            }
        }

        //Desembolso
        var mod_desembolso = '<div class="text-danger"><strong>SIN DESEMBOLSO</strong></div>';
        if (document.getElementById('desembolso').value) {
            mod_desembolso = (document.getElementById('desembolso').value).toUpperCase();
        }

        //Referencia
        var mod_referencia = '<div class="text-danger"><strong>SIN REFERENCIA</strong></div>';
        if (document.getElementById('referencia').value) {
            mod_referencia = (document.getElementById('referencia').value).toUpperCase();
        }

        //Observación
        var mod_observacion = '<div class="text-danger"><strong>SIN JUSTIFICACIÓN</strong></div>';
        if (document.getElementById('observacion').value) {
            mod_observacion = (document.getElementById('observacion').value).toUpperCase();
        }

        //Documentos
        var mod_documento = '<div class="text-danger"><strong>SIN DOCUMENTO</strong></div>';
        var str_docs = '';
        var aux_docs;
        for (i = 1; i < 30; i++) {
            element = (document.getElementById('doc' + i));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                aux_docs = document.getElementById('doc' + i).value;
                if (aux_docs) {
                    var startIndex = (aux_docs.indexOf('\\') >= 0 ? aux_docs.lastIndexOf('\\') : aux_docs.lastIndexOf('/'));
                    var filename = aux_docs.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    str_docs += filename + "<br>";
                }
            }
        }
        if (str_docs.length > 0) {
            mod_documento = str_docs;
        }

        //Tabla Items
        var total;
        total = 0;
        //***REVISAR CONTADOR
        var element;
        var str_items = "";
        var cont_items = 1;
        var td_can;
        var td_uni;
        var td_det;
        var td_pre;
        var td_sub;
        var td_total = (document.getElementById('total')).innerHTML;
        for (i = 0; i < 30; i++) {
            element = (document.getElementById('subtotal_' + i));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                td_can = (document.getElementById('cantidad_' + i)).value;
                var ele = document.getElementById('unidad_' + i);
                td_uni = ele.options[ele.selectedIndex].innerHTML;
                td_det = (document.getElementById('detalle_' + i)).value;
                td_pre = (document.getElementById('punitario_' + i)).value;
                td_sub = (document.getElementById('subtotal_' + i)).value;
                str_items += "<tr><td class='text-right'>" + (cont_items) + "</td><td class='text-right'>" + td_can + "</td><td>" + td_uni + "</td><td>" + td_det + "</td><td class='text-right'>" + td_pre + "</td><td class='text-right'>" + td_sub + "</td></tr>";
                cont_items++;
            }
        }
        if (str_items == '') {
            str_items = "<tr><td colspan='6'><div class='text-danger text-center'><strong>SIN ITEMS</strong></div></td></tr>";
        } else {
            str_items += "<tr><td colspan='5'><div class='text-right'><strong>TOTAL: </strong></div></td><td class='text-right'>" + td_total + "</td></tr>";
        }
        var mod_literal = (string_literal_conversion(td_total)).toUpperCase();

        document.getElementById('modal-solicitante').innerHTML = mod_solicitante;
        document.getElementById('modal-empresa').innerHTML = mod_empresa;
        document.getElementById('modal-contacto').innerHTML = mod_solicitante;
        document.getElementById('modal-proyecto').innerHTML = mod_proyecto;
        document.getElementById('modal-fecha-lim').innerHTML = mod_fecha_lim;

        document.getElementById('modal-modalidad').innerHTML = mod_modalidad;
        document.getElementById('modal-cambio').innerHTML = mod_tipo_cambio;
        document.getElementById('modal-moneda').innerHTML = mod_moneda;
        document.getElementById('modal-desembolso').innerHTML = mod_desembolso;
        document.getElementById('modal-referencia').innerHTML = mod_referencia;

        document.getElementById('modal-observacion').innerHTML = mod_observacion;
        document.getElementById('modal-documentos').innerHTML = mod_documento;
        document.getElementById('modal-items').innerHTML = str_items;
        document.getElementById('modal-literal').innerHTML = mod_literal;
    }
    //Conversion Literal
    function mod(dividendo, divisor)
    {
        resDiv = dividendo / divisor;
        parteEnt = Math.floor(resDiv);            // Obtiene la parte Entera de resDiv
        parteFrac = resDiv - parteEnt;      // Obtiene la parte Fraccionaria de la divisi�n
        modulo = Math.round(parteFrac * divisor);  // Regresa la parte fraccionaria * la divisi�n (modulo)
        return modulo;
    }
    function ObtenerParteEntDiv(dividendo, divisor)
    {
        resDiv = dividendo / divisor;
        parteEntDiv = Math.floor(resDiv);
        return parteEntDiv;
    }
    function fraction_part(dividendo, divisor)
    {
        resDiv = dividendo / divisor;
        f_part = Math.floor(resDiv);
        return f_part;
    }
    function string_literal_conversion(number)
    {
        centenas = ObtenerParteEntDiv(number, 100);

        number = mod(number, 100);

        decenas = ObtenerParteEntDiv(number, 10);
        number = mod(number, 10);

        unidades = ObtenerParteEntDiv(number, 1);
        number = mod(number, 1);
        string_hundreds = "";
        string_tens = "";
        string_units = "";

        if (centenas == 1) {
            string_hundreds = "ciento ";
        }

        if (centenas == 2) {
            string_hundreds = "doscientos ";
        }

        if (centenas == 3) {
            string_hundreds = "trescientos ";
        }

        if (centenas == 4) {
            string_hundreds = "cuatrocientos ";
        }

        if (centenas == 5) {
            string_hundreds = "quinientos ";
        }

        if (centenas == 6) {
            string_hundreds = "seiscientos ";
        }

        if (centenas == 7) {
            string_hundreds = "setecientos ";
        }

        if (centenas == 8) {
            string_hundreds = "ochocientos ";
        }

        if (centenas == 9) {
            string_hundreds = "novecientos ";
        }

        if (decenas == 1) {
            if (unidades == 1) {
                string_tens = "once";
            }

            if (unidades == 2) {
                string_tens = "doce";
            }

            if (unidades == 3) {
                string_tens = "trece";
            }

            if (unidades == 4) {
                string_tens = "catorce";
            }

            if (unidades == 5) {
                string_tens = "quince";
            }

            if (unidades == 6) {
                string_tens = "dieciseis";
            }

            if (unidades == 7) {
                string_tens = "diecisiete";
            }

            if (unidades == 8) {
                string_tens = "dieciocho";
            }

            if (unidades == 9) {
                string_tens = "diecinueve";
            }
        }

        if (decenas == 2) {
            string_tens = "veinti";
        }

        if (decenas == 3) {
            string_tens = "treinta";
        }

        if (decenas == 4) {
            string_tens = "cuarenta";
        }

        if (decenas == 5) {
            string_tens = "cincuenta";
        }

        if (decenas == 6) {
            string_tens = "sesenta";
        }

        if (decenas == 7) {
            string_tens = "setenta";
        }

        if (decenas == 8) {
            string_tens = "ochenta";
        }

        if (decenas == 9) {
            string_tens = "noventa";
        }

        if (decenas == 1){
            string_units = "";
        }else{
            if (unidades == 1) {
                string_units = "un";
            }
            if (unidades == 2) {
                string_units = "dos";
            }
            if (unidades == 3) {
                string_units = "tres";
            }
            if (unidades == 4) {
                string_units = "cuatro";
            }
            if (unidades == 5) {
                string_units = "cinco";
            }
            if (unidades == 6) {
                string_units = "seis";
            }
            if (unidades == 7) {
                string_units = "siete";
            }
            if (unidades == 8) {
                string_units = "ocho";
            }
            if (unidades == 9) {
                string_units = "nueve";
            }
        }
        if (centenas == 1 && decenas == 0 && unidades == 0) {
            string_hundreds = "cien ";
        }
        if (decenas == 1 && unidades == 0) {
            string_tens = "diez ";
        }
        if (decenas == 2 && unidades == 0) {
            string_tens = "veinte ";
        }
        if (decenas >= 3 && unidades >= 1) {
            string_tens = string_tens + " y ";
        }
        final_string = string_hundreds + string_tens + string_units;
        return final_string;
    }

    function covertirNumLetras(number){
        number1 = number;
        cent = number1.split('.');
        centavos = cent[1];
        numerparchado = cent[0];
        if (centavos == 0 || centavos == undefined) {
            centavos = "00";
        }

        if (number == 0 || number == "") {
            centenas_final_string = " cero ";
        } else {

            millions = ObtenerParteEntDiv(number, 1000000);
            number = mod(numerparchado, 1000000);

            if (millions != 0) {
                if (millions == 1) {
                    descriptor = " millon ";
                } else {
                    descriptor = " millones ";
                }
            } else {
                descriptor = " ";
            }
            millions_final_string = string_literal_conversion(millions) + descriptor;

            thousands = ObtenerParteEntDiv(number, 1000);
            number = mod(number, 1000);
            if (thousands != 1) {
                thousands_final_string = string_literal_conversion(thousands) + " mil ";
            }
            if (thousands == 1) {
                thousands_final_string = " mil ";
            }
            if (thousands < 1) {
                thousands_final_string = " ";
            }
            centenas = number;
            centenas_final_string = string_literal_conversion(centenas);

        }

        cad = millions_final_string + thousands_final_string + centenas_final_string;

        /* Convierte la cadena a May�sculas*/
        cad = cad.toUpperCase();

        if (centavos.length > 2){
            if (centavos.substring(2, 3) >= 5) {
                centavos = centavos.substring(0, 1) + (parseInt(centavos.substring(1, 2)) + 1).toString();
            } else {
                centavos = centavos.substring(0, 2);
            }
        }

        /* Concatena a los centavos la cadena "/100" */
        if (centavos.length == 1){
            centavos = centavos + "0";
        }
        centavos = centavos + "/100";

        /* Asigna el tipo de moneda, para 1 = PESO, para distinto de 1 = PESOS*/
        if (number == 1){
            moneda = " BOLIVIANO ";
        } else {
            moneda = " BOLIVIANOS ";
        }
        /* Regresa el n�mero en cadena entre par�ntesis y con tipo de moneda y la fase M.N.*/
        //resultado = ( cad+moneda+centavos+" Pesos MX ");
        resultado = (cad + " " + cent[1] + "/100");
        return resultado;
    }

    function guardarImprimir(){
        var imprimir_valor = document.getElementById('imprimir').value;
        document.getElementById('imprimir').value = 1;
        document.getElementById('btn-registro-solicitud').click();
    }
</script>
@endsection