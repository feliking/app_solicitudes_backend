@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-default no-print">
                <div class="panel-heading" style="background: #{{ $extras[1] }}">
                    <div class="col-md-4">
                        <h2 style='color: #{{ $extras[0] }}'>SOLICITUDES POR RENDIR</h2>
                    </div>
                    @php
                        $array_docs_adjuntos = array();
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $id_usuario = $usuario->id;
                    @endphp
                    <div class="col-md-8">
                        {!! Form::open(array('action' => 'SolicitudController@autorizar', 'method'=>'POST', 'class'=>'form-inline')) !!}
                            @include('solicitudes.partial.buscador')
                        {!! Form::close() !!}
                        @if(isset($_GET['ordenar']))
                            <p style="font-size:12px; margin-bottom: 0px; padding-bottom: 0px; text-align: center;">ORDENADO POR: {{ strtoupper($_GET['ordenar']) }} ({{ $orden_type }})</p>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                    <!-- SEARCH ENGINE -->
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-3">
                        <div class="crear_empresa"></div>
                    </div>
                    <!-- END SEARCH ENGINE -->
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered table-hover table-solicitudes">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                <th>@sortablelink('created_at', 'FECHA')</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>@sortablelink('estado', 'ESTADO')</th>
                                <th>@sortablelink('total', 'MONTO')</th>
                                <th class="proy_simple"><a class="fa fa-arrow-circle-right icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> @sortablelink('proyecto_id', 'PROYECTO')</th>
                                <th class="proy_cols"><a class="fa fa-arrow-circle-left icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> CENTRO DE COSTO</th>
                                <th class="proy_cols">GENERADOR DE COSTO</th>
                                <th class="proy_cols">ACTIVIDAD</th>
                                <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($solicitudes as $solicitud)
                            <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)">
                                <td>
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                    <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud"></a>
                                    <i class="fa fa-check-circle-o icono-opciones-files icono-green" data-toggle="modal" data-target="#rendicionModal" onclick="rendirModal({{$solicitud->id}})" title="Rendir Solicitud"></i>
                                </td>
                                <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                <td id="fecha_{{ $solicitud->id }}">
                                    {{ $solicitud->created_at }}
                                </td>
                                <td>
                                    @if(sizeof($solicitud->documentos) > 0)
                                        <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                    @endif
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                        {{ $solicitud->estadoCodigo }}
                                    </span>
                                </td>
                                <td id="total_{{ $solicitud->id }}" style="text-align: right;">{{ number_format($solicitud->total, 2, '.', ',') }}</td>
                                <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}" class="proy_simple">
                                    <i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i>
                                </td>
                                <td class="proy_cols">
                                	@php
                                		switch($solicitud->proyecto->nivel){
                                			case 1: 
                                				echo $solicitud->proyecto->nombre;
                                				break;
                                			case 2: 
                                				echo $solicitud->proyecto->padre->nombre;
                                				break;
                                			case 3: 
                                				echo $solicitud->proyecto->padre->padre->nombre;
                                				break;
                                			default:
                                		    	echo "";
                                		    	break;	
                                		}
                                	@endphp
                                </td>
                                <td class="proy_cols">
                                	@php
                                		switch($solicitud->proyecto->nivel){
                                			case 2: 
                                				echo $solicitud->proyecto->nombre;
                                				break;
                                			case 3: 
                                				echo $solicitud->proyecto->padre->nombre;
                                				break;	
                                		    default:
                                		    	echo "";
                                		    	break;
                                		}
                                	@endphp
                                </td>
                                <td class="proy_cols">{{ ($solicitud->proyecto->nivel == 3)?$solicitud->proyecto->nombre:"" }}</td>
                                <td id="usuario_{{ $solicitud->id }}">
                                    {{ $solicitud->usuario->nombre }}
                                </td>
                                <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                <td id="cambio_{{ $solicitud->id }}">{{ $solicitud->tipo_cambio->cambio }}</td>
                                <td id="tipo_{{ $solicitud->id }}">{{$solicitud->modalidad->nombre}}</td>
                                <td id="moneda_{{ $solicitud->id }}">{{$solicitud->moneda->nombre}}</td>
                                <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
    Modal solicitud
</button>
<button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
    Modal descarga
</button>
<!-- MODAL -->
@include('solicitudes.partial.datosSolicitud')
<!-- END MODAL -->
<!-- MODAL HISTORIAL SOLICITUD-->
@include('solicitudes.partial.historial')
<!-- END MODAL -->
<!-- MODAL DOWNLOAD DOCS-->
@include('solicitudes.partial.docsDown')
<!-- END MODAL -->
<!-- MODAL DOWNLOAD DOCS-->
@include('solicitudes.partial.rendicionModal')
<!-- END MODAL -->
<!-- MODAL ESTADOS SOLICITUD-->
@include('solicitudes.partial.estadosModal')
<!-- END MODAL -->

@endsection
