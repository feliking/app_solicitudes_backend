@extends('main')

@section('headerScripts')

@endsection
@section('content')
<style>
    .table-af tr th{
        background-color: #559988;
        color: #FFF;
    }

    .head_frm_af{
        background-color: #5a738e;
        margin: 0;
        padding: 10px;
        color: #FFF;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-default no-print">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>SOLICITUD #{{ $solicitud->numero }}</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>PROYECTO</td>
                                <td colspan="3">{{ ($solicitud->padre_id)?$solicitud->padre->nombre:$solicitud->proyecto->nombre }}</td>
                            </tr>
                            <tr>
                                <td>SOLICITANTE</td>
                                <td>{{ $solicitud->usuario->nombre }}</td>
                                <td>FECHA</td>
                                <td>{{ $solicitud->created_at }}</td>
                            </tr>
                            <tr>
                                <td>DESEMBOLSO A</td>
                                <td>{{ $solicitud->desembolso }}</td>
                                <td>MODALIDAD</td>
                                <td>{{ $solicitud->modalidad->nombre }}</td>
                            </tr>
                            <tr>
                                <td>MONEDA</td>
                                <td>{{ $solicitud->moneda->nombre }}</td>
                                <td>TIPO DE CAMBIO</td>
                                <td>{{ $solicitud->tipo_cambio->cambio }}</td>
                            </tr>
                            <tr>
                                <td>REFERENCIA</td>
                                <td colspan="3">{{ $solicitud->referencia }}</td>
                            </tr>
                            <tr>
                                <td>JUSTIFICACIÓN</td>
                                <td colspan="3">{{ $solicitud->observacion }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>ITEMS PARA RENDIR</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'solicitud.store', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="col-md-1">UNIDAD</th>
                                <th class="col-md-1">TIPO DE COMPRA</th>
                                <th class="col-md-5">DETALLE</th>
                                <th class="col-md-1">FECHA FACTURA</th>
                                <th class="col-md-1">FACTURA</th>
                                <th class="col-md-1">CANTIDAD</th>
                                <th class="col-md-1">PRECIO</th>
                                <th class="col-md-1">SUB TOTAL</th>
                                <th>OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody id="items_para_rendir">
                            @php
                                $total = 0;
                            @endphp
                            @foreach($items as $item)
                                <tr id="item_{{ $item->id }}">
                                    <td style="text-align: center;">{{ $loop->iteration }}</td>
                                    <td style="text-align: left;">
                                        {{ Form::select('unidad_'.$item->id, $unidades, $item->unidad->id, ['class' => 'form-control','id' => 'preunidad_'.$loop->iteration]) }}
                                    </td>
                                    <td style="text-align: left;">
                                        {{ Form::select('tipocompra_'.$item->id, $tipocompra, $item->tipo_compra->id, ['class' => 'form-control', 'id' => 'pretipocompra_'.$loop->iteration, 'onChange' => "subtotal_cantidad('pretipocompra_$loop->iteration')"]) }}
                                    </td>
                                    <td><input type="text"  class="form-control" id="predetalle_{{$loop->iteration}}" value="{{ $item->detalle }}" style="text-transform: uppercase;"/></td>
                                    <td><input type="date" class="form-control mask-fecha" id="prefechafactura_{{$loop->iteration}}" value="" max="{{ date('Y-m-d') }}" /></td>
                                    <td><input type="text" class="form-control mask-factura" id="prefactura_{{$loop->iteration}}" value=""/></td>
                                    <td style="text-align: center;">
                                        <input type="hidden" id="itemId_{{$loop->iteration}}" value="{{ $item->id }}"/>
                                        <input type="hidden" id="solicitudId_{{$loop->iteration}}" value="{{ $solicitud->id }}"/>
                                        <input type="text" id="precantidad_{{$loop->iteration}}" data-cantidad="{{ $item->id }}" class="form-control mask-cantidad" value="{{ $item->cantidad }}" onchange="subtotal_cantidad(this.id)" onkeyup="subtotal_cantidad(this.id)"/>
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="prepunitario_{{$loop->iteration}}"  value="{{ number_format($item->costo, '5', '.', '') }}" class="form-control mask-decimal" step="0.00001" onchange="subtotal_precio(this.id)" onkeyup="subtotal_precio(this.id)"/>
                                    </td>
                                    <td ><input type="text" class="form-control text-right" size="6" id="subtotal_{{$loop->iteration}}" id="subtotal_{{$loop->iteration}}" step="0.00001" readonly="true" onchange="total_precio()" value="{{ number_format($item->cantidad * $item->costo, '2', '.', '') }}" /></td>
                                    <td>
                                        <i class="fa fa-pencil-square-o icono-opciones icono-green" data-toggle="modal" data-target="#rendirItemModal" onclick="rendirItemModal({{$loop->iteration}})" title="RENDIR ITEM"></i>
                                        <i class="fa fa-close icono-opciones icono-red" data-toggle="modal" onclick="eliminarItem({{$item->id}})" title="ELIMINAR ITEM"></i>
                                    </td>
                                </tr>
                                <tr id="table_{{ $item->id }}"></tr>
                                @php
                                    $total += $item->cantidad * $item->costo;
                                @endphp
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7"><a id="btn-creafila1" class="btn btn-primary" onclick="creaFila()"><i class="fa fa-plus-circle icono-opciones"></i> AGREGAR NUEVO ITEM</a></td>
                                <td style="text-align: right; font-weight: bold;">TOTAL</td>
                                <td style="text-align: right; font-weight: bold;" id="total">{{ number_format($total, '5', '.', '') }}</td>
                            </tr>
                        </tfoot>
                    </table>
                    {!! Form::close() !!}
                    <h2>DOCUMENTO RENDICIÓN</h2>
                    {!! Form::open(array('route' => 'solicitud.documentos.rendicion.guardar', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
                    {{ Form::hidden('solicitud_id', $solicitud->id) }}
                    <div class="col-md-4">
                        <label for="doc" class="custom-file-upload" style="display: inline;">
                            DOCUMENTO(S) ADJUNTO(S)
                            <ul>
                                @foreach($docrendidos as $docrendido)
                                <li>{{ $docrendido->direccion }}</li>
                                @endforeach
                            </ul>
                            <i class="fa fa-plus-circle icono-opciones-files icono-green" id="adjuntar-archivo" onclick="addFileInput()" style="display: inline;"></i>
                        </label>
                        <div id="moreUploadsLink"></div>
                    </div>
                    <div class="col-md-8">
                        @if(isset($documentos))
                        <ul style="list-style-type: none; padding-left: 0px;">
                            @foreach($documentos as $documento)
                                <li style="font-weight: bold; font-size: 14px;" id="doc_{{ $documento->id }}" documento-nombre="{{ $documento->direccion }}">{{ $documento->direccion }} <a class="fa fa-times-circle-o icono-opciones-files icono-red" onclick="borrarDocumentoModal({{ $documento->id }})"></a></li>
                            @endforeach

                        </ul>
                        @endif
                        {{ form::file('doc1', ["class" => 'inputfile', "value" => " old('doc') ", "id"=>'doc1', 'onchange' => 'cambio_input(this)', 'style' => 'display:none']) }}
                        <input type="file" name="doc1" id="doc1" onchange="cambio_input(this)" style="display:none" />
                        <div id="moreUploads"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6" style="text-align: center;">
                        {!! Form::submit('GUARDAR DOCUMENTOS', ['class' => 'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>INFORMACIÓN RENDICIÓN</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td style=" font-weight: bold;">Rendido por</td>
                                <td>{{ $solicitud->usuario->nombre }}</td>
                                <td style=" font-weight: bold;">Fecha</td>
                                <td>{{ $solicitud->created_at }}</td>
                            </tr>
                            <tr>
                                <td style=" font-weight: bold;">Proyecto</td>
                                <td>{{ ($solicitud->padre_id)?$solicitud->padre->nombre:$solicitud->proyecto->nombre }}</td>
                                <td style=" font-weight: bold;">Sub Proyecto</td>
                                <td>{{ ($solicitud->padre_id)?$solicitud->proyecto->nombre:'' }}</td>
                            </tr>
                            <tr>
                                <td style=" font-weight: bold;">Monto Desembolso</td>
                                <td style="color: red; font-weight: bold;">{{ number_format($extras['total_desembolso'], '2', '.', '') }}</td>
                                <td style=" font-weight: bold;">Monto Rendido</td>
                                <td style="color: green; font-weight: bold;">{{ number_format($extras['total_rendido'], '2', '.', '') }}</td>
                            </tr>
                            <tr>
                                <td style=" font-weight: bold;">Monto favor del solicitante</td>
                                <td>{{ number_format($extras['favor_solicitante'], '2', '.', '') }}</td>
                                <td style=" font-weight: bold;">Monto favor de la empresa</td>
                                <td>{{ number_format($extras['favor_empresa'], '2', '.', '') }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <h2>ITEMS RENDIDOS</h2>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>TIPO DE COMPRA</th>
                                <th>FACTURA</th>
                                <th>FECHA FACTURA</th>
                                <th>DETALLE</th>
                                <th>CANTIDAD</th>
                                <th>PRECIO</th>
                                <th>SUBTOTAL</th>
                                <th>OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total_rendidos = 0;
                                // dd($items_rendidos);
                            @endphp
                            @foreach($items_rendidos as $itemRendido)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @if(isset($itemRendido->tipo_compra->descripcion))
                                        {{ $itemRendido->tipo_compra->descripcion }}
                                        
                                    @endif
                                </td>
                                <td>{{ ($itemRendido->num_factura != 0)? $itemRendido->num_factura: 'Sin Factura' }}</td>
                                <td>{{ $itemRendido->fecha_factura }}</td>
                                <td>{{ $itemRendido->detalle }}</td>
                                <td style="text-align: right;">{{ $itemRendido->cantidad }}</td>
                                <td style="text-align: right;">{{ number_format($itemRendido->costo, '5', '.', '') }}</td>
                                <td style="text-align: right;">{{ number_format($itemRendido->costo * $itemRendido->cantidad, '5', '.', '') }}</td>
                                <td><a href="/solicitudes/item/rendidos/eliminar?doc={{ $itemRendido->id }}" class="fa fa-times-circle-o icono-red"></a></td>
                                @php
                                    $total_rendidos += number_format($itemRendido->costo * $itemRendido->cantidad, '2', '.', '');
                                @endphp
                            </tr>
                            @endforeach
                            <tr>
                                <td style="text-align: right;" colspan="7"><b>TOTAL RENDIDO</b></td>
                                <td style="text-align: right;"><b>{{ number_format($total_rendidos, '5', '.', '') }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                    <h2>OBSERVACIÓN</h2>
                    <textarea style="text-transform: uppercase; width: 100%;" name="observacion" id="observacion" rows="5" placeholder="EN CASO DE TENER ALGUNA OBSERVACIÓN SOBRE LA RENDICIÓN, FAVOR DESCRIBA LA MISMA EN ESTE ESPACIO..." form="frm_rendicion"></textarea>
                </div>
            </div>
            {!! Form::open(array('route' => 'solicitud.store', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>FORMULARIO DE DEVOLUCIÓN</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="col-md-3">#</th>
                                <th class="col-md-2">N° CUENTA</th>
                                <th class="col-md-1">FECHA</th>
                                <th class="col-md-4">DETALLE</th>
                                <th class="col-md-2">MONTO</th>
                                <th >OPCIONES</th>
                            </tr>
                        </thead>
                        <tbody id="items_devolucion">
                            @foreach($devoluciones as $devolucion)
                                <tr id="item_{{ $loop->iteration }}">
                                    <td style="text-align: center;">{{ $loop->iteration }}</td>
                                    <td><input id="precuenta_{{$loop->iteration}}" style="text-align: right; width: 80px;" value="{{ $devolucion->cuenta_id }}" readonly="readonly"/></td>
                                    <td><input id="prefecha_{{$loop->iteration}}" style="text-align: right; width: 100px;" value="{{ date_format($devolucion->created_at, 'Y-m-d') }}" readonly="readonly"/></td>
                                    <td><input type="text" id="predetalle_{{$loop->iteration}}" value="{{ $devolucion->detalle }}" style=" width: 100%; text-transform: uppercase;" readonly="readonly"/></td>
                                    <td style="text-align: center;"><input id="precantidad_{{$loop->iteration}}" style="text-align: right; width: 60px;" value="{{ $devolucion->monto }}" readonly="readonly"/></td>
                                    <td style="text-align: center;"><i class="fa fa-close icono-opciones icono-red" style="font-size: 18px!important;" onclick="eliminarItem({{ $loop->iteration }})"/></i></td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5"><a id="btn-creafila1" class="btn btn-primary" onclick="creaFilaDevolucion()"><i class="fa fa-plus-circle icono-opciones"></i> AGREGAR NUEVO ITEM PARA DEVOLUCION</a></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            {!! Form::close() !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-12">
                        <h2>FINALIZAR RENDICIÓN</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <button class="btn btn-success btn-block" data-toggle="modal" data-target="#finalizarSolicitudModal" onclick="finalizarSolicitud({{ $solicitud->id }})">ACEPTAR FINALIZAR RENDICIÓN</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="rendirItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">VERIFICAR DATOS RENDICIÓN ITEM</h4>
            </div>
            {!! Form::open(array('route' => 'rendir.solicitud.item', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table_items" class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="hidden">ID</th>
                                <th style="width: 100px;">TIPO DE COMPRA</th>
                                <th style="width: 100px;">FECHA FACTURA</th>
                                <th style="width: 100px;">Nº FACTURA</th>
                                <th>DETALLE</th>
                                <th style="width: 70px;">CANT.</th>
                                <th style="width: 90px;">COSTO</th>
                                <th style="width: 105px;">SUB TOTAL</th>
                            </tr>
                        </thead>
                        <tbody id="item_info"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <div id="solicitud_rendir"></div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<div id="devolucionItemModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">VERIFICAR DATOS RENDICIÓN ITEM</h4>
            </div>
            {!! Form::open(array('route' => 'rendir.solicitud.devolucion', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true)) !!}
            <div class="modal-body">
                <table id="table_items_devolucion" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 45px;">N° CUENTA</th>
                            <th style="width: 100px;">DOCUMENTO</th>
                            <th style="width: 70px;">FECHA</th>
                            <th>DETALLE</th>
                            <th style="width: 90px;">MONTO</th>
                        </tr>
                    </thead>
                    <tbody id="modal_items_devolucion"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div id="solicitud_devolucion"></div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<div id="finalizarSolicitudModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-info">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">FINALIZAR SOLICITUD</h4>
            </div>
            {!! Form::open(array('route' => 'finalizar.solicitud', 'method'=>'POST', 'class'=>'form-horizontal', 'files' => true, 'id' => 'frm_rendicion')) !!}
            <div class="modal-body">
                <p id="mensaje-rendicion">LUEGO DE FINALIZAR LA SOLICITUD, NO PODRA MODIFICAR LA RENDICIÓN</p>
                <input type="hidden" id="rendirSolicitudId" name="solicitud_id" value=""/>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button class='btn btn-default' data-dismiss="modal">CERRAR</button>
                    <input id="btn-finalizar-rendicion" class='btn btn-primary' type='submit' value='FINALIZAR RENDICIÓN'>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
@endsection

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    //Añadir archivos
    var adjuntarArchivo = document.getElementById('adjuntar-archivo');
    var cont_archivos = 0;
    function elegirArchivo(el){
        var btn_elegir_imagen;
        if(el.id == 'elegir-archivo'){
            btn_elegir_imagen = document.getElementById('doc1');
            btn_elegir_imagen.click();
        }else{
            var tam = (el.id).length;
            var valor = (el.id).substring(7, tam);
            btn_elegir_imagen = document.getElementById('doc'+valor);
            btn_elegir_imagen.click();
        }
    }

    function cambio_input(e){
        if(e.id == 'doc1'){
            var fullPath;
            fullPath = document.getElementById(e.id).value;
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            document.getElementById('elegir-archivo').style.display = 'none';
            var primer_archivo = document.getElementById('primer-archivo');
            primer_archivo.innerHTML = '<span style="font-size:14px; font-weight: bold;">'+filename+'</span>'+' <i class="fa fa-times-circle-o icono-opciones icono-red" onclick="borrarArchivo(this)" id="eli1"></i>';
        }else{
            fullPath = document.getElementById(e.id).value;
            if(fullPath){
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
            }
            var doc_tam = (e.id).length;
            var doc_val = (e.id).substr(3, doc_tam);
            document.getElementById('archivo'+doc_val).style.display = 'none';
            var archivos = document.getElementById('docnro'+doc_val);
            //archivos.innerHTML = filename;
            var a_tag = document.createElement('a');
            a_tag.setAttribute("class", "fa fa-times-circle-o icono-opciones-files icono-red");
            a_tag.setAttribute("onclick","borrarArchivo(this)");
            a_tag.setAttribute("id", "eli"+doc_val);

            var preview_btn = document.createElement('a');
            preview_btn.setAttribute("class", "fa fa-search icono-opciones-files icono-green");
            preview_btn.setAttribute("onclick","previewDoc(this)");
            preview_btn.setAttribute("id", "prev"+doc_val);

            var span_tag = document.createElement('span');
            span_tag.setAttribute("style", "font-size:14px; font-weight: bold;");
            span_tag.innerHTML = filename+" ";
            //span_tag.appendChild(preview_btn);
            span_tag.appendChild(a_tag);
            archivos.appendChild(span_tag);
        }
    }

    var upload_number = 2;
    function addFileInput() {
        var d = document.createElement("div");
        d.setAttribute('id', 'docnro'+upload_number);
        var file = document.createElement("input");
        file.setAttribute("type", "file");
        file.setAttribute("name", "doc" + upload_number);
        file.setAttribute("id", "doc" + upload_number);
        file.setAttribute("onchange", "cambio_input(this)");
        file.setAttribute("style", "display:none");
        d.appendChild(file);

        var file_add = document.createElement('div');
        file_add.setAttribute('id', 'archivo' + upload_number);
        file_add.setAttribute('value', upload_number);
        file_add.setAttribute('style', 'display:none;');
        file_add.innerHTML = "<i class='fa fa-upload icono-opciones icono-blue'></i> ";
        file_add.setAttribute('onclick', 'elegirArchivo(this)');
        d.appendChild(file_add);
        document.getElementById("moreUploads").append(d);
        document.getElementById("archivo"+upload_number).click();
        upload_number++;
    }
    //Eliminar Archivo
    function borrarArchivo(eli) {
        var doc_tam = (eli.id).length;
        var doc_val = (eli.id).substring(3, doc_tam);

        //***HACER
        if(doc_val == 1){
            var element = document.getElementById("primer-archivo");
        }else{
            var element = document.getElementById("docnro"+doc_val);
        }
        element.parentNode.removeChild(element);
    }

    function rendirItemModal(item_id){
        $('#item_info').empty();
        $('#solicitud_rendir').empty();

        var factura = "";
        var fecha_factura = "";
        var detalle = "";
        var cantidad = "";
        var costo = "";
        var itemId = document.getElementById('itemId_'+item_id).value;
        var solicitudId = document.getElementById('solicitudId_'+item_id).value;
        var unidad = document.getElementById('preunidad_'+item_id);
        var unidadId = unidad.options[unidad.selectedIndex].value;
        var tipocompra = document.getElementById('pretipocompra_'+item_id);
        var tipocompraId = tipocompra.options[tipocompra.selectedIndex].value;
        var mensaje = [];

        if(document.getElementById('prefactura_'+item_id).value != null){
            factura = document.getElementById('prefactura_'+item_id).value;
            fecha_factura = document.getElementById('prefechafactura_'+item_id).value;console.log('feha'+fecha_factura);
            if(fecha_factura == ''){
                mensaje.push("FECHA FACTURA");
            }
        }
        
        if(document.getElementById('predetalle_'+item_id).value != null){
            detalle = document.getElementById('predetalle_'+item_id).value;
        }

        if(document.getElementById('precantidad_'+item_id).value != null){
            cantidad = document.getElementById('precantidad_'+item_id).value;
        }

        if(document.getElementById('prepunitario_'+item_id).value != null){
            costo = document.getElementById('prepunitario_'+item_id).value;
        }

        if(detalle.trim().length == 0){
            mensaje.push("DETALLE");
        }

        if(cantidad.trim().length == 0){
            mensaje.push("CANTIDAD");
        }

        if(costo.trim().length == 0){
            mensaje.push("COSTO");
        }

        if($('select#pretipocompra_' + item_id + ' :selected').val() == 1 ||
           $('select#pretipocompra_' + item_id + ' :selected').val() == 7){
            for(i=1; i<=cantidad; i++){
                descripcion_especifica = document.getElementById('des_especifica_'+item_id+'_'+i).value;
                if(descripcion_especifica == ''){
                    mensaje.push("DESCRIPCION ESPECIFICA DEL ACTIVO FIJO "+i);
                }
                serie = document.getElementById('serie_'+item_id+'_'+i).value;
                if(serie == ''){
                    mensaje.push("NUMERO DE SERIE / MODELO / CODIGO / PLACA / CHASIS DEL ACTIVO FIJO "+i);
                }
                asignacion = document.getElementById('asignacion_'+item_id+'_'+i).value;
                if(asignacion == ''){
                    mensaje.push("ASIGNACION DEL ACTIFO FIJO "+i);
                }

                actividad = document.getElementById('actividad_'+item_id+'_'+i).value;
            }
        }

        if(mensaje.length > 0){
            tabla += "<tr>";
            tabla += "<td colspan='8'><span style='color: red; font-weight: bold;'>FALTAN LOS SIGUIENTES DATOS: "+mensaje.join(', ')+"</span></td>";
            tabla += "</tr>";
            $('#item_info').append(tabla);
            var btn_rendir = "<button type='button' class='btn btn-default' data-dismiss='modal'>CERRAR</button>";
            $('#solicitud_rendir').append(btn_rendir);
        }else{
            if((factura != null) && (detalle != null) && (cantidad != null) && (costo != null)){
                var tabla = "";
                tabla += "<tr>";
                tabla += "<td class='hidden'><input class='form-control' style='text-align: center;' type='text' name='item_id' value='"+itemId+"' readonly/>";
                tabla += "<input class='form-control' type='hidden' name='solicitud_id' value='{{$solicitud->id}}'/>";
                tabla += "<input class='form-control' type='hidden' name='unidad_id' value='"+unidadId+"'/></td>";
                tabla += "<td><input class='form-control' type='hidden' name='tipocompra_id' value='"+tipocompraId+"' readonly/><input class='form-control' type='text' name='tipocompra_name' value='"+tipocompra.options[tipocompra.selectedIndex].innerHTML+"' readonly/></td>";
                // tabla += "<td><input class='form-control' type='hidden' name='tipocompra_id' value='"+tipocompraId+"' readonly/></td>";
                tabla += "<td><input class='form-control' type='date' name='fecha_factura' value='"+fecha_factura+"' readonly/></td>";
                if(factura.length>0){
                    tabla += "<td><input class='form-control mask-factura' style='text-align: right;' type='text' name='num_factura' value='"+factura+"' readonly/></td>";
                }else{
                    tabla += "<td><input class='form-control mask-factura' style='text-align: center; color: red;' type='text' name='num_factura' value='Sin Factura' readonly/></td>";
                }
                tabla += "<td><input class='form-control' type='text' name='detalle' value='"+detalle+"' readonly/></td>";
                tabla += "<td><input class='form-control mask-cantidad' style='text-align: center;' type='text' name='cantidad' value='"+parseFloat(cantidad)+"' readonly/></td>";
                tabla += "<td><input class='form-control mask-decimal' style='text-align: right;' type='text' name='costo' value='"+(parseFloat(costo)).toFixed(5)+"' readonly/></td>";
                tabla += "<td><input class='form-control mask-decimal' style='text-align: right;' type='text' name='sub_total' value='"+(parseFloat((costo*cantidad))).toFixed(2)+"' readonly/></td>";
                tabla += "</tr>";
                if(tipocompraId == 1 || tipocompraId == 7){ //si es un activo fijo
                    tabla += "<tr>";
                    tabla += "<td colspan='9'><h2 style='text-align: center;'>DATOS ACTIVO FIJO</h2></td>";
                    tabla += "</tr>";
                    tabla += "<tr>";
                    tabla += "<td>Descripcion Especifica</td>";
                    tabla += "<td>Nº Serie / Modelo / Codigo / Placa / Chasis</td>";
                    tabla += "<td>Asignacion</td>";
                    tabla += "<td>Actividad Especifica</td>";
                    tabla += "<td>Nuevo</td>";
                    tabla += "<td>Reemplazo</td>";
                    tabla += "<td>Garantía</td>";
                    tabla += "</tr>";
                    for(i=1; i<=cantidad; i++){
                        descripcion_especifica = document.getElementById('des_especifica_'+item_id+'_'+i).value;
                        serie = document.getElementById('serie_'+item_id+'_'+i).value;
                        asignacion = document.getElementById('asignacion_'+item_id+'_'+i).value;
                        actividad = document.getElementById('actividad_'+item_id+'_'+i).value;
                        if (document.getElementById("nuevo_si_"+item_id+'_'+i).checked)
                            nuevo = "Si";
                        else
                            nuevo = "No";
                        if (document.getElementById("reemplazo_si_"+item_id+'_'+i).checked)
                            reemplazo = "Si";
                        else
                            reemplazo = "No";
                        if (document.getElementById("garantia_si_"+item_id+'_'+i).checked)
                            garantia = "Si";
                        else
                            garantia = "No";

                        tabla += "<tr>";
                        tabla += "<td><input class='form-control' type='text' name='af_descripcion_especifica[]' value='"+descripcion_especifica+"' readonly/></td>";
                        tabla += "<td><input class='form-control' type='text' name='af_numero_serie[]' value='"+serie.toUpperCase()+"' readonly/></td>";
                        tabla += "<td><input class='form-control' type='text' name='af_asignacion[]' value='"+asignacion.toUpperCase()+"' readonly/></td>";
                        tabla += "<td><input class='form-control' type='text' name='af_actividad[]' value='"+actividad.toUpperCase()+"' readonly/></td>";
                        tabla += "<td><input class='form-control' type='text' name='af_nuevo[]' value='"+nuevo+"' readonly/></td>";
                        tabla += "<td><input class='form-control' type='text' name='af_reemplazo[]' value='"+reemplazo+"' readonly/></td>";
                        tabla += "<td><input class='form-control' type='text' name='af_garantia[]' value='"+garantia+"' readonly/></td>";
                        tabla += "</tr>";
                    }
                }
                $('#item_info').append(tabla);
                //Rendir solicitud
                var btn_rendir = "<button type='button' class='btn btn-default' data-dismiss='modal'>CERRAR</button>";
                btn_rendir += "<input class='btn btn-primary' type='submit' value='Rendir Item'>";
                $('#solicitud_rendir').append(btn_rendir);
            }else{                
            }
        }
    }

    function eliminarItem(item_id){
        var item_borrar = document.getElementById('item_'+item_id);
        item_borrar.parentNode.removeChild(item_borrar);
        var table_borrar = document.getElementById('table_'+item_id);
        table_borrar.parentNode.removeChild(table_borrar);
        total_precio();
    }

    function creaFila() {
        // items = document.getElementById('items_para_rendir');
        items = $('#items_para_rendir tr[id^="item_"]');

        var indice = 1;
        if(items.length > 0){
            // ultimo_item = items.rows.length - 1;
            ultimo_item = items.length;
            // iteracion = items.rows[ultimo_item].cells[0].innerHTML;
            iteracion = items.length;

            var table = document.getElementById("items_para_rendir");
            var row = table.rows[ultimo_item];
            var array_indice = row.id.split('_');
            indice = parseInt(array_indice[1])+1;
        }else{
            iteracion = 1;
        }
        var contador = parseInt(iteracion) + 1;
        var tabla = document.getElementById("items_para_rendir");
        var fila = document.createElement("tr");
        fila.setAttribute("id", "item_"+(indice+1));

        //CELDA NUMERO
        var celda1 = document.createElement("td");
        celda1.setAttribute("style", "text-align: center;");
        celda1.innerHTML = contador;

        //CELDA CANTIDAD
        var celda2 = document.createElement("td");
        celda2.setAttribute("style", "text-align: center;");
        var itemId = document.createElement("input");
        itemId.setAttribute("type", "hidden");
        itemId.setAttribute("id", "itemId_"+contador);
        itemId.setAttribute("value", "0");
        celda2.appendChild(itemId);
        var solicitudId = document.createElement("INPUT");
        solicitudId.setAttribute("type", "hidden");
        solicitudId.setAttribute("id", "solicitudId_"+contador);
        solicitudId.setAttribute("value", {{$solicitud->id}});
        celda2.appendChild(solicitudId);

        itemId.setAttribute("value", "0");
        var cantidad = document.createElement("INPUT");
        cantidad.setAttribute("type", "text");
        cantidad.setAttribute("min", "0");
        cantidad.setAttribute("id", "precantidad_" + contador);
        cantidad.setAttribute("step", "0.00001");
        cantidad.setAttribute("size", "3");
        cantidad.setAttribute("maxlength", "12");
        cantidad.setAttribute("name", "cantidad[]");
        cantidad.setAttribute("class", "form-control mask-cantidad");
        cantidad.setAttribute("onchange", "subtotal_cantidad(this.id)");
        cantidad.setAttribute("onkeyup", "subtotal_cantidad(this.id)");
        cantidad.setAttribute("data-cantidad", (indice + 1));
        celda2.appendChild(cantidad);


        //CELDA UNIDAD
        var celda3 = document.createElement("td");
        var unidad = document.createElement("SELECT");
        unidad.setAttribute("size", "1");
        //unidad.setAttribute("class", "form-control"); style='height: 20px;'
        unidad.setAttribute("id", "preunidad_" + contador);
        unidad.setAttribute("class", "form-control");
        unidad.setAttribute("name", "unidad[]");
        <?php
        $unidades = App\Unidades::all();
        foreach ($unidades as $unidad) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $unidad->descripcion . "';";
            echo "opcioncur.value = '" . $unidad->id . "';";
            echo "unidad.appendChild(opcioncur);";
        }
        ?>
        celda3.appendChild(unidad);

        //CELDA TIPO COMPRA
        var celda10 = document.createElement("td");
        var tipocompra = document.createElement("SELECT");
        tipocompra.setAttribute("size", "1");
        //tipocompra.setAttribute("class", "form-control"); style='height: 20px;'
        tipocompra.setAttribute("id", "pretipocompra_" + contador);
        tipocompra.setAttribute("class", "form-control");
        tipocompra.setAttribute("name", "tipocompra_" + (indice + 1));
        tipocompra.setAttribute("onChange", "subtotal_cantidad('pretipocompra_"+ contador +"')");
        <?php
        $tipos = App\ItemsTipos::all();
        foreach ($tipos as $tipo) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $tipo->descripcion . "';";
            echo "opcioncur.value = '" . $tipo->id . "';";
            if($tipo->id == 2){
                echo "opcioncur.selected=true;";
            }
            echo "tipocompra.appendChild(opcioncur);";
        }
        ?>
        celda10.appendChild(tipocompra);

        //CELDA FACTURA
        var celda4 = document.createElement("td");
        var factura = document.createElement("input");
        factura.setAttribute("type", "text");
        factura.setAttribute("size", "40"); //22
        factura.setAttribute("maxlength", "70");
        factura.setAttribute("id", "prefactura_" + contador);
        factura.setAttribute("name", "factura[]");        
        factura.setAttribute("class", "mask-factura form-control");
        celda4.appendChild(factura);

        //CELDA FECHA FACTURA
        var celda9 = document.createElement("td");
        var fecha_factura = document.createElement("input");
        fecha_factura.setAttribute("type", "date");
        fecha_factura.setAttribute("max", "{{ date('Y-m-d') }}");
        fecha_factura.setAttribute("maxlength", "70");
        fecha_factura.setAttribute("id", "prefechafactura_" + contador);
        fecha_factura.setAttribute("name", "fechafactura[]");
        fecha_factura.setAttribute("class", "form-control mask-fecha");
        celda9.appendChild(fecha_factura);

        //CELDA DETALLE
        var celda5 = document.createElement("td");
        var detalle = document.createElement("input");
        detalle.setAttribute("type", "text");
        detalle.setAttribute("maxlength", "70");
        detalle.setAttribute("id", "predetalle_" + contador);
        detalle.setAttribute("name", "detalle[]");
        detalle.setAttribute("style", "text-transform:uppercase;");
        detalle.setAttribute("class", "form-control");
        celda5.appendChild(detalle);

        //CELDA P-UNITARIO
        var celda6 = document.createElement("td");
        var punitario = document.createElement("input");
        punitario.setAttribute("type", "text");
        punitario.setAttribute("min", "0");
        punitario.setAttribute("id", "prepunitario_" + contador);
        punitario.setAttribute("step", "0.01");
        punitario.setAttribute("size", "6");
        punitario.setAttribute("maxlength", "10");
        punitario.setAttribute("name", "punitario[]");
        punitario.setAttribute("class", "form-control mask-decimal");
        punitario.setAttribute("onchange", "subtotal_precio(this.id)");
        punitario.setAttribute("onkeyup", "subtotal_precio(this.id)");
        celda6.appendChild(punitario);

        //CELDA SUB-TOTAL
        var celda7 = document.createElement("td");
        var subtotal = document.createElement("input");
        subtotal.setAttribute("type", "text");
        subtotal.setAttribute("maxlength", "10");
        subtotal.setAttribute("name", "subtotal[]");
        subtotal.setAttribute("class", "form-control text-right");
        subtotal.setAttribute("id", "subtotal_" + contador);
        subtotal.setAttribute("ReadOnly", true);
        subtotal.setAttribute("onchange", "total_precio()");
        celda7.appendChild(subtotal);

        //BOTON ELIMINAR FILA
        var celda8 = document.createElement('td');
        var btn_detalle = document.createElement('i');
        btn_detalle.setAttribute('class', 'fa fa-pencil-square-o icono-opciones icono-green');        
        btn_detalle.setAttribute('data-toggle', 'modal');
        btn_detalle.setAttribute('data-target', '#rendirItemModal');
        btn_detalle.setAttribute('title', 'RENDIR ITEM');
        btn_detalle.setAttribute('onclick', 'rendirItemModal('+contador+')');
        celda8.appendChild(btn_detalle);

        var btn_eliminarItem = document.createElement('i');
        btn_eliminarItem.setAttribute('class', 'fa fa-close icono-opciones icono-red');        
        btn_eliminarItem.setAttribute('title', 'ELIMINAR ITEM');
        btn_eliminarItem.onclick = function () {
            eliminarItem(indice+1);
        }
        celda8.appendChild(btn_eliminarItem);

        fila.appendChild(celda1); //numero
        fila.appendChild(celda3); //unidad
        fila.appendChild(celda10); //tipocompra
        fila.appendChild(celda5); //detalle
        fila.appendChild(celda9); //fecha factura
        fila.appendChild(celda4); //factura
        fila.appendChild(celda2); //cantidad
        fila.appendChild(celda6); //precio
        fila.appendChild(celda7); //sub-total
        fila.appendChild(celda8); //opciones

        tabla.appendChild(fila);

        var filaTabla = document.createElement("tr");
        filaTabla.setAttribute("id", "table_"+(indice+1));

        tabla.appendChild(filaTabla);

        mascara();

        $('select[id^="tipo_compra_"] option[value=8]').removeAttr('selected').hide().attr('disabled', 'disabled');
        $('select[id^="tipo_compra_"] option[value=9]').removeAttr('selected').hide().attr('disabled', 'disabled');
    }

    function finalizarSolicitud(id){
        //items_para_rendir
        var rows = items = $('#items_para_rendir tr[id^="item_"]').length;
        var btn_fin_rendicion = document.getElementById("btn-finalizar-rendicion");
        if(rows > 0){
            var mensaje = "<i style='font-weight: bold; color: red;'>TODAVIA TIENE ";
            mensaje += rows;
            mensaje += " ITEM(S) PARA RENDIR, ELIMINELO(S) O RINDALO(S) PARA FINALIZAR LA RENDICIÓN</i><br>";
            mensaje += "LUEGO DE FINALIZAR LA SOLICITUD, NO PODRA MODIFICAR LA RENDICIÓN";
            document.getElementById("mensaje-rendicion").innerHTML = mensaje;
            btn_fin_rendicion.setAttribute("style", "display:none;");
        }else{
            var mensaje = "LUEGO DE FINALIZAR LA SOLICITUD, NO PODRA MODIFICAR LA RENDICIÓN";
            document.getElementById("mensaje-rendicion").innerHTML = mensaje;
            btn_fin_rendicion.setAttribute("style", "display:initial;");
        }
        document.getElementById('rendirSolicitudId').value = id;
    }

    $(document).ready(function(){
        $.each($('[id^="precantidad_"]'), function(i, obj){
            var cantidad_id = obj.id;
            subtotal_cantidad(cantidad_id);
        });
    });

    function subtotal_cantidad(id) {
        var res_array = id.split('_');
        var sub;
        var cant;
        var precio = document.getElementById('prepunitario_' + res_array[1]);
        var cantidad = document.getElementById('precantidad_' + res_array[1]);
        var table_id = cantidad.getAttribute('data-cantidad');
        
        $('#table_'+table_id).empty();

        if(
            $('select#pretipocompra_' + res_array[1] + ' :selected').val() == 1 ||
            $('select#pretipocompra_' + res_array[1] + ' :selected').val() == 7
        ){
            sub = document.getElementById('subtotal_' + res_array[1]);
            cant = document.getElementById('precantidad_' + res_array[1]);
            sub.value = parseFloat((cant.value) * (precio.value)).toFixed(2);
            total_precio();
            var tabla = document.getElementById('table_'+table_id);
            var tabla_str = "<td colspan='10'>";
            tabla_str += "<h2 style='text-align: center' class='head_frm_af'>Formulario Activo Fijo</h2>";
            tabla_str += "<table class='table-af table table-striped table-bordered'>";
            tabla_str += "<tr>";
            tabla_str += "<th>#</th>";
            tabla_str += "<th>Descripción Especifica</th>";
            tabla_str += "<th>Nº Serie / Modelo / Código / Placa / Chasis</th>";
            tabla_str += "<th>Asignación</th>";
            tabla_str += "<th>Actividad Espedifica</th>";
            tabla_str += "<th>Nuevo</th>";
            tabla_str += "<th>Reemplazo</th>";
            tabla_str += "<th>Garantía</th>";
            tabla_str += "</tr>";
            for(i=1; i <= cantidad.value; i++){
                tabla_str += "<tr>";
                tabla_str += "<td>"+i+"</td>";
                tabla_str += "<td><input type='text' id='des_especifica_"+res_array[1]+"_"+i+"' name='descripcion_"+table_id+"[]' style=' width: 100%; text-transform: uppercase;' maxlength='255' required/></td>";
                tabla_str += "<td><input type='text' id='serie_"+res_array[1]+"_"+i+"' name='serie_"+table_id+"[]' style=' width: 100%; text-transform: uppercase;' maxlength='255' required/></td>";
                tabla_str += "<td><input type='text' id='asignacion_"+res_array[1]+"_"+i+"' name='asignacion_"+table_id+"[]' style=' width: 100%; text-transform: uppercase;' maxlength='255' required/></td>";
                tabla_str += "<td><input type='text' id='actividad_"+res_array[1]+"_"+i+"' name='actividad_"+table_id+"[]' style=' width: 100%; text-transform: uppercase;'/></td>";
                tabla_str += "<td>";
                tabla_str += "<input type='radio' id='nuevo_si_"+res_array[1]+"_"+i+"' name='nuevo_"+table_id+"_"+i+"[]' value='1'/>";
                tabla_str += "<label for='nuevo_si_"+res_array[1]+"_"+i+"'>Si</label>&nbsp;&nbsp;&nbsp;";
                tabla_str += "<input type='radio' id='nuevo_no_"+res_array[1]+"_"+i+"' name='nuevo_"+table_id+"_"+i+"[]' value='0' checked/>";
                tabla_str += "<label for='nuevo_no_"+res_array[1]+"_"+i+"'>No</label>";
                tabla_str += "</td>";
                tabla_str += "<td>";
                tabla_str += "<input type='radio' id='reemplazo_si_"+res_array[1]+"_"+i+"' name='reemplazo_"+table_id+"_"+i+"[]' value='1'/>";
                tabla_str += "<label for='reemplazo_si_"+res_array[1]+"_"+i+"'>Si</label>&nbsp;&nbsp;&nbsp;";
                tabla_str += "<input type='radio' id='reemplazo_no_"+res_array[1]+"_"+i+"' name='reemplazo_"+table_id+"_"+i+"[]' value='0' checked/>";
                tabla_str += "<label for='reemplazo_no_"+res_array[1]+"_"+i+"'>No</label>";
                tabla_str += "</td>";
                tabla_str += "<td>";
                tabla_str += "<input type='radio' id='garantia_si_"+res_array[1]+"_"+i+"' name='garantia_"+table_id+"_"+i+"[]' value='1'/>";
                tabla_str += "<label for='garantia_si_"+res_array[1]+"_"+i+"'>Si</label>&nbsp;&nbsp;&nbsp;";
                tabla_str += "<input type='radio' id='garantia_no_"+res_array[1]+"_"+i+"' name='garantia_"+table_id+"_"+i+"[]' value='0' checked/>";
                tabla_str += "<label for='garantia_no_"+res_array[1]+"_"+i+"'>No</label>";
                tabla_str += "</td>";
                tabla_str += "</tr>";
            }
            tabla_str += "</table>";
            tabla_str += "</td>";
            tabla.innerHTML = tabla_str;
        }
    }

    function subtotal_precio(id) {
        var res_array = id.split('_');
        var sub;
        var cant;
        var precio = document.getElementById('precantidad_' + res_array[1]);
        if (precio.value != '') {
            sub = document.getElementById('subtotal_' + res_array[1]);
            cant = document.getElementById('prepunitario_' + res_array[1]);

            sub.value = parseFloat((cant.value) * (precio.value)).toFixed(2);
            sub.setAttribute('value', parseFloat((cant.value) * (precio.value)).toFixed(2));
            total_precio();
        }
    }

    function total_precio() {
        var total;
        total = 0;
        var element;
        //***REVISAR CONTADOR
        for (i = 0; i < 30; i++) {
            element = (document.getElementById('subtotal_' + i));
            if (typeof (element) != 'undefined' && element != null && element.value != null && element.value != '') {
                total += parseFloat(element.value);
            }
        }
        document.getElementById("total").innerHTML = parseFloat(total).toFixed(2);
    }

    function creaFilaDevolucion() {
        items = document.getElementById('items_devolucion');
        var indice = 1;
        if(items.rows.length > 0){
            ultimo_item = items.rows.length - 1;
            iteracion = items.rows[ultimo_item].cells[0].innerHTML;

            var table = document.getElementById("items_devolucion");
            var row = table.rows[ultimo_item];
            var array_indice = row.id.split('_');
            indice = parseInt(array_indice[1]);
        }else{
            iteracion = 1;
        }
        var contador = parseInt(iteracion) + 1;
        var tabla = document.getElementById("items_devolucion");
        var fila = document.createElement("tr");
        fila.setAttribute("id", "item_"+(indice+1));

        //CELDA NUMERO DE CUENTA
        var celda1 = document.createElement("td");
        var nro_cuenta_dev = document.createElement("SELECT");
        nro_cuenta_dev.setAttribute("size", "1");
        //nro_cuenta_dev.setAttribute("class", "form-control");
        nro_cuenta_dev.setAttribute("style", "height: 20px;");
        nro_cuenta_dev.setAttribute("id", "nro_cuenta_dev_" + contador);
        nro_cuenta_dev.setAttribute("name", "nro_cuenta_dev[]");
        <?php
        $cuentas = App\CuentaBancaria::where('empresa_id', session('empresa'))->get();
        foreach ($cuentas as $cuenta) {
            echo "opcioncur = document.createElement('OPTION');";
            echo "opcioncur.innerHTML = '" . $cuenta->numero."(".$cuenta->banco->nombre . ")';";
            echo "opcioncur.value = '" . $cuenta->id . "';";
            echo "nro_cuenta_dev.appendChild(opcioncur);";
        }
        ?>
        celda1.appendChild(nro_cuenta_dev);

        //CELDA DOCUMENTO
        var celda2 = document.createElement("td");
        var documento_dev = document.createElement("INPUT");
        documento_dev.setAttribute("type", "text");
        //documento_dev.setAttribute("class", "form-control");
        documento_dev.setAttribute("size", "40"); //22
        documento_dev.setAttribute("maxlength", "70");
        documento_dev.setAttribute("id", "documento_dev_" + contador);
        documento_dev.setAttribute("name", "documento_dev[]");
        documento_dev.setAttribute("style", "text-transform:uppercase; width: 80px; text-align: right;");
        celda2.appendChild(documento_dev);

        //CELDA FECHA
        var celda3 = document.createElement("td");
        var fecha_dev = document.createElement("INPUT");
        fecha_dev.setAttribute("type", "date");
        //fecha_dev.setAttribute("class", "form-control");
        fecha_dev.setAttribute("maxlength", "70");
        fecha_dev.setAttribute("id", "fecha_dev_" + contador);
        fecha_dev.setAttribute("name", "fecha_dev[]");
        fecha_dev.setAttribute("style", "width: 100px;");
        celda3.appendChild(fecha_dev);

        //CELDA DETALLE
        var celda4 = document.createElement("td");
        var detalle_dev = document.createElement("INPUT");
        detalle_dev.setAttribute("type", "text");
        detalle_dev.setAttribute("id", "detalle_dev_" + contador);
        detalle_dev.setAttribute("name", "detalle_dev[]");
        detalle_dev.setAttribute("style", "text-transform:uppercase; width: 100%;");
        celda4.appendChild(detalle_dev);

        //CELDA MONTO
        var celda5 = document.createElement("td");
        var monto_dev = document.createElement("INPUT");
        monto_dev.setAttribute("type", "text");
        monto_dev.setAttribute("class", "mask-decimal");
        monto_dev.setAttribute("min", "0");
        monto_dev.setAttribute("step", "0.01");
        monto_dev.setAttribute("name", "monto_dev[]");
        monto_dev.setAttribute("style", "text-transform:uppercase; text-align: right; width: 90px;");
        monto_dev.setAttribute("id", "monto_dev_" + contador);
        celda5.appendChild(monto_dev);

        //CELDA OPCIONES
        var celda6 = document.createElement('td');
        var btn_detalle = document.createElement('i');
        btn_detalle.setAttribute('class', 'fa fa-pencil-square-o icono-opciones icono-green');
        btn_detalle.setAttribute('style', 'font-size: 18px!important;');
        btn_detalle.setAttribute('data-toggle', 'modal');
        btn_detalle.setAttribute('data-target', '#devolucionItemModal');
        btn_detalle.setAttribute('title', 'REALIZAR DEVOLUCION');
        btn_detalle.setAttribute('onclick', 'devolucionItemModal('+contador+')');
        celda6.appendChild(btn_detalle);

        var btn_eliminarItem = document.createElement('i');
        btn_eliminarItem.setAttribute('class', 'fa fa-close icono-opciones icono-red');
        btn_eliminarItem.setAttribute('style', 'font-size: 18px!important;');
        btn_eliminarItem.setAttribute('title', 'ELIMINAR ITEM');
        btn_eliminarItem.onclick = function () {
            eliminarItem(indice+1);
        }
        celda6.appendChild(btn_eliminarItem);

        fila.appendChild(celda1); //numero de cuenta
        fila.appendChild(celda2); //documento
        fila.appendChild(celda3); //fecha
        fila.appendChild(celda4); //detalle
        fila.appendChild(celda5); //monto
        fila.appendChild(celda6); //opciones

        tabla.appendChild(fila);
        mascara();
    }

    function devolucionItemModal(item_id){
        $('#item_devolucion').empty();
        $('#solicitud_devolucion').empty();
        $('#modal_items_devolucion').empty();

        var cuenta = document.getElementById('nro_cuenta_dev_'+item_id);
        var cuentaId = cuenta.options[cuenta.selectedIndex].value;
        var documento = "asdf";
        var fecha = document.getElementById('fecha_dev_'+item_id).value;
        var detalle = "";
        var monto = "";
//        var itemId = document.getElementById('itemId_'+item_id).value;
//        var solicitudId = document.getElementById('solicitudId_'+item_id).value;

        if(document.getElementById('detalle_dev_'+item_id).value != null){
            detalle = document.getElementById('detalle_dev_'+item_id).value;
        }

        if(document.getElementById('monto_dev_'+item_id).value != null){
            monto = document.getElementById('monto_dev_'+item_id).value;
        }

        var mensaje = [];
        if(detalle.trim().length == 0){
            mensaje.push("DETALLE");
        }

        if(monto.trim().length == 0){
            mensaje.push("MONTO");
        }

        if(mensaje.length > 0){
            tabla += "<tr>";
            tabla += "<td colspan='5'><span style='color: red; font-weight: bold;'>FALTAN LOS SIGUIENTES DATOS: "+mensaje.join(', ')+"</span></td>";
            tabla += "</tr>";
            $('#modal_items_devolucion').append(tabla);
        }else{
            if((detalle != null) && (monto != null)){
                var tabla = "";
                tabla += "<tr>";

                tabla += "<td><input class='form-control' style='text-align: center;' type='text' name='item_id' value='"+cuentaId+"' readonly/>";
                tabla += "<input class='form-control' type='hidden' name='solicitud_id' value='{{$solicitud->id}}'/>";
                tabla += "<input class='form-control' type='hidden' name='unidad_devolucion_id' value='"+documento+"'/></td>";
                tabla += "<td><input class='form-control' type='text' name='documento_devolucion' value='"+documento+"' readonly/></td>";
                tabla += "<td><input class='form-control' type='text' name='fecha_devolucion' value='"+fecha+"' readonly/></td>";
                tabla += "<td><input class='form-control'' type='text' name='detalle_devolucion' value='"+detalle+"' readonly/></td>";
                tabla += "<td><input class='form-control' style='text-align: right;' type='text' name='monto_devolucion' value='"+(parseFloat(monto)).toFixed(2)+"' readonly/></td>";
                tabla += "</tr>";
                $('#modal_items_devolucion').append(tabla);
                //Rendir solicitud
                var btn_rendir = "<input class='btn btn-primary' type='submit' value='Rendir Item'>";
                $('#solicitud_devolucion').append(btn_rendir);
            }else{
            }
        }
    }
</script>
@endsection
