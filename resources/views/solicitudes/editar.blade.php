@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<?php
use App\Proyectos;
$user = auth()->user();

$id_proys = array();
$str_proys = '';
if(sizeof($proyectos) == 0){
    echo "<script>window.location.href = '/solicitud';</script>";
    exit();
}
$ids_proys = array();
foreach ($proyectos as $proye) {
    $ids_proys[] = $proye['id'];
    $str_proys .= $proye['id'];
    if ($proye !== end($proyectos)) {
        $str_proys .= ',';
    }
}
if (substr($str_proys, -1) == ',') {
    $str_proys = substr($str_proys, 0, strlen($str_proys) - 1);
}
$consulta_proy = '';
if(!isset($ids_proys)){
//    echo "<script>window.location.href = '/solicitud';</script>";
//    exit();
    //Mostrar redireccionar y mostrar mensaje
}
$resp_proy = $proyectos;
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Guardar -->
                {!! Form::model($solicitud, array('route' => ['solicitud.update', $solicitud->id], 'method'=>'PATCH', 'class'=>'form-horizontal', 'files' => true)) !!}
                {{-- {!! Form::model($solicitud, array('route' => ['solicitud.update', $solicitud->id], 'method'=>'PATCH', 'class'=>'form-horizontal', 'files' => true, 'onkeypress' => 'return event.keyCode != 13;')) !!} --}}
                <!-- Enviar a Controller -->
                <div class="panel panel-default no-print">
                    <div class="panel-heading"><h2>Información Solicitud #{{ $solicitud->numero }}</h2></div>
                    <div class="panel-body">
                        @include('solicitudes.partial.form')
                    </div>
                </div>
                <div class="panel panel-default no-print">
                    <div class="panel-heading">
                        <div class="col-md-8">
                            <h2>Items Para Solicitud</h2>
                            <span style="color:green; font-weight: bold;">(Introduzca cantidad y precios unitarios correspondientes)</span>
                        </div>
                        <div id="seccion_excel" class="col-md-4 text-right" style="display: none;">
                            <div class="btn-group" role="group">
                                <a class="btn btn-default" href='{{ url("public") }}/fr_excel.xlsx'>
                                    <i class="fa fa-download"></i>  Descargar Plantilla Excel
                                </a>
                            </div>
                            <br/><br/>
                            <div class="btn-group" role="group">
                                {!! Form::file('file_excel', ['style' => 'display: none;']) !!}
                                <button id="btn_upload_excel" class="btn btn-info" type="button" onclick="document.getElementsByName('file_excel')[0].click()">
                                    <i class="fa fa-file-excel-o"></i> Seleccionar Plantilla Excel
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="solicitud-items">
                                <thead>
                                    <tr>
                                        <th style="width: 1%">#</th>
                                        <th style="width: 5%">Cantidad</th>
                                        <th style="width: 9%">Unidad</th>
                                        <th style="width: 9%">Tipo Compra</th>
                                        <th style="width: 15%">Detalle</th>
                                        <th style="width: 5%" class="row_fr">Nro. Factura / Recibo</th>
                                        <th style="width: 5%" class="row_fr">Fecha Factura / Recibo</th>
                                        <th style="width: 10%" class="row_fr">Proyecto</th>
                                        <th style="width: 10%" class="row_fr">Proveedor</th>
                                        <th style="width: 10%" class="row_fr">Tipo</th>
                                        <th style="width: 8%">P. Unitario</th>
                                        <th style="width: 8%">Sub-Total</th>
                                        <th style="width: 5%">Opción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($solicitud->items as $item)
                                    <tr>
                                        <input type="hidden" value="{{ $loop->iteration-1 }}" name="counter">
                                        <td>
                                            <span style="text-align:center;">{{ $loop->iteration }}</span>
                                        </td>
                                        <td style="text-align: right;">
                                            {{-- 
                                                BEGIN: ISSUE 0016
                                            --}}
                                            <input type="text" min="0" id="cantidad_{{ $loop->iteration-1 }}" value="{{ $item->cantidad }}" class="form-control mask-cantidad" size="3" maxlength="12" name="cantidad[]" step="0.00001" onchange="subtotal_cantidad(this.id)" onkeyup="subtotal_cantidad(this.id)" />
                                            {{-- 
                                                END: ISSUE 0016
                                            --}}
                                        </td>
                                        <td>
                                            {!! Form::select('unidad[]', $unidades, $item->unidad->id, ['id' => 'unidad_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                        </td>
                                        <td>
                                            {!! Form::select('tipo_compra[]', $tipocompra, $item->tipo_compra->id, ['id' => 'tipo_compra_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" size="40" maxlength="70" id="detalle_{{ $loop->iteration-1 }}" name="detalle[]" style="text-transform:uppercase;" value="{{ $item->detalle }}">
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            <input type="number" class="form-control" min="0" id="nrofactura_{{ $loop->iteration-1 }}" name="nrofactura[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->num_factura }}"/>
                                            @else
                                            <input type="number" class="form-control" min="0" id="nrofactura_{{ $loop->iteration-1 }}" name="nrofactura[]" value=""/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            <input type="date" class="form-control mask-fecha" id="fechafactura_{{ $loop->iteration-1 }}" name="fechafactura[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->fecha_factura }}"/>
                                            @else
                                            <input type="date" class="form-control mask-fecha" id="fechafactura_{{ $loop->iteration-1 }}" name="fechafactura[]" value=""/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            <input type="text" class="form-control" id="itemproyecto_{{ $loop->iteration-1 }}" name="itemproyecto[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->proyecto }}" style="text-transform:uppercase;"/>
                                            @else
                                            <input type="text" class="form-control" id="itemproyecto_{{ $loop->iteration-1 }}" name="itemproyecto[]" value="" style="text-transform:uppercase;"/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            <input type="text" class="form-control" id="itemproveedor_{{ $loop->iteration-1 }}" name="itemproveedor[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->proveedor }}" style="text-transform:uppercase;"/>
                                            @else
                                            <input type="text" class="form-control" id="itemproveedor_{{ $loop->iteration-1 }}" name="itemproveedor[]" value="" style="text-transform:uppercase;"/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            {!! Form::select('itemtipo[]', ['FACTURA' => 'FACTURA', 'RECIBO' => 'RECIBO'], \App\ItemsRendiciones::where('item_id', $item->id)->first()->tipo, ['id' => 'itemtipo_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                            @else
                                            {!! Form::select('itemtipo[]', ['FACTURA' => 'FACTURA', 'RECIBO' => 'RECIBO'], null, ['id' => 'itemtipo_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                            @endif
                                        </td>

                                        <td style="text-align: right;">
                                            {{-- 
                                                BEGIN: ISSUE 0016
                                            --}}
                                            <input type="text" id="punitario_{{ $loop->iteration-1 }}" class="form-control mask-decimal" step="0.00001" size="6" maxlength="10" name="punitario[]" style="text-transform:uppercase; text-align: right" onchange="subtotal_precio(this.id)" onkeyup="subtotal_precio(this.id)" value="{{ $item->costo }}">
                                            {{-- 
                                                END: ISSUE 0016
                                            --}}
                                        </td>
                                        <td style="text-align: right;">
                                            <input type="text" class="form-control" size="6" maxlength="10" name="subtotal[]" style="text-transform:uppercase; text-align: right" id="subtotal_{{ $loop->iteration-1 }}" readonly="true" onchange="total_precio()" value="{{ number_format(($item->costo * $item->cantidad), '2', '.', '') }}">
                                        </td>
                                        <td>
                                            <div class="botoneliminar" onclick="eliFila(this)">
                                                <i class="fa fa-times-circle-o icono-opciones icono-red" value='Quitar'></i>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            {{-- 
                                BEGIN: ISSUE 0006
                            --}}
                            <span id="table-items" class="help-block" style="color: #a94442;">
                                <strong></strong>
                            </span>
                            {{-- 
                                END: ISSUE 0006
                            --}}

                            <table class="table table-hover" id="solicitud-items-total" style="margin-bottom: 20px;">
                                <tbody>
                                    <tr style="border: 0;">
                                        <td colspan="3"><a id="btn-creafila1" class="btn btn-primary" onclick="creaFila()"><i class="fa fa-plus-circle icono-opciones"></i> AGREGAR NUEVO ITEM</a></td>
                                        <td style="width: 10%; text-align: right; padding-top: 15px;"><strong>TOTAL: </strong></td>
                                        <td style="width: 10%;"><a class="btn btn-info" id="total" style="width: 100%; margin: 0px; text-align:right;">0</a></td>
                                        <td colspan="2" style="text-align:left; width: 5%; padding-top: 15px;"><strong id="monedaSiglaTag"> Bs.</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('referencia') ? ' has-error' : '' }}">
                                <label for="referencia" class="col-md-2 control-label">REFERENCIA </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('referencia', null, ['placeholder' => 'referencia','class' => 'form-control', 'id' => 'referencia', 'style' => 'text-transform:uppercase', 'rows' => 4]) !!}

                                    @if ($errors->has('referencia'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('referencia') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('observacion') ? ' has-error' : '' }}">
                                <label for="observacion" class="col-md-2 control-label">JUSTIFICACIÓN </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('observacion', null, ['placeholder' => 'justificación','class' => 'form-control', 'id' => 'observacion', 'style' => 'text-transform:uppercase', 'rows' => 4]) !!}

                                    @if ($errors->has('observacion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('observacion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                {!! Form::hidden('imprimir', '0', array('id' => 'imprimir')) !!}
                                {!! Form::submit('GUARDAR CAMBIOS OCULTO', ['class' => 'btn btn-success btn-block', 'id' => 'btn-registro-solicitud', 'style' => 'display:none']) !!}
                                {{-- 
                                    BEGIN: ISSUE 0001, 0006
                                --}}
                                <button type="button" class="btn btn-success" onclick="datosModal();">
                                    GUARDAR CAMBIOS
                                </button>
                                {{-- 
                                    END: ISSUE 0001, 0006
                                --}}
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <!-- MODAL -->
                {{-- 
                    BEGIN: ISSUE 0006
                --}}
                @include('solicitudes.partial.modalVerificar');
                {{-- 
                    END: ISSUE 0006
                --}}
                <!-- END MODAL -->
                {{-- 
                    BEGIN: ISSUE 0001
                --}}
                {{-- BEGIN MODAL ADVERTENCIA --}}
                @include('solicitudes.partial.modalAdvertencia');
                {{-- END MODAL ADVERTENCIA --}}
                {{-- 
                    END: ISSUE 0001
                --}}
            </div>
        </div>
    </div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    
    /**
     * BEGIN: ISSUE 0001
     */
    $(document).ready(function (e){
        total_precio();
        $('select.select_control').change(function(event) {
            var usuario_id = {!! $solicitud->usuario_id !!};
            var nombre_usuario = "{!! $solicitud->usuario->nombre !!}";
            var proyecto_actual_id = {!! $solicitud->proyecto_id !!};
            var nro_solicitud = {{ $solicitud->numero }};
            verificaProyecto(usuario_id, nombre_usuario, proyecto_actual_id, nro_solicitud);
        });
    });
    /**
     * END: ISSUE 0001
     */
    
    function eliFila(e){
        borrarFila(e);
        total_precio();
    }

    //Filas Tabla Items
    var contador = {{ count($solicitud->items) }};

    // Procesa el Excel
    // $('button#btn_process_excel').on('click', function(event){
    $('input[name="file_excel"]').on('change', function(event){
        // $('form#frm_excel').submit();
        
        var data = new FormData();
        data.append('file', $('input[name="file_excel"]').prop('files')[0]);

        $.ajax({
            url: '{{ route('importar.excel') }}',
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
            // data: frm_excel.serialize(),
            data: data,
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
        }).done(function (response){
            if(!response.error){
                $.each(response['items'], function(key, value){
                    creaFilaDatos(value.cantidad, value.unidad_id, value.tipo_id, value.detalle, value.nro_factura, value.fecha_factura, value.proyecto, value.proveedor, value.tipo, value.costo);
                });
            }else{
                // alert(response.error);
                $('div#excelErrorModal .modal-body').html('<span>'+ response.error +'</span><br/><br/><span>No se importaron las datos.</span>')
                $('div#excelErrorModal').modal('show');
            }

            $('input[name="file_excel"]').val('');
        });
    });
</script>
{{ Html::script('/js/solicitud/funciones.js') }}
@endsection