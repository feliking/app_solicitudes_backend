@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<?php
use App\Proyectos;
$user = auth()->user();

$id_proys = array();
$str_proys = '';
if(sizeof($proyectos) == 0){
    echo "<script>window.location.href = '/solicitud';</script>";
    exit();
}
$ids_proys = array();
foreach ($proyectos as $proye) {
    $ids_proys[] = $proye['id'];
    $str_proys .= $proye['id'];
    if ($proye !== end($proyectos)) {
        $str_proys .= ',';
    }
}
if (substr($str_proys, -1) == ',') {
    $str_proys = substr($str_proys, 0, strlen($str_proys) - 1);
}
$consulta_proy = '';
if(!isset($ids_proys)){
//    echo "<script>window.location.href = '/solicitud';</script>";
//    exit();
    //Mostrar redireccionar y mostrar mensaje
}
$resp_proy = $proyectos;
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Guardar -->
                {!! Form::model($solicitud, array('route' => ['solicitud.update', $solicitud->id], 'method'=>'PATCH', 'class'=>'form-horizontal', 'files' => true)) !!}
                <!-- Enviar a Controller -->
                <div class="panel panel-default no-print">
                    <div class="panel-heading"><h2>Modificar Información Solicitud #{{ $solicitud->numero }} (Controller)</h2></div>
                    <div class="panel-body">
                        @include('solicitudes.partial.form')
                    </div>
                </div>
                <div class="panel panel-default no-print">
                    <div class="panel-heading">
                        <h2>Items Para Solicitud</h2>
                        <span style="color:green; font-weight: bold;">(Introduzca cantidad y precios unitarios correspondientes)</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered" id="solicitud-items">
                                <thead>
                                    <tr>
                                        <th style="width: 1%">#</th>
                                        <th style="width: 5%">Cantidad</th>
                                        <th style="width: 9%">Unidad</th>
                                        <th style="width: 9%">Tipo Compra</th>
                                        <th style="width: 15%">Detalle</th>
                                        <th style="width: 5%" class="row_fr">Nro. Factura / Recibo</th>
                                        <th style="width: 5%" class="row_fr">Fecha Factura / Recibo</th>
                                        <th style="width: 10%" class="row_fr">Proyecto</th>
                                        <th style="width: 10%" class="row_fr">Proveedor</th>
                                        <th style="width: 10%" class="row_fr">Tipo</th>
                                        <th style="width: 8%">P. Unitario</th>
                                        <th style="width: 8%">Sub-Total</th>
                                        <th style="width: 5%">Opción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($solicitud->items as $item)
                                    <tr>
                                        <input type="hidden" value="{{ $loop->iteration-1 }}" name="counter">
                                        <input type="hidden" value="true" name="controller">
                                        <td>
                                            <span style="text-align:center;">{{ $loop->iteration }}</span>
                                        </td>
                                        <td style="text-align: right;">
                                            <input type="number" min="0" id="cantidad_{{ $loop->iteration-1 }}" value="{{ $item->cantidad }}" class="form-control" step="0.00001" size="3" maxlength="12" name="cantidad[]" onchange="subtotal_cantidad(this.id)" onkeyup="subtotal_cantidad(this.id)" />
                                        </td>
                                        <td>
                                            {!! Form::select('unidad[]', $unidades, $item->unidad->id, ['id' => 'unidad_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                        </td>
                                        <td>
                                            {!! Form::select('tipo_compra[]', $tipocompra, $item->tipo_compra->id, ['id' => 'tipo_compra_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" size="40" maxlength="70" id="detalle_{{ $loop->iteration-1 }}" name="detalle[]" style="text-transform:uppercase;" value="{{ $item->detalle }}">
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            <input type="number" class="form-control" min="1" id="nrofactura_{{ $loop->iteration-1 }}" name="nrofactura[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->num_factura }}"/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                        <input type="date" class="form-control mask-fecha" id="fechafactura_{{ $loop->iteration-1 }}" name="fechafactura[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->fecha_factura }}"/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            <input type="text" class="form-control" id="itemproyecto_{{ $loop->iteration-1 }}" name="itemproyecto[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->proyecto }}" style="text-transform:uppercase;"/>
                                            @else
                                            <input type="text" class="form-control" id="itemproyecto_{{ $loop->iteration-1 }}" name="itemproyecto[]" value="" style="text-transform:uppercase;"/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            <input type="text" class="form-control" id="itemproveedor_{{ $loop->iteration-1 }}" name="itemproveedor[]" value="{{ \App\ItemsRendiciones::where('item_id', $item->id)->first()->proveedor }}" style="text-transform:uppercase;"/>
                                            @else
                                            <input type="text" class="form-control" id="itemproveedor_{{ $loop->iteration-1 }}" name="itemproveedor[]" value="" style="text-transform:uppercase;"/>
                                            @endif
                                        </td>

                                        <td class="row_fr">
                                            @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
                                            {!! Form::select('itemtipo[]', ['FACTURA' => 'FACTURA', 'RECIBO' => 'RECIBO'], \App\ItemsRendiciones::where('item_id', $item->id)->first()->tipo, ['id' => 'itemtipo_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                            @else
                                            {!! Form::select('itemtipo[]', ['FACTURA' => 'FACTURA', 'RECIBO' => 'RECIBO'], null, ['id' => 'itemtipo_'.($loop->iteration-1), 'class' => 'form-control']) !!}
                                            @endif
                                        </td>

                                        <td style="text-align: right;">
                                            <input type="number" min="0" id="punitario_{{ $loop->iteration-1 }}" class="form-control" step="0.00001" size="6" maxlength="10" name="punitario[]" style="text-transform:uppercase; text-align: right" onchange="subtotal_precio(this.id)" onkeyup="subtotal_precio(this.id)" value="{{ number_format($item->costo, '2', '.', '') }}">
                                        </td>
                                        <td style="text-align: right;">
                                            <input type="text" class="form-control" size="6" maxlength="10" name="subtotal[]" style="text-transform:uppercase; text-align: right" id="subtotal_{{ $loop->iteration-1 }}" readonly="true" onchange="total_precio()" value="{{ number_format(($item->costo * $item->cantidad), '2', '.', '') }}">
                                        </td>
                                        <td>
                                            <div class="botoneliminar" onclick="eliFila(this)">
                                                <i class="fa fa-times-circle-o icono-opciones icono-red" value='Quitar'></i>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <span id="table-items" class="help-block" style="color: #a94442;">
                                <strong></strong>
                            </span>

                            <table class="table table-hover" id="solicitud-items-total" style="margin-bottom: 20px;">
                                <tbody>
                                    <tr style="border: 0;">
                                        <td colspan="3"><a id="btn-creafila1" class="btn btn-primary" onclick="creaFila()"><i class="fa fa-plus-circle icono-opciones"></i> AGREGAR NUEVO ITEM</a></td>
                                        <td style="width: 10%; text-align: right; padding-top: 15px;"><strong>TOTAL: </strong></td>
                                        <td style="width: 10%;"><a class="btn btn-info" id="total" style="width: 100%; margin: 0px; text-align:right;">0</a></td>
                                        <td colspan="2" style="text-align:left; width: 5%; padding-top: 15px;"><strong id="monedaSiglaTag"> Bs.</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('referencia') ? ' has-error' : '' }}">
                                <label for="referencia" class="col-md-2 control-label">REFERENCIA </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('referencia', null, ['placeholder' => 'referencia','class' => 'form-control', 'id' => 'referencia', 'style' => 'text-transform:uppercase', 'rows' => 4, 'readonly' => ($controller)?'true':'false']) !!}

                                    @if ($errors->has('referencia'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('referencia') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group{{ $errors->has('observacion') ? ' has-error' : '' }}">
                                <label for="observacion" class="col-md-2 control-label">JUSTIFICACIÓN </label>

                                <div class="col-md-10">
                                    {!! Form::textarea('observacion', null, ['placeholder' => 'justificación','class' => 'form-control', 'id' => 'observacion', 'style' => 'text-transform:uppercase', 'rows' => 4, 'readonly' => ($controller)?'true':'false']) !!}

                                    @if ($errors->has('observacion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('observacion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-4 col-md-offset-4">
                                <a href="{{ URL::previous() }}" class="btn btn-default">VOLVER</a>
                                {!! Form::hidden('imprimir', '0', array('id' => 'imprimir')) !!}
                                {!! Form::submit('GUARDAR CAMBIOS OCULTO', ['class' => 'btn btn-success btn-block', 'id' => 'btn-registro-solicitud', 'style' => 'display:none']) !!}
                                <button type="button" class="btn btn-success" onclick="verificaProyecto({!! $solicitud->usuario_id !!})">
                                    GUARDAR CAMBIOS
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <!-- MODAL -->
                @include('solicitudes.partial.modalVerificar');
                <!-- END MODAL -->
            </div>
        </div>
    </div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
     $(document).ready(function (e){
        total_precio();

        @if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4)
        $('.row_fr').css('display', 'table-cell');
        @endif
    });
    
    function eliFila(e){
        borrarFila(e);
        total_precio();
    }

    //Filas Tabla Items
    var contador = {{ count($solicitud->items) }};
    function verificaProyecto(id_usuario) {
        var proyecto_id = $('#proyecto').val();
        var nombre_proyecto = $('#proyecto').text();
        var url_usuario = '/json/existe_proyecto/'+id_usuario+'/'+proyecto_id;
        $.getJSON(url_usuario, function(cantidad) {
            datosModal();
            if(parseInt(cantidad['valor']) === 0){
                $('#guardar_btn').attr('disabled', 'true');
            }else{
                
                $('#guardar_btn').removeAttr('disabled');
            }
        });
    }
</script>
@endsection