@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
@endsection

@section('content')
<style>
    div#revisarModal .modal-lg{
        width: 1400px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                @php
                    $color = "#F39C12";
                    if(isset($_GET['modalidad'])){
                        $color = "#".\App\Modalidades::find($_GET['modalidad'])->color;
                    }
                    if(isset($_GET['tipo_fr'])){
                        $color = "#".\App\FondoRotativoTipos::find($_GET['tipo_fr'])->color;
                    }
                @endphp
                <div class="panel-heading" style="background: {{ $color }}">
                    <div class="col-md-4">
                        <h2 style='color: #FFF'>Aprobar Solicitudes Revisor</h2>
                    </div>
                    @php
                        $array_docs_adjuntos = array();
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    <div class="col-md-8">
                        @php
                        $array_data_get = ['SolicitudController@buscadorController', 'tabs=revisor'];
                        if(isset($_GET['modalidad'])){
                            $array_data_get['modalidad'] = $_GET['modalidad'];
                        }
                        if(isset($_GET['tipo_fr'])){
                            $array_data_get['tipo_fr'] = $_GET['tipo_fr'];
                        }
                        @endphp
                        {!! Form::open(array('action' => $array_data_get, 'method'=>'POST', 'class'=>'form-inline')) !!}
                            @if(isset($_GET['modalidad']))
                                {!! Form::hidden('moda_search', $_GET['modalidad'], ['class' => 'form-control', 'style' => 'text-transform:uppercase', 'id' => 'moda_search']) !!}
                            @endif
                            @if(isset($_GET['tipo_fr']))
                                {!! Form::hidden('tipo_search', $_GET['tipo_fr'], ['class' => 'form-control', 'style' => 'text-transform:uppercase', 'id' => 'tipo_search']) !!}
                            @endif
                            
                            @include('solicitudes.partial.buscador')
                        {!! Form::close() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    <table class="table table-bordered table-solicitudes table-hover">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>@sortablelink('estado', 'ESTADO')</th>
                                <th>@sortablelink('total', 'MONTO')</th>
                                <th class="proy_simple"><a class="fa fa-arrow-circle-right icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> @sortablelink('proyecto_id', 'PROYECTO')</th>
                                <th class="proy_cols"><a class="fa fa-arrow-circle-left icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> CENTRO DE COSTO</th>
                                <th class="proy_cols">GENERADOR DE COSTO</th>
                                <th class="proy_cols">ACTIVIDAD</th>
                                <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $fecha_actual = new DateTime(\Carbon\Carbon::now());
                            @endphp
                            @foreach($solicitudes as $solicitud)
                                @php
                                    $fecha_sol = new DateTime($solicitud->created_at);
                                    $fecha_diff = $fecha_sol->diff($fecha_actual)->format("%a");
                                    if($fecha_diff <= 7){
                                        $color = "#99ffbb";
                                    }elseif($fecha_diff > 7 && $fecha_diff <=14){
                                        $color = "#ffffcc";
                                    }elseif($fecha_diff > 14 && $fecha_diff <=30){
                                        $color = "#ffe6cc";
                                    }elseif($fecha_diff > 30){
                                        $color = "#ffb3b3";
                                    }
                                @endphp
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)" style="background-color: {{ $color }}">
                                    <td>
                                        {!! Form::open(['action' => ['SolicitudController@guardarAutorizacion', 'id_sol='.$solicitud->id], 'style' => 'display:none']) !!}
                                            {!! Form::hidden('id_sol', $solicitud->id, array('class' => 'form-control', 'readonly' => 'true')) !!}
                                            {!! Form::submit('Aprobar', ['class' => 'btn btn-success', 'id' => 'solicitud'.($loop->iteration-1)]) !!}
                                        {!! Form::close() !!}
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud"></a>
                                        <a class="fa fa-pencil-square icono-opciones-files icono-green" href="{{ route('solicitud.controller.editar', [$solicitud->id, true]) }}" title="Modificar Solicitud(Revisor)"></a>

                                        @if($solicitud->modalidad_id == 1 || $solicitud->modalidad_id == 2)
                                        <i class="fa fa-check-circle-o icono-opciones-files icono-green" data-toggle="modal" data-target="#aprobarModal" onclick="aprobarModal({{ $solicitud->id }})" title="Revisar Solicitud"></i>
                                        @else
                                        <i class="fa fa-check-circle-o icono-opciones-files icono-green" data-toggle="modal" data-target="#revisarModal" onclick="revisarModal({{ $solicitud->id }})" title="Revisar Solicitud"></i>
                                        @endif

                                        <i class="fa fa-question-circle-o icono-opciones-files icono-orange" data-toggle="modal" data-target="#observacionModal" onclick="observarSolicitud({{ $solicitud->id }})" title="Observar Solicitud"></i>
                                        <i class="fa fa-times-circle-o icono-opciones-files icono-red" data-toggle="modal" data-target="#rechazoModal" onclick="datosRechazoModal({{ $solicitud->id }})" title="Rechazar Solicitud"></i>
                                        @if($solicitud->estadoCodigo != 'REV-P')
                                            <a id="sol_pen_{{ $solicitud->id }}" class="fa fa-clock-o icono-opciones-files icono-green" title="Solicitud en proceso de revisión" data-id="{{ $solicitud->id }}" href="/solicitudes-controller/cambio/pendiente?pendiente={{ $solicitud->id }}"></a>
                                        @endif
                                    </td>
                                    <td id="solicitud_{{ $solicitud->id }}">{{ $solicitud->numero }}</td>
                                    <td>
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                            {{ $solicitud->estadoCodigo }}
                                        </span>
                                    </td>
                                    <td id="total_{{ $solicitud->id }}">{{ number_format($solicitud->total, 2, '.', ',') }}</td>
                                    <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}" class="proy_simple">
                                    <i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i>
                                </td>
                                <td class="proy_cols">
                                	@php
                                		switch($solicitud->proyecto->nivel){
                                			case 1:
                                				echo $solicitud->proyecto->nombre;
                                				break;
                                			case 2:
                                				echo $solicitud->proyecto->padre->nombre;
                                				break;
                                			case 3:
                                				echo $solicitud->proyecto->padre->padre->nombre;
                                				break;
                                			default:
                                		    	echo "";
                                		    	break;
                                		}
                                	@endphp
                                </td>
                                <td class="proy_cols">
                                	@php
                                		switch($solicitud->proyecto->nivel){
                                			case 2:
                                				echo $solicitud->proyecto->nombre;
                                				break;
                                			case 3:
                                				echo $solicitud->proyecto->padre->nombre;
                                				break;
                                		    default:
                                		    	echo "";
                                		    	break;
                                		}
                                	@endphp
                                </td>
                                <td class="proy_cols">{{ ($solicitud->proyecto->nivel == 3)?$solicitud->proyecto->nombre:"" }}</td>
                                    <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre }}</td>
                                    <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                    <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                    <td id="tipo_{{ $solicitud->id }}">
                                        {{ $solicitud->modalidad->nombre }}
                                        @php
                                            if($solicitud->modalidad_id == 3){
                                                echo " (".(\App\FondosRotativos::find(intval($solicitud->frtipo_id))->nombre).")";
                                            }
                                        @endphp
                                    </td>
                                    <td id="cambio_{{ $solicitud->id }}" style="text-align: right;">
                                        @if($solicitud->tipo_cambio->cambio != 1)
                                        {{ $solicitud->tipo_cambio->cambio }}
                                        @endif
                                    </td>
                                    <td id="moneda_{{ $solicitud->id }}">{{ $solicitud->moneda->nombre }}</td>
                                    <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-aprobar" class="btn btn-success btn-block" data-toggle="modal" data-target="#aprobarModal" style="display:none;">
                Modal aprobar
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <!-- OBSERVACION MODAL -->
            @include('solicitudes.partial.observacionSolicitud')
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL -->
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL RECHAZO-->
            <div class="modal fade" id="rechazoModal" tabindex="-1" role="dialog" aria-labelledby="rechazoModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="5" id='modalrechazo-proyecto'></td>
                                            <td><label for="turno">FECHA LÍMITE: </label></td>
                                            <td id="modalrechazo-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modalrechazo-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modalrechazo-desembolso'></td>
                                        </tr>
                                        <tr id="tr_con_tipo_rec">
                                            <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                            <td id='modalrechazo-cambio'></td>
                                            <td><label for="grupo">MODALIDAD: </label></td>
                                            <td colspan="3" id='modalrechazo-modalidad'></td>
                                            <td><label for="grupo">MONEDA: </label></td>
                                            <td colspan="3" id='modalrechazo-moneda'></td>
                                        </tr>
                                        <tr id="tr_sin_tipo_rec">
                                            <td><label >MODALIDAD: </label></td>
                                            <td colspan="4" id='modalrechazo-modalidad-sin'></td>
                                            <td><label >MONEDA: </label></td>
                                            <td colspan="4" id='modalrechazo-moneda-sin'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">REFERENCIA: </label></td>
                                            <td colspan="9" id='modalrechazo-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">OBSERVACIÓN: </label></td>
                                            <td colspan="9" id='modalrechazo-observacion'></td>
                                        </tr>
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-total'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>ITEMS SOLICITUD</h4>
                                        <table class="table table-bordered table-items" id="table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th style="width:20px">TIPO COMPRA</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal-rechazo"></span> CON <span id="modal-decimal-rechazo"></span>/100 <span id="modalrechazo-literal-moneda"></span></p>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_id')) !!}
                                    {!! Form::hidden('tipo', 'revisor', array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::textarea('motivo', null, ['placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform:uppercase', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-primary" onclick="rechazo()">RECHAZAR SOLICITUD</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->

            @if($modalidad == 1 || $modalidad == 2)
            <!-- AUTORIZAR MODAL -->
            @include('solicitudes.partial.aprobarSolicitud')
            <!-- END MODAL -->
            @else
            <!-- REVISAR FONDO ROTATIVO MODAL -->
            @include('solicitudes.partial.revisarFRSolicitud')
            <!-- END MODAL -->
            @endif

            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->

            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            <div class="modal fade" id="subirDocsModal" tabindex="-1" role="dialog" aria-labelledby="subirDocsModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>SUBIR DOCUMENTOS SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                                <div class="panel-body">
                                    {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                                        {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                                        <table class="table-modal-subir-archivo">
                                            <tr>
                                                <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                                <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                            </tr>
                                        </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
        </div>
    </div>
</div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    function aprobarSolicitud(e){        
        var obs = "SO";
        var url;
        if(document.getElementById('observa').checked){
            obs = document.getElementById('des_observacion').value;

            if(obs.trim() == ''){
                $('div#aprobarModal .help-block').css('color', 'red');
                $('div#aprobarModal .help-block strong').html('Debe especificar una observación');
            }else{
                url = '/solicitud-aprobacion-controller?id_sol='+e.id+"&obs="+obs;
                window.location.href = url;
            }
        }else{
            url = '/solicitud-aprobacion-controller?id_sol='+e.id+"&obs="+obs;
            window.location.href = url;
        }
    }

    function imprimirSolicitud(e){
        var id = e.id;
        //window.location.href = '/solicitud-imprimir?id_sol='+id;
    }
    function datosRechazoModal(fila){
        document.getElementById('tr_con_tipo_rec').style.display = 'contents';
        document.getElementById('tr_sin_tipo_rec').style.display = 'none';
        
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalrechazo-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalrechazo-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalrechazo-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalrechazo-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalrechazo-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        if(document.getElementById('moneda_'+fila).innerHTML == 'BOLIVIANOS'){
            document.getElementById('tr_con_tipo_rec').style.display = 'none';
            document.getElementById('tr_sin_tipo_rec').style.display = 'contents';
        }
        document.getElementById('modalrechazo-modalidad-sin').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda-sin').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalrechazo-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalrechazo-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('rechazoModalLabel').innerHTML = "<strong>RECHAZAR SOLICITUD# "+numeroSolicitud+"</strong>";
        document.getElementById('sol_id').value = fila;
        document.getElementById('modalrechazo-literal-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        var url = "/json/items_solicitud/" + fila;

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';
        if(p_decimal){
            str_decimal = p_decimal;
        }
        document.getElementById('modal-decimal-rechazo').innerHTML = str_decimal;

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-rechazo').innerHTML = str_literal.toUpperCase();

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalrechazo-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        $('textarea[name="motivo"]').val('');
    }

    function aprobarModal(fila){
        $('div#aprobarModal .help-block').css('color', 'red');
        $('div#aprobarModal .help-block strong').html('');

        $('#observa').prop('checked', false);
        document.getElementById('des_observacion').style.display = 'none';

        document.getElementById('tr_con_tipo_apr').style.display = 'contents';
        document.getElementById('tr_sin_tipo_apr').style.display = 'none';

        var modal_items_rendidos = document.getElementById('div_items_rendidos');
        modal_items_rendidos.style.display = "none";
        var div_antiguos = document.getElementById('div_items_antiguos');
        div_antiguos.setAttribute('style', 'display: none;');

        var id_fila = fila;
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalautorizar-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        //document.getElementById('modalautorizar-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
        document.getElementById('modalautorizar-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalautorizar-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalautorizar-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalautorizar-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        if(document.getElementById('moneda_'+fila).innerHTML == 'BOLIVIANOS'){
            document.getElementById('tr_con_tipo_apr').style.display = 'none';
            document.getElementById('tr_sin_tipo_apr').style.display = 'contents';
        }
        document.getElementById('modalautorizar-modalidad-sin').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda-sin').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalautorizar-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalautorizar-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('aprobarModalLabel').innerHTML = "<strong>REVISOR - REVISAR SOLICITUD# "+numeroSolicitud+"</strong>";
        document.getElementById('modal-moneda-aprobar').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        var span_p = document.getElementById('modalautorizaropciones');
        $('#modalautorizaropciones').empty();

        var btn_aprobar = document.createElement("button");
        btn_aprobar.setAttribute("type", "button");
        btn_aprobar.setAttribute("class", "btn btn-primary");
        btn_aprobar.setAttribute("style", "vertical-align: top;");
        btn_aprobar.setAttribute("id", fila);
        btn_aprobar.innerHTML = "REVISIÓN APROBADA";
        btn_aprobar.setAttribute("onclick", "aprobarSolicitud(this)");
        span_p.appendChild(btn_aprobar);
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("style", "vertical-align: top;");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        var url_solicitud = '/json/datos_solicitud/'+id_fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalautorizar-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-aprobar').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-aprobar').innerHTML = str_decimal;
    }

    function revisarModal(fila){
        $('div#revisarModal .help-block').css('color', 'red');
        $('div#revisarModal .help-block strong').html('');

        $('#observa').prop('checked', false);
        document.getElementById('des_observacion').style.display = 'none';

        document.getElementById('tr_con_tipo_apr').style.display = 'contents';
        document.getElementById('tr_sin_tipo_apr').style.display = 'none';

        var modal_items_rendidos = document.getElementById('div_items_rendidos');
        modal_items_rendidos.style.display = "none";
        var div_antiguos = document.getElementById('div_items_antiguos');
        div_antiguos.setAttribute('style', 'display: none;');

        var id_fila = fila;
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalautorizar-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        //document.getElementById('modalautorizar-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
        document.getElementById('modalautorizar-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalautorizar-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalautorizar-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalautorizar-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        if(document.getElementById('moneda_'+fila).innerHTML == 'BOLIVIANOS'){
            document.getElementById('tr_con_tipo_apr').style.display = 'none';
            document.getElementById('tr_sin_tipo_apr').style.display = 'contents';
        }
        document.getElementById('modalautorizar-modalidad-sin').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda-sin').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalautorizar-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalautorizar-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('revisarModalLabel').innerHTML = "<strong>REVISOR - REVISAR SOLICITUD# "+numeroSolicitud+"</strong>";
        document.getElementById('modal-moneda-aprobar').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        var span_p = document.getElementById('modalautorizaropciones');
        $('#modalautorizaropciones').empty();

        var btn_aprobar = document.createElement("button");
        btn_aprobar.setAttribute("type", "button");
        btn_aprobar.setAttribute("class", "btn btn-primary");
        btn_aprobar.setAttribute("style", "vertical-align: top;");
        btn_aprobar.setAttribute("id", fila);
        btn_aprobar.innerHTML = "REVISIÓN APROBADA";
        btn_aprobar.setAttribute("onclick", "aprobarSolicitud(this)");
        span_p.appendChild(btn_aprobar);
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("style", "vertical-align: top;");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        var url_solicitud = '/json/datos_solicitud/'+id_fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalautorizar-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var url = "/json/items_rendidos/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items_rendidos tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.fecha_factura+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.num_factura+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.proyecto+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.proveedor+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.tipo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items_rendidos tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='11' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items_rendidos tbody").append(id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-aprobar').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-aprobar').innerHTML = str_decimal;

        // DOCUMENTOS
        var url = "/json/documentos_solicitud/" + id_fila;
        $.getJSON(url, function(data) {
            $("#doc_table_revisor tbody").empty();
            docs = data;
            $.each(data, function (i, object) {
                $fila = "<tr>";
                $fila += "<td>"+object.id+"</td>";
                $fila += "<td>"+object.direccion+"</td>";
                $fila += "<td>";
                $fila += "<a href='/empresas/"+object.empresa_slug+"/"+object.gestion+"/solicitud/"+object.direccion+"' download='"+object.direccion+"'><i class='fa fa-download icono-opciones icono-blue' title='Descargar Documento' style= 'font-size: 18px!important;'></i></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-file-text icono-opciones icono-green' onclick='ver_documento_revisor(this, \""+object.empresa_slug+"\", "+ object.gestion +")' data='"+object.direccion+"' title='Ver Documento' style= 'font-size: 18px!important;'></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-eye-slash icono-opciones icono-green' onclick='cerrar_vista_revisor(this)' data='"+object.direccion+"' title='Cerrar Vista Previa' style= 'font-size: 18px!important;'></a>";
                $fila += "</td>";
                $fila += "</tr>";
                $("#doc_table_revisor tbody").append($fila);
            });
        });
    }

    function observarSolicitud(fila){
        document.getElementById('tr_con_tipo_obs').style.display = 'contents';
        document.getElementById('tr_sin_tipo_obs').style.display = 'none';

        var id_fila = fila;
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalobservacion-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalobservacion-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalobservacion-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalobservacion-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalobservacion-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalobservacion-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        if(document.getElementById('moneda_'+fila).innerHTML == 'BOLIVIANOS'){
            document.getElementById('tr_con_tipo_obs').style.display = 'none';
            document.getElementById('tr_sin_tipo_obs').style.display = 'contents';
        }
        document.getElementById('modalobservacion-modalidad-sin').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalobservacion-moneda-sin').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('modalobservacion-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalobservacion-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalobservacion-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalobservacion-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('observarModalLabel').innerHTML = "<strong>CONTROLLER - APROBAR SOLICITUD# "+numeroSolicitud+"</strong>";
        document.getElementById('modal-moneda-obs').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('id_rol').value = 4;
        document.getElementById('id_sol').value = id_fila;

        var span_p = document.getElementById('modalobservacionopciones');
        $('#modalobservacionopciones').empty();
        var file = document.createElement("button");
        file.setAttribute("type", "submit");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "OBSERVAR";
        //file.setAttribute("onclick", "realizaObservacionSolicitud(this)");
        span_p.appendChild(file);
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalobservacion-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-obs').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-obs').innerHTML = str_decimal;

        $('textarea[name="motivo"]').val('');
    }

    function rechazo(){
        document.getElementById('id_solicitud').click();
    }
    </script>
@endsection