@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-8">
                        <h2>
                            REPORTE REVISOR CANTIDAD: {{ count($solicitudes) }}
                            <div class="btn btn-success pull-right" id="btn-pdf"><span class="fa fa-file-excel-o"></span> EXPORTAR</div>
                        </h2>
                    </div>
                    {!! Form::open(['action' => 'ExcelController@generarExcelRevisor', 'method'=>'POST', 'class'=>'form-inline', 'id' => 'revExcelForm']) !!}
                        @foreach($excelsolicitudes as $exsol)
                                <input type="text" name="exsol_id[]" value="{{ $exsol }}" style="display: none;">
                        @endforeach
                    {!! Form::close() !!}
                    @php
                        $array_docs_adjuntos = array();
                        $contador = 1;
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    <div class="col-md-4 pull-right">
                        
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    {!! Form::open(['action' => 'ReportesController@reporteRevisor', 'method'=>'POST', 'class'=>'form-inline']) !!}
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group row col-md-12">
                                    {!! Form::label('fecha_ini', 'DESDE: ', ['class' => 'col-md-3']) !!}
                                    <div class="col-md-9">
                                        {!! Form::date('fecha_ini', null, ['id' => 'searchFechaIni', 'placeholder' => '', 'class' => 'form-control form-admin']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row col-md-12">
                                    {!! Form::label('fecha_fin', 'HASTA: ', ['class' => 'col-md-3']) !!}
                                    <div class="col-md-9">
                                        {!! Form::date('fecha_fin', null, ['id' => 'searchFechaFin', 'placeholder' => '', 'class' => 'form-control form-admin']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="center-block">
                                    {!! Form::submit('FILTRAR', ['class' => 'btn btn-success form-control btn-block btn-admin']) !!}
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <div class="div-solicitudes" style="overflow-x: scroll;">
                        <table class="table table-bordered table-solicitudes table-hover table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 90px">OPCIONES</th>
                                    <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                    <th style="width: 60px;">DOC.</th>
                                    <th>@sortablelink('created_at', 'FECHA')</th>
                                    <th>@sortablelink('estado', 'ESTADO')</th>
                                    <th>@sortablelink('total', 'MONTO')</th>
                                    <th>@sortablelink('empresa', 'EMPRESA')</th>
                                    <th>@sortablelink('proyecto_id', 'PROYECTO')</th>
                                    <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                    <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                                    <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                    <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                    <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                    <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                    <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $fecha_actual = new DateTime(\Carbon\Carbon::now());
                            @endphp
                            @foreach($solicitudes as $solicitud)
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)">
                                    <td>
                                        {!! Form::open(['action' => ['SolicitudController@guardarAutorizacion', 'id_sol='.$solicitud->id], 'style' => 'display:none']) !!}
                                            {!! Form::hidden('id_sol', $solicitud->id, array('class' => 'form-control', 'readonly' => 'true')) !!}
                                            {!! Form::submit('Aprobar', ['class' => 'btn btn-success', 'id' => 'solicitud'.($contador-1)]) !!}
                                        {!! Form::close() !!}
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="VER SOLICITUD"></a>
                                        <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="HISTORIAL SOLICITUD" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud"></a>
                                        @if(in_array($solicitud->estadoCodigo, ['REV', 'APR-P','REV-M']) && $user->rol_id > 4)
                                            <i class="fa fa-check-circle-o icono-opciones-files icono-green" data-toggle="modal" data-target="#aprobarModal" onclick="aprobarModal({{ $solicitud->id }})" title="APROBAR SOLICITUD"></i>
                                            <i class="fa fa-question-circle-o icono-opciones-files icono-orange" data-toggle="modal" data-target="#observacionModal" onclick="observarSolicitud({{ $solicitud->id }})" title="OBSERVAR SOLICITUD"></i>
                                            <i class="fa fa-times-circle-o icono-opciones-files icono-red" data-toggle="modal" data-target="#rechazoModal" onclick="datosRechazoModal({{ $solicitud->id }})" title="RECHAZAR SOLICITUD"></i>
                                        @endif
                                    </td>
                                    <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                    <td>
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                        @endif
                                    </td>
                                    <td id="fecha_{{ $solicitud->id }}" style="text-align: right;">{{ date_format(date_create($solicitud->created_at), 'd-m-Y') }}</td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                            {{ $solicitud->estadoCodigo }}
                                        </span>
                                    </td>
                                    <td id="total_{{ $solicitud->id }}" style="text-align: right;">{{ number_format($solicitud->total, '2', '.', ',') }}</td>
                                    <td id="empresa_{{ $solicitud->id }}">{{ $solicitud->proyecto->empresa->nombre }}</td>
                                    <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}"><i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i></td>
                                    <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
                                    <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                    <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                    <td id="cambio_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->tipo_cambio->cambio }}</td>
                                    <td id="moneda_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->moneda->sigla }}</td>
                                    <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                    <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-aprobar" class="btn btn-success btn-block" data-toggle="modal" data-target="#aprobarModal" style="display:none;">
                Modal aprobar
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <!-- OBSERVACION MODAL -->
            @include('solicitudes.partial.observacionSolicitud')
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL -->
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL RECHAZO-->
            <div class="modal fade" id="rechazoModal" tabindex="-1" role="dialog" aria-labelledby="rechazoModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">

                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="5" id='modalrechazo-proyecto'></td>
                                            <td><label for="turno">FECHA LÍMITE: </label></td>
                                            <td id="modalrechazo-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modalrechazo-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modalrechazo-desembolso'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                            <td id='modalrechazo-cambio'></td>
                                            <td><label for="grupo">MODALIDAD: </label></td>
                                            <td colspan="3" id='modalrechazo-modalidad'></td>
                                            <td><label for="grupo">MONEDA: </label></td>
                                            <td colspan="3" id='modalrechazo-moneda'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">REFERENCIA: </label></td>
                                            <td colspan="9" id='modalrechazo-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                                            <td colspan="9" id='modalrechazo-observacion'></td>
                                        </tr>
<!--                                        <tr class="no-print">
                                            <td class="text-right"><strong>DOCUMENTO(S)<br>ADJUNTO(S)</strong></td>
                                            <td colspan="9" id='modalrechazo-documentos'></td>
                                        </tr>-->
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-total'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>ITEMS SOLICITUD</h4>
                                        <table class="table table-bordered table-items" id="table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th style="width:20px">TIPO COMPRA</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal-rechazo"></span> CON <span id="modal-decimal-aprobar"></span>/100 BOLIVIANOS</p>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_id')) !!}
                                    {!! Form::hidden('tipo', 'Aprobar', array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::textarea('motivo', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform: uppercase;')) !!}
                                </div>
                            </div>
<!--                            <div class="text-center">
                                <input type="checkbox" name="correo" id="correo" value="correo" checked="checked">&nbsp;&nbsp;<label for='correo'>¿ENVIAR AL CORREO?</label>
                            </div>-->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-primary" onclick="rechazo()">RECHAZAR SOLICITUD</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- AUTORIZAR MODAL -->
            @include('solicitudes.partial.aprobarSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            <div class="modal fade" id="subirDocsModal" tabindex="-1" role="dialog" aria-labelledby="subirDocsModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>SUBIR DOCUMENTOS SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                                <div class="panel-body">
                                    {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                                        {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                                        <table class="table-modal-subir-archivo">
                                            <tr>
                                                <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                                <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                            </tr>
                                        </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
        </div>
    </div>
</div>
<input type="hidden" name="apro_id" id="apr_id" data-id="{{ $id_usuario }}" value="{{ $id_usuario }}">
<input type="hidden" name="soli_id" id="sol_id" data-id="{{ session('empresa') }}" value="{{ session('empresa') }}">
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    $('#btn-pdf').on('click', function(){
//        var values = $("input[name='exsol_id[]']")
//              .map(function(){return $(this).val();}).get();
//        window.location.href = '/exportar/excel/revisor?ids='+values;
        document.getElementById("revExcelForm").submit();
    });
</script>
@endsection