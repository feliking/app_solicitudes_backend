@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default panel-solicitud">
                <div class="panel-heading" style="background: #1ABB9C">
                    <div class="col-md-9">
                        <h2 style="color: #fff;">
                            REPORTE DE ACTIVOS FIJOS
                            <!-- <div class="btn btn-default pull-right" id="btn-pdf"><span class="fa fa-file-excel-o"></span> EXPORTAR</div> -->
                            {{-- @include('pdf.partial.input_reporte') --}}
                        </h2>
                        @foreach($items_ids as $item)
                            <input type="text" name="items_ids[]" value="{{ $item }}" style="display: none;">
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">

                    {!! Form::open(array('action' => 'ReportesController@reporteActivosFijos', 'method'=>'POST', 'class'=>'form-inline', 'id' => 'frm_sol_admin')) !!}
                        @include('solicitudes.reportes.filtros')
                    {!! Form::close() !!}
                    
                    <div class="row">
                        <div class="col-md-6">
                            {{ $items->appends($filters)->links() }}
                        </div>
                    </div>
                    
                    <div class="div-items" style="overflow-x: scroll;">
                        <table class="table table-bordered table-items table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 90px">OPCIONES</th>
                                    <th>NRO. SOL.</th>
                                    <th style="width: 60px;">DOC.</th>
                                    <th>FECHA FACTURA</th>
                                    <th>DESCRIPCIÓN ESPECÍFICA</th>
                                    <th>NÚMERO DE SERIE</th>
                                    <th>ASIGNACIÓN</th>
                                    <th>ACTIVIDAD ESPECÍFICA</th>
                                    <th>EMPRESA</th>
                                    <th>PROYECTO</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($items as $item)
                                <tr id="sol_fila_{{ $item->item_rendido->solicitud->id }}" ondblclick="seleccion_solicitud(this)">
                                    <td>
                                        <a id="sol_fila_{{ $item->item_rendido->solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        <a id="sol_hist_{{ $item->item_rendido->solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $item->item_rendido->solicitud->id }}" data-numero="{{ $item->numero }}" data-target="#historialSolicitud" data-toggle="modal"></a>
                                        
                                        <a id="sol_fila_{{ $item->item_rendido->solicitud->id }}" class="fa fa-file-o icono-opciones-files icono-green" href="/solicitud-imprimir/rendicion?id_sol={{ $item->item_rendido->solicitud->id }}" target="blank" title="Ver Rendición"></a>
                                            
                                        <a id="sol_fila_{{ $item->item_rendido->solicitud->id }}" class="fa fa-file-text-o icono-opciones-files icono-green" onclick="formularios_solicitud(this)" title="Ver Formularios AF"></a>
                                    </td>
                                    <td id="solicitud_{{ $item->item_rendido->solicitud->id }}" style="text-align: center;">{{ $item->item_rendido->solicitud->numero }}</td>
                                    <td>
                                        @if(sizeof($item->item_rendido->solicitud->documentos) > 0)
                                            <a id="{{ $item->item_rendido->solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $item->item_rendido->solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="Descargar Documento(s)"></i></a>
                                        @endif
                                        @if(sizeof($item->item_rendido->solicitud->documentosRendidos) > 0)
                                            <a id="{{ $item->item_rendido->solicitud->id }}" onclick="descarga_docs_rendidos(this)" data-numero="{{ $item->item_rendido->solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-orange" title="Descargar Documento(s)"></i></a>
                                        @endif
                                    </td>
                                    <td style="text-align: right;">
                                        {{ $item->item_rendido->fecha_factura }}
                                        <input type="hidden" id="total_{{ $item->item_rendido->solicitud_id }}"/>
                                    </td>

                                    <td>{{ $item->descripcion_especifica }}</td>
                                    <td>{{ $item->numero_serie }}</td>
                                    <td>{{ $item->asignacion }}</td>
                                    <td>{{ $item->actividad_especifica }}</td>
                                   
                                    <td id="empresa_{{ $item->item_rendido->solicitud->id }}" data-empresa='{{ $item->item_rendido->solicitud->proyecto->empresa->slug }}'>{{ $item->item_rendido->solicitud->proyecto->empresa->nombre }}</td>
                                    <td id="proyecto_td_{{$item->item_rendido->solicitud->id}}" title="{{ $item->nom_completo_proyecto }}"><i id='proyecto_{{ $item->item_rendido->solicitud->id }}'>{{$item->item_rendido->solicitud->proyecto->nombre}}</i></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $items->appends($filters)->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-solicitud-af" class="btn btn-success btn-block" data-toggle="modal" data-target="#formulariosSolicitud" style="display:none;">
                Modal solicitud
            </button>
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL FORMULARIO AF-->
            @include('solicitudes.partial.formAF')
            <!-- END MODAL -->
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
{{ Html::script('/js/main.js') }}
<script type="text/javascript">
    var config = {
        rutas:[
            {
                showCorrespon: "{{route('documentos.showcr', ['id'=>':id'])}}",
                descargar: "{{ route('doc.descargar', ['id'=>':id']) }}",
                storage: "{{ asset('storage/archivo') }}",
                token: "{{Session::token()}}"
            }
        ]
    };
    $('#btn-pdf').on('click', function(){
        var values = $("input[name='items_ids[]']")
              .map(function(){return $(this).val();}).get();
        window.location.href = '/exportar/excel?ids='+values;
    });
</script>
{{Html::script('/js/docs/documento_solicitud.js')}}
{{ Html::script('/js/admin.js') }}
@endsection