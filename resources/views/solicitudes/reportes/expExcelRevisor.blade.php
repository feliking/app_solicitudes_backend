<html>
    <tr>
        <td>EMPRESA</td>
        <td>PROYECTO</td>
        <td>SOLICITUD</td>
        <td>SOLICITANTE</td>
        <td>DESEMBOLSO</td>
        <td>REFERENCIA</td>
        <td>JUSTIFICACIÓN</td>
        <td>DESCRIPCIÓN</td>
        <td>TIPO DE COMPRA</td>
        <td>UNIDAD</td>
        <td>MODALIDAD</td>
        <td>FECHA SOLICITUD</td>
        <td>ESTADO</td>
        <td>CANTIDAD</td>
        <td>PRECIO UNITARIO</td>
        <td>TOTAL</td>
        <td>PROVEEDOR</td>
        <td>MONEDA</td>
        <td>T. CAMBIO</td>
        <td>AUTORIZADOR</td>
        <td>FECHA AUT.</td>
        <td>REVISOR</td>
        <td>FECHA REV.</td>
        <td>APROBADOR</td>
        <td>FECHA APR.</td>
    </tr>
    @if(count($solicitudes) > 1)
        @foreach($solicitudes as $solicitud)
            @php
                switch($solicitud->proyecto->nivel){
                    case 1:
                        $proyecto = $solicitud->proyecto->nombre;
                        break;
                    case 2:
                        $proyecto = $solicitud->proyecto->padre->nombre." -> ".$solicitud->proyecto->nombre;
                        break;
                    case 3:
                        $proyecto = $solicitud->proyecto->padre->padre->nombre." -> ".$solicitud->proyecto->padre->nombre." -> ".$solicitud->proyecto->nombre;
                        break;
                }
            @endphp
            @foreach($solicitud->items as $item)
                <tr>
                    <td>{{ $solicitud->proyecto->empresa->nombre }}</td>
                    <td>{{ $proyecto }}</td>
                    <td>{{ $solicitud->numero }}</td>
                    <td>{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
                    <td>{{ $solicitud->desembolso }}</td>
                    <td>{{ $solicitud->referencia }}</td>
                    <td>{{ $solicitud->observacion }}</td>
                    <td>{{ $item->detalle }}</td>
                    <td>{{ $item->tipo_compra->descripcion }}</td>
                    <td>{{ $item->unidad->descripcion }}</td>
                    <td>{{ $solicitud->modalidad->nombre }}</td>
                    <td>{{ date_format(date_create($solicitud->created_at), 'd-m-Y') }}</td>
                    <td>{{ $solicitud->estado }}</td>
                    <td>{{ $item->cantidad }}</td>
                    <td>{{ $item->costo }}</td>
                    <td>{{ $item->cantidad * $item->costo }}</td>
                    <td>{{ (isset($item->rendicion[0]))?$item->rendicion[0]->proveedor:'' }}</td>
                    <td>{{ $solicitud->moneda->nombre }}</td>
                    <td>{{ $solicitud->tipo_cambio->cambio }}</td>
                    <td>{{ strtoupper($solicitud->autorizador) }}</td>
                    <td>{{ $solicitud->fecha_aut }}</td>
                    <td>{{ strtoupper($solicitud->controller) }}</td>
                    <td>{{ $solicitud->fecha_rev }}</td>
                    <td>{{ strtoupper($solicitud->aprobador) }}</td>
                    <td>{{ $solicitud->fecha_apr }}</td>
                </tr>
            @endforeach
        @endforeach
    @endif
</html>