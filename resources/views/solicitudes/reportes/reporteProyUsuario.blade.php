@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-8">
                        <h2>
                            LISTADO EMPRESAS/PROYECTOS/USUARIO/AUTORIZADORES
                        </h2>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    <div class="div-solicitudes" style="overflow-x: scroll;">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Nº</th>
                                    <th>EMPRESA</th>
                                    <th>PROYECTO</th>
                                    <th>USUARIO</th>
                                    <th>USER</th>
                                    <th>AUTORIZADOR</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($proyectos as $proyecto)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $proyecto->nomEmpresa }}</td>
                                    <td>{{ $proyecto->nom_completo_proyecto }}</td>
                                    <td>{{ $proyecto->nomUsuario }}</td>
                                    <td>{{ $proyecto->usuario }}</td>
                                    <td>{{ implode("\n ", $proyecto->autorizadores) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">

</script>
@endsection