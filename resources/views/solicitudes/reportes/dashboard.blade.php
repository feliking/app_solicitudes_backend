@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-3">
                        <h2>Dashboard</h2>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-3"></div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    @foreach($empresas as $empresa)
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5>{{ $empresa->nombre }}</h4>
                            </div>
                            <div class="panel-body">
                                <h6>Cuentas: {{ count($empresa->cuentas) }}</h6>
                                <h6>Solicitudes: </h6>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">

</script>
@endsection