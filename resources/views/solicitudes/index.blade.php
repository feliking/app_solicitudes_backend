@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
@endsection
@section('content')
@php
if(!isset($array_addquery))
    $array_addquery = '';
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default no-print">
                <div class="panel-heading" style="background: #{{ $extras[1] }}">
                    <div class="col-md-2">
                        <h2 id="tituloTipoSolicitud" style='color: #{{ $extras[0] }}'>{{ $titulo }}</h2>
                    </div>
                    <!-- SEARCH ENGINE -->
                    <div class="col-md-8">
                        @if(isset($_GET['t']))
                        {!! Form::open(array('route' => ['solicitudes', 't' => $_GET['t']], 'method'=>'POST', 'class'=>'form-inline')) !!}
                            @include('solicitudes.partial.buscador')
                        @else
                        {!! Form::open(array('route' => 'solicitudes', 'method'=>'POST', 'class'=>'form-inline')) !!}
                            @include('solicitudes.partial.buscador')
                        @endif
                        {!! Form::close() !!}
                        @if(isset($_GET['ordenar']))
                            <p style="font-size:12px; margin-bottom: 0px; padding-bottom: 0px; text-align: center;">ORDENADO POR: {{ strtoupper($_GET['ordenar']) }} ({{ $orden_type }})</p>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <div class="crear_empresa pull-right">
                            <a class="btn btn-default" href="{{ route('solicitud.create') }}">NUEVA SOLICITUD</a>
                        </div>
                    </div>
                    <!-- END SEARCH ENGINE -->
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body panel-solicitud">
                    {{ $solicitudes->links() }}
                    <table class="table table-bordered table-hover table-solicitudes">
                        <thead>
                            <tr>
                                <th>OPCIONES</th>
                                <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                <th>@sortablelink('created_at', 'FECHA')</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>@sortablelink('estado', 'ESTADO')</th>
                                <th>@sortablelink('total', 'MONTO')</th>
                                <th class="proy_simple"><a class="fa fa-arrow-circle-right icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> @sortablelink('proyecto_id', 'PROYECTO')</th>
                                <th class="proy_cols"><a class="fa fa-arrow-circle-left icono-opciones-files icono-green" onclick="cambio_proy()" title="Ver Solicitud"></a> CENTRO DE COSTO</th>
                                <th class="proy_cols">GENERADOR DE COSTO</th>
                                <th class="proy_cols">ACTIVIDAD</th>
                                <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($solicitudes as $solicitud)
                                @if ($solicitud->modalidad_id == 5)
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)" class="info">
                                @else
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)">
                                @endif
                                    <td>
                                        @if ($solicitud->modalidad_id == 5)
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud_transferencia(this)" title="Ver Solicitud"></a>
                                        @else
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        @endif
                                        <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-target="#historialSolicitud" data-toggle="modal"></a>
                                        @php
                                            $usuario = auth()->user();

                                            $ultimo_estado = $solicitud->estados->last();
                                        @endphp

                                        @if ($solicitud->modalidad_id == 5 && $solicitud->usuario_id == \Auth::user()->id)
                                        <i class="fa fa-times-circle-o icono-opciones-files icono-red" data-toggle="modal" data-target="#rechazoModalTransferencia" onclick="datosRechazoModalTransferencia({{ $solicitud->id }})" title="Anular Solicitud"></i>
                                        @elseif ($solicitud->usuario_id == \Auth::user()->id && $ultimo_estado->usuario_id == \Auth::user()->id && in_array($solicitud->estadoCodigo, ['AUT', 'AUT-M', 'AUT-AF']))
                                        <i class="fa fa-times-circle-o icono-opciones-files icono-red" data-toggle="modal" data-target="#rechazoModal" onclick="datosRechazoModal({{ $solicitud->id }})" title="Rechazar Solicitud"></i>
                                        @endif

                                        @if((in_array($solicitud->estadoCodigo, ['SOL', 'SOL-M', 'AUT-O', 'REV-O', 'APR-O'])) && $usuario->id == $solicitud->usuario_id)
                                            <a class="fa fa-edit icono-opciones-files icono-green" href="{{ route('solicitud.edit', $solicitud->id) }}" title="Modificar Solicitud"></a>
                                        @elseif($usuario->rol_id == 1)
                                            <a class="fa fa-edit icono-opciones-files icono-green" href="{{ route('solicitud.edit', $solicitud->id) }}" title="Modificar Solicitud"></a>
                                        @endif
                                        
                                        @if((count($solicitud->itemsRendidos) > 0 && $solicitud->estados->last()->estado_id > 50) || count($solicitud->devoluciones) > 0)
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-o icono-opciones-files icono-green" href="/solicitud-imprimir/rendicion?id_sol={{ $solicitud->id }}" target="blank" title="Ver Rendición"></a>
                                        @endif
                                        @if($solicitud->formulariosAF)
                                            <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text-o icono-opciones-files icono-green" onclick="formularios_solicitud(this)" title="Ver Formularios AF"></a>
                                        @endif
                                    </td>
                                    <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                        <span id="nom_proy_{{ $solicitud->id }}" style="display:none;"></span>
                                        <span id="nom_subproy_{{ $solicitud->id }}" style="display:none;"></span>
                                    </td>
                                    <td>
                                        {{ $solicitud->created_at }}
                                    </td>
                                    <td>
                                        @if((in_array($solicitud->estadoCodigo, ['SOL', 'SOL-M', 'AUT-O', 'REV-O', 'APR-O'])) && $usuario->id == $solicitud->usuario_id)
                                            <a id="{{ $solicitud->id }}" onclick="subir_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-upload icono-opciones-files icono-green" title="SUBIR DOCUMENTO(S)"></i></a>&nbsp;
                                        @elseif($usuario->rol_id == 1)
                                            <a id="{{ $solicitud->id }}" onclick="subir_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-upload icono-opciones-files icono-green" title="SUBIR DOCUMENTO(S)"></i></a>&nbsp;
                                        @endif
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="Descargar Documento(s)"></i></a>
                                        @endif
                                        @if(sizeof($solicitud->documentosRendidos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs_rendidos(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-orange" title="Descargar Documento(s)"></i></a>
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                            {{ $solicitud->estadoCodigo }}
                                        </span>
                                    </td>
                                    <td id="total_{{ $solicitud->id }}" style="text-align: right;">{{ number_format($solicitud->total, '2', '.', ',') }}</td>
                                    <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}" class="proy_simple">
                                        <i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i>
                                    </td>
                                    <td class="proy_cols">
                                    	@php
                                    		switch($solicitud->proyecto->nivel){
                                    			case 1: 
                                    				echo $solicitud->proyecto->nombre;
                                    				break;
                                    			case 2: 
                                    				echo $solicitud->proyecto->padre->nombre;
                                    				break;
                                    			case 3: 
                                    				echo $solicitud->proyecto->padre->padre->nombre;
                                    				break;
                                    			default:
                                    		    	echo "";
                                    		    	break;	
                                    		}
                                    	@endphp
                                    </td>
                                    <td class="proy_cols">
                                    	@php
                                    		switch($solicitud->proyecto->nivel){
                                    			case 2: 
                                    				echo $solicitud->proyecto->nombre;
                                    				break;
                                    			case 3: 
                                    				echo $solicitud->proyecto->padre->nombre;
                                    				break;	
                                    		    default:
                                    		    	echo "";
                                    		    	break;
                                    		}
                                    	@endphp
                                    </td>
                                    <td class="proy_cols">{{ ($solicitud->proyecto->nivel == 3)?$solicitud->proyecto->nombre:"" }}</td>
                                    <td id="usuario_{{ $solicitud->id }}">
                                        {{ $solicitud->usuario->nombre }}
                                    </td>
                                    <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                    <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                    <td id="cambio_{{ $solicitud->id }}" style="text-align: right;">
                                        @if($solicitud->tipo_cambio->cambio != 1)
                                        {{ $solicitud->tipo_cambio->cambio }}
                                        @endif
                                    </td>
                                    <td id="tipo_{{ $solicitud->id }}">
                                        {{ $solicitud->modalidad->nombre }}
                                        @php
                                            if($solicitud->modalidad_id == 3){
                                                echo " (".(\App\FondosRotativos::find(intval($solicitud->frtipo_id))->nombre).")";
                                            }
                                        @endphp
                                    </td>
                                    <td id="moneda_{{ $solicitud->id }}">{{ $solicitud->moneda->nombre }}</td>
                                    <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->links() }}
                </div>
            </div>
            <!-- MODAL -->
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-solicitud-transferencia" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModalTransferencia" style="display:none">
                Modal solicitud transferecnia
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <button type="button" id="btn-modal-solicitud-af" class="btn btn-success btn-block" data-toggle="modal" data-target="#formulariosSolicitud" style="display:none;">
                Modal solicitud
            </button>
            @include('solicitudes.partial.datosSolicitud')
            @include('solicitudes.partial.datosSolicitudTransferencia')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            @include('solicitudes.partial.docsUp')
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL FORMULARIO AF-->
            @include('solicitudes.partial.formAF')
            <!-- END MODAL -->

            <!-- MODAL RECHAZO-->
            <div class="modal fade" id="rechazoModal" tabindex="-1" role="dialog" aria-labelledby="rechazoModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default " style="overflow-x: scroll;">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="5" id='modalrechazo-proyecto'></td>
                                            <td><label for="turno">FECHA LÍMITE: </label></td>
                                            <td id="modalrechazo-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modalrechazo-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label >DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modalrechazo-desembolso'></td>
                                        </tr>
                                        <tr id="tr_con_tipo_rec">
                                            <td><label >TIPO DE CAMBIO: </label></td>
                                            <td id='modalrechazo-cambio'></td>
                                            <td><label >MODALIDAD: </label></td>
                                            <td colspan="3" id='modalrechazo-modalidad'></td>
                                            <td><label >MONEDA: </label></td>
                                            <td colspan="3" id='modalrechazo-moneda'></td>
                                        </tr>
                                        <tr id="tr_sin_tipo_rec">
                                            <td><label >MODALIDAD: </label></td>
                                            <td colspan="4" id='modalrechazo-modalidad-sin'></td>
                                            <td><label >MONEDA: </label></td>
                                            <td colspan="4" id='modalrechazo-moneda-sin'></td>
                                        </tr>
                                        <tr>
                                            <td><label >REFERENCIA: </label></td>
                                            <td colspan="9" id='modalrechazo-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label >JUSTIFICACIÓN: </label></td>
                                            <td colspan="9" id='modalrechazo-observacion'></td>
                                        </tr>
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-total'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>ITEMS SOLICITUD</h4>
                                        <table class="table table-bordered table-items" id="table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th style="width:20px">TIPO COMPRA</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal-rechazo"></span> CON <span id="modal-decimal-rechazo"></span>/100 <span id="modalrechazo-literal-moneda"></span></p>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_id')) !!}
                                    {!! Form::hidden('tipo', 'autorizar', array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::textarea('motivo', null, ['placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform:uppercase', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-primary" onclick="rechazo()">RECHAZAR SOLICITUD</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="rechazoModalTransferencia" tabindex="-1" role="dialog" aria-labelledby="rechazoModalTransferenciaLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalTransferenciaLabel"><strong>ANULAR SOLICITUD DE TRANSFERENCIA</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default " style="overflow-x: scroll;">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD DE TRANSFERENCIA</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered" id="reporte-trs-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="5" id='modalrechazo-trs-proyecto'></td>
                                            <td><label for="turno">FECHA LÍMITE: </label></td>
                                            <td id="modalrechazo-trs-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modalrechazo-trs-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label >DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modalrechazo-trs-desembolso'></td>
                                        </tr>
                                        <tr id="tr_con_tipo_rec">
                                            <td><label >TIPO DE CAMBIO: </label></td>
                                            <td id='modalrechazo-trs-cambio'></td>
                                            <td><label >MODALIDAD: </label></td>
                                            <td colspan="3" id='modalrechazo-trs-modalidad'></td>
                                            <td><label >MONEDA: </label></td>
                                            <td colspan="3" id='modalrechazo-trs-moneda'></td>
                                        </tr>
                                        <tr id="tr_sin_tipo_rec">
                                            <td><label >MODALIDAD: </label></td>
                                            <td colspan="4" id='modalrechazo-trs-modalidad-sin'></td>
                                            <td><label >MONEDA: </label></td>
                                            <td colspan="4" id='modalrechazo-trs-moneda-sin'></td>
                                        </tr>
                                        <tr>
                                            <td><label >REFERENCIA: </label></td>
                                            <td colspan="9" id='modalrechazo-trs-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label >JUSTIFICACIÓN: </label></td>
                                            <td colspan="9" id='modalrechazo-trs-observacion'></td>
                                        </tr>
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-trs-total'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>DETALLE DE TRANSFERENCIA</h4>
                                        <table class="table table-bordered table-items" id="table_trs_items">
                                            <tbody id='modal-trs-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal-trs-rechazo"></span> CON <span id="modal-decimal-trs-rechazo"></span>/100 <span id="modalrechazo-trs-literal-moneda"></span></p>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE ANULACIÓN</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_trs_id')) !!}
                                    {!! Form::hidden('tipo', 'autorizar', array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::textarea('motivo', null, ['placeholder' => '','class' => 'form-control', 'rows' => '3', 'minlength' => '5', 'maxlength' => '500', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform:uppercase', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-primary" onclick="rechazo()">ANULAR SOLICITUD</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
{{ Html::script('/js/main.js') }}
<script type="text/javascript">
        var config = {
            rutas:[
                {
                    showCorrespon: "{{route('documentos.showcr', ['id'=>':id'])}}",
                    descargar: "{{ route('doc.descargar', ['id'=>':id']) }}",
                    storage: "{{ asset('storage/archivo') }}",
                    token: "{{Session::token()}}"
                }
            ]
        };


    function datosRechazoModal(fila){
        document.getElementById('tr_con_tipo_rec').style.display = 'contents';
        document.getElementById('tr_sin_tipo_rec').style.display = 'none';

        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalrechazo-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        //document.getElementById('modalrechazo-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
        document.getElementById('modalrechazo-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalrechazo-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalrechazo-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalrechazo-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        if(document.getElementById('moneda_'+fila).innerHTML == 'BOLIVIANOS'){
            document.getElementById('tr_con_tipo_rec').style.display = 'none';
            document.getElementById('tr_sin_tipo_rec').style.display = 'contents';
        }
        document.getElementById('modalrechazo-modalidad-sin').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda-sin').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalrechazo-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalrechazo-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('rechazoModalLabel').innerHTML = "<strong>RECHAZO DE SOLICITUD #"+numeroSolicitud+"</strong>";
        document.getElementById('sol_id').value = fila;

        document.getElementById('modalrechazo-literal-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        var url = "/json/items_solicitud/" + fila;

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';
        if(p_decimal){
            str_decimal = p_decimal;
        }
        document.getElementById('modal-decimal-rechazo').innerHTML = str_decimal;

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-rechazo').innerHTML = str_literal.toUpperCase();

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalrechazo-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        $('textarea[name="motivo"]').val('');
    }

    function datosRechazoModalTransferencia(fila){
        document.getElementById('tr_con_tipo_rec').style.display = 'contents';
        document.getElementById('tr_sin_tipo_rec').style.display = 'none';

        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalrechazo-trs-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        //document.getElementById('modalrechazo-trs-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        if(document.getElementById('moneda_'+fila).innerHTML == 'BOLIVIANOS'){
            document.getElementById('tr_con_tipo_rec').style.display = 'none';
            document.getElementById('tr_sin_tipo_rec').style.display = 'contents';
        }
        document.getElementById('modalrechazo-trs-modalidad-sin').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-moneda-sin').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        document.getElementById('modalrechazo-trs-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalrechazo-trs-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('rechazoModalTransferenciaLabel').innerHTML = "<strong>ANULAR SOLICITUD #"+numeroSolicitud+"</strong>";
        document.getElementById('sol_trs_id').value = fila;

        document.getElementById('modalrechazo-trs-literal-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        var url = "/json/items_solicitud/" + fila;

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';
        if(p_decimal){
            str_decimal = p_decimal;
        }
        document.getElementById('modal-decimal-trs-rechazo').innerHTML = str_decimal;

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-trs-rechazo').innerHTML = str_literal.toUpperCase();

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalrechazo-trs-fecha-lim').innerHTML = datos.fecha_limite;

            $("#table_trs_items tbody").empty();
            fila_th = "<tr>";
                fila_th += "<th>Cuenta Bancaria Origen</th>";
                fila_th += "<th>Tipo de Transacción</th>";
                fila_th += "<th>Monto de Transferencia</th>";
            fila_th += "</tr>";

            fila_th += "<tr>";
                fila_th += "<td>" + datos.banco_origen.nombre + " (CTA. " + datos.cuenta_origen.numero + ")</td>";
                fila_th += "<td>[" + datos.tipo_transferencia.codigo + "] " + datos.tipo_transferencia.descripcion + "</td>";
                fila_th += "<td>" + datos.monto + "</td>";
            fila_th += "</tr>";

            fila_th += "<tr>";
                fila_th += "<th>Empresa Destino</th>";
                fila_th += "<th>Cuenta Bancaria Destino</th>";
                fila_th += "<th>Factor de conversión</th>";
            fila_th += "</tr>";

            fila_th += "<tr>";
                fila_th += "<td>" + datos.empresa_destino.nombre + "</td>";
                fila_th += "<td>" + datos.banco_destino.nombre + " (CTA. " + datos.cuenta_destino.numero + ")</td>";
                fila_th += "<td>" + datos.factor + "</td>";
            fila_th += "</tr>";
            $("#table_trs_items tbody").append(fila_th);
        });

        $('textarea[name="motivo"]').val('');
    }

</script>
{{Html::script('/js/docs/documento_solicitud.js')}}

@endsection
