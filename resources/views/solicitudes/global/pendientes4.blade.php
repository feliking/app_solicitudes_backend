@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default panel-solicitud">
                <div class="panel-heading" style="background: #FFCC66">
                    <div class="col-md-9">
                        <h2 style="color: #fff;">
                            SOLICITUDES PENDIENTES 4
                            <div class="btn btn-default pull-right" id="btn-pdf"><span class="fa fa-file-excel-o"></span> EXPORTAR</div>
                            @include('pdf.partial.input_reporte')
                        </h2>
                        @foreach($excelsolicitudes as $exsol)
                            <input type="text" name="exsol_id[]" value="{{ $exsol }}" style="display: none;">
                        @endforeach
                    </div>
                    @php
                        $array_docs_adjuntos = array();
                        $contador = 1;
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    <div class="col-md-3 pull-right">
                        @foreach($todasmonedas as $moneda)
                            <h2 style="color: #{{ $moneda->color }}">TOTAL ({{ $moneda->sigla }}): {{ number_format($totalmonedas[$moneda->id], '2', '.', ',') }}</h2>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    {!! Form::open(array('action' => 'SolicitudGlobalController@pendientes4', 'method'=>'POST', 'class'=>'form-inline', 'id' => 'frm_sol_admin')) !!}
                        <input id="consulta" type="hidden" name="consulta" value="{{ $con }}">
                        @include('solicitudes.admin.filtros')
                    {!! Form::close() !!}
                    
                    <div class="row">
                        <div class="col-md-6">
                            {{ $solicitudes->appends($filters)->links() }}
                        </div>
                        <div class="col-md-6 text-right">
                            <label for="chk_fr_cc" style="margin: 25px 0;">VER SOLO FONDOS ROTATIVOS, CAJAS CHICAS Y REPOSICIONES</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @if(isset($filters['chk_fr_cc']))
                            <input form="frm_sol_admin" name="chk_fr_cc" type="checkbox" value="view_fr_cc" id="chk_fr_cc" onchange="this.form.submit()" checked/>
                            @else
                            <input form="frm_sol_admin" name="chk_fr_cc" type="checkbox" value="view_fr_cc" id="chk_fr_cc" onchange="this.form.submit()"/>
                            @endif
                        </div>
                    </div>
                    
                    <div class="div-solicitudes" style="overflow-x: scroll;">
                        <table class="table table-bordered table-solicitudes table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 90px">OPCIONES</th>
                                    <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                    <th style="width: 60px;">DOC.</th>
                                    <th>@sortablelink('created_at', 'FECHA')</th>
                                    <th>@sortablelink('estado', 'ESTADO')</th>
                                    <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                    <th>@sortablelink('total', 'MONTO')</th>
                                    <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                    <th>@sortablelink('empresa', 'EMPRESA')</th>
                                    <th>@sortablelink('proyecto_id', 'PROYECTO')</th>
                                    <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                    <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                    <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                    <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                    <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $fecha_actual = new DateTime(\Carbon\Carbon::now());
                            @endphp
                            @foreach($solicitudes as $solicitud)
                                @php
                                    $fecha_sol = new DateTime($solicitud->created_at);
                                    $fecha_diff = $fecha_sol->diff($fecha_actual)->format("%a");
                                    if($fecha_diff <= 3){
                                        $color = "#99ffbb";
                                    }elseif($fecha_diff > 3 && $fecha_diff <=7){
                                        $color = "#ffffcc";
                                    }elseif($fecha_diff > 7 && $fecha_diff <=14){
                                        $color = "#ffe6cc";
                                    }elseif($fecha_diff > 14){
                                        $color = "#ffb3b3";
                                    }
                                @endphp
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)" style="background-color: {{ $color }};">
                                    <td>
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud"></a>
                                    </td>
                                    <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                    <td>
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                        @endif
                                    </td>
                                    <td id="fecha_{{ $solicitud->id }}" style="text-align: right;">{{ $solicitud->created_at }}</td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                            {{ $solicitud->estadoCodigo }}
                                        </span>
                                    </td>
                                    <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                    <td id="total_{{ $solicitud->id }}" style="text-align: right; color: #{{ $solicitud->moneda->color }}; font-weight: bold; font-size: 13px;">{{ number_format($solicitud->total, '2', '.', ',') }}</td>
                                    <td id="moneda_{{ $solicitud->id }}" style="text-align: center; color: #{{ $solicitud->moneda->color }}; font-weight: bold">{{ $solicitud->moneda->sigla }}</td>
                                    <td id="empresa_{{ $solicitud->id }}" data-empresa='{{ $solicitud->proyecto->empresa->slug }}'>{{ $solicitud->proyecto->empresa->nombre }}</td>
                                    <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}"><i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i></td>
                                    <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
                                    <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                    <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                    <td id="cambio_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->tipo_cambio->cambio }}</td>
                                    <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $solicitudes->appends($filters)->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL -->
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
        </div>
    </div>
</div>
<input type="hidden" name="apro_id" id="apr_id" data-id="{{ $id_usuario }}" value="{{ $id_usuario }}">
<input type="hidden" name="soli_id" id="sol_id" data-id="{{ session('empresa') }}" value="{{ session('empresa') }}">
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    $('li#global_pendientes').addClass('current-page')
        .parent().css('display', 'block')
        .parent().addClass('active');

    @if(!isset($filters['campo']) || $filters['campo'] == 0)
        $('#formField').get(0).selectedIndex = 0;

        document.getElementById("searchTexto").value = "";
        document.getElementById("searchTexto").disabled = true;
    @endif

    @if(isset($filters['empresa_id']) && $filters['empresa_id'] != 0)
    empresaSelect();
    @endif

    function formFields(){
        var selOption = document.getElementById('formField');
        var valor = parseInt(selOption[selOption.selectedIndex].value);
        
        if(valor != 0){
            document.getElementById("searchTexto").disabled = false;
        }else{
            document.getElementById("searchTexto").disabled = true;
            document.getElementById("searchTexto").value = "";
        }
    }

    function empresaSelect(){
        var selOption = document.getElementById('empresa_id');
        var valor = parseInt(selOption[selOption.selectedIndex].value);

        @if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0)
        var oldProy = {{ $filters['proyecto_id'] }};
        @else
        var oldProy = 0;
        @endif

        if(valor != 0){
            var url = "/json/proyectos/empresa/" + valor;
            var cad = "<option value='0'>TODOS LOS PROYECTOS</option>";
            $.getJSON(url, function(data) {
                document.getElementById('proyecto_id').innerHTML = '';
                $.each(data, function (i, object) {
                    if(oldProy != 0 && oldProy == object.id){
                        cad += "<option value="+object.id+" selected='selected'>"+object.nombre+"</option>";
                    }else{
                        cad += "<option value="+object.id+">"+object.nombre+"</option>";
                    }
                });    
                document.getElementById('proyecto_id').innerHTML = cad;
            });

            document.getElementById("proyecto_id").disabled = false;
        }else{
            document.getElementById("proyecto_id").disabled = true;
            document.getElementById('proyecto_id').innerHTML = "<option value='0'>NINGUNO</option>";
        }
    }

    $('#btn-pdf').on('click', function(){
        var values = $("input[name='exsol_id[]']")
              .map(function(){return $(this).val();}).get();
        window.location.href = '/exportar/excel?ids='+values;
    });
</script>
{{ Html::script('/js/admin.js') }}
@endsection