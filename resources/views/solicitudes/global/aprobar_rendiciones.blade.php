@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default panel-solicitud">
                <div class="panel-heading" style="background: #1ABB9C">
                    <div class="col-md-9">
                        <h2 style="color: #fff;">
                            RENDICIONES A SER APROBADAS
                            <div class="btn btn-default pull-right" id="btn-pdf"><span class="fa fa-file-excel-o"></span> EXPORTAR</div>
                            @include('pdf.partial.input_reporte')
                        </h2>
                        @foreach($excelsolicitudes as $exsol)
                            <input type="text" name="exsol_id[]" value="{{ $exsol }}" style="display: none;">
                        @endforeach
                    </div>
                    @php
                        $array_docs_adjuntos = array();
                        $contador = 1;
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    <div class="col-md-3 pull-right">
                        @foreach($todasmonedas as $moneda)
                            <h2 style="color: #{{ $moneda->color }}">TOTAL ({{ $moneda->sigla }}): {{ number_format($totalmonedas[$moneda->id], '2', '.', ',') }}</h2>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    {!! Form::open(array('action' => 'SolicitudGlobalController@aprobar_rendiciones', 'method'=>'POST', 'class'=>'form-inline', 'id' => 'frm_sol_admin')) !!}
                        <input id="consulta" type="hidden" name="consulta" value="{{ $con }}">
                        @include('solicitudes.admin.filtros')
                    {!! Form::close() !!}
                    
                    <div class="row">
                        <div class="col-md-6">
                            {{ $solicitudes->appends($filters)->links() }}
                        </div>
                        <div class="col-md-6 text-right">
                            <label for="chk_fr_cc" style="margin: 25px 0;">VER SOLO FONDOS ROTATIVOS, CAJAS CHICAS Y REPOSICIONES</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @if(isset($filters['chk_fr_cc']))
                            <input form="frm_sol_admin" name="chk_fr_cc" type="checkbox" value="view_fr_cc" id="chk_fr_cc" onchange="this.form.submit()" checked/>
                            @else
                            <input form="frm_sol_admin" name="chk_fr_cc" type="checkbox" value="view_fr_cc" id="chk_fr_cc" onchange="this.form.submit()"/>
                            @endif
                        </div>
                    </div>
                    
                    <div class="div-solicitudes" style="overflow-x: scroll;">
                        <table class="table table-bordered table-solicitudes table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 90px">OPCIONES</th>
                                    <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                    <th style="width: 60px;">DOC.</th>
                                    <th>@sortablelink('created_at', 'FECHA')</th>
                                    <th>@sortablelink('estado', 'ESTADO')</th>
                                    <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                    <th>@sortablelink('total', 'MONTO')</th>
                                    <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                    <th>@sortablelink('empresa', 'EMPRESA')</th>
                                    <th>@sortablelink('proyecto_id', 'PROYECTO')</th>
                                    <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                    <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                    <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                    <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                    <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $fecha_actual = new DateTime(\Carbon\Carbon::now());
                            @endphp
                            @foreach($solicitudes as $solicitud)
                                @php
                                    $fecha_sol = new DateTime($solicitud->created_at);
                                    $fecha_diff = $fecha_sol->diff($fecha_actual)->format("%a");
                                    if($fecha_diff <= 3){
                                        $color = "#99ffbb";
                                    }elseif($fecha_diff > 3 && $fecha_diff <=7){
                                        $color = "#ffffcc";
                                    }elseif($fecha_diff > 7 && $fecha_diff <=14){
                                        $color = "#ffe6cc";
                                    }elseif($fecha_diff > 14){
                                        $color = "#ffb3b3";
                                    }
                                @endphp
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)" style="background-color: {{ $color }};">
                                    <td>
                                        {!! Form::open(['action' => ['SolicitudController@guardarAutorizacion', 'id_sol='.$solicitud->id], 'style' => 'display:none']) !!}
                                            {!! Form::hidden('id_sol', $solicitud->id, array('class' => 'form-control', 'readonly' => 'true')) !!}
                                            @if(isset($_GET['page']))
                                                {{ Form::hidden('page', $_GET['page'], ['class' => 'form-control']) }}
                                            @endif
                                            {!! Form::submit('Autorizar', ['class' => 'btn btn-success', 'id' => 'solicitud'.($loop->iteration - 1)]) !!}
                                        {!! Form::close() !!}
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud"></a>
                                        <i class="fa fa-check-circle-o icono-opciones-files icono-green" data-toggle="modal" data-target="#aprobarModal" onclick="aprobarModal({{ $solicitud->id }})" title="Aprobar Rendicion Solicitud"></i>
                                        <i class="fa fa-question-circle-o icono-opciones-files icono-orange" data-toggle="modal" data-target="#observacionModal" onclick="observarSolicitud({{ $solicitud->id }})" title="Observar Rendición"></i>
                                        <i class="fa fa-times-circle-o icono-opciones-files icono-red" data-toggle="modal" data-target="#rechazoModal" onclick="datosRechazoModal({{ $solicitud->id }})" title="Rechazar Rendición"></i>
                                        @php
                                             $items_af = DB::select('SELECT COUNT(id) as cantidad, solicitud_id FROM item_rendiciones WHERE solicitud_id = '.$solicitud->id.' AND tipo_id IN (1,7) GROUP BY solicitud_id');
                                        @endphp
                                        @if(count($items_af) > 0)
                                            <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text-o icono-opciones-files icono-green" onclick="formularios_solicitud(this)" title="Ver Formularios AF"></a>
                                        @endif
                                    </td>
                                    <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                    <td>
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                        @endif
                                        @if(sizeof($solicitud->documentosRendidos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs_rendidos(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-orange" title="Descargar Documento(s)"></i></a>
                                        @endif
                                    </td>
                                    <td id="fecha_{{ $solicitud->id }}" style="text-align: right;">{{ $solicitud->created_at }}</td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                            {{ $solicitud->estadoCodigo }}
                                        </span>
                                    </td>
                                    <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                    <td id="total_{{ $solicitud->id }}" style="text-align: right; color: #{{ $solicitud->moneda->color }}; font-weight: bold; font-size: 13px;">{{ number_format($solicitud->total, '2', '.', ',') }}</td>
                                    <td id="moneda_{{ $solicitud->id }}" style="text-align: center; color: #{{ $solicitud->moneda->color }}; font-weight: bold">{{ $solicitud->moneda->sigla }}</td>
                                    <td id="empresa_{{ $solicitud->id }}" data-empresa='{{ $solicitud->proyecto->empresa->slug }}'>{{ $solicitud->proyecto->empresa->nombre }}</td>
                                    <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}"><i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i></td>
                                    <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
                                    <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                    <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                    <td id="cambio_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->tipo_cambio->cambio }}</td>
                                    <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $solicitudes->appends($filters)->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <button type="button" id="btn-modal-solicitud-af" class="btn btn-success btn-block" data-toggle="modal" data-target="#formulariosSolicitud" style="display:none;">
                Modal solicitud
            </button>
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL -->
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- AUTORIZAR MODAL -->
            <div class="modal fade" id="aprobarModal" tabindex="-1" role="dialog" aria-labelledby="aprobarModalLabel">
                <div class="modal-dialog modal-lg" style="width: 1400px;" role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="aprobarModalLabel"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="panel panel-default ">
                                        <div class="panel-heading" id="aprobarModalTitulo"></div>
                                        <div class="panel-body" style="overflow-x: scroll;">
                                            <table class="table table-bordered" id="aprobar-solicitud">
                                                <tr>
                                                    <td><strong>PROYECTO: </strong></td>
                                                    <td colspan="5" id='modalautorizar-proyecto'></td>
                                                    <td><strong>FECHA LÍMITE: </strong></td>
                                                    <td id="modalautorizar-fecha-lim"></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>SOLICITANTE: </strong></td>
                                                    <td colspan="9" id="modalautorizar-solicitante"></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>DESEMBOLSO A: </strong></td>
                                                    <td colspan="9" id='modalautorizar-desembolso'></td>
                                                </tr>
                                                <tr id="tr_con_tipo_apr">
                                                    <td><strong>TIPO DE CAMBIO: </strong></td>
                                                    <td id='modalautorizar-cambio'></td>
                                                    <td><strong>MODALIDAD: </strong></td>
                                                    <td colspan="3" id='modalautorizar-modalidad'></td>
                                                    <td><strong>MONEDA: </strong></td>
                                                    <td colspan="3" id='modalautorizar-moneda'></td>
                                                </tr>
                                                <tr id="tr_sin_tipo_apr">
                                                    <td><strong>MODALIDAD: </strong></td>
                                                    <td colspan="4" id='modalautorizar-modalidad-sin'></td>
                                                    <td><strong>MONEDA: </strong></td>
                                                    <td colspan="4" id='modalautorizar-moneda-sin'></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>REFERENCIA: </strong></td>
                                                    <td colspan="9" id='modalautorizar-referencia'></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>JUSTIFICACIÓN: </strong></td>
                                                    <td colspan="9" id='modalautorizar-observacion'></td>
                                                </tr>
                                                <tr class="no-print">
                                                    <td class="text-right"><strong>TOTAL</strong></td>
                                                    <td colspan="9" id='modalautorizar-total'></td>
                                                </tr>
                                            </table>
                                            <div>
                                                <div id="div_items_solicitud">
                                                    <h4>ITEMS SOLICITUD</h4>
                                                    <table class="table table-bordered table-items" id="table_items" style="font-size: 11px;">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:10px">#</th>
                                                                <th style="width:20px">UNIDAD</th>
                                                                <th style="width:20px">TIPO COMPRA</th>
                                                                <th>DETALLE</th>
                                                                <th style="width:20px">CANTIDAD</th>
                                                                <th style="width:20px">P. UNITARIO</th>
                                                                <th style="width:20px">SUB TOTAL</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id='modal-items'>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div id="div_items_rendidos1">
                                                    <h4>ITEMS RENDIDOS</h4>
                                                    <table class="table table-bordered table-items" id="table_items_rendidos" style="font-size: 11px;">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:10px">#</th>
                                                                <th style="width:20px">UNIDAD</th>
                                                                <th style="width:20px">TIPO COMPRA</th>
                                                                <th>DETALLE</th>
                                                                <th style="width:20px">CANTIDAD</th>
                                                                <th style="width:20px">P. UNITARIO</th>
                                                                <th style="width:20px">FECHA FACTURA</th>
                                                                <th style="width:20px">NRO. FACTURA</th>
                                                                <th style="width:20px">SUB TOTAL</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id='modal-items'>
                                                        </tbody>
                                                    </table>
                                                </div>
        
                                                <p><strong>SON: </strong> <span id="modal-literal-aprobar"></span> CON <span id="modal-decimal-aprobar"></span>/100 <span id="modal-moneda-aprobar"></span></p>
                                            </div>
                                            <div class="text-center">
                                                <input type="checkbox" name="observa" id="observa" onclick="verificar()">&nbsp;&nbsp;<label for='correo'>¿DESEA REALIZAR OBSERVACIÓN?</label>
                                                {!! Form::textarea('des_observacion', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'maxlength' => '250', 'placeholder' => 'DESCRIBA SUS OBSERVACIONES...', 'style' => 'text-transform:uppercase', 'id' => 'des_observacion')) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-default ">
                                        <div class="panel-heading"><h2>ARCHIVOS ADJUNTOS</h2></div>
                                        <div class="panel-body">
                                            <table class="table table-hover table-bordered" id="doc_table">
                                                <thead>
                                                    <tr>
                                                        <th style="width:10px">#</th>
                                                        <th>NOMBRE ARCHIVO</th>
                                                        <th style="width:20px">DESCARGA</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="modal-items">
                                                </tbody>
                                            </table>
                                            <div id="ver_doc_1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer"  id="modalautorizaropciones">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- OBSERVACION MODAL -->
            @include('solicitudes.partial.observacionSolicitud')
            <!-- END MODAL -->
            <!-- MODAL RECHAZO-->
            <div class="modal fade" id="rechazoModal" tabindex="-1" role="dialog" aria-labelledby="rechazoModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default " style="overflow-x: scroll;">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">

                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="5" id='modalrechazo-proyecto'></td>
                                            <td><label for="turno">FECHA LÍMITE: </label></td>
                                            <td id="modalrechazo-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modalrechazo-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modalrechazo-desembolso'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                            <td id='modalrechazo-cambio'></td>
                                            <td><label for="grupo">MODALIDAD: </label></td>
                                            <td colspan="3" id='modalrechazo-modalidad'></td>
                                            <td><label for="grupo">MONEDA: </label></td>
                                            <td colspan="3" id='modalrechazo-moneda'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">REFERENCIA: </label></td>
                                            <td colspan="9" id='modalrechazo-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                                            <td colspan="9" id='modalrechazo-observacion'></td>
                                        </tr>
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-total'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>ITEMS SOLICITUD</h4>
                                        <table class="table table-bordered table-items" id="table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th style="width:20px">TIPO COMPRA</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal-rechazo"></span> CON <span id="modal-decimal-aprobar"></span>/100 BOLIVIANOS</p>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_id')) !!}
                                    {!! Form::hidden('tipo', 'rendicion', array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::textarea('motivo', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform: uppercase;')) !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-primary" onclick="rechazo()">RECHAZAR</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- MODAL FORMULARIO AF-->
            @include('solicitudes.partial.formAF')
            <!-- END MODAL -->
            @section('footerScripts')
            @parent
            <script>
                document.getElementById('des_observacion').style.display = 'none';
                function verificar(){
                    if(document.getElementById('observa').checked){
                        document.getElementById('des_observacion').style.display = 'block';
                    }else{
                        document.getElementById('des_observacion').style.display = 'none';
                    }
                }
            </script>

            @endsection
            <!-- END MODAL -->
        </div>
    </div>
</div>
<input type="hidden" name="apro_id" id="apr_id" data-id="{{ $id_usuario }}" value="{{ $id_usuario }}">
<input type="hidden" name="soli_id" id="sol_id" data-id="{{ session('empresa') }}" value="{{ session('empresa') }}">
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    @if(!isset($filters['campo']) || $filters['campo'] == 0)
        $('#formField').get(0).selectedIndex = 0;

        document.getElementById("searchTexto").value = "";
        document.getElementById("searchTexto").disabled = true;
    @endif

    @if(isset($filters['empresa_id']) && $filters['empresa_id'] != 0)
    empresaSelect();
    @endif

    function imprimirSolicitud(e){
        var id = e.id;
        //window.location.href = '/solicitud-imprimir?id_sol='+id;
    }

    function aprobarModal(fila){
        var div_items_solicitud = document.getElementById('div_items_solicitud');
        div_items_solicitud.setAttribute('style', 'display: none;');
        $("#table_items_rendidos tbody").empty();

        var id_fila = fila;
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');

        document.getElementById('modalautorizar-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalautorizar-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalautorizar-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalautorizar-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalautorizar-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalautorizar-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalautorizar-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('aprobarModalLabel').innerHTML = "<strong>APROBAR RENDICION SOLICITUD # "+numeroSolicitud+"</strong>";

        var span_p = document.getElementById('modalautorizaropciones');
        span_p.innerHTML = '';
        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "APROBAR RENDICIÓN";
        file.setAttribute("onclick", "aprobarSolicitud(this)");
        span_p.appendChild(file);
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        document.getElementById('modal-moneda-aprobar').innerHTML = document.getElementById('moneda_'+fila).innerHTML;

        $("#div_items_rendidos").removeClass('hide');

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalautorizar-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var url = "/json/items_solicitud/" + fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            console.log("cant items"+data.length);
            if(data.length < 1){
                //div_items_solicitud.setAttribute('style', 'display: none;');
            }
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var url_lastEstadoCodigo = "/solicitud-lastEstadoCodigo/"+id_fila;
        var div_antiguos = document.getElementById('div_items_rendidos1');
        div_antiguos.setAttribute('style', 'display: none;');

        $.get(url_lastEstadoCodigo, function(data) {
            if(data == "REN-F"){
                var url_rendidos = "/json/items_rendidos/"+fila;
                var cantidad = 0;

                $.getJSON(url_rendidos, function(data){
                    cantidad = data.length;
                    if(cantidad > 0){
                        div_antiguos.setAttribute('style', 'display: initial;');
                        
                        total_items = 0;

                        $.each(data, function (i, object) {
                            $fila = "<tr>";
                            $fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                            $fila += "<td>"+object.unidad_descripcion+"</td>";
                            $fila += "<td>"+object.tipocompra+"</td>";
                            $fila += "<td>"+object.detalle+"</td>";
                            $fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                            $fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                            $fila += "<td style='text-align:right;'>"+object.fecha_factura+"</td>";
                            $fila += "<td style='text-align:right;'>"+object.num_factura+"</td>";
                            $fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                            $fila += "</tr>";
                            $("#table_items_rendidos tbody").append($fila);
                            total_items += parseFloat(object.subtotal);
                        });
                        $fila = "<tr>";
                        $fila += "<td colspan='8' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                        $fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
                        $fila += "</tr>";
                        $("#table_items_rendidos tbody").append($fila);
                    }else{
                        var div_antiguos1 = document.getElementById('div_items_rendidos1');
                        div_antiguos1.setAttribute('style', 'display: none;');
                    }
                });
            }
        });

        //var str_literal = string_literal_conversion(p_entera);
        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-aprobar').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-aprobar').innerHTML = str_decimal;


        // DOCUMENTOS
        var url = "/json/documentos_rendicion/" + id_fila;
        $.getJSON(url, function(data) {
            $("#doc_table tbody").empty();
            docs = data;
            $.each(data, function (i, object) {
                $fila = "<tr>";
                $fila += "<td>"+object.id+"</td>";
                $fila += "<td>"+object.direccion+"</td>";
                $fila += "<td>";
                $fila += "<a href='/empresas/"+object.empresa_slug+"/<?=session('gestion')?>/rendicion/"+object.direccion+"' download='"+object.direccion+"'><i class='fa fa-download icono-opciones icono-blue' title='Descargar Documento'></i></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-file-text icono-opciones icono-green' onclick='ver_documento_aprrendicion(this, \""+object.empresa_slug+"\", "+ object.gestion +")' data='"+object.direccion+"' title='Ver Documento'></a>";
                $fila += "&nbsp;<a id='"+object.id+"' class='fa fa-eye-slash icono-opciones icono-green' onclick='cerrar_vista(this)' data='"+object.direccion+"' title='Cerrar Vista Previa'></a>";
                $fila += "</td>";
                $fila += "</tr>";
                $("#doc_table tbody").append($fila);
            });
        });
    }

    function aprobarSolicitud(e){
        var obs = "SO";
        var url;
        if(document.getElementById('observa').checked){
            obs = document.getElementById('des_observacion').value;
        }
        url = '/solicitud-aprobar-rendicion?id_sol='+e.id+"&obs="+obs;
        window.location.href = url;
    }

    function observarSolicitud(fila){
        var id_fila = fila;
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalobservacion-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalobservacion-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalobservacion-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalobservacion-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalobservacion-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalobservacion-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalobservacion-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalobservacion-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalobservacion-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalobservacion-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('observarModalLabel').innerHTML = "<strong>OBSERVAR SOLICITUD #"+numeroSolicitud+"</strong>";

        document.getElementById('id_rol').value = 8;
        document.getElementById('id_sol').value = id_fila;

        var span_p = document.getElementById('modalobservacionopciones');
        $('#modalobservacionopciones').empty();
        var file = document.createElement("button");
        file.setAttribute("type", "submit");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "OBSERVAR RENDICIÓN";
        span_p.appendChild(file);
        /**
         * BEGIN: ISSUE 0037
         */
        var rechazoRendicion = document.createElement("input");
        rechazoRendicion.setAttribute("type", "hidden");
        rechazoRendicion.setAttribute("name", "rechazoRendicion");
        rechazoRendicion.setAttribute("id", "rechazoRendicion");
        rechazoRendicion.setAttribute("value", true);
        span_p.appendChild(rechazoRendicion);
        /**
         * END: ISSUE 0037
         */
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalobservacion-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-obs').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-obs').innerHTML = str_decimal;
    }

    function datosRechazoModal(fila){
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalrechazo-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalrechazo-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalrechazo-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalrechazo-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalrechazo-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalrechazo-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalrechazo-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('rechazoModalLabel').innerHTML = "<strong>RECHAZAR SOLICITUD # "+numeroSolicitud+"</strong>";
        document.getElementById('sol_id').value = fila;

        var url_solicitud = '/json/datos_solicitud/'+fila;
        $('#modal-desembolso-antiguo').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modalrechazo-fecha-lim').innerHTML = datos.fecha_limite;
        });

        var url = "/json/items_solicitud/" + fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                id_fila = "<tr>";
                id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                id_fila += "<td>"+object.unidad_descripcion+"</td>";
                id_fila += "<td>"+object.tipocompra+"</td>";
                id_fila += "<td>"+object.detalle+"</td>";
                id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                id_fila += "<td style='text-align:right;'>"+formatNumber(object.subtotal)+"</td>";
                id_fila += "</tr>";
                $("#table_items tbody").append(id_fila);
                total_items += parseFloat(object.subtotal);
            });
            id_fila = "<tr>";
            id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            id_fila += "<td style='text-align:right;'>"+formatNumber(total_items)+"</td>";
            id_fila += "</tr>";
            $("#table_items tbody").append(id_fila);
        });
        //modal-literal-rechazo
        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';
        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-rechazo').innerHTML = str_literal.toUpperCase();
    }

    function rechazo(){
        document.getElementById('id_solicitud').click();
    }

    function formFields(){
        var selOption = document.getElementById('formField');
        var valor = parseInt(selOption[selOption.selectedIndex].value);
        
        if(valor != 0){
            document.getElementById("searchTexto").disabled = false;
        }else{
            document.getElementById("searchTexto").disabled = true;
            document.getElementById("searchTexto").value = "";
        }
    }

    function ver_documento_aprrendicion(e, empresa, gestion){
        console.log('EMP' + empresa + 'G' + gestion);
        var doc = document.getElementById('ver_doc_1');
        var documento = e.getAttribute('data');
        var array_doc = documento.split('.');

        var url = "";
        if(array_doc.length >= 2){
            var doc_id = array_doc[0].split('_');
            // var empresa = document.getElementById('empresa_'+doc_id[0]);
            // if(empresa){
                url = "/empresas/"+ empresa +"/"+ gestion +"/rendicion/"+documento;
            // }else{
            //     url = "/empresas/"+ empresa +"/"+ gestion +"/rendicion/"+documento;
            // }
        }
        if(array_doc[array_doc.length-1] == 'pdf'){
            PDFObject.embed(url, "#ver_doc_1");
            var docFrame = document.getElementById('ver_doc_1');
            docFrame.setAttribute("style", "width: 100%; height: 500px;");
        }else{
            if(url != ""){
                switch(array_doc[1]){
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'bmp':
                        url = "/empresas/"+ empresa +"/"+ gestion +"/rendicion/"+documento;
                        break;
                    default:
                        url = "";
                }
                if(url == ""){
                    doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
                }else{
                    doc.innerHTML = "<iframe src='"+url+"' width='100%' height='500px'></iframe>";
                }
            }else{
                doc.innerHTML = "<i>Debe descargar el documento para poder abrirlo</i>";
            }
        }

        var div_doc = document.getElementById('ver_doc_1');
        div_doc.style.display = 'block';
    }

    function empresaSelect(){
        var selOption = document.getElementById('empresa_id');
        var valor = parseInt(selOption[selOption.selectedIndex].value);

        @if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0)
        var oldProy = {{ $filters['proyecto_id'] }};
        @else
        var oldProy = 0;
        @endif

        if(valor != 0){
            var url = "/json/proyectos/empresa/" + valor;
            var cad = "<option value='0'>TODOS LOS PROYECTOS</option>";
            $.getJSON(url, function(data) {
                document.getElementById('proyecto_id').innerHTML = '';
                $.each(data, function (i, object) {
                    if(oldProy != 0 && oldProy == object.id){
                        cad += "<option value="+object.id+" selected='selected'>"+object.nombre+"</option>";
                    }else{
                        cad += "<option value="+object.id+">"+object.nombre+"</option>";
                    }
                });    
                document.getElementById('proyecto_id').innerHTML = cad;
            });

            document.getElementById("proyecto_id").disabled = false;
        }else{
            document.getElementById("proyecto_id").disabled = true;
            document.getElementById('proyecto_id').innerHTML = "<option value='0'>NINGUNO</option>";
        }
    }

    $('#btn-pdf').on('click', function(){
        var values = $("input[name='exsol_id[]']")
              .map(function(){return $(this).val();}).get();
        window.location.href = '/exportar/excel?ids='+values;
    });
</script>
{{ Html::script('/js/admin.js') }}
@endsection