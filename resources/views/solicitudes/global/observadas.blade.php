@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<script src="/js/pdfobject.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default panel-solicitud">
                <div class="panel-heading" style="background: #F39C12">
                    <div class="col-md-9">
                        <h2 style="color: #fff;">
                            SOLICITUDES OBSERVADAS
                            <div class="btn btn-default pull-right" id="btn-pdf"><span class="fa fa-file-excel-o"></span> EXPORTAR</div>
                            @include('pdf.partial.input_reporte')
                        </h2>
                        @foreach($excelsolicitudes as $exsol)
                            <input type="text" name="exsol_id[]" value="{{ $exsol }}" style="display: none;">
                        @endforeach
                    </div>
                    @php
                        $array_docs_adjuntos = array();
                        $contador = 1;
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    @endphp
                    <div class="col-md-3 pull-right">
                        @foreach($todasmonedas as $moneda)
                            <h2 style="color: #{{ $moneda->color }}">TOTAL ({{ $moneda->sigla }}): {{ number_format($totalmonedas[$moneda->id], '2', '.', ',') }}</h2>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    {!! Form::open(array('action' => 'SolicitudGlobalController@observadas', 'method'=>'POST', 'class'=>'form-inline', 'id' => 'frm_sol_admin')) !!}
                        <input id="consulta" type="hidden" name="consulta" value="{{ $con }}">
                        @include('solicitudes.admin.filtros')
                    {!! Form::close() !!}
                    
                    <div class="row">
                        <div class="col-md-6">
                            {{ $solicitudes->appends($filters)->links() }}
                        </div>
                    </div>
                    
                    <div class="div-solicitudes" style="overflow-x: scroll;">
                        <table class="table table-bordered table-solicitudes table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 90px">OPCIONES</th>
                                    <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                    <th style="width: 60px;">DOC.</th>
                                    <th>@sortablelink('created_at', 'FECHA')</th>
                                    <th>@sortablelink('estado', 'ESTADO')</th>
                                    <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                    <th>@sortablelink('total', 'MONTO')</th>
                                    <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                    <th>@sortablelink('empresa', 'EMPRESA')</th>
                                    <th>@sortablelink('proyecto_id', 'PROYECTO')</th>
                                    <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                    <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                    <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                    <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                    <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $fecha_actual = new DateTime(\Carbon\Carbon::now());
                            @endphp
                            @foreach($solicitudes as $solicitud)
                                @php
                                    $fecha_sol = new DateTime($solicitud->created_at);
                                    $fecha_diff = $fecha_sol->diff($fecha_actual)->format("%a");
                                    if($fecha_diff <= 3){
                                        $color = "#99ffbb";
                                    }elseif($fecha_diff > 3 && $fecha_diff <=7){
                                        $color = "#ffffcc";
                                    }elseif($fecha_diff > 7 && $fecha_diff <=14){
                                        $color = "#ffe6cc";
                                    }elseif($fecha_diff > 14){
                                        $color = "#ffb3b3";
                                    }
                                @endphp
                                <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)" style="background-color: {{ $color }};">
                                    <td>
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                        <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-target="#historialSolicitud" data-toggle="modal"></a>
                                        @php
                                            $usuario = auth()->user();
                                        @endphp
                                        @if((in_array($solicitud->estadoCodigo, ['SOL', 'SOL-M', 'AUT-O', 'REV-O', 'APR-O'])) && $usuario->id == $solicitud->usuario_id)
                                            <a class="fa fa-edit icono-opciones-files icono-green" href="{{ route('solicitud.edit', $solicitud->id) }}" title="Modificar Solicitud"></a>
                                        @elseif($usuario->rol_id == 1)
                                            <a class="fa fa-edit icono-opciones-files icono-green" href="{{ route('solicitud.edit', $solicitud->id) }}" title="Modificar Solicitud"></a>
                                        @endif
                                        @if((count($solicitud->itemsRendidos) > 0 && $solicitud->estados->last()->estado_id > 50) || count($solicitud->devoluciones) > 0)
                                        <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-o icono-opciones-files icono-green" href="/solicitud-imprimir/rendicion?id_sol={{ $solicitud->id }}" target="blank" title="Ver Rendición"></a>
                                        @endif
                                        @if($solicitud->formulariosAF)
                                            <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text-o icono-opciones-files icono-green" onclick="formularios_solicitud(this)" title="Ver Formularios AF"></a>
                                        @endif
                                    </td>
                                    <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                    <td>
                                        @if((in_array($solicitud->estadoCodigo, ['SOL', 'SOL-M', 'AUT-O', 'REV-O', 'APR-O'])) && $usuario->id == $solicitud->usuario_id)
                                            <a id="{{ $solicitud->id }}" onclick="subir_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-upload icono-opciones-files icono-green" title="SUBIR DOCUMENTO(S)"></i></a>&nbsp;
                                        @elseif($usuario->rol_id == 1)
                                            <a id="{{ $solicitud->id }}" onclick="subir_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-upload icono-opciones-files icono-green" title="SUBIR DOCUMENTO(S)"></i></a>&nbsp;
                                        @endif
                                        @if(sizeof($solicitud->documentos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="Descargar Documento(s)"></i></a>
                                        @endif
                                        @if(sizeof($solicitud->documentosRendidos) > 0)
                                            <a id="{{ $solicitud->id }}" onclick="descarga_docs_rendidos(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-orange" title="Descargar Documento(s)"></i></a>
                                        @endif
                                    </td>
                                    <td id="fecha_{{ $solicitud->id }}" style="text-align: right;">{{ $solicitud->created_at }}</td>
                                    <td style="text-align: center; vertical-align: middle;">
                                        <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                            {{ $solicitud->estadoCodigo }}
                                        </span>
                                    </td>
                                    <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                    <td id="total_{{ $solicitud->id }}" style="text-align: right; color: #{{ $solicitud->moneda->color }}; font-weight: bold; font-size: 13px;">{{ number_format($solicitud->total, '2', '.', ',') }}</td>
                                    <td id="moneda_{{ $solicitud->id }}" style="text-align: center; color: #{{ $solicitud->moneda->color }}; font-weight: bold">{{ $solicitud->moneda->sigla }}</td>
                                    <td id="empresa_{{ $solicitud->id }}" data-empresa='{{ $solicitud->proyecto->empresa->slug }}'>{{ $solicitud->proyecto->empresa->nombre }}</td>
                                    <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}"><i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i></td>
                                    <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
                                    <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                    <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                    <td id="cambio_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->tipo_cambio->cambio }}</td>
                                    <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $solicitudes->appends($filters)->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <button type="button" id="btn-modal-solicitud-af" class="btn btn-success btn-block" data-toggle="modal" data-target="#formulariosSolicitud" style="display:none;">
                Modal solicitud
            </button>
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            @include('solicitudes.partial.docsUp')
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL FORMULARIO AF-->
            @include('solicitudes.partial.formAF')
            <!-- END MODAL -->
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
{{ Html::script('/js/main.js') }}
<script type="text/javascript">
    var config = {
        rutas:[
            {
                showCorrespon: "{{route('documentos.showcr', ['id'=>':id'])}}",
                descargar: "{{ route('doc.descargar', ['id'=>':id']) }}",
                storage: "{{ asset('storage/archivo') }}",
                token: "{{Session::token()}}"
            }
        ]
    };
    $('#btn-pdf').on('click', function(){
        var values = $("input[name='exsol_id[]']")
              .map(function(){return $(this).val();}).get();
        window.location.href = '/exportar/excel?ids='+values;
    });
</script>
{{Html::script('/js/docs/documento_solicitud.js')}}
{{ Html::script('/js/admin.js') }}
@endsection