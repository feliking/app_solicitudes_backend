@extends('main')

@section('headerScripts')
<style>
    div.nav_menu{
        margin-bottom: 0px !important;
    }

    iframe#dashboard{
        height: 90vh;
        width: 100%;
    }

    .nav-sm .container.body .right_col{
        margin-left: 50px !important;
    }
</style>
@endsection

@section('content')
<div class="container">
    <iframe id="dashboard" src="https://app.powerbi.com/reportEmbed?reportId=9c1bdb21-f0cb-444f-b503-ebe545783914&autoAuth=true&ctid=e61cebb9-13f1-4cf3-a9d7-6204869e4892&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXNvdXRoLWNlbnRyYWwtdXMtcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0=" frameborder="0" allowFullScreen="true"></iframe>
</div>
@stop