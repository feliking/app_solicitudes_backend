<div class="col-md-12">
    <div class="col-md-3  col-xs-12">
        <div class="form-group row col-md-12">
            {!! Form::label('empresa_id', 'EMPRESA: ', ['class' => 'col-md-3 col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                <select id="empresa_id" name="empresa_id" class="form-control form-admin" onchange="empresaSelect()">
                    @if(count($empresas) > 1)
                        <option value='0'>TODAS LAS EMPRESAS</option>
                        @foreach($empresas as $empresa)
                            @if(isset($filters['empresa_id']) && $filters['empresa_id'] == $empresa['id'])
                            <option value='{{ $empresa['id'] }}' selected='selected'>{{ $empresa['nombre'] }}</option>
                            @else
                            <option value='{{ $empresa['id'] }}' >{{ $empresa['nombre'] }}</option>
                            @endif
                        @endforeach
                    @else
                        @foreach($empresas as $empresa)
                            <option value='{{ $empresa['id'] }}' >{{ $empresa['nombre'] }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group row col-md-12  col-xs-12">
            {!! Form::label('proyecto_id', 'PROYECTO: ', ['class' => 'col-md-3  col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                <select id="proyecto_id" name="proyecto_id" class="form-control form-admin" disabled>
                    <option value='0'>NINGUNO</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group row col-md-12  col-xs-12">
            {!! Form::label('gestion_id', 'GESTIÓN: ', ['class' => 'col-md-3  col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                <select id="gestion_id" name="gestion_id" class="form-control form-admin">
                    @php
                        $selected_gestion = false;
                    @endphp
                    @if (isset($filters['gestion_id']) && $filters['gestion_id'] == 0)
                    <option value='0' selected>TODAS</option>
                    @php
                        $selected_gestion = true;
                    @endphp
                    @else
                    <option value='0'>TODAS</option>
                    @endif
                    @foreach($gestiones as $gestion)
                    @if(isset($filters['gestion_id']) && $filters['gestion_id'] == $gestion->id)
                    <option value="{{ $gestion->id }}" selected>{{ $gestion->gestion }}</option>
                    @php
                        $selected_gestion = true;
                    @endphp
                    @else
                    <option value="{{ $gestion->id }}" {{ ($loop->last && !$selected_gestion) ? 'selected' : '' }}>{{ $gestion->gestion }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group row col-md-12 col-xs-12">
            {!! Form::label('buscar', 'MONTO: ', ['class' => 'col-md-3 col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        {!! Form::select('selectMonto', ['=', '>', '>=', '<', '<='], 0, ['id' => 'selectMonto', 'class' => 'form-control col-md-3', 'style' => 'width: 21%; height: 25px; padding: 3px 6px;']) !!}
                    </div>
                </div>
                {!! Form::number('monto', null, ['id' => 'searchMonto', 'placeholder' => '', 'class' => 'form-control col-md-9', 'style' => 'width: 70%; height: 25px; padding: 3px 6px;']) !!}
            </div>
        </div>
    </div>
</div><br><br>
<div class="col-md-12">
    <div class="col-md-3 col-xs-12">
        <div class="form-group row col-md-12  col-xs-12">
            {!! Form::label('fecha_ini', 'DESDE: ', ['class' => 'col-md-3 col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                {!! Form::date('fecha_ini', null, ['id' => 'searchFechaIni', 'placeholder' => '', 'class' => 'form-control form-admin']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group row col-md-12 col-xs-12">
            {!! Form::label('fecha_fin', 'HASTA: ', ['class' => 'col-md-3 col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
                {!! Form::date('fecha_fin', null, ['id' => 'searchFechaFin', 'placeholder' => '', 'class' => 'form-control form-admin']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <!-- ELIMINAR SOLICITANTE CUANDO ES USUARIO COMUN FALTA!!!! -->
        <div class="form-group row col-md-12 col-xs-12">
            {!! Form::label('lblCampo', 'COLUMNA: ', ['class' => 'col-md-3 col-xs-3']) !!}
            <div class="col-md-9 col-xs-9">
            @if($usuario->rol_id != 2)
                {!! Form::select('campo', ['NINGUNO', 'SOLICITANTE', 'DESEMBOLSO', 'REFERENCIA', 'OBSERVACIÓN', 'NÚMERO DE SOLICITUD', 'TODOS'], 0, ['id' => 'formField', 'class' => 'form-control form-admin', 'onchange' => 'formFields()']) !!}
            @else
                {!! Form::select('campo', ['NINGUNO', 'SOLICITANTE', 'DESEMBOLSO', 'REFERENCIA', 'OBSERVACIÓN'], 0, ['id' => 'formField', 'class' => 'form-control form-admin col-xs-9', 'onchange' => 'formFields()']) !!}
            @endif
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="form-group row col-md-12 col-xs-12">
            <div class="col-md-12 col-xs-12">
                {!! Form::text('texto', null, ['id' => 'searchTexto', 'placeholder' => '', 'class' => 'form-control form-admin', 'style' => 'margin-left: -22px;']) !!}
            </div>
        </div>
    </div>
<!--    @if($usuario->rol_id == 8)
        <div class="col-md-4">
            <div class="form-group row col-md-12">
                {!! Form::label('lblEstado', 'ESTADO: ', ['class' => 'col-md-3']) !!}
                <div class="col-md-9">
                    {!! Form::select('selEstado', ['POR APROBAR', 'AUTORIZADOS', 'APROBADO TESORERIA', 'RENDICION APROBADA', 'APROBADAS'], 0, ['id' => 'selEstado', 'class' => 'form-control', 'style' => 'width: 100%; height: 25px; padding: 3px 6px;']) !!}
                </div>
            </div>
        </div><br><br>
    @else
        <div class="col-md-4">
            <div class="form-group row col-md-12">
                {!! Form::label('lblEstadoUsuario', 'ESTADO: ', ['class' => 'col-md-3']) !!}
                <div class="col-md-9">
                    {!! Form::select('selEstadoUsuario', ['TODAS', 'CREADAS', 'EN AUTORIZADOR', 'EN REVISOR', 'EN APROBADOR', 'EN TESORERIA', 'EN RENDICION', 'FINALIZADAS', 'EN OBSERVACION', 'RECHAZADAS'], 0, ['id' => 'selEstadoUsuario', 'class' => 'form-control', 'style' => 'width: 100%; height: 25px; padding: 3px 6px;']) !!}
                </div>
            </div>
        </div><br><br>
    @endif-->
</div><br><br>
<div class="col-md-2 col-md-offset-5">
    <div class="center-block" style="margin-top: 10px;">
        {!! Form::submit('FILTRAR', ['class' => 'btn btn-success form-control btn-block btn-admin']) !!}
    </div>
</div>
<div class="col-md-12">
    <div class="alert alert-info alert-dismissible fade in" style="margin-bottom: 0px;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <div id="filtro-info">FILTROS {{ ($filtros == "")?"'SIN FILTROS'":$filtros }}</div>
    </div>
</div>