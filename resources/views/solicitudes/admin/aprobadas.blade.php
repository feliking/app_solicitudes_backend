@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
<style>
    .form-admin{
        padding: 3px 6px;
        height: 24px;
        width: 100%!important;
    }
    .form-select-admin{
        padding: 3px 6px;
        height: 24px;
    }
    .btn-admin{
        height: 24px;
        padding: 1px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="col-md-3">
                        <h2>Aprobar Solicitudes</h2>
                    </div>
                    <?php
                        $array_docs_adjuntos = array();
                        $contador = 1;
                        //id_empresa & gestion
                        $id_empresa = session('empresa');
                        $gestion_actual = session('gestion');
                        //ID usuario actual
                        $user = auth()->user();
                        $id_usuario = $user->id;
                    ?>
                    <div class="col-md-3 pull-right">
                        <h2>TOTAL: {{ $totalsolicitudes }}</h2>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body panel-solicitud">
                    {!! Form::open(array('action' => 'SolicitudController@solicitudesAdmin', 'method'=>'POST', 'class'=>'form-inline', 'id' => 'frm_sol_admin')) !!}
                        @include('solicitudes.admin.filtros')
                    {!! Form::close() !!}

                    <div class="row">
                        <div class="col-md-6">
                            {{ $solicitudes->appends(['query' => $con])->links() }}
                        </div>
                        <div class="col-md-6 text-right">
                            <label for="chk_fr_cc" style="margin: 25px 0;">VER SOLO FONDOS ROTATIVOS, CAJAS CHICAS Y REPOSICIONES</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @if(strpos($filtros, 'FONDOS') !== false)
                            <input form="frm_sol_admin" name="chk_fr_cc" type="checkbox" value="view_fr_cc" id="chk_fr_cc" onchange="this.form.submit()" checked/>
                            @else
                            <input form="frm_sol_admin" name="chk_fr_cc" type="checkbox" value="view_fr_cc" id="chk_fr_cc" onchange="this.form.submit()"/>
                            @endif
                        </div>
                    </div>

                    <table class="table table-bordered table-solicitudes table-hover">
                        <thead>
                            <tr>
                                <th style="width: 90px">OPCIONES</th>
                                <th>@sortablelink('numero', 'NRO. SOL.')</th>
                                <th style="width: 60px;">DOC.</th>
                                <th>@sortablelink('created_at', 'FECHA')</th>
                                <th>@sortablelink('estado', 'ESTADO')</th>
                                <th>@sortablelink('total', 'MONTO')</th>
                                <th>@sortablelink('empresa', 'EMPRESA')</th>
                                <th>@sortablelink('proyecto_id', 'PROYECTO')</th>
                                <th>@sortablelink('usuario_id', 'SOLICITANTE')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                <th>@sortablelink('modalidad_id', 'MODALIDAD')</th>
                                <th>@sortablelink('cambio_id', 'TIPO DE CAMBIO')</th>
                                <th>@sortablelink('moneda_id', 'MONEDA')</th>
                                <th>@sortablelink('referencia', 'REFERENCIA')</th>
                                <th>@sortablelink('observacion', 'JUSTIFICACIÓN')</th>
                                <th>@sortablelink('desembolso', 'DESEMBOLSO')</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php
                            $fecha_actual = new DateTime(\Carbon\Carbon::now());
                        @endphp
                        @foreach($solicitudes as $solicitud)
                            @php
                                $fecha_sol = new DateTime($solicitud->created_at);
                                $fecha_diff = $fecha_sol->diff($fecha_actual)->format("%a");
                                if($fecha_diff <= 7){
                                    $color = "#99ffbb";
                                }elseif($fecha_diff > 7 && $fecha_diff <=14){
                                    $color = "#ffffcc";
                                }elseif($fecha_diff > 14 && $fecha_diff <=30){
                                    $color = "#ffe6cc";
                                }elseif($fecha_diff > 30){
                                    $color = "#ffb3b3";
                                }
                            @endphp
                            <tr id="sol_fila_{{ $solicitud->id }}" ondblclick="seleccion_solicitud(this)" style="background-color: {{ $color }};">
                                <td>
                                    {!! Form::open(['action' => ['SolicitudController@guardarAutorizacion', 'id_sol='.$solicitud->id], 'style' => 'display:none']) !!}
                                        {!! Form::hidden('id_sol', $solicitud->id, array('class' => 'form-control', 'readonly' => 'true')) !!}
                                        {!! Form::submit('Aprobar', ['class' => 'btn btn-success', 'id' => 'solicitud'.($contador-1)]) !!}
                                    {!! Form::close() !!}
                                    <a id="sol_fila_{{ $solicitud->id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" title="Ver Solicitud"></a>
                                    <a id="sol_hist_{{ $solicitud->id }}" class="fa fa-header icono-opciones-files icono-green" title="Historial Solicitud" data-id="{{ $solicitud->id }}" data-numero="{{ $solicitud->numero }}" data-toggle="modal" data-target="#historialSolicitud"></a>
                                </td>
                                <td id="solicitud_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->numero }}</td>
                                <td>
                                    @if(sizeof($solicitud->documentos) > 0)
                                        <a id="{{ $solicitud->id }}" onclick="descarga_docs(this)" data-numero="{{ $solicitud->numero }}"><i class="fa fa-cloud-download icono-opciones-files icono-red" title="DESCARGAR DOCUMENTOS(S)"></i></a>
                                    @endif
                                </td>
                                <td id="fecha_{{ $solicitud->id }}" style="text-align: right;">{{ date_format(date_create($solicitud->created_at), 'd-m-Y') }}</td>
                                <td style="text-align: center;">
                                    <span class="label label-default label-estados" data-toggle="modal" data-target="#estadosSolModal" onclick="estadosSolicitud('{{ $solicitud->estadoCodigo }}')">
                                        {{ $solicitud->estadoCodigo }}
                                    </span>
                                </td>
                                <td id="total_{{ $solicitud->id }}" style="text-align: right;">{{ number_format($solicitud->total, '2', '.', '') }}</td>
                                <td id="empresa_{{ $solicitud->id }}">{{ $solicitud->proyecto->empresa->nombre }}</td>
                                <td id="proyecto_td_{{$solicitud->id}}" title="{{ $solicitud->nom_completo_proyecto }}"><i id='proyecto_{{ $solicitud->id }}'>{{$solicitud->proyecto->nombre}}</i></td>
                                <td id="usuario_{{ $solicitud->id }}">{{ $solicitud->usuario->nombre." ".$solicitud->usuario->ap." ".$solicitud->usuario->am }}</td>
                                <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                <td id="tipo_{{ $solicitud->id }}">{{ $solicitud->modalidad->nombre }}</td>
                                <td id="cambio_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->tipo_cambio->cambio }}</td>
                                <td id="moneda_{{ $solicitud->id }}" style="text-align: center;">{{ $solicitud->moneda->sigla }}</td>
                                <td id="referencia_{{ $solicitud->id }}">{{ $solicitud->referencia }}</td>
                                <td id="obs_{{ $solicitud->id }}">{{ $solicitud->observacion }}</td>
                                <td id="desembolso_{{ $solicitud->id }}">{{ $solicitud->desembolso }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $solicitudes->appends(['query' => $con])->links() }}
                </div>
            </div>
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#solicitudModal" style="display:none;">
                Modal solicitud
            </button>
            <button type="button" id="btn-modal-aprobar" class="btn btn-success btn-block" data-toggle="modal" data-target="#aprobarModal" style="display:none;">
                Modal aprobar
            </button>
            <button type="button" id="btn-modal-descarga" class="btn btn-success btn-block" data-toggle="modal" data-target="#descargaDocsModal" style="display:none;">
                Modal descarga
            </button>
            <button type="button" id="btn-modal-subir" class="btn btn-success btn-block" data-toggle="modal" data-target="#subirDocsModal" style="display:none;">
                Modal subir
            </button>
            <!-- OBSERVACION MODAL -->
            @include('solicitudes.partial.observacionSolicitud')
            <!-- END MODAL -->
            <!-- MODAL HISTORIAL SOLICITUD-->
            @include('solicitudes.partial.historial')
            <!-- END MODAL -->
            <!-- MODAL -->
            @include('solicitudes.partial.datosSolicitud')
            <!-- END MODAL -->
            <!-- MODAL RECHAZO-->
            <div class="modal fade" id="rechazoModal" tabindex="-1" role="dialog" aria-labelledby="rechazoModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>RECHAZO DE SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">

                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label for="pago">PROYECTO: </label></td>
                                            <td colspan="5" id='modalrechazo-proyecto'></td>
                                            <td><label for="turno">FECHA LÍMITE: </label></td>
                                            <td id="modalrechazo-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="ruta">SOLICITANTE: </label></td>
                                            <td colspan="9" id="modalrechazo-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modalrechazo-desembolso'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">TIPO DE CAMBIO: </label></td>
                                            <td id='modalrechazo-cambio'></td>
                                            <td><label for="grupo">MODALIDAD: </label></td>
                                            <td colspan="3" id='modalrechazo-modalidad'></td>
                                            <td><label for="grupo">MONEDA: </label></td>
                                            <td colspan="3" id='modalrechazo-moneda'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">REFERENCIA: </label></td>
                                            <td colspan="9" id='modalrechazo-referencia'></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">JUSTIFICACIÓN: </label></td>
                                            <td colspan="9" id='modalrechazo-observacion'></td>
                                        </tr>
<!--                                        <tr class="no-print">
                                            <td class="text-right"><strong>DOCUMENTO(S)<br>ADJUNTO(S)</strong></td>
                                            <td colspan="9" id='modalrechazo-documentos'></td>
                                        </tr>-->
                                        <tr class="no-print">
                                            <td class="text-right"><strong>TOTAL</strong></td>
                                            <td colspan="9" id='modalrechazo-total'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>ITEMS SOLICITUD</h4>
                                        <table class="table table-bordered table-items" id="table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th style="width:20px">TIPO COMPRA</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal-rechazo"></span> CON <span id="modal-decimal-aprobar"></span>/100 BOLIVIANOS</p>
                                    </div>
                                </div>
                            </div>
                            {!! Form::open(array('action' => 'SolicitudController@rechazo','method'=>'POST', 'class'=>'form-horizontal')) !!}
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>MOTIVO DE RECHAZO</h2></div>
                                <div class="panel-body">
                                    {!! Form::hidden('id_solicitud', null, array('placeholder' => '','class' => 'form-control', 'id' => 'sol_id')) !!}
                                    {!! Form::hidden('tipo', 'Aprobar', array('placeholder' => '','class' => 'form-control')) !!}
                                    {!! Form::textarea('motivo', null, array('placeholder' => '','class' => 'form-control', 'rows' => '3', 'placeholder' => 'DESCRIBA EL MOTIVO DEL RECHAZO...', 'style' => 'text-transform: uppercase;')) !!}
                                </div>
                            </div>
<!--                            <div class="text-center">
                                <input type="checkbox" name="correo" id="correo" value="correo" checked="checked">&nbsp;&nbsp;<label for='correo'>¿ENVIAR AL CORREO?</label>
                            </div>-->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CANCELAR</button>
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-primary" onclick="rechazo()">RECHAZAR SOLICITUD</button><!--***REVISAR rechazo-->
                                </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- AUTORIZAR MODAL -->
            @include('solicitudes.partial.aprobarSolicitud')
            <!-- END MODAL -->
            <!-- MODAL DOWNLOAD DOCS-->
            @include('solicitudes.partial.docsDown')
            <!-- END MODAL -->
            <!-- MODAL ESTADOS SOLICITUD-->
            @include('solicitudes.partial.estadosModal')
            <!-- END MODAL -->
            <!-- MODAL SUBIR DOCS-->
            <div class="modal fade" id="subirDocsModal" tabindex="-1" role="dialog" aria-labelledby="subirDocsModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <br>
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="rechazoModalLabel"><strong>SUBIR DOCUMENTOS SOLICITUD</strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>ELEGIR ARCHIVO</h2></div>
                                <div class="panel-body">
                                    {!! Form::open(array('action' => 'SolicitudController@guardarArchivo', 'method'=>'POST', 'class'=>'form-inline', 'files' => true)) !!}
                                        {!! Form::hidden('id_sol_subir', null, array('placeholder' => '', 'class' => 'form-control', 'required' => 'true', 'style' => 'text-transform:uppercase', 'id' => 'id_sol_subir')) !!}
                                        <table class="table-modal-subir-archivo">
                                            <tr>
                                                <td>{!! Form::label('buscar', 'ELEGIR DOCUMENTO: ') !!}</td>
                                                <td>{!! Form::file('doc_up', ['required' => 'true']) !!}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">{!! Form::submit('SUBIR ARCHIVO', ['class' => 'btn btn-success']) !!}</td>
                                            </tr>
                                        </table>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
        </div>
    </div>
</div>
<input type="hidden" name="apro_id" id="apr_id" data-id="{{ $id_usuario }}" value="{{ $id_usuario }}">
<input type="hidden" name="soli_id" id="sol_id" data-id="{{ session('empresa') }}" value="{{ session('empresa') }}">
@stop

@section('footerScripts')
@parent
<script language="javascript" type="text/javascript">
    document.getElementById("searchTexto").value = "";
    document.getElementById("searchTexto").disabled = true;
    $('#formField').get(0).selectedIndex = 0;
    function aprobarSolicitud(e){
        var obs = "SO";
        var url;
        if(document.getElementById('observa').checked){
            obs = document.getElementById('des_observacion').value;
        }
        url = '/solicitud-aprobacion?id_sol='+e.id+"&obs="+obs;
        window.location.href = url;
    }

    function imprimirSolicitud(e){
        var id = e.id;
        //window.location.href = '/solicitud-imprimir?id_sol='+id;
    }
    function datosRechazoModal(fila){
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalrechazo-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        //document.getElementById('modalrechazo-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
        document.getElementById('modalrechazo-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalrechazo-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalrechazo-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalrechazo-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalrechazo-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalrechazo-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalrechazo-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalrechazo-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('rechazoModalLabel').innerHTML = "<strong>RECHAZAR SOLICITUD # "+numeroSolicitud+"</strong>";
        document.getElementById('sol_id').value = fila;

        var url = "/json/items_solicitud/" + fila;

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-rechazo').innerHTML = str_literal.toUpperCase();

        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                $id_fila = "<tr>";
                $id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                $id_fila += "<td>"+object.unidad_descripcion+"</td>";
                $id_fila += "<td>"+object.tipocompra+"</td>";
                $id_fila += "<td>"+object.detalle+"</td>";
                $id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                $id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                $id_fila += "<td style='text-align:right;'>"+parseFloat(object.subtotal).toFixed(2)+"</td>";
                $id_fila += "</tr>";
                $("#table_items tbody").append($id_fila);
                total_items += parseFloat(object.subtotal);
            });
            $id_fila = "<tr>";
            $id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            $id_fila += "<td style='text-align:right;'>"+total_items.toFixed(2)+"</td>";
            $id_fila += "</tr>";
            $("#table_items tbody").append($id_fila);
        });
    }
    function aprobarModal(fila){
        var sol_id = document.getElementById('sol_id').value;
        var apr_id = document.getElementById('apr_id').value;
        var url;

        //document.getElementById('aprobarModalTitulo').innerHTML = 'asdf';
        var id_fila = fila;
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalautorizar-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        //document.getElementById('modalautorizar-subproyecto').innerHTML = document.getElementById('nom_subproy_'+fila).innerHTML;
        document.getElementById('modalautorizar-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalautorizar-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalautorizar-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalautorizar-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalautorizar-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalautorizar-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalautorizar-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalautorizar-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('aprobarModalLabel').innerHTML = "<strong>APROBAR SOLICITUD # "+numeroSolicitud+"</strong>";

        var span_p = document.getElementById('modalautorizaropciones');
        span_p.innerHTML = '';
        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "APROBAR";
        file.setAttribute("onclick", "aprobarSolicitud(this)");
        span_p.appendChild(file);
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                $id_fila = "<tr>";
                $id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                $id_fila += "<td>"+object.unidad_descripcion+"</td>";
                $id_fila += "<td>"+object.tipocompra+"</td>";
                $id_fila += "<td>"+object.detalle+"</td>";
                $id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                $id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                $id_fila += "<td style='text-align:right;'>"+object.subtotal.toFixed(2)+"</td>";
                $id_fila += "</tr>";
                $("#table_items tbody").append($id_fila);
                total_items += parseFloat(object.subtotal);
            });
            $id_fila = "<tr>";
            $id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            $id_fila += "<td style='text-align:right;'>"+total_items.toFixed(2)+"</td>";
            $id_fila += "</tr>";
            $("#table_items tbody").append($id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-aprobar').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-aprobar').innerHTML = str_decimal;

    }

    function observarSolicitud(fila){
        var id_fila = fila;
        var numeroSolicitud = $('#sol_hist_'+fila).attr('data-numero');
        document.getElementById('modalobservacion-proyecto').innerHTML = document.getElementById('proyecto_td_'+fila).title;
        document.getElementById('modalobservacion-solicitante').innerHTML = document.getElementById('usuario_'+fila).innerHTML;
        document.getElementById('modalobservacion-desembolso').innerHTML = document.getElementById('desembolso_'+fila).innerHTML;
        document.getElementById('modalobservacion-cambio').innerHTML = document.getElementById('cambio_'+fila).innerHTML;
        document.getElementById('modalobservacion-modalidad').innerHTML = document.getElementById('tipo_'+fila).innerHTML;
        document.getElementById('modalobservacion-moneda').innerHTML = document.getElementById('moneda_'+fila).innerHTML;
        document.getElementById('modalobservacion-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('modalobservacion-total').innerHTML = document.getElementById('total_'+fila).innerHTML;
        document.getElementById('modalobservacion-observacion').innerHTML = document.getElementById('obs_'+fila).innerHTML;
        document.getElementById('modalobservacion-referencia').innerHTML = document.getElementById('referencia_'+fila).innerHTML;
        document.getElementById('observarModalLabel').innerHTML = "<strong>OBSERVAR SOLICITUD #"+numeroSolicitud+"</strong>";

        document.getElementById('id_rol').value = 5;
        document.getElementById('id_sol').value = id_fila;

        var span_p = document.getElementById('modalobservacionopciones');
        $('#modalobservacionopciones').empty();
        var file = document.createElement("button");
        file.setAttribute("type", "submit");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", fila);
        file.innerHTML = "OBSERVAR";
        //file.setAttribute("onclick", "realizaObservacionSolicitud(this)");
        span_p.appendChild(file);
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.innerHTML = "CERRAR";
        span_p.appendChild(btn_cerrar);

        var url = "/json/items_solicitud/" + id_fila;
        var total_items = 0;
        $.getJSON(url, function(data) {
            $("#table_items tbody").empty();
            $.each(data, function (i, object) {
                $id_fila = "<tr>";
                $id_fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                $id_fila += "<td>"+object.unidad_descripcion+"</td>";
                $id_fila += "<td>"+object.tipocompra+"</td>";
                $id_fila += "<td>"+object.detalle+"</td>";
                $id_fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                $id_fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                $id_fila += "<td style='text-align:right;'>"+object.subtotal+"</td>";
                $id_fila += "</tr>";
                $("#table_items tbody").append($id_fila);
                total_items += parseFloat(object.subtotal);
            });
            $id_fila = "<tr>";
            $id_fila += "<td colspan='6' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            $id_fila += "<td style='text-align:right;'>"+total_items.toFixed(2)+"</td>";
            $id_fila += "</tr>";
            $("#table_items tbody").append($id_fila);
        });

        var total = document.getElementById('total_'+fila).innerHTML;
        var res = total.split(".");
        var p_entera = res[0];
        var p_decimal = res[1];
        var str_decimal = '00';

        //var str_literal = string_literal_conversion(p_entera);
        var str_literal = numeroLiteral(p_entera);
        document.getElementById('modal-literal-obs').innerHTML = str_literal.toUpperCase();
        if(p_decimal>0 && p_decimal <10){
            str_decimal = '0'+str_decimal;
        }else if(p_decimal > 9){
            str_decimal = res[1];
        }
        document.getElementById('modal-decimal-obs').innerHTML = str_decimal;
    }
    function rechazo(){
        document.getElementById('id_solicitud').click();
    }

    function formFields(){
        var selOption = document.getElementById('formField');
        var valor = parseInt(selOption[selOption.selectedIndex].value);
    }

    function empresaSelect(){
        var selOption = document.getElementById('empresa_id');
        var valor = parseInt(selOption[selOption.selectedIndex].value);
        if(valor != 0){
            var url = "/json/proyectos/empresa/" + valor;
            var cad = "<option value='0'>TODOS LOS PROYECTOS</option>";
            $.getJSON(url, function(data) {
                document.getElementById('proyecto_id').innerHTML = '';
                $.each(data, function (i, object) {
                    cad += "<option value="+object.id+">"+object.nombre+"</option>";
                });
                document.getElementById('proyecto_id').innerHTML = cad;
            });

            document.getElementById("proyecto_id").disabled = false;
        }else{
            document.getElementById("proyecto_id").disabled = true;
            document.getElementById('proyecto_id').innerHTML = "<option value='0'>NINGUNO</option>";
        }
    }
</script>
@endsection