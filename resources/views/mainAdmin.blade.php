<!DOCTYPE html>
<html lang="en">
<head>
    @include('parts.header')
    @yield('headerScripts')
</head>

@php
$userData = auth()->user();
@endphp

<body class="nav-md">
    <div class="container body">
        <div class="main_container">

            @include('parts.nav')

            <!-- page content -->
            <div class="right_col">
                @include('parts.messages')
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs no-print" role="tablist">
                    @if(!isset($_GET['tabs']) && $userData->rol_id == 8)
                            <li class="{{ Request::is('solicitudes-admin/creadas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/creadas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/creadas') ? '#9B59B6' : '#fff' }}; color: {{ Request::is('solicitudes-admin/creadas') ? '#FFFFFF' : '#5A738E' }}; ">
                                        CREADAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/autorizadas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/autorizadas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/autorizadas') ? '#F6B855' : '#fff' }}; color: {{ Request::is('solicitudes-admin/autorizadas') ? '#FFFFFF' : '#5A738E' }};">
                                        REVISADAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes/admin') ? 'active' : '' }}">
                                    <a href="/solicitudes/admin" style="display: inline-block; background: {{ Request::is('solicitudes/admin') ? '#1ABB9C' : '#fff' }}; color: {{ Request::is('solicitudes/admin') ? '#FFFFFF' : '#5A738E' }};">
                                        PARA APROBAR
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/transf_tesoreria') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/transf_tesoreria" style="display: inline-block; background: {{ Request::is('solicitudes-admin/transf_tesoreria') ? '#50C1CF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/transf_tesoreria') ? '#FFFFFF' : '#5A738E' }};">
                                        TRANSFERENCIAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 1
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes2') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes2" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes2') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes2') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 2
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes3') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes3" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes3') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes3') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 3
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes4') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes4" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes4') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes4') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 4
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes5') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes5" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes5') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes5') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 5
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/tesoreria') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/tesoreria" style="display: inline-block; background: {{ Request::is('solicitudes-admin/tesoreria') ? '#50C1CF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/tesoreria') ? '#FFFFFF' : '#5A738E' }};">
                                        TESORERIA
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/rechazadas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/rechazadas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/rechazadas') ? '#50AACF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/rechazadas') ? '#FFFFFF' : '#5A738E' }};">
                                        RECHAZADAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/rechazadas-tesoreria') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/rechazadas-tesoreria" style="display: inline-block; background: {{ Request::is('solicitudes-admin/rechazadas-tesoreria') ? '#50AACF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/rechazadas-tesoreria') ? '#FFFFFF' : '#5A738E' }};">
                                        RECHAZADAS TESORERIA
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/todas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/todas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/todas') ? '#AAC1CF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/todas') ? '#FFFFFF' : '#5A738E' }};">
                                        TODAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                        @endif
                    </ul>
                </div>
                @yield('content')
                <div class="clearfix"></div>
            </div>
            <!-- /page content -->
            <!-- footer content -->
            <footer>
                <div class="text-center">
                    <a href="#">&COPY; {{ \Carbon\Carbon::now()->year }} Pragma Invest S.A</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>
    <input type="hidden" name="empresa" id="emp_id" data-id="{{ session('empresa') }}" value="{{ session('empresa') }}">
    <input type="hidden" name="user_rol_id" id="user_rol_id" data-id="{{ $userData->rol_id }}" value="{{ $userData->rol_id }}">
    @include('parts.javascript')
    @yield('footerScripts')

    <script language="javascript" type="text/javascript">
        datosResumenUsuario();

        function datosResumenUsuario(){
            var emp_id = document.getElementById('emp_id').value;
            var rol_id = document.getElementById('user_rol_id').value;
            if(rol_id == 8){
                var url = "/json/cantidad/empresas/estados";
                $.getJSON(url, function(data) {
                    $("#datos-adicionales").empty();
                    var bub_adm_poraprobar = document.getElementById('bubble_admin_poraprobar');
                    bub_adm_poraprobar.innerHTML = data['curso'];

                    var bub_adm_aprobadas = document.getElementById('bubble_admin_aprobadas');
                    bub_adm_aprobadas.innerHTML = data['rendiciones'];
                });
            }else{
                var url = "/json/cantidad/estados/" + emp_id;
                $.getJSON(url, function(data) {
                    $("#datos-adicionales").empty();

                    if(document.getElementById('bubble_curso')){
                        var bub_curso = document.getElementById('bubble_curso');
                        bub_curso.innerHTML = data['curso'];
                        var bub_porrendir = document.getElementById('bubble_rendiciones');
                        bub_porrendir.innerHTML = data['rendiciones'];
                        var bub_obs = document.getElementById('bubble_observadas');
                        bub_obs.innerHTML = data['observadas'];
                        var bub_rechazadas = document.getElementById('bubble_rechazadas');
                        bub_rechazadas.innerHTML = data['rechazadas'];
                        var bub_rendidas = document.getElementById('bubble_rendidas');
                        bub_rendidas.innerHTML = data['rendidas'];
                        var bub_finalizadas = document.getElementById('bubble_finalizadas');
                        bub_finalizadas.innerHTML = data['finalizadas'];
                    }

                    var bub_porautorizar = document.getElementById('bubble_porautorizar');
                    var bub_autorizadas = document.getElementById('bubble_autorizadas');
                    var bub_porrevisar = document.getElementById('bubble_porrevisar');
                    var bub_revisadas = document.getElementById('bubble_revisadas');
                    var bub_poraprobar = document.getElementById('bubble_poraprobar');
                    var bub_aprobadas = document.getElementById('bubble_aprobadas');
                    var bub_portesoreria = document.getElementById('bubble_portesoreria');
                    var bub_tesoreria = document.getElementById('bubble_tesoreria');
                    var bub_aprrendir = document.getElementById('bubble_aprrendicion');

                    if(data['aprrendir'] != null && data['aprrendir'] != ''){
//                        bub_aprrendir.innerHTML = data['aprrendir'];
                    }

                    var entero = parseInt(data['rol_id']);
                    if(entero > 2){
                        var titulo = "";
                        var cadListos = "";
                        var cadPor = "";
                        var cantidadListos = 0;
                        var cantidadPor = 0;
                        switch(entero){
                            case 3:
                                titulo = "Autorizador";
                                cadListos = "Autorizadas";
                                cadPor = "Por Autorizar";
                                bub_porautorizar.innerHTML = data['porautorizar'];
                                bub_autorizadas.innerHTML = data['autorizadas'];
                                break;
                            case 4:
                                titulo = "Revisor";
                                cadListos = "Revisadas";
                                cadPor = "Por Revisar";
                                bub_porrevisar.innerHTML = data['porrevisar'];
                                bub_revisadas.innerHTML = data['revisadas'];
                                break;
                            case 5:
                                titulo = "Aprobador";
                                cadListos = "Aprobadas";
                                cadPor = "Por Aprobar";
                                bub_poraprobar.innerHTML = data['poraprobar'];
                                bub_aprobadas.innerHTML = data['aprobadas'];
                                break;
                            case 6:
                                titulo = "Tesoreria";
                                cadListos = "Reg. Tesoreria Realizados";
                                cadPor = "Reg. Tesoreria Faltantes";
                                bub_portesoreria.innerHTML = data['portesoreria'];
                                bub_tesoreria.innerHTML = data['tesoreria'];
                                break;
                        }
                    }
                });
            }
        }
    </script>

    <script>
        $(document).ready(function (){
            $('form').submit(function() {
                $(this).find('button[type="submit"]').prop('disabled', true);
                $('button[form="' + $(this).attr('id') + '"]').prop('disabled', true);
            });

            // $('a[id^="sol_pen"]').on('click', function() {
            //     $(this).attr('disabled', true);
            //     $(this).attr('href', '');
            // });
        });
    </script>

</body>
</html>