<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="http://localhost:7002/bootstrap.css" rel="stylesheet">

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="login-body">
    <div id="app">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/jquery.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/main.js"></script>
    @include('parts.javascript')
    @yield('footerScripts')
</body>
</html>