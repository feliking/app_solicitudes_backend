<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Solicitudes </title>

    <link href="/css/style.css" rel="stylesheet">
    <link href="http://localhost:7002/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="/menu/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/menu/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/menu/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="/menu/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="/menu/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
    <!-- Custom Theme Style -->
    <link href="/menu/build/css/custom.min.css" rel="stylesheet">
    
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="/solicitud" class="site_title">
                            <?php
                            $req_id_empresa = session('empresa');
                            //var_dump(session('id_empresa'));
                            //exit();
                            if($req_id_empresa){
                                $tit_emp = App\Empresas::find(session('empresa'));?>
                                <img src="{{ asset($tit_emp->imagen) }}">
                                <?php
                            }
                            ?>
                        </a>
                    </div>

                    <div class="clearfix"></div>


                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <!-- ADMINISTRADOR -->
                        <?php
                        $user = auth()->user();
                        $acceso_usuario = $user->acceso;
                        if($acceso_usuario == ''){
                            //REDIRECCIONAR INDEX
                        }elseif($acceso_usuario == 'administrador'){
                            ?>
                            <div class="menu_section">
                                <h3>Administración</h3>
                                <ul class="nav side-menu">
                                    <li>
                                        <a><i class="fa fa-institution"></i> Empresas <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="/empresas/listado">Listado</a></li>
                                            <li><a href="/empresa/create">Crear</a></li>
<!--                                            <li><a href="/asignar-empresa">Asignar</a></li>-->
                                        </ul>
                                    </li>
                                    <li>
                                        <a><i class="fa fa-group"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="/usuario">Listado</a></li>
                                            <li><a href="/usuario/create">Crear</a></li>
                                            <li><a href="#">Buscar</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a><i class="fa fa-building-o"></i> Proyectos <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="/proyectos-listado">Listado</a></li>
                                            <li>
                                                <div class="form-crea-proy" style="display: none;">
                                                    {!! Form::open(array('route' => 'proyectos-form','method'=>'POST', 'class'=>'form-horizontal')) !!}
                                                        {!! Form::hidden('id_empresa', $req_id_empresa, array('placeholder' => '','class' => 'form-control')) !!}
                                                        {!! Form::submit('Crear Proyecto', ['class' => 'btn btn-success', 'id' => 'form-crea-proyecto']) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                                <a id="crea-proyecto">Crear</a>
                                            </li>
                                            <li><a href="#">Buscar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                        <!-- END ADMINISTRADOR -->
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li>
                                    <a href="/seleccionar_empresa"><i class="fa fa-institution"></i> Empresas</a>
                                </li>
                                <li>
                                    <a><i class="fa fa-edit"></i> Solicitudes <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="/solicitud">Listado</a></li>
                                        <li><a href="/solicitud/create">Crear</a></li>
                                        <li><a href="#">Modificar</a></li>
                                        <li><a href="#">Observadas</a></li>
                                        <li><a href="#">Rechazadas</a></li>
                                        <li><a href="#">Estado</a> <span class="badge bg-red pull-right">50%</span></li>
                                        <li><a href="#">Buscar</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-calendar-o"></i> Gestiones*** <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                    <?php
                                    
                                    ?>
                                    </ul>
                                </li>
                                <li id="sidebar-opciones" class="hidden-xs"><a><i class="fa fa-calculator"></i> Tipo de Cambio</a>
                                    <?php
                                    //Web service tipo de cambio
                                    session(['tipo_cambio' => "6.96"]);
                                    echo "TIPO DE CAMBIO ".session('tipo_cambio');
                                    ?>
                                    <ul class="">
                                        <li><i class="fa fa-money" style="color: green;"></i> Compra: <?=$cotizacion->compra?></li>
                                        <li><i class="fa fa-money" style="color: #c9302c;"></i> Venta:&nbsp;&nbsp;&nbsp;&nbsp;<?=$cotizacion->venta?></li>
                                        <li><i class="fa fa-home"></i> UFV:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$cotizacion->ufv?></li>
                                        <li id="form-tipo-cambio">
                                            <form>
                                                <input class="form-control" type="text" value="<?=$cotizacion->compra?>" style="width: 90%">
                                                <input class="btn btn-primary" type="submit" value="Guardar">
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                <li  id="sidebar-opciones"><a><i class="fa fa-calendar"></i> Calendario</a>
                                    <ul class="">
                                        <li>
                                            Calendario
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Configuración">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Pantalla Completa">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="SELECCIONAR EMPRESA" href="/seleccionar_empresa">
                            <span class="fa fa-institution" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="CERRAR SESIÓN" id="btn-logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav no-print">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle" style="display: none;"><!-- CORREGIR -->
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <div class="nav_nombre_empresa">
                            <?php
                            if(FALSE !== session('empresa')){
                                $tit_emp = App\Empresas::find(session('empresa'));
                                echo "&nbsp;&nbsp;&nbsp;<i class='fa fa-institution'></i> Empresa: <span id='nom_empresa'>".$tit_emp->nom_emp."</span>";
                            }elseif (\Auth::user()->global_view == 1){
                                echo "&nbsp;&nbsp;&nbsp;<i class='fa fa-institution'></i> TODAS LAS EMPREAS";
                            }else{
                                echo "SIN EMPRESAS!!!";
                            }
                            ?>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    {{ ucfirst(Auth::user()->nombre).'&nbsp;&nbsp;'.ucfirst(Auth::user()->ap).'&nbsp;&nbsp;'.ucfirst(Auth::user()->am) }}&nbsp;&nbsp;
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="javascript:;"> Perfil</a></li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="badge bg-red pull-right">50%</span>
                                            <span>Configuracion</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out pull-right"></i>  Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs no-print" role="tablist">
                        <li class="{{ Request::is('solicitud') ? 'active' : '' }}"><a href="/solicitud">SOLICITUDES</a></li>
                        <?php
                        if($acceso_usuario == 'administrador' || $acceso_usuario == 'aprobador'){
                            ?>
                            <li class="{{ Request::is('solicitud-aprobar') ? 'active' : '' }}"><a href="/solicitud-aprobar">AUTORIZACIÓN</a></li>
                            <?php
                        }
                        ?>
                        
<!--                        <li class="{{ Request::is('solicitud-verificar') ? 'active' : '' }}"><a href="#" >Verificación</a></li>
                        <li class="{{ Request::is('solicitud-tesoreria') ? 'active' : '' }}"><a href="#4" >Tesoreria</a></li>
                        <li class="{{ Request::is('solicitud-rendicion') ? 'active' : '' }}"><a href="#" >Rendición</a></li>-->
                        <?php
                        if($acceso_usuario == 'administrador' || $acceso_usuario == 'autorizador'){
                            ?>
                            <li class="{{ Request::is('solicitud-autorizar') ? 'active' : '' }}"><a href="/solicitud-autorizar" >APROBACIÓN</a></li>
                            <?php
                        }
                        ?>
<!--                        <li class="{{ Request::is('solicitud-reportes') ? 'active' : '' }}"><a href="#" >Reportes</a></li>
                        <li class="{{ Request::is('solicitud-opciones') ? 'active' : '' }}"><a href="#" >Opciones</a></li>
                        <li class="{{ Request::is('solicitud-configuracion') ? 'active' : '' }}"><a href="#" >Configuración</a></li>-->
                    </ul>
                </div>
                @yield('content')
            </div>
            <!-- /page content -->
            <
            <!-- footer content -->
            <footer>
                <div class="text-center">
                    <a href="#">&COPY; Pragma Invest S.A</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="/menu/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/menu/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/menu/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/menu/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/menu/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
<!--        <script src="/menu/vendors/bernii/gauge.js/dist/gauge.min.js"></script>-->
    <!-- bootstrap-progressbar -->
    <script src="/menu/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/menu/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/menu/vendors/skycons/skycons.js"></script>
    <!-- jVectorMap -->
    <script src="/menu/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/menu/js/moment/moment.min.js"></script>
    <script src="/menu/js/datepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="/menu/build/js/custom.min.js"></script>
    <!-- jVectorMap -->
    <script src="/menu/js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jVectorMap -->
    <script src="/menu/js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script src="/menu/js/maps/jquery-jvectormap-us-aea-en.js"></script>
    <script src="/menu/js/maps/gdp-data.js"></script>
    <!-- Scripts -->
    <script src="/js/jquery.js"></script>
    <script src="/js/main.js"></script>

</body>
</html>