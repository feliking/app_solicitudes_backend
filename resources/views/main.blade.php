<!DOCTYPE html>
<html lang="en">
<head>
    @include('parts.header')
    @yield('headerScripts')
</head>

@php
$userData = auth()->user();
@endphp

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            
            @include('parts.nav')
            
            <!-- page content -->
            <div class="right_col">
                @include('parts.messages')
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    @if(!Request::is('reporte/general/usuario') && !Request::is('solicitudes/admin') && !Request::is('libreta') && !Request::is('transferencia') && !Request::is('global/*'))
                        <ul class="nav nav-tabs no-print" role="tablist">
                            @if(!isset($_GET['tabs']) && $userData->rol_id != 8 && $userData->global_view != 1)
                                <li class="{{ Request::is('solicitud') ? 'active' : '' }}">
                                    <a href="/solicitud" style="display: inline-block">
                                        EN CURSO
                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('rendiciones') ? 'active' : '' }}">
                                    <a href="/rendiciones" style="display: inline-block">
                                        POR RENDIR
                                        <span class="badge bg-purple" id="bubble_rendiciones">0</span>
                                    </a>

                                </li>
                                <li class="{{ Request::is('solicitudes?t=observadas') ? 'active' : '' }}">
                                    <a href="/solicitudes?t=observadas" style="display: inline-block">
                                        OBSERVADAS
                                        <span class="badge bg-orange" id="bubble_observadas">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/solicitudes?t=rechazadas" style="display: inline-block">
                                        RECHAZADAS
                                        <span class="badge bg-red" id="bubble_rechazadas">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/solicitudes?t=rendidas" style="display: inline-block">
                                        RENDIDAS
                                        <span class="badge bg-blue-sky" id="bubble_rendidas">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/solicitudes?t=finalizadas" style="display: inline-block">
                                        FINALIZADAS
                                        <span class="badge bg-blue" id="bubble_finalizadas">0</span>
                                    </a>
                                </li>
                            @elseif( $userData->rol_id == 8)
                                {{-- <li class="{{ Request::is('solicitudes/admin') ? 'active' : '' }}">
                                    <a href="/solicitudes/admin" style="display: inline-block">
                                        POR APROBAR
                                        <span class="badge bg-green" id="bubble_admin_poraprobar" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes/admin/aprobadas') ? 'active' : '' }}">
                                    <a href="/solicitudes/admin/aprobadas" style="display: inline-block">
                                        APROBADAS
                                        <span class="badge bg-purple" id="bubble_admin_aprobadas">0</span>
                                    </a>

                                </li> --}}
                            @endif

                            @if(isset($_GET['tabs']) && $_GET['tabs'] == 'autorizador')
                                <li>
                                    <a href="/solicitud-autorizar?tabs=autorizador" style="display: inline-block">
                                        AUTORIZAR
                                        <span class="badge bg-orange" id="bubble_porautorizar">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/solicitudes-disponibles?tabs=autorizador" style="display: inline-block">
                                        DISPONIBLES
                                        <span class="badge bg-green" id="bubble_autorizadas">0</span>
                                    </a>
                                </li>
                            @endif
                            @if(isset($_GET['tabs']) && $_GET['tabs'] == 'revisor')
                                {{-- <li>
                                    <a href="/solicitud-controller?tabs=revisor" style="display: inline-block">
                                        REVISOR
                                        <span class="badge bg-orange" id="bubble_porrevisar">0</span>
                                    </a>
                                </li> --}}
                                @if(Auth::user()->revisa->contains(\App\Modalidades::find(1)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&modalidad=1" style="display: inline-block">
                                        SUJETOS A RENDICIÓN
                                        <span class="badge" id="bubble_porrevisarsr" style="background-color: #{{ \App\Modalidades::find(1)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->revisa->contains(\App\Modalidades::find(2)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&modalidad=2" style="display: inline-block">
                                        GASTO DIRECTO
                                        <span class="badge" id="bubble_porrevisargd" style="background-color: #{{ \App\Modalidades::find(2)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->revisa->contains(\App\Modalidades::find(4)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&modalidad=4" style="display: inline-block">
                                        REPOSICIÓN
                                        <span class="badge" id="bubble_porrevisarrepo" style="background-color: #{{ \App\Modalidades::find(4)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->revisa_fr->contains(\App\FondoRotativoTipos::find(1)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&tipo_fr=1" style="display: inline-block">
                                        CAJA CHICA
                                        <span class="badge" id="bubble_porrevisarcc" style="background-color: #{{ \App\FondoRotativoTipos::find(1)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->revisa_fr->contains(\App\FondoRotativoTipos::find(2)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&tipo_fr=2" style="display: inline-block">
                                        FONDO ROTATIVO ADMINISTRATIVO
                                        <span class="badge" id="bubble_porrevisarfra" style="background-color: #{{ \App\FondoRotativoTipos::find(2)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->revisa_fr->contains(\App\FondoRotativoTipos::find(3)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&tipo_fr=3" style="display: inline-block">
                                        FONDO ROTATIVO OPERATIVO
                                        <span class="badge" id="bubble_porrevisarfro" style="background-color: #{{ \App\FondoRotativoTipos::find(3)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->revisa_fr->contains(\App\FondoRotativoTipos::find(4)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&tipo_fr=4" style="display: inline-block">
                                        FONDO ROTATIVO CONSTRUCCION
                                        <span class="badge" id="bubble_porrevisarfrc" style="background-color: #{{ \App\FondoRotativoTipos::find(4)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                @if(Auth::user()->revisa_fr->contains(\App\FondoRotativoTipos::find(5)))
                                <li>
                                    <a href="/solicitud-controller?tabs=revisor&tipo_fr=5" style="display: inline-block">
                                        FONDO ROTATIVO MANTENIMIENTO
                                        <span class="badge" id="bubble_porrevisarfrm" style="background-color: #{{ \App\FondoRotativoTipos::find(5)->color }}">0</span>
                                    </a>
                                </li>
                                @endif
                                <li>
                                    <a href="/solicitudes-disponibles?tabs=revisor" style="display: inline-block">
                                        DISPONIBLES
                                        <span class="badge bg-green" id="bubble_revisadas">0</span>
                                    </a>
                                </li>
                            @endif
                            @if(isset($_GET['tabs']) && $_GET['tabs'] == 'aprobador')
                                <li>
                                    <a href="/solicitud-aprobar?tabs=aprobador" style="display: inline-block">
                                        APROBADOR
                                        <span class="badge bg-orange" id="bubble_poraprobar">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/solicitudes-disponibles?tabs=aprobador" style="display: inline-block">
                                        DISPONIBLES
                                        <span class="badge bg-green" id="bubble_aprobadas">0</span>
                                    </a>
                                </li>
                            @endif
                            @if(isset($_GET['tabs']) && $_GET['tabs'] == 'tesoreria')
                                <li>
                                    <a href="/solicitud-tesoreria?tabs=tesoreria" style="display: inline-block">
                                        TESORERIA
                                        <span class="badge bg-orange" id="bubble_portesoreria">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/solicitudes-disponibles?tabs=tesoreria" style="display: inline-block">
                                        DISPONIBLES
                                        <span class="badge bg-green" id="bubble_tesoreria">0</span>
                                    </a>
                                </li>
                            @endif
                            @if(isset($_GET['tabs']) && $_GET['tabs'] == 'aprrendiciones')
                                <li>
                                    <a href="/aprobador-rendiciones?tabs=aprrendiciones" style="display: inline-block">
                                        APROBAR RENDICIONES
                                        <span class="badge bg-orange" id="bubble_aprrendicion">0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/solicitudes-disponibles?tabs=aprrendiciones" style="display: inline-block">
                                        RENDICIONES APROBADAS
                                        <span class="badge bg-green" id="bubble_aprrendiciondisp">0</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @else
                        <ul class="nav nav-tabs no-print" role="tablist">
                            @if(!isset($_GET['tabs']) && $userData->rol_id == 8)
                                <li class="{{ Request::is('solicitudes-admin/creadas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/creadas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/creadas') ? '#9B59B6' : '#fff' }}; color: {{ Request::is('solicitudes-admin/creadas') ? '#FFFFFF' : '#5A738E' }}; ">
                                        CREADAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/autorizadas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/autorizadas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/autorizadas') ? '#F6B855' : '#fff' }}; color: {{ Request::is('solicitudes-admin/autorizadas') ? '#FFFFFF' : '#5A738E' }};">
                                        REVISADAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes/admin') ? 'active' : '' }}">
                                    <a href="/solicitudes/admin" style="display: inline-block; background: {{ Request::is('solicitudes/admin') ? '#1ABB9C' : '#fff' }}; color: {{ Request::is('solicitudes/admin') ? '#FFFFFF' : '#5A738E' }};">
                                        PARA APROBAR
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/transf_tesoreria') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/transf_tesoreria" style="display: inline-block; background: {{ Request::is('solicitudes-admin/transf_tesoreria') ? '#50C1CF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/transf_tesoreria') ? '#FFFFFF' : '#5A738E' }};">
                                        TRANSFERENCIAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 1
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes2') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes2" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes2') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes2') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 2
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes3') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes3" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes3') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes3') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 3
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes4') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes4" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes4') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes4') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 4
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/pendientes5') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/pendientes5" style="display: inline-block; background: {{ Request::is('solicitudes-admin/pendientes5') ? '#ffcc66' : '#fff' }}; color: {{ Request::is('solicitudes-admin/pendientes5') ? '#FFFFFF' : '#5A738E' }};">
                                        PEN - 5
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/tesoreria') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/tesoreria" style="display: inline-block; background: {{ Request::is('solicitudes-admin/tesoreria') ? '#50C1CF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/tesoreria') ? '#FFFFFF' : '#5A738E' }};">
                                        TESORERIA
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/rechazadas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/rechazadas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/rechazadas') ? '#50AACF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/rechazadas') ? '#FFFFFF' : '#5A738E' }};">
                                        RECHAZADAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/rechazadas-tesoreria') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/rechazadas-tesoreria" style="display: inline-block; background: {{ Request::is('solicitudes-admin/rechazadas-tesoreria') ? '#50AACF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/rechazadas-tesoreria') ? '#FFFFFF' : '#5A738E' }};">
                                        RECHAZADAS TESORERIA
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                                <li class="{{ Request::is('solicitudes-admin/todas') ? 'active' : '' }}">
                                    <a href="/solicitudes-admin/todas" style="display: inline-block; background: {{ Request::is('solicitudes-admin/todas') ? '#AAC1CF' : '#fff' }}; color: {{ Request::is('solicitudes-admin/todas') ? '#FFFFFF' : '#5A738E' }};">
                                        TODAS
    <!--                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>-->
                                    </a>
                                </li>
                            @elseif ($userData->global_view == 1 && (\Route::currentRouteName() == 'global.autorizar') || \Route::currentRouteName() == 'global.autorizar.disponibles')
                                <li class="{{ Request::is('global/autorizar') ? 'active' : '' }}">
                                    <a href="{{ route('global.autorizar') }}" style="display: inline-block;">
                                        AUTORIZAR
                                        <span class="badge bg-green" id="bubble_porautorizar" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/autorizar/disponibles') ? 'active' : '' }}">
                                    <a href="{{ route('global.autorizar.disponibles') }}" style="display: inline-block;">
                                        DISPONIBLES
                                        <span class="badge bg-orange" id="bubble_autorizadas" style="display: inline-block">0</span>
                                    </a>
                                </li>
                            @elseif ($userData->global_view == 1 && (\Route::currentRouteName() == 'global.aprobar' || \Route::currentRouteName() == 'global.aprobar.disponibles'))
                                <li class="{{ Request::is('global/aprobar') ? 'active' : '' }}">
                                    <a href="{{ route('global.aprobar') }}" style="display: inline-block;">
                                        APROBAR
                                        <span class="badge bg-green" id="bubble_poraprobar" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/aprobar/disponibles') ? 'active' : '' }}">
                                    <a href="{{ route('global.aprobar.disponibles') }}" style="display: inline-block;">
                                        DISPONIBLES
                                        <span class="badge bg-orange" id="bubble_aprobadas" style="display: inline-block">0</span>
                                    </a>
                                </li>
                            @elseif ($userData->global_view == 1 && (\Route::currentRouteName() == 'global.aprobar_rendiciones' || \Route::currentRouteName() == 'global.aprobar_rendiciones.disponibles'))
                                <li class="{{ Request::is('global/aprobar_rendiciones') ? 'active' : '' }}">
                                    <a href="{{ route('global.aprobar_rendiciones') }}" style="display: inline-block;">
                                        APROBAR RENDICIONES
                                        <span class="badge bg-green" id="bubble_aprrendicion" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/aprobar_rendiciones/disponibles') ? 'active' : '' }}">
                                    <a href="{{ route('global.aprobar_rendiciones.disponibles') }}" style="display: inline-block;">
                                        DISPONIBLES
                                        <span class="badge bg-orange" id="bubble_aprrendiciondisp" style="display: inline-block">0</span>
                                    </a>
                                </li>
                            @elseif ($userData->global_view == 1 && (\Route::currentRouteName() == 'global.pendientes' || \Route::currentRouteName() == 'global.pendientes2' || \Route::currentRouteName() == 'global.pendientes3' || \Route::currentRouteName() == 'global.pendientes4' || \Route::currentRouteName() == 'global.pendientes5'))
                                <li class="{{ Request::is('global/pendientes') ? 'active' : '' }}">
                                    <a href="{{ route('global.pendientes') }}" style="display: inline-block;">
                                        PEN - 1
                                        <span class="badge bg-orange" id="bubble_pendientes" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/pendientes2') ? 'active' : '' }}">
                                    <a href="{{ route('global.pendientes2') }}" style="display: inline-block;">
                                        PEN - 2
                                        <span class="badge bg-orange" id="bubble_pendientes2" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/pendientes3') ? 'active' : '' }}">
                                    <a href="{{ route('global.pendientes3') }}" style="display: inline-block;">
                                        PEN - 3
                                        <span class="badge bg-orange" id="bubble_pendientes3" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/pendientes4') ? 'active' : '' }}">
                                    <a href="{{ route('global.pendientes4') }}" style="display: inline-block;">
                                        PEN - 4
                                        <span class="badge bg-orange" id="bubble_pendientes4" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/pendientes5') ? 'active' : '' }}">
                                    <a href="{{ route('global.pendientes5') }}" style="display: inline-block;">
                                        PEN - 5
                                        <span class="badge bg-orange" id="bubble_pendientes5" style="display: inline-block">0</span>
                                    </a>
                                </li>
                            @elseif ($userData->global_view == 1 && (\Route::currentRouteName() == 'global.revisor_sujeto_rendicion' || \Route::currentRouteName() == 'global.revisor_gasto_directo' || \Route::currentRouteName() == 'global.revisor_fondo_rotativo' || \Route::currentRouteName() == 'global.revisor_reposicion'))
                                <li class="{{ Request::is('global/revisor/sujeto_rendicion') ? 'active' : '' }}">
                                    <a href="{{ route('global.revisor_sujeto_rendicion') }}" style="display: inline-block;">
                                        SUJETO A RENDICIÓN
                                        <span class="badge" id="bubble_sujeto_rendicion" style="display: inline-block; background-color: #1ABC42">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/revisor/gasto_directo') ? 'active' : '' }}">
                                    <a href="{{ route('global.revisor_gasto_directo') }}" style="display: inline-block;">
                                        GASTO DIRECTO
                                        <span class="badge" id="bubble_gasto_directo" style="display: inline-block; background-color: #1ABCBC">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/revisor/fondo_rotativo') ? 'active' : '' }}">
                                    <a href="{{ route('global.revisor_fondo_rotativo') }}" style="display: inline-block;">
                                        FONDO ROTATIVO
                                        <span class="badge" id="bubble_fondo_rotativo" style="display: inline-block; background-color: #1A42BC">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/revisor/reposicion') ? 'active' : '' }}">
                                    <a href="{{ route('global.revisor_reposicion') }}" style="display: inline-block;">
                                        REPOSICIÓN
                                        <span class="badge" id="bubble_reposicion" style="display: inline-block; background-color: #BC6B1A">0</span>
                                    </a>
                                </li>
                            @elseif ($userData->global_view == 1 && 
                                (\Route::currentRouteName() == 'global.en_curso' || 
                                \Route::currentRouteName() == 'global.por_rendir' ||
                                \Route::currentRouteName() == 'global.observadas' ||
                                \Route::currentRouteName() == 'global.rechazadas' ||
                                \Route::currentRouteName() == 'global.rendidas' ||
                                \Route::currentRouteName() == 'global.finalizadas'))
                                <li class="{{ Request::is('global/en_curso') ? 'active' : '' }}">
                                    <a href="{{ route('global.en_curso') }}" style="display: inline-block;">
                                        EN CURSO
                                        <span class="badge bg-green" id="bubble_curso" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/por_rendir') ? 'active' : '' }}">
                                    <a href="{{ route('global.por_rendir') }}" style="display: inline-block;">
                                        POR RENDIR
                                        <span class="badge bg-purple" id="bubble_rendiciones" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/observadas') ? 'active' : '' }}">
                                    <a href="{{ route('global.observadas') }}" style="display: inline-block;">
                                        OBSERVADAS
                                        <span class="badge bg-orange" id="bubble_observadas" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/rechazadas') ? 'active' : '' }}">
                                    <a href="{{ route('global.rechazadas') }}" style="display: inline-block;">
                                        RECHAZADAS
                                        <span class="badge bg-red" id="bubble_rechazadas" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/rendidas') ? 'active' : '' }}">
                                    <a href="{{ route('global.rendidas') }}" style="display: inline-block;">
                                        RENDIDAS
                                        <span class="badge bg-blue-sky" id="bubble_rendidas" style="display: inline-block">0</span>
                                    </a>
                                </li>
                                <li class="{{ Request::is('global/finalizadas') ? 'active' : '' }}">
                                    <a href="{{ route('global.finalizadas') }}" style="display: inline-block;">
                                        FINALIZADAS
                                        <span class="badge bg-blue" id="bubble_finalizadas" style="display: inline-block">0</span>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    @endif
                </div>
                @yield('content')
                <div class="clearfix"></div>
            </div>
            <!-- /page content -->
            <!-- footer content -->
            <footer>
                <div class="text-center">
                    <a href="#">&COPY; 2020 Pragma Invest S.A</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>
    <input type="hidden" name="empresa" id="emp_id" data-id="{{ session('empresa') }}" value="{{ session('empresa') }}">
    <input type="hidden" name="user_rol_id" id="user_rol_id" data-id="{{ $userData->rol_id }}" value="{{ $userData->rol_id }}">
    <input type="hidden" name="user_global_view" id="user_global_view" data-id="{{ $userData->global_view }}" value="{{ $userData->global_view }}">
    @include('parts.javascript')
    @yield('footerScripts')

    <script language="javascript" type="text/javascript">
        datosResumenUsuario();

        function datosResumenUsuario(){
            var emp_id = document.getElementById('emp_id').value;
            var rol_id = document.getElementById('user_rol_id').value;
            var global_view = document.getElementById('user_global_view').value;
            if(rol_id == 8){
                var url = "/json/cantidad/empresas/estados";
                $.getJSON(url, function(data) {
                    $("#datos-adicionales").empty();
                    var bub_adm_poraprobar = document.getElementById('bubble_admin_poraprobar');
                    bub_adm_poraprobar.innerHTML = data['curso'];

                    var bub_adm_aprobadas = document.getElementById('bubble_admin_aprobadas');
                    bub_adm_aprobadas.innerHTML = data['rendiciones'];
                });
            }else if (global_view == 0){
                var url = "/json/cantidad/estados/" + emp_id;
                $.getJSON(url, function(data) {
                    $("#datos-adicionales").empty();

                    if(document.getElementById('bubble_curso')){
                        var bub_curso = document.getElementById('bubble_curso');
                        bub_curso.innerHTML = data['curso'];
                        var bub_porrendir = document.getElementById('bubble_rendiciones');
                        bub_porrendir.innerHTML = data['rendiciones'];
                        var bub_obs = document.getElementById('bubble_observadas');
                        bub_obs.innerHTML = data['observadas'];
                        var bub_rechazadas = document.getElementById('bubble_rechazadas');
                        bub_rechazadas.innerHTML = data['rechazadas'];
                        var bub_rendidas = document.getElementById('bubble_rendidas');
                        bub_rendidas.innerHTML = data['rendidas'];
                        var bub_finalizadas = document.getElementById('bubble_finalizadas');
                        bub_finalizadas.innerHTML = data['finalizadas'];
                    }

                    var bub_porautorizar = document.getElementById('bubble_porautorizar');
                    var bub_autorizadas = document.getElementById('bubble_autorizadas');
                    
                    var bub_revisadas = document.getElementById('bubble_revisadas');
                    // var bub_porrevisar = document.getElementById('bubble_porrevisar');
                    var bub_porrevisarsr = document.getElementById('bubble_porrevisarsr');
                    var bub_porrevisargd = document.getElementById('bubble_porrevisargd');
                    var bub_porrevisarrepo = document.getElementById('bubble_porrevisarrepo');
                    var bub_porrevisarcc = document.getElementById('bubble_porrevisarcc');
                    var bub_porrevisarfra = document.getElementById('bubble_porrevisarfra');
                    var bub_porrevisarfro = document.getElementById('bubble_porrevisarfro');
                    var bub_porrevisarfrc = document.getElementById('bubble_porrevisarfrc');
                    var bub_porrevisarfrm = document.getElementById('bubble_porrevisarfrm');

                    var bub_poraprobar = document.getElementById('bubble_poraprobar');
                    var bub_aprobadas = document.getElementById('bubble_aprobadas');
                    var bub_portesoreria = document.getElementById('bubble_portesoreria');
                    var bub_tesoreria = document.getElementById('bubble_tesoreria');
                    var bub_aprrendir = document.getElementById('bubble_aprrendicion');
                    var bub_aprrendidas = document.getElementById('bubble_aprrendiciondisp');

                    if(bub_aprrendir != null){
                       bub_aprrendir.innerHTML = data['aprrendir'];
                    }

                    if(bub_aprrendidas != null){
                       bub_aprrendidas.innerHTML = data['aprrendidas'];
                    }

                    // var entero = parseInt(data['rol_id']);
                    // if(entero > 2){
                    //     var titulo = "";
                    //     var cadListos = "";
                    //     var cadPor = "";
                    //     var cantidadListos = 0;
                    //     var cantidadPor = 0;
                    //     switch(entero){
                    //         case 3:
                                if(bub_porautorizar != null){
                                    titulo = "Autorizador";
                                    cadListos = "Autorizadas";
                                    cadPor = "Por Autorizar";
                                    bub_porautorizar.innerHTML = data['porautorizar'];
                                    bub_autorizadas.innerHTML = data['autorizadas'];
                                }
                            //     break;
                            // case 4:
                                if(bub_porrevisarsr != null){
                                    titulo = "Revisor Sujeto a Rendición";
                                    cadListos = "Revisadas";
                                    cadPor = "Por Revisar";
                                    bub_porrevisarsr.innerHTML = data['porrevisarsr'];
                                    bub_revisadas.innerHTML = data['revisadas'];
                                }

                                if(bub_porrevisargd != null){
                                    titulo = "Revisor Gasto Directo";
                                    cadListos = "Revisadas";
                                    cadPor = "Por Revisar";
                                    bub_porrevisargd.innerHTML = data['porrevisargd'];
                                    bub_revisadas.innerHTML = data['revisadas'];
                                }

                                if(bub_porrevisarrepo != null){
                                    titulo = "Revisor Reposición";
                                    cadListos = "Revisadas";
                                    cadPor = "Por Revisar";
                                    bub_porrevisarrepo.innerHTML = data['porrevisarrepo'];
                                    bub_revisadas.innerHTML = data['revisadas'];
                                }

                                if(bub_porrevisarcc != null){
                                    titulo = "Revisor Caja Chica";
                                    cadListos = "Revisadas";
                                    cadPor = "Por Revisar";
                                    bub_porrevisarcc.innerHTML = data['porrevisarcc'];
                                    bub_revisadas.innerHTML = data['revisadas'];
                                }

                                if(bub_porrevisarfra != null){
                                    titulo = "Revisor Fondo Rotativo Administativo";
                                    cadListos = "Revisadas";
                                    cadPor = "Por Revisar";
                                    bub_porrevisarfra.innerHTML = data['porrevisarfra'];
                                    bub_revisadas.innerHTML = data['revisadas'];
                                }

                                if(bub_porrevisarfro != null){
                                    titulo = "Revisor Fondo Rotativo Operativo";
                                    cadListos = "Revisadas";
                                    cadPor = "Por Revisar";
                                    bub_porrevisarfro.innerHTML = data['porrevisarfro'];
                                    bub_revisadas.innerHTML = data['revisadas'];
                                }

                                if(bub_porrevisarfrc != null){
                                    titulo = "Revisor Fondo Rotativo Construcción";
                                    cadListos = "Revisadas";
                                    cadPor = "Por Revisar";
                                    bub_porrevisarfrc.innerHTML = data['porrevisarfrc'];
                                    bub_revisadas.innerHTML = data['revisadas'];
                                }
                            //     break;
                            // case 5:
                                if(bub_poraprobar != null){
                                    titulo = "Aprobador";
                                    cadListos = "Aprobadas";
                                    cadPor = "Por Aprobar";
                                    bub_poraprobar.innerHTML = data['poraprobar'];
                                    bub_aprobadas.innerHTML = data['aprobadas'];
                                }
                            //     break;
                            // case 6:
                                if(bub_portesoreria != null){
                                    titulo = "Tesoreria";
                                    cadListos = "Reg. Tesoreria Realizados";
                                    cadPor = "Reg. Tesoreria Faltantes";
                                    bub_portesoreria.innerHTML = data['portesoreria'];
                                    bub_tesoreria.innerHTML = data['tesoreria'];
                                }
                        //         break;
                        // }
                    // }
                });
            } else {
                var url = "/json/cantidad/estados.global";
                
                $.getJSON(url, function(data) {
                    $("#datos-adicionales").empty();

                    // Usuario
                    var bub_curso = document.getElementById('bubble_curso');
                    var bub_porrendir = document.getElementById('bubble_rendiciones');
                    var bub_obs = document.getElementById('bubble_observadas');
                    var bub_rechazadas = document.getElementById('bubble_rechazadas');
                    var bub_rendidas = document.getElementById('bubble_rendidas');
                    var bub_finalizadas = document.getElementById('bubble_finalizadas');

                    if (bub_curso != null && bub_porrendir != null && bub_obs != null && bub_rechazadas != null && bub_rendidas != null && bub_finalizadas != null)
                    {
                        bub_curso.innerHTML = data['curso'];
                        bub_porrendir.innerHTML = data['rendiciones'];
                        bub_obs.innerHTML = data['observadas'];
                        bub_rechazadas.innerHTML = data['rechazadas'];
                        bub_rendidas.innerHTML = data['rendidas'];
                        bub_finalizadas.innerHTML = data['finalizadas'];
                    }

                    // Autorizador
                    var bub_porautorizar = document.getElementById('bubble_porautorizar');
                    var bub_autorizadas = document.getElementById('bubble_autorizadas');

                    if (bub_porautorizar != null && bub_autorizadas != null) {
                        bub_porautorizar.innerHTML = data['porautorizar'];
                        bub_autorizadas.innerHTML = data['autorizadas'];
                    }

                    // Aprobador
                    var bub_poraprobar = document.getElementById('bubble_poraprobar');
                    var bub_aprobadas = document.getElementById('bubble_aprobadas');

                    if (bub_poraprobar != null && bub_aprobadas != null) {
                        bub_poraprobar.innerHTML = data['poraprobar'];
                        bub_aprobadas.innerHTML = data['aprobadas'];
                    }

                    // Aprobador rendiciones
                    var bub_aprrendir = document.getElementById('bubble_aprrendicion');
                    var bub_aprrendidas = document.getElementById('bubble_aprrendiciondisp');

                    if (bub_aprrendir != null && bub_aprrendidas != null) {
                       bub_aprrendir.innerHTML = data['aprrendir'];
                       bub_aprrendidas.innerHTML = data['aprrendidas'];
                    }

                    // Pendientes
                    var bub_pendientes = document.getElementById('bubble_pendientes');
                    var bub_pendientes2 = document.getElementById('bubble_pendientes2');
                    var bub_pendientes3 = document.getElementById('bubble_pendientes3');
                    var bub_pendientes4 = document.getElementById('bubble_pendientes4');
                    var bub_pendientes5 = document.getElementById('bubble_pendientes5');

                    if (bub_pendientes != null && bub_pendientes2 != null && bub_pendientes3 != null && bub_pendientes4 != null && bub_pendientes5 != null) {
                        bub_pendientes.innerHTML = data['pendientes'];
                        bub_pendientes2.innerHTML = data['pendientes2'];
                        bub_pendientes3.innerHTML = data['pendientes3'];
                        bub_pendientes4.innerHTML = data['pendientes4'];
                        bub_pendientes5.innerHTML = data['pendientes5'];
                    }

                    // Revisor
                    var bub_sujeto_rendicion = document.getElementById('bubble_sujeto_rendicion');
                    var bub_gasto_directo = document.getElementById('bubble_gasto_directo');
                    var bub_fondo_rotativo = document.getElementById('bubble_fondo_rotativo');
                    var bub_reposicion = document.getElementById('bubble_reposicion');

                    if (bub_sujeto_rendicion != null && bub_gasto_directo != null && bub_fondo_rotativo != null && bub_reposicion != null) {
                        bub_sujeto_rendicion.innerHTML = data['sujeto_rendicion'];
                        bub_gasto_directo.innerHTML = data['gasto_directo'];
                        bub_fondo_rotativo.innerHTML = data['fondo_rotativo'];
                        bub_reposicion.innerHTML = data['reposicion'];
                    }
                });
            }
        }
    </script>

    <script>
        $(document).ready(function (){
            $('form').submit(function() {
                $(this).find('button[type="submit"]').prop('disabled', true);
                $('button[form="' + $(this).attr('id') + '"]').prop('disabled', true);
            });
        });

        function empresaSelect(){
            var selOption = document.getElementById('empresa_id');
            var valor = parseInt(selOption[selOption.selectedIndex].value);

            @if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0)
            var oldProy = {{ $filters['proyecto_id'] }};
            @else
            var oldProy = 0;
            @endif

            if(valor != 0){
                var url = "/json/proyectos/empresa/" + valor;
                var cad = "<option value='0'>TODOS LOS PROYECTOS</option>";
                $.getJSON(url, function(data) {
                    document.getElementById('proyecto_id').innerHTML = '';
                    $.each(data, function (i, object) {
                        if(oldProy != 0 && oldProy == object.id){
                            cad += "<option value="+object.id+" selected='selected'>"+object.nombre+"</option>";
                        }else{
                            cad += "<option value="+object.id+">"+object.nombre+"</option>";
                        }
                    });
                    document.getElementById('proyecto_id').innerHTML = cad;
                });

                document.getElementById("proyecto_id").disabled = false;
            }else{
                document.getElementById("proyecto_id").disabled = true;
                document.getElementById('proyecto_id').innerHTML = "<option value='0'>NINGUNO</option>";
            }
        }
    </script>

</body>
</html>