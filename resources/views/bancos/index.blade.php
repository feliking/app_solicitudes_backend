@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>
                        Bancos
                        <div class="crear_empresa pull-right">
                            <a class="btn btn-success" href="{{ route('bancos.create') }}">Nuevo Banco</a>
                        </div>
                    </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Sigla</th>
                                <th>Nombre</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bancos as $banco)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $banco->codigo }}</td>
                                <td>{{ $banco->nombre }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection