@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Nuevo Banco</h2></div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'bancos.store','method'=>'POST', 'class'=>'form-horizontal')) !!}
                        <div class="form-group{{ $errors->has('codigo') ? ' has-error' : '' }}">
                            <label for="codigo" class="col-md-4 control-label">Sigla del Banco</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="codigo" placeholder="SIGLA DEL BANCO" style="text-transform: uppercase">
                                @if ($errors->has('codigo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('codigo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                            <label for="nombre" class="col-md-4 control-label">Nombre del Banco</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nombre" placeholder="NOMBRE DEL BANCO" style="text-transform: uppercase">
                                @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-offset-4">
                                {!! Form::submit('Crear Banco', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection