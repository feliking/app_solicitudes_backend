<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0"/>
    </head>

    <body>

        <h2><i>Número de Solicitud :</i> {{ $num_solicitud }}</h2>
        <h3><i>Empresa :</i> {{ $empresa }}</h3>
        <h3><i>Proyecto :</i> {{ $proyecto }}</h3>
        <h3><i>Referencia :</i> {{ $referencia }}</h3>
        <hr/>

        <span><i>* Puede realizar el seguimiento de las solicitudes con el siguiente enalce <a href="http://pragmasolicitudes.pragmainvest.com">clic aquí</a></i></span>
        <br/>
        <span><i>* Si requiere de asistencia, comuniquese con el número piloto 1610 o envía un correo electrónico a <a href="mailto:soporte@pragmainvest.com.bo">soporte@pragmainvest.com.bo</a></i></span>
    </body>
</html>