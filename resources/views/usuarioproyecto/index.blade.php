@extends('main')

@section('content')
<?php include_once(app_path().'/functions.php')?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>ASIGNACIÓN DE PROYECTOS A USUARIO</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre Completo</th>
                                <th>Rol</th>
                                <th>Asignar</th>
                            </tr>
                        </thead>
                        <?php $contador = 1?>
                        <tbody>
                            @foreach($usuarios as $usuario)
                            <tr>
                                <td><?=$contador++;?>{{ " (".$usuario->id.")" }}</td>
                                <td>{{$usuario->nombre." ".$usuario->ap." ".$usuario->am}}</td>
                                <td>{{$usuario->rol->nombre}}</td>
                                <td>
                                    <i class="fa fa-check-circle-o icono-opciones icono-green" data-toggle="modal" data-target="#listadoProyectos" onclick="listado_proyectos({{$usuario->id}})"></i>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- MODAL -->
            <button type="button" id="btn-modal-solicitud" class="btn btn-success btn-block" data-toggle="modal" data-target="#listadoProyectos" style="display:none">Modal solicitud</button>
            <div class="modal fade" id="listadoProyectos" tabindex="-1" role="dialog" aria-labelledby="listadoProyectos">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="favoritesModalLabel"><strong>ASIGNAR PROYECTOS <span id="modal-num"></span></strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="pdf-solicitud-title">
                                <h1></h1>
                                <h2>USUARIO</h2>
                            </div>
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>LISTADO DE PROYECTOS</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered table-hover" id="reporte-solicitud">
                                        <thead>
                                            <th style="width:50px">#</th>
                                            <th>Nombre</th>
                                            <th style="width:50px">Asignar</th>
                                        </thead>
                                        <?php
                                        foreach($usuarios as $usuario){
                                            $contador = 1;?>
                                            {!! Form::open(array('action' => 'UsuarioController@UsuarioProyectoStore', 'method'=>'POST', 'class'=>'form-inline')) !!}
                                            <tbody id="proys_usuario_<?=$usuario->id?>" style="display:none;">
                                                <?php
                                                foreach($proyectos as $proyecto){
                                                    ?>
                                                    <tr>
                                                        <td><?=$contador++?></td>
                                                        <td><?=$proyecto['nombre'];?></td>
                                                        <td style='text-align:center;'>
                                                            <?php
                                                            if(in_array($proyecto['id'], $usuario_proyectos[$usuario->id])){
                                                                ?>
                                                                {{-- {!! Form::checkbox('proyecto_'.$usuario->id.'[]', $proyecto['id'], true, ['id' => 'proyecto_'.$proyecto['id'], 'onclick' => 'verificar('.$proyecto['id'].','.$usuario->id.', this)']); !!} --}}
                                                                {!! Form::checkbox('proyecto_'.$usuario->id.'[]', $proyecto['id'], true, ['id' => 'proyecto_'.$proyecto['id']]); !!}
                                                            <?php
                                                            }else{
                                                                ?>
                                                                {!! Form::checkbox('proyecto_'.$usuario->id.'[]', $proyecto['id'], false, ['id' => 'proyecto_'.$proyecto['id']]); !!}
                                                            <?php
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }?>
                                                <tr>
                                                    <td colspan="3">
                                                        <input id="usuario_id_<?=$usuario->id?>" name="usuario_id" type="hidden"/>
                                                        <div class="modal-footer no-print">
                                                            {!! Form::submit('GUARDAR ASIGNACIÓN', ['class' => 'btn btn-success']) !!}
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                                                            <span class="pull-right" id="print-span"></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            {!! Form::close() !!}
                                            <?php
                                        }?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            
            {{-- BEGIN: ISSUE 0001 --}}            
            <div class="modal fade" id="modal-advertencia">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-info">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title text-center">Información</h4>
                        </div>
                        <div class="modal-body">
                            No puede quitar el proyecto al usuario
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- END: ISSUE 0001 --}}
            <script>
                function listado_proyectos(e){
                    for (i = 0; i < 5000; i++) {
                        if(document.getElementById('proys_usuario_'+i)){
                            document.getElementById('proys_usuario_'+i).style.display = "none";
                        }
                    }
                    element = document.getElementById('proys_usuario_'+e);
                    element.style.display = "";
                    usuario = document.getElementById('usuario_id_'+e);
                    usuario.value = e;
                    //file.setAttribute("type", "button");
                    <?php
                    ob_start();
                    
                    $content = ob_get_clean();
                    ?>
                }

                /**
                 * BEGIN: ISSUE 0001
                 */
                function verificar (proyecto_id, usuario_id, obj) {                    
                    var url_usuario = '/json/proyectos_usuario/'+proyecto_id+'/'+usuario_id;
                    $.getJSON(url_usuario, function(cantidad) {
                        if(parseInt(cantidad['valor']) > 0){                            
                            $($(obj)).prop("checked", true);
                            $('#modal-advertencia').modal('show');                                
                        }
                    });
                }    
                /**
                 * END: ISSUE 0001
                 */
            </script>
        </div>
    </div>
</div>
@endsection

