{!! Form::label('buscar', 'BUSCAR: ', ['id' => 'lblBuscar']) !!}
{!! Form::text('buscar', null, ['placeholder' => 'TEXTO A BUSCAR', 'class' => 'form-control', 'style' => 'text-transform:uppercase', 'id' => 'buscar']) !!}
{!! Form::number('numero', null, ['placeholder' => 'INTRODUZCA CANTIDAD', 'class' => 'form-control', 'style' => 'display: none;', 'id' => 'numero']) !!}
{!! Form::date('fechaIni', null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'text-transform:uppercase; display: none; width: 135px; padding: 6px;', 'id' => 'fechaIni']) !!}
{!! Form::label('fechaFin', 'HASTA: ', ['id' => 'lblFechaFin', 'style' => 'display: none;']) !!}
{!! Form::date('fechaFin', null, ['placeholder' => '', 'class' => 'form-control', 'style' => 'text-transform:uppercase; display: none; width: 135px; padding: 6px;', 'id' => 'fechaFin']) !!}
<div class="input-group" style="margin-bottom: 0px; width: 230px;">
    {!! Form::select('selectOpcion', ['TODOS', 'NRO. DE CHEQUE', 'MONTO', 'FECHA', 'BENEFICIARIO', 'BANCO', 'NRO. DE CUENTA', 'CONCEPTO'], 0, ['class' => 'form-control', 'onchange' => 'selOpcion()', 'id' => 'selectOpcion', 'style' => 'padding: 6px;']) !!}
    <span class="input-group-btn" style="width:0;">
        {!! Form::submit('BUSCAR', ['class' => 'btn btn-success']) !!}
    </span>
</div>

@section('footerScripts')
@parent
    <script type="text/javascript">
        function selOpcion(){
            var selId = document.getElementById('selectOpcion');

            var lblBuscar = document.getElementById('lblBuscar');
            var inputBuscar = document.getElementById('buscar');
            var inputNumero = document.getElementById('numero');
            var inputFechaIni = document.getElementById('fechaIni');
            var lblFechaFin = document.getElementById('lblFechaFin');
            var inputFechaFin = document.getElementById('fechaFin');
            var selOpcion = document.getElementById('selectOpcion');
            if(selId.value == 0 || selId.value == 4 || selId.value == 5 || selId.value == 6){
                //0 Todos
                //4 Beneficiario
                //5 Banco
                //6 Cuenta
                lblBuscar.innerHTML = "BUSCAR: ";
                inputBuscar.style.display = "initial";
                inputNumero.style.display = "none";
                inputFechaIni.style.display = "none";
                lblFechaFin.style.display = "none";
                inputFechaFin.style.display = "none";

            }
            if(selId.value == 1 || selId.value == 2){
                //1 Numero de solicitud
                //2 Monto
                lblBuscar.innerHTML = "BUSCAR: ";
                inputBuscar.style.display = "none";
                inputNumero.style.display = "initial";
                inputFechaIni.style.display = "none";
                lblFechaFin.style.display = "none";
                inputFechaFin.style.display = "none";
            }
            if(selId.value == 3){
                //3 Fecha
                lblBuscar.innerHTML = "DESDE: ";
                inputBuscar.style.display = "none";
                inputNumero.style.display = "none";
                inputFechaIni.style.display = "initial";
                lblFechaFin.style.display = "initial";
                inputFechaFin.style.display = "initial";
            }
        }
    </script>
@endsection