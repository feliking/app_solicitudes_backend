@extends('main')

@section('headerScripts')
<script src="/js/literal.js"></script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-4">
                            <h2>CHEQUES</h2>
                        </div>
                        <div class="col-md-8">
<!--                        {!! Form::open(['action' => 'ChequesController@index', 'method'=>'POST', 'class'=>'form-inline']) !!}
                            @include('cheques.buscador')
                        {!! Form::close() !!}-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-info alert-dismissible fade in">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <div id="filtro-info">BUSQUEDA {{ ($busqueda == "")?"'NINGUNA'":$busquedatexto }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body panel-solicitud">
                    <div  style="overflow-x: scroll;">
                        <table class="table table-bordered table-hover table-solicitudes">
                            <thead>
                                <th>OPCIONES</th>
                                <th>@sortablelink('fecha', 'FECHA')</th>
                                <th>@sortablelink('numero_cheque', 'NRO. CHEQUE')</th>
                                <th>@sortablelink('solicitud_id', 'NRO. SOLICITUD')</th>
                                <th>@sortablelink('banco_id', 'BANCO')</th>
                                <th>@sortablelink('cuenta_id', 'NRO. CUENTA')</th>
                                <th>@sortablelink('monto', 'MONTO')</th>
                                <th>@sortablelink('beneficiario', 'BENEFICIARIO')</th>
                                <th>@sortablelink('modalidad_id', 'CONCEPTO')</th>
                                <th>ESTADO</th>
                            </thead>
                            <tbody>
                            @foreach($cheques as $cheque)
                                <tr>
                                    <td>
                                        <a id="sol_{{ $cheque->solicitud_id }}" class="fa fa-file-text icono-opciones-files icono-green" onclick="seleccion_solicitud(this)" data-target="#solicitudModal" data-toggle="modal" title="Ver Solicitud"></a>
                                        <i class="fa fa-file-archive-o icono-opciones-files icono-green" title="Registro de Tesoreria" onclick="registroModal(<?=($cheque->id)?>,<?=($cheque->solicitud_id)?>)"></i>
                                        @php
                                            $ultimoEstado = DB::table('estados_solicitud')->where('solicitud_id', $cheque->solicitud_id)
                                                ->orderBy('id', 'DESC')
                                                ->first()
                                                ->estado_id;
                                        @endphp

                                        @if($ultimoEstado != 62 && $ultimoEstado != 70 && $ultimoEstado != 82 && $ultimoEstado != 90)
                                        <i class="fa fa-times-circle-o icono-opciones-files icono-red" title="Anular cheque" onclick="anularModal(<?=($cheque->id)?>,<?=($cheque->solicitud_id)?>)"></i>
                                        @endif
                                    </td>
                                    <td>{{ $cheque->fecha }}</td>
                                    <td>{{ $cheque->numero_cheque }}</td>
                                    <td>{{ $cheque->solicitud->numero }}</td>
                                    <td>{{ $cheque->cuenta->banco->nombre }}</td>
                                    <td>{{ $cheque->cuenta->numero." (".$cheque->cuenta->moneda->sigla.")" }}</td>
                                    <td id="total_tes_{{ $cheque->id }}" style="text-align: right;">{{ number_format($cheque->monto, '2', '.', ',') }}</td>
                                    <td>{{ $cheque->beneficiario }}</td>
                                    <td>{{ $cheque->concepto }}</td>
                                    <td>{{ $cheque->nombre }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $cheques->links() }}
                </div>
            </div>
            <!-- MODAL -->
            <div class="modal fade" id="solicitudModal" tabindex="-1" role="dialog" aria-labelledby="solicitudModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="solicitudModalLabel"><strong>DATOS DE LA SOLICITUD # <span id="modal-num"></span></strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="pdf-solicitud-title">
                                <h1><?=session('empresa_nombre');?></h1>
                            </div>
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>INFORMACIÓN SOLICITUD</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered" id="reporte-solicitud">
                                        <tr>
                                            <td><label>PROYECTO: </label></td>
                                            <td colspan="5" id='modal-proyecto'></td>
                                            <td><label>Fecha Limite: </label></td>
                                            <td id="modal-fecha-lim"></td>
                                        </tr>
                                        <tr>
                                            <td><label>SOLICITANTE: </label></td>
                                            <td colspan="9" id="modal-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='modal-desembolso'></td>
                                        </tr>
                                        <tr>
                                            <td><label>TIPO DE CAMBIO: </label></td>
                                            <td id='modal-cambio'></td>
                                            <td><label>MODALIDAD: </label></td>
                                            <td colspan="3" id='modal-modalidad'></td>
                                            <td><label>MONEDA: </label></td>
                                            <td colspan="3" id='modal-moneda'></td>
                                        </tr>
                                    </table>
                                    <div >
                                        <h4>ITEMS SOLICITUD</h4>
                                        <table class="table table-bordered table-items" id="table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='modal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="modal-literal"></span> CON <span id="modal-decimal"></span>/100 <span id="modal-literal-moneda"></p>
                                        <table class="table table-bordered" id="a">
                                            <tr>
                                                <td style="width: 100px"><label>REFERENCIA: </label></td>
                                                <td id='modal-referencia'></td>
                                            </tr>
                                            <tr>
                                                <td><label>JUSTIFICACIÓN: </label></td>
                                                <td id='modal-observacion'></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer no-print"><!-- ELIMINAR DISPLAY NONE -->
                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            <span class="pull-right" id="print-span"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- MODAL REGISTRO TESORERIA-->
            <button type="button" name="btn-registro" id="btn-registro" data-toggle="modal" data-target="#registroModal" class="btn btn-default" style="display:none;">Registro Modal</button>
            <div class="modal fade" id="registroModal" tabindex="-1" role="dialog" aria-labelledby="registroModalLabel">
                <div class="modal-dialog modal-lg " role="document">
                    <div class="modal-content">
                        <div class="modal-header no-print">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center text-success" id="registroModalLabel"><strong>REGISTRO DE TESORERIA SOLICITUD # <span id="regmodal-num"></span></strong></h4>
                        </div>
                        <div class="modal-body">
                            <div class="pdf-solicitud-title">
                                <h1><?=session('empresa_nombre');?></h1>
                            </div>
                            <div class="panel panel-default ">
                                <div class="panel-heading"><h2>DATOS SOLICITUD</h2></div>
                                <div class="panel-body">
                                    <table class="table table-bordered registro-solicitud" id="registro-solicitud">
                                        <tr>
                                            <td><label>PROYECTO: </label></td>
                                            <td colspan="5" id='regmodal-proyecto'></td>
                                            <td><label>Fecha Limite: </label></td>
                                            <td id="regmodal-fecha"></td>
                                        </tr>
                                        <tr>
                                            <td><label>SOLICITANTE: </label></td>
                                            <td colspan="9" id="regmodal-solicitante"></td>
                                        </tr>
                                        <tr>
                                            <td><label for="grupo">DESEMBOLSO A: </label></td>
                                            <td colspan="9" id='regmodal-desembolso'></td>
                                        </tr>
                                        <tr>
                                            <td><label>TIPO DE CAMBIO: </label></td>
                                            <td id='regmodal-cambio'></td>
                                            <td><label>MODALIDAD: </label></td>
                                            <td colspan="3" id='regmodal-modalidad'></td>
                                            <td><label>MONEDA: </label></td>
                                            <td colspan="3" id='regmodal-moneda'></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <h4>ITEMS</h4>
                                        <table class="table table-bordered table-items" id="reg_table_items">
                                            <thead>
                                                <tr>
                                                    <th style="width:10px">#</th>
                                                    <th style="width:20px">UNIDAD</th>
                                                    <th>DETALLE</th>
                                                    <th style="width:20px">CANTIDAD</th>
                                                    <th style="width:20px">P. UNITARIO</th>
                                                    <th style="width:20px">SUB TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody id='regmodal-items'>
                                            </tbody>
                                        </table>
                                        <p><strong>SON:</strong> <span id="regmodal-literal"></span> CON <span id="regmodal-decimal"></span>/100 <span id="regmodal-literal-moneda"></span></p>
                                        <table class="table table-bordered" id="a">
                                            <tr>
                                                <td style="width: 100px"><label>REFERENCIA: </label></td>
                                                <td id='regmodal-referencia'></td>
                                            </tr>
                                            <tr>
                                                <td><label>JUSTIFICACIÓN: </label></td>
                                                <td id='regmodal-observacion'></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>CHEQUES</h2>
                                </div>
                                <div class="panel-body datos-cheque-body"></div>
                            </div>
                        </div>
                        <div class="modal-footer no-print"><!-- ELIMINAR DISPLAY NONE -->
                            <button type="button" class="btn btn-default" data-dismiss="modal">CERRAR</button>
                            <span class="pull-right" id="print-span"></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MODAL -->
            <!-- Anular Modal-->
            <button type="button" name="btn-anular" id="btn-anular" data-toggle="modal" data-target="#anularModal" class="btn btn-default" style="display:none;">Anular Cheque</button>
            <div class="modal fade" id="anularModal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title anular-title">Anular cheque #</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>DATOS SOLICITUD</h2>
                                </div>
                                <div class="panel-body anular-solicitud-body"></div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>ITEMS SOLICITUD</h2>
                                </div>
                                <div class="panel-body anular-solicitud-item-body">
                                    <table class="table table-bordered table-items" id="table_items">
                                        <thead>
                                            <tr>
                                                <th style="width:10px">#</th>
                                                <th style="width:20px">UNIDAD</th>
                                                <th>DETALLE</th>
                                                <th style="width:20px">CANTIDAD</th>
                                                <th style="width:20px">P. UNITARIO</th>
                                                <th style="width:20px">SUB TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody id='modal-items'>
                                        </tbody>
                                    </table>
                                    <p><strong>SON:</strong> <span id="modalrechazo-literal"></span> CON <span id="modalrechazo-decimal"></span>/100 <span id="modalrechazo-literal-moneda"></span></p>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2>DATOS CHEQUE</h2>
                                </div>
                                <div class="panel-body anular-datos-cheque-body"></div>
                            </div>
                        </div>
                        <div class="modal-footer" id="modal-footer-eliminar">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Anular Modal-->
        </div>
    </div>
</div>
@endsection

@section('footerScripts')
<script language="javascript" type="text/javascript">
    function seleccion_solicitud(e){
        var array_sol = e.id.split('_');
        var solicitud_id = array_sol[1];
        var id_fila = solicitud_id;
        var url_solicitud = '/json/datos_solicitud/'+solicitud_id;
        var url_solicitud_items = '/json/items_solicitud/'+solicitud_id;
        $('.anular-solicitud-body').empty();
        $.getJSON(url_solicitud, function(datos){
            document.getElementById('modal-solicitante').innerHTML = datos.usuario;
            document.getElementById('modal-proyecto').innerHTML = datos.proyecto;
            document.getElementById('modal-desembolso').innerHTML = datos.desembolso;
            document.getElementById('modal-cambio').innerHTML = datos.tipo_cambio;
            document.getElementById('modal-num').innerHTML = datos.numero;
            document.getElementById('modal-modalidad').innerHTML = datos.modalidad;
            document.getElementById('modal-moneda').innerHTML = datos.moneda;
            document.getElementById('modal-referencia').innerHTML = datos.referencia;
            document.getElementById('modal-observacion').innerHTML = datos.observacion;

            document.getElementById('modal-literal-moneda').innerHTML = datos.moneda;
        });

        $("#table_items tbody").empty();
        var total_items = 0;
        $.getJSON(url_solicitud_items, function(items){
            $("#table_items tbody").empty();
            $.each(items, function(i, object){
                fila = "<tr>";
                fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                fila += "<td>"+object.unidad_descripcion+"</td>";
                fila += "<td>"+object.detalle+"</td>";
                fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                fila += "<td style='text-align:right;'>"+(parseFloat(object.subtotal)).toFixed(2)+"</td>";
                fila += "</tr>";
                $("#table_items tbody").append(fila);
                total_items += parseFloat(object.subtotal);
            });
            fila = "<tr>";
            fila += "<td colspan='5' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            fila += "<td style='text-align:right;'>"+total_items.toFixed(2)+"</td>";
            fila += "</tr>";
            $("#table_items tbody").append(fila);

            var total = total_items.toFixed(2);
            var res = total.split(".");
            var p_entera = res[0];
            var p_decimal = res[1];
            var str_decimal = '00';

            if(p_entera != ""){
                var str_literal = numeroLiteral(p_entera);
                document.getElementById('modal-literal').innerHTML = str_literal.toUpperCase();
            }

            if(p_decimal>0 && p_decimal <10){
                str_decimal = '0'+str_decimal;
            }else if(p_decimal > 9){
                str_decimal = res[1];
            }
            document.getElementById('modal-decimal').innerHTML = str_decimal;
        });        

        var span_p = document.getElementById('print-span');
        span_p.innerHTML = "";
        var file = document.createElement("button");
        file.setAttribute("type", "button");
        file.setAttribute("class", "btn btn-primary");
        file.setAttribute("id", id_fila);
        file.innerHTML = "IMPRIMIR";
        file.setAttribute("onclick", "imprimirSolicitud(this)");
        span_p.appendChild(file);
    }

    function anularModal(cheque_id, solicitud_id){
        var url_solicitud = '/json/datos_solicitud/'+solicitud_id;
        var url_solicitud_items = '/json/items_solicitud/'+solicitud_id;
        $('.anular-solicitud-body').empty();
        $.getJSON(url_solicitud, function(datos){
            var tabla = "<table class='table table-bordered' id='reporte-solicitud'>";
            tabla += "<tr>";
            tabla += "<td>PROYECTO</td><td>"+datos.proyecto+"</td>";
            tabla += "<td>SUBPROYECTO</td><td>"+datos.proyecto+"</td>";
            tabla += "<td>FECHA LIMITE</td><td>"+datos.fecha_limite+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>SOLICITANTE</td><td colspan='5'>"+datos.usuario+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>DESEMBOLSO</td><td colspan='5'>"+datos.desembolso+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>TIPO DE CAMBIO</td><td>"+datos.tipo_cambio+"</td>";
            tabla += "<td>MODALIDAD</td><td>"+datos.modalidad+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>REFERENCIA</td><td colspan='5'>"+datos.referencia+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>JUSTIFICACIÓN</td><td colspan='5'>"+datos.observacion+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>TOTAL</td><td colspan='5'>"+datos.total+" "+datos.moneda+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>NUMERO DE SOLICITUD</td><td colspan='5'>"+datos.numero+"</td>";
            tabla += "</tr><tr>";
            if(datos.autorizador != null){
                tabla += "<td>AUTORIZADOR</td><td colspan='5'>"+datos.autorizador+"</td>";
                tabla += "</tr><tr>";
                tabla += "<td>FECHA AUTORIZACION</td><td colspan='5'>"+datos.fecha_autorizacion+"</td>";
                tabla += "</tr>";
            }
            document.getElementById('modalrechazo-literal-moneda').innerHTML = datos.moneda;
            
            tabla +="</table>";
            $('.anular-solicitud-body').append(tabla);
        });

        $("#table_items tbody").empty();
        var total_items = 0;
        $.getJSON(url_solicitud_items, function(items){
            $("#table_items tbody").empty();
            $.each(items, function(i, object){
                fila = "<tr>";
                fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                fila += "<td>"+object.unidad_descripcion+"</td>";
                fila += "<td>"+object.detalle+"</td>";
                fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                fila += "<td style='text-align:right;'>"+(parseFloat(object.subtotal)).toFixed(2)+"</td>";
                fila += "</tr>";
                $("#table_items tbody").append(fila);
                total_items += parseFloat(object.subtotal);
            });
            fila = "<tr>";
            fila += "<td colspan='5' style='text-align:right; font-weight:bold;'>TOTAL</td>";
            fila += "<td style='text-align:right;'>"+total_items.toFixed(2)+"</td>";
            fila += "</tr>";

            $("#table_items tbody").append(fila);

            var total = total_items.toFixed(2);
            var res = total.split(".");
            var p_entera = res[0];
            var p_decimal = res[1];
            var str_decimal = '00';
            if(p_decimal){
                str_decimal = p_decimal;
            }
            document.getElementById('modalrechazo-decimal').innerHTML = str_decimal;

            if(p_entera != ""){
                var str_literal = numeroLiteral(p_entera);
                document.getElementById('modalrechazo-literal').innerHTML = str_literal.toUpperCase();
            }
        });

        var url_solicitud = '/json/datos/cheque/'+cheque_id;
        var sol_id;
        $('.anular-datos-cheque-body').empty();
        $.getJSON(url_solicitud, function(datos){
            var tabla = "<table class='table table-bordered' id='reporte-solicitud'>";
            tabla += "<tr>";
            tabla += "<td>BANCO</td><td>"+datos.cuenta.banco.nombre+"</td>";
            tabla += "<td>NRO. CUENTA</td><td>"+datos.cuenta.numero+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>MONTO</td><td>"+datos.monto+"</td>";
            tabla += "<td>MONEDA</td><td>"+datos.cuenta.moneda.nombre+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>DESEMBOLSO</td><td>"+datos.beneficiario+"</td>";
            tabla += "<td>FECHA</td><td>"+datos.fecha+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>OBSERVACION</td><td colspan='4'>"+datos.observacion+"</td>";
            tabla += "</tr><tr>";
            tabla += "<td>EMITIDO POR</td><td colspan='4'>"+datos.solicitud.usuario.nombre+"</td>";
            tabla += "</tr><tr>";
            tabla +="</table>";
            $('.anular-datos-cheque-body').append(tabla);
            sol_id = datos.solicitud_id;

            $('.anular-title').empty();
        $('.anular-title').append("Anular Cheque #"+datos.numero_cheque);
        });

        var btn_modal = document.getElementById('btn-anular');
        btn_modal.click();
        var opciones = document.getElementById('modal-footer-eliminar');
        $('#modal-footer-eliminar').empty();
        var btn_anular_solicitud = document.createElement("button");
        btn_anular_solicitud.setAttribute("type", "button");
        btn_anular_solicitud.setAttribute("onclick", "anularChequeSolicitud("+cheque_id+", "+solicitud_id+")");
        btn_anular_solicitud.setAttribute("data-dismiss", "modal");
        btn_anular_solicitud.setAttribute("class", "btn btn-primary");
        btn_anular_solicitud.innerHTML = "ANULAR SOLICITUD/CHEQUE";

        var btn_anular_cheque = document.createElement("button");
        btn_anular_cheque.setAttribute("type", "button");
        
        btn_anular_cheque.setAttribute("data-dismiss", "modal");
        btn_anular_cheque.setAttribute("class", "btn btn-primary");
        btn_anular_cheque.innerHTML = "ANULAR CHEQUE";
        btn_anular_cheque.setAttribute("onclick", "anularCheque("+cheque_id+", "+solicitud_id+")");
        var btn_cerrar = document.createElement("button");
        btn_cerrar.setAttribute("type", "button");
        btn_cerrar.setAttribute("data-dismiss", "modal");
        btn_cerrar.setAttribute("class", "btn btn-default");
        btn_cerrar.innerHTML = "CERRAR";

        opciones.append(btn_anular_solicitud);
        opciones.append(btn_anular_cheque);
        opciones.append(btn_cerrar);
    }

    function anularCheque(cheque_id, solicitud_id){
        var url = "cheque/anular?cheque_id="+cheque_id+"&solicitud_id="+solicitud_id;
        window.location = url;
    }

    function anularChequeSolicitud(cheque_id, solicitud_id){
        var url = "cheque/anular/solicitud?cheque_id="+cheque_id+"&solicitud_id="+solicitud_id;
        window.location = url;
    }

    function registroModal(cheque_id, solicitud_id){
        var url_solicitud = '/json/datos/cheque/'+solicitud_id;
        var sol_id;
        $('.datos-cheque-body').empty();
        $.getJSON(url_solicitud, function(datos){
            
            var tabla = "<table class='table table-bordered'>";
            tabla += "<thead><tr>";
            tabla += "<th>N°</th>";
            tabla += "<th>BANCO</th>";
            tabla += "<th>Nº CUENTA</th>";
            tabla += "<th>BENEFICIARIO</th>";
            tabla += "<th>Nº DE CHEQUE</th>";
            tabla += "<th>FECHA</th>";
            tabla += "<th>MONTO</th>";
            tabla += "<th>CONCEPTO</th>";
            tabla += "<th>OBSERVACIONES</th>";
            tabla += "</tr></thead>";
            tabla += "<tbody>";
            var total = 0;
            var mon_value = "";
            var contador = 1;

            $.each(datos, function (i, object) {
                tabla += "<tr>";
                tabla += "<td>"+contador+"</td>";
                tabla += "<td>"+object.banco+"</td>";
                tabla += "<td>"+object.cuenta+"</td>";
                tabla += "<td>"+object.cheque_beneficiario+"</td>";
                tabla += "<td>"+object.cheque_nro+"</td>";
                tabla += "<td>"+object.cheque_fecha+"</td>";
                tabla += "<td>"+object.cheque_monto+"</td>";
                tabla += "<td>"+object.cheque_concepto+"</td>";
                tabla += "<td>"+object.cheque_observacion+"</td>";
                tabla += "</tr>";
                mon_value = object.moneda;
                total += parseFloat(object.cheque_monto);
                contador++;
            });

            console.log(datos);
            tabla += "</tbody>";
            tabla += "</table>";
            $('.datos-cheque-body').append(tabla);
            document.getElementById('regmodal-literal-moneda').innerHTML = mon_value;
            sol_id = solicitud_id;
        }).done(function(){
            var url_solicitud = '/json/datos_solicitud/'+sol_id;
            $('.registro-solicitud').empty();
            console.log(url_solicitud);
            var num_sol;
            $.getJSON(url_solicitud, function(datos){
                var tabla = "<tr>";
                tabla += "<td><label>PROYECTO</label></td><td colspan='3'>"+datos.proyecto+"</td>";
                tabla += "</tr><tr>";
                tabla += "<td><label>DESEMBOLSO</label></td><td colspan='3'>"+datos.desembolso+"</td>";
                tabla += "</tr><tr>";
                tabla += "<td><label>SOLICITANTE</label></td><td colspan='3'>"+datos.usuario+"</td>";
                tabla += "</tr><tr>";
                tabla += "<td><label>FECHA LIMITE</label></td><td colspan='3'>"+datos.fecha_limite+"</td>";
                tabla += "</tr><tr>";
                tabla += "<td><label>MODALIDAD</label></td><td colspan='3'>"+datos.modalidad+"</td>";
                tabla += "</tr><tr>";
                if(datos.moneda == "BOLIVIANOS"){
                    tabla += "<td><label>TOTAL</label></td><td colspan='3'>"+datos.total+" "+datos.moneda+"</td>";
                }else{
                    tabla += "<td><label>TOTAL</label></td><td>"+datos.total+" "+datos.moneda+"</td>";
                    tabla += "<td><label>TIPO DE CAMBIO</label></td><td colspan='2'>"+datos.tipo_cambio+"</td>";
                }
                tabla += "</tr><tr>";
                if(datos.autorizador != null){
                    tabla += "<td><label>AUTORIZADOR</label></td><td colspan='3'>"+datos.autorizador+"</td>";
                    tabla += "<td><label>FECHA</label></td><td>"+datos.fecha_autorizacion+"</td>";
                }
                tabla += "</tr>";
                num_sol = datos.numero;
                $('.registro-solicitud').append(tabla);
                document.getElementById('regmodal-referencia').innerHTML = datos.referencia;
                document.getElementById('regmodal-observacion').innerHTML = datos.observacion;
                document.getElementById('regmodal-num').innerHTML = num_sol;
            }).done(function(){
                var url_solicitud_items = '/json/items_solicitud/'+sol_id;
                var fila;
                var total_items = 0;
                $('#regmodal-items').empty();
                $.getJSON(url_solicitud_items, function(items){
                    $.each(items, function(i, object){
                        fila = "<tr>";
                        fila += "<td style='text-align:center;'>"+(i+1)+"</td>";
                        fila += "<td>"+object.unidad_descripcion+"</td>";
                        fila += "<td>"+object.detalle+"</td>";
                        fila += "<td style='text-align:center;'>"+object.cantidad+"</td>";
                        fila += "<td style='text-align:right;'>"+object.costo+"</td>";
                        fila += "<td style='text-align:right;'>"+object.subtotal.toFixed(2)+"</td>";
                        fila += "</tr>";
                        $('#regmodal-items').append(fila);
                        total_items += parseFloat(object.subtotal);
                    });
                    fila = "<tr>";
                    fila += "<td colspan='5' style='text-align:right; font-weight:bold;'>TOTAL</td>";
                    fila += "<td style='text-align:right;'>"+total_items.toFixed(2)+"</td>";
                    fila += "</tr>";
                    $("#regmodal-items").append(fila);

                    var total = total_items.toFixed(2);
                    var res = total.split(".");
                    var p_entera = res[0];
                    var p_decimal = res[1];
                    var str_decimal = '00';
                    if(p_decimal){
                        str_decimal = p_decimal;
                    }
                    document.getElementById('regmodal-decimal').innerHTML = str_decimal;

                    if(p_entera != ""){
                        var str_literal = numeroLiteral(p_entera);
                        document.getElementById('regmodal-literal').innerHTML = str_literal.toUpperCase();
                    }
                });
            });
        });
        var btn_modal = document.getElementById('btn-registro');
        btn_modal.click();
    }
</script>
@endsection