@extends('main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>
                        Cuentas Bancarias
                        <div class="crear_empresa pull-right">
                            <a class="btn btn-success" href="{{ route('cuentas.create') }}">Nueva Cuenta</a>
                        </div>
                    </h2>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Banco</th>
                                <th>Número</th>
                                <th>Monto Inicial</th>
                                <th>Saldo</th>
                                <th>Moneda</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cuentas as $cuenta)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $cuenta->banco->nombre }}</td>
                                <td>{{ $cuenta->numero }}</td>
                                <td>{{ $cuenta->monto_inicial }}</td>
                                <td>{{ $cuenta->saldo }}</td>
                                <td>{{ $cuenta->moneda->nombre }}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection