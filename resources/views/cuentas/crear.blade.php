@extends('main')
<?php
if(sizeof($errors)>0){
    print_r($errors);
}
?>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Nueva Cuenta para {{ session('empresa_nombre') }}</h2></div>
                <div class="panel-body">
                    {!! Form::open(array('route' => 'cuentas.store','method'=>'POST', 'class'=>'form-horizontal')) !!}

                        <div class="form-group{{ $errors->has('banco') ? ' has-error' : '' }}">
                            <label for="banco" class="col-md-4 control-label">Banco</label>
                            <div class="col-md-6">
                                <select class="form-control" id="banco" name="banco">
                                <?php
                                foreach($bancos as $banco){
                                    echo "<option value='".$banco->id."'>".$banco->nombre."</option>";
                                }?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
                            <label for="numero" class="col-md-4 control-label">Numero de cuenta</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="numero" placeholder="NÚMERO DE CUENTA" style="text-transform: uppercase">
                                @if ($errors->has('numero'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('monto_inicial') ? ' has-error' : '' }}">
                            <label for="monto_inicial" class="col-md-4 control-label">Monto Inicial</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="monto_inicial" placeholder="MONTO INICIAL DE LA CUENTA" style="text-transform: uppercase">
                                @if ($errors->has('monto_inicial'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('monto_inicial') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('moneda') ? ' has-error' : '' }}">
                            <label for="moneda" class="col-md-4 control-label">Moneda</label>
                            <div class="col-md-6">
                                <select class="form-control" id="moneda" name="moneda">
                                <?php
                                foreach($monedas as $moneda){
                                    echo "<option value='".$moneda->id."'>".$moneda->nombre." (".$moneda->sigla.")</option>";
                                }?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                            <div class="col-md-6 col-md-offset-4">
                                {!! Form::submit('Crear Cuenta', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection