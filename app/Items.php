<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    //Mutators
    public function setDetalleAttribute($value)
    {
        $this->attributes['detalle'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function rendicion(){
        return $this->hasMany('App\ItemsRendiciones', 'item_id');
    }

    // One to Many <- (inverse)
    public function solicitud(){
        return $this->belongsTo('App\Solicitudes');
    }

    public function unidad(){
        return $this->belongsTo('App\Unidades');
    }

    public function tipo_compra(){
        return $this->belongsTo('App\ItemsTipos', 'tipo_id');
    }
}
