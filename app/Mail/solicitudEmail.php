<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Solicitudes;

class solicitudEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $empresa;
    protected $proyecto;
    protected $referencia;
    protected $num_solicitud;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($num_solicitud, $empresa, $proyecto, $referencia){
        $this->empresa = $empresa;
        $this->proyecto = $proyecto;
        $this->referencia = $referencia;
        $this->num_solicitud = $num_solicitud;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->view('correos.envioSolicitud')
            ->subject('Solicitud Creada')
            ->with([
                'empresa' => $this->empresa,
                'proyecto' => $this->proyecto,
                'referencia' => $this->referencia,
                'num_solicitud' => $this->num_solicitud,
            ]);
    }
}
