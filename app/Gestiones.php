<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gestiones extends Model
{
    // One to Many ->
    public function solicitud(){
        return $this->hasMany('App\Solicitudes');
    }
}
