<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NivelAprobador extends Model
{
    // Tabla modelo
    protected $table = 'nivel_aprobador';

    // Many to Many
    public function usuarios(){
        return $this->belongsToMany('App\User', 'nivelaprobador', 'proyecto_id', 'user_id');
    }

    // One to Many ->
    public function aprobadores(){
        return $this->hasMany('App\NivelesAprobacion');
    }
}
