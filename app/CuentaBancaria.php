<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentaBancaria extends Model
{
    // Tabla modelo
    protected $table = 'cuenta_bancarias';

    // One to Many ->
    public function cheques(){
        return $this->hasMany('App\Cheques');
    }

    // One to Many <- (inverse)
    public function banco(){
        return $this->belongsTo('App\Bancos');
    }

    public function empresa(){
        return $this->belongsTo('App\Empresas');
    }

    public function moneda(){
        return $this->belongsTo('App\Monedas');
    }
}
