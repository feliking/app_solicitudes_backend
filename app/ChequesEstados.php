<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChequesEstados extends Model
{
    // Tabla modelo
    protected $table = 'estado_cheques';

    // Many to Many
    public function cheque(){
        return $this->belongsToMany('App\Cheques', 'cheques_estados', 'cheque_estado_id', 'cheque_id');
    }
}
