<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    // Tabla modelo
    protected $table = 'users';

    protected $fillable = [
        'nombre', 'usuario', 'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // JWT Auth
    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    //Mutators
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function solicitudes(){
        return $this->hasMany('App\Solicitudes');
    }

    // One to Many <- (inverse)
    public function rol(){
        return $this->belongsTo('App\Roles');
    }

    // One to Many <- inverse
    public function empleado(){
        return $this->belongsTo('App\Empleado', 'empleado_id');
    }

    // Many to Many
    public function proyectos(){
        return $this->belongsToMany('App\Proyectos', 'proyectos_users', 'user_id', 'proyecto_id');
    }
    
    public function autorizados(){
        return $this->belongsToMany('App\User', 'autorizador_usuarios', 'autorizador_id', 'usuario_id');
    }

    public function autorizado_por(){
        return $this->belongsToMany('App\User', 'autorizador_usuarios', 'usuario_id', 'autorizador_id');
    }

    public function aprrendiciones_empresas(){
        return $this->belongsToMany('App\Empresas', 'empresas_aprrendiciones', 'usuario_id', 'empresa_id');
    }

    public function revisa(){
        return $this->belongsToMany('App\Modalidades', 'modalidades_users', 'user_id', 'modalidad_id');
    }

    public function nivel_aprobaciones(){
        return $this->belongsToMany('App\NivelesAprobacion','nivel_aprobador','aprobador_id','nivel_id')
            ->withPivot('porcentaje')
            ->withPivot('id');
    }

    public function fondo_rotativo(){
        return $this->belongsToMany('App\FondosRotativos', 'fr_user', 'user_id', 'fr_id');
    }

    public function revisa_fr(){
        return $this->belongsToMany('App\FondoRotativoTipos', 'fr_user', 'user_id', 'fr_id')
            ->withPivot('id');
    }

    public function aprorendicion_empresas(){
        return $this->belongsToMany('App\Empresas', 'aprorendicion_empresa', 'aprorendicion_id', 'empresa_id');
    }

    public function proyectosRevisor()
    {
        return $this->belongsToMany('App\Proyectos', 'proyecto_revisor', 'revisor_id', 'proyecto_id');
    }
}