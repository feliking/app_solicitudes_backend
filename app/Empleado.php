<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    // Conexión
    protected $connection = 'serrhh';

    // Tabla asociada al modelo
    protected $table = 'empleados';

    // ¿Utiliza timestamps?
    public $timestamps = true;

    // Campos de fechas
    protected $dates = ['created_at', 'updated_at'];

    // Campos a ordenar
    public $sortable = ['cubo_id', 'ci_numero', 'apellido_1', 'apellido_2', 'nombres'];

    /*
     * Relationships
     */
    // One to One
    public function usuario(){
        return $this->hasOne('App\User', 'empleado_id');
    }

    /**
     * BEGIN: ISSUE 0007
     */
    // One to Many (Inverse)
    public function empresa(){
        return $this->belongsTo('App\Empresas', 'sol_empresa_id');
    }
    /**
     * END: ISSUE 0007
     */
}
