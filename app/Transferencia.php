<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transferencia extends Model
{
    // Tabla modelo
    protected $table = 'transferencias';

    public function cuentaOrigen()
    {
        return $this->belongsTo('App\CuentaBancaria', 'cuenta_origen_id');
    }

    public function cuentaDestino()
    {
        return $this->belongsTo('App\CuentaBancaria', 'cuenta_destino_id');
    }

    public function tipoTransferencia()
    {
        return $this->belongsTo('App\ItemsTipos', 'tipo_transferencia_id');
    }

    public function solicitud()
    {
        return $this->hasOne('App\Solicitudes', 'solicitud_id');
    }
}
