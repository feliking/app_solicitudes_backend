<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// ColumnSortable
use Kyslik\ColumnSortable\Sortable;

// Carbon
use Carbon\Carbon;

class Solicitudes extends Model
{
    use Sortable;

    // Campos a ordenar
    public $sortable = [
        'numero',
        'desembolso',
        'referencia',
        'observacion',
        'total',
        'empleado_id',
        'modalidad_id',
        'moneda_id',
        'usuario_id',
        'proyecto_id',
        'cambio_id',
        'created_at'
    ];

    //Accessors
    public function getCreatedAtAttribute($date)
    {
        return date_format(date_create($date), 'd-m-Y');
    }
    
    public function getTimeAttribute($date)
    {
        return date_format(date_create($date), 'H:i:s');
    }

    //Mutators
    public function setDesembolsoAttribute($value)
    {
        $this->attributes['desembolso'] = mb_strtoupper($value);
    }

    public function setReferenciaAttribute($value)
    {
        $this->attributes['referencia'] = mb_strtoupper($value);
    }

    public function setObservacionAttribute($value)
    {
        $this->attributes['observacion'] = mb_strtoupper($value);
    }

    // One to one <- (inverse)
    public function cheque(){
        return $this->hasOne('App\Cheques');
    }

    public function observacionRendicion(){
        return $this->hasOne('App\ObservacionRendicion', 'solicitud_id');
    }

    // One to Many ->
    public function documentos(){
        return $this->hasMany('App\Documentos', 'solicitud_id');
    }
    
    public function documentosRendidos(){
        return $this->hasMany('App\DocumentosRendicion', 'solicitud_id');
    }

    public function items(){
        return $this->hasMany('App\Items', 'solicitud_id');
    }

    public function itemsRendidos(){
        return $this->hasMany('App\ItemsRendiciones', 'solicitud_id');
    }

    public function itemsController(){
        return $this->hasMany('App\ItemsController', 'solicitud_id');
    }

    public function estados(){
        return $this->hasMany('App\EstadoSolicitudes', 'solicitud_id');
    }

    public function devoluciones(){
        return $this->hasMany('App\Devoluciones', 'solicitud_id');
    }

    // One to Many <- (inverse)
    public function usuario(){
        return $this->belongsTo('App\User');
    }

    public function proyecto(){
        return $this->belongsTo('App\Proyectos');
    }

    public function moneda(){
        return $this->belongsTo('App\Monedas');
    }

    public function modalidad(){
        return $this->belongsTo('App\Modalidades');
    }

    public function prioridad(){
        return $this->belongsTo('App\Prioridades');
    }

    public function tipo_cambio(){
        return $this->belongsTo('App\TiposCambios', 'cambio_id');
    }

    public function gestion(){
        return $this->belongsTo('App\Gestiones');
    }

    public function transferencia()
    {
        return $this->hasOne('App\Transferencia', 'solicitud_id');
    }
}
