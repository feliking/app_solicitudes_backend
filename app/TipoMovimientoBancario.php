<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoMovimientoBancario extends Model
{
    //Mutators
    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value);
    }

    // Tabla modelo
    protected $table = 'tipo_movimientos_bancarios';
}
