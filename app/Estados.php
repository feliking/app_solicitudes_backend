<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    //Mutators
    public function setCodigoAttribute($value)
    {
        $this->attributes['codigo'] = mb_strtoupper($value);
    }

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value);
    }

    public function solicitud(){
        return $this->hasMany('App\Solicitudes');
    }

    // One to Many ->
    public function estados(){
        return $this->hasMany('App\EstadoSolicitudes');
    }
}
