<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpresaGestion extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'empresa_id', 'gestion_id'
    ];
}
