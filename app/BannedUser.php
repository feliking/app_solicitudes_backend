<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannedUser extends Model
{
    protected $table = 'banned_users';

    // Relationships
    // One to Many (Inverse)
    public function usuario(){
        return $this->belongsTo('App\User');
    }
}
