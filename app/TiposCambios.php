<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposCambios extends Model
{
    // Tabla modelo
    protected $table = 'tipos_cambio';

    // One to Many ->
    public function solicitud(){
        return $this->hasMany('App\Solicitudes');
    }

    /**
     * Scope
     */
    public function scopeMoneda($query, $monedaId){
        return $query->where('moneda_id', $monedaId);
    }
}
