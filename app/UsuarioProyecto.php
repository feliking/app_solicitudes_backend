<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioProyecto extends Model
{
    public $timestamps = false;

    // Tabla modelo
    protected $table = 'proyectos_users';

    protected $fillable = [
        'user_id', 'proyecto_id'
    ];
}
