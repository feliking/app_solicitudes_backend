<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// ColumnSortable
use Kyslik\ColumnSortable\Sortable;

class Cheques extends Model
{
    use Sortable;

    // Tabla modelo
    protected $table = 'cheques';

    // Campos a ordenar
    public $sortable = [
        'numero_cheque',
        'beneficiario',
        'fecha',
        'monto',
        'observacion',
        'concepto',
        'cuenta_id',
        'solicitud_id'
    ];

    //Mutators
    public function setBeneficiarioAttribute($value)
    {
        $this->attributes['beneficiario'] = mb_strtoupper($value);
    }

    public function setConceptoAttribute($value)
    {
        $this->attributes['concepto'] = mb_strtoupper($value);
    }

    // One to one ->
    public function solicitud(){
        return $this->belongsTo('App\Solicitudes');
    }

    // One to Many <- (inverse)
    public function cuenta(){
        return $this->belongsTo('App\CuentaBancaria');
    }

    // Many to Many
    public function cheque(){
        return $this->belongsToMany('App\ChequesEstados', 'cheques_estados', 'cheque_estado_id', 'cheque_id');
    }
}
