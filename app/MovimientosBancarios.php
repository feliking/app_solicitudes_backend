<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovimientosBancarios extends Model
{
    // Tabla modelo
    protected $table = 'movimientos_bancarios';
}
