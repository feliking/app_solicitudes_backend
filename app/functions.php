<?php
function generarClave(){
    $alpha = "abcdefghijklmnopqrstuvwxyz";
    $alpha_upper = strtoupper($alpha);
    $numeric = "0123456789";
    $special = ".-+=_,!@$#*%<>[]{}";
    $length = 14;

    $chars = "";
    $chars .= $alpha;
    $chars .= $numeric;
    $chars .= $alpha_upper;
    $chars .= $special;

    $len = strlen($chars);
    $pw = '';

    for ($i=0;$i<$length;$i++){
        $pw .= substr($chars, rand(0, $len-1), 1);
    }

    return str_shuffle($pw);
}

function creaSolicitudesEmpresa(){

}


function dividirNumero($numero){
    
}

function modalidad($modalidad){
    switch($modalidad){
        case 1:
            return "Sujeto a Rendición";
            break;
        case 2:
            return "Gasto Directo";
            break;
    }
    return "";
}

function moneda($moneda){
    switch($moneda){
        case 1:
            return "Bolivianos";
            break;
        case 2:
            return "Dolares";
            break;
    }
    return "";
}

function crearBD($nombre, $gestion){
    $con_bd = mysqli_connect('localhost', 'root', config('db_pass.db_password'));
    if (!$con_bd) {
       die('No se pudo conectar a mysql.' . mysqli_error());
    }

    $nombre_bd = str_replace(' ', '_', $nombre);
    $query = "CREATE DATABASE solicitudes_".$nombre_bd;
    if (mysqli_query($con_bd, $query)) {
       echo "
          <SCRIPT>
          console.log('error al guardar bd');
          $.fx.speeds._default = 1000;
          $(function(){
             var mensaje = $('<div title=\'Aviso\'><p style=\"font-size:14px;\">La nueva Empresa fue creada con exito.</p> </div>');
             mensaje.dialog({
                modal: 'true',
                buttons: {
                   'Aceptar': function() {
                      $( this ).dialog( 'close' );
                   }
                }
             });
          });
          </SCRIPT>";
    }
    else {
       echo 'Error al crear la base de datos: ' . mysqli_error() . "\n";
    }

    crearGestion($nombre_bd, $gestion);
}
function crearGestion($nombre, $gestion){
    $empresas_query = App\Empresas::where('slug', 'LIKE', $nombre.'%')->get();
    foreach($empresas_query as $empresa_query){
        $id_empresa = $empresa_query->id_empresa;
    }
    //***REVISAR AL CREAR NUEVAS GESTIONES***
    //Insertamos relacion empresa gestion
    DB::table('gestion_empresas')->insert([
        'id_empresa' => $id_empresa,
        'prefijo'    => 'solicitudes_'.$nombre,
        'gestion'    => $gestion
    ]);
    //INSERTAR DATO EN gestion_empresa
    $nombre = str_replace(' ', '_', $nombre);
    $prefix = 'solicitudes_'.$nombre.'.'.$gestion.'_';
    //Tabla proyectos
    Schema::connection('mysql')->create($prefix.'proyectos', function($table){
        $table->increments('id_proyecto');
        $table->integer('id_gestion');
        $table->integer('id_padre')->default(0);
        $table->string('nombre');
        $table->text('descripcion')->nullable();
    });

    //Tabla Solicitudes
    Schema::connection('mysql')->create($prefix.'solicitudes', function($table){
        $table->increments('id_solicitud');
        $table->integer('id_proyecto');
        $table->integer('id_usuario');
        $table->date('fecha_solicitud');
        $table->time('hora_solicitud');
        $table->string('desembolso')->nullable();
        $table->date('fecha_limite_solicitud')->nullable();
        $table->float('moneda', 6, 2);
        $table->float('tipo_cambio', 3, 2);
        $table->string('tipo_solicitud');
        $table->string('estado');
        $table->integer('aprobador')->nullable();
        $table->date('fecha_aprobador')->nullable();
        $table->integer('autorizador')->nullable();
        $table->date('fecha_autorizacion')->nullable();
        $table->string('descripcion')->nullable();
        $table->text('observacion')->nullable();
        $table->text('referencia')->nullable();
        $table->float('total', 6, 2);
        $table->float('total_rend', 6, 2);
    });

    //Tabla items
    Schema::connection('mysql')->create($prefix.'items', function($table){
        $table->increments('id_item');
        $table->integer('id_solicitud');
        $table->string('tipo');
        $table->text('descripcion');
        $table->integer('cantidad');
        $table->float('costo', 6, 2);
        $table->float('subtotal', 6, 2);
        $table->string('num_partida')->nullable();
        $table->string('nom_partida')->nullable();
    });

    //Tabla documentos
    Schema::connection('mysql')->create($prefix.'docs', function($table){
        $table->increments('id_docs');
        $table->integer('id_solicitud');
        $table->string('direccion');
        $table->text('observaciones')->nullable();
    });
    //Tabla logs
    Schema::connection('mysql')->create($prefix.'vitacora', function($table){
        $table->increments('id_vitacora');
        $table->integer('id_solicitud');
        $table->date('fecha');
        $table->time('hora');
        $table->text('descripcion')->nullable();
        $table->integer('id_usuario');
    });
    //Tabla items rendicion
    Schema::connection('mysql')->create($prefix.'items_ren', function($table){
        $table->increments('id_rendicion');
        $table->integer('id_solicitud');
        $table->string('tipo');
        $table->text('descripcion');
        $table->integer('cantidad');
        $table->float('costo', 6, 2);
        $table->float('subtotal', 6, 2);
    });
    //Tabla doc rendicion
    Schema::connection('mysql')->create($prefix.'doc_ren', function($table){
        $table->increments('id_doc_ren');
        $table->integer('id_solicitud');
        $table->string('direccion');
        $table->text('observacion')->nullable();
    });
    //Tabla cheques
    Schema::connection('mysql')->create($prefix.'cheques', function($table){
        $table->increments('id_cheques');
        $table->integer('id_solicitud');
        $table->string('banco');
        $table->string('cuenta');
        $table->float('monto', 6, 2);
        $table->text('observacion');
        $table->string('estado');
    });
    //Tabla rechazos
    Schema::connection('mysql')->create($prefix.'rechazos', function($table){
        $table->increments('id_rechazo');
        $table->integer('id_solicitud');
        $table->integer('id_usuario');
        $table->integer('estado');
        $table->date('fecha_rechazo');
        $table->text('motivo')->nullable();
    });
}

function cantPalabras ($text, $limit)
{
    $buffer = $text;
    $normal_array = explode(' ', $text);
    if (sizeof($normal_array) > $limit) {
        $array = explode(" ", $text, $limit + 1);
        if (count($array) > $limit) {
            unset($array[$limit]);
        }
        $buffer = implode(" ", $array) . ' ...';
    }
    return $buffer;
}

function basico ($numero){
    $valor = array ('UNO', 'DOS', 'TRES', 'CUATRO', 'CINCO', 'SEIS', 'SIETE', 'OCHO', 'NUEVE', 'DIEZ',
        'ONCE', 'DOCE', 'TRECE', 'CATORCE', 'QUINCE', 'DIECISEIS', 'DIEDISIETE', 'DIECIOCHO', 'DIECINUEVE', 'VEINTE',
        'VEINTIUNO', 'VEINTIDOS', 'VEINTITRES', 'VEINTICUATRO', 'VEINTICINCO', 'VEINTISÉIS', 'VEINTISIETE', 'VEINTIOCHO', 'VEINTINUEVE');
    return $valor[$numero - 1];
}

function decenas ($n){
    $decenas = array(30 => 'TREINTA', 40 => 'CUARENTA', 50 => 'CINCUENTA', 60 => 'SESENTA',
        70 => 'SETENTA', 80 => 'OCHENTA', 90 => 'NOVENTA');
    if ($n <= 29)
        return basico($n);
    $x = $n % 10;
    if ($x == 0) {
        return $decenas[$n];
    } else
        return $decenas[$n - $x] . ' Y ' . basico($x);
}

function centenas ($n){
    $cientos = array(100 => 'CIEN', 200 => 'DOSCIENTOS', 300 => 'TRECIENTOS',
        400 => 'CUATROCIENTOS', 500 => 'QUINIENTOS', 600 => 'SEISCIENTOS',
        700 => 'SETECIENTOS', 800 => 'OCHOCIENTOS', 900 => 'NOVECIENTOS');
    if ($n >= 100) {
        if ($n % 100 == 0) {
            return $cientos[$n];
        } else {
            $u = (int) substr($n, 0, 1);
            $d = (int) substr($n, 1, 2);
            return (($u == 1) ? 'CIENTO' : $cientos[$u * 100]) . ' ' . decenas($d);
        }
    } else
        return decenas($n);
}

function miles ($n){
    if ($n > 999) {
        if ($n == 1000) {
            return 'MIL';
        } else {
            $l = strlen($n);
            $c = (int) substr($n, 0, $l - 3);
            $x = (int) substr($n, -3);
            if ($c == 1) {
                $cadena = 'MIL ' . centenas($x);
            } else if ($x != 0) {
                $cadena = centenas($c) . ' MIL ' . centenas($x);
            } else
                $cadena = centenas($c) . ' MIL';
            return $cadena;
        }
    } else
        return centenas($n);
}

function millones ($n){
    if ($n == 1000000) {
        return 'UN MILLÓN';
    } else {
        $l = strlen($n);
        $c = (int) substr($n, 0, $l - 6);
        $x = (int) substr($n, -6);
        if ($c == 1) {
            $cadena = ' MILLÓN ';
        } else {
            $cadena = ' MILLONES ';
        }
        return miles($c) . $cadena . (($x > 0) ? miles($x) : '');
    }
}

function numeroLiteral ($n){
    $n = intval($n);
    switch (true) {
        case ( $n >= 1 && $n <= 29 ) : return basico($n);
            break;
        case ( $n >= 30 && $n < 100 ) : return decenas($n);
            break;
        case ( $n >= 100 && $n < 1000 ) : return centenas($n);
            break;
        case ( $n >= 1000 && $n <= 999999 ): return miles($n);
            break;
        case ( $n >= 1000000 ): return millones($n);
    }
}

function numeroDecimal($n){
    $pos = strpos($n, '.');
    if($pos === false){
        return '00';
    }else{
        $decimal = substr($n, $pos+1);
        $dec = '00';
        if(intval($decimal) >= 0 && intval($decimal) < 10){
            $dec = $decimal."0";
        }else{
            $dec = substr($decimal, 0, 2);
        }
        return $dec;
    }
}
