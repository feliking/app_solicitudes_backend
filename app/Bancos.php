<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bancos extends Model
{
    //Mutators
    public function setCodigoAttribute($value)
    {
        $this->attributes['codigo'] = mb_strtoupper($value);
    }

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function cuentas(){
        return $this->hasMany('App\CuentaBancaria');
    }
}
