<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NivelesAprobacion extends Model
{
    // Tabla modelo
    protected $table = 'niveles_aprobacion';

    // Many to Many
    public function proyectos(){
        return $this->belongsToMany('App\Proyectos', 'proyectos_users', 'user_id', 'proyecto_id');
    }

    public function users(){
        return $this->belongsToMany('App\User','nivel_aprobador','nivel_id','aprobador_id')
            ->withPivot('porcentaje')
            ->withPivot('id');
    }

    // One to Many <- (inverse)
    public function niveles(){
        return $this->belongsTo('App\NivelAprobador');
    }
}
