<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsTipos extends Model
{
    //Mutators
    public function setCodigoAttribute($value)
    {
        $this->attributes['codigo'] = mb_strtoupper($value);
    }

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value);
    }

    // Tabla modelo
    protected $table = 'items_tipos';

    // One to Many ->
    public function items(){
        return $this->hasMany('App\Items', 'tipo_id');
    }

    public function itemsController(){
        return $this->hasMany('App\ItemsController', 'tipo_id');
    }
}
