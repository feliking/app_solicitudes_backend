<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactorTipoCambio extends Model
{
    // Tabla modelo
    protected $table = 'factores_tipo_cambio';

    public function monedaOrigen()
    {
        return $this->belongsTo('App\Monedas', 'moneda_origen_id');
    }

    public function monedaDestino()
    {
        return $this->belongsTo('App\Monedas', 'moneda_destino_id');
    }
}
