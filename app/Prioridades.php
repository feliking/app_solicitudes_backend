<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prioridades extends Model
{
    //Mutators
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value);
    }

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function solicitud(){
        return $this->hasMany('App\Solicitudes');
    }
}
