<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsController extends Model
{
    //Mutators
    public function setDetalleAttribute($value)
    {
        $this->attributes['detalle'] = mb_strtoupper($value);
    }

    // Tabla modelo
    protected $table = 'items_controller';

    // One to Many <- (inverse)
    public function solicitud(){
        return $this->belongsTo('App\Solicitudes');
    }

    public function unidad(){
        return $this->belongsTo('App\Unidades');
    }

    public function tipo_compra(){
        return $this->belongsTo('App\ItemsTipos', 'tipo_id');
    }
}
