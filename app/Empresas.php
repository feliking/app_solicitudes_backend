<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    protected $fillable = [
        'nombre', 'razon', 'nit', 'descripcion', 'imagen', 'slug'
    ];

    //Mutators
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value);
    }

    public function setRazonSocialAttribute($value)
    {
        $this->attributes['razon_social'] = mb_strtoupper($value);
    }

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function cuentas(){
        return $this->hasMany('App\CuentaBancaria', 'empresa_id');
    }

    public function proyectos(){
        return $this->hasMany('App\Proyectos', 'empresa_id');
    }

    public function empleados(){
        return $this->hasMany('App\Empleado', 'sol_empresa_id')
            ->where('empleados.estado', 'Activo')
            ->orderBy('apellido_1', 'ASC')
            ->orderBy('apellido_2', 'ASC')
            ->orderBy('nombres', 'ASC');
    }

    // Many to Many <-
    public function aprrendiciones_empresas(){
        return $this->belongsToMany('App\User', 'empresas_aprrendiciones', 'empresa_id', 'usuario_id');
    }

    public function fondosrotativos(){
        return $this->belongsToMany('App\FondosRotativos','fondorotativos','fr_id','empresa_id');
    }

    public function aprorendicion_empresas(){
        return $this->belongsToMany('App\User', 'aprorendicion_empresa', 'empresa_id', 'aprorendicion_id');
    }
}
