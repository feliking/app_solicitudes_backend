<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monedas extends Model
{
    //Mutators
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value);
    }

    public function setSiglaAttribute($value)
    {
        $this->attributes['sigla'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function cuentas(){
        return $this->hasMany('App\CuentaBancaria');
    }

    public function solicitud(){
        return $this->hasMany('App\Solicitudes');
    }

    public function tipos_cambio () {
        return $this->hasMany('App\TiposCambios', 'moneda_id');
    }
}
