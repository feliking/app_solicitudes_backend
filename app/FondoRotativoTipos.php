<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FondoRotativoTipos extends Model
{
    protected $table = 'fr_tipos';

    // One to Many ->
    public function fondo_rotativo(){
        return $this->hasMany('App\FondosRotativos');
    }
}
