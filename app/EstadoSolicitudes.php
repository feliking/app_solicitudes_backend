<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Carbon
use Carbon\Carbon;

class EstadoSolicitudes extends Model
{
    //Mutators
    public function setObservacionAttribute($value)
    {
        $this->attributes['observacion'] = mb_strtoupper($value);
    }

    // Tabla modelo
    protected $table = 'estados_solicitud';

    // One to Many <- (inverse)
    public function solicitud(){
        return $this->belongsTo('App\Solicitudes');
    }

    public function estado(){
        return $this->belongsTo('App\Estados');
    }

    //Accessors
//    public function getCreatedAtAttribute($date)
//    {
//        return date_format(date_create($date), 'd-m-Y');
//    }
}
