<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AprorendicionEmpresa extends Model
{
    public $timestamps = false;

    // Tabla modelo
    protected $table = 'aprorendicion_empresa';

    protected $fillable = [
        'aprorendicion_id', 'empresa_id'
    ];
}
