<?php

namespace App\Http\Middleware;

use Closure;

use App\BannedUser;
use Illuminate\Support\Facades\Auth;

class BannedCreateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $banned = BannedUser::where([
            ['user_id', '=', Auth::user()->id],
            ['banned', 'LIKE', 'create']
        ])
            ->first();

        if($banned != null){
            abort('403', 'banned');
        }

        return $next($request);
    }
}
