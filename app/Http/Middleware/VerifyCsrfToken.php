<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

use Closure;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        /*'login', 'logout'*/
    ];

    public function handle($request, Closure $next)
    {
        if($request->input('_token'))
        {
            if ( \Session::getToken() != $request->input('_token'))
            {
                return redirect('/');
            }
        }
        
        return $next($request);
    }
}
