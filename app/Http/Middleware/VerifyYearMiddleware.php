<?php

namespace App\Http\Middleware;

use Closure;

// Modelos a utilizar
use App\Gestiones;

// Carbon
use Carbon\Carbon;

class VerifyYearMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_year = Carbon::now()->year;

        if (\Auth::user()->global_view == 0) {
            $session_year = Gestiones::find(session('gestion_id')[0])->gestion;
    
            if (intval($current_year) != intval($session_year)){
                abort('403', 'year');
            }
        }
        
        return $next($request);
    }
}
