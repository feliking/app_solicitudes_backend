<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gestiones;
use App\Proyectos;
use Session;

class ProyectosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "ProyectosController@index";
        $proy_ids_padres = Proyectos::where('padre_id', null)
            ->select('proyectos.id')
            ->where('empresa_id', session('empresa'))
            ->get()
            ->pluck('id');
        $proyectos = array();
        foreach($proy_ids_padres as $proy_padre){
            $proy_padre = Proyectos::find($proy_padre);
            $proyectos[] = $proy_padre;
            $proy_hijos = Proyectos::where('padre_id', $proy_padre->id)->get();
            if(count($proy_hijos) > 0){
                foreach($proy_hijos as $proy_hijo){
                    $sub_proyecto = Proyectos::find($proy_hijo->id);
                    $sub_proyecto->nombre = $proy_padre->nombre." &#10148; ".$proy_hijo->nombre;
                    $proyectos[] = $sub_proyecto;
                    $proy_subsub = Proyectos::where('padre_id', $sub_proyecto->id)->get();
                    if(count($proy_subsub) > 0){
                        foreach($proy_subsub as $proy_nieto){
                            $nieto_proyecto = Proyectos::find($proy_nieto->id);
                            $nieto_proyecto->nombre = $sub_proyecto->nombre." &#10148; ".$nieto_proyecto->nombre;
                            $proyectos[] = $nieto_proyecto;
                        }
                    }
                }
            }
        }
        return view('proyectos.index')->withProyectos($proyectos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo "ProyectosController@create";
        $empresa_id = 0;
        if(isset($_GET['id']) && null != $_GET['id']){
            $empresa_id = $_GET['id'];
        }else{
            $empresa_id = session('empresa');
        }
        $proy_ids_padres = Proyectos::where('padre_id', null)
            ->select('proyectos.id')
            ->where('empresa_id', $empresa_id)
            ->get()
            ->pluck('id');
        $proyectos = array();
        foreach($proy_ids_padres as $proy_padre){
            $proy_padre = Proyectos::find($proy_padre);
            $proyectos[] = $proy_padre;
            $proy_hijos = Proyectos::where('padre_id', $proy_padre->id)->get();
            if(count($proy_hijos) > 0){
                foreach($proy_hijos as $proy_hijo){
                    $sub_proyecto = Proyectos::find($proy_hijo->id);
                    $sub_proyecto->nombre = $proy_padre->nombre." -> ".$proy_hijo->nombre;
                    $proyectos[] = $sub_proyecto;
//                    $proy_subsub = Proyectos::where('padre_id', $sub_proyecto->id)->get();
//                    if(count($proy_subsub) > 0){
//                        echo "tiene nietos<br>";
//                        foreach($proy_subsub as $proy_nieto){
//                            $nieto_proyecto = Proyectos::find($proy_nieto->id);
//                            $nieto_proyecto->nombre = $sub_proyecto->nombre." -> ".$nieto_proyecto->nombre;
//                            $proyectos[] = $nieto_proyecto;
//                        }
//                    }
                }
            }
        }
        return view('proyectos.crear')->withProy_array_padres($proyectos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo "ProyectosController@store";
        $proyecto = new Proyectos;
        $proyecto->nombre = mb_strtoupper($request->nombre);
        if(strlen(trim($request->descripcion)) > 0){
            $proyecto->descripcion = mb_strtoupper($request->descripcion);
        }
        if($request->opcion_sub == '0'){
            $proyecto->padre_id = null;
        }else{
            $proyecto_padre = Proyectos::find($request->id_padre);
            $proyecto->padre_id = $request->id_padre;
            $proyecto->nivel = $proyecto_padre->nivel+1;
        }
        $proyecto->empresa_id = session('empresa');

        $proyecto->save();

        Session::flash('success', 'PROYECTO "'.$proyecto->nombre.'" CREADO!!!');
        //redirect()->back()->getTargetUrl();
        return redirect()->action('ProyectosController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "ProyectosController@show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo "ProyectosController@edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo "ProyectosController@update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "ProyectosController@destroy";
    }
}
