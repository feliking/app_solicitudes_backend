<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use App\EmpresaGestion;
use App\Solicitudes;
use App\Gestiones;
use App\Proyectos;
use App\Empresas;
use App\Estados;
use App\User;
use Session;
use Image;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "EmpresaController@index";
        $empresas = Empresas::orderBy('nombre')->get();
        return view('empresas.index')->withEmpresas($empresas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo "EmpresaController@create";
        return view('empresas.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //include_once (app_path().'\functions.php');
        $this->validate($request,[
            'nombre' => 'required',
            'razon' => 'required',
            'nit' => 'required',
        ]);

        $nombre = str_replace(' ', '_', $request['nombre']);
        $nombre = str_replace('.', '', $nombre);
        $nombre = mb_strtolower($nombre);

        $path = public_path().'/empresas/';
        if (!file_exists($path)) {
            $result = \File::makeDirectory($path);
        }
        $path .= $nombre.'/';
        $result = \File::makeDirectory($path);

        $filename = 'default.jpg';
        if($request->hasFile('imagen')){
            $logo = $request->file('imagen');
            //Resize
            $image=getimagesize($logo);
            $width=$image[0]; //[0] is the width
            $height=$image[1];//[1] is the height
            $val_ratio = 500;
            if($width > $val_ratio || $height > $val_ratio){
                if($width>$height){
                    $max = $width;
                }else{
                    $max = $height;
                }
                $ratio = $max/$val_ratio;
                $width = $width/$ratio;
                $height = $height/$ratio;
                $width = intval($width);
                $height = intval($height);
            }
            //End Resize
            $nombre_imagen = $nombre;
            $filename = $nombre_imagen.'.'.$logo->getClientOriginalExtension();
            //$path = public_path('empresas/'.$filename);
            $file_path ='empresas/'.$nombre.'/'.$filename;
            //exit();
            Image::make($logo)->resize($width,$height)->save($file_path);
        }

        $empresa = new Empresas;
        $empresa->nombre = mb_strtoupper($request['nombre']);
        $empresa->razon = mb_strtoupper($request['razon']);
        $empresa->nit = $request['nit'];
        $empresa->descripcion = mb_strtoupper($request['descripcion']);
        $empresa->logo = $file_path;
        $empresa->slug = $nombre;
        $empresa->save();
        $empresa_id = $empresa->id;

        //Gestion
        $gestion = date('Y');
        $gestion_db = Gestiones::where('gestion', $gestion)->first();
        if(count($gestion_db) == 1){
//            echo "si";
//            dd($gestion_db);
            $gestion_actual = $gestion_db->gestion;
            $gestion_id = $gestion_db->id;
            $gestion_db->empresas()->attach($empresa_id);
        }else{
            $gestion_new = new Gestiones;
            $gestion_new->gestion = $gestion;
            $gestion_new->save();
            $gestion_actual = $gestion_new->gestion;
            $gestion_id = $gestion_new->id;
            $gestion_new->empresas()->attach($empresa_id);
        }

        $path .= $gestion_actual.'/';
        $result = \File::makeDirectory($path);
        $result = \File::makeDirectory($path.'cheques');
        $result = \File::makeDirectory($path.'rendicion');
        $result = \File::makeDirectory($path.'solicitud');

        //crearBD($nombre, $gestion_actual);

        $empresas = Empresas::all();
        //return view('empresas.index', compact('empresas')); No valido
        Session::flash('success', 'Empresa Creada!!!');
        return view('empresas.index')->withEmpresas($empresas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function seleccion_empresa(){
        //********REVISAR PARA AUTORIZADOR

        session()->forget('empresa');
        session()->forget('empresa_nombre');
        session()->forget('empresa_slug');
        session()->forget('paginacion');
        session()->forget('gestion_id');
        session()->forget('gestion');

        $usuario = auth()->user();
        // $gestion_search = Gestiones::all()->last();
        // session(['gestion_id' => $gestion_search->id]);
        // session(['gestion' => $gestion_search->gestion]);
        // session(['paginacion' => 20]);
        $gestiones = Gestiones::orderBy("id", "DESC")->get();

        $ultimas_gestiones = Gestiones::orderBy('id', 'DESC')
            ->take(2)
            ->get();

        if($usuario->rol_id == 8){
            session(['empresa' => null]);
            $empresa = Empresas::find(session('empresa'));
            return redirect()->route('solicitudes.admin');
        } else if ($usuario->global_view == 1) {
            return redirect()->route('global.en_curso');
        }else{
            if(($usuario->rol_id == 1 || $usuario->rol_id >= 4 || $usuario->revisor == true) && ($usuario->rol_id != 7)){
                $empresas = Empresas::where('estado', true)->get();
                //$empresas = Empresas::orderBy('nombre')->get();
            }else{
                if($usuario->rol_id == 7){
                    $empresas_aprrendiciones = $usuario->aprrendiciones_empresas;
                    $user_id = $usuario->id;
                    $array_empresas = array();
                    foreach($empresas_aprrendiciones as $empresa){
                        $array_empresas[] = $empresa->id;
                    }
                    $empresas = Empresas::whereIn('id', $array_empresas)->where('estado', true)->get();
                }else{
                    $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                        $query->where('user_id', $usuario->id);
                    })->pluck('id')->toArray();

                    $empresas = Empresas::whereHas('proyectos', function ($query) use($proyectos){
                        $query->whereIn('id', $proyectos);
                    })->get();
                }
            }
        }
        if(count($empresas) == 0 && $usuario->rol_id == 1){
            return view('empresas.crear');
        }
        return view('empresas.seleccion')
            ->withUltimasGestiones($ultimas_gestiones)
            ->withGestiones($gestiones)
            ->withEmpresas($empresas);
    }

    public function getLastEstado($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.descripcion')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados.id', 'DESC')
            ->first();
        return $lastEstado->descripcion;
    }

    public function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null){
        $perPage = 20;
        
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }

    /**
     * BEGIN: ISSUE 0007
     */
    /**
     * Obtiene los empleados por empresa
     *
     * @param  int  $empresaId
     * @return Json
     */
    public function getEmpleadosPorEmpresa($empresaId){
        $empresa = Empresas::find($empresaId);

        if($empresa != null){
            return response()->json([
                'empleados' => $empresa->empleados,
            ]);
        }else{
            return response()->json([
                'empleados' => null,
            ]);
        }
    }
    /**
     * END: ISSUE 0007
     */
}
