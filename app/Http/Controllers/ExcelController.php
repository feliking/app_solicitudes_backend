<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EstadoSolicitudes;
use App\ItemsRendiciones;
use App\ItemsController;
use App\UsuarioProyecto;
use App\CuentaBancaria;
use App\TiposCambios;
use App\Devoluciones;
use App\Solicitudes;
use App\Modalidades;
use App\ItemsTipos;
use App\Documentos;
use App\Proyectos;
use App\Gestiones;
use App\Empresas;
use App\Unidades;
use App\Monedas;
use App\Estados;
use App\Items;
use App\User;

use Session;
use Excel;
use Mail;
use DB;

class ExcelController extends Controller
{
    public function generarExcel(Request $request){
        //dd($request->all());
        $solicitudes_ids = explode(',', $_GET['ids']);
        
        $solicitudes = Solicitudes::whereIn('id', $solicitudes_ids)
            ->orderBy('gestion_id', 'DESC')    
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->get();

        $data = array();
        array_push($data, ['Nº', 'FECHA', 'ESTADO', 'MODALIDAD', 'MONTO', 'MONEDA', 'EMPRESA', 'PROYECTO', 'SOLICITANTE', 'REFERENCIA', 'JUSTIFICACIÓN', 'TIPO DE CAMBIO', 'DESEMBOLSO']);
        foreach($solicitudes as $solicitud){
            switch($solicitud->proyecto->nivel){
                case 1:
                    $proyecto = $solicitud->proyecto->nombre;
                    break;
                case 2:
                    $proyecto = $solicitud->proyecto->padre->nombre." -> ".$solicitud->proyecto->nombre;
                    break;
                case 3:
                    $proyecto = $solicitud->proyecto->padre->padre->nombre." -> ".$solicitud->proyecto->padre->nombre." -> ".$solicitud->proyecto->nombre;
                    break;
            }

            if($solicitud->moneda_id == 1){
                $total = $solicitud->total;
                $moneda = $solicitud->moneda->sigla;
            }else{
                $total = $solicitud->total * $solicitud->tipo_cambio->cambio;
                $total = floatval($total);
                $moneda = "BS.(*)";
            }

            $solicitud->estadoCodigo = app('App\Http\Controllers\SolicitudController')->getLastEstadoCodigo($solicitud->id);

            array_push($data,[
                $solicitud->numero,
                $solicitud->created_at,
                $solicitud->estadoCodigo,
                $solicitud->modalidad->nombre,
                $total,
                $moneda,
                $solicitud->proyecto->empresa->nombre,
                $proyecto,
                $solicitud->usuario->nombre,
                $solicitud->referencia,
                $solicitud->observacion,
                $solicitud->tipo_cambio->cambio,
                $solicitud->desembolso
            ]);
        }

        Excel::create('Solicitudes_'.date('dmY'), function($excel) use ($data){
            $hoja = 1;
            $excel->sheet('Hoja '.$hoja, function($sheet) use ($data){
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('xls');
    }

    public function generarExcelRevisor(Request $request){
//        dd($request->exsol_id);
//        $ids = explode(',', $_GET['ids']);
        $solicitudes = Solicitudes::whereIN('id', $request->exsol_id)
            ->get();

        //dd($solicitudes);

        foreach($solicitudes as $solicitud){
            $solicitud->autorizador = "";
            $solicitud->fecha_aut = "";
            $estados = EstadoSolicitudes::where('solicitud_id', $solicitud->id)
                ->where("estado_id", 20)
                ->first();

            if(isset($estados)){
                $aut_soli = User::where('id', $estados->usuario_id)
                    ->first();
                $solicitud->autorizador = $aut_soli->nombre;
                $solicitud->fecha_aut = $estados->created_at->format('d-m-Y');
            }

            $solicitud->controller = "";
            $solicitud->fecha_rev = "";
            $estados = EstadoSolicitudes::where('solicitud_id', $solicitud->id)
                ->where("estado_id", 30)
                ->first();

            if(isset($estados)){
                $rev_soli = User::where('id', $estados->usuario_id)
                    ->first();
                $solicitud->controller = $rev_soli->nombre;
                $solicitud->fecha_rev = $estados->created_at->format('d-m-Y');
            }

            $solicitud->aprobador = "";
            $solicitud->fecha_apr = "";
            $estados = EstadoSolicitudes::where('solicitud_id', $solicitud->id)
                ->where("estado_id", 40)
                ->first();

            if(isset($estados)){
                $apr_soli = User::where('id', $estados->usuario_id)
                    ->first();
                $solicitud->aprobador = $apr_soli->nombre;
                $solicitud->fecha_apr = $estados->created_at->format('d-m-Y');
            }

            $solicitud->estado = $this->getLastEstadoCodigo($solicitud->id);
        }

        //return view('solicitudes.reportes.expExcelRevisor')->withSolicitudes($solicitudes);
        Excel::create('Revisor_'.date('dmY'), function($excel) use($solicitudes) {
            $excel->sheet('Solicitudes', function($sheet) use($solicitudes) {
                $col_ini = 'A';
                $col_fin = 'Y';
                $fila = 1;
                $sheet->cells($col_ini.$fila.":".$col_fin.$fila, function($cells){
                    $cells->setBackground('#007000');
                    $cells->setFontColor('#FFFFFF');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });
                $sheet->loadView('solicitudes.reportes.expExcelRevisor')
                    ->withSolicitudes($solicitudes);
            });
        })->download('xls');
    }

    public function generarExcelAprRendiciones(Request $request){
        $ids = explode(',', $_GET['ids']);
        $solicitudes = Solicitudes::whereIN('id', $ids)
            ->get();

        $data = array();
        array_push($data, ['EMPRESA', 'PROYECTO', 'SOLICITUD', 'SOLICITANTE', 'DESEMBOLSO', 'CATEGORIA', 'SUBCATEGORIA', 'TIPO ITEM', 'DESCRIPCION', 'TIPO DE COMPRA', 'UNIDAD', 'MARCA', 'CIUDAD', 'DESTINO', 'CANTIDAD', 'PRECIO UNITARIO', 'TOTAL', 'FECHA APROBACION', 'MONEDA', 'T. CAMBIO']);
        foreach($solicitudes as $solicitud){
            switch($solicitud->proyecto->nivel){
                case 1:
                    $proyecto = $solicitud->proyecto->nombre;
                    break;
                case 2:
                    $proyecto = $solicitud->proyecto->padre->nombre." -> ".$solicitud->proyecto->nombre;
                    break;
                case 3:
                    $solicitud->proyecto->padre->padre->nombre." -> ".$solicitud->proyecto->padre->nombre." -> ".$solicitud->proyecto->nombre;
                    break;
            }

            $items = Items::where('solicitud_id', $solicitud->id)->get();
            $itemsRendiciones = ItemsRendiciones::where('solicitud_id', $solicitud->id)->get();
            foreach($items as $item){
                array_push($data,[
                    $solicitud->proyecto->empresa->nombre,
                    $proyecto,
                    $solicitud->numero,
                    $solicitud->usuario->nombre,
                    $solicitud->desembolso,
                    '',
                    '',
                    'ITEM PEDIDO',
                    $item->detalle,
                    $item->tipo_compra->descripcion,
                    $item->unidad->descripcion,
                    '',
                    '',
                    '',
                    $item->cantidad,
                    $item->costo,
                    $item->cantidad * $item->costo,
                    '',
                    $solicitud->moneda->nombre,
                    $solicitud->tipo_cambio->cambio
                ]);
            }
            foreach($itemsRendiciones as $itemRendicion){
                array_push($data,[
                    $solicitud->proyecto->empresa->nombre,
                    $proyecto,
                    $solicitud->numero,
                    $solicitud->usuario->nombre,
                    $solicitud->desembolso,
                    '',
                    '',
                    'ITEM RENDIDO',
                    $itemRendicion->detalle,
                    '',
                    $itemRendicion->unidad->descripcion,
                    '',
                    '',
                    '',
                    $itemRendicion->cantidad,
                    $itemRendicion->costo,
                    $itemRendicion->cantidad * $itemRendicion->costo,
                    '',
                    $solicitud->moneda->nombre,
                    $solicitud->tipo_cambio->cambio
                ]);
            }

        }

        Excel::create('AprRendiciones_'.date('dmY'), function($excel) use ($data){
            $hoja = 1;
            $excel->sheet('Solicitudes '.$hoja, function($sheet) use ($data){
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('xls');
    }

    public function generarExcelCantidades(){
        $empresas_ids = [1, 2, 3, 4, 5];


        $empresas = Empresas::whereIn('id', $empresas_ids)->get();

        $totales = array();
        foreach($empresas as $empresa){
            $emp_proyectos = Proyectos::where('empresa_id', $empresa->id)
                ->select('id')
                ->pluck('id')
                ->toArray();

            $emp_solicitudes = Solicitudes::whereIn('proyecto_id', $emp_proyectos)->get();

            $totales[$empresa->id]['creadas'] = 0;
            $totales[$empresa->id]['autorizadas'] = 0;
            $totales[$empresa->id]['revisadas'] = 0;
            $totales[$empresa->id]['aprobadas'] = 0;
            $totales[$empresa->id]['tesoreria'] = 0;
            $totales[$empresa->id]['rendidas'] = 0;
            $totales[$empresa->id]['finalizadas'] = 0;
            $totales[$empresa->id]['observadas'] = 0;
            $totales[$empresa->id]['rechazadas'] = 0;
            $totales[$empresa->id]['total'] = count($emp_solicitudes);
            foreach($emp_solicitudes as $emp_solicitud){
                $estado = $this->getLastEstadoCodigo($emp_solicitud->id);
                $array_creadas = ['SOL', 'SOL-M'];
                $array_autorizadas = ['AUT', 'AUT-M', 'AUT-O'];
                $array_revisadas = ['REV', 'REV-M'];
                $array_aprobadas = ['APR', 'APR-P', 'APR-M'];
                $array_tesoreria = ['TES', 'TES-M'];
                $array_rendidas = ['REN', 'REN-P', 'REN-F', 'REN-A', 'APR-RO', 'REN-OA', 'REN-OB', 'REN-OC'];
                $array_observadas = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];
                $array_rechazadas = ['AUT-R', 'REV-R', 'APR-R', 'TES-R', 'REN-R', 'SOL-A'];
                $array_fin = ['FIN'];

                if(in_array($estado, $array_creadas)){
                    $totales[$empresa->id]['creadas'] += 1;
                }

                if(in_array($estado, $array_autorizadas)){
                    $totales[$empresa->id]['autorizadas'] += 1;
                }

                if(in_array($estado, $array_revisadas)){
                    $totales[$empresa->id]['revisadas'] += 1;
                }

                if(in_array($estado, $array_aprobadas)){
                    $totales[$empresa->id]['aprobadas'] += 1;
                }

                if(in_array($estado, $array_tesoreria)){
                    $totales[$empresa->id]['tesoreria'] += 1;
                }

                if(in_array($estado, $array_rendidas)){
                    $totales[$empresa->id]['rendidas'] += 1;
                }

                if(in_array($estado, $array_observadas)){
                    $totales[$empresa->id]['observadas'] += 1;
                }

                if(in_array($estado, $array_rechazadas)){
                    $totales[$empresa->id]['rechazadas'] += 1;
                }

                if(in_array($estado, $array_fin)){
                    $totales[$empresa->id]['finalizadas'] += 1;
                }
            }
        }
        //dd($totales);
        
        $data = array();
        array_push($data, ['EMPRESA', 'CREADAS', 'AUTORIZADAS', 'REVISADAS', 'APROBADAS', 'TESORERIA', 'RENDIDAS', 'OBSERVADAS', 'RECHAZADAS', 'FINALIZADAS', 'TOTALES']);
        foreach($totales as $key => $total){
            $empresa = Empresas::find($key);

            array_push($data,[
                $empresa->nombre,
                $total['creadas'],
                $total['autorizadas'],
                $total['revisadas'],
                $total['aprobadas'],
                $total['tesoreria'],
                $total['rendidas'],
                $total['observadas'],
                $total['rechazadas'],
                $total['finalizadas'],
                $total['creadas'] + $total['autorizadas'] + $total['revisadas'] + $total['aprobadas'] + $total['tesoreria'] + $total['rendidas'] + $total['observadas'] + $total['rechazadas'] + $total['finalizadas']
            ]);

        }

        Excel::create('Totales_'.date('dmY'), function($excel) use ($data){
            $hoja = 1;
            $excel->sheet('Solicitudes Totales'.$hoja, function($sheet) use ($data){
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('xls');
    }

    public function getLastEstado($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.descripcion')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados.id', 'DESC')
            ->first();
        return $lastEstado->descripcion;
    }

    public function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    /**
     * Importa los Items de un archivo de Excel
     * 
     * 
     */
    public function importExcel(Request $request){
        $mensaje_error = '';

        $items_excel = array();
        
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $archivo = $request->file('file');
            //REVISAR IMPORTACION
            if($archivo->getClientOriginalExtension() === 'xlsx' && ((int)($archivo->getClientSize()/1024)) <= 300){
                $data = Excel::selectSheetsByIndex(0)->load($path, function($reader) {})->get();
                
                // dd($data);
                
                if(!empty($data) && $data->count()){
                    $bandera = 0;
                    $contador = 0;

                    //Array ItemsTipo
                    $item_tipos = ItemsTipos::all();
                    $array_tipos = array();
                    foreach($item_tipos as $tipo){
                        $array_tipos[] = [
                            'id' => $tipo->id,
                            'descripcion' => $tipo->descripcion
                        ];
                    }
                    //Array Unidades
                    $unidades = Unidades::all();
                    $array_unidades = array();
                    foreach($unidades as $unidad){
                        $array_unidades[] = [
                            'id' => $unidad->id,
                            'descripcion' => $unidad->descripcion,
                        ];
                    }

                    $mensaje_error = '';

                    try{
                        foreach($data->toArray() as $key=>$value){
                            $contador++;
                            $campos = array_keys($value);
                            
                            $campos_importacion = array(
                                'cantidad', 'unidad', 'tipo_compra', 'detalle', 'nro_factura_recibo', 'fecha_factura_recibo', 'proyecto', 'proveedor', 'tipo', 'p_unitario', 'sub_total'
                            );

                            // Control Fila vacía
                            if(
                                $value['unidad'] == '' &&
                                $value['tipo_compra'] == '' &&
                                $value['detalle'] == ''
                            ){
                                continue;
                            }
                            // Control Fila vacía
                            
                            if($campos_importacion === $campos){
                                $value['cantidad'] = trim($value['cantidad']);
                                $value['unidad'] = trim($value['unidad']);
                                $value['tipo_compra'] = trim($value['tipo_compra']);
                                $value['detalle'] = trim($value['detalle']);
                                $value['nro_factura_recibo'] = trim($value['nro_factura_recibo']);
                                $value['fecha_factura_recibo'] = trim($value['fecha_factura_recibo']);
                                $value['proyecto'] = trim($value['proyecto']);
                                $value['proveedor'] = trim($value['proveedor']);
                                $value['tipo'] = trim($value['tipo']);
                                $value['p_unitario'] = trim($value['p_unitario']);

                                //campo cantidad
                                if($value['cantidad'] == ''){
                                    $mensaje_error = 'Sin dato en la columna CANTIDAD';
                                    $bandera = 1;
                                    break;
                                }
                                //campo unidad
                                if($value['unidad'] == ''){
                                    $mensaje_error = 'Sin dato en la columna UNIDAD';
                                    $bandera = 1;
                                    break;
                                }
                                //campo tipo
                                if($value['tipo_compra'] == ''){
                                    $mensaje_error = 'Sin dato en la columna TIPO_COMPRA';
                                    $bandera = 1;
                                    break;
                                }
                                //campo detalle
                                if($value['detalle'] == ''){
                                    $mensaje_error = 'Sin dato en la columna DETALLE';
                                    $bandera = 1;
                                    break;
                                }

                                //campo nro_factura
                                if($value['nro_factura_recibo'] == ''){
                                    $mensaje_error = 'Sin dato en la columna NRO_FACTURA';
                                    $bandera = 1;
                                    break;
                                }

                                //campo fecha_factura
                                if($value['fecha_factura_recibo'] == ''){
                                    $mensaje_error = 'Sin dato en la columna FECHA_FACTURA';
                                    $bandera = 1;
                                    break;
                                }

                                //campo proyecto
                                if($value['proyecto'] == ''){
                                    $mensaje_error = 'Sin dato en la columna PROYECTO';
                                    $bandera = 1;
                                    break;
                                }

                                //campo proveedor
                                if($value['proveedor'] == ''){
                                    $mensaje_error = 'Sin dato en la columna PROVEEDOR';
                                    $bandera = 1;
                                    break;
                                }

                                //campo proveedor
                                if($value['tipo'] == ''){
                                    $mensaje_error = 'Sin dato en la columna TIPO';
                                    $bandera = 1;
                                    break;
                                }

                                //campo unitario
                                if($value['p_unitario'] == ''){
                                    $mensaje_error = 'Sin dato en la columna P_UNITARIO';
                                    $bandera = 1;
                                    break;
                                }
                                
                                if($bandera == 0 && !empty($value)){
                                    // cantidad
                                    if(!is_numeric($value['cantidad'])){
                                        $mensaje_error = "Verifique que la columna 'CANTIDAD' sea un número (".$value['cantidad'].")";
                                    }
                                    if($mensaje_error != ''){
                                        $bandera = 1;
                                        break;
                                    }

                                    // unidad_id
                                    foreach($array_unidades as $key => $unidad){
                                        if($unidad['descripcion'] == $value['unidad']){
                                            $unidad_id = $unidad['id'];
                                            $mensaje_error = '';
                                            break;
                                        }else{
                                            $mensaje_error = "Verifique la columna 'UNIDAD' (".$value['unidad'].")";
                                        }
                                    }
                                    if($mensaje_error != ''){
                                        $bandera = 1;
                                        break;
                                    }

                                    // tipo_id
                                    $value['tipo_compra'] = trim($value['tipo_compra']);
                                    foreach($array_tipos as $key => $tipo){
                                        if($tipo['descripcion'] == $value['tipo_compra']){
                                            $tipo_id = $tipo['id'];
                                            $mensaje_error = '';
                                            break;
                                        }else{
                                            $mensaje_error = "Verifique la columna 'TIPO_COMPRA' (".$value['tipo_compra'].")";
                                        }
                                    }
                                    if($mensaje_error != ''){
                                        $bandera = 1;
                                        break;
                                    }

                                    // precio unitario
                                    if(!is_numeric($value['p_unitario'])){
                                        $mensaje_error = "Verifique que la columna 'P_UNITARIO' sea un número (".$value['p_unitario'].")";
                                    }
                                    if($mensaje_error != ''){
                                        $bandera = 1;
                                        break;
                                    }

                                    // Generar Item
                                    $item = new Items();
                                    $item->cantidad = $value['cantidad'];
                                    $item->detalle = $value['detalle'];
                                    $item->nro_factura = $value['nro_factura_recibo'];
                                    $item->fecha_factura = $value['fecha_factura_recibo'];
                                    $item->proyecto = $value['proyecto'];
                                    $item->proveedor = $value['proveedor'];
                                    $item->tipo = $value['tipo'];
                                    $item->costo = $value['p_unitario'];
                                    $item->unidad_id = $unidad_id;
                                    $item->tipo_id = $tipo_id;

                                    $items_excel = array_add($items_excel, ($contador - 1), $item);
                                }
                            }else{
                                $mensaje_error = 'El archivo que subió no cuenta con el formato predefinido; favor descargue la plantilla, luego llene los campos y vuelva a subir el archivo';
                                $contador = 0;
                                $bandera = 1;
                                break;
                            }
                        }
                    } catch (Exception $e){
                        return response()->json([
                            'error' => 'Ocurrió un problema al importar el archivo, favor verifique el archivo y vuelva a intentarlo',
                        ]);
                    }

                    if($bandera){
                        return response()->json([
                            'error' => $mensaje_error. ' Fila: '.($contador + 1),
                        ]);
                    }else{
                        return response()->json([
                            'success' => 'Importación correcta, favor verifique en la sección de ítems',
                            'items' => $items_excel,
                        ]);
                    }
                }else{
                    return response()->json([
                        'error' => 'El archivo no cuenta con ítems, favor verifique el archivo',
                    ]);
                }
            }else{
                //Extension invalida
                return response()->json([
                    'error' => 'El archivo que intenta subir, no es un archivo de excel (*.xlsx) o es demasiado grande',
                ]);
            }
        }else{
            return response()->json([
                'error' => 'Favor seleccione un archivo válido',
            ]);
        }
    }
}
