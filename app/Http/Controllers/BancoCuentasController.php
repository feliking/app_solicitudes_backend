<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CuentaBancaria;
use App\BancoEmpresas;
use App\Monedas;
use App\Bancos;

use Session;

class BancoCuentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bancos = Bancos::all();
        $cuentas = CuentaBancaria::where('empresa_id', session('empresa'))->get();

        return View('cuentas.index')->withCuentas($cuentas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $monedas = Monedas::all();
        $bancos = Bancos::all();
        return view('cuentas.crear')->withMonedas($monedas)->withBancos($bancos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'numero' => 'required',
            'banco' => 'required',
            'moneda' => 'required',
        ]);

        $cuenta = new CuentaBancaria();
        $cuenta->numero = $request->numero;
        $cuenta->monto_inicial = $request->monto_inicial;
        $cuenta->saldo = $request->monto_inicial;
        $cuenta->moneda_id = $request->moneda;
        $cuenta->banco_id = $request->banco;
        $cuenta->empresa_id = session('empresa');

        $cuenta->save();

//        $banco_empresa = new BancoEmpresas();
//        $banco_empresa->banco_id = $request->banco;
//        $banco_empresa->empresa_id = session('empresa');
//        $banco_empresa->save();

        Session::flash('success', 'Cuenta Creada!!!');
        return redirect()->action('BancoCuentasController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
