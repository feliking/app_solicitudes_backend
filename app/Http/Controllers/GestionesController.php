<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gestiones;
use App\Empresas;

use Session;
use Mail;
use DB;

class GestionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function actualizaGestion($gestion_id){
        $usuario = auth()->user();
        session()->forget('gestion');
        session()->forget('gestion_id');
        $gestion = Gestiones::find($gestion_id);
        session(['gestion_id' => [$gestion->id]]);
        session(['gestion' => $gestion->gestion]);
        if($usuario->rol_id == 8){
            session(['empresa' => null]);
            $empresa = Empresas::find(session('empresa'));
            return redirect()->route('solicitudes.admin');
        }else{
            return redirect()->route('solicitudes');
        }
    }
}
