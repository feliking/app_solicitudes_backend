<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\AutorizadorUsuario;
use App\NivelesAprobacion;
use App\Proyectos;
use App\Empresas;
use App\Empleado;
use App\Roles;
use App\User;
use App\NivelAprobador;
use Session;
use Mail;
use DB;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('usuarios.index')->withUsuarios($usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = Empresas::orderBy('nombre')
            ->pluck('nombre', 'id');
        
        $roles = Roles::all();
        $niveles = NivelesAprobacion::all();
        
        return view('usuarios.crear')
            ->withNiveles($niveles)
            ->withEmpresas($empresas)
            ->withRoles($roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $this->validate($request,[
            'usuario' => 'required|unique:users,usuario',
            //'email' => 'required',
            'password' => 'required',
            // 'empleado_id' => 'required|unique:users,empleado_id',
            // 'empresa_id' => 'required',
            // 'acceso' => 'required',
        ]);

//        Mail::send('correos.test', array(), function($message) use ($request){
//            //$message->to('dive777@gmail.com', 'Erick Alvarez')->subject('Correo de Solicitudes!');
//            $message->to('eralvarez@pragmainvest.com.bo', mb_strtoupper($request['nombre']).' '.mb_strtoupper($request['ap']))->subject('Usuario: '.$request['usuario']." Clave: ".$request['password']);
//            $message->to($request['email'], mb_strtoupper($request['nombre']).' '.mb_strtoupper($request['ap']))->subject('Usuario: '.$request['usuario']." Clave: ".$request['password']);
//        });

        if($request->rendiciones){
            $rendiciones = true;
        }else{
            $rendiciones = false;
        }

        if($request->asignar_proyecto){
            $asignar_proyecto = true;
        }else{
            $asignar_proyecto = false;
        }

        $new_user_id = User::select('id')->where('id', '<', 1000)->max('id') + 1;

        // $nombre = explode('-', $request->empleado_id);
        $empleado = Empleado::find($request->empleado_id);
        $nombre = $empleado->nombres . ' ' . $empleado->apellido_1 . ' ' . $empleado->apellido_2;
        $usuario = new User;
        $usuario->id        = $new_user_id;
        $usuario->nombre    = mb_strtoupper($nombre);
        $usuario->usuario   = $request->usuario;
        $usuario->password  = bcrypt($request->password);
        $usuario->apr_rendiciones = $rendiciones;
        $usuario->asignador_proyecto = $asignar_proyecto;
        $usuario->empleado_id    = $empleado->id;
        $usuario->rol_id    = $request->acceso;
        $usuario->save();

        if(isset($request->nivel)){
            $niveles = $request->nivel_id;
            $porcentajes = $request->nivel;
            for ($i = 0; $i < count($niveles); $i++) {
                $usuario->nivel_aprobaciones()->attach($niveles[$i], ['porcentaje' => $porcentajes[$i]]);
            }
        }

        $usuarios = User::all();

        Session::flash('success', 'Usuario '.$request->usuario.' Creado!!!');
        return redirect()->action('UsuarioController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresas = Empresas::orderBy('nombre')
            ->pluck('nombre', 'id');

        $niveles = NivelesAprobacion::all();
        $usuario = User::find($id);
        $roles = Roles::all();        
        return view('usuarios.editar', compact('usuario', 'roles', 'niveles'))
            ->withEmpresas($empresas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $this->validate($request, [
            'usuario' => 'required|unique:users,usuario,'.$id,
            'password' => 'min:6',
            'confirm_password' => 'required_with:password|min:6|same:password',
        ]);

        $messages = [
            'confirm_password.same' => 'el campo de confirmacion de contraseña es obligatorio'
        ];

        $usuario = User::find($id);
        $usuario->usuario = $request->usuario;
        if(strlen($request['password'])>5){
            $usuario->password = bcrypt($request['password']);
        }
        $usuario->rol_id = $request->acceso;
        if($request->rendiciones){
            $usuario->apr_rendiciones = true;
        }

        else{
            $usuario->apr_rendiciones = false;
        }
        if($request->asignar_proyecto){
            $usuario->asignador_proyecto = true;
        }else{
            $usuario->asignador_proyecto = false;
        }

        $usuario->save();

        if(isset($request->nivel)){            
            if(isset($request->nivel_aprobador_id)){
                $nivel_aprobador_ids = $request->nivel_aprobador_id;
                $porcentajes_actualizados = $request->nivel;
                $nivel_ids = $request->nivel_id;
                for ($i = 0; $i < count($nivel_aprobador_ids); $i++) {
                    DB::table('nivel_aprobador')
                        ->where('id', $nivel_aprobador_ids[$i])
                        ->update(['porcentaje' => $porcentajes_actualizados[$i]]);
                }
            }else{                
                $niveles = $request->nivel_id;
                $porcentajes = $request->nivel;
                for ($i = 0; $i < count($niveles); $i++) {
                    $usuario->nivel_aprobaciones()->attach($niveles[$i], ['porcentaje' => $porcentajes[$i]]);
                }
            }
        }else{
            $usuario->nivel_aprobaciones()->detach();
        }

        return redirect()->route('usuario.index')->with('success','Usuario Actualizada Correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuarios = User::all();
        User::find($id)->delete();
        return redirect()->route('usuario.index')->with('success','Usuario Eliminada Correctamente!');
    }

    public function UsuarioEmpresa(Request $request){
        //echo "UsuarioController@UsuarioEmpresa";
        $empresas_ids = DB::table('empresa_users')->select('empresa_id')->where('user_id','=', $request->id_usuario)->get();
        $array_ids = array();
        foreach($empresas_ids as $empresa){
            $array_ids[] = $empresa->empresa_id;
        }
        $empresas = Empresas::all();
        foreach($empresas as $empresa){
            if(in_array($empresa->id, $array_ids)){
                $empresa->checked = true;
            }else{
                $empresa->checked = false;
            }
        }
        return view('usuarioempresa.form')->withEmpresas($empresas);
    }

    public function UsuarioEmpresaStore(Request $request){
        //echo "UsuarioController@UsuarioEmpresaStore";
        $usuario = User::find($request->usuario_id);
        if(count($request->empresa) == 0){
            $usuario->empresas()->sync(array());
        }else{
            $usuario->empresas()->sync($request->empresa);
        }
        $usuarios = User::all();
        return view('usuarios.index', compact('usuarios'));
    }

    public function UsuarioProyecto(){
        //echo "UsuarioController@UsuarioProyecto";
        $proy_ids_padres = Proyectos::where('padre_id', null)
            ->select('proyectos.id')
            ->where('empresa_id', session('empresa'))
            ->get()
            ->pluck('id');
        $proyectos = array();
        foreach($proy_ids_padres as $proy_padre){
            $proy_padre = Proyectos::find($proy_padre);
            $proyectos[] = $proy_padre;
            $proy_hijos = Proyectos::where('padre_id', $proy_padre->id)->get();
            if(count($proy_hijos) > 0){
                foreach($proy_hijos as $proy_hijo){
                    $sub_proyecto = Proyectos::find($proy_hijo->id);
                    $sub_proyecto->nombre = $proy_padre->nombre." &#10148; ".$proy_hijo->nombre;
                    $proyectos[] = $sub_proyecto;
                    $proy_subsub = Proyectos::where('padre_id', $sub_proyecto->id)->get();
                    if(count($proy_subsub) > 0){
                        foreach($proy_subsub as $proy_nieto){
                            $nieto_proyecto = Proyectos::find($proy_nieto->id);
                            $nieto_proyecto->nombre = $sub_proyecto->nombre." &#10148; ".$nieto_proyecto->nombre;
                            $proyectos[] = $nieto_proyecto;
                        }
                    }
                }
            }
        }
        $usuarios = User::all();
        foreach($usuarios as $usuario){
            $query = "SELECT proyecto_id FROM proyectos_users WHERE user_id = ".$usuario->id;
            $proys_ids = DB::select($query);
            $ids = array();
            foreach($proys_ids as $proys_id){
                $ids[] = $proys_id->proyecto_id;
            }
            $usuario_proyectos[$usuario->id] = $ids;
        }
        return view('usuarioproyecto.index')->withUsuarios($usuarios)->withProyectos($proyectos)->withUsuario_proyectos($usuario_proyectos);
    }

    public function UsuarioProyectoStore(Request $request){
        $array_proyectos_empresa = Proyectos::where('empresa_id', session('empresa'))->pluck('id')->toArray();
        $query = "DELETE FROM proyectos_users WHERE user_id = ".$request->usuario_id." AND proyecto_id IN (".implode(',', $array_proyectos_empresa).")";
        $proys_ids = DB::delete($query);

        $proyectos = 'proyecto_'.$request->usuario_id;
        foreach($request->$proyectos as $proyecto){
            DB::insert('INSERT INTO proyectos_users (proyecto_id, user_id) values (?, ?)', [$proyecto, $request->usuario_id]);
        }
        return back();
    }

    public function usuarioProyectoStoreIndividual(Request $request){
        DB::insert('INSERT INTO proyectos_users (proyecto_id, user_id) values (?, ?)', [$request->proyecto_id, $request->usuario_id]);
        return response($content = ["proyecto_id" => $request->proyecto_id, "usuario_id" => $request->usuario_id], $status = 200);
    }

    public function AutorizadorUsuarioProyectoLista(){
        $autorizadores = array();
        $autorizadores = User::where('rol_id', '=', 3)
            ->orwhere('revisor', true)
            ->get();

        return view('autorizadorusuarioproyecto.index')
            ->withAutorizadores($autorizadores);
    }
    
    public function AutorizadorUsuarioProyecto($autorizador_id){
        $proyectos = Proyectos::where('empresa_id', session('empresa'))
            ->whereIn('gestion_id', session('gestion_id'))
            ->where('padre_id', 0)
            ->get();
        $array_proyectos = array();
        foreach($proyectos as $proyecto){
            $array_usuarios = array();
            foreach($proyecto->usuarios as $usuario){
                $array_usuarios[] = [
                    'id' => $usuario->id,
                    'nombre' => $usuario->nombre." ".$usuario->ap." ".$usuario->am
                ];
            }
            $array_proyectos[]=[
                'id' => $proyecto->id,
                'nombre' => $proyecto->nombre,
                'descripcion' => $proyecto->descripcion,
                'usuarios' => $array_usuarios
            ];
            $sub_proyectos = Proyectos::where('padre_id', $proyecto->id)->get();
            foreach($sub_proyectos as $sub_proyecto){
                $array_usuarios = array();
                foreach($sub_proyecto->usuarios as $usuario){
                    $array_usuarios[] = [
                        'id' => $usuario->id,
                        'nombre' => $usuario->nombre." ".$usuario->ap." ".$usuario->am
                    ];
                }
                $array_proyectos[]=[
                    'id' => $sub_proyecto->id,
                    'nombre' => "&#10148;".$sub_proyecto->nombre,
                    'descripcion' => $sub_proyecto->descripcion,
                    'usuarios' => $array_usuarios
                ];
            }
        }
        //FALTA USUARIO
        $array_autorizaciones = array();
        $query = "SELECT proyecto_id, usuario_id FROM autorizador_usuarios WHERE empresa_id = ".session('empresa')." AND autorizador_id = ".$autorizador_id;
        $autorizaciones = DB::select($query);
        foreach($autorizaciones as $autorizacion){
            $array_autorizaciones[] = $autorizacion->proyecto_id."-,-".$autorizacion->usuario_id;
        }
        return view('autorizadorusuarioproyecto.asignar')->withProyectos($array_proyectos)->withAutorizador_id($autorizador_id)->withAutorizaciones($array_autorizaciones);
    }

    public function AutorizadorUsuarioProyectoStore(Request $request){

        //Eliminar relaciones anteriores
        $query = "DELETE FROM autorizador_usuarios WHERE autorizador_id = ".$request->autorizador_id." AND empresa_id = ".session('empresa');
        DB::delete($query);

        foreach($request->usuario_proyecto as $usuario_proyecto){
            $array_datos = array();
            $array_datos = explode("-,-", $usuario_proyecto);
            var_dump($usuario_proyecto);
            echo "Autorizador_id: ".$request->autorizador_id."<br>";
            echo "Usuario_id: ".$array_datos[1]."<br>";
            echo "Proyecto_id: ".$array_datos[0]."<br>";
            echo "Empresa_id: ".session('empresa')."<br><br>";

            $autorizacion = new AutorizadorUsuario();
            $autorizacion->autorizador_id = $request->autorizador_id;
            $autorizacion->usuario_id = $array_datos[1];
            $autorizacion->proyecto_id = $array_datos[0];
            $autorizacion->empresa_id = session('empresa');
            $autorizacion_verify = AutorizadorUsuario::where('autorizador_id', $request->autorizador_id)
                ->where('usuario_id', $array_datos[1])
                ->where('proyecto_id', $array_datos[0])
                ->get();
            if(count($autorizacion_verify) == 0){
                $autorizacion->save();
            }
        }
        return back();
    }

    public function AutorizadorUsuarioStore(Request $request){
        //echo "UsuarioController@AutorizadorUsuarioStore";
        $autorizador_id = $request->autorizador_id;
        $empresa_id = intval($request->empresa_id);
        $array_autorizaciones = $request->proyectos_usuario;
        //echo "autorizador id: ".$autorizador_id."<br>empresa id: ".$empresa_id."<br>";
        $query = "DELETE FROM autorizador_usuarios WHERE autorizador_id = ".$request->autorizador_id." AND empresa_id = ".session('empresa');
        DB::delete($query);
        if(isset($array_autorizaciones) && count($array_autorizaciones) > 0 )
        foreach($array_autorizaciones as $autorizacion){
            $array_autorizacion = explode('_', $autorizacion);
            DB::insert('INSERT INTO autorizador_usuarios (autorizador_id, usuario_id, proyecto_id, empresa_id)VALUES(?, ?, ?, ?)', [$autorizador_id, $array_autorizacion[1], $array_autorizacion[0], $empresa_id]);
        }
        return back();
    }

    public function AprorendicionEmpresaStore(Request $request){
        //echo "UsuarioController@AutorizadorUsuarioStore";
        $aprorendicion_id = $request->autorizador_id;
        $empresa_id = intval($request->empresa_id);
        $array_autorizaciones = $request->proyectos_usuario;
        //echo "autorizador id: ".$autorizador_id."<br>empresa id: ".$empresa_id."<br>";
        $query = "DELETE FROM aprorendicion_empresa WHERE aprorendicion_id = ".$aprorendicion_id;
        DB::delete($query);
        if(isset($array_autorizaciones) && count($array_autorizaciones) > 0 ){
            foreach($array_autorizaciones as $autorizacion){
                $array_autorizacion = $autorizacion;
                DB::insert('INSERT INTO aprorendicion_empresa (aprorendicion_id, empresa_id)VALUES(?, ?)', [$aprorendicion_id, $array_autorizacion]);
            }
        }
        
        return back();
    }

    public function UsuarioClave(){
        $usuario = auth()->user();
        return view('usuarios.password', compact('usuario'));
    }

    public function UsuarioClaveUpdate(Request $request){
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
            // 'confirm_password' => 'required|confirmed'
        ]);

        $usuario = auth()->user();

        // if(password_verify($request->old_password, $usuario->password) && strlen($request->password) && $request->password == $request->confirm_password){

        if(\Hash::check($request->old_password, $usuario->password)){
            
            $usuario->password = bcrypt($request->password);
            $usuario->update();

            return redirect()->route('clave/usuario')->with('success','Contraseña Actualizada Correctamente');
        }else{
            return redirect()->route('clave/usuario');
        }
    }

    public function AprorendicionEmpresaLista(){
        $aprorendiciones = array();
        $aprorendiciones = User::where('apr_rendiciones', 1)
            ->get();

        return view('aprorendiciones.index')
            ->withAutorizadores($aprorendiciones);
    }
}