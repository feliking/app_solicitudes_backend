<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use Session;

use App\DocumentosRendicion;
use App\ItemsRendiciones;
use App\Configuraciones;
use App\ItemsController;
use App\UsuarioProyecto;
use App\CuentaBancaria;
use App\TiposCambios;
use App\Devoluciones;
use App\Solicitudes;
use App\Modalidades;
use App\ItemsTipos;
use App\Documentos;
use App\Proyectos;
use App\Gestiones;
use App\Empresas;
use App\Unidades;
use App\Monedas;
use App\Estados;
use App\Items;
use App\User;

class ConfiguracionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configuraciones = Configuraciones::orderBy('variable', 'ASC')->pluck('valor', 'variable');
        return view('configuraciones.index')
            ->withConfiguraciones($configuraciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $item)
    {
        $configuracion = Configuraciones::where('variable',  $item)->first();
        $configuracion->valor = $request->variable;
        $configuracion->update();

        Session::flash('success', 'Variable "'.$item.'" actualizado a: "'.$request->variable.'"');
        $configuraciones = Configuraciones::orderBy('variable', 'ASC')->pluck('valor', 'variable');
        return view('configuraciones.index')
            ->withConfiguraciones($configuraciones);
    }

    function solicitudDuracion(){
        $vidautil = Configuraciones::select('valor')
            ->where('variable', 'vidautil')
            ->first()
            ->toArray();
        $tiempoVida = intVal($vidautil['valor']);

        
        $solicitudes_all = Solicitudes::select('id')
            ->pluck('id')
            ->toArray();
        
        $solicitudes_ids = array();
        $fechaActual = strtotime(Carbon::now());
        foreach($solicitudes_all as $sol_id){
            $fecha_solicitud = Solicitudes::find($sol_id);
            $fec_sol = strtotime($fecha_solicitud->created_at);
            if($this->getLastEstadoCodigo($sol_id) != 'SOL-A'
                && $this->getLastEstadoCodigo($sol_id) != 'AUT-R'
                && $this->getLastEstadoCodigo($sol_id) != 'REV-R'
                && $this->getLastEstadoCodigo($sol_id) != 'APR-R'
                && $this->getLastEstadoCodigo($sol_id) != 'TES-R'
                && $this->getLastEstadoCodigo($sol_id) != 'SOL-V'
                && $this->getLastEstadoCodigo($sol_id) != 'FIN'
                && ($fechaActual - $fec_sol > $tiempoVida)){
                $solicitudes_ids[] = $sol_id;
            }
        }
        dd($solicitudes_ids);
    }

    public function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
