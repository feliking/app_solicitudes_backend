<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use App\MovimientosBancarios;
use App\ChequesEstados;
use App\CuentaBancaria;
use App\Solicitudes;
use App\Proyectos;
use App\Cheques;
use App\Estados;
use App\Bancos;
use App\Items;
use Session;
use DB;

class ChequesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $search = '*')
    {
        $busqueda = "";
        $busqueda_texto = "";
        if(isset($request)){
            switch($request->selectOpcion){
                case 1:
                    //NRO. DE CHEQUE
                    if($request->numero != ''){
                        $busqueda = "numero_cheque = ".$request->numero;
                        $busqueda_texto = "NRO. CHEQUE = ". $request->numero;
                    }
                    break;
                case 2:
                    //MONTO
                    if($request->numero != ''){
                        $busqueda = "monto = ".$request->numero;
                        $busqueda_texto = "MONTO = ". $request->numero;
                    }
                    break;
                case 3:
                    //FECHA
                    if(isset($request->fechaIni) && $request->fechaIni != ""){
                        if(isset($request->fechaFin) && $request->fechaFin != ""){
                            $busqueda = "fecha BETWEEN '".$request->fechaIni."' AND '".$request->fechaFin."'";
                            $busqueda_texto = "FECHAS DESDE: ". $request->fechaIni." HASTA: ".$request->fechaFin;
                        }else{
                            $busqueda = "fecha LIKE '".$request->fechaIni."'";
                            $busqueda_texto = "FECHA: ". $request->fechaIni;
                        }
                    }
                    break;
                case 4:
                    //BENEFICIARIO
                    if($request->buscar != ''){
                        $busqueda = "beneficiario LIKE '%".$request->buscar."%'";
                        $busqueda_texto = "BENEFICIARIO: ". $request->buscar;
                    }
                    break;
                case 5:
                    //BANCO
                    $banco_ids = Bancos::where('nombre', 'LIKE', "%$request->buscar%")
                        ->orWhere('codigo', 'LIKE', "%$request->buscar%")
                        ->pluck('id')
                        ->toArray();
                    $cuentas_ids = CuentaBancaria::whereIn('empresa_id', $banco_ids)
                        ->pluck('id')
                        ->toArray();
                    $busqueda = " cuenta_id IN (". implode(',', $cuentas_ids).")";
                    $busqueda_texto = "BANCO: ". $request->buscar;
                    break;
                case 6:
                    //NRO. DE CUENTA
                    $cuentas_ids = CuentaBancaria::where('numero', 'LIKE',  "%$request->buscar%")
                        ->pluck('id')
                        ->toArray();
                    $busqueda = " cuenta_id IN (". implode(',', $cuentas_ids).")";
                    $busqueda_texto = "CUENTA: ". $request->buscar;
                    break;
                case 7:
                    //CONCEPTO
                    $busqueda = "concepto LIKE '%".$request->buscar."%'";
                    $busqueda_texto = "CONCEPTO: ". $request->buscar;
                    break;
            }
        }
        $array_proyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();
        $solicitudes = Solicitudes::whereIn('proyecto_id', $array_proyectos)
            ->whereIn('gestion_id', session('gestion_id'))
            ->pluck('id')
            ->toArray();

        if($busqueda == ""){
            $cheques_array = Cheques::whereIN('solicitud_id', $solicitudes)
                ->get();
        }else{
            $cheques_array = DB::select('SELECT * FROM cheques WHERE '.$busqueda);
        }        

        $cheques_ids = array();
        foreach($cheques_array as $cheque_ele){
            if($this->getLastEstadoCheque($cheque_ele->id) == "EMITIDO"){
                $cheques_ids[] = $cheque_ele->id;
            }
        }

        $cheques = Cheques::sortable()
            ->orderBy('solicitud_id', 'DESC')
            ->orderBy('fecha', 'ASC')
            ->orderBy('numero_cheque', 'ASC')
            ->orderBy('cuenta_id', 'ASC')
            ->orderBy('monto', 'ASC')
            ->orderBy('beneficiario', 'ASC')
            ->orderBy('concepto', 'ASC')
            ->whereIn('id', $cheques_ids)
            ->paginate(session('paginacion'));        

        return View('cheques.index')
            ->withBusquedatexto($busqueda_texto)
            ->withBusqueda($busqueda)
            ->withCheques($cheques);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $usuario = auth()->user();
        $mensaje = "Pago realizado correctamente";
        for($i = 0; $i < count($request->monto_cheque); $i++){
            if($request->monto_cheque[$i] > 0 && $request->cuenta[$i] != 0){
                $cheque = new Cheques;
                $cheque->numero_cheque = $request->nro_cheque[$i];
                $cheque->beneficiario = mb_strtoupper($request->beneficiario[$i]);
                $cheque->fecha = $request->fecha[$i];
                $cheque->monto = floatval($request->monto_cheque[$i]);
                $cheque->observacion = mb_strtoupper($request->observacion[$i]);
                $cheque->concepto = mb_strtoupper($request->concepto[$i]);
                $cheque->cuenta_id = $request->cuenta[$i];
                $cheque->solicitud_id = $request->solicitud_id;
                $cheque->empresa_id = session('empresa');
                $cheque->save();
                
                if($request->pago[$i] == 1){
                    if($request->cheque[$i] !== ''){
                        $cheque_find = Cheques::where('numero_cheque', $request->cheque)
                            ->where('solicitud_id', $request->solicitud_id)
                            ->where('cuenta_id', $request->cuenta_id)
                            ->get();
                        if(sizeof($cheque_find)>0){
                            return back()->withErrors(array('message' => 'VERIFIQUE EL NÚMERO DE CHEQUE!!!'));
                        }
                    }
                    if(count($request->monto_cheque) > 1){
                        $mensaje = "Cheques ".implode(',', $request->nro_cheque)." creados!!!";
                    }else{
                        $mensaje = "Cheque ".implode(',', $request->nro_cheque)." creado!!!";
                    }
                }

                DB::table('cheques_estados')->insert([
                    'usuario_id' => $usuario->id,
                    'cheque_id' => $cheque->id,
                    'cheque_estado_id' => 1,
                    "created_at" =>  Carbon::now(),
                    "updated_at" => Carbon::now(),
                ]);

                //Egreso
                $egreso = new MovimientosBancarios;
                $egreso->monto = $request->monto[$i];
                $egreso->cuenta_ori_id = $request->cuenta[$i];
                $egreso->cuenta_des_id = null;
                $egreso->tipo_movimiento_id = 3;
                $egreso->created_at = Carbon::now();
                $egreso->updated_at = Carbon::now();
                $egreso->save();
            }
        }
        
        //Egreso
        $egreso = new MovimientosBancarios;
        $egreso->monto = $request->monto_real;
        // $egreso->cuenta_ori_id = $request->cuenta_id;
        $egreso->cuenta_ori_id = $request->numero_cuenta;
        $egreso->cuenta_des_id = null;
        $egreso->tipo_movimiento_id = 3;
        $egreso->created_at = Carbon::now();
        $egreso->updated_at = Carbon::now();
        $egreso->save();

        DB::table('estados_solicitud')->insert([
            'observacion' => 'REGISTRO DE TESORERIA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $request->solicitud_id,
            'estado_id' => 50,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        $solicitud = Solicitudes::find($request->solicitud_id);
        if($solicitud->modalidad_id == 2 || $solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4 || $solicitud->modalidad_id == 5){
            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD FINALIZADA',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $request->solicitud_id,
                'estado_id' => 90,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);

            if ($solicitud->modalidad_id == 5)
            {
                $transferencia = $solicitud->transferencia;
                $transferencia->aprobado = true;
                $transferencia->update();
            }
        }

        $proyectos_ids = Proyectos::select('id')
            ->where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();
        $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
            ->select('solicitudes.id')
            ->whereIn('gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.proyecto_id', $proyectos_ids)
            ->orderBy('solicitudes.created_at')
            ->groupBy('solicitudes.id')
            ->pluck('solicitudes.id')
            ->toArray();

        $solicitudes_ids = array();
        foreach($sol_ids as $sol_id){
            if(in_array($this->getLastEstadoCodigo($sol_id), ['APR', 'REV', 'TES-M'])){
                $solicitudes_ids[] = $sol_id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitudes_ids)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->subtotal
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        Session::flash('success', $mensaje);
        // return view('solicitudes.tesoreria.index')
        //     ->withSolicitudes($solicitudes)
        //     ->withUsuario($usuario)
        //     ->withItems($items);

        return redirect()->route('cheques.index');

        //return redirect()->action('ChequesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "ChequesController@show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function anularCheque(){
        if(isset($_GET['cheque_id'])){
            $usuario = auth()->user();
            DB::table('cheques_estados')->insert([
                'usuario_id' => $usuario->id,
                'cheque_id' => $_GET['cheque_id'],
                'cheque_estado_id' => 2,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);

            DB::table('estados_solicitud')->insert([
                'observacion' => 'CHEQUE ANULADO',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $_GET['solicitud_id'],
                'estado_id' => 40,
                "created_at" =>  Carbon::now()->subSecond(3),
                "updated_at" => Carbon::now()->subSecond(3),
            ]);

            Session::flash('success', 'Cheque Anulado!!!');
            return redirect()->action('ChequesController@index');
        }
    }

    public function anularSolicitudCheque(){
        $usuario = auth()->user();
        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD ANULADA POR TESORERIA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $_GET['solicitud_id'],
            'estado_id' => 51,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        DB::table('cheques_estados')->insert([
            'usuario_id' => $usuario->id,
            'cheque_id' => $_GET['cheque_id'],
            'cheque_estado_id' => 2,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        Session::flash('success', 'Solicitud/Cheque Anulados!!!');
        return redirect()->action('ChequesController@index');
    }

    
    public function chequesAnulados(){
        /*$query = "SELECT * FROM solicitudes s, bancos b, cuenta_bancarias cu, cheques c WHERE s.id = c.solicitud_id AND c.cuenta_id = cu.id AND s.empresa_id = ".session('empresa')." AND s.gestion_id = ".session('gestion_id')." AND b.id = cu.banco_id AND c.estado_id = 2";
        $cheques = DB::select($query);

        return View('cheques.index')->withCheques($cheques);*/
    }

    public function chequesRegistrados(){
        /*$query = "SELECT * FROM solicitudes s, bancos b, cuenta_bancarias cu, cheques c WHERE s.id = c.solicitud_id AND c.cuenta_id = cu.id AND s.empresa_id = ".session('empresa')." AND s.gestion_id = ".session('gestion_id')." AND b.id = cu.banco_id AND c.estado_id = 1";        
        $cheques = DB::select($query);
        return View('cheques.index')->withCheques($cheques);*/
    }

    public function getLastEstadoCheque($cheque_id){
        $lastEstado = ChequesEstados::join('cheques_estados', 'cheques_estados.cheque_estado_id', '=', 'estado_cheques.id')
            ->select('estado_cheques.nombre')
            ->where('cheques_estados.cheque_id', $cheque_id)
            ->orderBy('estado_cheques.id', 'DESC')
            ->first();
        return $lastEstado->nombre;
    }

    public function getLastEstado($solicitud_id){
        $usuario = auth()->user();
        if($usuario->rol_id == 8){
            $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
                ->select('estados.descripcion')
                ->where('estados_solicitud.usuario_id', $usuario->id)
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->orderBy('estados.id', 'DESC')
                ->first();
            if(!$lastEstado){
                $lastEstado = Estados::first();
                $lastEstado->descripcion = "";
            }
        }else{
            $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
                ->select('estados.descripcion')
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->orderBy('estados.id', 'DESC')
                ->first();
        }

        return $lastEstado->descripcion;
    }

    public function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null){
        $perPage = 20;
        
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }
}
