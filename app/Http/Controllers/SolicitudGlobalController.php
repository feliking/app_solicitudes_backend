<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Carbon
use Carbon\Carbon;

// Session
use Session;

// DB
use DB;

// Auth
use Illuminate\Support\Facades\Auth;

// Job
use App\Jobs\jobEnvioSolicitud;

// Modelos a Utilizar
use App\ItemsRendiciones;
use App\TiposCambios;
use App\Solicitudes;
use App\Modalidades;
use App\Documentos;
use App\Gestiones;
use App\Proyectos;
use App\Empresas;
use App\Monedas;
use App\Estados;
use App\Items;
use App\User;

class SolicitudGlobalController extends Controller
{
    /**
     * Devuelve los id de las solicitudes por sección (pestaña)
     */
    public static function seccionSolicitudGlobal ($seccion, $filters = null){
        // Todos los proyectos de todas las empresas
        $idProyectos = Proyectos::pluck('id')
            ->toArray();

        $by_pass_modalidad = false;

        switch($seccion){
            case 'autorizacion-autorizar':
                $estados = ['SOL', 'SOL-M', 'AUT-M'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'autorizacion-disponibles':
                $estados = ['AUT', 'AUT-R', 'AUT-O', 'AUT-M', 'AUT-AF', 'REV', 'REV-R', 'REV-P', 'REV-O', 'REV-M', 'APR', 'APR-R', 'APR-O', 'APR-M', 'APR-P', 'PEN', 'PEN-2', 'PEN-3', 'TES', 'TES-R', 'TES-O', 'TES-M', 'REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'SOL-V', 'APR-RO', 'FIN'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'aprobacion-aprobar':
                $estados = ['AUT', 'AUT-AF', 'REV', 'APR-M', 'APR-P'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'aprobacion-disponibles':
                $estados = ['APR', 'APR-R', 'APR-O', 'APR-M', 'APR-P', 'PEN', 'PEN-2', 'PEN-3', 'TES', 'TES-R', 'TES-O', 'TES-M', 'REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'SOL-V', 'APR-RO', 'FIN'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();
                break;
            case 'rendicion-aprobar':
                $estados = ['REN-F'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'rendicion-disponibles':
                $estados = ['REN-A','REN-R','REN-OA','REN-OB','REN-OC','SOL-V','APR-RO','FIN'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'pendientes':
                $estados = ['PEN'];

                $idUsuarios = User::pluck('id')
                    ->toArray();
                break;
            case 'pendientes2':
                $estados = ['PEN-2'];

                $idUsuarios = User::pluck('id')
                    ->toArray();
                break;
            case 'pendientes3':
                $estados = ['PEN-3'];

                $idUsuarios = User::pluck('id')
                    ->toArray();
                break;
            case 'pendientes4':
                $estados = ['PEN-4'];

                $idUsuarios = User::pluck('id')
                    ->toArray();
                break;
            case 'pendientes5':
                $estados = ['PEN-5'];

                $idUsuarios = User::pluck('id')
                    ->toArray();
                break;
            case 'usuario-en_curso':
                $estados = ['SOL', 'SOL-M', 'AUT', 'AUT-M', 'AUT-AF', 'REV', 'REV-M', 'REV-P', 'APR', 'APR-M', 'APR-P', 'TES-M', 'PEN', 'PEN-2', 'PEN-3', 'SOL-V'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();

                $by_pass_modalidad = true;
                break;
            case 'usuario-por_rendir':
                $estados = ['TES', 'REN-R', 'REN-O'];
                
                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();
                break;
            case 'usuario-observadas':
                $estados = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();

                $by_pass_modalidad = true;
                break;
            case 'usuario-rechazadas':
                $estados = ['AUT-R', 'REV-R', 'APR-R', 'TES-R'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();

                $by_pass_modalidad = true;
                break;
            case 'usuario-rendidas':
                $estados = ['REN-P', 'REN-F'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();

                $by_pass_modalidad = true;
                break;
            case 'usuario-finalizadas':
                $estados = ['FIN'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();

                $by_pass_modalidad = true;
                break;
        }

        if(!empty($estados)){
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::pluck('id')
                ->toArray();
        }

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            });

        /** BEGIN Filters */

        // Empresa
        if(isset($filters['empresa_id']) && $filters['empresa_id'] != 0){
            $idProyectos = Proyectos::where('empresa_id', $filters['empresa_id'])
                ->pluck('id')
                ->toArray();

            $idSolicitudes = $idSolicitudes
                ->whereIn('solicitudes.proyecto_id', $idProyectos);
        }

        // Proyecto
        if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0){
            $idSolicitudes = $idSolicitudes
                ->where('solicitudes.proyecto_id', $filters['proyecto_id']);
        }

        // Monto
        if(isset($filters['monto']) && $filters['monto'] != 0 && isset($filters['selectMonto'])){
            switch($filters['selectMonto']){
                case 0:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', $filters['monto']);
                    break;
                case 1:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>', $filters['monto']);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>=', $filters['monto']);
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<', $filters['monto']);
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<=', $filters['monto']);
                    break;
            }
        }

        // Fechas
        if (isset($filters['fecha_ini']) && isset($filters['fecha_fin']) && $filters['fecha_ini'] != '' && $filters['fecha_fin'] != '') {
            $fecha_ini = $filters['fecha_ini'].' 00:00:00';
            $fecha_fin = $filters['fecha_fin'].' 23:59:59';

            $idSolicitudes = $idSolicitudes->where([
                ['solicitudes.created_at', '>=', $fecha_ini],
                ['solicitudes.created_at', '<=', $fecha_fin]
            ]);
        } elseif (isset($filters['fecha_ini']) && $filters['fecha_ini'] != '') {
            $fecha_ini = $filters['fecha_ini'].' 00:00:00';

            $idSolicitudes = $idSolicitudes->where('solicitudes.created_at', '>=', $fecha_ini);
        }

        // Custom
        if(isset($filters['campo']) && $filters['campo'] != 0 && isset($filters['texto']) && $filters['texto'] != ''){
            $texto = $filters['texto'];
            switch($filters['campo']){
                case 1:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();
                    $idSolicitudes = $idSolicitudes->whereIn('solicitudes.usuario_id', $idUsuarios);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.desembolso', 'LIKE', "%$texto%");
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.referencia', 'LIKE', "%$texto%");
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.observacion', 'LIKE', "%$texto%");
                    break;
                case 5:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.numero', $texto);
                    break;
                case 6:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();

                    $idSolicitudes = $idSolicitudes
                        ->orWhereIn('solicitudes.usuario_id', $idUsuarios)
                        ->orWhere('solicitudes.desembolso', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.referencia', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.observacion', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.numero', $texto);
                    break;
            }
        }

        // Modalidad
        if (!$by_pass_modalidad)
        if(isset($filters['chk_fr_cc'])){
            $idSolicitudes = $idSolicitudes->whereIn('solicitudes.modalidad_id', [3, 4]);
        }else{
            $idSolicitudes = $idSolicitudes->whereIn('solicitudes.modalidad_id', [1, 2]);
        }

        // Gestión
        if (isset($filters['gestion_id']) && $filters['gestion_id'] != 0) {
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $filters['gestion_id']);
        } else if (!isset($filters['gestion_id'])){
            $gestion = Gestiones::orderBy('id', 'DESC')->first();
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $gestion->id);
        }

        /** END Filters */

        //dd($idSolicitudes->toSql());

        $idSolicitudes = $idSolicitudes->pluck('solicitudes.id')->toArray();

        if($seccion == 'aprobacion-aprobar'){
            $idSolicitudesAprobadas = Solicitudes::join('aprobador', 'aprobador.solicitud_id', '=', 'solicitudes.id')
                ->join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
                ->select('solicitudes.*')
                ->whereIn('solicitudes.usuario_id', $idUsuarios)
                ->whereIn('estados_solicitud.estado_id', $idEstados)
                ->where('aprobador.aprobador_id', Auth::user()->id)
                ->whereIn('estados_solicitud.id', function($query){
                    $query->selectRaw('MAX(estados_solicitud.id)')
                        ->from('estados_solicitud')
                        ->groupBy('estados_solicitud.solicitud_id')
                        ->get();
                })
                ->pluck('solicitudes.id')
                ->toArray();

            $idSolicitudes = array_diff($idSolicitudes, $idSolicitudesAprobadas);
        }

        return $idSolicitudes;
    }

    public static function seccionFrSolicitudGlobal ($seccion, $filters = null)
    {
        // Todos los proyectos de todas las empresas
        $idProyectos = Proyectos::pluck('id')
            ->toArray();

        $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P'];

        // $idUsuarios = User::whereIn('id', [107, 108])
        //     ->pluck('id')
        //     ->toArray();

        // Proyectos
        $proyectos_revisor = [];
        $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id');

        // dd($proyectos_revisor);

        switch ($seccion)
        {
            case 'sujeto_rendicion':
                $modalidad = 1;
            break;
            case 'gasto_directo':
                $modalidad = 2;
            break;
            case 'fondo_rotativo':
                $modalidad = 3;
            break;
            case 'reposicion':
                $modalidad = 4;
            break;
        }

        if(!empty($estados)){
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::pluck('id')
                ->toArray();
        }

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            ->where('modalidad_id', $modalidad)
            // ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('solicitudes.proyecto_id', $proyectos_revisor)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            });

        /** BEGIN Filters */

        // Empresa
        if(isset($filters['empresa_id']) && $filters['empresa_id'] != 0){
            $idProyectos = Proyectos::where('empresa_id', $filters['empresa_id'])
                ->pluck('id')
                ->toArray();

            $idSolicitudes = $idSolicitudes
                ->whereIn('solicitudes.proyecto_id', $idProyectos);
        }

        // Proyecto
        if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0){
            $idSolicitudes = $idSolicitudes
                ->where('solicitudes.proyecto_id', $filters['proyecto_id']);
        }

        // Monto
        if(isset($filters['monto']) && $filters['monto'] != 0 && isset($filters['selectMonto'])){
            switch($filters['selectMonto']){
                case 0:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', $filters['monto']);
                    break;
                case 1:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>', $filters['monto']);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>=', $filters['monto']);
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<', $filters['monto']);
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<=', $filters['monto']);
                    break;
            }
        }

        // Fechas
        if (isset($filters['fecha_ini']) && isset($filters['fecha_fin']) && $filters['fecha_ini'] != '' && $filters['fecha_fin'] != '') {
            $fecha_ini = $filters['fecha_ini'].' 00:00:00';
            $fecha_fin = $filters['fecha_fin'].' 23:59:59';

            $idSolicitudes = $idSolicitudes->where([
                ['solicitudes.created_at', '>=', $fecha_ini],
                ['solicitudes.created_at', '<=', $fecha_fin]
            ]);
        } elseif (isset($filters['fecha_ini']) && $filters['fecha_ini'] != '') {
            $fecha_ini = $filters['fecha_ini'].' 00:00:00';

            $idSolicitudes = $idSolicitudes->where('solicitudes.created_at', '>=', $fecha_ini);
        }

        // Custom
        if(isset($filters['campo']) && $filters['campo'] != 0 && isset($filters['texto']) && $filters['texto'] != ''){
            $texto = $filters['texto'];
            switch($filters['campo']){
                case 1:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();
                    $idSolicitudes = $idSolicitudes->whereIn('solicitudes.usuario_id', $idUsuarios);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.desembolso', 'LIKE', "%$texto%");
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.referencia', 'LIKE', "%$texto%");
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.observacion', 'LIKE', "%$texto%");
                    break;
                case 5:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.numero', $texto);
                    break;
                case 6:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();

                    $idSolicitudes = $idSolicitudes
                        ->orWhereIn('solicitudes.usuario_id', $idUsuarios)
                        ->orWhere('solicitudes.desembolso', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.referencia', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.observacion', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.numero', $texto);
                    break;
            }
        }

        // Gestión
        if (isset($filters['gestion_id']) && $filters['gestion_id'] != 0) {
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $filters['gestion_id']);
        } else if (!isset($filters['gestion_id'])){
            $gestion = Gestiones::orderBy('id', 'DESC')->first();
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $gestion->id);
        }

        /** END Filters */

        $idSolicitudes = $idSolicitudes->pluck('solicitudes.id')->toArray();

        return $idSolicitudes;
    }

    /**
     * Monto total por moneda
     */
    public static function montoTotal ($solicitudes, $monedas) {
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = SolicitudGlobalController::getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = SolicitudGlobalController::getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        return $total_monedas;
    }

    /**
     * Aplicar filtros
     */
    public static function aplicarFiltros (Request $request) {
        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        return $filtros;
    }

    /**
     * Último estado
     */
    public static function getLastEstado($solicitud_id){
        $usuario = auth()->user();
        if($usuario->rol_id == 8){
            $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
                ->select('estados.descripcion')
                ->where('estados_solicitud.usuario_id', $usuario->id)
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->orderBy('estados.id', 'DESC')
                ->first();
            if(!$lastEstado){
                $lastEstado = Estados::first();
                $lastEstado->descripcion = "";
            }
        }else{
            $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
                ->select('estados.descripcion')
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->orderBy('estados.id', 'DESC')
                ->first();
        }
        
        return $lastEstado->descripcion;
    }

    /**
     * Código del último estado
     */
    public static function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    /**
     * Paginación
     */
    function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null){
        $perPage = 20;
        
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }

    /**
     * Remover acentos
     */
    function remover_acentos($str){
      $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
      $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
      return str_replace($a, $b, $str);
    }

    // Solicitudes a ser Autorizadas
    public function autorizar (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('autorizacion-autorizar', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.autorizar')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES A SER AUTORIZADAS')
            ->withGestiones($gestiones);
    }

    // Solicitudes Autorizadas disponibles
    public function autorizar_disponibles (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('autorizacion-disponibles', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.autorizar_disponibles')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES DISPONIBLES PARA AUTORIZADOR')
            ->withGestiones($gestiones);
    }

    // Solicitudes a ser Aprobadas
    public function aprobar (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('aprobacion-aprobar', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.aprobar')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES A SER APROBADAS')
            ->withGestiones($gestiones);
    }

    // Solicitudes Autorizadas disponibles
    public function aprobar_disponibles (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('aprobacion-disponibles', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.aprobar_disponibles')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES DISPONIBLES PARA APROBADOR PARCIAL')
            ->withGestiones($gestiones);
    }

    // Rendiciones a ser Aprobadas
    public function aprobar_rendiciones (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('rendicion-aprobar', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.aprobar_rendiciones')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('RENDICIONES A SER APROBADAS')
            ->withGestiones($gestiones);
    }

    // Rendiciones Aprobadas disponibles
    public function aprobar_rendiciones_disponibles (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('rendicion-disponibles', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.aprobar_rendiciones_disponibles')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('RENDICIONES DISPONIBLES')
            ->withGestiones($gestiones);
    }

    // Solicitudes Pendientes
    public function pendientes (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('pendientes', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.pendientes')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES')
            ->withGestiones($gestiones);
    }

    public function pendientes2 (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('pendientes2', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.pendientes2')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES 2')
            ->withGestiones($gestiones);
    }

    public function pendientes3 (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('pendientes3', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.pendientes3')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES 3')
            ->withGestiones($gestiones);
    }

    public function pendientes4 (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('pendientes4', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.pendientes4')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES 4')
            ->withGestiones($gestiones);
    }

    public function pendientes5 (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('pendientes5', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.pendientes5')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES 5')
            ->withGestiones($gestiones);
    }

    // Revisor
    public function revisorSujetoRendicion (Request $request) 
    {
        $idSolicitudes = $this->seccionFrSolicitudGlobal('sujeto_rendicion', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.revisor.sujeto_rendicion')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES A SER REVISADAS')
            ->withGestiones($gestiones);
    }

    public function revisorGastoDirecto (Request $request) 
    {
        $idSolicitudes = $this->seccionFrSolicitudGlobal('gasto_directo', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.revisor.gasto_directo')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES A SER REVISADAS')
            ->withGestiones($gestiones);
    }

    public function revisorFondoRotativo (Request $request) 
    {
        $idSolicitudes = $this->seccionFrSolicitudGlobal('fondo_rotativo', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.revisor.fondo_rotativo')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES A SER REVISADAS')
            ->withGestiones($gestiones);
    }

    public function revisorReposicion (Request $request) 
    {
        $idSolicitudes = $this->seccionFrSolicitudGlobal('reposicion', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.revisor.reposicion')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES A SER REVISADAS')
            ->withGestiones($gestiones);
    }

    // Get Proyectos
    public function getProyectos ($empresa_id) {
        $usuario = auth()->user();

        $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
            $query->where('user_id', $usuario->id);
        })
            ->where('empresa_id', $empresa_id)
            ->get();

        foreach($proyectos as $proyecto){
            switch($proyecto->nivel){
                case 2:
                    $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
                case 3:
                    $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
            }
        }

        return $proyectos;
    }

    // Crear Solicitud
    public function create () {
        $usuario = auth()->user();
        $modalidades = Modalidades::where('oculto', false)->get();
        $monedas = Monedas::all();

        $tipo_cambio = TiposCambios::moneda(2)->get()->last()->toArray();
        $tipo_cambio_euro = TiposCambios::moneda(3)->get()->last()->toArray();

        $gestiones = Gestiones::orderBy('id', 'DESC')->get();

        $empresa_ids = Proyectos::whereHas('usuarios', function ($query) use($usuario){
            $query->where('user_id', $usuario->id);
        })
            ->select('empresa_id')
            ->distinct()
            ->pluck('empresa_id');
        
        $empresas = Empresas::whereIn('id', $empresa_ids)->get();

        return view('solicitudes.global.crear')
            ->withCambioEuro($tipo_cambio_euro)
            ->withModalidades($modalidades)
            ->withCambio($tipo_cambio)
            ->withMonedas($monedas)
            ->withUser($usuario)
            ->withGestiones($gestiones)
            ->withEmpresas($empresas)
            ->withDate(Carbon::now()->format('Y-m-d'));
    }

    // Grabar solicitud
    public function store (Request $request) {
        $usuario = auth()->user();
        
        //Total solicitud
        $total = 0;
        if(count($request->subtotal) > 0){
            $sub_totales = $request->subtotal;
            foreach($sub_totales as $sub_total){
                $total += $sub_total;
            }
        }

        //Secuencial Solicitud Empresa Gestion
        $secuencial = 0;
        $proyectos_ids = Proyectos::where('empresa_id', $request->empresa_id)
            ->pluck('id')->toArray();

        $sol_emp_ges = Solicitudes::whereIn('proyecto_id', $proyectos_ids)
            ->where('gestion_id', $request->gestion_id)
            ->max('numero');

        $secuencial = (count($sol_emp_ges) == 0)? 1:($sol_emp_ges+1);

        try{
            $solicitud = new Solicitudes;
            $solicitud->numero      = $secuencial;
            $solicitud->desembolso  = mb_strtoupper($request->desembolso);
            $solicitud->referencia  = mb_strtoupper($request->referencia);
            $solicitud->observacion = mb_strtoupper($request->observacion);
            
            if($request->fecha_lim != ''){
                $solicitud->fecha_limite = $request->fecha_lim;
            }
            
            $solicitud->total       = array_sum($request->subtotal);

            $solicitud->modalidad_id  = $request->modalidad;

            if($solicitud->modalidad_id == 3){
                $solicitud->frtipo_id  = $request->empresa_fr;
            }

            $solicitud->moneda_id     = $request->moneda;
            $solicitud->usuario_id    = $usuario->id;
            $solicitud->proyecto_id = $request->proyecto;
            $solicitud->gestion_id  = $request->gestion_id;

            if($usuario->empleado_id != null){
                $solicitud->empleado_id = $usuario->empleado_id;
            }else{
                $solicitud->empleado_id = 0;
            }
            
            $solicitud->cambio_id = TiposCambios::moneda($request->moneda)->get()->last()->id;
            
            $solicitud->save();

            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD CREADA',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 10,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);

            switch($usuario->rol_id){
                case 4:
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD AUTORIZADA (REVISOR)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 20,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                    break;
                case 5:
                    DB::table('estados_solicitud')->insert([
                        [
                            'observacion' => 'SOLICITUD AUTORIZADA (APROBADOR)',
                            'usuario_id' => $usuario->id,
                            'solicitud_id' => $solicitud->id,
                            'estado_id' => 20,
                            "created_at" =>  Carbon::now(),
                            "updated_at" => Carbon::now()
                        ],
                        [
                            'observacion' => 'SOLICITUD REVISADA (APROBADOR)',
                            'usuario_id' => $usuario->id,
                            'solicitud_id' => $solicitud->id,
                            'estado_id' => 30,
                            "created_at" =>  Carbon::now(),
                            "updated_at" => Carbon::now()
                        ]
                    ]);
                    break;
                case 6:
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD AUTORIZADA (TESORERIA)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 20,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD REVISADA (TESORERIA)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 30,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD REVISADA (TESORERIA)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 40,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                    break;
            }

            //Datos Items
            $cantidades = $request->cantidad;
            $unidades = $request->unidad;
            $detalles = $request->detalle;
            $punitarios = $request->punitario;
            $tipo_compra = $request->tipo_compra;
            $cant_items = count($sub_totales);
            
            for($i = 0; $i < $cant_items; $i++){
                $item = new Items;
                $item->detalle          = mb_strtoupper($detalles[$i]);
                $item->cantidad         = $cantidades[$i];
                $item->tipo_id          = $tipo_compra[$i];
                $item->costo            = $punitarios[$i];
                $item->numero_partida   = 0;
                $item->nombre_partida   = 0;
                $item->unidad_id        = $unidades[$i];
                $item->solicitud_id     = $solicitud->id;
                $item->save();

                if($request->modalidad == 3 || $request->modalidad == 4){
                    $nroFacturas = $request->nrofactura;
                    $fechaFacturas = $request->fechafactura;
                    $proyectos = $request->itemproyecto;
                    $proveedores = $request->itemproveedor;
                    $tipos = $request->itemtipo;

                    // Datos Items rendidos
                    $itemRen = new ItemsRendiciones();
                    $itemRen->cantidad          = $cantidades[$i];
                    $itemRen->costo             = $punitarios[$i];
                    $itemRen->fecha_factura     = $fechaFacturas[$i];
                    $itemRen->num_factura       = $nroFacturas[$i];
                    $itemRen->proyecto          = mb_strtoupper($proyectos[$i]);
                    $itemRen->proveedor         = mb_strtoupper($proveedores[$i]);
                    $itemRen->tipo              = $tipos[$i];
                    $itemRen->detalle           = mb_strtoupper($detalles[$i]);
                    $itemRen->direccion         = '';
                    $itemRen->item_id           = $item->id;
                    $itemRen->unidad_id         = $unidades[$i];
                    $itemRen->solicitud_id      = $solicitud->id;
                    $itemRen->tipo_id           = $tipo_compra[$i];
                    $itemRen->save();
                }
            }

            $empresa = Empresas::find($request->empresa_id);
            $gestion = Gestiones::find($request->gestion_id);

            //Upload Files
            $path = public_path().'/empresas/'.$empresa->slug."/".$gestion->gestion."/solicitud/";
            
            for($i = 1; $i <= 30; $i++){
                $doc_name = 'doc'.$i;
                if(isset($_FILES[$doc_name]['name']) && $_FILES[$doc_name]['name']!=''){
                    $str_name = trim($_FILES[$doc_name]['name']);
                    $file_type = $_FILES[$doc_name]['type'];
                    $str_name = $this->remover_acentos($str_name);
                    $str_name = strtolower($str_name);
                    $str_name = str_replace('-', '', $str_name);
                    $str_name = str_replace(' ', '_', $str_name);
                    $str_name = str_replace('__', '_', $str_name);
                    $str_name = str_replace('___ ', '_', $str_name);
                    $str_name = str_replace('____', '_', $str_name);
                    $db_filename = $solicitud->id."_".time()."_".$str_name;
                    move_uploaded_file($_FILES[$doc_name]['tmp_name'], $path.$db_filename);
                    $documento = new Documentos;
                    $documento->direccion = $db_filename;
                    $documento->observaciones = '';
                    $documento->solicitud_id = $solicitud->id;

                    $documento->save();
                }
            }

            $solicitudes = Solicitudes::whereIn('proyecto_id', $proyectos_ids)
                ->where('gestion_id',$request->gestion_id)
                ->where('usuario_id', $usuario->id)
                ->orderBy('id', 'DESC')
                ->paginate(session('paginacion'));

            $arrayItems = array();
            foreach($solicitudes as $sol){
                $items = Items::where("solicitud_id", $sol->id)->get();
                foreach($items as $item){
                    $arrayItems[$sol->id][]=[
                        'unidad'    =>  $item->unidad_id,
                        'detalle'   =>  $item->detalle,
                        'cantidad'  =>  $item->cantidad,
                        'costo'     =>  $item->costo,
                        'subtotal'  =>  $item->subtotal
                    ];
                }
            }

            $emailErr = "Valid";
            if(isset($request->correo_envio) && trim($request->correo_envio) != ''){
                $correo = mb_strtolower(trim($request->correo_envio));
                if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid";
                }
                if($emailErr == "Valid"){
                    //enviar correo
                    dispatch(new jobEnvioSolicitud($solicitud->numero, $empresa->nombre, $solicitud->proyecto->nombre, $solicitud->referencia, $correo));
                }
            }
        }catch(Exception $e){
            echo "Excepción :".$e->getMessage();
        }

        foreach($solicitudes as $sol){
            $sol->estado = $this->getLastEstado($sol->id);
            $sol->estadoCodigo = $this->getLastEstadoCodigo($sol->id);

            if($sol->proyecto->padre_id == null){
                $sol->nom_completo_proyecto = $sol->proyecto->nombre;
            }elseif($sol->proyecto->padre->padre_id == null){
                $sol->nom_completo_proyecto = $sol->proyecto->padre->nombre." &#10148; ".$sol->proyecto->nombre;
            }else{
                $sol->nom_completo_proyecto = $sol->proyecto->padre->padre->nombre." &#10148; ".$sol->proyecto->padre->nombre." &#10148; ".$sol->proyecto->nombre;
            }
        }

        Session::flash('success', 'Solicitud #'.$solicitud->numero.' creada!!!');
        
        return redirect()->route('global.en_curso');
    }

    // Solicitudes en curso
    public function en_curso (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('usuario-en_curso', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.en_curso')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES EN CURSO')
            ->withGestiones($gestiones);
    }

    // Solicitudes por rendir
    public function por_rendir (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('usuario-por_rendir', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.por_rendir')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES POR RENDIR')
            ->withGestiones($gestiones);
    }

    // Solicitudes observadas
    public function observadas (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('usuario-observadas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.observadas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES OBSERVADAS')
            ->withGestiones($gestiones);
    }

    // Solicitudes rechazadas
    public function rechazadas (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('usuario-rechazadas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.rechazadas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES RECHAZADAS')
            ->withGestiones($gestiones);
    }

    // Solicitudes rendidas
    public function rendidas (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('usuario-rendidas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.rendidas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES RENDIDAS')
            ->withGestiones($gestiones);
    }

    // Solicitudes finalizadas
    public function finalizadas (Request $request) {
        $idSolicitudes = $this->seccionSolicitudGlobal('usuario-finalizadas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        
        $total_monedas = $this->montoTotal($solicitudes, $monedas);

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.global.finalizadas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("GLOBAL")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES FINALIZADAS')
            ->withGestiones($gestiones);
    }
}
