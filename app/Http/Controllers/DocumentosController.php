<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Documentos;

use Session;
use Response;

class DocumentosController extends Controller
{
    public function getDocumento($id){
        $documento = Documentos::find($id);
//        echo storage_path('app/public/'.$documento->ubicacion);
//        $ubicacion = Storage::disk('public')->url('public/'.$documento->ubicacion);

        $ubicacion = storage_path('app/public/empresas/'.session('empresa_nombre').'/'.session('gestion').'/solicitud/'.$documento->ubicacion);
//        echo $ubicacion;


        return response()->download($ubicacion,$documento->nombre);

        //echo $documento;
    }

    public function eliminarDoc($id){
        $documento = Documentos::find($id);

        $path = public_path()."/empresas/".$documento->solicitud->proyecto->empresa->slug."/".session('gestion')."/solicitud/".$documento->direccion;
        unlink($path);

        $documento->delete();

        return Response::json("DOCUMENTO ELIMINADO");
    }
}
