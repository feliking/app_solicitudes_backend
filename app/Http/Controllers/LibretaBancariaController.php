<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\TipoMovimientoBancario;
use App\MovimientosBancarios;
use App\ItemsRendiciones;
use App\ItemsController;
use App\UsuarioProyecto;
use App\CuentaBancaria;
use App\ChequesEstados;
use App\TiposCambios;
use App\Devoluciones;
use App\Solicitudes;
use App\Modalidades;
use App\ItemsTipos;
use App\Documentos;
use App\Proyectos;
use App\Gestiones;
use App\Empresas;
use App\Unidades;
use App\Monedas;
use App\Estados;
use App\Cheques;
use App\Bancos;
use App\Items;
use App\User;

use Session;
use Mail;
use DB;

class LibretaBancariaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bancos_ids = CuentaBancaria::select('banco_id')
            ->where('empresa_id', Session('empresa'))
            ->groupBy('banco_id')
            ->pluck('banco_id')
            ->toArray();

        $bancos = Bancos::whereIn('id', $bancos_ids)->get();

        $array_bancos = array();
        foreach($bancos as $banco){
            $cuentas = CuentaBancaria::where('empresa_id', Session('empresa'))
                ->where('banco_id', $banco->id)
                ->get();

            $array_bancos[$banco->id] = [
                'nombre' => $banco->nombre,
                'cuentas' => []
            ];
            foreach($cuentas as $cuenta){
                $array_cuentas = array();
                $cheques = Cheques::where('cuenta_id', $cuenta->id)
                    ->get();

                $monto_ingreso = 0;
                $ingresos = MovimientosBancarios::where('cuenta_des_id', $cuenta->id)
                    ->where('tipo_movimiento_id', 2)
                    ->get();
                foreach($ingresos as $ingreso){
                    $monto_ingreso += $ingreso->monto;
                }

                $array_cheques = array();
                $cantidad_cheques = 0;
                if(count($cheques)>0){
                    foreach($cheques as $cheque){
                        if($this->getLastEstadoCheque($cheque->id) == 'EMITIDO'){
                            $cantidad_cheques ++;
                        }
                    }
                }

                $monto_egreso = 0;
                $egresos = MovimientosBancarios::where('cuenta_ori_id', $cuenta->id)
                    ->where('tipo_movimiento_id', 3)
                    ->get();
                foreach($egresos as $egreso){
                    $monto_egreso += $egreso->monto;
                }

                $array_chequera[$cuenta->id] = $array_cheques;
                $array_cuentas = [
                    'numero' => $cuenta->numero,
                    'monto_inicial' => $cuenta->monto_inicial,
                    'moneda' => $cuenta->moneda->nombre,
                    'monto_ingreso' => $monto_ingreso,
                    'monto_egreso' => $monto_egreso,
                    'cantidad_cheques' => $cantidad_cheques
                ];
                array_push($array_bancos[$banco->id]['cuentas'], $array_cuentas);
            }
        }
        return view('solicitudes.tesoreria.libreta.index')
            ->withBancos($array_bancos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getLastEstadoCheque($cheque_id){
        $lastEstado = ChequesEstados::join('cheques_estados', 'cheques_estados.cheque_estado_id', '=', 'estado_cheques.id')
            ->select('estado_cheques.nombre')
            ->where('cheques_estados.cheque_id', $cheque_id)
            ->orderBy('cheques_estados.id', 'DESC')
            ->first();
        return $lastEstado->nombre;
    }

    public function transferencia(){
        $cuentas_origen = CuentaBancaria::where('empresa_id', session('empresa'))->get();
        $cuentas_destino = CuentaBancaria::where('empresa_id', '!=', session('empresa'))->get();
        $otra_cuenta = new CuentaBancaria();
        $otra_cuenta->id = 0;
        $otra_cuenta->numero = 0;
        $otra_cuenta->monto_inicial = 0;
        $otra_cuenta->saldo = 0;
        $otra_cuenta->moneda_id = 0;
        $otra_cuenta->empresa_id = 0;
        $otra_cuenta->banco_id = 0;

        $empresas = Empresas::orderBy('nombre')
            ->pluck('nombre', 'id');

        $cuentas_destino->prepend($otra_cuenta);

        $tipo_movimiento = TipoMovimientoBancario::where('descripcion', 'LIKE', 'TRANSFERENCIA')
            ->pluck('id')
            ->first();
        //dd($tipo_movimiento);
        return view('solicitudes.tesoreria.libreta.transferencia')
            ->withMovimiento($tipo_movimiento)
            ->withDestinos($cuentas_destino)
            ->withOrigenes($cuentas_origen)
            ->withEmpresas($empresas);
    }

    public function transferenciaStore(Request $request){
        switch($request->movimiento_id){
            case 1:
                //transferencia
                $movimiento = new MovimientosBancarios;
                $movimiento->monto = $request->monto;
                $movimiento->cuenta_ori_id = null;
                $movimiento->cuenta_des_id = null;

                if($request->origen_cuenta != 0 && $request->destino_cuenta != 0){
                    $movimiento->cuenta_ori_id = $request->origen_cuenta;
                    $movimiento->cuenta_des_id = $request->destino_cuenta;
                    //Egreso
                    $egreso = new MovimientosBancarios;
                    $egreso->monto = $request->monto;
                    $egreso->cuenta_ori_id = $request->origen_cuenta;
                    $egreso->cuenta_des_id = null;
                    $egreso->tipo_movimiento_id = 3;
                    $egreso->created_at = Carbon::now();
                    $egreso->updated_at = Carbon::now();
                    $egreso->save();
                    //Ingreso
                    $ingreso = new MovimientosBancarios;
                    $ingreso->monto = $request->monto;
                    $ingreso->cuenta_ori_id = null;
                    $ingreso->cuenta_des_id = $request->destino_cuenta;
                    $ingreso->tipo_movimiento_id = 2;
                    $ingreso->created_at = Carbon::now();
                    $ingreso->updated_at = Carbon::now();
                    $ingreso->save();
                }elseif($request->origen_cuenta != 0){
                    //Egreso
                    $movimiento->cuenta_ori_id = $request->origen_cuenta;
                    $egreso = new MovimientosBancarios;
                    $egreso->monto = $request->monto;
                    $egreso->cuenta_ori_id = $request->origen_cuenta;
                    $egreso->cuenta_des_id = null;
                    $egreso->tipo_movimiento_id = 3;
                    $egreso->created_at = Carbon::now();
                    $egreso->updated_at = Carbon::now();
                    $egreso->save();
                }else{
                    $movimiento->cuenta_des_id = $request->destino_cuenta;
                    //Ingreso
                    $ingreso = new MovimientosBancarios;
                    $ingreso->monto = $request->monto;
                    $ingreso->cuenta_ori_id = null;
                    $ingreso->cuenta_des_id = $request->destino_cuenta;
                    $ingreso->tipo_movimiento_id = 2;
                    $ingreso->created_at = Carbon::now();
                    $ingreso->updated_at = Carbon::now();
                    $ingreso->save();
                }
                $movimiento->tipo_movimiento_id = $request->movimiento_id;
                $movimiento->created_at = Carbon::now();
                $movimiento->updated_at = Carbon::now();
                $movimiento->save();
                break;
            case 2:
                //ingreso
                break;
            case 3:
                //egreso

                break;
        }
        Session::flash('success', 'Transferencia Realizado Exitosamente!!!');
        return redirect()->route('transferencia');
    }

    public function ingreso(){
        $tipo_movimiento = TipoMovimientoBancario::where('descripcion', 'LIKE', 'INGRESO')
            ->pluck('id')
            ->first();
        $empresas = Empresas::orderBy('nombre')
            ->pluck('nombre', 'id');

        return View('solicitudes.tesoreria.libreta.ingreso')
            ->withMovimiento($tipo_movimiento)
            ->withEmpresas($empresas);
    }

    public function ingresoStore(Request $request){
        //Ingreso
        $ingreso = new MovimientosBancarios;
        $ingreso->monto = $request->monto;
        $ingreso->cuenta_ori_id = null;
        $ingreso->cuenta_des_id = $request->destino_cuenta;
        $ingreso->tipo_movimiento_id = 2;
        $ingreso->created_at = Carbon::now();
        $ingreso->updated_at = Carbon::now();
        $ingreso->save();

        Session::flash('success', 'Ingreso Realizado Exitosamente!!!');
        return redirect()->route('movimiento.ingreso');
    }

    public function egreso(){
        $tipo_movimiento = TipoMovimientoBancario::where('descripcion', 'LIKE', 'INGRESO')
            ->pluck('id')
            ->first();
        $empresas = Empresas::orderBy('nombre')
            ->pluck('nombre', 'id');

        return View('solicitudes.tesoreria.libreta.egreso')
            ->withMovimiento($tipo_movimiento)
            ->withEmpresas($empresas);
    }

    public function egresoStore(Request $request){
        //Egreso
        $egreso = new MovimientosBancarios;
        $egreso->monto = $request->monto;
        $egreso->cuenta_ori_id = $request->origen_cuenta;
        $egreso->cuenta_des_id = null;
        $egreso->tipo_movimiento_id = 3;
        $egreso->created_at = Carbon::now();
        $egreso->updated_at = Carbon::now();
        $egreso->save();

        Session::flash('success', 'Egreso Realizado Exitosamente!!!');
        return redirect()->route('movimiento.egreso');
    }
}
