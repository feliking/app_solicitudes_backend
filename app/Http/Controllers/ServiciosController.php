<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Empresas;

class ServiciosController extends Controller{
    public function getEmpresas(){
        $empresas = Empresas::select('id', 'nombre', 'razon', 'nit')->get();
        foreach($empresas as $empresa){
            $empresa->proyecto;
        }

        return response()->json(['empresas' => $empresas]);
    }
}
