<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AprorendicionEmpresa;
use App\FondoRotativoEmpresa;
use App\AutorizadorUsuario;
use App\NivelesAprobacion;
use App\ItemsRendiciones;
use App\FondosRotativos;
use App\ItemsController;
use App\UsuarioProyecto;
use App\NivelAprobador;
use App\CuentaBancaria;
use App\TiposCambios;
use App\Solicitudes;
use App\Modalidades;
use App\ItemsTipos;
use App\Documentos;
use App\Gestiones;
use App\Proyectos;
use App\Empresas;
use App\Unidades;
use App\ItemsAf;
use App\Cheques;
use App\Monedas;
use App\Estados;
use App\Bancos;
use App\Items;
use App\User;

use App\EstadoSolicitudes;

use App\Http\Controllers\SolicitudController;

use Response;
use DB;
use App\DocumentosRendicion;
use Session;

class JsonController extends Controller
{
    public function getCambio(Request $request){
        // Verifica la existencia de la variable 'monedaId'
        if($request->monedaId != null){
            $tipo_cambio = TiposCambios::moneda($request->monedaId)->get()->last();
        }else{
            $tipo_cambio = TiposCambios::moneda(2)->get()->last();
        }

        // Verifica que exista el registro en la tabla
        if($tipo_cambio != null){
            return Response::json($tipo_cambio->toArray());
        }else{
            return Response::json(null);
        }
    }

    public function getProyectosEmpresa($empresa_id = 0){
        $usuario = auth()->user();
        if($empresa_id != 0){
            if($usuario->rol_id == 2 && $usuario->apr_parcial == false && $usuario->id != 32){
                $proyectos =  Proyectos::whereHas('usuarios', function ($query) use($usuario){
                    $query->where('user_id', $usuario->id);
                    $query->whereIn('nivel', [2,3]);
                })
                    ->where('empresa_id', session('empresa'))
                    ->get();
            }else{
                //$proyectos = Proyectos::has('hijo', '>', 0)
                $proyectos = Proyectos::whereIn('nivel', [2,3])
                    ->where('empresa_id', $empresa_id)
                    ->get();
            }
        }else{
            //$proyectos = Proyectos::has('hijo', '>', 0)->get();
            $proyectos = Proyectos::whereIn('nivel', [2,3])
                ->get();
        }

        foreach($proyectos as $proyecto){
            switch($proyecto->nivel){
                case 2:
                    $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
                case 3:
                    $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
            }
        }

        $proyectos = $proyectos->sortBy(
            function($proy){
                return $proy->nombre;
            }
        );

        return Response::json($proyectos);
    }

    public function getUsuarioProyectos($usuario_id){
        $query = "SELECT proyecto_id FROM proyectos_users WHERE user_id = ".$usuario_id." AND empresa_id = ".session('empresa');
        $proyectos_id = DB::select($query);
        return Response::json($proyectos_id);
    }

    public function getDocumentosSolicitud($solicitud_id){
        $documentos = Documentos::where('solicitud_id', $solicitud_id)->get();

        foreach($documentos as $documento){
            $documento->empresa_slug = $documento->solicitud->proyecto->empresa->slug;
            $documento->gestion = $documento->solicitud->gestion->gestion;
        }
        return Response::json($documentos);
    }

    public function getDocumentosRendicion($solicitud_id){
        $documentos = DocumentosRendicion::where('solicitud_id', $solicitud_id)->get();

        foreach($documentos as $documento){
            $documento->empresa_slug = $documento->solicitud->proyecto->empresa->slug;
            $documento->gestion = $documento->solicitud->gestion->gestion;
        }
        return Response::json($documentos);
    }

    public function getItemsSolicitud($solicitud_id){
    $solicitud = Solicitudes::find($solicitud_id);
    if($solicitud->modalidad->id == 3){
        $items = ItemsRendiciones::where("solicitud_id", $solicitud_id)->get();
        foreach($items as $item){
            $item->num_factura = $item->num_factura;
            $item->fecha_factura = $item->fecha_factura;
            $item->unidad_descripcion = $item->unidad->descripcion;
            $item->tipocompra = $item->tipo_compra->descripcion;
            $item->subtotal = $item->cantidad * $item->costo;
            $item->proyecto = ($item->proyecto != null)? $item->proyecto:"";
            $item->proveedor = ($item->proveedor != null)? $item->proveedor:"";
        }
    }else{
        $items = Items::where("solicitud_id", $solicitud_id)->get();
        foreach($items as $item){
            $item->unidad_descripcion = $item->unidad->descripcion;
            $item->tipocompra = $item->tipo_compra->descripcion;
            $item->subtotal = $item->cantidad * $item->costo;
        }
    }
    return Response::json($items);
}

    public function getCantidadItemsAntiguosSolicitud($solicitud_id){
        $items = ItemsController::where("solicitud_id", $solicitud_id)->get();
        return Response::json(count($items));
    }

    public function getItemsAntiguosSolicitud($solicitud_id){
        $items = ItemsController::where("solicitud_id", $solicitud_id)->get();
        foreach($items as $item){
            $item->unidad_descripcion = $item->unidad->descripcion;
            $item->tipocompra = $item->tipo_compra->descripcion;
            $item->subtotal = $item->cantidad * $item->costo;
        }
        return Response::json($items);
    }

    public function getDatosSolicitud($solicitud_id){
        $solicitud = Solicitudes::find($solicitud_id);
        $array_datos_solicitud = array();
        if(sizeof($solicitud) > 0){
            $estado = Estados::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'estados.id')
                ->join('users', 'users.id', 'estados_solicitud.usuario_id')
                ->select('estados_solicitud.solicitud_id', 'estados_solicitud.usuario_id', 'estados_solicitud.created_at', 'users.nombre AS autorizador')
                ->where('solicitud_id', $solicitud_id)
                ->where('estado_id', 2)
                ->orderBy('estados_solicitud.created_at', 'DESC')
                ->first();

            $nombre = $solicitud->proyecto->nombre;
            switch($solicitud->proyecto->nivel){
                case 2:
                    $nombre = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                    break;
                case 3:
                    $nombre = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                    break;
            }

            if($solicitud->modalidad_id == 3){
                $modalidad = $solicitud->modalidad->nombre." (".\App\FondosRotativos::find(intval($solicitud->frtipo_id))->nombre.")";
            }else{
                $modalidad = $solicitud->modalidad->nombre;
            }

            $array_datos_solicitud = [
                "id" => $solicitud->id,
                "numero" => $solicitud->numero,
                "desembolso" => $solicitud->desembolso,
                "desembolso_antiguo" => $solicitud->desembolso_antiguo,
                "referencia" => $solicitud->referencia,
                "observacion" => $solicitud->observacion,
                "moneda" => $solicitud->moneda->nombre,
                "tipo_cambio" => $solicitud->tipo_cambio->cambio,
                "modalidad" => $modalidad,
                "modalidad_id" => $solicitud->modalidad->id,
                "fecha_limite" => ($solicitud->fecha_limite)?$solicitud->fecha_limite:'SIN FECHA LIMITE',
                "total" => $solicitud->total,
                "usuario" => $solicitud->usuario->nombre,
                "proyecto" => $nombre,
                "proyecto_id" => $solicitud->proyecto_id,
                "empresa" => $solicitud->proyecto->empresa->nombre,
                "empresa_id" => $solicitud->proyecto->empresa_id,
                "last_estado_id" => $solicitud->estados->last()->estado_id,
            ];

            if ($solicitud->transferencia != null)
            {
                $array_datos_solicitud['banco_origen'] = $solicitud->transferencia->cuentaOrigen->banco;
                $array_datos_solicitud['cuenta_origen'] = $solicitud->transferencia->cuentaOrigen;
                $array_datos_solicitud['empresa_destino'] = $solicitud->transferencia->cuentaDestino->empresa;
                $array_datos_solicitud['banco_destino'] = $solicitud->transferencia->cuentaDestino->banco;
                $array_datos_solicitud['cuenta_destino'] = $solicitud->transferencia->cuentaDestino;
                $array_datos_solicitud['tipo_transferencia'] = $solicitud->transferencia->tipoTransferencia;
                $array_datos_solicitud['monto'] = $solicitud->transferencia->monto;
                $array_datos_solicitud['factor'] = $solicitud->transferencia->factor;
            }
        }
        return Response::json($array_datos_solicitud);
    }

    public function getDatosAutorizadorAsignacion($autorizador_id){
        $proy_usuarios = array();
        $proyectos_user_ids = UsuarioProyecto::select('proyecto_id')
                                ->where('user_id', $autorizador_id)
                                ->pluck('proyecto_id')
                                ->toArray();
       
        $proyectos = Proyectos::where('empresa_id', session('empresa'))
            ->whereIn('id', $proyectos_user_ids)
            ->get();                            
        foreach($proyectos as $proyecto){
            switch($proyecto->nivel){
                case 1:
                    $proyecto->nom_completo = $proyecto->nombre;
                    break;
                case 2:
                    $proyecto->nom_completo = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
                case 3:
                    $proyecto->nom_completo = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
            }
            $usuarios_proyecto = DB::table('proyectos_users AS pu')
                ->join('users AS u', 'pu.user_id', '=', 'u.id')
                ->join('proyectos AS p', 'pu.proyecto_id', '=', 'p.id')
                ->select('u.id', 'u.nombre', 'pu.proyecto_id', 'p.nombre as proyecto')
                ->where('pu.proyecto_id', '=', $proyecto->id)
                ->where('u.rol_id', '<>', $autorizador_id)
                ->get();
            if(count($usuarios_proyecto)>0){
                $proy_usuarios[$proyecto->nom_completo] = $usuarios_proyecto;
            }
        }
        $autorizador_usuarios = DB::table('autorizador_usuarios AS au')
            ->select('au.usuario_id', 'au.proyecto_id')
            ->where('empresa_id', '=', session('empresa'))
            ->where('autorizador_id', '=', $autorizador_id)
            ->get();
        $array_proy_ids = array();
        foreach($autorizador_usuarios as $id){
            $array_proy_ids[$id->proyecto_id][] = $id->usuario_id;
        }
        $array_ids = array_keys($array_proy_ids);
        foreach($proy_usuarios as $usuarios){
            foreach($usuarios as $usuario){
                if(in_array($usuario->proyecto_id, $array_ids) && in_array($usuario->id, $array_proy_ids[$usuario->proyecto_id])){
                    $usuario->checkbox=true;
                }else{
                    $usuario->checkbox=false;
                }
            }
        }
        return Response::json($proy_usuarios);
    }

    public function getDatosAprorendicionAsignacion($usuario_id){
        $proy_usuarios = array();
        $proyectos_user_ids = UsuarioProyecto::select('proyecto_id')
        ->where('user_id', $usuario_id)
        ->pluck('proyecto_id')
        ->toArray();
        $empresas_ids = Proyectos::select('empresa_id')
        ->distinct('empresa_id')
        ->whereIn('id', $proyectos_user_ids)->pluck('empresa_id')->toArray();
        $empresas = Empresas::whereIn('id', $empresas_ids)
        ->get();
        $aprorendicion_empresas = DB::table('aprorendicion_empresa AS ae')
        ->select('ae.empresa_id')
        ->where('aprorendicion_id', '=', $usuario_id)
        ->pluck('ae.empresa_id')
        ->toArray();

        foreach($empresas as $empresa){
            if(in_array($empresa->id, $aprorendicion_empresas)){
                $empresa->checkbox=true;
            }else{
                $empresa->checkbox=false;
            }
        }
        return Response::json($empresas);
    }

    public function getDatosItem($item_id){
        $item = Items::find($item_id);
        return Response::json($item);
    }

    //Generador Password
    public function passGenerator(){
        include_once (app_path().'/functions.php');
        echo "Contraseña<br>".generarClave();
    }

    public function getCantidadEstados($empresa_id){
        // session::set('empresa' , $empresa_id);
        // $empresa = Empresas::find(session('empresa'));
        
        // session(['empresa_nombre' => $empresa->nombre]);
        // session(['empresa_slug' => $empresa->slug]);
        // session(['paginacion' => 20]);
        
        $usuario = auth()->user();
        $cantidadEstados = array();

        $cantidadEstados = [
            'rol_id'        => $usuario->rol_id,
            'curso'         => count(SolicitudController::seccionSolicitud('usuario-en_curso')),
            'rendiciones'   => count(SolicitudController::seccionSolicitud('usuario-por_rendir')),
            'observadas'    => count(SolicitudController::seccionSolicitud('usuario-observadas')),
            'rechazadas'    => count(SolicitudController::seccionSolicitud('usuario-rechazadas')),
            'rendidas'      => count(SolicitudController::seccionSolicitud('usuario-rendidas')),
            'finalizadas'   => count(SolicitudController::seccionSolicitud('usuario-finalizadas')),
            //AprRendiciones
            'aprrendir'     => count(SolicitudController::seccionSolicitud('aprobar_rendiciones-aprobar_rendiciones')),
            'aprrendidas'   => count(SolicitudController::seccionSolicitud('aprobar_rendiciones-rendiciones_aprobadas')),
            //Autorizador
            'autorizadas'   => count(SolicitudController::seccionSolicitud('autorizacion-disponibles')),
            'porautorizar'  => count(SolicitudController::seccionSolicitud('autorizacion-autorizar')),
            //Revisor
            'revisadas'     => count(SolicitudController::seccionSolicitud('revisor-disponibles')),
            // 'porrevisar'    => count(SolicitudController::seccionSolicitud('revisor-revisor')),
            'porrevisarsr'    => count(SolicitudController::seccionSolicitud('revisor-revisor_sr')),
            'porrevisargd'    => count(SolicitudController::seccionSolicitud('revisor-revisor_gd')),
            'porrevisarrepo'    => count(SolicitudController::seccionSolicitud('revisor-revisor_repo')),
            'porrevisarfr'    => count(SolicitudController::seccionSolicitud('revisor-revisor_fra')),
            //'porrevisarfro'    => count(SolicitudController::seccionSolicitud('revisor-revisor_fro')),
            'porrevisarcc'     => count(SolicitudController::seccionFrSolicitud('revisor-revisor_cc')),
            'porrevisarfr' => count(SolicitudController::seccionSolicitud('revisor-revisor_fra')),  
            'porrevisarfra' => count(SolicitudController::seccionSolicitud('revisor-revisor_fra')),
            //'porrevisarfro' => count(SolicitudController::seccionSolicitud('revisor-revisor_fro')),    'porrevisarfro' => count(SolicitudController::seccionSolicitud('revisor-revisor_fro')),
            'porrevisarcc' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_cc')),     
            'porrevisarfra' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_fra')),       
            'porrevisarfro' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_fro')),       
            'porrevisarfrc' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_frc')),
            'porrevisarfrm' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_frm')),
            //Aprobador
            'aprobadas'     => count(SolicitudController::seccionSolicitud('aprobador_parcial-disponibles')),
            'poraprobar'    => count(SolicitudController::seccionSolicitud('aprobador-aprobador')),
            //Tesoreria
            'tesoreria'     => count(SolicitudController::seccionSolicitud('tesoreria-disponibles')),
            'portesoreria'  => count(SolicitudController::seccionSolicitud('tesoreria-tesoreria')),
        ];

        return response()->json($cantidadEstados);
    }

    public function getCantidadEstadosGlobal(Request $request){
        $url = \URL::previous();

        $usuario = auth()->user();
        $cantidadEstados = array();

        if (strpos($url, 'global/en_curso') !== false) {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Usuario
                'curso'         => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-en_curso', $request->all())),
                'rendiciones'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-por_rendir')),
                'observadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-observadas')),
                'rechazadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rechazadas')),
                'rendidas'      => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rendidas')),
                'finalizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-finalizadas')),
            ];
        } else if (strpos($url, 'global/autorizar') !== false) {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Autorizador
                'autorizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-disponibles')),
                'porautorizar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-autorizar')),
            ];
        } else if (strpos($url, 'global/aprobar_rendiciones') !== false) {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Aprobador rendiciones
                'aprrendir'     => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-aprobar')),
                'aprrendidas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-disponibles')),
            ];
        } else if (strpos($url, 'global/aprobar') !== false) {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Aprobador
                'aprobadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-disponibles')),
                'poraprobar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-aprobar')),
            ];
        } else if (strpos($url, 'global/pendientes') !== false) {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                'pendientes'    => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes')),
                'pendientes2'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes2')),
                'pendientes3'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes3')),
                'pendientes4'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes4')),
                'pendientes5'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes5')),
            ];
        } else if (strpos($url, 'global/revisor') !== false) {
            $cantidadEstados = [
                'rol_id'            => $usuario->rol_id,
                'sujeto_rendicion'  => count(SolicitudGlobalController::seccionFrSolicitudGlobal('sujeto_rendicion')),
                'gasto_directo'     => count(SolicitudGlobalController::seccionFrSolicitudGlobal('gasto_directo')),
                'fondo_rotativo'    => count(SolicitudGlobalController::seccionFrSolicitudGlobal('fondo_rotativo')),
                'reposicion'        => count(SolicitudGlobalController::seccionFrSolicitudGlobal('reposicion')),
            ];
        } else {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Usuario
                'curso'         => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-en_curso')),
                'rendiciones'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-por_rendir')),
                'observadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-observadas')),
                'rechazadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rechazadas')),
                'rendidas'      => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rendidas')),
                'finalizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-finalizadas')),
                // Autorizador
                'autorizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-disponibles')),
                'porautorizar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-autorizar')),
                // Aprobador rendiciones
                'aprrendir'     => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-aprobar')),
                'aprrendidas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-disponibles')),
                // Aprobador
                'aprobadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-disponibles')),
                'poraprobar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-aprobar')),
            ];
        }

        return response()->json($cantidadEstados);
    }

    public function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    public function getCantidadEstadosEmpresas(){
        $gestion = session('gestion');
        $gestion_id = session('gestion_id')[0];
        $empresa_id = session('empresa');

        $usuario = auth()->user();
        $cantidadEstados = array();
        $estadosCurso = ['SOL', 'SOL-M', 'AUT', 'REV', 'REV-M', 'APR', 'APR-P', 'REN-F', 'TES'];
        $estadosObservados = ['AUT-O', 'REV-O', 'APR-0', 'TES-O', 'REN-OA', 'REN-OB', 'REN-OC'];
        $estadosRechazadas = ['SOL-A', 'AUT-R', 'REV-R', 'APR-R', 'TES-R'];
        $estadosRendiciones = ['TES', 'REN', 'REN-P'];
        $estadosRendidas = ['REN-A', 'REN-B', 'REN-C'];
        $estadosFinalizadas = ['FIN'];
        $cantidadEstados = [
            'curso' => 0,
            'rendiciones' => 0,
            'observadas' => 0,
            'rechazadas' => 0,
            'rendidas' => 0,
            'finalizadas' => 0,
            //AprRendiciones
            'aprrendir' => 0,
            'aprrendidas' => 0,
            //Autorizador
            'autorizadas' => 0,
            'porautorizar' => 0,
            //Revisor
            'revisadas' => 0,
            'porrevisar' => 0,
            //Aprobador
            'aprobadas' => 0,
            'poraprobar' => 0,
            //Tesoreria
            'tesoreria' => 0,
            'portesoreria' => 0,
        ];

        if($usuario->rol_id == 1){
            //Rol Administrador
            $array_proyectos = Proyectos::where('empresa_id', $empresa_id)
                ->pluck('id')
                ->toArray();

            $solicitudes = Solicitudes::whereIn('proyecto_id', $array_proyectos)
                ->where('gestion_id', $gestion_id)
                ->get();
        }elseif($usuario->rol_id == 2 || $usuario->rol_id == 3){
            //Rol Usuario & Autorizador
            $array_proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                $query->where('user_id', $usuario->id);
            })
                ->where('empresa_id', $empresa_id)
                ->pluck('id')->toArray();
            $solicitudes = Solicitudes::whereIn('proyecto_id', $array_proyectos)
                ->where('gestion_id', $gestion_id)
                ->where('usuario_id', $usuario->id)
                ->get();
        }elseif($usuario->rol_id > 3){
            $array_proyectos = Proyectos::where('empresa_id', $empresa_id)
                ->pluck('id')
                ->toArray();

            $solicitudes = Solicitudes::whereIn('proyecto_id', $array_proyectos)
                ->where('gestion_id', $gestion_id)
                ->where('usuario_id', $usuario->id)
                ->get();
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = app('App\Http\Controllers\SolicitudController')->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = app('App\Http\Controllers\SolicitudController')->getLastEstadoCodigo($solicitud->id);
            if(in_array($solicitud->estadoCodigo, $estadosCurso)){
                $cantidadEstados['curso']++;
            }
            if(in_array($solicitud->estadoCodigo, $estadosRendiciones)){
                $cantidadEstados['rendiciones']++;
            }
            if(in_array($solicitud->estadoCodigo, $estadosObservados)){
                $cantidadEstados['observadas']++;
            }
            if(in_array($solicitud->estadoCodigo, $estadosRechazadas)){
                $cantidadEstados['rechazadas']++;
            }
            if(in_array($solicitud->estadoCodigo, $estadosRendidas)){
                $cantidadEstados['rendidas']++;
            }
            if(in_array($solicitud->estadoCodigo, $estadosFinalizadas)){
                $cantidadEstados['finalizadas']++;
            }
        }

        switch($usuario->rol_id){
            case 3:
                //Autorizador
                $proyectos = UsuarioProyecto::where('user_id', $usuario->id)
                    ->select('proyectos_users.proyecto_id')
                    ->pluck('proyectos_users.proyecto_id');

                $usuarios_proyectos = DB::table('autorizador_usuarios')
                    ->select('usuario_id', 'proyecto_id')
                    ->where('autorizador_id', $usuario->id)
                    ->get();

                $sol_ids = array();
                foreach($usuarios_proyectos as $usuario_proy){
                    $solicitudes_usu_proy = Solicitudes::select('solicitudes.id')
                        ->where('usuario_id', $usuario_proy->usuario_id)
                        ->where('proyecto_id', $usuario_proy->proyecto_id)
                        ->pluck('solicitudes.id')
                        ->toArray();
                    foreach($solicitudes_usu_proy as $sol_usu_proy){
                        $sol_ids[] = $sol_usu_proy;
                    }
                }
                $cantidadEstados['autorizadas'] = count($sol_ids);

                //usuarios autorizados por autorizador
                $usuarios_proyectos = DB::table('autorizador_usuarios')
                    ->select('usuario_id', 'proyecto_id')
                    ->where('autorizador_id', $usuario->id)
                    ->where('empresa_id', $empresa_id)
                    ->get();
                //proyectos de autorizador
                $proyectos_autorizador = UsuarioProyecto::where('user_id', $usuario->id)
                    ->select('proyectos_users.proyecto_id')
                    ->pluck('proyectos_users.proyecto_id');

                //ids solicitudes por usuario
                $sol_ids = array();
                foreach($usuarios_proyectos as $usuario_proy){
                    $solicitudes_usu_proy = Solicitudes::select('solicitudes.id')
                        ->where('usuario_id', $usuario_proy->usuario_id)
                        ->where('proyecto_id', $usuario_proy->proyecto_id)
                        ->pluck('solicitudes.id')
                        ->toArray();
                    foreach($solicitudes_usu_proy as $sol_usu_proy){
                        $sol_ids[] = $sol_usu_proy;
                    }
                }

                foreach($proyectos_autorizador as $proyecto_aut){
                    //ids solicitudes autorizador
                    $solicitudes_usu_proy = Solicitudes::select('solicitudes.id')
                        ->where('usuario_id', $usuario->id)
                        ->where('proyecto_id', $proyecto_aut)
                        ->pluck('solicitudes.id')
                        ->toArray();

                    foreach($solicitudes_usu_proy as $sol_usu_proy){
                        $sol_ids[] = $sol_usu_proy;
                    }
                }

                $solicitudes_ids = array();
                foreach($sol_ids as $sol_id){
                    if($this->getLastEstadoCodigo($sol_id) == 'SOL'
                        || $this->getLastEstadoCodigo($sol_id) == 'SOL-M'
                        || $this->getLastEstadoCodigo($sol_id) == 'AUT-M'
                        || $this->getLastEstadoCodigo($sol_id) == 'AUT-O'){

                        $solicitudes_ids[] = $sol_id;
                    }
                }
                $cantidadEstados['porautorizar'] = count($solicitudes_ids);
                break;
            case 4:
                //Revisor
                $proyectos_ids = Proyectos::select('id')
                    ->where('empresa_id', $empresa_id)
                    ->pluck('id')
                    ->toArray();
                $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                    ->select('solicitudes.id')
                    ->where('gestion_id', $gestion_id)
                    ->whereIn('solicitudes.proyecto_id', $proyectos_ids)
                    ->orderBy('solicitudes.created_at')
                    ->groupBy('solicitud_id')
                    ->pluck('solicitudes.id')
                    ->toArray();

                $solicitudes_ids = array();
                foreach($sol_ids as $sol_id){
                    if(in_array($this->getLastEstadoCodigo($sol_id), ['AUT', 'REV-M', 'REV-O', 'REV-M'])){
                        $solicitudes_ids[] = $sol_id;
                    }
                }
                $cantidadEstados['porrevisar'] = count($solicitudes_ids);

                $proyectos = Proyectos::where('empresa_id', $empresa_id)
                    ->select('proyectos.id')
                    ->pluck('proyectos.id');
                $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                    ->select('solicitudes.id')
                    ->where('gestion_id', $gestion_id)
                    ->whereIn('proyecto_id', $proyectos)
                    ->orderBy('solicitudes.created_at')
                    ->groupBy('solicitud_id')
                    ->pluck('solicitudes.id')
                    ->toArray();

                $cantidadEstados['revisadas'] = count($sol_ids);
                break;
            case 5:
                //Aprobador
                $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                    ->select('solicitudes.id')
                    ->where('gestion_id', $gestion_id)
                    ->orderBy('solicitudes.created_at')
                    ->groupBy('solicitud_id')
                    ->pluck('solicitudes.id')
                    ->toArray();

                $solicitudes_ids = array();
                foreach($sol_ids as $sol_id){
                    if($this->getLastEstadoCodigo($sol_id) == 'REV'){
                        $solicitudes_ids[] = $sol_id;
                    }
                    if($this->getLastEstadoCodigo($sol_id) == 'APR-P'){
                        $solicitudes_ids[] = $sol_id;
                    }
                    if($this->getLastEstadoCodigo($sol_id) == 'REV-M'){
                        $solicitudes_ids[] = $sol_id;
                    }
                }
                $cantidadEstados['poraprobar'] = count($solicitudes_ids);

                $proyectos = Proyectos::where('empresa_id', $empresa_id)->select('proyectos.id')->pluck('proyectos.id');
                $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                    ->select('solicitudes.id')
                    ->where('gestion_id', $gestion_id)
                    ->whereIn('proyecto_id', $proyectos)
                    ->orderBy('solicitudes.created_at')
                    ->groupBy('solicitud_id')
                    ->pluck('solicitudes.id')
                    ->toArray();

                $cantidadEstados['aprobadas'] = count($sol_ids);
                break;
            case 6:
                //Tesoreria
                $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                    ->select('solicitudes.id')
                    ->where('gestion_id', $gestion_id)
                    ->orderBy('solicitudes.created_at')
                    ->groupBy('solicitud_id')
                    ->pluck('solicitudes.id')
                    ->toArray();

                $solicitudes_ids = array();
                foreach($sol_ids as $sol_id){
                    if($this->getLastEstadoCodigo($sol_id) == 'APR' || $this->getLastEstadoCodigo($sol_id) == 'REV'){
                        $solicitudes_ids[] = $sol_id;
                    }
                }
                $cantidadEstados['portesoreria'] = count($solicitudes_ids);

                $proyectos = Proyectos::where('empresa_id', $empresa_id)->select('proyectos.id')->pluck('proyectos.id');
                $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                    ->select('solicitudes.id')
                    ->where('gestion_id', $gestion_id)
                    ->whereIn('proyecto_id', $proyectos)
                    ->orderBy('solicitudes.created_at')
                    ->groupBy('solicitud_id')
                    ->pluck('solicitudes.id')
                    ->toArray();
                $cantidadEstados['tesoreria'] = count($sol_ids);

                break;
        }

        if($usuario->apr_rendiciones){
            $usuario = auth()->user();

            //
            //usuarios autorizados por autorizador
            $usuarios_proyectos = DB::table('autorizador_usuarios')
                ->select('usuario_id', 'proyecto_id')
                ->where('autorizador_id', $usuario->id)
                ->where('empresa_id', $empresa_id)
                ->get();
            //proyectos de autorizador
            $proyectos_autorizador = UsuarioProyecto::where('user_id', $usuario->id)
                ->select('proyectos_users.proyecto_id')
                ->pluck('proyectos_users.proyecto_id')->toArray();

            $proyectos_empresa = Proyectos::where('empresa_id', $empresa_id)
                ->pluck('id')
                ->toArray();

            $emp_proys = array_intersect($proyectos_autorizador, $proyectos_empresa);

            $sol_ids = array();
            foreach($usuarios_proyectos as $usuario_proy){
                $solicitudes_usu_proy = Solicitudes::select('solicitudes.id')
                    ->where('usuario_id', $usuario_proy->usuario_id)
                    ->where('proyecto_id', $usuario_proy->proyecto_id)
                    ->pluck('solicitudes.id')
                    ->toArray();
                foreach($solicitudes_usu_proy as $sol_usu_proy){
                    if(!in_array($sol_usu_proy, $sol_ids)){
                        $sol_ids[] = $sol_usu_proy;
                    }
                }
            }

            foreach($emp_proys as $proyecto_aut){
                //ids solicitudes autorizador
                $solicitudes_usu_proy = Solicitudes::select('solicitudes.id')
                    ->where('usuario_id', $usuario->id)
                    ->where('proyecto_id', $proyecto_aut)
                    ->pluck('solicitudes.id')
                    ->toArray();

                foreach($solicitudes_usu_proy as $sol_usu_proy){
                    if(!in_array($sol_usu_proy, $sol_ids)){
                        $sol_ids[] = $sol_usu_proy;
                    }
                }
            }

            $solicitudes_ids = array();
            foreach($sol_ids as $sol_id){
                if(in_array($this->getLastEstadoCodigo($sol_id), ['REN-F', 'APR-RO'])){
                    $solicitudes_ids[] = $sol_id;
                }
            }
            $cantidadEstados['aprrendir'] = count($solicitudes_ids);
        }

        return response()->json($cantidadEstados);
    }

    public function getItemsRendidosSolicitud($solicitud_id){
        $items = ItemsRendiciones::where("solicitud_id", $solicitud_id)->get();
        foreach($items as $item){
            $item->unidad_descripcion = $item->unidad->descripcion;
            if (isset($item->tipo_id)) {
                $item->tipocompra = $item->tipo_compra->descripcion;
            }else {
                $item->tipocompra = "";
            }
            $item->subtotal = $item->cantidad * $item->costo;
        }
        return Response::json($items);
    }

    public function getEstadosSolicitud(){
        $estado = $_GET['estado'];
        $codigo_estado = Estados::where('codigo', $estado)->first();
        $ordenes_array = [10, 20, 30, 40, 50, 60, 70, 90];
        $flag = true;
        foreach($ordenes_array as $orden_item){
            if($codigo_estado->orden < $orden_item && $flag){
                $ordenes[] = $codigo_estado->orden;
                $flag = false;
            }else{
                $ordenes[] = $orden_item;
            }
        }
        $estados = Estados::whereIn('orden', $ordenes)
            ->get();
        return Response::json($estados);
    }

    public function getPorcentajeAprobador($aprobador_id, $solicitud_id){
        $monto_solicitud = Solicitudes::where('id', $solicitud_id)
            ->pluck('total')
            ->first();

        $nivel_id = NivelesAprobacion::where('desde', '<=', $monto_solicitud)
            ->where('hasta', '>=', $monto_solicitud)
            ->pluck('id')
            ->first();

        $porcentaje = NivelAprobador::where('aprobador_id', $aprobador_id)
            ->where('nivel_id', $nivel_id)
            ->get();
        return Response::json($porcentaje);
    }

    public function getDatosCheque($solicitud_id){
        $sol_cheques = Cheques::where('solicitud_id', $solicitud_id)->get();
        foreach($sol_cheques as $cheque){
            $cheques[] = [
                'id' => $cheque->solicitud->id,
                'cuenta' => $cheque->cuenta->numero,
                'moneda' => $cheque->cuenta->moneda->nombre,
                'banco' => $cheque->cuenta->banco->nombre,
                'numero' => $cheque->solicitud->numero,
                'desembolso' => $cheque->solicitud->desembolso,
                'referencia' => $cheque->solicitud->referencia,
                'observacion' => $cheque->solicitud->observacion,
                'fecha' => $cheque->solicitud->fecha_limite,
                'total' => $cheque->solicitud->total,
                //'total_rendido' => $cheque->solicitud->id,
                'empleado_id' => $cheque->solicitud->empleado_id,
                'modalidad' => $cheque->solicitud->modalidad->nombre,
                'moneda' => $cheque->solicitud->moneda->nombre,
                'usuario_id' => $cheque->solicitud->usuario_id,
                'solicitante' => $cheque->solicitud->usuario->nombre,
                'proyecto' => $cheque->solicitud->proyecto->nombre,
                'gestion' => $cheque->solicitud->gestion->gestion,
                'cambio' => $cheque->solicitud->tipo_cambio->cambio,
                'cheque_id' => $cheque->id,
                'cheque_nro' => $cheque->numero_cheque,
                'cheque_beneficiario' => $cheque->beneficiario,
                'cheque_fecha' => $cheque->fecha,
                'cheque_monto' => $cheque->monto,
                'cheque_observacion' => $cheque->observacion,
                'cheque_concepto' => $cheque->concepto,
                'cheque_cuenta_id' => $cheque->cuenta_id,
                'cheque_empresa_id' => $cheque->empresa_id,
            ];
        }
        return Response::json($cheques);
    }

    public function solicitudCompleta($solicitud_id){
        $solicitud = Solicitudes::find($solicitud_id);
        $respuesta = false;
        if(count($solicitud->itemsController) > 0){
            $respuesta = true;
        }

        return Response::json($respuesta);
    }

    public function getBancosEmpresa($empresa_id){
        $bancos_ids = CuentaBancaria::where('empresa_id', $empresa_id)
            ->pluck('banco_id')
            ->toArray();
        $bancos = Bancos::whereIn('id', $bancos_ids)
            ->get();
        return Response::json($bancos);
    }

    public function getCuentasEmpresa($empresa_id, $banco_id){
        $cuentas = CuentaBancaria::where('banco_id', $banco_id)
            ->where('empresa_id', $empresa_id)
            ->get();

        foreach($cuentas as $cuenta){
            $cuenta->moneda_texto = $cuenta->moneda->sigla;
        }

        return Response::json($cuentas);
    }

    public function getTiposCompra(){
        $tipos = ItemsTipos::orderBy('descripcion', 'ASC')
            ->pluck('id', 'descripcion')
            ->toArray();

        return Response::json($tipos);
    }

    public function getEmpresas(){
        $empresas = Empresas::select('id', 'nombre')->pluck('nombre', 'id')->toArray();

        return Response::json($empresas);
    }

    public function existeProyecto($usuario_id, $proyecto_id){        
        $existe = UsuarioProyecto::select('proyecto_id')
                                ->where('user_id', $usuario_id)
                                ->where('proyecto_id', $proyecto_id)
                                ->get()
                                ->count();
        return response()->json(['valor' => $existe]);
    }

    public function proyectosUsuario($proyecto_id, $usuario_id)
    {
        // Cantidad de solicitudes que el usuario creo en ese proyecto
        $cantidad_solicitudes = Solicitudes::where('proyecto_id', $proyecto_id)
                                    ->where('usuario_id', $usuario_id)
                                    ->get()
                                    ->count();

        $solicitudes_ids = Solicitudes::select('id')
                                ->where('proyecto_id', $proyecto_id)
                                ->get()
                                ->pluck('id')
                                ->toArray();
        // Solicitudes en las que se realizo movimientos
        $cantidad_solicitudes_pendientes = EstadoSolicitudes::whereIn('solicitud_id', $solicitudes_ids)
                                            ->where('usuario_id', $usuario_id)
                                            ->select('solicitud_id')
                                            ->distinct('solicitud_id')
                                            ->get()
                                            ->count();
        // canntidad de personas que esta asignadas al al autorisador
        $cantidad_usuarios = DB::table('autorizador_usuarios')                                
                                ->where('autorizador_id', $usuario_id)
                                ->where('proyecto_id', $proyecto_id)
                                ->get()
                                ->count();
        $total = $cantidad_solicitudes + $cantidad_solicitudes_pendientes + $cantidad_usuarios;
        return response()->json(['valor' => $total]);
    }

    public function getDatosItemsAF($solicitud_id){
        $itemsRendidos = ItemsRendiciones::where('solicitud_id', $solicitud_id)->get();
        $ret = false;
        foreach($itemsRendidos as $item){
            $form_af = count(ItemsAf::where('item_rendido_id', $item->id)->get());
            if($form_af > 0){
                $ret = true;
            }
        }

        return response()->json(['respuesta' => $ret]);
    }

    public function getFormuItemsAF($solicitud_id){
        $itemsRendidos = ItemsRendiciones::where('solicitud_id', $solicitud_id)->get();
        foreach($itemsRendidos as $item){
            $array_ids[] = $item->id;
        }

        $itemsFormularios = ItemsAf::whereIn('item_rendido_id', $array_ids)->get();
        return Response::json($itemsFormularios);
    }

    public function getFrProyecto($proyecto_id){
        $proyectos_id = FondoRotativoEmpresa::select('fr_id')
            ->where('proyecto_id', $proyecto_id)
            ->pluck('fr_id')
            ->toArray();

        $frs = FondosRotativos::where('status', 1)
            ->whereIn('id', $proyectos_id)->get();

        return Response::json($frs);
    }
}
