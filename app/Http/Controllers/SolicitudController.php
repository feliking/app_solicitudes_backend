<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

// Email
use App\Mail\solicitudEmail;

// Job
use App\Jobs\jobEnvioSolicitud;

use App\DocumentosRendicion;
use App\EstadoSolicitudes;
use App\FactorTipoCambio;
use App\ItemsRendiciones;
use App\FondosRotativos;
use App\Configuraciones;
use App\ItemsController;
use App\UsuarioProyecto;
use App\CuentaBancaria;
use App\Transferencia;
use App\TiposCambios;
use App\Devoluciones;
use App\Solicitudes;
use App\Modalidades;
use App\ItemsTipos;
use App\Documentos;
use App\Proyectos;
use App\Gestiones;
use App\Empresas;
use App\Unidades;
use App\Monedas;
use App\Estados;
use App\ItemsAf;
use App\Items;
use App\User;

use Session;
use Mail;
use DB;

// Auth
use Illuminate\Support\Facades\Auth;

class SolicitudController extends Controller
{
    function __construct()
    {
        $this->middleware('banned_create', ['only' => ['create', 'store']]);
        
        $this->middleware('banned_authorize', ['only' => ['guardarAutorizacion']]);

        $this->middleware('banned_revise', ['only' => ['guardarAprobacionController']]);

        $this->middleware('banned_approve', ['only' => ['cambiarAprobadorSolicitud']]);

        $this->middleware('year', ['only' => 
            [
                'create', 'store',
                'edit', 'update',
                'modificarController',
            ]
        ]);
    }

    /**
     * Devuelve los id de las solicitudes por sección (pestaña)
     */
    public static function seccionSolicitudAdminTransferencias($filters = null){
        $estados = ['AUT', 'AUT-M', 'PEN', 'PEN-2', 'PEN-3'];

        if(!empty($estados)){
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::pluck('id')
                ->toArray();
        }

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            // ->where('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            });

        /** BEGIN Filters */

        // Empresa
        if(isset($filters['empresa_id']) && $filters['empresa_id'] != 0){
            $idProyectos = Proyectos::where('empresa_id', $filters['empresa_id'])
                ->pluck('id')
                ->toArray();

            $idSolicitudes = $idSolicitudes
                ->whereIn('solicitudes.proyecto_id', $idProyectos);
        }

        // Proyecto
        if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0){
            $idSolicitudes = $idSolicitudes
                ->where('solicitudes.proyecto_id', $filters['proyecto_id']);
        }

        // Monto
        if(isset($filters['monto']) && $filters['monto'] != 0 && isset($filters['selectMonto'])){
            switch($filters['selectMonto']){
                case 0:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', $filters['monto']);
                    break;
                case 1:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>', $filters['monto']);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>=', $filters['monto']);
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<', $filters['monto']);
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<=', $filters['monto']);
                    break;
            }
        }

        // Fechas
        if(isset($filters['fecha_ini']) && isset($filters['fecha_fin']) && $filters['fecha_ini'] != '' && $filters['fecha_fin'] != ''){
            $idSolicitudes = $idSolicitudes->whereBetween('solicitudes.created_at', ['CAST("'.$filters['fecha_ini'].'")', 'CAST("'.$filters['fecha_fin'].'")']);
        }elseif(isset($filters['fecha_ini']) && $filters['fecha_ini'] != ''){
            $fecha_ini = $filters['fecha_ini'];
            $idSolicitudes = $idSolicitudes->where('solicitudes.created_at', 'LIKE', "%$fecha_ini%");
        }

        // Custom
        if(isset($filters['campo']) && $filters['campo'] != 0 && isset($filters['texto']) && $filters['texto'] != ''){
            $texto = $filters['texto'];
            switch($filters['campo']){
                case 1:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();
                    $idSolicitudes = $idSolicitudes->whereIn('solicitudes.usuario_id', $idUsuarios);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.desembolso', 'LIKE', "%$texto%");
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.referencia', 'LIKE', "%$texto%");
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.observacion', 'LIKE', "%$texto%");
                    break;
                case 5:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.numero', $texto);
                    break;
                case 6:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();

                    $idSolicitudes = $idSolicitudes
                        ->orWhereIn('solicitudes.usuario_id', $idUsuarios)
                        ->orWhere('solicitudes.desembolso', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.referencia', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.observacion', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.numero', $texto);
                    break;
            }
        }

        // Modalidad
        $idSolicitudes = $idSolicitudes->whereIn('solicitudes.modalidad_id', [5]);

        // Gestión
        if (isset($filters['gestion_id']) && $filters['gestion_id'] != 0) {
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $filters['gestion_id']);
        } else if (!isset($filters['gestion_id'])){
            $gestion = Gestiones::orderBy('id', 'DESC')->first();
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $gestion->id);
        }

        /** END Filters */
        
        // dd($filters['gestion_id'], $idSolicitudes->toSql());
        
        $idSolicitudes = $idSolicitudes->pluck('solicitudes.id')->toArray();

        return $idSolicitudes;
    }
    
    /**
     * Devuelve los id de las solicitudes por sección (pestaña)
     */
    public static function seccionSolicitudAdmin($seccion, $filters = null){
        switch($seccion){
            case 'admin-creadas':
                $estados = ['SOL', 'SOL-M', 'APR-O'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-revisadas':
                $estados = ['REV', 'REV-P'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-aprobar':
                $estados = ['AUT', 'AUT-M', 'AUT-AF', 'APR-P', 'REV', 'REV-P'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-pendientes':
                $estados = ['PEN'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-pendientes2':
                $estados = ['PEN-2'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-pendientes3':
                $estados = ['PEN-3'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-pendientes4':
                $estados = ['PEN-4'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-pendientes5':
                $estados = ['PEN-5'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2];
                }
                break;
            case 'admin-tesoreria':
                $estados = ['APR'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2, 5];
                }
                break;
            case 'admin-rechazadas':
                $estados = ['AUT-R', 'REV-R', 'APR-R', 'TES-R', 'REN-R'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2, 5];
                }
                break;
            case 'admin-rechazadas_tesoreria':
                $estados = ['TES-R'];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2, 5];
                }
                break;
            case 'admin-todas':
                $estados = [];

                // Modalidad
                if(isset($filters['chk_fr_cc'])){
                    $modalidades_array =  [3, 4];
                }else{
                    $modalidades_array = [1, 2, 5];
                }
                break;
        }

        if(!empty($estados)){
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::pluck('id')
                ->toArray();
        }

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            // ->where('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            });

        /** BEGIN Filters */

        // Empresa
        if(isset($filters['empresa_id']) && $filters['empresa_id'] != 0){
            $idProyectos = Proyectos::where('empresa_id', $filters['empresa_id'])
                ->pluck('id')
                ->toArray();

            $idSolicitudes = $idSolicitudes
                ->whereIn('solicitudes.proyecto_id', $idProyectos);
        }

        // Proyecto
        if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0){
            $idSolicitudes = $idSolicitudes
                ->where('solicitudes.proyecto_id', $filters['proyecto_id']);
        }

        // Monto
        if(isset($filters['monto']) && $filters['monto'] != 0 && isset($filters['selectMonto'])){
            switch($filters['selectMonto']){
                case 0:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', $filters['monto']);
                    break;
                case 1:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>', $filters['monto']);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '>=', $filters['monto']);
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<', $filters['monto']);
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.total', '<=', $filters['monto']);
                    break;
            }
        }

        // Fechas
        if(isset($filters['fecha_ini']) && isset($filters['fecha_fin']) && $filters['fecha_ini'] != '' && $filters['fecha_fin'] != ''){
            $idSolicitudes = $idSolicitudes->whereBetween('solicitudes.created_at', ['CAST("'.$filters['fecha_ini'].'")', 'CAST("'.$filters['fecha_fin'].'")']);
        }elseif(isset($filters['fecha_ini']) && $filters['fecha_ini'] != ''){
            $fecha_ini = $filters['fecha_ini'];
            $idSolicitudes = $idSolicitudes->where('solicitudes.created_at', 'LIKE', "%$fecha_ini%");
        }

        // Custom
        if(isset($filters['campo']) && $filters['campo'] != 0 && isset($filters['texto']) && $filters['texto'] != ''){
            $texto = $filters['texto'];
            switch($filters['campo']){
                case 1:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();
                    $idSolicitudes = $idSolicitudes->whereIn('solicitudes.usuario_id', $idUsuarios);
                    break;
                case 2:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.desembolso', 'LIKE', "%$texto%");
                    break;
                case 3:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.referencia', 'LIKE', "%$texto%");
                    break;
                case 4:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.observacion', 'LIKE', "%$texto%");
                    break;
                case 5:
                    $idSolicitudes = $idSolicitudes->where('solicitudes.numero', $texto);
                    break;
                case 6:
                    $idUsuarios = User::where('nombre', 'LIKE', "%$texto%")
                        ->pluck('id')
                        ->toArray();

                    $idSolicitudes = $idSolicitudes
                        ->orWhereIn('solicitudes.usuario_id', $idUsuarios)
                        ->orWhere('solicitudes.desembolso', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.referencia', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.observacion', 'LIKE', "%$texto%")
                        ->orWhere('solicitudes.numero', $texto);
                    break;
            }
        }

        // Modalidad
        $idSolicitudes = $idSolicitudes->whereIn('solicitudes.modalidad_id', $modalidades_array);

        // Gestión
        if (isset($filters['gestion_id']) && $filters['gestion_id'] != 0) {
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $filters['gestion_id']);
        } else if (!isset($filters['gestion_id'])){
            $gestion = Gestiones::orderBy('id', 'DESC')->first();
            $idSolicitudes = $idSolicitudes->where('solicitudes.gestion_id', $gestion->id);
        }

        /** END Filters */
        
        // dd($filters['gestion_id'], $idSolicitudes->toSql());
        
        $idSolicitudes = $idSolicitudes->pluck('solicitudes.id')->toArray();

        return $idSolicitudes;
    }
    
    /**
     * Devuelve los id de las solicitudes por sección (pestaña)
     */
    public static function seccionSolicitud($seccion, $extra_query = ""){
        $ses_user = Auth::user();
        // Proyectos de la empresa actual (sesión)

        if($ses_user->rol_id == 3 && $seccion == 'autorizacion-autorizar'){
            $idProyectos = Proyectos::join('proyectos_users', 'proyectos_users.proyecto_id', '=', 'proyectos.id')
                ->join('autorizador_usuarios', 'autorizador_usuarios.proyecto_id', '=', 'proyectos.id')
                ->where('autorizador_usuarios.autorizador_id', $ses_user->id)
                ->where('proyectos.empresa_id', session('empresa'))
                ->distinct()
                ->pluck('proyectos.id')
                ->toArray();
        }else{
            $idProyectos = Proyectos::where('empresa_id', session('empresa'))
                ->pluck('id')
                ->toArray();
        }

        // Modalidad
        $modalidad = 0;

        // Tipo FR
        $tipo_fr = 0;

        // Proyectos
        $proyectos_revisor = [];

        switch($seccion){
            /**
             * BEGIN: SECCIONES DE USARIO
             */
            case 'usuario-en_curso':
                $estados = ['SOL', 'SOL-M', 'AUT', 'AUT-M', 'AUT-AF', 'REV', 'REV-M', 'REV-P', 'APR', 'APR-M', 'APR-P', 'TES-M', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5', 'SOL-V'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();
                break;
            case 'usuario-por_rendir':
                $estados = ['TES', 'REN-R', 'REN-O'];
                
                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();
                break;
            case 'usuario-observadas':
                $estados = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();
                break;
            case 'usuario-rechazadas':
                $estados = ['AUT-R', 'REV-R', 'APR-R', 'TES-R'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();
                break;
            case 'usuario-rendidas':
                $estados = ['REN-P', 'REN-F'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();
                break;
            case 'usuario-finalizadas':
                $estados = ['FIN'];

                $idUsuarios = User::where('id', Auth::user()->id)
                    ->pluck('id')
                    ->toArray();
                break;
            /**
             * END: SECCIONES DE USARIO
             */
            /**
             * BEGIN: SECCIONES DE AUTORIZADOR
             */
            case 'autorizacion-autorizar':
                $estados = ['SOL', 'SOL-M', 'AUT-M'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'autorizacion-disponibles':
                $estados = ['AUT', 'AUT-R', 'AUT-O', 'AUT-M', 'AUT-AF', 'REV', 'REV-R', 'REV-P', 'REV-O', 'REV-M', 'APR', 'APR-R', 'APR-O', 'APR-M', 'APR-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5', 'TES', 'TES-R', 'TES-O', 'TES-M', 'REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'SOL-V', 'APR-RO', 'FIN'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            /**
             * END: SECCIONES DE AUTORIZADOR
             */
            /**
             * BEGIN: SECCIONES DE APROBADOR PARCIAL
             */
            case 'aprobador_parcial-aprobador':
                $estados = ['AUT', 'AUT-AF', 'REV', 'APR-M', 'APR-P'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'aprobador_parcial-disponibles':
                $estados = ['APR', 'APR-R', 'APR-O', 'APR-M', 'APR-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5', 'TES', 'TES-R', 'TES-O', 'TES-M', 'REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'SOL-V', 'APR-RO', 'FIN'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();
                break;
            /**
             * END: SECCIONES DE APROBADOR PARCIAL
             */
            /**
             * BEGIN: SECCIONES DE APROBADOR DE RENDICIONES
             */
            case 'aprobar_rendiciones-aprobar_rendiciones':
                $estados = ['REN-F'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'aprobar_rendiciones-rendiciones_aprobadas':
                $estados = ['REN-A','REN-R','REN-OA','REN-OB','REN-OC','SOL-V','APR-RO','FIN'];

                $idUsuariosAutoriza = DB::table('autorizador_usuarios')
                    ->where('autorizador_id', Auth::user()->id)
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('usuario_id')
                    ->toArray();

                $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
                    ->pluck('id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            /**
             * END: SECCIONES DE APROBADOR DE RENDICIONES
             */
            /**
             * BEGIN: SECCIONES DE REVISOR
             */
            case 'revisor-revisor':
                $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();
                break;
            case 'revisor-revisor_sr':
                $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->whereNotIn('user_id', [107, 108])
                    ->pluck('user_id')
                    ->toArray();

                $modalidad = 1;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_gd':
                $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->whereNotIn('user_id', [107, 108])
                    ->pluck('user_id')
                    ->toArray();

                $modalidad = 2;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_repo':
                $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->whereNotIn('user_id', [107, 108])
                    ->pluck('user_id')
                    ->toArray();

                $modalidad = 4;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_fra':
                $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->whereNotIn('user_id', [107, 108])
                    ->pluck('user_id')
                    ->toArray();

                $modalidad = 3;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_fro':
                $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->whereNotIn('user_id', [107, 108])
                    ->pluck('user_id')
                    ->toArray();

                $modalidad = 4;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-disponibles':
                $estados = ['REV', 'REV-R', 'REV-O', 'REV-M', 'REV-P', 'APR', 'APR-R', 'APR-O', 'APR-M', 'APR-P', 'PEN', 'PEN-2', 'PEN-3', 'PEN-4', 'PEN-5', 'TES', 'TES-R', 'TES-O', 'TES-M', 'REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'SOL-V', 'APR-RO', 'FIN'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            /**
             * END: SECCIONES DE REVISOR
             */
            /**
             * BEGIN: SECCIONES DE APROBADOR
             */
            case 'aprobador-aprobador':
                $estados = ['AUT', 'AUT-AF', 'REV', 'APR-M', 'APR-P'];

                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            /**
             * END: SECCIONES DE APROBADOR
             */
            /**
             * BEGIN: SECCIONES DE TESORERÍA
             */
            case 'tesoreria-tesoreria':
                $estados = ['APR', 'TES-M'];
                
                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();

                $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);
                break;
            case 'tesoreria-disponibles':
                $estados = ['TES', 'TES-R', 'TES-O', 'TES-M', 'REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'SOL-V', 'APR-RO', 'FIN'];
                    
                $idUsuarios = DB::table('proyectos_users')
                    ->whereIn('proyecto_id', $idProyectos)
                    ->pluck('user_id')
                    ->toArray();
                break;
            /**
             * END: SECCIONES DE TESORERÍA
             */
        }

        if(!empty($estados)){
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::pluck('id')
                ->toArray();
        }

        if(Auth::user()->rol_id == 1){
            $idUsuarios = User::get()
                ->pluck('id')
                ->toArray();
        }

//        dd($idProyectos,'123');
        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            ->whereIn('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            // ->whereIn('solicitudes.proyecto_id', $idProyectos)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            });

        // Modalidad
        if($modalidad != 0){
            $idSolicitudes = $idSolicitudes->where('solicitudes.modalidad_id', $modalidad);
        }

        // Proyectos revisor
        $secciones_revisor = ['revisor-revisor_sr', 'revisor-revisor_gd', 'revisor-revisor_repo', 'revisor-revisor_fra', 'revisor-revisor_fro', 'revisor-disponibles'];
        if (in_array($seccion, $secciones_revisor)){
            if (count($proyectos_revisor) > 0){
                $intersect = array_intersect($idProyectos, $proyectos_revisor);
                $idSolicitudes = $idSolicitudes->whereIn('solicitudes.proyecto_id', $intersect);
            }else{
                $proyectos_revisor = DB::table('proyecto_revisor')
                    ->pluck('proyecto_id');

                $idSolicitudes = $idSolicitudes->whereIn('solicitudes.proyecto_id', $idProyectos)
                    ->whereNotIn('solicitudes.proyecto_id', $proyectos_revisor);
            }
        }else{
            $idSolicitudes = $idSolicitudes->whereIn('solicitudes.proyecto_id', $idProyectos);
        }

        if($extra_query != ""){
            if (strpos($extra_query, 'LIKE') !== false){
                $fields = explode('LIKE', $extra_query);
            }else{
                $fields = explode('=', $extra_query);
            }

            if (strpos($extra_query, 'created_at') !== false) {
                $fields = explode(',', $extra_query);
            }

            switch(trim($fields[0])){
                case "numero":
                    $idSolicitudes = $idSolicitudes
                        ->where('solicitudes.numero', $fields[1]);
                    break;
                case "total":
                    $idSolicitudes = $idSolicitudes
                        ->where('solicitudes.total', $fields[1]);
                    break;
                case "proyecto_id":
                    $p_ids = explode(',', trim($fields[1]));
                    foreach( $p_ids as $pos => $value ){
                        $p_ids[$pos] = intval( $value);
                    }
                    $idSolicitudes = $idSolicitudes
                        ->whereIn('solicitudes.proyecto_id', $p_ids);
                    break;
                case "usuario_id":
                    $p_ids = explode(',', trim($fields[1]));
                    foreach( $p_ids as $pos => $value ){
                        $p_ids[$pos] = intval( $value);
                    }
                    $idSolicitudes = $idSolicitudes
                        ->whereIn('solicitudes.usuario_id', $p_ids);
                    break;
                case "desembolso":
                    $fields[1] = trim($fields[1]);
                    $idSolicitudes = $idSolicitudes
                        ->where('solicitudes.desembolso', 'LIKE', "%$fields[1]%");
                    break;
                case "referencia":
                    $fields[1] = trim($fields[1]);
                    $idSolicitudes = $idSolicitudes
                        ->where('solicitudes.referencia', 'LIKE', "%$fields[1]%");
                    break;
                case "observacion":
                    $fields[1] = trim($fields[1]);
                    $idSolicitudes = $idSolicitudes
                        ->where('solicitudes.observacion', 'LIKE', "%$fields[1]%");
                    break;
                case "created_at":
                    $startDate = Carbon::createFromFormat('Y-m-d', $fields[1]);
                    $endDate = Carbon::createFromFormat('Y-m-d', $fields[2]);
                    $idSolicitudes = $idSolicitudes
                        ->whereBetween('solicitudes.created_at', [$startDate, $endDate]);
                    break;
            }
        }
        //dd($extra_query);

        $idSolicitudes = $idSolicitudes->pluck('solicitudes.id')->toArray();
        
        if($seccion == 'aprobador_parcial-aprobador' || $seccion == 'aprobador-aprobador'){
            $idSolicitudesAprobadas = Solicitudes::join('aprobador', 'aprobador.solicitud_id', '=', 'solicitudes.id')
                ->join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
                ->select('solicitudes.*')
                ->whereIn('solicitudes.gestion_id', session('gestion_id'))
                ->whereIn('solicitudes.usuario_id', $idUsuarios)
                ->whereIn('estados_solicitud.estado_id', $idEstados)
                ->whereIn('solicitudes.proyecto_id', $idProyectos)
                ->where('aprobador.aprobador_id', Auth::user()->id)
                ->whereIn('estados_solicitud.id', function($query){
                    $query->selectRaw('MAX(estados_solicitud.id)')
                        ->from('estados_solicitud')
                        ->groupBy('estados_solicitud.solicitud_id')
                        ->get();
                })
                ->pluck('solicitudes.id')
                ->toArray();

            // dd($idSolicitudes, $idSolicitudesAprobadas);

            $idSolicitudes = array_diff($idSolicitudes, $idSolicitudesAprobadas);
        }

        return $idSolicitudes;
    }

    public static function seccionFrSolicitud($seccion, $extra_query = ""){
        // Proyectos de la empresa actual (sesión)
        $idProyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();
        // Modalidad
        $modalidad = 0;

        // Proyectos revisor
        $proyectos_revisor = [];

        $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P', 'PEN', 'PEN-2', 'PEN-3'];
        $idUsuarios = DB::table('proyectos_users')
            ->whereIn('proyecto_id', $idProyectos)
            ->whereNotIn('user_id', [107, 108])
            ->pluck('user_id')
            ->toArray();
        $modalidad = 3;
        switch($seccion){
            case 'revisor-revisor_cc':
                $tipo_fr = 1;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_fra':
                $tipo_fr = 2;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_fro':
                $tipo_fr = 3;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_frc':
                $tipo_fr = 4;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;
            case 'revisor-revisor_frm':
                $tipo_fr = 5;

                $proyectos_revisor = Auth::user()->proyectosRevisor->pluck('id')->toArray();
                break;

        }
        if(!empty($estados)){
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::pluck('id')
                ->toArray();
        }
        if(Auth::user()->rol_id == 1){
            $idUsuarios = User::get()
                ->pluck('id')
                ->toArray();
        }
        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->join('fondorotativos', 'solicitudes.frtipo_id', '=', 'fondorotativos.id')
            ->select('solicitudes.*')
            ->whereIn('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            // ->whereIn('solicitudes.proyecto_id', $idProyectos)
            ->where('fondorotativos.tipo_id', $tipo_fr)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            });
        // Modalidad
        if($modalidad != 0){
            $idSolicitudes = $idSolicitudes->where('solicitudes.modalidad_id', $modalidad);
        }
        // Tipo FR        
        if($extra_query != ""){
            $fields = explode('=', $extra_query);
            switch($fields[0]){
                case "numero":
                    $idSolicitudes = $idSolicitudes
                        ->where('solicitudes.numero', $fields[1]);
                    break;
                case "total":
                    $idSolicitudes = $idSolicitudes
                        ->where('solicitudes.total', $fields[1]);
                    break;
            }
        }

        // Proyectos revisor
        $secciones_revisor = ['revisor-revisor_fra', 'revisor-revisor_fro', 'revisor-revisor_frc', 'revisor-revisor_frm'];
        
        if (in_array($seccion, $secciones_revisor))
        {
            if (count($proyectos_revisor) > 0)
            {
                $intersect = array_intersect($idProyectos, $proyectos_revisor);
                $idSolicitudes = $idSolicitudes->whereIn('solicitudes.proyecto_id', $intersect);
            }
            else
            {
                $proyectos_revisor = DB::table('proyecto_revisor')
                    ->pluck('proyecto_id');
    
                $idSolicitudes = $idSolicitudes->whereIn('solicitudes.proyecto_id', $idProyectos)
                    ->whereNotIn('solicitudes.proyecto_id', $proyectos_revisor);
            }
        }
        else
        {
            $idSolicitudes = $idSolicitudes->whereIn('solicitudes.proyecto_id', $idProyectos);
        }

        $idSolicitudes = $idSolicitudes->pluck('solicitudes.id')->toArray();
        if($seccion == 'aprobador_parcial-aprobador' || $seccion == 'aprobador-aprobador'){
            $idSolicitudesAprobadas = Solicitudes::join('aprobador', 'aprobador.solicitud_id', '=', 'solicitudes.id')
                ->join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
                ->select('solicitudes.*')
                ->whereIn('solicitudes.gestion_id', session('gestion_id'))
                ->whereIn('solicitudes.usuario_id', $idUsuarios)
                ->whereIn('estados_solicitud.estado_id', $idEstados)
                ->whereIn('solicitudes.proyecto_id', $idProyectos)
                ->where('aprobador.aprobador_id', Auth::user()->id)
                ->whereIn('estados_solicitud.id', function($query){
                    $query->selectRaw('MAX(estados_solicitud.id)')
                        ->from('estados_solicitud')
                        ->groupBy('estados_solicitud.solicitud_id')
                        ->get();
                })
                ->pluck('solicitudes.id')
                ->toArray();
            $idSolicitudes = array_diff($idSolicitudes, $idSolicitudesAprobadas);
        }
        return $idSolicitudes;
    }
    
    /**
     * Devuelve los id de las solicitudes a las cuales el usuario puede 'editar'
     * 
     * @param string $seccion
     * @param string $action
     */
    private function canEditSolicitud($id){
        if(Auth::user()->rol_id == 1){
            return true;
        }
        $idProyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();

        $estados = ['SOL', 'SOL-M', 'AUT-O', 'REV-O', 'APR-O', 'TES-O'];

        $idEstados = Estados::whereIn('codigo', $estados)
            ->pluck('id')
            ->toArray();

        $idUsuarios = User::where('id', Auth::user()->id)
            ->pluck('id')
            ->toArray();

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            ->whereIn('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('solicitudes.proyecto_id', $idProyectos)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            })
            ->pluck('solicitudes.id')
            ->toArray();

        if(in_array($id, $idSolicitudes)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Devuelve los id de las solicitudes a las cuales el autorizador puede 'editar'
     * 
     * @param string $seccion
     * @param string $action
     */
    private function canEditAutSolicitud($id){
        $idProyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();

        $estados = ['SOL', 'SOL-M', 'AUT-M'];

        $idEstados = Estados::whereIn('codigo', $estados)
            ->pluck('id')
            ->toArray();

        $idUsuariosAutoriza = DB::table('autorizador_usuarios')
            ->where('autorizador_id', Auth::user()->id)
            ->whereIn('proyecto_id', $idProyectos)
            ->pluck('usuario_id')
            ->toArray();

        $idUsuarios = User::whereIn('id', $idUsuariosAutoriza)
            ->pluck('id')
            ->toArray();

        $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            ->whereIn('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('solicitudes.proyecto_id', $idProyectos)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            })
            ->pluck('solicitudes.id')
            ->toArray();

        if(in_array($id, $idSolicitudes)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Devuelve los id de las solicitudes a las cuales el revisor puede 'editar'
     * 
     * @param string $seccion
     * @param string $action
     */
    private function canEditRevSolicitud($id){
        $idProyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();

        $estados = ['AUT', 'AUT-AF', 'REV-M', 'REV-P'];

        $idEstados = Estados::whereIn('codigo', $estados)
            ->pluck('id')
            ->toArray();

        $idUsuarios = DB::table('proyectos_users')
            ->whereIn('proyecto_id', $idProyectos)
            ->pluck('user_id')
            ->toArray();

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            ->whereIn('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('solicitudes.proyecto_id', $idProyectos)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            })
            ->pluck('solicitudes.id')
            ->toArray();

        // dd($idProyectos, $idEstados, $idUsuarios, $idSolicitudes, !in_array($id, $idSolicitudes));

        if(in_array($id, $idSolicitudes)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Devuelve los id de las solicitudes a las cuales el revisor puede 'editar'
     * 
     * @param string $seccion
     * @param string $action
     */
    private function canEditTesSolicitud($id){
        $idProyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();

        $estados = ['APR', 'TES-M'];

        $idEstados = Estados::whereIn('codigo', $estados)
            ->pluck('id')
            ->toArray();

        $idUsuarios = DB::table('proyectos_users')
            ->whereIn('proyecto_id', $idProyectos)
            ->pluck('user_id')
            ->toArray();

        $idUsuarios = array_prepend($idUsuarios, Auth::user()->id);

        $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('solicitudes.*')
            ->whereIn('solicitudes.gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.usuario_id', $idUsuarios)
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('solicitudes.proyecto_id', $idProyectos)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            })
            ->pluck('solicitudes.id')
            ->toArray();

        if(in_array($id, $idSolicitudes)){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $search = '*'){
        $extra_query = "";
        // Pregunta si existe información del filtrado
        if(isset($request)){
            // Selección del filtro utilizado
            switch($request->selectOpcion){
                case 0:
                    //Todos
                    break;
                case 1:
                    //Numero
                    if($request->buscar != ''){
                        $extra_query = "numero=".$request->buscar;
                    }
                    break;
                case 2:
                    //Monto
                    if($request->buscar != ''){
                        $extra_query = "total=".$request->buscar;
                    }
                    break;
                case 3:
                    //Proyecto
                    if($request->buscar != ''){
                        $proy_query = Proyectos::where('nombre', 'LIKE', "%$request->buscar%")
                            ->pluck('id')
                            ->toArray();
                        if(count($proy_query) > 0){
                            $extra_query = "proyecto_id IN (".implode(', ', $proy_query).")";
                        }
                    }
                    break;
                case 4:
                    //Fecha
                    // if(isset($request->fechaIni)){
                    //     if(isset($request->fechaFin)){
                    //         $extra_query = "created_at BETWEEN '".$request->fechaIni."' AND '".$request->fechaFin."'";
                    //     }else{
                    //         $extra_query = "created_at LIKE ".$request->fechaIni;
                    //     }
                    // }
                    if (isset($request->fechaIni)) {
                        if (isset($request->fechaFin)) {
                            $extra_query = "created_at,".$request->fechaIni.",".$request->fechaFin;
                        } else {
                            $extra_query = "created_at,".$request->fechaIni.",".$request->fechaIni;
                        }
                    }
                    break;
                case 5:
                    //Solicitante
                    if($request->buscar != ''){
                        $usu_query = User::where("nombre", "LIKE", "'%".$request->buscar."%'")
                            ->pluck('id')
                            ->toArray();
                        if(count($usu_query) > 0){
                            $extra_query = "usuario_id IN (".implode(', ', $usu_query).")";
                        }
                    }
                    break;
                case 6:
                    //Desembolso
                    if($request->buscar != ''){
                        $extra_query = "desembolso LIKE '%".$request->buscar."%'";
                    }
                    break;
                case 7:
                    //Referencia
                    if($request->buscar != ''){
                        $extra_query = "referencia LIKE '%".$request->buscar."%'";
                    }
                    break;
                case 8:
                    //Observacion
                    if($request->buscar != ''){
                        $extra_query = "observacion LIKE '%".$request->buscar."%'";
                    }
                    break;
            }
        }

        $usuario = auth()->user();
        // $id_usuario = $usuario->id;
        if (isset($request->empresa) && $request->empresa > 0) {
        // if(!session()->has('empresa') && isset($request->empresa)){
            //INIT VARIABLES DE SESION
            session::set('empresa' , $request->empresa);
            $empresa = Empresas::find(session('empresa'));
            
            if(isset($request->gestion)){
                session()->forget('gestion_id');
                session()->forget('gestion');

                $id_gestiones = explode('|', $request->gestion);

                $text_gestiones = '';
                foreach ($id_gestiones as $id_gestion) {
                    $gestion = Gestiones::find($id_gestion);
                    $text_gestiones .= $gestion->gestion . ' ';
                }

                session(['gestion_id' => $id_gestiones]);
                session(['gestion' => trim($text_gestiones)]);
            }

            session(['empresa_nombre' => $empresa->nombre]);
            session(['empresa_slug' => $empresa->slug]);
            session(['paginacion' => 20]);
            
            // dump(session('empresa'), session('gestion_id'), session('gestion'));
        }
        // if(isset($request->gestion)){
        //     session()->forget('gestion_id');
        //     session()->forget('gestion');

        //     $id_gestiones = explode('|', $request->gestion);

        //     $text_gestiones = '';
        //     foreach ($id_gestiones as $id_gestion) {
        //         $gestion = Gestiones::find($id_gestion);
        //         $text_gestiones .= $gestion->gestion . ' ';
        //     }

        //     session(['gestion_id' => $id_gestiones]);
        //     session(['gestion' => trim($text_gestiones)]);
        // }

        //Relacion usuario-proyecto
        $proyectos = Proyectos::where('empresa_id', session('empresa'))->get();
        // Verifica que no existan proyectos y que el usuario tenga privilegios para crear los mismos
        if(count($proyectos) == 0 && ($usuario->rol_id == 1 || $usuario->rol_id >= 4)){
            return redirect()->route('proyectos.create');
        }

        if(session('empresa') != null){
            if(!isset($array_addquery)){
                $array_addquery = '';
            }

            $tituloSeleccionSolicitud = "EN CURSO";
            $extras = ["FFF", "1ABB9C"];
            
            // Establece la pestaña elegida por el usuario, sino es En curso
            if(isset($_GET['t'])){
                switch($_GET['t']){
                    case 'observadas':
                        // $var = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];
                        $idSolicitudes = $this->seccionSolicitud('usuario-observadas', $extra_query);
                        $tituloSeleccionSolicitud = "OBSERVADAS";
                        $extras = [
                            "FFF", "F39C12"
                        ];
                        break;
                    case 'rechazadas':
                        // $var = ['AUT-R', 'REV-R', 'APR-R', 'TES-R', 'REN-R'];
                        $idSolicitudes = $this->seccionSolicitud('usuario-rechazadas', $extra_query);
                        $tituloSeleccionSolicitud = "RECHAZADAS";
                        $extras = [
                            "FFF", "E74C3C"
                        ];
                        break;
                    case 'rendidas':
                        // $var = ['REN-F'];
                        $idSolicitudes = $this->seccionSolicitud('usuario-rendidas', $extra_query);
                        $tituloSeleccionSolicitud = "RENDIDAS";
                        $extras = [
                            "FFF", "50C1CF"
                        ];
                        break;
                    case 'finalizadas':
                        // $var = ['FIN'];
                        $idSolicitudes = $this->seccionSolicitud('usuario-finalizadas', $extra_query);
                        $tituloSeleccionSolicitud = "FINALIZADAS";
                        $extras = [
                            "FFF", "3498DB"
                        ];
                        break;
                    default:
                        $idSolicitudes = $this->seccionSolicitud('usuario-en_curso', $extra_query);
                        $tituloSeleccionSolicitud = "EN CURSO";
                        $extras = [
                            "FFF", "1ABB9C"
                        ];
                        break;
                }
            }
            if(!isset($idSolicitudes)){
                $idSolicitudes = $this->seccionSolicitud('usuario-en_curso', $extra_query);
            }

            // Se está cambiando la variable $solicitud_ids -> $idSolicitudes
            $solicitudes = Solicitudes::sortable()
                ->orderBy('numero', 'DESC')
                ->orderBy('desembolso', 'ASC')
                ->orderBy('referencia', 'ASC')
                ->orderBy('observacion', 'ASC')
                ->orderBy('total', 'ASC')
                ->orderBy('modalidad_id', 'ASC')
                ->orderBy('moneda_id', 'ASC')
                ->orderBy('usuario_id', 'ASC')
                ->orderBy('proyecto_id', 'ASC')
                ->orderBy('cambio_id', 'ASC')
                ->orderBy('created_at', 'ASC')
                // ->whereIn('id', $solicitud_ids)
                ->whereIn('id', $idSolicitudes)
                ->get();

            // Genera la estructura de centros de costos
            foreach($solicitudes as $solicitud){
                $solicitud->estado = $this->getLastEstado($solicitud->id);
                $solicitud->empresa = $solicitud->proyecto->empresa->nombre;
                $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
                if($solicitud->modalidad_id == 3){
                    $solicitud->frNombre = $solicitud->frtipo_id;
                }
                if($solicitud->proyecto->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
                }elseif($solicitud->proyecto->padre->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }else{
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }
                $solicitud->formulariosAF = false;
                $items_af = DB::select('SELECT COUNT(id) as cantidad, solicitud_id FROM item_rendiciones WHERE solicitud_id = '.$solicitud->id.' AND tipo_id IN (1,7) GROUP BY solicitud_id');
                $cantidad = 0;
                foreach($items_af as $item_af){
                    if($item_af->cantidad > $cantidad){
                        $cantidad = $item_af->cantidad;
                    }
                }
                if($cantidad > 0){
                    $solicitud->formulariosAF = true;
                }
            }

            if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
                if($_GET['order'] == 'asc'){
                    $solicitudes = $solicitudes->sortBy('estado');
                }else{
                    $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
                }
            }
            if(isset($_GET['sort']) && $_GET['sort'] == 'empresa'){
                if($_GET['order'] == 'asc'){
                    $solicitudes = $solicitudes->sortBy('empresa');
                }else{
                    $solicitudes = $solicitudes->sortBy('empresa', SORT_REGULAR, true);
                }
            }
            $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

            if(isset($_GET['t'])){
                return view('solicitudes.index', ['t' => $_GET['t']])
                    ->withTitulo($tituloSeleccionSolicitud)
                    ->withSolicitudes($solicitudes)
                    ->withExtras($extras);
            }else{
                return view('solicitudes.index')
                    ->withTitulo($tituloSeleccionSolicitud)
                    ->withSolicitudes($solicitudes)
                    ->withExtras($extras);
            }
        }elseif($usuario->rol_id == 1){
            return view('empresas.crear');
        }else{
            return redirect('/');
        }
    }

    public function solicitudesPorEstado($estado){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //echo "SolicitudController@create";
        $usuario = auth()->user();
        $modalidades = Modalidades::where('oculto', false)->get();
        $monedas = Monedas::all();
        //$tipo_cambio = TiposCambios::all()->last()->toArray();
        $tipo_cambio = TiposCambios::moneda(2)->get()->last()->toArray();
        $tipo_cambio_euro = TiposCambios::moneda(3)->get()->last()->toArray();
        $array_ids = array();
        if($usuario->rol_id == 1 || $usuario->rol_id >3 || $usuario->revisor){
            $proyectos = Proyectos::where('empresa_id', session('empresa'))
                ->get();
            foreach($proyectos as $proyecto){
                switch($proyecto->nivel){
                    case 2:
                        $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                    case 3:
                        $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                }
            }
        }else{
            $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                $query->where('user_id', $usuario->id);
            })
                ->where('empresa_id', session('empresa'))
                ->get();
            foreach($proyectos as $proyecto){
                switch($proyecto->nivel){
                    case 2:
                        $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                    case 3:
                        $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                }
            }
        }

        if(count($proyectos) == 0){
            Session::flash('success', 'No cuenta con proyectos asignados');
            return redirect()->route('solicitudes');
            //VERIFICAR REDIRECCION
        }else{
            $controller = false;

            $proyectos = $proyectos->sortBy(
                function($proy){
                    return $proy->nombre;
                }
            );

            $frs = DB::select("SELECT empresa_fr.fr_id FROM empresa_fr WHERE empresa_id = ".session('empresa'));
            $fr_array = array();
            foreach($frs as $fr){
                $fr_array[] = $fr->fr_id;
            }
            $frs = FondosRotativos::whereIn('id', $fr_array)->get();

            return view('solicitudes.crear')
                ->withCambioEuro($tipo_cambio_euro)
                ->withController($controller)
                ->withProyectos($proyectos)
                ->withModalidades($modalidades)
                ->withCambio($tipo_cambio)
                ->withMonedas($monedas)
                ->withController($controller)
                ->withDate(Carbon::now()->format('Y-m-d'))
                ->withFrs($frs);             
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        
        //echo "SolicitudController@store";
        $usuario = auth()->user();
        //Total solicitud
        $total = 0;//dd($request->all());
        if(count($request->subtotal) > 0){
            $sub_totales = $request->subtotal;
            foreach($sub_totales as $sub_total){
                $total += $sub_total;
            }
        }
        $tituloSeleccionSolicitud = "En curso";

        //Secuencial Solicitud Empresa Gestion
        $secuencial = 0;
        $empresa_secuencial = Proyectos::find($request->proyecto)->empresa;
        // $proyectos_ids = Proyectos::where('empresa_id', session('empresa'))
        //    ->pluck('id')->toArray();
        $proyectos_ids = Proyectos::where('empresa_id', $empresa_secuencial->id)
            ->pluck('id')->toArray();
        $sol_emp_ges = Solicitudes::whereIn('proyecto_id', $proyectos_ids)
            ->whereIn('gestion_id', session('gestion_id'))
            ->max('numero');
        $secuencial = (count($sol_emp_ges) == 0)? 1:($sol_emp_ges+1);

        try{
            $solicitud = new Solicitudes;
            $solicitud->numero      = $secuencial;
            $solicitud->desembolso  = mb_strtoupper($request->desembolso);
            
            if ($request->modalidad == 5) // TRANSFERENCIA
            {
                $cuenta_origen = CuentaBancaria::find($request->cuenta_origen);
                $cuenta_destino = CuentaBancaria::find($request->cuenta_destino);
                $tipo_transferencia = ItemsTipos::find($request->tipo_transferencia);
                
                $solicitud->referencia = 'TRANSFERENCIA DE LA CUENTA: ' . $cuenta_origen->banco->nombre . ' - ' . $cuenta_origen->numero . ' A LA CUENTA: ' . $cuenta_destino->banco->nombre . ' - ' . $cuenta_destino->numero . ' DE LA EMPRESA ' . $cuenta_destino->empresa->nombre . ' POR CONCEPTO DE ' . $tipo_transferencia->descripcion;
            }
            else
            {
                $solicitud->referencia  = mb_strtoupper($request->referencia);
            }

            $solicitud->observacion = mb_strtoupper($request->observacion);
            if($request->fecha_lim != ''){
                $solicitud->fecha_limite = $request->fecha_lim;
            }
            
            if ($request->modalidad == 5) // TRASFERENCIA
            {
                $solicitud->total = $request->monto;
            }
            else 
            {
                $solicitud->total = array_sum($request->subtotal);
            }
            
            //$solicitud->total_rendido = 0; //variable de rendicion
            $solicitud->modalidad_id  = $request->modalidad;
            if($solicitud->modalidad_id == 3){
                $solicitud->frtipo_id  = $request->empresa_fr;
            }
            $solicitud->moneda_id     = $request->moneda;
            $solicitud->usuario_id    = $usuario->id;
            $solicitud->proyecto_id = $request->proyecto;
            $solicitud->gestion_id  = session('gestion_id')[0];
            if($usuario->empleado_id != null){
                $solicitud->empleado_id = $usuario->empleado_id;
            }else{
                $solicitud->empleado_id = 0;
            }
            //$solicitud->prioridad_id = 2;
            //$solicitud->cambio_id   = $request->cambio_id;
            $solicitud->cambio_id = TiposCambios::moneda($request->moneda)->get()->last()->id;
            
            $solicitud->save();

            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD CREADA',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 10,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);

            switch($usuario->rol_id){
                case 4:
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD AUTORIZADA (REVISOR)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 20,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                    break;
                case 5:
                    DB::table('estados_solicitud')->insert([
                        [
                            'observacion' => 'SOLICITUD AUTORIZADA (APROBADOR)',
                            'usuario_id' => $usuario->id,
                            'solicitud_id' => $solicitud->id,
                            'estado_id' => 20,
                            "created_at" =>  Carbon::now(),
                            "updated_at" => Carbon::now()
                        ],
                        [
                            'observacion' => 'SOLICITUD REVISADA (APROBADOR)',
                            'usuario_id' => $usuario->id,
                            'solicitud_id' => $solicitud->id,
                            'estado_id' => 30,
                            "created_at" =>  Carbon::now(),
                            "updated_at" => Carbon::now()
                        ]
                    ]);
                    break;
                case 6:
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD AUTORIZADA (TESORERIA)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 20,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                    /*DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD REVISADA (TESORERIA)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 30,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);*/
                    /*DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD APROBADA (TESORERIA)',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 40,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);*/
                    break;
            }

            
            if ($request->modalidad == 5) // TRANSFERENCIA
            {
                $transferencia = new Transferencia();
                $transferencia->monto = $request->monto;
                $transferencia->factor = $request->tipo_cambio;
                $transferencia->cuenta_origen_id = $request->cuenta_origen;
                $transferencia->cuenta_destino_id = $request->cuenta_destino;
                $transferencia->tipo_transferencia_id = $request->tipo_transferencia;
                $transferencia->solicitud_id = $solicitud->id;

                $transferencia->save();
            }
            else
            {
                //Datos Items
                $cantidades = $request->cantidad;
                $unidades = $request->unidad;
                $detalles = $request->detalle;
                $punitarios = $request->punitario;
                $tipo_compra = $request->tipo_compra;
                $cant_items = count($sub_totales);
                for($i = 0; $i < $cant_items; $i++){
                    $item = new Items;
                    $item->detalle          = mb_strtoupper($detalles[$i]);
                    $item->cantidad         = $cantidades[$i];
                    $item->tipo_id          = $tipo_compra[$i];
                    $item->costo            = $punitarios[$i];
                    $item->numero_partida   = 0;
                    $item->nombre_partida   = 0;
                    $item->unidad_id        = $unidades[$i];
                    $item->solicitud_id     = $solicitud->id;
                    $item->save();

                    if($request->modalidad == 3 || $request->modalidad == 4){
                        $nroFacturas = $request->nrofactura;
                        $fechaFacturas = $request->fechafactura;
                        $proyectos = $request->itemproyecto;
                        $proveedores = $request->itemproveedor;
                        $tipos = $request->itemtipo;

                        // Datos Items rendidos
                        $itemRen = new ItemsRendiciones();
                        $itemRen->cantidad          = $cantidades[$i];
                        $itemRen->costo             = $punitarios[$i];
                        $itemRen->fecha_factura     = $fechaFacturas[$i];
                        $itemRen->num_factura       = $nroFacturas[$i];
                        $itemRen->proyecto          = mb_strtoupper($proyectos[$i]);
                        $itemRen->proveedor         = mb_strtoupper($proveedores[$i]);
                        $itemRen->tipo              = $tipos[$i];
                        $itemRen->detalle           = mb_strtoupper($detalles[$i]);
                        $itemRen->direccion         = '';
                        $itemRen->item_id           = $item->id;
                        $itemRen->unidad_id         = $unidades[$i];
                        $itemRen->solicitud_id      = $solicitud->id;
                        $itemRen->tipo_id           = $tipo_compra[$i];
                        $itemRen->save();
                    }
                }
            }

            //Upload Files
            $array_documentos = array();
            $path = public_path().'/empresas/'.session('empresa_slug')."/".session('gestion')."/solicitud/";
            //echo $path;
            for($i = 1; $i <= 30; $i++){
                $doc_name = 'doc'.$i;
                if(isset($_FILES[$doc_name]['name']) && $_FILES[$doc_name]['name']!=''){
                    $str_name = trim($_FILES[$doc_name]['name']);
                    $file_type = $_FILES[$doc_name]['type'];
                    $str_name = $this->remover_acentos($str_name);
                    $str_name = strtolower($str_name);
                    $str_name = str_replace('-', '', $str_name);
                    $str_name = str_replace(' ', '_', $str_name);
                    $str_name = str_replace('__', '_', $str_name);
                    $str_name = str_replace('___ ', '_', $str_name);
                    $str_name = str_replace('____', '_', $str_name);
                    //$db_filename = $id_query."_".$str_name."_".time();
                    //***REVISAR NOMBRE DE ARCHIVO SUBIDO
                    $db_filename = $solicitud->id."_".time()."_".$str_name;
                    move_uploaded_file($_FILES[$doc_name]['tmp_name'], $path.$db_filename);
                    $documento = new Documentos;
                    $documento->direccion = $db_filename;
                    $documento->observaciones = '';
                    $documento->solicitud_id = $solicitud->id;

                    $documento->save();
                }
            }

            $orden_type = "DESC";
            $solicitudes = Solicitudes::whereIn('proyecto_id', $proyectos_ids)
                ->whereIn('gestion_id',session('gestion_id'))
                ->where('usuario_id', $usuario->id)
                ->orderBy('id', 'DESC')
                ->paginate(session('paginacion'));
            $arrayItems = array();
            foreach($solicitudes as $sol){
                $items = Items::where("solicitud_id", $sol->id)->get();
                foreach($items as $item){
                    $arrayItems[$sol->id][]=[
                        'unidad'    =>  $item->unidad_id,
                        'detalle'   =>  $item->detalle,
                        'cantidad'  =>  $item->cantidad,
                        'costo'     =>  $item->costo,
                        'subtotal'  =>  $item->subtotal
                    ];
                }
            }

            $emailErr = "Valid";
            if(isset($request->correo_envio) && trim($request->correo_envio) != ''){
                $correo = mb_strtolower(trim($request->correo_envio));
                if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid";
                }
                if($emailErr == "Valid"){
                    //enviar correo
                    dispatch(new jobEnvioSolicitud($solicitud->numero, session('empresa_nombre'), $solicitud->proyecto->nombre, $solicitud->referencia, $correo));
                }
            }
        }catch(Exception $e){
            echo "Excepción :".$e->getMessage();
        }

        foreach($solicitudes as $sol){
            $sol->estado = $this->getLastEstado($sol->id);
            $sol->estadoCodigo = $this->getLastEstadoCodigo($sol->id);
            if($sol->proyecto->padre_id == null){
                $sol->nom_completo_proyecto = $sol->proyecto->nombre;
            }elseif($sol->proyecto->padre->padre_id == null){
                $sol->nom_completo_proyecto = $sol->proyecto->padre->nombre." &#10148; ".$sol->proyecto->nombre;
            }else{
                $sol->nom_completo_proyecto = $sol->proyecto->padre->padre->nombre." &#10148; ".$sol->proyecto->padre->nombre." &#10148; ".$sol->proyecto->nombre;
            }
        }

        //return redirect()->action('SolicitudController@index');
        Session::flash('success', 'Solicitud #'.$solicitud->numero.' creada!!!');
        return redirect()->route('solicitudes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "SolicitudController@show";
        exit();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $controller=false)
    {
        $solicitud = Solicitudes::find($id);
        
        if (\Auth::user()->global_view == 0) {
            if(!$this->canEditSolicitud($id)){
                $control = false;
                switch(Auth::user()->rol_id){
                    case 3: // Autorizador
                        $control = true;
                        if(!$this->canEditAutSolicitud($id)){
                            abort('403', 'edit');
                        }
                        break;
                    case 6: // Tesorería
                        $control = true;
                        if(!$this->canEditTesSolicitud($id)){
                            abort('403', 'edit');
                        }
                        break;
                }
    
                if(!$control){
                    abort('403', 'edit');
                }
            }
        }
        
        $usuario = auth()->user();

        if($usuario->rol_id == 2){
            if($solicitud->usuario_id != $usuario->id){
                return back();
            }
        }

        $modalidades = Modalidades::where('oculto', false)->get();
        $monedas = Monedas::all();
        //$tipo_cambio = TiposCambios::all()->last()->toArray();
        $tipo_cambio = TiposCambios::moneda(2)->get()->last()->toArray();
        $tipo_cambio_euro = TiposCambios::moneda(3)->get()->last()->toArray();
        $unidades = Unidades::all()->pluck('descripcion', 'id');
        $tipo_compra = ItemsTipos::all()
            ->pluck('descripcion', 'id')
            ->prepend('SELECCIONE TIPO DE COMPRA', '');

        $array_edit = ['SOL', 'SOL-M', 'AUT-O'];
        if($usuario->rol_id == 2 && ($solicitud->usuario_id != $usuario->id)){
            return redirect()->route('solicitudes');
        }

        $array_ids = array();
        if($usuario->rol_id == 1 || $usuario->rol_id >=3){
            $proyectos = Proyectos::where('empresa_id', session('empresa'))
                ->get();
        }else{
            $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                $query->where('user_id', $usuario->id);
            })->get();
        }

        if($usuario->rol_id == 1 || $usuario->rol_id >3){
            $proyectos = Proyectos::where('empresa_id', session('empresa'))
                ->get();
            foreach($proyectos as $proyecto){
                switch($proyecto->nivel){
                    case 2:
                        $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                    case 3:
                        $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                }
            }
        }else{
            $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                $query->where('user_id', $usuario->id);
            })
                ->where('empresa_id', session('empresa'))
                ->get();
            foreach($proyectos as $proyecto){
                switch($proyecto->nivel){
                    case 2:
                        $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                    case 3:
                        $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                        break;
                }
            }
        }

        if (\Auth::user()->global_view == 1) {
            $proyectos = Proyectos::get();
        }

        $documentos = Documentos::where('solicitud_id', $id)
            ->get();

        foreach($documentos as $documento){
            $documento->empresa_slug = "";
            $documento->empresa_slug = $documento->solicitud->proyecto->empresa->slug;
        }

        $proyectos = $proyectos->sortBy(
            function($proy){
                return $proy->nombre;
            }
        );

        if (\Auth::user()->global_view == 0){
            $frs = DB::select("SELECT empresa_fr.fr_id FROM empresa_fr WHERE empresa_id = ".session('empresa'));
        } else {
            $frs = DB::select("SELECT empresa_fr.fr_id FROM empresa_fr");
        }
        
        $fr_array = array();
        foreach($frs as $fr){
            $fr_array[] = $fr->fr_id;
        }
        $frs = FondosRotativos::whereIn('id', $fr_array)->get();
        
        return view('solicitudes.editar')
            ->withProyectos($proyectos)
            ->withModalidades($modalidades)
            ->withDocumentos($documentos)
            ->withTipocompra($tipo_compra)
            ->withController($controller)
            ->withSolicitud($solicitud)
            ->withCambio($tipo_cambio)
            ->withCambioEuro($tipo_cambio_euro)
            ->withUnidades($unidades)
            ->withMonedas($monedas)
            ->withFrs($frs)
            ->withDate(Carbon::now()->format('Y-m-d'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = auth()->user();
        $tituloSeleccionSolicitud = "En curso";
        $proyectos_ids = Proyectos::where('empresa_id', session('empresa'))->pluck('id')->toArray();
        //Total solicitud
        $total = 0;
        if(count($request->subtotal) > 0){
            $sub_totales = $request->subtotal;
            foreach($sub_totales as $sub_total){
                $total += $sub_total;
            }
        }

        try{
            $solicitud = Solicitudes::find($id);
            $solicitud->desembolso_antiguo  = mb_strtoupper($solicitud->desembolso);
            $solicitud->desembolso  = mb_strtoupper($request->desembolso);
            $solicitud->referencia  = mb_strtoupper($request->referencia);
            $solicitud->observacion = mb_strtoupper($request->observacion);
            if($request->fecha_lim != ''){
                $solicitud->fecha_limite = $request->fecha_lim;
            }
            $solicitud->total       = array_sum($request->subtotal);
            //$solicitud->total_rendido = 0; //variable de rendicion
            $solicitud->modalidad_id  = $request->modalidad;
            $solicitud->moneda_id     = $request->moneda;
            if($solicitud->modalidad_id == 3){
                $solicitud->frtipo_id  = $request->empresa_fr;
            }
            $solicitud->proyecto_id = $request->proyecto;
            // $solicitud->gestion_id  = session('gestion_id')[0];ñ
            if(isset($usuario->empleado_id)){
                $solicitud->empleado_id    = $usuario->empleado_id;
            }
            //$solicitud->prioridad_id = 2;
            //$solicitud->cambio_id   = $request->cambio_id;
            $solicitud->cambio_id   = TiposCambios::moneda($request->moneda)->get()->last()->id;

            $solicitud->save();

            //Datos Items
            $cantidades = $request->cantidad;
            $unidades = $request->unidad;
            $tipo_compra = $request->tipo_compra;
            $detalles = $request->detalle;
            $punitarios = $request->punitario;
            $cant_items = count($sub_totales);

            if($solicitud->usuario_id != Auth::user()->id){
                if(strpos(url()->previous(), 'solicitud-controller-editar') !== false){
                    $estado_modificado = 33;
                    $obs_texto = "SOLICITUD MODIFICADA POR REVISOR";
                }else{
                    $estado_modificado = 23;
                    $obs_texto = "SOLICITUD MODIFICADA POR AUTORIZADOR";
                }
            }else{
                $estado_modificado = 11;
                $obs_texto = "SOLICITUD MODIFICADA";
            }

            if(isset($request->controller)){
                $items_controller = ItemsController::where('solicitud_id', $solicitud->id)->get();
                if(count($items_controller) < 1){
                    $old_items = Items::where('solicitud_id', $solicitud->id)->get();
                    foreach($old_items as $old_item){
                        $item = new ItemsController();
                        $item->detalle = $old_item->detalle;
                        $item->cantidad = $old_item->cantidad;
                        $item->costo = $old_item->costo;
                        $item->numero_partida = $old_item->numero_partida;
                        $item->nombre_partida = $old_item->nombre_partida;
                        $item->unidad_id = $old_item->unidad_id;
                        $item->solicitud_id = $old_item->solicitud_id;
                        $item->tipo_id = $old_item->tipo_id;
                        $item->created_at = Carbon::now();
                        $item->updated_at = Carbon::now();
                        $item->save();
                    }
                }

                DB::table('estados_solicitud')->insert([
                    'observacion' => $obs_texto,
                    'usuario_id' => $usuario->id,
                    'solicitud_id' => $solicitud->id,
                    'estado_id' => $estado_modificado,
                    "created_at" =>  Carbon::now(),
                    "updated_at" => Carbon::now()
                ]);
            }else{
                if($usuario->rol_id == 6){
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD MODIFICADA POR TESORERIA',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 53,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                }elseif($usuario->rol_id == 3){
                    if($solicitud->usuario_id != Auth::user()->id){
                        DB::table('estados_solicitud')->insert([
                            'observacion' => 'SOLICITUD MODIFICADA POR AUTORIZADOR',
                            'usuario_id' => $usuario->id,
                            'solicitud_id' => $solicitud->id,
                            'estado_id' => 23,
                            "created_at" =>  Carbon::now(),
                            "updated_at" => Carbon::now()
                        ]);
                    }else{
                        DB::table('estados_solicitud')->insert([
                            'observacion' => 'SOLICITUD MODIFICADA',
                            'usuario_id' => $usuario->id,
                            'solicitud_id' => $solicitud->id,
                            'estado_id' => 11,
                            "created_at" =>  Carbon::now(),
                            "updated_at" => Carbon::now()
                        ]);
                    }
                }else{
                    DB::table('estados_solicitud')->insert([
                        'observacion' => $obs_texto,
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => $estado_modificado,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                }
            }

            DB::table('items')->where('solicitud_id', $solicitud->id)->delete();

            if($request->modalidad == 3 || $request->modalidad == 4){
                DB::table('item_rendiciones')->where('solicitud_id', $solicitud->id)->delete();
            }

            for($i = 0; $i < $cant_items; $i++){
                $item = new Items;
                $item->detalle          = mb_strtoupper($detalles[$i]);
                $item->cantidad         = $cantidades[$i];
                $item->costo            = $punitarios[$i];
                $item->numero_partida   = 0;
                $item->nombre_partida   = 0;
                $item->unidad_id        = $unidades[$i];
                $item->solicitud_id     = $solicitud->id;
                $item->tipo_id          = intval($tipo_compra[$i]);
                $item->save();

                if($request->modalidad == 3 || $request->modalidad == 4){
                    $nroFacturas = $request->nrofactura;
                    $fechaFacturas = $request->fechafactura;
                    $proyectos = $request->itemproyecto;
                    $proveedores = $request->itemproveedor;
                    $tipos = $request->itemtipo;

                    // Datos Items rendidos
                    $itemRen = new ItemsRendiciones();
                    $itemRen->cantidad          = $cantidades[$i];
                    $itemRen->costo             = $punitarios[$i];
                    $itemRen->fecha_factura     = $fechaFacturas[$i];
                    $itemRen->num_factura       = $nroFacturas[$i];
                    $itemRen->proyecto          = mb_strtoupper($proyectos[$i]);
                    $itemRen->proveedor         = mb_strtoupper($proveedores[$i]);
                    $itemRen->tipo              = $tipos[$i];
                    $itemRen->detalle           = mb_strtoupper($detalles[$i]);
                    $itemRen->direccion         = '';
                    $itemRen->item_id           = $item->id;
                    $itemRen->unidad_id         = $unidades[$i];
                    $itemRen->solicitud_id      = $solicitud->id;
                    $itemRen->tipo_id           = $tipo_compra[$i];
                    $itemRen->save();
                }
            }

            //Upload Files
            $array_documentos = array();
            $path = public_path().'/empresas/'.session('empresa_slug')."/".session('gestion')."/solicitud/";
            //echo $path;
            for($i = 1; $i <= 30; $i++){
                $doc_name = 'doc'.$i;
                if(isset($_FILES[$doc_name]['name']) && $_FILES[$doc_name]['name']!=''){
                    $str_name = trim($_FILES[$doc_name]['name']);
                    $file_type = $_FILES[$doc_name]['type'];
                    $str_name = $this->remover_acentos($str_name);
                    $str_name = strtolower($str_name);
                    $str_name = str_replace('-', '', $str_name);
                    $str_name = str_replace(' ', '_', $str_name);
                    $str_name = str_replace('__', '_', $str_name);
                    $str_name = str_replace('___ ', '_', $str_name);
                    $str_name = str_replace('____', '_', $str_name);
                    //$db_filename = $id_query."_".$str_name."_".time();
                    //***REVISAR NOMBRE DE ARCHIVO SUBIDO
                    $db_filename = $solicitud->id."_".time()."_".$str_name;
                    move_uploaded_file($_FILES[$doc_name]['tmp_name'], $path.$db_filename);
                    $documento = new Documentos;
                    $documento->direccion = $db_filename;
                    $documento->observaciones = '';
                    $documento->solicitud_id = $solicitud->id;
                    $documento->save();
                }
            }

            // $orden_type = "DESC";
            // $solicitudes = Solicitudes::whereIn('proyecto_id', $proyectos_ids)
            //     ->whereIn('gestion_id',session('gestion_id'))
            //     ->where('usuario_id', $usuario->id)
            //     ->orderBy('solicitudes.id', 'DESC')
            //     ->paginate(session('paginacion'));
            // $arrayItems = array();
            // foreach($solicitudes as $soli){
            //     $items = Items::where("solicitud_id", $soli->id)->get();
            //     foreach($items as $item){
            //         $arrayItems[$soli->id][]=[
            //             'unidad'    =>  $item->unidad_id,
            //             'detalle'   =>  $item->detalle,
            //             'cantidad'  =>  $item->cantidad,
            //             'costo'     =>  $item->costo,
            //             'subtotal'  =>  $item->subtotal
            //         ];
            //     }
            // }
        }catch(Exception $e){
            echo "Excepción :".$e->getMessage();
        }

        // foreach($solicitudes as $soli){
        //     $soli->estado = $this->getLastEstado($soli->id);
        // }
        Session::flash('success', 'Solicitud #'.$solicitud->numero.' modificada!!!');

        if ($usuario->global_view == 1) {
            return redirect()->route('global.en_curso');
        } else {
            return redirect()->route('solicitudes');
        }
    }

    public function cambiarProyectoSolicitud(Request $request)
    {
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($request->cambiar_solicitud_id);
        
        $solicitud->proyecto_id = $request->cambiar_proyecto_id;
        $solicitud->save();

        DB::table('estados_solicitud')->insert([
            'observacion' => 'GENERADOR DE COSTOS / ACTIVIDAD MODIFICADO',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 43,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        Session::flash('success', 'Solicitud #'.$solicitud->numero.' modificada!!!');
        return redirect()->action('SolicitudController@aprobar', ['tabs' => 'aprobador']);
    }

    public function cambiarAprobadorSolicitud(Request $request){
        $solicitud = Solicitudes::find($request->modalAprobar_solicitud_id);

        // Verificamos que el aprobador no vuelva a aprobar la solicitud
        $aprobador = DB::table('aprobador')
        ->where([
            ['aprobador_id', '=', auth()->user()->id],
            ['solicitud_id', '=', $solicitud->id]
        ])
        ->first();

        //dd($aprobador);

        // Si el usuario ya aprobó la solicitud, abortamos el proceso
        if($aprobador != null){
            abort('403', 'unauthorized-apr');
        }
        
        $usuario = auth()->user();

        $sw_activoFijo = false;
        $sw_sol_modificada = false;

        // Verificación sobre el cambio en el proyecto
        if($request->selectProyectos != 0 && $solicitud->proyecto_id != $request->selectProyectos){
            // Cambio en el proyecto
            $solicitud->proyecto_id = $request->selectProyectos;
            $solicitud->save();
            $sw_sol_modificada = true;
        }

        // Verificación sobre el cambio en el tipo de compra
        $solicitud = Solicitudes::find($request->modalAprobar_solicitud_id);
        foreach($request->tipocompra as $key => $tipo){
            if($solicitud->modalidad_id == 3){
                $item = ItemsRendiciones::find($key);
            }else{
                $item = Items::find($key);
            }
            if($item->tipo_id != $tipo){
                // Cambio en el tipo de compra
                $item->tipo_id = $tipo;
                $item->save();
                $sw_sol_modificada = true;

                // Activo Fijo
                if(in_array($tipo, [1, 7])){
                    $sw_activoFijo = true;
                }
            }
        }

        // dd($request->all(), $sw_sol_modificada, $sw_activoFijo);

        // Verifica si la solicitud fue modificada
        if($sw_sol_modificada){
            // Verifica si la solicitud tiene un nuevo AF
            if($sw_activoFijo){
                DB::table('estados_solicitud')->insert([
                    'observacion' => 'SOLICITUD MODIFICADA POR APROBADOR - AUT-AF',
                    'usuario_id' => $usuario->id,
                    'solicitud_id' => $solicitud->id,
                    'estado_id' => 24,
                    "created_at" =>  Carbon::now(),
                    "updated_at" => Carbon::now()
                ]);
            }else{
                DB::table('estados_solicitud')->insert([
                    'observacion' => 'SOLICITUD MODIFICADA POR APROBADOR',
                    'usuario_id' => $usuario->id,
                    'solicitud_id' => $solicitud->id,
                    'estado_id' => 43,
                    "created_at" =>  Carbon::now(),
                    "updated_at" => Carbon::now()
                ]);
            }
        }

        // Verifica el ROL == ADMIN => rjofre
        if($usuario->rol_id == 8){
            // Aprobación de la solciitud al 100%
            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD APROBADA',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 40,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }else{
            // Verifica si el usuario tiene el atributo de Aprobador parcial
            if($usuario->apr_parcial || $usuario->rol_id == 5){
                $total_solicitud = $solicitud->total;
                $monto_minimo = PHP_INT_MAX;
                $monto_maximo = 0;
                $limite_niveles = DB::table('niveles_aprobacion')
                    ->get();
                $factor = 1;
                
                if($solicitud->moneda_id != 1){ // BOLIVIANOS
                    $factor = $solicitud->tipo_cambio->cambio;
                }

                foreach($limite_niveles as $limite_nivel){
                    if($total_solicitud <= ($limite_nivel->hasta / $factor) &&
                        $total_solicitud >= ($limite_nivel->desde / $factor)){
                        $nivel_id = $limite_nivel->id;
                    }

                    if(($limite_nivel->desde / $factor) < $monto_minimo){
                        $monto_minimo = $limite_nivel->desde / $factor;
                    }
                    
                    if(($limite_nivel->hasta / $factor) > $monto_maximo){
                        $monto_maximo = $limite_nivel->hasta / $factor;
                    }
                }

                if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
                    $porcentaje = DB::table('nivel_aprobador')
                        ->select('porcentaje')
                        ->where('aprobador_id', $usuario->id)
                        ->where('nivel_id', $nivel_id)
                        ->first()->porcentaje;

                    DB::table('aprobador')->insert([
                        'monto_aprobacion' => $porcentaje,
                        'aprobador_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);

                    $monto_aprobaciones = DB::table('aprobador')
                        ->select('monto_aprobacion')
                        ->where('solicitud_id', $solicitud->id)
                        ->get();

                    $total_aprobacion = 0;

                    foreach($monto_aprobaciones as $monto_apr){
                        $total_aprobacion += $monto_apr->monto_aprobacion;
                    }

                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD APROBADA PARCIALMENTE',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        'estado_id' => 44,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);

                    if($total_aprobacion >=100){
                        DB::table('estados_solicitud')->insert([
                            'observacion' => 'SOLICITUD APROBADA',
                            'usuario_id' => $usuario->id,
                            'solicitud_id' => $solicitud->id,
                            'estado_id' => 40,
                            "created_at" =>  Carbon::now(),
                            "updated_at" => Carbon::now()
                        ]);
                    }

                }elseif($total_solicitud < $monto_minimo){
                    DB::table('aprobador')->insert([
                        'monto_aprobacion' => 100,
                        'aprobador_id' => $usuario->id,
                        'solicitud_id' => $solicitud->id,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                }

                // $monto_aprobaciones = DB::table('aprobador')
                //     ->select('monto_aprobacion')
                //     ->where('solicitud_id', $solicitud->id)
                //     ->get();
            }else{
                abort('403', 'unauthorized-apr');
            }
        }
        
        if ($usuario->global_view == 0) {
            return redirect()->action('SolicitudController@aprobar', ['tabs' => 'aprobador']);
        } else {
            return redirect()->route('global.aprobar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "SolicitudController@destroy";
        exit();
    }

    public function guardarArchivo(){
        $usuario = auth()->user();
        $doc_name = 'doc_up';
        $id_solicitud   = $_POST['id_sol_subir'];
        $str_name = trim($_FILES[$doc_name]['name']);
        $file_type = $_FILES[$doc_name]['type'];
        $str_name = $this->remover_acentos($str_name);
        $str_name = strtolower($str_name);
        $str_name = str_replace('-', '', $str_name);
        $str_name = str_replace(' ', '_', $str_name);
        $str_name = str_replace('__', '_', $str_name);
        $str_name = str_replace('___ ', '_', $str_name);
        $str_name = str_replace('____', '_', $str_name);
        $db_filename = $id_solicitud."_".$str_name."_".time();
        $db_filename = $id_solicitud."_".time()."_".$str_name;

        $solicitud = Solicitudes::find($id_solicitud);

        $path = public_path().'/empresas/'.$solicitud->proyecto->empresa->slug."/".$solicitud->gestion->gestion."/solicitud/";
        $db_filename = $id_solicitud."_".time()."_".$str_name;
        move_uploaded_file($_FILES[$doc_name]['tmp_name'], $path.$db_filename);
        $documento = new Documentos;
        $documento->direccion = $db_filename;
        $documento->observaciones = '';
        $documento->solicitud_id = $id_solicitud;
        $documento->save();

        $array_proyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();

        if($usuario->rol->nombre == "ADMINISTRADOR"){
            $solicitudes = Solicitudes::whereIn('proyecto_id', $array_proyectos)
                ->whereIn('gestion_id', session('gestion_id'))
                ->get();
        }else{
            $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                $query->where('user_id', $usuario->id);
            })->pluck('id')->toArray();

            if (\Auth::user()->global_view == 0) {
                $solicitudes = Solicitudes::whereIn('proyecto_id', $proyectos)
                    ->whereIn('gestion_id', session('gestion_id'))
                    ->where('usuario_id', $usuario->id)
                    ->get();
            } else {
                $solicitudes = Solicitudes::whereIn('proyecto_id', $proyectos)
                    ->where('gestion_id', $solicitud->gestion_id)
                    ->where('usuario_id', $usuario->id)
                    ->get();
            }
        }
        
        $var = ['SOL', 'SOL-M','AUT', 'REV', 'APR', 'TES'];
        if(isset($_GET['t'])){
            switch($_GET['t']){
                case 'observadas':
                    $var = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];
                    break;
                case 'rechazadas':
                    $var = ['AUT-R', 'REV-R', 'APR-R', 'TES-R'];
                    break;
                case 'rendidas':
                    $var = ['REN'];
                    break;
                case 'finalizadas':
                    $var = ['FIN'];
                    break;
            }
        }

        $solicitud_ids = array();
        foreach($solicitudes as $solicitud){
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if(in_array($solicitud->estadoCodigo, $var)){
                $solicitud_ids [] = $solicitud->id;
            }
        }

        $solicitudes = Solicitudes::whereIn('id', $solicitud_ids)
            ->orderBy('solicitudes.id', 'DESC')
            ->paginate(session('paginacion'));

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }
        $orden = array();

        if (\Auth::user()->global_view == 0) {
            return redirect()->route('solicitudes');
        } else {
            return redirect()->route('global.en_curso');
        }
    }

    //Autorizacion
    public function autorizar(Request $request, $search = '*'){
        // dd($request->all());
        $extra_query = "";
        $extra_query2 = "";
        //dd($request);
        if(isset($request)){
            switch(intVal($request->selectOpcion)){
                case 1:
                    //Numero
                    if($request->numero != ''){
                        $extra_query = "numero = ".$request->numero;
                        $extra_query2 = "numero = ".$request->numero;
                    }
                    break;
                case 2:
                    //Monto
                    if($request->numero != ''){
                        $extra_query = "total = ".$request->numero;
                        $extra_query2 = "total = ".$request->numero;
                    }
                    break;
                case 3:
                    //Proyecto
                    if($request->buscar != ''){
                        $proy_query = Proyectos::where('nombre', 'LIKE', "%$request->buscar%")
                            ->pluck('id')
                            ->toArray();
                        if(count($proy_query) > 0){
                            $extra_query = "proyecto_id IN (".implode(', ', $proy_query).")";
                            $extra_query2 = "proyecto_id = ".implode(',', $proy_query);
                        }
                    }
                    break;
                case 4:
                    //Fecha
                    if (isset($request->fechaIni)) {
                        if (isset($request->fechaFin)) {
                            $extra_query2 = "created_at,".$request->fechaIni.",".$request->fechaFin;
                        } else {
                            $extra_query2 = "created_at,".$request->fechaIni.",".$request->fechaIni;
                        }
                    }
                    break;
                case 5:
                    //Solicitante
                    if($request->buscar != ''){
                        $usu_query = User::where("nombre", "LIKE", "'%".$request->buscar."%'")
                            ->pluck('id')
                            ->toArray();
                        if(count($usu_query) > 0){
                            $extra_query = "usuario_id IN (".implode(', ', $usu_query).")";
                            $extra_query2 = "usuario_id = ".implode(',', $usu_query);
                        }
                    }
                    break;
                case 6:
                    //Desembolso
                    if($request->buscar != ''){
                        $extra_query = "desembolso LIKE '%".$request->buscar."%'";
                        $extra_query2 = "desembolso = ".$request->buscar;
                    }
                    break;
                case 7:
                    //Referencia
                    if($request->buscar != ''){
                        $extra_query = "referencia LIKE '%".$request->buscar."%'";
                        $extra_query2 = "referencia = ".$request->buscar;
                    }
                    break;
                case 8:
                    //Observacion
                    if($request->buscar != ''){
                        $extra_query = "observacion LIKE '%".$request->buscar."%'";
                        $extra_query2 = "observacion = ".$request->buscar;
                    }
                    break;
            }
        }
        $usuario = auth()->user();

        //dd($extra_query);
        $idSolicitudes = $this->seccionSolicitud('autorizacion-autorizar', $extra_query2);
//        dd($idSolicitudes);

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            // ->whereIn('id', $solicitudes_ids)
            ->whereIn('id', $idSolicitudes)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->costo * $array_item->cantidad
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        
        if ($usuario->global_view != 1) {
            return view('solicitudes.autorizador.index')
                ->withSolicitudes($solicitudes)
                ->withUsuario($usuario)
                ->withItems($items);
        } else {
            return redirect()->route('global.autorizar');
        }
    }

    public function buscadorAutorizar(Request $request){
        $add_query = "";
        switch ($request->campo) {
            case 0:
                $add_query = " AND (";
                if(is_numeric($request->buscar)){
                    $add_query .= " id_solicitud = ".$request->buscar. " OR ";
                }

                //Nombre de Usuario
                $str_ids_usuarios = "";
                $cont_ids_usu = 0;
                $usuarios = User::where('nombre', 'LIKE', '%'.$request->buscar.'%')->orWhere('ap', 'LIKE', '%'.$request->buscar.'%')->orWhere('am', 'LIKE', '%'.$request->buscar.'%')->get();
                foreach($usuarios as $usuario){
                    $str_ids_usuarios .= $usuario->id.",";
                    $cont_ids_usu++;
                }
                if(substr($str_ids_usuarios, -1) == ','){
                    $str_ids_usuarios = substr($str_ids_usuarios, 0, strlen($str_ids_usuarios)-1);
                }

                $add_query .= " total LIKE '%".$request->buscar."%'";
                if($str_ids_usuarios != ''){
                    $add_query .= " OR id_usuario IN (".$str_ids_usuarios.")";
                }
                $add_query .= " OR pro.nombre LIKE '%".$request->buscar."%'";
                $add_query .= " OR created_ad LIKE '%".$request->buscar."%'";
                $add_query .= " OR hora_solicitud LIKE '%".$request->buscar."%'";
                $add_query .= " OR desembolso LIKE '%".$request->buscar."%'";
                $add_query .= " OR tipo_cambio LIKE '%".$request->buscar."%'";
                $add_query .= " OR tipo_solicitud LIKE '%".$request->buscar."%'";
                $add_query .= " OR moneda LIKE '%".$request->buscar."%'";
                //$add_query .= " OR referencia LIKE '%".$request->buscar."%'";
                $add_query .= " OR observacion LIKE '%".$request->buscar."%')";

                break;
            case 1:
                $add_query .= " AND id_solicitud = ".$request->buscar;
                break;
            case 2:
                $add_query .= " AND total LIKE '%".$request->buscar."%'";
                break;
            case 3:
                $add_query .= " AND pro.nombre LIKE '%".$request->buscar."%'";
                break;
            case 4:
                $add_query .= " AND created_at LIKE '%".$request->buscar."%'";
                break;
            case 5:
                $add_query .= " AND hora_solicitud LIKE '%".$request->buscar."%'";
                break;
            case 6:
                //Nombre de Usuario
                $str_ids_usuarios = "";
                $cont_ids_usu = 0;
                $usuarios = User::where('nombre', 'LIKE', '%'.$request->buscar.'%')->orWhere('ap', 'LIKE', '%'.$request->buscar.'%')->orWhere('am', 'LIKE', '%'.$request->buscar.'%')->get();
                foreach($usuarios as $usuario){
                    $str_ids_usuarios .= $usuario->id.",";
                    $cont_ids_usu++;
                }
                if(substr($str_ids_usuarios, -1) == ','){
                    $str_ids_usuarios = substr($str_ids_usuarios, 0, strlen($str_ids_usuarios)-1);
                }
                if($str_ids_usuarios != ''){
                    $add_query .= " AND id_usuario IN (".$str_ids_usuarios.")";
                }
                break;
            case 7:
                $add_query .= " AND desembolso LIKE '%".$request->buscar."%'";
                break;
            case 8:
                $add_query .= " AND tipo_cambio LIKE '%".$request->buscar."%'";
                break;
            case 9:
                $add_query .= " AND tipo_solicitud LIKE '%".$request->buscar."%'";
                break;
            case 10:
                $add_query .= " AND moneda = ".$request->buscar;
                break;
            case 11:
                //$add_query .= " AND referencia LIKE '%".$request->buscar."%'";
                break;
            case 12:
                $add_query .= " AND observacion LIKE '%".$request->buscar."%'";
                break;
        }
        //echo $add_query;
        $data['array_addquery'] = $add_query;
        return view('solicitudes.autorizador.index', $data);
    }

    public function guardarAutorizacion(){
        //echo "SolicitudController@guardarAutorizacion";
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));

        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 22,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $items = Items::where('solicitud_id', $solicitud->id)->get();
        $sw_af = false;
        foreach($items as $item){
            if($item->tipo_id == 1 || $item->tipo_id == 7){
                $sw_af = true;
            }
        }

        if($sw_af){
            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD AUTORIZADA - AF',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 24,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }else{
            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD AUTORIZADA',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 20,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        if ($usuario->global_view == 0) {
            return redirect()->action('SolicitudController@autorizar', ['tabs' => 'autorizador']);
        } else {
            return redirect()->route('global.autorizar');
        }
    }

    //Controller
    public function controller(Request $request){
        $usuario = auth()->user();

        switch($request->modalidad){
            case '1':
                $idSolicitudes = $this->seccionSolicitud('revisor-revisor_sr', "");
                break;
            case '2':
                $idSolicitudes = $this->seccionSolicitud('revisor-revisor_gd', "");
                break;
            case '3':
                $idSolicitudes = $this->seccionSolicitud('revisor-revisor_fra', "");
                break;
            case '4':
                $idSolicitudes = $this->seccionSolicitud('revisor-revisor_repo', "");
                break;
            // default:
            //     $idSolicitudes = array();
        }

        switch($request->tipo_fr){
            case '1':
                $idSolicitudes = $this->seccionFrSolicitud('revisor-revisor_cc', "");
                break;
            case '2':
                $idSolicitudes = $this->seccionFrSolicitud('revisor-revisor_fra', "");
                break;
            case '3':
                $idSolicitudes = $this->seccionFrSolicitud('revisor-revisor_fro', "");
                break;
            case '4':
                $idSolicitudes = $this->seccionFrSolicitud('revisor-revisor_frc', "");
                break;
            case '5':
                $idSolicitudes = $this->seccionFrSolicitud('revisor-revisor_frm', "");
                break;
            // default:
            //     $idSolicitudes = array();
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            // ->whereIn('id', $solicitudes_ids)
            ->whereIn('id', $idSolicitudes)
            ->whereNotIn('usuario_id', [107, 108])
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->costo * $array_item->cantidad
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.controller.index')
            ->withSolicitudes($solicitudes)
            ->withItems($items)
            ->withUsuario($usuario)
            ->withModalidad($request->modalidad);
    }

    public function buscadorController(Request $request){
        //dd($request);
        $usuario = auth()->user();
        $add_query = "";
        switch ($request->selectOpcion) {
            case 0:
                if($request->buscar != ''){
                    $add_query = " AND (";
                    if(is_numeric($request->buscar)){
                        $add_query .= " numero = ".$request->buscar. " OR ";
                        $add_query .= " total = ".$request->buscar. " OR ";
                    }

                    //Nombre de Usuario
                    $str_ids_usuarios = "";
                    $cont_ids_usu = 0;
                    $usuarios = User::where('nombre', 'LIKE', '%'.$request->buscar.'%')->get();
                    foreach($usuarios as $usuario){
                        $str_ids_usuarios .= $usuario->id.",";
                        $cont_ids_usu++;
                    }
                    if(substr($str_ids_usuarios, -1) == ','){
                        $str_ids_usuarios = substr($str_ids_usuarios, 0, strlen($str_ids_usuarios)-1);
                    }

                    //$add_query .= " total = '%".$request->buscar."%'";
                    if($str_ids_usuarios != ''){
                        if(strlen($add_query) > 6){
                            $add_query .= " OR ";
                        }
                        $add_query .= "usuario_id IN (".$str_ids_usuarios.")";
                    }

                    $proyectos = Proyectos::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->where('empresa_id', session('empresa'))
                        ->pluck('id')
                        ->toArray();
                    if(count($proyectos)>0){
                        if(strlen($add_query) > 6){
                            $add_query .= " OR ";
                        }
                        $add_query .= "proyecto_id IN (".implode(', ',$proyectos).")";
                    }

                    if(strlen($add_query) > 6){
                        //$add_query .= " OR ";
                    }

                    $monedas = Monedas::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->pluck('id')
                        ->toArray();

                    $modalidades = Modalidades::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->pluck('id')
                        ->toArray();

                    $add_query .= "created_at LIKE '%".$request->buscar."%'";
                    $add_query .= " OR desembolso LIKE '%".$request->buscar."%'";

                    if(count($monedas) > 0){
                        $add_query .= " OR moneda_id IN (".implode(',', $monedas).")";
                    }

                    if(count($modalidades) > 0){
                        $add_query .= " OR modalidad_id IN (".implode(',', $modalidades).")";
                    }

                    $add_query .= " OR referencia LIKE '%".$request->buscar."%'";
                    $add_query .= " OR observacion LIKE '%".$request->buscar."%')";
                }
                break;
            case 1:
                $add_query .= " AND numero = ".$request->numero;
                break;
            case 2:
                $add_query .= " AND total LIKE '%".$request->numero."%'";
                break;
            case 3:
                $proyectos = Proyectos::select('id')
                    ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                    ->where('empresa_id', session('empresa'))
                    ->pluck('id')
                    ->toArray();
                if(count($proyectos)>0){
                    $add_query .= " AND proyecto_id IN (".implode(', ',$proyectos).")";
                }
                break;
            case 4:
                $add_query .= " AND created_at LIKE '%".$request->buscar."%'";
                break;
            case 5:
                //Nombre de Usuario
                $str_ids_usuarios = "";
                $cont_ids_usu = 0;
                $usuarios = User::where('nombre', 'LIKE', '%'.$request->buscar.'%')->get();
                foreach($usuarios as $usuario){
                    $str_ids_usuarios .= $usuario->id.",";
                    $cont_ids_usu++;
                }
                if(substr($str_ids_usuarios, -1) == ','){
                    $str_ids_usuarios = substr($str_ids_usuarios, 0, strlen($str_ids_usuarios)-1);
                }
                if($str_ids_usuarios != ''){
                    $add_query .= " AND usuario_id IN (".$str_ids_usuarios.")";
                }
                break;
            case 6:
                $add_query .= " AND desembolso LIKE '%".$request->buscar."%'";
                break;
            case 7:
                $add_query .= " AND referencia LIKE '%".$request->buscar."%'";
                break;
            case 8:
                $add_query .= " AND observacion LIKE '%".$request->buscar."%'";
                break;
        }
        $data['array_addquery'] = $add_query;
        $proyectos_ids = Proyectos::where('empresa_id', session('empresa'))
            ->select('id')
            ->pluck('id')
            ->toArray();
        $query = " WHERE gestion_id IN (".implode(',', session('gestion_id')).") AND proyecto_id IN (".implode(',', $proyectos_ids).") ".$add_query;
        $sol_ids = DB::select('SELECT id FROM solicitudes'.$query.' ORDER BY created_at DESC');
        $solicitudes_ids = array();
        foreach($sol_ids as $sol_id){
            if(in_array($this->getLastEstadoCodigo($sol_id->id), ['AUT', 'AUT-M', 'AUT-AF', 'REV-M', 'REV-O', 'REV-M'])){
                $solicitudes_ids[] = $sol_id->id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitudes_ids)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->costo * $array_item->cantidad
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        
        $var_busqueda = ['tabs' => 'revisor', 'id_empresa' => session('empresa')];
        if(isset($request->moda_search)){
            echo "si";
            $var_busqueda['modalidad'] = $request->moda_search;
        }
        if(isset($request->tipo_search)){
            $var_busqueda['tipo_fr'] = $request->tipo_search;
        }
        
        return view('solicitudes.controller.index', $var_busqueda)
            ->withModalidad($request->moda_search)
            ->withSolicitudes($solicitudes)
            ->withItems($items)
            ->withUsuario($usuario);
    }

    //aprobacion controller
    public function guardarAprobacionController(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 32,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD AUTORIZADA REVISOR',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 30,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        if (\Auth::user()->global_view != 1)
        {
            return redirect()->action('SolicitudController@controller', ['tabs' => 'revisor', 'tipo_fr' => Auth::user()->revisa_fr->first()->id]);
        }
        else
        {
            return redirect()->back();
        }
    }

    //modificar controller
    public function modificarController($solicitud_id, $controller=false){
        if(!$this->canEditRevSolicitud($solicitud_id)){
            abort('403', 'edit');
        }

        $usuario = auth()->user();
        $solicitud = Solicitudes::find($solicitud_id);

        $modalidades = Modalidades::where('oculto', false)->get();
        $monedas = Monedas::all();
        //$tipo_cambio = TiposCambios::all()->last()->toArray();
        $tipo_cambio = TiposCambios::moneda(2)->get()->last()->toArray();
        $tipo_cambio_euro = TiposCambios::moneda(3)->get()->last()->toArray();
        $array_ids = array();
        if($usuario->rol_id == 1 || $usuario->rol_id >=3){
            $proyectos = Proyectos::where('empresa_id', session('empresa'))
                ->get();
        }else{
            $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                $query->where('user_id', $usuario->id);
            })->get();
        }

        $array_proyectos = Proyectos::where('id',$solicitud->proyecto_id)->get();
        foreach($array_proyectos as $proyecto){
            switch($proyecto->nivel){
                case 2:
                    $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
                case 3:
                    $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
            }
        }
        $unidades = Unidades::all()->pluck('codigo', 'id');
        $tipo_compra = ItemsTipos::all()->pluck('descripcion', 'id');

        $documentos = Documentos::where('solicitud_id', $solicitud_id)
            ->get();

        foreach($documentos as $documento){
            $documento->empresa_slug = "";
            $documento->empresa_slug = $documento->solicitud->proyecto->empresa->slug;
        }

        return view('solicitudes.controller.editar')
            ->withCambioEuro($tipo_cambio_euro)
            ->withProyectos($array_proyectos)
            ->withDocumentos($documentos)
            ->withModalidades($modalidades)
            ->withTipocompra($tipo_compra)
            ->withController($controller)
            ->withDocumentos($documentos)
            ->withSolicitud($solicitud)
            ->withCambio($tipo_cambio)
            ->withUnidades($unidades)
            ->withMonedas($monedas)
            ->withDate(Carbon::now()->format('Y-m-d'));
    }

    public function controllerSolicitudPendiente(){
        $usuario = auth()->user();
        $sol_id = $_GET['pendiente'];
        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD PUESTA EN PENDIENTES POR REVISOR',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $sol_id,
            'estado_id' => 34,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        return back();
    }

    //Aprobacion
    public function aprobar(Request $request){
        // Usuario que inició sesión
        if(isset($_GET['empresa_id'])){
            session()->forget('empresa');
            session::set('empresa' , $_GET['empresa_id']);
        }
        $usuario = auth()->user();
        $extra_query = "";
        $extra_query2 = "";
        //dd($request);
        if(isset($request)){
            switch(intVal($request->selectOpcion)){
                case 1:
                    //Numero
                    if($request->numero != ''){
                        $extra_query = "numero = ".$request->numero;
                        $extra_query2 = "numero = ".$request->numero;
                    }
                    break;
                case 2:
                    //Monto
                    if($request->numero != ''){
                        $extra_query = "total = ".$request->numero;
                        $extra_query2 = "total = ".$request->numero;
                    }
                    break;
                case 3:
                    //Proyecto
                    if($request->buscar != ''){
                        $proy_query = Proyectos::where('nombre', 'LIKE', "%$request->buscar%")
                            ->pluck('id')
                            ->toArray();
                        if(count($proy_query) > 0){
                            $extra_query = "proyecto_id IN (".implode(', ', $proy_query).")";
                            $extra_query2 = "proyecto_id = ".implode(',', $proy_query);
                        }
                    }
                    break;
                case 4:
                    //Fecha
                    if(isset($request->fechaIni)){
                        if(isset($request->fechaFin)){
                            $extra_query = "created_at BETWEEN ".$request->fechaIni." AND ".$request->fechaFin;
                            $extra_query2 = "created_at = ".$request->fechaIni.",".$request->fechaFin;
                        }else{
                            $extra_query = "created_at LIKE ".$request->fechaIni;
                            $extra_query2 = "created_at = ".$request->fechaIni;
                        }
                    }
                    break;
                case 5:
                    //Solicitante
                    if($request->buscar != ''){
                        $usu_query = User::where("nombre", "LIKE", "%$request->buscar%")
                            ->pluck('id')
                            ->toArray();
                        if(count($usu_query) > 0){
                            $extra_query = "usuario_id IN (".implode(', ', $usu_query).")";
                            $extra_query2 = "usuario_id = ".implode(',', $usu_query);
                        }
                    }
                    break;
                case 6:
                    //Desembolso
                    if($request->buscar != ''){
                        $extra_query = "desembolso LIKE '%".$request->buscar."%'";
                        $extra_query2 = "desembolso = ".$request->buscar;
                    }
                    break;
                case 7:
                    //Referencia
                    if($request->buscar != ''){
                        $extra_query = "referencia LIKE '%".$request->buscar."%'";
                        $extra_query2 = "referencia = ".$request->buscar;
                    }
                    break;
                case 8:
                    //Observacion
                    if($request->buscar != ''){
                        $extra_query = "observacion LIKE '%".$request->buscar."%'";
                        $extra_query2 = "observacion = ".$request->buscar;
                    }
                    break;
            }
        }
        
        //dd($extra_query2);

        $idSolicitudes = $this->seccionSolicitud('aprobador_parcial-aprobador', $extra_query2);

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $idSolicitudes)
            ->get();

        // Prepara la solicitud para ser mostrada (Estado, Estado código y Proyecto)
        // **** SE DEBE FACTORIZAR PARA TODOS **** //
        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        // Ordenación de las solicitudes con respecto al estado
        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Genera un array con los items de las solicitudes
        // **** SE DEBE FACTORIZAR PARA TODOS **** //
        // $items = array();
        // foreach($solicitudes as $solicitud){
        //     $array_items = Items::where('solicitud_id', $solicitud->id)->get();
        //     $items_aux = array();
        //     foreach($array_items as $array_item){
        //         $items_aux[] = [
        //             'solicitud_id' => $solicitud->id,
        //             'tipo' => $array_item->unidad->descripcion,
        //             'descripcion' => $array_item->detalle,
        //             'cantidad' => $array_item->cantidad,
        //             'costo' => $array_item->costo,
        //             'subtotal' => $array_item->subtotal
        //         ];

        //     }
        //     $items[$solicitud->id] = $items_aux;
        // }

        // Envía las Solicitudes, usuario que inició sesión y los items de cada solicitud
        return view('solicitudes.aprobador.index')
            ->withSolicitudes($solicitudes)
            ->withUsuario($usuario);
            // ->withItems($items);
    }

    public function guardarAprobacion(){
        //echo "SolicitudController@guardarAprobacion";
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }
        
        if($total_aprobacion >= 100){
            if($usuario->rol_id != 8){
                DB::table('estados_solicitud')->insert([
                    'observacion' => 'SOLICITUD APROBADA PARCIALMENTE',
                    'usuario_id' => $usuario->id,
                    'solicitud_id' => $solicitud->id,
                    'estado_id' => 44,
                    "created_at" =>  Carbon::now(),
                    "updated_at" => Carbon::now()
                ]);
            }

            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD APROBADA',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 40,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }else{
            DB::table('estados_solicitud')->insert([
                'observacion' => 'SOLICITUD APROBADA PARCIALMENTE',
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 44,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }
        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }
        if($usuario->rol_id == 8){
            return redirect()->action('SolicitudController@solicitudesAdmin', ['page' => $page, 'query' => $query]);
        }else{
            return redirect()->action('SolicitudController@aprobar', ['tabs' => 'aprobador', 'page' => $page, 'query' => $query]);
        }

    }

    public function guardarAprobacionCreada(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD AUTORIZADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 20,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@adminCreadas', ['page' => $page, 'query' => $query]);
    }

    public function guardarAprobacionAutorizada(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@adminAutorizadas', ['page' => $page, 'query' => $query]);
    }

    public function guardarAprobacionPendiente(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }
        return redirect()->action('SolicitudController@adminPendientes', ['page' => $page, 'query' => $query]);
    }

    public function guardarAprobacionPendiente2(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }
        return redirect()->action('SolicitudController@adminPendientes2', ['page' => $page, 'query' => $query]);
    }

    public function guardarAprobacionPendiente3(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }
        return redirect()->action('SolicitudController@adminPendientes3', ['page' => $page, 'query' => $query]);
    }

    public function guardarAprobacionPendiente4(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }
        return redirect()->action('SolicitudController@adminPendientes4', ['page' => $page, 'query' => $query]);
    }

    public function guardarAprobacionPendiente5(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }
        return redirect()->action('SolicitudController@adminPendientes5', ['page' => $page, 'query' => $query]);
    }

    public function guardarAprobacionTransferencia(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 42,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $total_solicitud = 0;
        foreach($solicitud->items as $item){
            $total_solicitud += $item->costo*$item->cantidad;
        }
        $limite_niveles = DB::table('niveles_aprobacion')
            ->get();
        foreach($limite_niveles as $limite_nivel){
            if($total_solicitud <= $limite_nivel->hasta && $total_solicitud >= $limite_nivel->desde){
                $nivel_id = $limite_nivel->id;
            }
            if($limite_nivel->desde < $monto_minimo){
                $monto_minimo = $limite_nivel->desde;
            }
            if($limite_nivel->hasta > $monto_maximo){
                $monto_maximo = $limite_nivel->hasta;
            }
        }

        if($total_solicitud >= $monto_minimo && $total_solicitud <= $monto_maximo){
            $porcentaje = DB::table('nivel_aprobador')
                ->select('porcentaje')
                ->where('aprobador_id', $usuario->id)
                ->where('nivel_id', $nivel_id)
                ->first()->porcentaje;

            //buscar aprobador igual en solicitud para no volver a aprobar con el mismo usuario
            DB::table('aprobador')->insert([
                'monto_aprobacion' => $porcentaje,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }elseif($total_solicitud < $monto_minimo){
            DB::table('aprobador')->insert([
                'monto_aprobacion' => 100,
                'aprobador_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        $monto_aprobaciones = DB::table('aprobador')
            ->select('monto_aprobacion')
            ->where('solicitud_id', $solicitud->id)
            ->get();

        $total_aprobacion = 0;
        foreach($monto_aprobaciones as $monto_apr){
            $total_aprobacion += $monto_apr->monto_aprobacion;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }else{
            $page = null;
        }
        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
        }
        return redirect()->action('SolicitudController@adminTransferencias', ['page' => $page, 'query' => $query]);
    }

    public function adminGuardarRechazo(Request $request){
        $usuario = auth()->user();
        $solicitud = $request->id_solicitud;
        $observacion = mb_strtoupper(trim($request->motivo));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion == ''){
            $observacion = "SOLICITUD RECHAZADA POR APROBADOR";
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => $observacion,
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud,
            'estado_id' => 41,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
        
        //$solicitud->save();
        if(isset($request->page)){
            $page = $request->page;
        }else{
            $page = null;
        }
        if(isset($request->consulta)){
            $query = $request->consulta;
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@solicitudesAdmin', ['page' => $page, 'query' => $query]);
    }

    public function adminGuardarRechazoCreada(Request $request){
        //echo "SolicitudController@guardarAprobacion";
        $usuario = auth()->user();
        $solicitud = $request->id_solicitud;
        $observacion = mb_strtoupper(trim($request->motivo));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion == ''){
            $observacion = "SOLICITUD RECHAZADA POR APROBADOR";
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => $observacion,
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud,
            'estado_id' => 41,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($request->page)){
            $page = $request->page;
        }else{
            $page = null;
        }
        if(isset($request->consulta)){
            $query = $request->consulta;
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@adminCreadas', ['page' => $page, 'query' => $query]);
    }

    public function adminGuardarRechazoRevisada(Request $request){
        //echo "SolicitudController@guardarAprobacion";
        $usuario = auth()->user();
        $solicitud = $request->id_solicitud;
        $observacion = mb_strtoupper(trim($request->motivo));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion == ''){
            $observacion = "SOLICITUD RECHAZADA POR APROBADOR";
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => $observacion,
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud,
            'estado_id' => 41,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($request->page)){
            $page = $request->page;
        }else{
            $page = null;
        }
        if(isset($request->consulta)){
            $query = $request->consulta;
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@adminAutorizadas', ['page' => $page, 'query' => $query]);
    }

    public function adminGuardarObservacion(Request $request){
        //echo "SolicitudController@guardarAprobacion";
        $usuario = auth()->user();
        $solicitud = $request->id_solicitud;
        $observacion = mb_strtoupper(trim($request->motivo));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion == ''){
            $observacion = "SOLICITUD OBSERVADA POR APROBADOR";
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => $observacion,
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud,
            'estado_id' => 42,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($request->page)){
            $page = $request->page;
        }else{
            $page = null;
        }
        if(isset($request->consulta)){
            $query = $request->consulta;
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@solicitudesAdmin', ['page' => $page, 'query' => $query]);
    }

    public function adminGuardarObservacionCreada(Request $request){
        //echo "SolicitudController@guardarAprobacion";
        $usuario = auth()->user();
        $solicitud = $request->id_solicitud;
        $observacion = mb_strtoupper(trim($request->motivo));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion == ''){
            $observacion = "SOLICITUD OBSERVADA POR APROBADOR";
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => $observacion,
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud,
            'estado_id' => 42,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($request->page)){
            $page = $request->page;
        }else{
            $page = null;
        }
        if(isset($request->consulta)){
            $query = $request->consulta;
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@adminCreadas', ['page' => $page, 'query' => $query]);
    }

    public function adminGuardarObservacionRevisada(Request $request){
        //echo "SolicitudController@guardarAprobacion";
        $usuario = auth()->user();
        $solicitud = $request->id_solicitud;
        $observacion = mb_strtoupper(trim($request->motivo));
        $monto_minimo = PHP_INT_MAX;
        $monto_maximo = 0;
        if($observacion == ''){
            $observacion = "SOLICITUD OBSERVADA POR APROBADOR";
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => $observacion,
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud,
            'estado_id' => 42,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        //$solicitud->save();
        if(isset($request->page)){
            $page = $request->page;
        }else{
            $page = null;
        }
        if(isset($request->consulta)){
            $query = $request->consulta;
        }else{
            $query = "";
        }

        return redirect()->action('SolicitudController@adminAutorizadas', ['page' => $page, 'query' => $query]);
    }

    public function solicitudesAdmin(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-aprobar', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.index')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('APROBAR SOLICITUDES')
            ->withGestiones($gestiones);
    }

    public function adminSolicitudAprobadas(){
        $usuario = auth()->user();
        $sol_id = $_GET['id_sol'];

        //aca revisar

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD APROBADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $sol_id,
            'estado_id' => 40,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        return back();
    }

    public function solicitudesAdminAprobadas(Request $request, $search = '*'){
        $empresas = Empresas::all();
        $query_array = array();
        if($request->fecha_ini != '' && $request->fecha_fin != ''){
            $query_array[] = " created_at BETWEEN ".$request->fecha_ini." AND ".$request->fecha_fin;
        }elseif($request->fecha_ini != ''){
            $query_array[] = " created_at LIKE '%".$request->fecha_ini."%'";
        }

        if($request->proyecto_id != 0){
            $query_array[] = " proyecto_id = ".$request->proyecto_id;
        }elseif($request->empresa_id != 0){
            $proyectos_id = Proyectos::where('empresa_id', $request->empresa_id)
                ->pluck('id')
                ->toArray();
            $query_array[] = " proyecto_id IN (".implode(',', $proyectos_id).")";
        }

        if($request->monto > 0){
            $selMonto = intVal($request->selectMonto);
            switch($selMonto){
                case 0 :
                    //igual
                    $query_array[] = " total == ".$request->monto;
                    break;
                case 1 :
                    //mayor que
                    $query_array[] = " total > ".$request->monto;
                    break;
                case 2 :
                    //mayor igual que
                    $query_array[] = " total >= ".$request->monto;
                    break;
                case 3 :
                    //menor que
                    $query_array[] = " total < ".$request->monto;
                    break;
                case 4 :
                    //menor igual que
                    $query_array[] = " total >= ".$request->monto;
                    break;
            }
        }

        if($request->texto != null){
            $selMonto = intVal($request->campo);
            switch($selMonto){
                case 1 :
                    //solicitante
                    $query_array[] = " solicitante LIKE '%".$request->texto."%'";
                    break;
                case 2 :
                    //desembolso
                    $query_array[] = " desembolso LIKE '%".$request->texto."%'";
                    break;
                case 3 :
                    //referencia
                    $query_array[] = " referencia LIKE '%".$request->texto."%'";
                    break;
                case 4 :
                    //observacion
                    $query_array[] = " observacion LIKE '%".$request->texto."%'";
                    break;
            }
        }

        if(isset($_GET['query'])){
            $query = $_GET['query'];
        }else{
            $query = "";
            $first = true;
            foreach($query_array as $query_item){
                if($first){
                    $query .= " WHERE".$query_item;
                    $first = false;
                }else{
                    $query .= " AND".$query_item;
                }
            }
        }

        if($query == ""){
            $solicitudes = Solicitudes::whereIn('gestion_id', session('gestion_id'))
                ->orderBy('created_at', 'DESC')
                ->get();
        }else{
            $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' ORDER BY created_at DESC');
        }

        $usuario = auth()->user();
        session(['gestion' => Carbon::now()->year]);
        session(['paginacion' => 20]);

        $var = ['APR', 'TES', 'TES-O', 'TES-M', 'REN', 'REN-P', 'REN-F', 'REN-A', 'REN-OA', 'REN-OB', 'REN-OC', 'FIN'];
        //$var = ['SOL', 'SOL-M','AUT','AUT-M', 'REV','REV-M', 'APR','APR-M', 'APR-P', 'TES', 'REN-F'];
        $tituloSeleccionSolicitud = "En curso";
        if(isset($_GET['t'])){
            switch($_GET['t']){
                case 'observadas':
                    $var = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];
                    $tituloSeleccionSolicitud = "Observadas";
                    break;
                case 'rechazadas':
                    $var = ['AUT-R', 'REV-R', 'APR-R', 'TES-R'];
                    $tituloSeleccionSolicitud = "Rechazadas";
                    break;
                case 'rendidas':
                    $var = ['REN-F'];
                    $tituloSeleccionSolicitud = "Rendidas";
                    break;
                case 'finalizadas':
                    $var = ['FIN'];
                    $tituloSeleccionSolicitud = "Finalizadas";
                    break;
            }
        }

        $solicitud_ids = array();
        foreach($solicitudes as $solicitud){
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if(in_array($solicitud->estadoCodigo, $var)){
                $solicitud_ids [] = $solicitud->id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $solicitud_ids)->get();

        $total_solicitudes = 0;
        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            $total_solicitudes += $solicitud->total;
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }
        $total_solicitudes = number_format($total_solicitudes, '2', '.', ',');

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){

            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        return view('solicitudes.admin.aprobadas')
            ->withTotalsolicitudes($total_solicitudes)
            ->withSolicitudes($solicitudes)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all());
    }

    public function adminCreadas(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-creadas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.creadas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES CREADAS')
            ->withGestiones($gestiones);
    }

    public function adminAutorizadas(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-revisadas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.autorizadas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES REVISADAS')
            ->withGestiones($gestiones);
    }

    public function adminPendientes(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-pendientes', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.pendientes')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES')
            ->withGestiones($gestiones);
    }

    public function adminPendientes2(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-pendientes2', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */
        
        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.pendientes2')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES')
            ->withGestiones($gestiones);
    }

    public function adminPendientes3(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-pendientes3', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */
        
        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.pendientes3')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES')
            ->withGestiones($gestiones);
    }

    public function adminPendientes4(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-pendientes4', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */
        
        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.pendientes4')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES')
            ->withGestiones($gestiones);
    }

    public function adminPendientes5(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-pendientes5', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */
        
        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.pendientes5')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES PENDIENTES')
            ->withGestiones($gestiones);
    }

    public function adminTransferencias(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdminTransferencias($request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */
        
        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.transferencias')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES DE TRANSFERENCIA')
            ->withGestiones($gestiones);
    }

    public function adminSolicitudPendiente(){
        $usuario = auth()->user();
        $sol_id = $_GET['pendiente'];

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD PUESTA EN PENDIENTES POR APROBADOR',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $sol_id,
            'estado_id' => 45,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        return back();
    }

    public function adminSolicitudPendiente2(){
        $usuario = auth()->user();
        $sol_id = $_GET['pendiente'];

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD PUESTA EN PENDIENTES POR APROBADOR',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $sol_id,
            'estado_id' => 46,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        return back();
    }

    public function adminSolicitudPendiente3(){
        $usuario = auth()->user();
        $sol_id = $_GET['pendiente'];

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD PUESTA EN PENDIENTES POR APROBADOR',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $sol_id,
            'estado_id' => 47,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        return back();
    }

    public function adminSolicitudPendiente4(){
        $usuario = auth()->user();
        $sol_id = $_GET['pendiente'];

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD PUESTA EN PENDIENTES POR APROBADOR',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $sol_id,
            'estado_id' => 48,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        return back();
    }

    public function adminSolicitudPendiente5(){
        $usuario = auth()->user();
        $sol_id = $_GET['pendiente'];

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD PUESTA EN PENDIENTES POR APROBADOR',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $sol_id,
            'estado_id' => 49,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        return back();
    }

    public function adminTesoreria(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-tesoreria', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.tesoreria')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES TESORERIA')
            ->withGestiones($gestiones);
    }

    public function adminRechazadas(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-rechazadas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.rechazadas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES RECHAZADAS')
            ->withGestiones($gestiones);
    }

    public function adminRechazadasTesoreria(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-rechazadas_tesoreria', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.rechazadosTesoreria')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('SOLICITUDES RECHAZADAS POR TESORERÍA')
            ->withGestiones($gestiones);
    }

    public function adminTodas(Request $request, $search = '*'){
        $idSolicitudes = $this->seccionSolicitudAdmin('admin-todas', $request->all());

        $solicitudes = Solicitudes::sortable()
            ->orderBy('gestion_id', 'DESC')
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->orderBy('created_at', 'ASC')
            ->whereIn('id', $idSolicitudes)->get();

        $monedas = Monedas::all();
        $total_monedas = array();
        foreach($monedas as $moneda){
            $total_monedas[$moneda->id] = 0;
        }

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            $total_monedas[$solicitud->moneda_id] += $solicitud->total;
            
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        if(isset($_GET['page'])){
            $pageName = $_GET['page'];
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }
        
        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        $empresas = Empresas::all();

        $usuario = auth()->user();

        $query = ''; /*************************** */

        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        return view('solicitudes.admin.todas')
            ->withExcelsolicitudes($idSolicitudes)
            ->withTotalmonedas($total_monedas)
            ->withSolicitudes($solicitudes)
            ->withTodasmonedas($monedas)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withCon($query)
            ->withTitulo("ADMIN")
            ->withVariables($request->all())
            ->withTipo_solicitud('TODAS LAS SOLICITUDES')
            ->withGestiones($gestiones);
    }

    public function rechazo(Request $request){
        $usuario = auth()->user();
        $id_estado = 21;

        switch($request->tipo){
            case 'autorizar':
                $id_estado = 21;
                break;
            case 'revisor':
                $id_estado = 31;
                break;
            case 'aprobador':
                $id_estado = 41;
                break;
            case 'tesoreria':
                $id_estado = 51;
                break;
            case 'Aprobar':
                $id_estado = 41;
                break;
            case 'rendicion':
                $id_estado = 71;
                break;
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => mb_strtoupper(trim($request->motivo)),
            'usuario_id' => $usuario->id,
            'solicitud_id' => $request->id_solicitud,
            'estado_id' => $id_estado,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        if ($request->tipo == 'revisor') {
            /* Eliminación de ítems rendidos */
            $solicitud = Solicitudes::find($request->id_solicitud);
            if($solicitud->modalidad_id == 3 || $solicitud->modalidad_id == 4){
                foreach($solicitud->itemsRendidos as $itemRendido){
                    $itemRendido->delete();
                }
            }
        }

        return back();
    }

    //Rechazo Solicitud
    public function rechazarSolicitud(){
        $tituloSeleccionSolicitud = "En curso";
        //echo "SolicitudController@rechazarSolicitud";
        $id_sol = $_GET['id_sol'];
        $solicitud = Solicitudes::find($id_sol);
        $proyecto = Proyectos::find($solicitud->proyecto_id);
        if($proyecto->padre_id == 0){
            $proyecto_nombre = $solicitud->proyecto->nombre;
            $subproy_nombre = "";
        }else{
            $subproy = Proyectos::find($proyecto->padre_id);
            $proyecto_nombre = $subproy->nombre;
            $subproy_nombre = $solicitud->proyecto->nombre;
        }
        $items = Items::where('solicitud_id', $solicitud->id)->get();;
        return view ('solicitudes.rechazar')
            ->withTitulo($tituloSeleccionSolicitud)
            ->withPadre($proyecto_nombre)
            ->withSolicitud($solicitud)
            ->withHijo($subproy_nombre)
            ->withItems($items);
    }

    public function observacion(Request $request){
        $estado = 22;
        $usuario = auth()->user();

        if(isset($request->rechazoRendicion)){
            $estado =  75;
            DB::table('estados_solicitud')->insert([
                'observacion' => mb_strtoupper($request->motivo),
                'usuario_id' => $usuario->id,
                'solicitud_id' => intVal($request->id_solicitud),
                'estado_id' => $estado,
                'created_at' =>  Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            DB::table('estados_solicitud')->insert([
                'observacion' => mb_strtoupper($request->motivo),
                'usuario_id' => $usuario->id,
                'solicitud_id' => intVal($request->id_solicitud),
                'estado_id' => 50,
                'created_at' =>  Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }else{
            switch($request->id_rol){
                case 3:
                    $estado =  22;
                    break;
                case 4:
                    $estado =  32;
                    break;
                case 5:
                    $estado =  42;
                    break;
                case 6:
                    $estado =  52;
                    break;
                case 7:
                    $estado =  82;
                    break;
                case 8:
                    $estado =  75;
                    break;
            }
            DB::table('estados_solicitud')->insert([
                'observacion' => mb_strtoupper($request->motivo),
                'usuario_id' => $usuario->id,
                'solicitud_id' => intVal($request->id_solicitud),
                'estado_id' => $estado,
                'created_at' =>  Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        return back();
    }

    public function tesoreria(){
        $usuario = auth()->user();

        $idSolicitudes = $this->seccionSolicitud('tesoreria-tesoreria', "");

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            // ->whereIn('id', $solicitudes_ids)
            ->whereIn('id', $idSolicitudes)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->subtotal
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.tesoreria.index')
            ->withSolicitudes($solicitudes)
            ->withUsuario($usuario)
            ->withItems($items);
    }

    public function buscadorTesoreria(Request $request){
        $usuario = auth()->user();
        $add_query = "";        
        switch ($request->selectOpcion) {
            case 0:
                if($request->buscar != ''){
                    $add_query = " AND (";
                    if(is_numeric($request->buscar)){
                        $add_query .= " numero = ".$request->buscar. " OR ";
                        $add_query .= " total = ".$request->buscar. " OR ";
                    }

                    //Nombre de Usuario
                    $str_ids_usuarios = "";
                    $cont_ids_usu = 0;
                    $usuarios = User::where('nombre', 'LIKE', '%'.$request->buscar.'%')->get();
                    foreach($usuarios as $usuario){
                        $str_ids_usuarios .= $usuario->id.",";
                        $cont_ids_usu++;
                    }
                    if(substr($str_ids_usuarios, -1) == ','){
                        $str_ids_usuarios = substr($str_ids_usuarios, 0, strlen($str_ids_usuarios)-1);
                    }

                    //$add_query .= " total = '%".$request->buscar."%'";
                    if($str_ids_usuarios != ''){
                        if(strlen($add_query) > 6){
                            $add_query .= " OR ";
                        }
                        $add_query .= "usuario_id IN (".$str_ids_usuarios.")";
                    }

                    $proyectos = Proyectos::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->where('empresa_id', session('empresa'))
                        ->pluck('id')
                        ->toArray();
                    if(count($proyectos)>0){
                        if(strlen($add_query) > 6){
                            $add_query .= " OR ";
                        }
                        $add_query .= "proyecto_id IN (".implode(', ',$proyectos).")";
                    }

                    if(strlen($add_query) > 6){
                        $add_query .= " OR ";
                    }

                    $monedas = Monedas::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->pluck('id')
                        ->toArray();

                    $modalidades = Modalidades::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->pluck('id')
                        ->toArray();

                    $add_query .= "created_at LIKE '%".$request->buscar."%'";
                    $add_query .= " OR desembolso LIKE '%".$request->buscar."%'";

                    if(count($monedas) > 0){
                        $add_query .= " OR moneda_id IN (".implode(',', $monedas).")";
                    }

                    if(count($modalidades) > 0){
                        $add_query .= " OR modalidad_id IN (".implode(',', $modalidades).")";
                    }

                    $add_query .= " OR referencia LIKE '%".$request->buscar."%'";
                    $add_query .= " OR observacion LIKE '%".$request->buscar."%')";
                }
        }
        $data['array_addquery'] = $add_query;
        $proyectos_ids = Proyectos::where('empresa_id', session('empresa'))
            ->select('id')
            ->pluck('id')
            ->toArray();
        $query = " WHERE gestion_id IN (".implode(',', session('gestion_id')).") AND proyecto_id IN (".implode(',', $proyectos_ids).") ".$add_query;
        $sol_ids = DB::select('SELECT id FROM solicitudes'.$query.' ORDER BY created_at DESC');
        $solicitudes_ids = array();
        foreach($sol_ids as $sol_id){
            if(in_array($this->getLastEstadoCodigo($sol_id->id), ['APR', 'REV', 'TES-M'])){
                $solicitudes_ids[] = $sol_id->id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitudes_ids)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->costo * $array_item->cantidad
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.tesoreria.index', ['tabs' => 'revisor', 'id_empresa' => session('empresa')])
            ->withSolicitudes($solicitudes)
            ->withItems($items)
            ->withUsuario($usuario);
    }

    public function express(){
        //echo "SolicitudController@express";
        $usuario = auth()->user();
        $modalidades = Modalidades::where('oculto', false)->get();
        $monedas = Monedas::all();
        //$tipo_cambio = TiposCambios::all()->last()->toArray();
        $tipo_cambio = TiposCambios::moneda(2)->get()->last()->toArray();
        if($usuario->rol->nombre == "TESORERIA"){
            $array_ids = array();
            if($usuario->rol_id == 1 || $usuario->rol_id >=3){
                $proyectos = Proyectos::where('empresa_id', session('empresa'))
                    ->get();
            }else{
                $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                    $query->where('user_id', $usuario->id);
                })->get();
            }

            if(count($proyectos) == 0){
                Session::flash('success', 'No cuenta con proyectos asignados');
                return redirect()->route('solicitudes');
                //VERIFICAR REDIRECCION
            }else{
                $array_proyectos = array();
                foreach($proyectos as $proyecto){
                    if($proyecto->padre_id == 0){
                        $array_proyectos[] = [
                            'id' => $proyecto->id,
                            'nombre' => $proyecto->nombre
                        ];
                    }else{
                        $proy_padre = Proyectos::find($proyecto->padre_id);
                        $array_proyectos[] = [
                            'id' => $proyecto->id,
                            'nombre' => $proy_padre->nombre." &#10148; ".$proyecto->nombre
                        ];
                    }
                }
                $controller = false;
                return view('solicitudes.crear')
                    ->withProyectos($array_proyectos)
                    ->withModalidades($modalidades)
                    ->withCambio($tipo_cambio)
                    ->withMonedas($monedas)
                    ->withController($controller);
            }

            $controller = false;
            return view('solicitudes.tesoreria.express')
                ->withUser($usuario)
                ->withProyectos($array_proyectos)
                ->withModalidades($modalidades)
                ->withCambio($tipo_cambio)
                ->withMonedas($monedas)
                ->withController($controller);
        }else{
            Session::flash('success', 'Acceso Restringido');
            $orden = "";
            $query = "SELECT s.id FROM solicitudes s "
                . "WHERE s.estado IN (2, 4) AND s.empresa_id = ".session('empresa')." AND s.usuario_id = ".$usuario->id;
            $solicitudes_id = DB::select($query);
            $array_ids = array();
            foreach($solicitudes_id as $solicitud){
                $array_ids[] = $solicitud->id;
            }
            $solicitudes = Solicitudes::whereIn("id", $array_ids)->get();
            return view('solicitudes.index')->withOrden_type($orden)->withSolicitudes($solicitudes);
        }
    }

    public function registroTesoreria($solicitud_id){
        $solicitud = Solicitudes::find($solicitud_id);
        $proyecto = Proyectos::find($solicitud->proyecto_id);
        $array_proyecto = array();
        $solicitud->autorizador = $this->getUsuarioPorEstado(20, $solicitud->id); // estado para autorizador 20
        $solicitud->controller = $this->getUsuarioPorEstado(30, $solicitud->id); // estado para controller 30
        $solicitud->aprobador = $this->getUsuarioPorEstado(40, $solicitud->id); // estado para aprobador 40

        if($proyecto->padre_id == 0){
            $array_proyecto = [
                'proyecto' => $proyecto->nombre,
                'subproyecto' => "",
                'modalidad' => $solicitud->modalidad->nombre
            ];
        }else{
            $subProy = Proyectos::find($proyecto->padre_id);
            $array_proyecto = [
                'proyecto' => $subProy->nombre,
                'subproyecto' => $proyecto->nombre,
                'modalidad' => $solicitud->modalidad->nombre
            ];
        }

        $cuentas = CuentaBancaria::where('empresa_id', session('empresa'))->get();
        return View('solicitudes.tesoreria.registro')
            ->withProyecto($array_proyecto)
            ->withSolicitud($solicitud)
            ->withCuentas($cuentas);
    }

    public function rendiciones(){
        $usuario = auth()->user();
        
        $proyectos_ids = Proyectos::where('empresa_id', session('empresa'))->pluck('id')->toArray();

        $solicitudes = Solicitudes::where('usuario_id', $usuario->id)
            ->whereIn('gestion_id', session('gestion_id'))
            ->whereIn('proyecto_id', $proyectos_ids)
            ->get();

        $solicitudes_ids = array();
        foreach($solicitudes as $solicitud){
            if($this->getLastEstadoCodigo($solicitud->id) == 'TES' || $this->getLastEstadoCodigo($solicitud->id) == 'REN-R' || $this->getLastEstadoCodigo($solicitud->id) == 'REN-O' ){
                $solicitudes_ids[] = $solicitud->id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitudes_ids)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $extras = ["FFF", "9B59B6"];
        return View('solicitudes.rendiciones.index')
            ->withSolicitudes($solicitudes)
            ->withUsuario($usuario)
            ->withExtras($extras);
    }

    public function rendir($solicitud_id){
        $solicitud = Solicitudes::find($solicitud_id);
        $items = $solicitud->items;
        $datos_extra = array();
        $datos_extra['total_desembolso'] = 0;
        $datos_extra['total_rendido'] = 0;
        foreach($items as $item){
            $items_ids[] = $item->id;
            $datos_extra['total_desembolso'] += number_format($item->cantidad * $item->costo, '2', '.', '');
        }
        $items_rendidos = ItemsRendiciones::where('solicitud_id', $solicitud->id)->get();
        $items_rendidos_ids = array();
        foreach($items_rendidos as $item){
            $datos_extra['total_rendido'] += number_format($item->cantidad * $item->costo, '2', '.', '');
            if($item->item_id != null){
                $items_rendidos_ids[] = $item->item_id;
            }
        }

        $items = Items::where("solicitud_id", $solicitud_id)
            ->whereNotIn('id', $items_rendidos_ids)
            ->get();

        $devoluciones = Devoluciones::where("solicitud_id", $solicitud_id)->get();

        ($datos_extra['total_rendido']>$datos_extra['total_desembolso'])?$datos_extra['favor_solicitante']=$datos_extra['total_desembolso'] - $datos_extra['total_rendido']:$datos_extra['favor_solicitante']=0;
        $datos_extra['favor_empresa'] = $datos_extra['total_desembolso'] - $datos_extra['total_rendido'];

        $unidades = Unidades::all()->pluck('descripcion', 'id');
        $tipo_compra = ItemsTipos::all()->pluck('descripcion', 'id');
        $doc_rendidos = DocumentosRendicion::where('solicitud_id', $solicitud_id)->get();

        return View('solicitudes.rendiciones.form')
            ->withItemsRendidos($items_rendidos)
            ->withDevoluciones($devoluciones)
            ->withDocrendidos($doc_rendidos)
            ->withTipocompra($tipo_compra)
            ->withSolicitud($solicitud)
            ->withExtras($datos_extra)
            ->withUnidades($unidades)
            ->withItems($items);

    }

    public function eliminarItemRendido(){
        $usuario = auth()->user();
        $itemrendido = ItemsRendiciones::find($_GET['doc']);
        if($itemrendido->tipo_id == 1 || $itemrendido->tipo_id == 7){
            DB::table('item_af_datos')->where('item_rendido_id', $itemrendido->id)->delete();
        }
        $itemrendido->delete();
        return back();
    }

    public function itemRendicionStore(Request $request){
        $usuario = auth()->user();
        $factura = 0;
        if($request->item_id){
            $item_id = $request->item_id;
        }else{
            $item_id = null;
        }
        if(is_numeric($request->num_factura) && intval($request->num_factura) > 0){
            $factura = $request->num_factura;
        }

        $item = Items::find($item_id);

        $item_rendicion = new ItemsRendiciones;
        $item_rendicion->cantidad = $request->cantidad;
        $item_rendicion->costo = $request->costo;
        $item_rendicion->unidad_id = $request->unidad_id;
        $item_rendicion->fecha_factura = $request->fecha_factura;
        $item_rendicion->num_factura = $factura;
        $item_rendicion->detalle =  mb_strtoupper($request->detalle);
        $item_rendicion->direccion = '';
        $item_rendicion->item_id = $item_id;
        $item_rendicion->solicitud_id = $request->solicitud_id;
        $item_rendicion->tipo_id = $request->tipocompra_id;
        $item_rendicion->created_at =  Carbon::now();
        $item_rendicion->updated_at = Carbon::now();
        $item_rendicion->save();

        if($item_rendicion->tipo_id == 1 || $item_rendicion->tipo_id == 7){
           for($i = 0; $i<$request->cantidad ; $i++){
                $item_af = new ItemsAf;
                $item_af->descripcion_especifica = mb_strtoupper($request->af_descripcion_especifica[$i]);
                $item_af->numero_serie = mb_strtoupper($request->af_numero_serie[$i]);
                $item_af->asignacion = mb_strtoupper($request->af_asignacion[$i]);
                $item_af->actividad_especifica = mb_strtoupper($request->af_actividad[$i]);
                $item_af->nuevo = ($request->af_nuevo[$i] == "Si")?true:false;
                $item_af->reemplazo = ($request->af_reemplazo[$i] == "Si")?true:false;
                $item_af->garantia = ($request->af_garantia[$i] == "Si")?true:false;
                $item_af->item_rendido_id = $item_rendicion->id;
                $item_af->save();                
            }
       }

        $solicitud = Solicitudes::find($request->solicitud_id);
        $items = $solicitud->items;
        $datos_extra = array();
        $datos_extra['total_desembolso'] = 0;
        $datos_extra['total_rendido'] = 0;
        foreach($items as $item){
            $items_ids[] = $item->id;
            $datos_extra['total_desembolso'] += number_format($item->cantidad * $item->costo, '2', '.', '');
        }
        $items_rendidos = ItemsRendiciones::whereIn('item_id', $items_ids)->get();
        $items_rendidos_ids = array();
        foreach($items_rendidos as $item){
            $datos_extra['total_rendido'] += number_format($item->cantidad * $item->costo, '2', '.', '');
            $items_rendidos_ids[] = $item->item_id;
        }

        $items = Items::where("solicitud_id", $request->solicitud_id)
            ->whereNotIn('id', $items_rendidos_ids)
            ->get();

        ($datos_extra['total_rendido']>$datos_extra['total_desembolso'])?$datos_extra['favor_solicitante']=$datos_extra['total_desembolso'] - $datos_extra['total_rendido']:$datos_extra['favor_solicitante']=0;
        $datos_extra['favor_empresa'] = $datos_extra['total_desembolso'] - $datos_extra['total_rendido'];

        $unidades = Unidades::all()->pluck('codigo', 'id');

        Session::flash('success', 'Item Rendido!!!');
        return redirect()->route('rendir/solicitud', $solicitud->id);

    }

    public function solicitudRendiciones(){
        $usuario = auth()->user();
        $solicitudes_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
            ->select('solicitudes.id')
            ->whereIn('gestion_id', session('gestion_id'))
            ->havingRaw('MAX(estados_solicitud.estado_id) IN (72)')//REVISAR
            ->groupBy('solicitud_id')
            ->pluck('solicitudes.id')
            ->toArray();
        $solicitudes = Solicitudes::whereIn('id', $solicitudes_ids)->get();

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = ItemsRendiciones::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'unidad' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => number_format($array_item->cantidad * $array_item->costo, '2', '.', '')
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.tesoreria.rendiciones')
            ->withSolicitudes($solicitudes)
            ->withItems($items);
    }

    public function solicitudDocumentoRendicionStore(Request $request){
        // Solicitud
        $solicitud_id = $request->solicitud_id;
        $solicitud = Solicitudes::find($solicitud_id);

        //Upload Files
        $array_documentos = array();
        $path = public_path().'/empresas/'.$solicitud->proyecto->empresa->slug."/".$solicitud->gestion->gestion."/rendicion/";
        //echo $path;
        for($i = 1; $i <= 30; $i++){
            $doc_name = 'doc'.$i;
            if(isset($_FILES[$doc_name]['name']) && $_FILES[$doc_name]['name']!=''){
                $str_name = trim($_FILES[$doc_name]['name']);
                $file_type = $_FILES[$doc_name]['type'];
                $str_name = $this->remover_acentos($str_name);
                $str_name = strtolower($str_name);
                $str_name = str_replace('-', '', $str_name);
                $str_name = str_replace(' ', '_', $str_name);
                $str_name = str_replace('__', '_', $str_name);
                $str_name = str_replace('___ ', '_', $str_name);
                $str_name = str_replace('____', '_', $str_name);
                $db_filename = $request->solicitud_id."_".time()."_".$str_name;
                move_uploaded_file($_FILES[$doc_name]['tmp_name'], $path.$db_filename);
                $documento = new DocumentosRendicion;
                $documento->direccion = $db_filename;
                $documento->solicitud_id = $request->solicitud_id;

                $documento->save();
            }
        }
        $items = $solicitud->items;
        $datos_extra = array();
        $datos_extra['total_desembolso'] = 0;
        $datos_extra['total_rendido'] = 0;
        foreach($items as $item){
            $items_ids[] = $item->id;
            $datos_extra['total_desembolso'] += number_format($item->cantidad * $item->costo, '2', '.', '');
        }
        $items_rendidos = ItemsRendiciones::where('solicitud_id', $solicitud->id)->get();
        $items_rendidos_ids = array();
        foreach($items_rendidos as $item){
            $datos_extra['total_rendido'] += number_format($item->cantidad * $item->costo, '2', '.', '');
            if($item->item_id != null){
                $items_rendidos_ids[] = $item->item_id;
            }
        }

        $items = Items::where("solicitud_id", $solicitud_id)
            ->whereNotIn('id', $items_rendidos_ids)
            ->get();

        $devoluciones = Devoluciones::where("solicitud_id", $solicitud_id)->get();

        ($datos_extra['total_rendido']>$datos_extra['total_desembolso'])?$datos_extra['favor_solicitante']=$datos_extra['total_desembolso'] - $datos_extra['total_rendido']:$datos_extra['favor_solicitante']=0;
        $datos_extra['favor_empresa'] = $datos_extra['total_desembolso'] - $datos_extra['total_rendido'];

        $unidades = Unidades::all()->pluck('descripcion', 'id');
        $tipo_compra = ItemsTipos::all()->pluck('descripcion', 'id');
        $doc_rendidos = DocumentosRendicion::where('solicitud_id', $solicitud_id)->get();

        return View('solicitudes.rendiciones.form')
            ->withItemsRendidos($items_rendidos)
            ->withDevoluciones($devoluciones)
            ->withDocrendidos($doc_rendidos)
            ->withTipocompra($tipo_compra)
            ->withSolicitud($solicitud)
            ->withExtras($datos_extra)
            ->withUnidades($unidades)
            ->withItems($items);
    }

    public function finalizarSolicitudStore(Request $request){
        if (trim($request->observacion) != '' && trim($request->observacion) != null) {
            DB::table('observaciones_rendicion')->insert([
                'observacion' => mb_strtoupper($request->observacion),
                'solicitud_id' => $request->solicitud_id,
                'created_at' =>  Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $usuario = auth()->user();
        DB::table('estados_solicitud')->insert([
            'observacion' => 'RENDICIÓN FINALIZADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $request->solicitud_id,
            'estado_id' => 62,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        $solicitudes_ids = null;
        if ($usuario->global_view == 0) {
            $solicitudes_ids = Solicitudes::where('solicitudes.usuario_id', $usuario->id)
                ->join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                ->select('solicitudes.id')
                ->whereIn('gestion_id', session('gestion_id'))
                ->havingRaw('MAX(estados_solicitud.estado_id) = 50')
                ->groupBy('solicitudes.id')
                ->pluck('solicitudes.id')
                ->toArray();
        } else {
            $solicitud_rend = Solicitudes::find($request->solicitud_id);
    
            $solicitudes_ids = Solicitudes::where('solicitudes.usuario_id', $usuario->id)
                ->join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                ->select('solicitudes.id')
                ->where('gestion_id', $solicitud_rend->gestion_id)
                ->havingRaw('MAX(estados_solicitud.estado_id) = 50')
                ->groupBy('solicitudes.id')
                ->pluck('solicitudes.id')
                ->toArray();
        }

        $solicitudes = Solicitudes::whereIn('id', $solicitudes_ids)
            ->orderBy('solicitudes.id', 'DESC')
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $extras = ["FFF", "9B59B6"];

        if ($usuario->global_view == 0) {
            return View('solicitudes.rendiciones.index')
                ->withSolicitudes($solicitudes)
                ->withUsuario($usuario)
                ->withExtras($extras);
        } else {
            return redirect()->route('global.en_curso');
        }
    }

    public function solicitudAprobarRendicion($solicitud_id){
        $solicitud = Solicitudes::find($solicitud_id);
        $items = $solicitud->itemsRendidos;
        $datos_extra = array();
        $datos_extra['total_desembolso'] = 0;
        $datos_extra['total_rendido'] = 0;
        foreach($items as $item){
            $items_ids[] = $item->id;
            $datos_extra['total_desembolso'] += number_format($item->cantidad * $item->costo, '2', '.', '');
        }
        $items_rendidos = ItemsRendiciones::where('solicitud_id', $solicitud->id)->get();
        $items_rendidos_ids = array();
        foreach($items_rendidos as $item){
            $datos_extra['total_rendido'] += number_format($item->cantidad * $item->costo, '2', '.', '');
            if($item->item_id != null){
                $items_rendidos_ids[] = $item->item_id;
            }
        }

        $items = Items::where("solicitud_id", $solicitud_id)
            ->whereNotIn('id', $items_rendidos_ids)
            ->get();

        ($datos_extra['total_rendido']>$datos_extra['total_desembolso'])?$datos_extra['favor_solicitante']=$datos_extra['total_desembolso'] - $datos_extra['total_rendido']:$datos_extra['favor_solicitante']=0;
        $datos_extra['favor_empresa'] = $datos_extra['total_desembolso'] - $datos_extra['total_rendido'];

        $unidades = Unidades::all()->pluck('codigo', 'id');
        return View('solicitudes.tesoreria.aprobarRendicion')
            ->withItemsRendidos($items_rendidos)
            ->withSolicitud($solicitud)
            ->withExtras($datos_extra)
            ->withUnidades($unidades)
            ->withItems($items);
    }

    public function solicitudAprobarRendicionStore(Request $request){
        $usuario = auth()->user();
        $items = Items::where('solicitud_id', $request->solicitud_id)->get();
        $items_rendidos = ItemsRendiciones::where('solicitud_id', $request->solicitud_id)->get();
        if(intVal($this->getLastEstadoId($request->solicitud_id)) < 17){
            if(count($items) == count($items_rendidos)){
                $items_total = $items_rendidos_total = 0;
                foreach($items as $item){
                    $items_total += $item->costo * $item->cantidad;
                }
                foreach($items_rendidos as $item){
                    $items_rendidos_total += $item->costo * $item->cantidad;
                }
                if($items_total == $items_rendidos_total){
                    //Solicitud rendida de forma ideal
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD OBSERVADA EN RENDICIÓN A',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $request->solicitud_id,
                        'estado_id' => 72,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                }else{
                    //solicitud rendida con variacion en precios
                    DB::table('estados_solicitud')->insert([
                        'observacion' => 'SOLICITUD OBSERVADA EN RENDICIÓN B',
                        'usuario_id' => $usuario->id,
                        'solicitud_id' => $request->solicitud_id,
                        'estado_id' => 73,
                        "created_at" =>  Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                }
            }else{
                DB::table('estados_solicitud')->insert([
                    'observacion' => 'SOLICITUD OBSERVADA EN RENDICIÓN C',
                    'usuario_id' => $usuario->id,
                    'solicitud_id' => $request->solicitud_id,
                    'estado_id' => 74,
                    "created_at" =>  Carbon::now(),
                    "updated_at" => Carbon::now()
                ]);
            }
        }

        $solicitudes_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
            ->select('solicitudes.id')
            ->whereIn('gestion_id', session('gestion_id'))
            ->havingRaw('MAX(estados_solicitud.estado_id) IN (14)')
            ->groupBy('solicitud_id')
            ->pluck('solicitudes.id')
            ->toArray();
        $solicitudes = Solicitudes::whereIn('id', $solicitudes_ids)->get();
        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = ItemsRendiciones::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'unidad' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => number_format($array_item->cantidad * $array_item->costo, '2', '.', '')
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }

        return view('solicitudes.tesoreria.rendiciones')
            ->withSolicitudes($solicitudes)
            ->withItems($items);
    }

    public function getLastEstado($solicitud_id){
        $usuario = auth()->user();
        if($usuario->rol_id == 8){
            $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
                ->select('estados.descripcion')
                ->where('estados_solicitud.usuario_id', $usuario->id)
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->orderBy('estados.id', 'DESC')
                ->first();
            if(!$lastEstado){
                $lastEstado = Estados::first();
                $lastEstado->descripcion = "";
            }
        }else{
            $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
                ->select('estados.descripcion')
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->orderBy('estados.id', 'DESC')
                ->first();
        }
        
        return $lastEstado->descripcion;
    }

    public function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    public function getLastEstadoId($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.id')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados.id', 'DESC')
            ->first();
        return $lastEstado->id;
    }

    public function getUsuarioPorEstado($estado_id, $solicitud_id){
        $usuarioPorEstado = User::join('estados_solicitud', 'estados_solicitud.usuario_id', '=', 'users.id')
            ->select('users.nombre')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->where('estados_solicitud.estado_id', $estado_id)
            ->orderBy('estados_solicitud.created_at', 'DESC')
            ->first();
        if(count($usuarioPorEstado) < 1){
            $cad_return = "";
            switch($estado_id){
                case 20:
                    $cad_return = "SIN AUTORIZADOR";
                    break;
                case 30:
                    $cad_return = "SIN REVISOR";
                    break;
                case 40:
                    $cad_return = "SIN APROBADOR";
                    break;
                case 50:
                    $cad_return = "SIN TESORERIA";
                    break;
            }
            return $cad_return;
        }else{
            return $usuarioPorEstado->nombre;
        }
    }

    public function getEstados(Request $request, $id){
        if ($request->ajax()){
            $solicitud = Solicitudes::find($id);
            $estados = $solicitud->estados()
                ->join('users', 'estados_solicitud.usuario_id', '=', 'users.id')
                ->join('estados', 'estados_solicitud.estado_id', '=', 'estados.id')
                ->select(DB::raw('CONCAT(IF(users.nombre = "Admin", "Admin", users.nombre), "|", estados.codigo, "|", estados_solicitud.observacion) AS estado, estados_solicitud.created_at'))
                ->orderBy('estados_solicitud.id', 'ASC')
                ->get();
                // ->pluck('created_at', 'estado');
            
            return response()->json([
                'solicitud' => $solicitud,
                'estados' => $estados,
            ]);
        }
    }

    public function aprobadorRendicion(){
        $usuario = auth()->user();

        $idSolicitudes = $this->seccionSolicitud('aprobar_rendiciones-aprobar_rendiciones', "");

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            // ->whereIn('id', $solicitudes_ids)
            ->whereIn('id', $idSolicitudes)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->costo * $array_item->cantidad
                ];

            }
            $solicitud->formulariosAF = false;
            $items_af = DB::select('SELECT COUNT(id) as cantidad, solicitud_id FROM item_rendiciones WHERE solicitud_id = '.$solicitud->id.' AND tipo_id IN (1,7) GROUP BY solicitud_id');
            $cantidad = 0;
            foreach($items_af as $item_af){
                if($item_af->cantidad > $cantidad){
                    $cantidad = $item_af->cantidad;
                }
            }
            if($cantidad > 0){
                $solicitud->formulariosAF = true;
            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.aprRendiciones.index')
            ->withSolicitudes($solicitudes)
            ->withUsuario($usuario)
            ->withItems($items);
    }

    public function aprParcialSolicitudes(){
        $proyectos_ids = Proyectos::select('id')
            ->where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();
        $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
            ->join('proyectos', 'proyectos.id', 'solicitudes.proyecto_id')
            ->select('solicitudes.id')
            ->whereIn('gestion_id', session('gestion_id'))
            ->where('proyectos.empresa_id', session('empresa'))
            ->orderBy('solicitudes.created_at')
            ->groupBy('solicitudes.id')
            ->pluck('solicitudes.id')
            ->toArray();

        $usuario = auth()->user();

        $solicitudes_ids = array();
        foreach($sol_ids as $sol_id){
            if($this->getLastEstadoCodigo($sol_id) == 'APR-P'){
                $solicitudes_ids[] = $sol_id;
            }
        }

//        $solAprobadas_Aprobador = DB::select('SELECT solicitud_id FROM aprobador WHERE aprobador_id = '.$usuario->id);
//        $sol_apr_ids = array();
//        foreach($solAprobadas_Aprobador as $solApr_ids){
//            $sol_apr_ids[] = $solApr_ids->solicitud_id;
//        }
//       $solicitudes_ids = array_diff($solicitudes_ids, $sol_apr_ids);

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitudes_ids)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->subtotal
                ];

            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.aprobador.aprparciales')
            ->withSolicitudes($solicitudes)
            ->withUsuario($usuario)
            ->withItems($items);
    }

    public function aprobarRendicionSolicitud(){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $observacion = mb_strtoupper(trim($_GET['obs']));

        if($observacion != 'SO'){
            DB::table('estados_solicitud')->insert([
                'observacion' => $observacion,
                'usuario_id' => $usuario->id,
                'solicitud_id' => $solicitud->id,
                'estado_id' => 72,
                "created_at" =>  Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD FINALIZADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 82,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        DB::table('estados_solicitud')->insert([
            'observacion' => 'SOLICITUD FINALIZADA',
            'usuario_id' => $usuario->id,
            'solicitud_id' => $solicitud->id,
            'estado_id' => 90,
            "created_at" =>  Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        if ($usuario->global_view == 0) {
            return redirect()->action('SolicitudController@aprobadorRendicion');
        } else {
            return redirect()->route('global.aprobar_rendiciones');
        }
    }

    public function modificarAprRendicion($solicitud_id, $controller=false){
        $usuario = auth()->user();
        $solicitud = Solicitudes::find($solicitud_id);

        $modalidades = Modalidades::where('oculto', false)->get();
        $monedas = Monedas::all();
        //$tipo_cambio = TiposCambios::all()->last()->toArray();
        $tipo_cambio = TiposCambios::moneda($solicitud->cambio_id)->get()->last()->toArray();
        $array_ids = array();
        if($usuario->rol_id == 1 || $usuario->rol_id >=3){
            $proyectos = Proyectos::where('empresa_id', session('empresa'))
                ->get();
        }else{
            $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                $query->where('user_id', $usuario->id);
            })->get();
        }

        $array_proyectos = array();
        foreach($proyectos as $proyecto){
            if($proyecto->padre_id == 0){
                $array_proyectos[] = [
                    'id' => $proyecto->id,
                    'nombre' => $proyecto->nombre
                ];
            }else{
                $proy_padre = Proyectos::find($proyecto->padre_id);
                $array_proyectos[] = [
                    'id' => $proyecto->id,
                    'nombre' => $proy_padre->nombre." &#10148; ".$proyecto->nombre
                ];
            }
        }

        $unidades = Unidades::all()->pluck('descripcion', 'id');
        $tipo_compra = ItemsTipos::all()->pluck('descripcion', 'id');
        return view('solicitudes.aprRendiciones.editar')
            ->withProyectos($array_proyectos)
            ->withModalidades($modalidades)
            ->withTipocompra($tipo_compra)
            ->withController($controller)
            ->withSolicitud($solicitud)
            ->withCambio($tipo_cambio)
            ->withUnidades($unidades)
            ->withMonedas($monedas);
    }

    public function itemDevolucionStore(Request $request){
        $usuario = auth()->user();
        //***REVISAR eliminar anteriores devoluciones?
        DB::table('devoluciones')->insert([
            'monto' => $request->monto_devolucion,
            'detalle' => $request->detalle_devolucion,
            'cuenta_id' => $request->item_id,
            'solicitud_id' => $request->solicitud_id,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Session::flash('success', 'Devolucion Realizada!!!');
        return redirect()->route('rendir/solicitud', $request->solicitud_id);
    }

    public function disponibles(Request $request){
        $extra_query = "";
        $extra_query2 = "";
        if(isset($_GET['conq']))
            $extra_query2 = $_GET['conq'];
//        }else if(isset($request)){
        if(isset($request)){
            switch(intval($request->selectOpcion)){
                case 1:
                    //Numero
                    if($request->numero != ''){
                        $extra_query = "numero = ".intval($request->numero);
                        $extra_query2 = $extra_query;
                    }
                    break;
                case 2:
                    //Monto
                    if($request->numero != ''){
                        $extra_query = "total = ".$request->numero;
                        $extra_query2 = $extra_query;
                    }
                    break;
                case 3:
                    //Proyecto
                    if($request->buscar != ''){
                        $proy_query = Proyectos::where('nombre', 'LIKE', "%$request->buscar%")
                            ->pluck('id')
                            ->toArray();
                        if(count($proy_query) > 0){
                            $extra_query = "proyecto_id IN (".implode(', ', $proy_query).")";
                            $extra_query2 = "proyecto_id = ".implode(',', $proy_query);
                        }
                    }
                    break;
                case 4:
                    //Fecha
                    if (isset($request->fechaIni)) {
                        if (isset($request->fechaFin)) {
                            $extra_query2 = "created_at,".$request->fechaIni.",".$request->fechaFin;
                        } else {
                            $extra_query2 = "created_at,".$request->fechaIni.",".$request->fechaIni;
                        }
                    }
                    break;
                case 5:
                    //Solicitante
                    if($request->buscar != ''){
                        $usu_query = User::where("nombre", "LIKE", "%$request->buscar%")
                            ->pluck('id')
                            ->toArray();
                        if(count($usu_query) > 0){
                            $extra_query = "usuario_id IN (".implode(', ', $usu_query).")";
                            $extra_query2 = "usuario_id = ".implode(',', $usu_query);
                        }
                    }
                    break;
                case 6:
                    //Desembolso
                    if($request->buscar != ''){
                        $extra_query = "desembolso LIKE '%".$request->buscar."%'";
                        $extra_query2 = "desembolso = ".$request->buscar;
                    }
                    break;
                case 7:
                    //Referencia
                    if($request->buscar != ''){
                        $extra_query = "referencia LIKE '%".$request->buscar."%'";
                        $extra_query2 = "referencia = ".$request->buscar;
                    }
                    break;
                case 8:
                    //Observacion
                    if($request->buscar != ''){
                        $extra_query = "observacion LIKE '%".$request->buscar."%'";
                        $extra_query2 = "observacion = ".$request->buscar;
                    }
                    break;
            }
        }
        //ID usuario actual
        $usuario = auth()->user();
        $id_usuario = $usuario->id;
        $sol_ids = array();
        if($usuario->rol_id == 3){
            //autorizador
            $usuario = auth()->user();
            $proyectos = UsuarioProyecto::where('user_id', $usuario->id)
                ->select('proyectos_users.proyecto_id')
                ->pluck('proyectos_users.proyecto_id');
            $usuarios_proyectos = DB::table('autorizador_usuarios')
                ->select('usuario_id', 'proyecto_id')
                ->where('autorizador_id', $usuario->id)
                ->where('empresa_id', session('empresa'))
                ->get();
            foreach($usuarios_proyectos as $usuario_proy){
                if($extra_query){
                    $proyectos_ids = Proyectos::where('empresa_id', session('empresa'))->pluck('id')->toArray();
                    $q= "SELECT solicitudes.id FROM solicitudes WHERE gestion_id IN (".implode(',', session('gestion_id')).") AND proyecto_id IN (".implode(',', $proyectos_ids).") AND ".$extra_query;
                    $solicitudes_usu_proy = DB::select($q);
                    foreach($solicitudes_usu_proy as $sol_usu_proy){
                        $sol_ids[] = $sol_usu_proy->id;
                    }
                }else{
                    $solicitudes_usu_proy = Solicitudes::select('solicitudes.id')
                        ->where('usuario_id', $usuario_proy->usuario_id)
                        ->where('proyecto_id', $usuario_proy->proyecto_id)
                        ->pluck('solicitudes.id')
                        ->toArray();
                    foreach($solicitudes_usu_proy as $sol_usu_proy){
                        $sol_ids[] = $sol_usu_proy;
                    }
                }
            }
        }elseif($usuario->rol_id >3 || $usuario->apr_parcial){
            $proyectos = Proyectos::where('empresa_id', session('empresa'))->select('proyectos.id')->pluck('proyectos.id');
            //controller, aprobador, tesoreria
            $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
                ->select('solicitudes.id')
                ->whereIn('gestion_id', session('gestion_id'))
                ->whereIn('proyecto_id', $proyectos)
                ->orderBy('solicitudes.created_at')
                ->groupBy('solicitudes.id')
                ->pluck('solicitudes.id')
                ->toArray();
        }

        if($request->tabs == 'autorizador'){
            $idSolicitudes = $this->seccionSolicitud('autorizacion-disponibles', $extra_query2);
        }elseif($request->tabs == 'aprobador'){
            $idSolicitudes = $this->seccionSolicitud('aprobador_parcial-disponibles', $extra_query2);
        }elseif($request->tabs == 'aprrendiciones'){
            $idSolicitudes = $this->seccionSolicitud('aprobar_rendiciones-rendiciones_aprobadas', $extra_query2);
        }elseif($request->tabs == 'revisor'){
            $idSolicitudes = $this->seccionSolicitud('revisor-disponibles', $extra_query2);
        }elseif($request->tabs == 'tesoreria'){
            $idSolicitudes = $this->seccionSolicitud('tesoreria-disponibles', $extra_query2);
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            // ->whereIn('id', $sol_ids)
            ->whereIn('id', $idSolicitudes)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        return view('solicitudes.disponibles')->withSolicitudes($solicitudes)->withConq($extra_query2);
        //return redirect()->action('SolicitudController@autorizar', ['tabs' => 'autorizador']);
        //, ['tabs' => 'revisor'

    }

    //Consultor
    public function consultor(Request $request){
        if(isset($_GET['id_empresa'])){
            $id_empresa = $_GET['id_empresa'];
            session()->forget('empresa');
            session::set('empresa' , $id_empresa);
            $empresa = Empresas::find(session('empresa'));
            //$gestion_search = Gestiones::all()->last();
            session(['empresa_nombre' => $empresa->nombre]);
            session(['empresa_slug' => $empresa->slug]);
            session(['gestion_id' => session('gestion_id')]);
            session(['gestion' => session('gestion')]);
            session(['paginacion' => 20]);
        }else{
            $id_empresa = session('empresa');
        }

        $usuario = auth()->user();

        $proyectos_ids = Proyectos::select('id')
            ->where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();
        $sol_ids = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'solicitudes.id')
            ->select('solicitudes.id')
            ->whereIn('gestion_id', session('gestion_id'))
            ->whereIn('solicitudes.proyecto_id', $proyectos_ids)
//            ->where('solicitudes.total', '>=', $monto_revisor)
            ->orderBy('solicitudes.created_at')
            ->groupBy('solicitudes.id')
            ->pluck('solicitudes.id')
            ->toArray();

        $solicitudes_ids = array();
        foreach($sol_ids as $sol_id){
            if(in_array($this->getLastEstadoCodigo($sol_id), ['REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'APR-RO', 'FIN'])){
                $solicitudes_ids[] = $sol_id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitudes_ids)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->costo * $array_item->cantidad
                ];

            }
            $solicitud->formulariosAF = false;
            $items_af = DB::select('SELECT COUNT(id) as cantidad FROM item_rendiciones WHERE solicitud_id = '.$solicitud->id.' AND tipo_id IN (1,7) ORDER BY created_at DESC');
            $cantidad = 0;
            foreach($items_af as $item_af){
                if($item_af->cantidad > $cantidad){
                    $cantidad = $item_af->cantidad;
                }
            }
            if($cantidad > 0){
                $solicitud->formulariosAF = true;
            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.usrConsultas.index')
            ->withSolicitudes($solicitudes)
            ->withUsuario($usuario);
    }

    public function buscadorConsultor(Request $request){
        $usuario = auth()->user();
        $add_query = "";
        //dd($request);
        switch ($request->selectOpcion) {
            case 0:
                if($request->buscar != ''){
                    $add_query = " AND (";
                    if(is_numeric($request->buscar)){
                        $add_query .= " numero = ".$request->buscar. " OR ";
                        $add_query .= " total = ".$request->buscar. " OR ";
                    }

                    //Nombre de Usuario
                    $str_ids_usuarios = "";
                    $cont_ids_usu = 0;
                    $usuarios = User::where('nombre', 'LIKE', '%'.$request->buscar.'%')->get();
                    foreach($usuarios as $usuario){
                        $str_ids_usuarios .= $usuario->id.",";
                        $cont_ids_usu++;
                    }
                    if(substr($str_ids_usuarios, -1) == ','){
                        $str_ids_usuarios = substr($str_ids_usuarios, 0, strlen($str_ids_usuarios)-1);
                    }

                    //$add_query .= " total = '%".$request->buscar."%'";
                    if($str_ids_usuarios != ''){
                        if(strlen($add_query) > 6){
                            $add_query .= " OR ";
                        }
                        $add_query .= "usuario_id IN (".$str_ids_usuarios.")";
                    }

                    $proyectos = Proyectos::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->where('empresa_id', session('empresa'))
                        ->pluck('id')
                        ->toArray();
                    if(count($proyectos)>0){
                        if(strlen($add_query) > 6){
                            $add_query .= " OR ";
                        }
                        $add_query .= "proyecto_id IN (".implode(', ',$proyectos).")";
                    }

                    if(strlen($add_query) > 6){
                        //$add_query .= " OR ";
                    }

                    $monedas = Monedas::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->pluck('id')
                        ->toArray();

                    $modalidades = Modalidades::select('id')
                        ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                        ->pluck('id')
                        ->toArray();

                    $add_query .= "created_at LIKE '%".$request->buscar."%'";
                    $add_query .= " OR desembolso LIKE '%".$request->buscar."%'";

                    if(count($monedas) > 0){
                        $add_query .= " OR moneda_id IN (".implode(',', $monedas).")";
                    }

                    if(count($modalidades) > 0){
                        $add_query .= " OR modalidad_id IN (".implode(',', $modalidades).")";
                    }

                    $add_query .= " OR referencia LIKE '%".$request->buscar."%'";
                    $add_query .= " OR observacion LIKE '%".$request->buscar."%')";
                }
                break;
            case 1:
                $add_query .= " AND numero = ".$request->buscar;
                break;
            case 2:
                $add_query .= " AND total LIKE '%".$request->numero."%'";
                break;
            case 3:
                $proyectos = Proyectos::select('id')
                    ->where('nombre', 'LIKE', '%' .$request->buscar. '%')
                    ->where('empresa_id', session('empresa'))
                    ->pluck('id')
                    ->toArray();
                if(count($proyectos)>0){
                    $add_query .= " AND proyecto_id IN (".implode(', ',$proyectos).")";
                }
                break;
            case 4:
                $add_query .= " AND created_at LIKE '%".$request->buscar."%'";
                break;
            case 5:
                //Nombre de Usuario
                $str_ids_usuarios = "";
                $cont_ids_usu = 0;
                $usuarios = User::where('nombre', 'LIKE', '%'.$request->buscar.'%')->get();
                foreach($usuarios as $usuario){
                    $str_ids_usuarios .= $usuario->id.",";
                    $cont_ids_usu++;
                }
                if(substr($str_ids_usuarios, -1) == ','){
                    $str_ids_usuarios = substr($str_ids_usuarios, 0, strlen($str_ids_usuarios)-1);
                }
                if($str_ids_usuarios != ''){
                    $add_query .= " AND usuario_id IN (".$str_ids_usuarios.")";
                }
                break;
            case 6:
                $add_query .= " AND desembolso LIKE '%".$request->buscar."%'";
                break;
            case 7:
                $add_query .= " AND referencia LIKE '%".$request->buscar."%'";
                break;
            case 8:
                $add_query .= " AND observacion LIKE '%".$request->buscar."%'";
                break;
        }
        $data['array_addquery'] = $add_query;
        $proyectos_ids = Proyectos::where('empresa_id', session('empresa'))
            ->select('id')
            ->pluck('id')
            ->toArray();
        $query = " WHERE gestion_id IN (".implode(',', session('gestion_id')).") AND proyecto_id IN (".implode(',', $proyectos_ids).") ".$add_query;
        $sol_ids = DB::select('SELECT id FROM solicitudes'.$query.' ORDER BY created_at DESC');
        $solicitudes_ids = array();
        foreach($sol_ids as $sol_id){
            if(in_array($this->getLastEstadoCodigo($sol_id->id), ['REN', 'REN-P', 'REN-F', 'REN-A', 'REN-R', 'REN-OA', 'REN-OB', 'REN-OC', 'APR-RO', 'FIN'])){
                $solicitudes_ids[] = $sol_id->id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitudes_ids)
            ->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->modalidad_id == 3){
                $solicitud->frNombre = $solicitud->frtipo_id;
            }
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }

        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        $items = array();
        foreach($solicitudes as $solicitud){
            $array_items = Items::where('solicitud_id', $solicitud->id)->get();
            $items_aux = array();
            foreach($array_items as $array_item){
                $items_aux[] = [
                    'solicitud_id' => $solicitud->id,
                    'tipo' => $array_item->unidad->descripcion,
                    'descripcion' => $array_item->detalle,
                    'cantidad' => $array_item->cantidad,
                    'costo' => $array_item->costo,
                    'subtotal' => $array_item->costo * $array_item->cantidad
                ];

            }
            $solicitud->formulariosAF = false;
            $items_af = DB::select('SELECT COUNT(id) as cantidad FROM item_rendiciones WHERE solicitud_id = '.$solicitud->id.' AND tipo_id IN (1,7) ORDER BY created_at DESC');
            $cantidad = 0;
            foreach($items_af as $item_af){
                if($item_af->cantidad > $cantidad){
                    $cantidad = $item_af->cantidad;
                }
            }
            if($cantidad > 0){
                $solicitud->formulariosAF = true;
            }
            $items[$solicitud->id] = $items_aux;
        }
        return view('solicitudes.usrConsultas.index', ['tabs' => 'revisor', 'id_empresa' => session('empresa')])
            ->withSolicitudes($solicitudes)
            ->withItems($items)
            ->withUsuario($usuario);
    }

    function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null){
        $perPage = 20;
        
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }

    function remover_acentos($str){
      $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
      $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
      return str_replace($a, $b, $str);
    }
}