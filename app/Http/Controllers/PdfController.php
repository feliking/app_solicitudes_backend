<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EstadoSolicitudes;
use App\ItemsRendiciones;
use App\Solicitudes;
use App\ItemsAf;
use App\User;

use App\Empresas;
use App\Proyectos;
use App\Gestiones;

use DB;

class PdfController extends Controller
{
    public function generar(){
        include_once (app_path().'/functions.php');
        $array_solicitud = array();
        $contador = 1;
        //id_empresa & gestion
        $id_empresa = session('empresa');
        $gestion_actual = session('gestion');
        //ID usuario actual
        $user = auth()->user();
        $id_usuario = $user->id;
        //nombre empresa
        $nom_emp = session('empresa_nombre');
        //

        //nombre proyecto
        $nombre_proy = "";
        $nombre_sub_proy = "";
        $query_proy = "SELECT * FROM `".$gestion_actual."_proyectos` WHERE id_proyecto = ".$array_res['id_proyecto'];
        $result_proy = mysqli_query($conexion, $query_proy);
        $array_proy = mysqli_fetch_array($result_proy);
        //nombre sub proyecto
        if($array_proy['id_padre'] != 0){
            $nombre_sub_proy = $array_proy['nombre'];
            $query_padre = "SELECT * FROM `".$gestion_actual."_proyectos` WHERE id_proyecto = ".$array_proy['id_padre'];
            $result_padre = mysqli_query($conexion, $query_padre);
            $array_padre = mysqli_fetch_array($result_padre);
            $nombre_proy = $array_padre['nombre'];
        }

        //items solicitud
        $query_items = "SELECT * FROM `".$gestion_actual."_items` WHERE id_solicitud = ".$id_solicitud;
        $result_items = mysqli_query($conexion, $query_items);
        $items_solicitud = array();
        while($array_items = mysqli_fetch_array($result_items)){
            $unidad = \App\Unidades::find($array_items['tipo']);
            $nombre_unidad = $unidad->descripcion;
            $items_solicitud[] = array(
                'tipo'=>$nombre_unidad,
                'descripcion'=>$array_items['descripcion'],
                'cantidad'=>$array_items['cantidad'],
                'costo'=>$array_items['costo'],
                'subtotal'=>$array_items['subtotal'],
            );
        }
        mysqli_close($conexion);
        $array_solicitud['proyecto'] = $nombre_proy;
        $array_solicitud['sub_proyecto'] = $nombre_sub_proy;

        //solicitante
        $id_solicitante = $array_res['id_usuario'];
        $nombre_solicitante = \App\User::all()->find($id_solicitante);
        $array_solicitud['solicitante'] = $nombre_solicitante->nombre." ".$nombre_solicitante->ap." ".$nombre_solicitante->am;

        //nombre empresa
        $array_solicitud['empresa'] = session('empresa_nombre');

        //id_solicitud
        $array_solicitud['id_solicitud'] = $array_res['id_solicitud'];

        //fecha
        $array_solicitud['fecha'] = $array_res['fecha_solicitud'];

        //hora
        $array_solicitud['hora'] = $array_res['hora_solicitud'];

        //fecha limite
        $array_solicitud['fecha_lim'] = $array_res['fecha_limite_solicitud'];

        //desembolso a
        $array_solicitud['desembolso'] = $array_res['desembolso'];

        //tipo de cambio
        $array_solicitud['tipo_cambio'] = $array_res['tipo_cambio'];

        //modalidad
        $array_solicitud['modalidad'] = 'GASTO DIRECTO';
        if($array_res['tipo_solicitud']==1){
            $array_solicitud['modalidad'] = 'SUJETO A RENDICIÓN';
        }

        //moneda
        $array_solicitud['moneda'] = 'BOLIVIANOS';
        if($array_res['moneda']==2){
            $array_solicitud['moneda'] = 'DOLARES';
        }

        //referencia
        $array_solicitud['referencia'] = $array_res['referencia'];

        //Observacion
        $array_solicitud['observacion'] = $array_res['observacion'];

        //total
        $array_solicitud['total'] = $array_res['total'];

        //total literal
        $array_solicitud['literal'] = numeroLiteral($array_res['total']);

        //items
        $array_solicitud['items'] = $items_solicitud;

        //return view('solicitudes.genpdf', ['solicitud_data' => $array_solicitud]); //comentar para generar pdf
        $pdf = PDF::loadView('solicitudes.genpdf',['solicitud_data' => $array_solicitud]);
        return $pdf->download(ucfirst($nom_emp->slug).'_sol_'.$id_solicitud.".pdf");
    }

    function imprimirSolicitud(){
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $solicitud->autorizador = $this->getUsuarioPorEstado(20, $solicitud->id); // estado para autorizador 20
        $solicitud->autorizadorHora = $this->getHoraPorEstado(20, $solicitud->id);
        $solicitud->controller = $this->getUsuarioPorEstado(30, $solicitud->id); // estado para controller 30
        $solicitud->controllerHora = $this->getHoraPorEstado(30, $solicitud->id); 
        $solicitud->aprobador = $this->getUsuarioPorEstado(40, $solicitud->id); // estado para aprobador 40
        $solicitud->aprobadorHora = $this->getHoraPorEstado(40, $solicitud->id);
        $solicitud->tesoreria = $this->getUsuarioPorEstado(50, $solicitud->id); // estado para aprobador 50
        $solicitud->tesoreriaHora = $this->getHoraPorEstado(50, $solicitud->id);

        if($solicitud->modalidad_id == 3){
            $modalidad = $solicitud->modalidad->nombre." (".\App\FondosRotativos::find(intval($solicitud->frtipo_id))->nombre.")";
        }else{
            $modalidad = $solicitud->modalidad->nombre;
        }
        $solicitud->modNombre = $modalidad;

        //aprobadores parciales
        $aprobadores_id = DB::table('aprobador')
            ->where('solicitud_id', $_GET['id_sol'])
            ->pluck('aprobador_id')
            ->toArray();

        $sol_estados = EstadoSolicitudes::where('solicitud_id', $solicitud->id)->get();
        $cad_estado = "";
        if(count(EstadoSolicitudes::where('solicitud_id', $solicitud->id)->where('estado_id', 40)->get()) == 0){
            $estado = EstadoSolicitudes::where('solicitud_id', $solicitud->id)->where('estado_id', 44)->orderBy('id', 'DESC')->first();
            if($estado){
                $cad_estado = $estado->observacion;
            }
        }

        $aprobadores = User::whereIn('id', $aprobadores_id)
            ->pluck('nombre');

        $solicitud->aprobadores = $aprobadores;
        $solicitud->apr_estado = $cad_estado;

        return \PDF::loadView('pdf.solicitud', compact('solicitud'))->stream('solicitud_'.$solicitud->numero.'.pdf');
        // return view('pdf.solicitud')->withSolicitud($solicitud);
    }

    function imprimirSolicitudTransferencia(){
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $solicitud->autorizador = $this->getUsuarioPorEstado(20, $solicitud->id); // estado para autorizador 20
        $solicitud->autorizadorHora = $this->getHoraPorEstado(20, $solicitud->id);
        $solicitud->controller = $this->getUsuarioPorEstado(30, $solicitud->id); // estado para controller 30
        $solicitud->controllerHora = $this->getHoraPorEstado(30, $solicitud->id); 
        $solicitud->aprobador = $this->getUsuarioPorEstado(40, $solicitud->id); // estado para aprobador 40
        $solicitud->aprobadorHora = $this->getHoraPorEstado(40, $solicitud->id);
        $solicitud->tesoreria = $this->getUsuarioPorEstado(50, $solicitud->id); // estado para aprobador 50
        $solicitud->tesoreriaHora = $this->getHoraPorEstado(50, $solicitud->id);

        if($solicitud->modalidad_id == 3){
            $modalidad = $solicitud->modalidad->nombre." (".\App\FondosRotativos::find(intval($solicitud->frtipo_id))->nombre.")";
        }else{
            $modalidad = $solicitud->modalidad->nombre;
        }
        $solicitud->modNombre = $modalidad;

        //aprobadores parciales
        $aprobadores_id = DB::table('aprobador')
            ->where('solicitud_id', $_GET['id_sol'])
            ->pluck('aprobador_id')
            ->toArray();

        $sol_estados = EstadoSolicitudes::where('solicitud_id', $solicitud->id)->get();
        $cad_estado = "";
        if(count(EstadoSolicitudes::where('solicitud_id', $solicitud->id)->where('estado_id', 40)->get()) == 0){
            $estado = EstadoSolicitudes::where('solicitud_id', $solicitud->id)->where('estado_id', 44)->orderBy('id', 'DESC')->first();
            if($estado){
                $cad_estado = $estado->observacion;
            }
        }

        $aprobadores = User::whereIn('id', $aprobadores_id)
            ->pluck('nombre');

        $solicitud->aprobadores = $aprobadores;
        $solicitud->apr_estado = $cad_estado;

        return \PDF::loadView('pdf.transferencia', compact('solicitud'))->stream('solicitud_'.$solicitud->numero.'.pdf');
    }

    function imprimirSolicitudCompleta(){
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $solicitud->autorizador = $this->getUsuarioPorEstado(20, $solicitud->id); // estado para autorizador 20
        $solicitud->autorizadorHora = $this->getHoraPorEstado(20, $solicitud->id);
        $solicitud->controller = $this->getUsuarioPorEstado(30, $solicitud->id); // estado para controller 30
        $solicitud->controllerHora = $this->getHoraPorEstado(30, $solicitud->id);
        $solicitud->tesoreria = $this->getUsuarioPorEstado(50, $solicitud->id); // estado para aprobador 50
        $solicitud->tesoreriaHora = $this->getHoraPorEstado(50, $solicitud->id);
        
        if($solicitud->modalidad_id == 3){
            $modalidad = $solicitud->modalidad->nombre." (".\App\FondosRotativos::find(intval($solicitud->frtipo_id))->nombre.")";
        }else{
            $modalidad = $solicitud->modalidad->nombre;
        }
        $solicitud->modNombre = $modalidad;

        //aprobadores parciales
        $aprobadores_id = DB::table('aprobador')
            ->where('solicitud_id', $_GET['id_sol'])
            ->pluck('aprobador_id')
            ->toArray();

        $sol_estados = EstadoSolicitudes::where('solicitud_id', $solicitud->id)->get();
        $cad_estado = "";
        if(count(EstadoSolicitudes::where('solicitud_id', $solicitud->id)->where('estado_id', 40)->get()) == 0){
            $estado = EstadoSolicitudes::where('solicitud_id', $solicitud->id)->where('estado_id', 44)->orderBy('id', 'DESC')->first();
            if($estado){
                $cad_estado = $estado->observacion;
            }
        }

        $aprobadores = User::whereIn('id', $aprobadores_id)
            ->pluck('nombre');

        $solicitud->aprobadores = $aprobadores;
        $solicitud->apr_estado = $cad_estado;

        return \PDF::loadView('pdf.solicitudCompleta', compact('solicitud'))->stream('solicitud_'.$solicitud->numero.'.pdf');
        // return view('pdf.solicitudCompleta')->withSolicitud($solicitud);
    }

    public function imprimirRendicionSolicitud(){
        $solicitud = Solicitudes::find($_GET['id_sol']);
        $solicitud->autorizador = $this->getUsuarioPorEstado(82, $solicitud->id); // autorizador solicitud rendida
        $solicitud->autorizadorHora = $this->getHoraPorEstado(82, $solicitud->id);

        return \PDF::loadView('pdf.rendicion', compact('solicitud'))->stream('rendicion_solicitud_'.$solicitud->numero.'.pdf');
    }

    public function imprimirFormularioAF(){
        $item_rendido = ItemsRendiciones::find($_GET['id_item_rendido']);
        $solicitud = Solicitudes::find($item_rendido->solicitud_id);
        
        $item = ItemsAf::where('item_rendido_id', $item_rendido->id)->get();
        
        switch($solicitud->proyecto->nivel){
            case 1:
                $nombre_proyecto = $solicitud->proyecto->nombre;
                break;
            case 2:
                $nombre_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                break;
            case 3:
                $nombre_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                break;
        }

        // return view('pdf.formularioAF')->withItem($item_af)->withSolicitud($solicitud);
        return \PDF::loadView('pdf.formularioAF', compact('item', 'solicitud'))->stream('item_formularioAF_'.$item_rendido->id.'.pdf');
    }

    public function imprimirFormularioItemsAF(){
        $items_rendidos = ItemsRendiciones::where('solicitud_id', $_GET['id_sol'])->get();
        $array_rendidos = array();
        foreach($items_rendidos as $item){
            $array_rendidos[] = $item->id;
        }
        $solicitud = Solicitudes::find($_GET['id_sol']);

        $item = ItemsAf::whereIn('item_rendido_id', $array_rendidos)->get();

        switch($solicitud->proyecto->nivel){
            case 1:
                $nombre_proyecto = $solicitud->proyecto->nombre;
                break;
            case 2:
                $nombre_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                break;
            case 3:
                $nombre_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                break;
        }
        // return view('pdf.formularioAF')->withItem($items_af)->withSolicitud($solicitud);
        return \PDF::loadView('pdf.formularioAF', compact('item', 'solicitud'))->stream('item_formularioAF.pdf');
    }

    public function getUsuarioPorEstado($estado_id, $solicitud_id){
        if($estado_id == 20){
            $usuarioPorEstado = User::join('estados_solicitud', 'estados_solicitud.usuario_id', '=', 'users.id')
                ->select('users.nombre')
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->whereIn('estados_solicitud.estado_id', [20,24])
                ->orderBy('estados_solicitud.created_at', 'DESC')
                ->first();
        }else{
            $usuarioPorEstado = User::join('estados_solicitud', 'estados_solicitud.usuario_id', '=', 'users.id')
                ->select('users.nombre')
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->where('estados_solicitud.estado_id', $estado_id)
                ->orderBy('estados_solicitud.created_at', 'DESC')
                ->first();
        }
        
        if(count($usuarioPorEstado) < 1){
            $cad_return = "";
            switch($estado_id){
                case 20:
                    $cad_return = "SIN AUTORIZADOR";
                    break;
                case 30:
                    $cad_return = "SIN REVISOR";
                    break;
                case 40:
                    $cad_return = "SIN APROBADOR";
                    break;
                case 50:
                    $cad_return = "SIN TESORERIA";
                    break;
            }
            return $cad_return;
        }else{
            return $usuarioPorEstado->nombre;
        }
    }

    public function getHoraPorEstado($estado_id, $solicitud_id){
        if($estado_id == 20){
            $usuarioPorEstado = User::join('estados_solicitud', 'estados_solicitud.usuario_id', '=', 'users.id')
                ->select('estados_solicitud.updated_at')
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->whereIn('estados_solicitud.estado_id', [20,24])
                ->orderBy('estados_solicitud.created_at', 'DESC')
                ->first();
        }else{
            $usuarioPorEstado = User::join('estados_solicitud', 'estados_solicitud.usuario_id', '=', 'users.id')
                ->select('estados_solicitud.updated_at')
                ->where('estados_solicitud.solicitud_id', $solicitud_id)
                ->where('estados_solicitud.estado_id', $estado_id)
                ->orderBy('estados_solicitud.created_at', 'DESC')
                ->first();
        }
        if(count($usuarioPorEstado) < 1){
            $cad_return = "";
            switch($estado_id){
                case 20:
                    $cad_return = "";
                    break;
                case 30:
                    $cad_return = "";
                    break;
                case 40:
                    $cad_return = "";
                    break;
                case 50:
                    $cad_return = "";
                    break;
            }
            return $cad_return;
        }else{
            return $usuarioPorEstado->updated_at;
        }
    }

    public function imprimirSolicitudes($ids , $variables)
    {        
        $solicitudes_ids = explode(',', $ids);
        if(count($solicitudes_ids) == 1 && $solicitudes_ids[0] == -1){
            return \PDF::loadView('pdf.solicitudes')->stream('solicitudes.pdf');
        }else{
            $filtros = explode(',', $variables);
            $id_empresa = $filtros[0];
            if($id_empresa == 0){
                $empresa = "TODAS";
            }else{
                $empresa = Empresas::find($id_empresa)->nombre;
            }
            $id_proyecto = $filtros[1];
            if($id_proyecto == 0){
                $proyecto = "TODOS";
            }else{
                $proyecto = Proyectos::find($id_proyecto)->nombre;
            }
            $id_gestion = $filtros[2];
            if ($id_gestion == 0) {
                $gestion = "TODAS";
            } else {
                $gestion = Gestiones::find($id_gestion)->gestion;
            }
            
            $monto_id = $filtros[3];
            $monto_ope_logico = "";
            switch ($monto_id) {
                case '0':
                    $monto_ope_logico = "IGUAL A";
                    break;
                case '1':
                    $monto_ope_logico = "MAYOR A";
                    break;
                case '2':
                    $monto_ope_logico = "MAYOR IGUAL A";
                    break;
                case '3':
                    $monto_ope_logico = "MENOR A";
                    break;
                case '4':
                    $monto_ope_logico = "MENOR IGUAL A";
                    break;
            }
            $monto = $filtros[4];

            if($monto != ''){
                $monto_filtro = $monto_ope_logico. ' '. $monto;
            }else{
                $monto_filtro = 'SIN MONTO';
            }
            
            // dd($id_proyecto);
            $desde = $filtros[5];
            $hasta = $filtros[6];

            $columna = $filtros[7];
            $descripcion_columna = "";
            switch ($columna) {
                case '0':
                    $descripcion_columna = "NINGUNO";
                    break;
                case '1':
                    $descripcion_columna = "SOLICITANTE";
                    break;
                case '2':
                    $descripcion_columna = "DESEMBOLSO";
                    break;
                case '3':
                    $descripcion_columna = "REFERENCIA";
                    break;
                case '4':
                    $descripcion_columna = "OBSERVACIÓN";
                    break;
                case '5':
                    $descripcion_columna = "NÚMERO DE SOLICITUD";
                    break;
                case '6':
                    $descripcion_columna = "TODOS";
                    break;
            }
            $texto = $filtros[8];
            $solicitudes = Solicitudes::whereIn('id', $solicitudes_ids)
                ->orderBy('gestion_id', 'DESC')
                ->orderBy('numero', 'DESC')
                ->orderBy('desembolso', 'ASC')
                ->orderBy('referencia', 'ASC')
                ->orderBy('observacion', 'ASC')
                ->orderBy('total', 'ASC')
                ->orderBy('modalidad_id', 'ASC')
                ->orderBy('moneda_id', 'ASC')
                ->orderBy('usuario_id', 'ASC')
                ->orderBy('proyecto_id', 'ASC')
                ->orderBy('cambio_id', 'ASC')
                ->orderBy('created_at', 'ASC')
                ->get();
            
            // Genera la estructura de centros de costos
            foreach($solicitudes as $solicitud){
                $solicitud->estado = app('App\Http\Controllers\SolicitudController')->getLastEstado($solicitud->id);
                $solicitud->empresa = $solicitud->proyecto->empresa->nombre;
                $solicitud->estadoCodigo = app('App\Http\Controllers\SolicitudController')->getLastEstadoCodigo($solicitud->id);
                if($solicitud->proyecto->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
                }elseif($solicitud->proyecto->padre->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }else{
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }
            }
            
            foreach ($solicitudes as $solicitud) {
                $solicitud->estadoCodigo = app('App\Http\Controllers\SolicitudController')->getLastEstadoCodigo($solicitud->id);
            }
            $tipo_solicitud = $filtros[9];
            $pdf = \PDF::loadView('pdf.solicitudes', array(
                        'solicitudes' => $solicitudes,
                        'empresa' => $empresa,
                        'proyecto' => $proyecto,
                        'gestion' => $gestion,
                        'monto_filtro' => $monto_filtro,
                        'desde' => $desde,
                        'hasta' => $hasta,
                        'columna' => $descripcion_columna,
                        'texto' => $texto,
                        'tipo_solicitud' => $tipo_solicitud,
                    ));
            $pdf->setOption('orientation', 'Landscape');
            $pdf->setOption('footer-center', 'Pagina [page] de [toPage]');        
            return $pdf->stream('solicitudes.pdf');

            // return view('pdf.solicitudes')
            //     ->with([
            //         'solicitudes' => $solicitudes,
            //         'empresa' => $empresa,
            //         'proyecto' => $proyecto,
            //         'monto_filtro' => $monto_filtro,
            //         'desde' => $desde,
            //         'hasta' => $hasta,
            //         'columna' => $descripcion_columna,
            //         'texto' => $texto,
            //         'tipo_solicitud' => $tipo_solicitud,
            //     ]);
        }
    }
}