<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitudes;
use App\Estados;
use Session;
use DB;

class EstadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "EstadosController@index";
        $usuario = auth()->user();
        $estados = Estados::all();
        foreach($estados as $estado){
            $total_sus = $total_bs = 0;
            $query = "SELECT SUM(s.total), e.codigo, e.descripcion, s.tipo_cambio, s.moneda";
            //$activos = DB::select($sql);
        }
        $solicitud = Solicitudes::where('empresa_id', session('empresa'))
            ->whereIn('gestion_id', session('gestion_id'))
            ->where('usuario_id', $usuario->id)
            ->get();

        return View('estados.index')->withEstados($estados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "EstadosController@create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "EstadosController@store";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "EstadosController@show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo "EstadosController@edit";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        echo "EstadosController@update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "EstadosController@destroy";
    }
}
