<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\User;
use App\BannedUser;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function username()
    {
        return 'usuario';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth('api')->user();

        $user->banned = BannedUser::where('user_id', $user->id)->get();

        return response()->json(compact('user'));
    }

     /**
     * Get a JWT via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = request(['usuario', 'password']);

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['message' => 'Usuario o contraseña incorrecto'], 401);
        }

        $user = $request->user('api');

        $user->banned = BannedUser::where('user_id', $user->id)->get();

        return response()->json(compact('token', 'user'));
    }

    // Autenticación Biométrica
    public function loginFingerPrint(Request $request)
    {
        $finger = \Config::get('custom.fingerprint_key');
        if ($finger === $request->password) {
            $user = User::where('usuario', $request->username)->first();
            if ($user) {
                $token = JWTAuth::fromUser($user);
                $user->banned = BannedUser::where('user_id', $user->id)->get();
            } else {
                return response()->json(['message' => 'Usuario o contraseña incorrecto'], 401);
            }
            return response()->json(compact('token', 'user'));
        } else {
            return response()->json(['message' => 'Ocurrió un error, intente ingresar con sus credenciales'], 401);
        }
        
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $token = auth()->refresh();

        return response()->json(compact('token'));
    }

    /**
     * Log the user out.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Salió del sistema']);
    }
}