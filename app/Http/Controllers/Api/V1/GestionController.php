<?php

namespace App\Http\Controllers\Api\V1;

use App\Gestiones;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Gestiones::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gestiones  $gestiones
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Gestiones::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gestiones  $gestiones
     * @return \Illuminate\Http\Response
     */
    public function edit(Gestiones $gestiones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gestiones  $gestiones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gestiones $gestiones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gestiones  $gestiones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gestiones $gestiones)
    {
        //
    }
}
