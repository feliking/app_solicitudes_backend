<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Documentos;

use Session;
use Response;

class DocumentosController extends Controller
{
    public function eliminarDoc($id, $gestion){
        $documento = Documentos::find($id);

        $path = public_path()."/empresas/".$documento->solicitud->proyecto->empresa->slug."/".$gestion."/solicitud/".$documento->direccion;
        unlink($path);

        $documento->delete();

        return Response::json("DOCUMENTO ELIMINADO");
    }
}
