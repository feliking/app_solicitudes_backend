<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /* CONTROLADOR QUE AYUDA A OBTENER INFORMACIÓN DEL USUARIO AUTENTICADO PARA SERVICIOS API
     * 
     * REVISOR
    */
    public function getRevisor (Request $request) {
        $user = User::with('revisa')->with('revisa_fr')->findOrFail($request->user_id);
        return $user;
    }
}
