<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\SolicitudController;

use App\Empresas;
use App\FondosRotativos;
use App\Solicitudes;
use App\Estados;
use App\Proyectos;

use Session;
use Response;

class JsonController extends Controller
{
    public function getCantidadEstados($empresa_id, $gestion_id){
        // if(session('gestion') < 2000){
        //     $gestion = date("Y");
        //     $gestion_id = Gestiones::where('gestion', $gestion)
        //         ->first()
        //         ->pluck('id');
        // }else{
        //     $gestion_id = session('gestion_id');
        // }

        // session(['empresa' => $empresa_id]);

        session::set('empresa' , $empresa_id);
        session::set('gestion_id' , [$gestion_id]);
        $empresa = Empresas::find(session('empresa'));
        // if (session('gestion_id') != null && session('gestion_id') != 0) {
        //     $gestion_search = Gestiones::find(session('gestion_id'));
        // } else {
        //     $gestion_search = Gestiones::all()->last();
        // }
        session(['empresa_nombre' => $empresa->nombre]);
        session(['empresa_slug' => $empresa->slug]);
        // session(['gestion_id' => $gestion_search->id]);
        // session(['gestion' => $gestion_search->gestion]);
        session(['paginacion' => 20]);

        // $empresa_id = session('empresa');
        
        $usuario = auth()->user();
        $cantidadEstados = array();

        // $estadosCurso = ['SOL', 'SOL-M', 'AUT', 'AUT-M', 'AUT-AF', 'REV', 'REV-M', 'APR', 'APR-P', 'APR-M', 'TES-M'];
        // $estadosObservados = ['AUT-O', 'REV-O', 'APR-0', 'TES-O', 'REN-OA', 'REN-OB', 'REN-OC'];
        // $estadosRechazadas = ['SOL-A', 'AUT-R', 'REV-R', 'APR-R', 'TES-R'];
        // $estadosRendiciones = ['TES', 'REN', 'REN-P'];
        // $estadosRendidas = ['REN-F', 'REN-A', 'REN-B', 'REN-C'];
        // $estadosFinalizadas = ['FIN'];

        $cantidadEstados = [
            'rol_id'        => $usuario->rol_id,
            'curso'         => count(SolicitudController::seccionSolicitud('usuario-en_curso')),
            'rendiciones'   => count(SolicitudController::seccionSolicitud('usuario-por_rendir')),
            'observadas'    => count(SolicitudController::seccionSolicitud('usuario-observadas')),
            'rechazadas'    => count(SolicitudController::seccionSolicitud('usuario-rechazadas')),
            'rendidas'      => count(SolicitudController::seccionSolicitud('usuario-rendidas')),
            'finalizadas'   => count(SolicitudController::seccionSolicitud('usuario-finalizadas')),
            //AprRendiciones
            'aprrendir'     => count(SolicitudController::seccionSolicitud('aprobar_rendiciones-aprobar_rendiciones')),
            'aprrendidas'   => count(SolicitudController::seccionSolicitud('aprobar_rendiciones-rendiciones_aprobadas')),
            //Autorizador
            'autorizadas'   => count(SolicitudController::seccionSolicitud('autorizacion-disponibles')),
            'porautorizar'  => count(SolicitudController::seccionSolicitud('autorizacion-autorizar')),
            //Revisor
            'revisadas'     => count(SolicitudController::seccionSolicitud('revisor-disponibles')),
            // 'porrevisar'    => count(SolicitudController::seccionSolicitud('revisor-revisor')),
            'porrevisarsr'    => count(SolicitudController::seccionSolicitud('revisor-revisor_sr')),
            'porrevisargd'    => count(SolicitudController::seccionSolicitud('revisor-revisor_gd')),
            'porrevisarrepo'    => count(SolicitudController::seccionSolicitud('revisor-revisor_repo')),
            'porrevisarfr'    => count(SolicitudController::seccionSolicitud('revisor-revisor_fra')),
            //'porrevisarfro'    => count(SolicitudController::seccionSolicitud('revisor-revisor_fro')),
            'porrevisarcc'     => count(SolicitudController::seccionFrSolicitud('revisor-revisor_cc')),
            'porrevisarfr' => count(SolicitudController::seccionSolicitud('revisor-revisor_fra')),  
            'porrevisarfra' => count(SolicitudController::seccionSolicitud('revisor-revisor_fra')),
            //'porrevisarfro' => count(SolicitudController::seccionSolicitud('revisor-revisor_fro')),    'porrevisarfro' => count(SolicitudController::seccionSolicitud('revisor-revisor_fro')),
            'porrevisarcc' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_cc')),     
            'porrevisarfra' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_fra')),       
            'porrevisarfro' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_fro')),       
            'porrevisarfrc' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_frc')),
            'porrevisarfrm' => count(SolicitudController::seccionFrSolicitud('revisor-revisor_frm')),
            //Aprobador
            'aprobadas'     => count(SolicitudController::seccionSolicitud('aprobador_parcial-disponibles')),
            'poraprobar'    => count(SolicitudController::seccionSolicitud('aprobador-aprobador')),
            //Tesoreria
            'tesoreria'     => count(SolicitudController::seccionSolicitud('tesoreria-disponibles')),
            'portesoreria'  => count(SolicitudController::seccionSolicitud('tesoreria-tesoreria')),
        ];

        return response()->json($cantidadEstados);
    }

    public function getDatosSolicitud($solicitud_id){
        $solicitud = Solicitudes::find($solicitud_id);
        $array_datos_solicitud = array();
        if(sizeof($solicitud) > 0){
            $estado = Estados::join('estados_solicitud', 'estados_solicitud.solicitud_id', 'estados.id')
                ->join('users', 'users.id', 'estados_solicitud.usuario_id')
                ->select('estados_solicitud.solicitud_id', 'estados_solicitud.usuario_id', 'estados_solicitud.created_at', 'users.nombre AS autorizador')
                ->where('solicitud_id', $solicitud_id)
                ->where('estado_id', 2)
                ->orderBy('estados_solicitud.created_at', 'DESC')
                ->first();

            $nombre = $solicitud->proyecto->nombre;
            switch($solicitud->proyecto->nivel){
                case 2:
                    $nombre = $solicitud->proyecto->padre->nombre." > ".$solicitud->proyecto->nombre;
                    break;
                case 3:
                    $nombre = $solicitud->proyecto->padre->padre->nombre." > ".$solicitud->proyecto->padre->nombre." > ".$solicitud->proyecto->nombre;
                    break;
            }

            if($solicitud->modalidad_id == 3){
                $fondo_rotativo = FondosRotativos::find(intval($solicitud->frtipo_id));
                $modalidad = $solicitud->modalidad->nombre." (".$fondo_rotativo->nombre.")";
            }else{
                $modalidad = $solicitud->modalidad->nombre;
            }

            $array_datos_solicitud = [
                "id" => $solicitud->id,
                "numero" => $solicitud->numero,
                "desembolso" => $solicitud->desembolso,
                "desembolso_antiguo" => $solicitud->desembolso_antiguo,
                "referencia" => $solicitud->referencia,
                "observacion" => $solicitud->observacion,
                "moneda" => $solicitud->moneda->nombre,
                "tipo_cambio" => $solicitud->tipo_cambio->cambio,
                "modalidad" => $modalidad,
                "modalidad_id" => $solicitud->modalidad->id,
                "fecha_limite" => ($solicitud->fecha_limite)?$solicitud->fecha_limite:'SIN FECHA LIMITE',
                "total" => $solicitud->total,
                "usuario" => $solicitud->usuario->nombre,
                "proyecto" => $nombre,
                "proyecto_id" => $solicitud->proyecto_id,
                "empresa" => $solicitud->proyecto->empresa->nombre,
                "empresa_id" => $solicitud->proyecto->empresa_id,
                "last_estado_id" => $solicitud->estados->last()->estado_id,
                "fondo_id" => isset($fondo_rotativo) ? $fondo_rotativo->id : ''
            ];

            if ($solicitud->transferencia != null)
            {
                $array_datos_solicitud['banco_origen'] = $solicitud->transferencia->cuentaOrigen->banco;
                $array_datos_solicitud['cuenta_origen'] = $solicitud->transferencia->cuentaOrigen;
                $array_datos_solicitud['empresa_destino'] = $solicitud->transferencia->cuentaDestino->empresa;
                $array_datos_solicitud['banco_destino'] = $solicitud->transferencia->cuentaDestino->banco;
                $array_datos_solicitud['cuenta_destino'] = $solicitud->transferencia->cuentaDestino;
                $array_datos_solicitud['tipo_transferencia'] = $solicitud->transferencia->tipoTransferencia;
                $array_datos_solicitud['monto'] = $solicitud->transferencia->monto;
                $array_datos_solicitud['factor'] = $solicitud->transferencia->factor;
            }
        }
        return Response::json($array_datos_solicitud);
    }

    public function getProyectosEmpresa($empresa_id = 0){
        $usuario = auth()->user();
        if($empresa_id != 0){
            if($usuario->rol_id == 2 && $usuario->apr_parcial == false && $usuario->id != 32){
                $proyectos =  Proyectos::whereHas('usuarios', function ($query) use($usuario){
                    $query->where('user_id', $usuario->id);
                    $query->whereIn('nivel', [2,3]);
                })
                    ->where('empresa_id', session('empresa'))
                    ->get();
            }else{
                //$proyectos = Proyectos::has('hijo', '>', 0)
                $proyectos = Proyectos::whereIn('nivel', [2,3])
                    ->where('empresa_id', $empresa_id)
                    ->get();
            }
        }else{
            //$proyectos = Proyectos::has('hijo', '>', 0)->get();
            $proyectos = Proyectos::whereIn('nivel', [2,3])
                ->get();
        }

        foreach($proyectos as $proyecto){
            switch($proyecto->nivel){
                case 2:
                    $proyecto->nombre = $proyecto->padre->nombre." > ".$proyecto->nombre;
                    break;
                case 3:
                    $proyecto->nombre = $proyecto->padre->padre->nombre." > ".$proyecto->padre->nombre." > ".$proyecto->nombre;
                    break;
            }
        }

        /* $proyectos = $proyectos->sortBy(
            function($proy){
                return $proy->nombre;
            }
        ); */

        return Response::json($proyectos);
    }

    public function getCantidadEstadosGlobal(Request $request){
        $section = $request->section;
        $usuario = auth()->user();
        $cantidadEstados = array();

        if ($section === 'global-usuario') {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Usuario
                'curso'         => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-en_curso', $request->all())),
                'rendiciones'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-por_rendir')),
                'observadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-observadas')),
                'rechazadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rechazadas')),
                'rendidas'      => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rendidas')),
                'finalizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-finalizadas')),
            ];
        } else if ($section === 'global-autorizar') {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Autorizador
                'autorizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-disponibles')),
                'porautorizar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-autorizar')),
            ];
        } else if ($section === 'global-aprobar_rendiciones') {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Aprobador rendiciones
                'aprrendir'     => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-aprobar')),
                'aprrendidas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-disponibles')),
            ];
        } else if ($section === 'global-aprobar') {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Aprobador
                'aprobadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-disponibles')),
                'poraprobar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-aprobar')),
            ];
        } else if ($section === 'global-pendientes') {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                'pendientes'    => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes')),
                'pendientes2'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes2')),
                'pendientes3'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes3')),
                'pendientes4'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes4')),
                'pendientes5'   => count(SolicitudGlobalController::seccionSolicitudGlobal('pendientes5')),
            ];
        } else if ($section === 'global-revisor') {
            $cantidadEstados = [
                'rol_id'            => $usuario->rol_id,
                'sujeto_rendicion'  => count(SolicitudGlobalController::seccionFrSolicitudGlobal('sujeto_rendicion')),
                'gasto_directo'     => count(SolicitudGlobalController::seccionFrSolicitudGlobal('gasto_directo')),
                'fondo_rotativo'    => count(SolicitudGlobalController::seccionFrSolicitudGlobal('fondo_rotativo')),
                'reposicion'        => count(SolicitudGlobalController::seccionFrSolicitudGlobal('reposicion')),
            ];
        } else {
            $cantidadEstados = [
                'rol_id'        => $usuario->rol_id,
                // Usuario
                'curso'         => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-en_curso')),
                'rendiciones'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-por_rendir')),
                'observadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-observadas')),
                'rechazadas'    => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rechazadas')),
                'rendidas'      => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-rendidas')),
                'finalizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('usuario-finalizadas')),
                // Autorizador
                'autorizadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-disponibles')),
                'porautorizar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('autorizacion-autorizar')),
                // Aprobador rendiciones
                'aprrendir'     => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-aprobar')),
                'aprrendidas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('rendicion-disponibles')),
                // Aprobador
                'aprobadas'   => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-disponibles')),
                'poraprobar'  => count(SolicitudGlobalController::seccionSolicitudGlobal('aprobacion-aprobar')),
            ];
        }

        return $cantidadEstados;
    }
}
