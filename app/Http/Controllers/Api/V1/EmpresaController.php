<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Modelos
use App\Gestiones;
use App\Empresas;
use App\Proyectos;

class EmpresaController extends Controller
{
    public function seleccion_empresa(){
        //********REVISAR PARA AUTORIZADOR

        session()->forget('empresa');
        session()->forget('empresa_nombre');
        session()->forget('empresa_slug');
        session()->forget('paginacion');
        session()->forget('gestion_id');
        session()->forget('gestion');

        $usuario = auth()->user();
        /* $gestion_search = Gestiones::all()->last();
        session(['gestion_id' => $gestion_search->id]);
        session(['gestion' => $gestion_search->gestion]);
        session(['paginacion' => 20]); */
        $gestiones = Gestiones::orderBy("id", "DESC")->get();

        $ultimas_gestiones = Gestiones::orderBy('id', 'DESC')
            ->take(2)
            ->get();

        if($usuario->rol_id == 8){
            // session(['empresa' => null]);
            $empresa = Empresas::find(session('empresa'));
            return redirect()->route('solicitudes.admin');
        } else if ($usuario->global_view == 1) {
            return redirect()->route('global.en_curso');
        }else{
            if(($usuario->rol_id == 1 || $usuario->rol_id >= 4 || $usuario->revisor == true) && ($usuario->rol_id != 7)){
                $empresas = Empresas::where('estado', true)->get();
                //$empresas = Empresas::orderBy('nombre')->get();
            }else{
                if($usuario->rol_id == 7){
                    $empresas_aprrendiciones = $usuario->aprrendiciones_empresas;
                    $user_id = $usuario->id;
                    $array_empresas = array();
                    foreach($empresas_aprrendiciones as $empresa){
                        $array_empresas[] = $empresa->id;
                    }
                    $empresas = Empresas::whereIn('id', $array_empresas)->where('estado', true)->get();
                }else{
                    $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                        $query->where('user_id', $usuario->id);
                    })->pluck('id')->toArray();

                    $empresas = Empresas::whereHas('proyectos', function ($query) use($proyectos){
                        $query->whereIn('id', $proyectos);
                    })->get();
                }
            }
        }
        if(count($empresas) == 0 && $usuario->rol_id == 1){
            return view('empresas.crear');
        }
        
        return response()->json([
            'ultimas_gestiones' => $ultimas_gestiones,
            'gestiones' => $gestiones,
            'empresas' => $empresas
        ]);
    }

    public function index () {
        return Empresas::get();
    }

    public function show ($id) {
        return Empresas::findOrFail($id);
    }
}
