<?php

namespace App\Http\Controllers;

use App\Transferencia;
use Illuminate\Http\Request;

// Carbon
use Carbon\Carbon;

use App\CuentaBancaria;
use App\Empresas;
use App\FactorTipoCambio;
use App\ItemsTipos;
use App\Modalidades;
use App\Monedas;
use App\Proyectos;
use App\TiposCambios;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class TransferenciaController extends Controller
{
    public function getCuentasBancariasPorMoneda (Request $request)
    {
        // Cuentas bancarias de origen
        $cuentas_bancarias_origen = CuentaBancaria::where([
            ['empresa_id', '=', session('empresa')],
            ['moneda_id', '=', $request->moneda_id] // BOLIVIANOS
        ])
            ->with('banco')
            ->with('moneda')
            ->get();

        return Response::json($cuentas_bancarias_origen);
    }

    public function getCuentasBancariasPorEmpresa (Request $request)
    {
        // Cuentas bancarias de destino
        $cuentas_bancarias_destino = CuentaBancaria::where([
            ['empresa_id', '=', $request->empresa_id],
        ])
            ->with('banco')
            ->with('moneda')
            ->get();

        return Response::json($cuentas_bancarias_destino);
    }

    public function getFactor (Request $request)
    {
        $factor = FactorTipoCambio::where([
            ['moneda_origen_id', '=', $request->moneda_origen_id],
            ['moneda_destino_id', '=', $request->moneda_destino_id],
        ])
            ->first();

            return Response::json($factor);
    }
    
    public function create()
    {
        $usuario = auth()->user();
        $modalidad_transferencia = Modalidades::where('oculto', true)->first();
        $monedas = Monedas::all();
        $tipo_cambio = TiposCambios::moneda(2)->get()->last()->toArray();
        $tipo_cambio_euro = TiposCambios::moneda(3)->get()->last()->toArray();

        $proyectos = Proyectos::where('empresa_id', session('empresa'))
            ->get();

        foreach($proyectos as $proyecto){
            switch($proyecto->nivel){
                case 2:
                    $proyecto->nombre = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
                case 3:
                    $proyecto->nombre = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
                    break;
            }
        }

        // Cuentas bancarias de origen
        $cuentas_bancarias_origen = CuentaBancaria::where([
            ['empresa_id', '=', session('empresa')],
            ['moneda_id', '=', 1] // BOLIVIANOS
        ])
            ->get();

        // Empresas destino
        $empresas = Empresas::get();

        // Tipos de transferencia
        $tipos_transferencia = ItemsTipos::whereIn('id', [3, 8, 9])
            ->get();

        // Factores de cambio
        $factores = FactorTipoCambio::where([
            ['factor', '!=', 1]
        ])
            ->orderBy('moneda_origen_id', 'ASC')
            ->get();

        if(count($proyectos) == 0){
            Session::flash('success', 'No cuenta con proyectos asignados');

            return redirect()->route('solicitudes');
        }else{
            $controller = false;

            $proyectos = $proyectos->sortBy(
                function($proy){
                    return $proy->nombre;
                }
            );

            return view('solicitudes.tesoreria.transferencia.crear')
                ->with([
                    'cambio' => $tipo_cambio,
                    'cambio_euro' => $tipo_cambio_euro,
                    'monedas' => $monedas,
                    'controller' => $controller,
                    'proyectos' => $proyectos,
                    'modalidad_transferencia' => $modalidad_transferencia,
                    'user' => $usuario,
                    'date' => Carbon::now()->format('Y-m-d'),
                    'cuentas_bancarias_origen' => $cuentas_bancarias_origen,
                    'empresas' => $empresas,
                    'tipos_transferencia' => $tipos_transferencia,
                    'factores' => $factores,
                ]);
        }
    }

    public function report (Request $request)
    {
        // dd($request->all());

        // dd(session());
        
        $empresas = Empresas::orderBy('id', 'ASC')
            ->get();

        if (\Auth::user()->rol_id == 8) // Presidencia
        {
            if (isset($request->empresa_id))
            {
                $empresa_origen = $request->empresa_id;
            }
            else
            {
                $empresa_origen = null;
            }
        }
        else
        {
            $empresa_origen = session('empresa');
        }

        $cuentas_disponibles = CuentaBancaria::where([
            ['empresa_id', '=', $empresa_origen]
        ])
            ->get();

        return view('solicitudes.tesoreria.transferencia.report')
            ->with([
                'empresas' => $empresas,
                'cuentas_disponibles' => $cuentas_disponibles,
                'filters' => $request->all(),
            ]);
    }

    public function reportExcel (Request $request)
    {
//         dd($request->all());

        if (\Auth::user()->rol_id == 8) // Presidencia
        {
            if (isset($request->empresa_id))
            {
                $empresa_origen = $request->empresa_id;
            }
            else
            {
                $empresa_origen = null;
            }
        }
        else
        {
            $empresa_origen = session('empresa');
        }

        if($empresa_origen != null)
            $empresa_origen_o = Empresas::find($empresa_origen);
        $cuentas_disponibles = CuentaBancaria::where([
            ['empresa_id', '=', $empresa_origen]
        ])
            ->get();
        $data = [];
        array_push($data, ['BANCO', 'CUENTA', 'TIPO', 'EMPRESA', 'BANCO ORIGEN/DESTINO', 'CUENTA ORIGEN/DESTINO', 'MONEDA', 'INGRESO', 'EGRESO']);
        foreach ($cuentas_disponibles as $cuenta_origen){
            $total_egresos = 0;
            $total_ingresos = 0;

            if (isset($request['origen_destino_empresa_id']) && $request['origen_destino_empresa_id'] != 0)
            {
                $transferencias2 = Transferencia::join('cuenta_bancarias', 'transferencias.cuenta_origen_id', '=', 'cuenta_bancarias.id')
                    ->where([
                        ['transferencias.aprobado', '=', 1],
                        ['transferencias.cuenta_destino_id', '=', $cuenta_origen->id],
                        ['cuenta_bancarias.empresa_id', '=', $request['origen_destino_empresa_id']]
                    ])
                    ->select('transferencias.*')
                    ->orderBy('transferencias.created_at', 'ASC')
                    ->get();
            }
            else
            {
                $transferencias2 = Transferencia::where([
                    ['aprobado', '=', 1],
                    ['cuenta_destino_id', '=', $cuenta_origen->id]
                ])
                    ->orderBy('created_at', 'ASC')
                    ->get();
            }

            if (count($transferencias2) > 0){
                foreach ($transferencias2 as $transferencia){
                    $cuenta_o = $transferencia->cuentaOrigen;
                    $banco_origen = $cuenta_o->banco;

                    $empresa_origen = $cuenta_o->empresa;

                    $total_ingresos += $transferencia->monto * $transferencia->factor;

                    array_push($data,[
                        $cuenta_origen->banco->nombre,
                        'CTA.'.$cuenta_origen->numero.' ['.$cuenta_origen->moneda->sigla.']',
                        $transferencia->tipoTransferencia->descripcion,
                        $empresa_origen->nombre,
                        $banco_origen->nombre,
                        'CTA. '.$cuenta_o->numero.' ['.$cuenta_o->moneda->sigla.']',
                        $cuenta_origen->moneda->sigla,
                        $transferencia->monto * $transferencia->factor,
                        '',
                    ]);
                }
            }

            if (isset($request['origen_destino_empresa_id']) && $request['origen_destino_empresa_id'] != 0)
            {
                $transferencias = \App\Transferencia::join('cuenta_bancarias', 'transferencias.cuenta_destino_id', '=', 'cuenta_bancarias.id')
                    ->where([
                        ['transferencias.aprobado', '=', 1],
                        ['transferencias.cuenta_origen_id', '=', $cuenta_origen->id],
                        ['cuenta_bancarias.empresa_id', '=', $request['origen_destino_empresa_id']]
                    ])
                    ->select('transferencias.*')
                    ->orderBy('transferencias.created_at', 'ASC')
                    ->get();
            }
            else
            {
                $transferencias = \App\Transferencia::where([
                    ['aprobado', '=', 1],
                    ['cuenta_origen_id', '=', $cuenta_origen->id]
                ])
                    ->orderBy('created_at', 'ASC')
                    ->get();
            }

            if (count($transferencias) > 0){
                foreach ($transferencias as $transferencia){
                    $cuenta_destino = $transferencia->cuentaDestino;
                    $banco_destino = $cuenta_destino->banco;
                    $empresa_destino = $cuenta_destino->empresa;

                    $total_egresos += $transferencia->monto;

                    array_push($data,[
                        $cuenta_origen->banco->nombre,
                        'CTA.'.$cuenta_origen->numero.' ['.$cuenta_origen->moneda->sigla.']',
                        $transferencia->tipoTransferencia->descripcion,
                        $empresa_destino->nombre,
                        $banco_destino->nombre,
                        'CTA. '.$cuenta_destino->numero.' ['.$cuenta_destino->moneda->sigla.']',
                        $cuenta_origen->moneda->sigla,
                        '',
                        $transferencia->monto,

                    ]);
                }
            }

        }

    //    dd($data);
        Excel::create('Reporte de Transferencias ('.$empresa_origen_o->nombre.')_'.date('dmY'), function ($excel) use ($data) {
            $excel->sheet('Hoja 1', function ($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download('xls');

//        return view('solicitudes.tesoreria.transferencia.report')
//            ->with([
//                'empresas' => $empresas,
//                'cuentas_disponibles' => $cuentas_disponibles,
//                'filters' => $request->all(),
//            ]);
    }

    public function reportPDF (Request $request)
    {
        // dd($request->all());

        // dd(session());

        $empresas = Empresas::orderBy('id', 'ASC')
            ->get();

        if (\Auth::user()->rol_id == 8) // Presidencia
        {
            if (isset($request->empresa_id))
            {
                $empresa_origen = $request->empresa_id;
            }
            else
            {
                $empresa_origen = null;
            }
        }
        else
        {
            $empresa_origen = session('empresa');
        }

        $cuentas_disponibles = CuentaBancaria::where([
            ['empresa_id', '=', $empresa_origen]
        ])
            ->get();


        if($empresa_origen != null)
            $empresa_origen_o = Empresas::find($empresa_origen);

        return \PDF::loadView('solicitudes.tesoreria.transferencia.pdf.report', compact('empresa_origen_o','cuentas_disponibles', 'filters'))->stream('Reporte de Transferencia_'.date('dmY').'.pdf');

//        return view('solicitudes.tesoreria.transferencia.report')
//            ->with([
//                'empresas' => $empresas,
//                'cuentas_disponibles' => $cuentas_disponibles,
//                'filters' => $request->all(),
//            ]);
    }
}
