<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ItemsRendiciones;
use App\ItemsController;
use App\UsuarioProyecto;
use App\CuentaBancaria;
use App\TiposCambios;
use App\Devoluciones;
use App\Solicitudes;
use App\Modalidades;
use App\ItemsTipos;
use App\Documentos;
use App\Proyectos;
use App\Gestiones;
use App\Empresas;
use App\Unidades;
use App\Monedas;
use App\Estados;
use App\ItemsAf;
use App\Items;
use App\User;

use Session;
use DB;

// Carbon
use Carbon\Carbon;

// Auth
use Illuminate\Support\Facades\Auth;

class ReportesController extends Controller
{
    public function dashboard(){

        $hoy = Carbon::now()->format('Y-m-d');
        $semana = Carbon::now()->subWeek()->format('Y-m-d');
        $mes = Carbon::now()->subMonth()->format('Y-m-d');

        $empresas = Empresas::orderBy('nombre')
            ->get();

        return view('solicitudes.reportes.dashboard')
            ->with('empresas', $empresas)
            ->with('hoy', $hoy)
            ->with('semana', $semana)
            ->with('mes', $mes);
    }

    public function reporteGeneral($search = '*'){
        $usuario = auth()->user();
        $filtros = "";
        $filtros_array = array();

        if(!isset($array_addquery)){
            $array_addquery = '';
        }

        if(!isset($result)){
            //id_empresa & gestion
            $gestion_actual = session('gestion_actual');

            //Carpeta Empresa

        }
        $array_proyectos = Proyectos::where('empresa_id', session('empresa'))
            ->pluck('id')
            ->toArray();

        $var = ['SOL', 'SOL-M',
            'AUT','AUT-M', 'AUT-AF', 'AUT-R',
            'REV', 'REV-M', 'REV-R',
            'APR', 'APR-M', 'APR-P', 'APR-R',
            'TES', 'REN-F', 'TES-R',
            'FIN'];
        $tituloSeleccionSolicitud = "En curso";
        if(isset($_GET['t'])){
            switch($_GET['t']){
                case 'observadas':
                    $var = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];
                    $tituloSeleccionSolicitud = "Observadas";
                    $filtros_array[] = "ESTADO: OBSERVADAS";
                    break;
                case 'rechazadas':
                    $var = ['AUT-R', 'REV-R', 'APR-R', 'TES-R'];
                    $tituloSeleccionSolicitud = "Rechazadas";
                    $filtros_array[] = "ESTADO: RECHAZADAS";
                    break;
                case 'rendidas':
                    $var = ['REN-F'];
                    $tituloSeleccionSolicitud = "Rendidas";
                    $filtros_array[] = "ESTADO: RENDIDAS";
                    break;
                case 'finalizadas':
                    $var = ['FIN'];
                    $tituloSeleccionSolicitud = "Finalizadas";
                    $filtros_array[] = "ESTADO: FINALIZADAS";
                    break;
            }
        }
        if($usuario->rol_id == 8){
            //$empresas = Empresas::orderBy('id', 'ASC')->pluck('id');
            $solicitudes = Solicitudes::whereIn('proyecto_id', $array_proyectos)
                ->whereIn('gestion_id', session('gestion_id'))
                ->get();
        }else{
            if($usuario->rol_id < 4){
                $proyectos = Proyectos::whereHas('usuarios', function ($query) use($usuario){
                    $query->where('user_id', $usuario->id);
                })
                    ->where('empresa_id', session('empresa'))
                    ->pluck('id')->toArray();
            }else{
                $proyectos = Proyectos::where('empresa_id', session('empresa'))
                    ->pluck('id')
                    ->toArray();
            }

            $solicitudes = Solicitudes::whereIn('proyecto_id', $proyectos)
                ->where('usuario_id', $usuario->id)
                ->get();
        }

        $solicitud_ids = array();
        foreach($solicitudes as $solicitud){
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if(in_array($solicitud->estadoCodigo, $var)){
                $solicitud_ids [] = $solicitud->id;
            }
        }

        $solicitudes = Solicitudes::sortable()
            ->orderBy('numero', 'DESC')
            ->orderBy('desembolso', 'ASC')
            ->orderBy('referencia', 'ASC')
            ->orderBy('observacion', 'ASC')
            ->orderBy('total', 'ASC')
            ->orderBy('modalidad_id', 'ASC')
            ->orderBy('moneda_id', 'ASC')
            ->orderBy('usuario_id', 'ASC')
            ->orderBy('proyecto_id', 'ASC')
            ->orderBy('cambio_id', 'ASC')
            ->whereIn('id', $solicitud_ids)->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){
            if($_GET['order'] == 'asc'){
                $solicitudes = $solicitudes->sortBy('estado');
            }else{
                $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
            }
        }
        $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');

        return view('solicitudes.reportes.reporteGral')
            ->withSolicitudes($solicitudes)
            ->withFiltros($filtros);
    }

    public function reporteGeneralUsuario(Request $request, $search = '*'){
        $total_solicitudes_bol = 0;
        $total_solicitudes_dol = 0;
        $solicitud_ids = array();
        $usuario = auth()->user();

        if ($usuario->global_view == 0 && $usuario->id != 32){
            $empresas = Empresas::where('id', session('empresa'))->get();
        } else {
            $empresas = Empresas::all();
        }
        $filtros = "";
        // Gestiones
        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        if($request->id_empresa == '' && $request->proyecto_id == '' && $request->gestion_id == '' && $request->monto == '' && $request->text == '' ){
            $solicitudes = new Solicitudes;
        }else{
            $filtros_array = array();

            $query_array = array();
            if($request->fecha_ini != '' && $request->fecha_fin != ''){
                $query_array[] = " created_at BETWEEN CAST('".$request->fecha_ini."' AS DATE) AND CAST('".$request->fecha_fin."' AS DATE)";
                $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
            }elseif($request->fecha_ini != ''){
                $query_array[] = " created_at LIKE '%".$request->fecha_ini."%'";
                $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
            }

            if($request->proyecto_id != 0){
                $query_array[] = " proyecto_id = ".$request->proyecto_id;
                $proyecto = Proyectos::find($request->proyecto_id);
                $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
            }elseif($request->empresa_id != 0){
                $proyectos_id = Proyectos::where('empresa_id', $request->empresa_id)
                    ->pluck('id')
                    ->toArray();
                $query_array[] = " proyecto_id IN (".implode(',', $proyectos_id).")";
            }

            if($request->monto > 0){
                $selMonto = intVal($request->selectMonto);
                switch($selMonto){
                    case 0 :
                        //igual
                        $query_array[] = " total = ".$request->monto;
                        $filtros_array[] = "MONTO IGUAL A: ".$request->monto;
                        break;
                    case 1 :
                        //mayor que
                        $query_array[] = " total > ".$request->monto;
                        $filtros_array[] = "MONTO MAYOR A: ".$request->monto;
                        break;
                    case 2 :
                        //mayor igual que
                        $query_array[] = " total >= ".$request->monto;
                        $filtros_array[] = "MONTO MAYOR IGUAL A: ".$request->monto;
                        break;
                    case 3 :
                        //menor que
                        $query_array[] = " total < ".$request->monto;
                        $filtros_array[] = "MONTO MENOR A: ".$request->monto;
                        break;
                    case 4 :
                        //menor igual que
                        $query_array[] = " total <= ".$request->monto;
                        $filtros_array[] = "MONTO MENOR IGUAL A: ".$request->monto;
                        break;
                }
            }

            if($request->texto != null){
                $selTexto = intVal($request->campo);
                $cad_auxiliar = "";
                switch($selTexto){
                    case 1 :
                        //solicitante
                        $request->texto = mb_strtoupper($request->texto);
                        $arrayUsers = User::where('nombre', 'LIKE', "%$request->texto%")
                            ->pluck('id')
                            ->toArray();
                        if(count($arrayUsers) > 0){
                            $query_array[] = " usuario_id IN (".implode(',', $arrayUsers).")";
                        }
                        $cad_auxiliar = "SOLICITANTE:";
                        break;
                    case 2 :
                        //desembolso
                        $query_array[] = " desembolso LIKE '%".$request->texto."%'";
                        $cad_auxiliar = "DESEMBOLSO:";
                        break;
                    case 3 :
                        //referencia
                        $query_array[] = " referencia LIKE '%".$request->texto."%'";
                        $cad_auxiliar = "REFERENCIA:";
                        break;
                    case 4 :
                        //observacion
                        $query_array[] = " observacion LIKE '%".$request->texto."%'";
                        $cad_auxiliar = "OBSERVACION:";
                        break;
                    case 5 :
                        //numero
                        $numero = "";
                        if(is_numeric($request->texto)){
                            $numero = " numero = ".$request->texto;
                        }
                        $query_array[] = "( ".$numero." )";
                        $cad_auxiliar = "NÚMERO:";
                        break;
                    case 6 :
                        //todos
                        $numero = "";
                        if(is_numeric($request->texto)){
                            $numero = " numero = ".$request->texto." OR ";
                        }
                        $query_array[] = "( ".$numero." referencia LIKE '%".$request->texto."%' OR desembolso LIKE '%".$request->texto."%' OR observacion LIKE '%".$request->texto."%')";
                        $cad_auxiliar = "TODOS:";
                        break;
                }
                $filtros_array[] = $cad_auxiliar." ".$request->texto;
            }

            $query = "";
            $first = true;
            foreach($query_array as $query_item){
                if($first){
                    $query .= " WHERE".$query_item;
                    $first = false;
                }else{
                    $query .= " AND".$query_item;
                }
            }

            if($usuario->rol_id == 8){
                if($query == ""){
                    if ($request->gestion_id != 0) {
                        $solicitudes = Solicitudes::where('gestion_id', $request->gestion_id)
                            ->orderBy('created_at', 'DESC')
                            ->get();
                    } else {
                        $solicitudes = Solicitudes::orderBy('created_at', 'DESC')
                            ->get();
                    }
                } else {
                    if ($request->gestion_id != 0) {
                        $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' AND gestion_id = '.$request->gestion_id.' ORDER BY created_at DESC');
                    } else {
                        $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' ORDER BY created_at DESC');
                    }
                }
            } elseif ($usuario->rol_id == 5 || $usuario->rol_id == 4 || $usuario->apr_parcial || $usuario->id == 32) {
                if ($usuario->global_view == 0 && $usuario->id != 32) {
                    $proyectos_array = Proyectos::where('empresa_id', session('empresa'))
                        ->pluck('id')
                        ->toArray();
                } else {
                    $proyectos_array = Proyectos::where('empresa_id', $request->empresa_id)
                        ->pluck('id')
                        ->toArray();
                }

                if($query == ""){
                    if ($request->gestion_id != 0) {
                        if (count($proyectos_array) == 0) {
                            $solicitudes = Solicitudes::where('gestion_id', $request->gestion_id)
                                ->orderBy('created_at', 'DESC')
                                ->get();
                        } else {
                            $solicitudes = Solicitudes::where('gestion_id', $request->gestion_id)
                                ->whereIn('proyecto_id', $proyectos_array)
                                ->orderBy('created_at', 'DESC')
                                ->get();
                        }
                    } else {
                        if (count($proyectos_array) == 0) {
                            $solicitudes = Solicitudes::orderBy('created_at', 'DESC')
                                ->get();
                        } else {
                            $solicitudes = Solicitudes::whereIn('proyecto_id', $proyectos_array)
                                ->orderBy('created_at', 'DESC')
                                ->get();
                        }
                    }
                }else{
                    if ($request->gestion_id != 0) {
                        if (count($proyectos_array) == 0) {
                            $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' AND gestion_id = '.$request->gestion_id.' ORDER BY created_at DESC');
                        } else {
                            $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' AND proyecto_id IN ('.implode(',', $proyectos_array).') AND gestion_id = '.$request->gestion_id.' ORDER BY created_at DESC');
                        }
                    } else {
                        if (count($proyectos_array) == 0) {
                            $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' ORDER BY created_at DESC');
                        } else {
                            $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' AND proyecto_id IN ('.implode(',', $proyectos_array).') ORDER BY created_at DESC');
                        }
                    }
                }
            } else {
                if($query == ""){
                    $proyectos_array = Proyectos::where('empresa_id', session('empresa'))
                        ->pluck('id')
                        ->toArray();

                    if ($request->gestion_id != 0) {
                        if (count($proyectos_array) == 0) {
                            $solicitudes = Solicitudes::where('usuario_id', $usuario->id)
                                ->whereIn('gestion_id', $request->gestion_id)
                                ->get();
                        } else {
                            $solicitudes = Solicitudes::where('usuario_id', $usuario->id)
                                ->whereIn('gestion_id', $request->gestion_id)
                                ->whereIn('proyecto_id', $proyectos_array)
                                ->get();
                        }
                    } else {
                        if (count($proyectos_array) == 0) {
                            $solicitudes = Solicitudes::where('usuario_id', $usuario->id)
                                ->get();
                        } else {
                            $solicitudes = Solicitudes::where('usuario_id', $usuario->id)
                                ->whereIn('proyecto_id', $proyectos_array)
                                ->get();
                        }
                    }
                }else{
                    if ($request->gestion_id != 0) {
                        $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' AND usuario_id = '.$usuario->id.' AND gestion_id = '.$request->gestion_id.' ORDER BY created_at DESC');
                    } else {
                        $solicitudes = DB::select('SELECT * FROM solicitudes'.$query.' AND usuario_id = '.$usuario->id.' ORDER BY created_at DESC');
                    }
                }
            }

            $var = ['SOL', 'SOL-M',
                'AUT','AUT-M', 'AUT-AF', 'AUT-R',
                'REV', 'REV-M', 'REV-R',
                'APR', 'APR-M', 'APR-P', 'APR-R',
                'TES', 'REN-F', 'TES-R',
                'FIN'];
            $tituloSeleccionSolicitud = "En curso";
            if(isset($request->selEstado) && $request->selEstado >= 0 && $usuario->rol_id == 8){
                switch($request->selEstado){
                    case 0:
                        //por Aprobar
                        $var = ['REV', 'APR-P','REV-M'];
                        break;
                    case 1:
                        //por autorizados
                        $var = ['AUT', 'AUT-M','AUT-O'];
                        break;
                    case 2:
                        //apro tesoreria
                        $var = ['TES', 'TES-M','TES-O'];
                        break;
                    case 3:
                        //rendicion aprobada
                        $var = ['REN-A'];
                        break;
                }
            }else{
                if(isset($request->selEstadoUsuario) && $request->selEstadoUsuario >=0){
                    switch($request->selEstadoUsuario){
                        case 1:
                            //creadas
                            $var = ['SOL', 'SOL-M'];
                            $filtros_array[] = "ESTADO: CREADAS";
                            break;
                        case 2:
                            //autorizadas
                            $var = ['AUT', 'AUT-M'];
                            $filtros_array[] = "ESTADO: EN AUTORIZADOR";
                            break;
                        case 3:
                            //revisor
                            $var = ['REV', 'REV-M'];
                            $filtros_array[] = "ESTADO: EN REVISOR";
                            break;
                        case 4:
                            //aprobador
                            $var = ['APR', 'APR-M', 'APR-P'];
                            $filtros_array[] = "ESTADO: EN APROBADOR";
                            break;
                        case 5:
                            //tesoreria
                            $var = ['TES', 'TES-M'];
                            $filtros_array[] = "ESTADO: EN TESORERIA";
                            break;
                        case 6:
                            //rendicion
                            $var = ['REN', 'REN-P', 'REN-A', 'REN-OA', 'REN-OB', 'REN-OC', 'REN-F'];
                            $filtros_array[] = "ESTADO: EN RENDICION";
                            break;
                        case 7:
                            //finalizdas
                            $var = ['FIN'];
                            $filtros_array[] = "ESTADO: FINALIZADAS";
                            break;
                        case 8:
                            //observadas
                            $var = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];
                            $filtros_array[] = "ESTADO: OBSERVADAS";
                            break;
                        case 9:
                            //rechazadas
                            $var = ['SOL-A', 'AUT-R', 'REV-R', 'APR-R', 'TES-R', 'REN-R'];
                            $filtros_array[] = "ESTADO: RECHAZADAS";
                            break;
                    }
                }else{
                    if(isset($_GET['t'])){
                        switch($_GET['t']){
                            case 'observadas':
                                $var = ['AUT-O', 'REV-O', 'APR-O', 'TES-O'];
                                $tituloSeleccionSolicitud = "Observadas";
                                $filtros_array[] = "ESTADO: OBSERVADAS";
                                break;
                            case 'rechazadas':
                                $var = ['AUT-R', 'REV-R', 'APR-R', 'TES-R'];
                                $tituloSeleccionSolicitud = "Rechazadas";
                                $filtros_array[] = "ESTADO: RECHAZADAS";
                                break;
                            case 'rendidas':
                                $var = ['REN','REN-P', 'REN-F'];
                                $tituloSeleccionSolicitud = "Rendidas";
                                $filtros_array[] = "ESTADO: RENDIDAS";
                                break;
                            case 'finalizadas':
                                $var = ['FIN'];
                                $tituloSeleccionSolicitud = "Finalizadas";
                                $filtros_array[] = "ESTADO: FINALIZADAS";
                                break;
                        }
                    }
                }
            }

            if(count($filtros_array) > 0){
                $cont = 0;
                foreach($filtros_array as $filtro){
                    if($cont == 0){
                        $filtros = "' ".$filtro;
                    }else{
                        $filtros .= ", ".$filtro;
                    }
                    $cont++;
                }
                $filtros .= " '";
            }

            foreach($solicitudes as $solicitud){
                $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
                if(in_array($solicitud->estadoCodigo, $var)){
                    $solicitud_ids [] = $solicitud->id;
                }
            }

            $solicitudes = Solicitudes::sortable()
                ->orderBy('gestion_id', 'DESC')
                ->orderBy('numero', 'DESC')
                ->orderBy('desembolso', 'ASC')
                ->orderBy('referencia', 'ASC')
                ->orderBy('observacion', 'ASC')
                ->orderBy('total', 'ASC')
                ->orderBy('modalidad_id', 'ASC')
                ->orderBy('moneda_id', 'ASC')
                ->orderBy('usuario_id', 'ASC')
                ->orderBy('proyecto_id', 'ASC')
                ->orderBy('cambio_id', 'ASC')
                ->orderBy('created_at', 'ASC')
                ->whereIn('id', $solicitud_ids)->get();

            foreach($solicitudes as $solicitud){
                $solicitud->estado = $this->getLastEstado($solicitud->id);
                $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
                if($solicitud->moneda_id == 1){
                    //bolivianos
                    $total_solicitudes_bol += $solicitud->total;
                }else{
                    //dolares
                    $total_solicitudes_dol += $solicitud->total;
                }

                if($solicitud->proyecto->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
                }elseif($solicitud->proyecto->padre->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }else{
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }
            }
            $total_solicitudes_bol = number_format($total_solicitudes_bol, '2', '.', '');
            $total_solicitudes_dol = number_format($total_solicitudes_dol, '2', '.', '');

            if(isset($_GET['sort']) && $_GET['sort'] == 'estado'){

                if($_GET['order'] == 'asc'){
                    $solicitudes = $solicitudes->sortBy('estado');
                }else{
                    $solicitudes = $solicitudes->sortBy('estado', SORT_REGULAR, true);
                }
            }
            $solicitudes = $this->paginateCollection($solicitudes, session('paginacion'), $pageName = 'page');
        }

        return view('solicitudes.reportes.reporteGral')
            ->withTotalsolicitudesbol($total_solicitudes_bol)
            ->withTotalsolicitudesdol($total_solicitudes_dol)
            ->withExcelsolicitudes($solicitud_ids)
            ->withSolicitudes($solicitudes)
            ->withEmpresas($empresas)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withTitulo("ADMIN")
            ->withGestiones($gestiones);
    }

    public function reporteRevisor(Request $request){
        $usuario = auth()->user();

        $solicitud_ids = array();
        if(isset($request->fecha_ini)){
            $fecha_ini = $request->fecha_ini;
            if(isset($request->fecha_fin)){
                $fecha_fin = $request->fecha_fin;
                $query = "SELECT * FROM solicitudes WHERE (created_at BETWEEN '".$fecha_ini." 00:00:00' AND '".$fecha_fin." 23:59:59') ORDER BY created_at DESC";
            }else{
                $query = "SELECT * FROM solicitudes WHERE (created_at BETWEEN '".$fecha_ini." 00:00:00' AND '".$fecha_ini." 23:59:59') ORDER BY created_at DESC";
            }
        }else{
            $fecha_actual = date('Y-m-d');
            $fecha_ini = date('Y-m-d',strtotime($fecha_actual)-(3*24*60*60));
            $fecha_fin = $fecha_actual;
            $query = "SELECT * FROM solicitudes WHERE (created_at BETWEEN '".$fecha_ini." 00:00:00' AND '".$fecha_fin." 23:59:59') ORDER BY created_at DESC";
        }
        $solicitudes = DB::select($query);

        foreach($solicitudes as $solicitud){
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);
            $solicitud_ids [] = $solicitud->id;
        }
        $solicitudes = Solicitudes::whereIn('id', $solicitud_ids)->get();

        foreach($solicitudes as $solicitud){
            $solicitud->estado = $this->getLastEstado($solicitud->id);
            $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

            if($solicitud->proyecto->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
            }elseif($solicitud->proyecto->padre->padre_id == null){
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }else{
                $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
            }
        }

        return view('solicitudes.reportes.reporteRevisor')
            ->withExcelsolicitudes($solicitud_ids)
            ->withSolicitudes($solicitudes);
    }

    public function reporteAprRendiciones(Request $request){
        $usuario = auth()->user();
        $estados = ['TES', 'REN-F', 'REN-A', 'REN-R', 'REN-P', 'REN-O', 'FIN'];

        // if($request->method() == 'POST'){
        // Proyectos de la empresa actual (sesión)
        if ($usuario->global_view == 0){
            $idProyectos = Proyectos::where('empresa_id', session('empresa'))
                ->pluck('id')
                ->toArray();
        } else {
            $idProyectos = Proyectos::pluck('id')
                ->toArray();
        }

        $idUsuarios = DB::table('proyectos_users')
            ->whereIn('proyecto_id', $idProyectos)
            ->pluck('user_id')
            ->toArray();

        if ($usuario->global_view == 0){
            $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
                ->select('solicitudes.*')
                ->whereIn('gestion_id', session('gestion_id'))
                ->whereIn('solicitudes.usuario_id', $idUsuarios)
                // ->whereIn('estados_solicitud.estado_id', $idEstados)
                ->whereIn('solicitudes.proyecto_id', $idProyectos)
                ->whereIn('estados_solicitud.id', function($query){
                    $query->selectRaw('MAX(estados_solicitud.id)')
                        ->from('estados_solicitud')
                        ->groupBy('estados_solicitud.solicitud_id')
                        ->get();
                });
        } else {
//            dd($idSolicitudes->get(), "asdf");
            $idSolicitudes = Solicitudes::join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
                ->select('solicitudes.*')
                ->whereIn('solicitudes.usuario_id', $idUsuarios)
                ->whereIn('solicitudes.proyecto_id', $idProyectos)
                ->whereIn('estados_solicitud.id', function($query){
                    $query->selectRaw('MAX(estados_solicitud.id)')
                        ->from('estados_solicitud')
                        ->groupBy('estados_solicitud.solicitud_id')
                        ->get();
                });
        }

        // Filtrado por solicitante
        if(isset($request->solicitante) && !empty($request->solicitante)){
            $idSolicitudes = $idSolicitudes->join('users', 'solicitudes.usuario_id', 'users.id')
                ->where('users.nombre', 'LIKE', "%$request->solicitante%");
        }

        // Filtrado por desembolso
        if(isset($request->desembolso) && !empty($request->desembolso)){
            $idSolicitudes = $idSolicitudes->where('solicitudes.desembolso', 'LIKE', "%$request->desembolso%");
        }

        // Filtrado por estado
        if(isset($request->estado) && $request->estado != 6){
            $idEstados = Estados::where('codigo', $estados[$request->estado])
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }
        $idSolicitudes = $idSolicitudes->whereIn('estados_solicitud.estado_id', $idEstados);
        dd($idSolicitudes->get());
        // Filtrado por Monto
        if(isset($request->costo) && !empty($request->costo)){
            $simbolos = ['=', '>', '>=', '<', '<='];

            $idSolicitudes = $idSolicitudes->where(
                'solicitudes.total', $simbolos[$request->select_costo], $request->costo
            );
        }
        dd($idSolicitudes->get());

        // Filtrado de fechas
        if(isset($request->fecha_ini) && !empty($request->fecha_ini)){
            $fecha_ini = $request->fecha_ini;
            if(isset($request->fecha_fin) && !empty($request->fecha_fin)){
                $fecha_fin = $request->fecha_fin;

                // $idSolicitudes = $idSolicitudes->whereBetween('solicitudes.created_at',
                //     [$fecha_ini." 00:00:00", $fecha_fin." 23:59:59"]);
                $idSolicitudes = $idSolicitudes->join('item_rendiciones', 'item_rendiciones.solicitud_id', 'solicitudes.id')
                    ->whereBetween('item_rendiciones.created_at', [$fecha_ini." 00:00:00", $fecha_fin." 23:59:59"]);
            }else{
                // $idSolicitudes = $idSolicitudes->whereBetween('solicitudes.created_at',
                //     [$fecha_ini." 00:00:00", $fecha_ini." 23:59:59"]);
                $idSolicitudes = $idSolicitudes->join('item_rendiciones', 'item_rendiciones.solicitud_id', 'solicitudes.id')
                    ->whereBetween('item_rendiciones.created_at', [$fecha_ini." 00:00:00", $fecha_ini." 23:59:59"]);
            }
        }

         dd($idSolicitudes->toSql());

        dd("Su", $idSolicitudes);
//        dd("Su", $idSolicitudes->pluck('solicitudes.id')->toArray());
        $idSolicitudes = array_values(array_unique($idSolicitudes->pluck('solicitudes.id')->toArray()));
        // $idSolicitudes = $idSolicitudes->pluck('solicitudes.id')->toArray();
        // }else{
        //     $idSolicitudes = array();
        // }

        if(count($idSolicitudes) > 0){
            $solicitudes = Solicitudes::sortable()
                ->orderBy('numero', 'DESC')
                ->orderBy('desembolso', 'ASC')
                ->orderBy('referencia', 'ASC')
                ->orderBy('observacion', 'ASC')
                ->orderBy('total', 'ASC')
                ->orderBy('modalidad_id', 'ASC')
                ->orderBy('moneda_id', 'ASC')
                ->orderBy('usuario_id', 'ASC')
                ->orderBy('proyecto_id', 'ASC')
                ->orderBy('cambio_id', 'ASC')
                ->orderBy('created_at', 'ASC')
                ->whereIn('id', $idSolicitudes)
                ->get();
        }else{
            $solicitudes = array();
        }

        if(count($solicitudes) > 0){
            foreach($solicitudes as $solicitud){
                $solicitud->estado = $this->getLastEstado($solicitud->id);
                $solicitud->estadoCodigo = $this->getLastEstadoCodigo($solicitud->id);

                if($solicitud->proyecto->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->nombre;
                }elseif($solicitud->proyecto->padre->padre_id == null){
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }else{
                    $solicitud->nom_completo_proyecto = $solicitud->proyecto->padre->padre->nombre." &#10148; ".$solicitud->proyecto->padre->nombre." &#10148; ".$solicitud->proyecto->nombre;
                }
            }
        }

        // dd($idSolicitudes, $solicitudes);

        array_push($estados, 'TODOS');

        if(isset($request->select_costo) && !empty($request->select_costo)){
            return view('solicitudes.reportes.reporteAprRendiciones')
                ->withExcelsolicitudes($idSolicitudes)
                ->withSolicitudes($solicitudes)
                ->withEstados($estados)
                ->withCostoselect($request->select_costo);
        }else{
            return view('solicitudes.reportes.reporteAprRendiciones')
                ->withExcelsolicitudes($idSolicitudes)
                ->withSolicitudes($solicitudes)
                ->withEstados($estados);
        }

    }

    public function reporteSolicitudesRendidas(){
        $solicitudes = DB::select('SELECT DISTINCT solicitud_id  FROM `estados_solicitud` WHERE `estado_id` = 62  ORDER BY `solicitud_id`');
        $solicitudes_ids = array();
        foreach($solicitudes as $solicitud){
            $solicitudes_ids[] = $solicitud->solicitud_id;
            $items = Items::where('solicitud_id', $solicitud->solicitud_id)->get();
            $itemsRendidos = ItemsRendiciones::where('solicitud_id', $solicitud->solicitud_id)->get();
            dd($items, $itemsRendidos);
        }
        dd($solicitudes_ids);
    }

    public function reporteSolicitudesProyecto(){
        $proyectos = Proyectos::where('empresa_id', session('empresa'))->get();
        foreach($proyectos as $proyecto){
            $solicitudes = Solicitudes::where('proyecto_id', $proyecto->id)->get();
            $proyecto->cantidad_sol = count($solicitudes);

            if($proyecto->padre_id == null){
                $proyecto->nom_completo_proyecto = $proyecto->nombre;
            }elseif($proyecto->padre->padre_id == null){
                $proyecto->nom_completo_proyecto = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
            }else{
                $proyecto->nom_completo_proyecto = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
            }

        }

        return view('solicitudes.reportes.solProyectos')
            ->withProyectos($proyectos);
    }

    public function reporteSolicitudesProyectoEmpresa(){
        $proyectos = Proyectos::join('proyectos_users', 'proyectos_users.proyecto_id', '=', 'proyectos.id')
            ->select('proyectos.*', 'proyectos_users.user_id')
            ->orderBy('proyectos.empresa_id', 'ASC')
            ->orderBy('proyectos.nombre')
            ->orderBy('proyectos.id', 'ASC')
            ->get();
        foreach($proyectos as $proyecto){
            $usuario = User::find($proyecto->user_id);
            $proyecto->usuario = $usuario->usuario;
            $proyecto->nomUsuario = $usuario->nombre;
            $proyecto->nomEmpresa = $proyecto->empresa->nombre;
            if($proyecto->padre_id == null){
                $proyecto->nom_completo_proyecto = $proyecto->nombre;
            }elseif($proyecto->padre->padre_id == null){
                $proyecto->nom_completo_proyecto = $proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
            }else{
                $proyecto->nom_completo_proyecto = $proyecto->padre->padre->nombre." &#10148; ".$proyecto->padre->nombre." &#10148; ".$proyecto->nombre;
            }
            $autorizadores = DB::select('SELECT u.nombre'
                .' FROM autorizador_usuarios AS au, users AS u'
                .' WHERE au.autorizador_id = u.id'
                .' AND au.empresa_id = '.$proyecto->empresa_id
                .' AND au.proyecto_id = '.$proyecto->id
                .' AND au.usuario_id = '.$proyecto->user_id
                .' ORDER BY u.usuario');
            $array_autorizadores = array();
            foreach($autorizadores as $autorizador){
                $array_autorizadores[] = $autorizador->nombre;
            }
            $proyecto->autorizadores = $array_autorizadores;
        }
        //30:a1:fa:92:2d:8f
        return view('solicitudes.reportes.reporteProyUsuario')
            ->withProyectos($proyectos);
    }

    public function getLastEstado($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.descripcion')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados.id', 'DESC')
            ->first();
        return $lastEstado->descripcion;
    }

    public function getLastEstadoCodigo($solicitud_id){
        $lastEstado = Estados::join('estados_solicitud', 'estados_solicitud.estado_id', '=', 'estados.id')
            ->select('estados.codigo')
            ->where('estados_solicitud.solicitud_id', $solicitud_id)
            ->orderBy('estados_solicitud.id', 'DESC')
            ->first();
        return $lastEstado->codigo;
    }

    function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null){
        $perPage = 20;

        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }

    function reporteActivosFijos (Request $request) {
        $items_activos_fijos_ids = $this->filterAf($request->all());

        $items_activos_fijos = ItemsAf::join('item_rendiciones', 'item_af_datos.item_rendido_id', '=', 'item_rendiciones.id')
            ->whereIn('item_af_datos.id', $items_activos_fijos_ids)
            ->orderBy('item_rendiciones.fecha_factura', 'DESC')
            ->get();

        $empresas = Empresas::all();

        $gestiones = Gestiones::orderBy('gestion', 'ASC')
            ->get();

        $items_activos_fijos = $this->paginateCollection($items_activos_fijos, session('paginacion'), $pageName = 'page');

        $filtros = $this->aplicarFiltros($request);

        $usuario = auth()->user();

        return view('solicitudes.reportes.reporteActivosFijos')
            ->withEmpresas($empresas)
            ->withGestiones($gestiones)
            ->withUsuario($usuario)
            ->withFiltros($filtros)
            ->withFilters($request->all())
            ->withTipo_solicitud('REPORTE DE ACTIVOS FIJOS')
            ->with('items_ids', $items_activos_fijos_ids)
            ->with('items', $items_activos_fijos);
    }

    /**
     * Devuelve los items de activos fijos
     */
    public static function filterAf ($filters = null){
        // Todos los proyectos de todas las empresas
        $idProyectos = Proyectos::pluck('id')
            ->toArray();

        $estados = ['REN-F'];

        if(!empty($estados)){
            $idEstados = Estados::whereIn('codigo', $estados)
                ->pluck('id')
                ->toArray();
        }else{
            $idEstados = Estados::pluck('id')
                ->toArray();
        }

        $idItems = ItemsAf::join('item_rendiciones', 'item_af_datos.item_rendido_id', '=', 'item_rendiciones.id')
            ->join('solicitudes', 'item_rendiciones.solicitud_id', '=', 'solicitudes.id')
            ->join('estados_solicitud', 'estados_solicitud.solicitud_id', '=', 'solicitudes.id')
            ->select('item_af_datos.id')
            ->whereIn('estados_solicitud.estado_id', $idEstados)
            ->whereIn('estados_solicitud.id', function($query){
                $query->selectRaw('MAX(estados_solicitud.id)')
                    ->from('estados_solicitud')
                    ->groupBy('estados_solicitud.solicitud_id')
                    ->get();
            });

        /** BEGIN Filters */

        // Empresa
        if(isset($filters['empresa_id']) && $filters['empresa_id'] != 0){
            $idProyectos = Proyectos::where('empresa_id', $filters['empresa_id'])
                ->pluck('id')
                ->toArray();

            $idItems = $idItems
                ->whereIn('solicitudes.proyecto_id', $idProyectos);
        }

        // Proyecto
        if(isset($filters['proyecto_id']) && $filters['proyecto_id'] != 0){
            $idItems = $idItems
                ->where('solicitudes.proyecto_id', $filters['proyecto_id']);
        }

        // Fechas
        if (isset($filters['fecha_ini']) && isset($filters['fecha_fin']) && $filters['fecha_ini'] != '' && $filters['fecha_fin'] != '') {
            $fecha_ini = $filters['fecha_ini'].' 00:00:00';
            $fecha_fin = $filters['fecha_fin'].' 23:59:59';

            $idItems = $idItems->where([
                ['item_rendiciones.fecha_factura', '>=', $fecha_ini],
                ['item_rendiciones.fecha_factura', '<=', $fecha_fin]
            ]);
        } elseif (isset($filters['fecha_ini']) && $filters['fecha_ini'] != '') {
            $fecha_ini = $filters['fecha_ini'].' 00:00:00';

            $idItems = $idItems->where('item_rendiciones.fecha_factura', '>=', $fecha_ini);
        }

        // Custom
        if(isset($filters['campo']) && $filters['campo'] != 0 && isset($filters['texto']) && $filters['texto'] != ''){
            $texto = $filters['texto'];
            switch($filters['campo']){
                case 1:
                    $idItems = $idItems->where('item_af_datos.descripcion_especifica', 'LIKE', "%$texto%");
                    break;
                case 2:
                    $idItems = $idItems->where('item_af_datos.numero_serie', 'LIKE', "%$texto%");
                    break;
                case 3:
                    $idItems = $idItems->where('item_af_datos.asignacion', 'LIKE', "%$texto%");
                    break;
                case 4:
                    $idItems = $idItems
                        ->orWhere('item_af_datos.descripcion_especifica', 'LIKE', "%$texto%")
                        ->orWhere('item_af_datos.numero_serie', 'LIKE', "%$texto%")
                        ->orWhere('item_af_datos.asignacion', 'LIKE', "%$texto%");
                    break;
            }
        }

        // Gestión
        if (isset($filters['gestion_id']) && $filters['gestion_id'] != 0) {
            $idItems = $idItems->where('solicitudes.gestion_id', $filters['gestion_id']);
        } else if (!isset($filters['gestion_id'])){
            $gestion = Gestiones::orderBy('id', 'DESC')->first();
            $idItems = $idItems->where('solicitudes.gestion_id', $gestion->id);
        }

        /** END Filters */

        //dd($idSolicitudes->toSql());

        $idItems = $idItems->pluck('item_af_datos.id')
            ->toArray();

        return $idItems;
    }

    /**
     * Aplicar filtros
     */
    public static function aplicarFiltros (Request $request) {
        // Filtros
        $filtros_array = array();
        foreach($request->all() as $key => $value){
            if($value != '' && $value != 0){
                switch($key){
                    case 'empresa_id':
                        $empresa = Empresas::where('id', $value)->first();
                        $filtros_array[] = "EMPRESA: ".$empresa->nombre;
                        break;
                    case 'proyecto_id':
                        $proyecto = Proyectos::where('id', $value)->first();
                        $filtros_array[] = "PROYECTO: ".$proyecto->nombre;
                        break;
                    case 'monto':
                        switch($request->selectMonto){
                            case 0:
                                $filtros_array[] = "MONTO IGUAL A: ".$value;
                                break;
                            case 1:
                                $filtros_array[] = "MONTO MAYOR A: ".$value;
                                break;
                            case 2:
                                $filtros_array[] = "MONTO MAYOR O IGUAL A: ".$value;
                                break;
                            case 3:
                                $filtros_array[] = "MONTO MENOR A: ".$value;
                                break;
                            case 4:
                                $filtros_array[] = "MONTO MENOR O IGUAL A: ".$value;
                                break;
                        }
                        break;
                    case 'fecha_ini':
                        if(isset($request->fecha_fin) && $request->fecha_fin != ''){
                            $filtros_array[] = "FECHAS DESDE: ".$request->fecha_ini." HASTA: ".$request->fecha_fin;
                        }else{
                            $filtros_array[] = "CON FECHA: ".$request->fecha_ini;
                        }
                        break;
                    case 'campo':
                        if(isset($request->texto) && $request->texto != ''){
                            switch($value){
                                case 1:
                                    $filtros_array[] = "SOLICITANTE: ".$request->texto;
                                    break;
                                case 2:
                                    $filtros_array[] = "DESEMBOLSO: ".$request->texto;
                                    break;
                                case 3:
                                    $filtros_array[] = "REFERENCIA: ".$request->texto;
                                    break;
                                case 4:
                                    $filtros_array[] = "OBSERVACIÓN: ".$request->texto;
                                    break;
                                case 5:
                                    $filtros_array[] = "NÚMERO DE SOLICITUD: ".$request->texto;
                                    break;
                                case 6:
                                    $filtros_array[] = "TODAS LAS COLUMNAS: ".$request->texto;
                                    break;
                            }
                        }
                        break;
                    case 'chk_fr_cc':
                        $filtros_array[] = "FONDOS, CAJAS y REPO.";
                        break;
                    case 'gestion_id':
                        $gestion = Gestiones::find($value);
                        $filtros_array[] = "GESTIÓN: ".$gestion->gestion;
                        break;
                }
            }
        }

        $filtros = '';
        if(count($filtros_array) > 0){
            $cont = 0;
            foreach($filtros_array as $filtro){
                if($cont == 0){
                    $filtros = "' ".$filtro;
                }else{
                    $filtros .= ", ".$filtro;
                }
                $cont++;
            }
            $filtros .= " '";
        }

        return $filtros;
    }
}

