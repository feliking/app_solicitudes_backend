<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsRendiciones extends Model
{
    //Mutators
    public function setDetalleAttribute($value)
    {
        $this->attributes['detalle'] = mb_strtoupper($value);
    }

    // Tabla modelo
    protected $table = 'item_rendiciones';

    // One to Many <- (inverse)
    public function item(){
        return $this->belongsTo('App\Items');
    }

    public function unidad(){
        return $this->belongsTo('App\Unidades', 'unidad_id');
    }

    public function solicitud(){
        return $this->belongsTo('App\Solicitudes');
    }
    public function tipo_compra(){
        return $this->belongsTo('App\ItemsTipos', 'tipo_id');
    }
}
