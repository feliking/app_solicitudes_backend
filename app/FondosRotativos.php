<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FondosRotativos extends Model
{
    protected $table = 'fondorotativos';

    // One to Many <- (inverse)
    public function tipo(){
        return $this->belongsTo('App\FondosRotativoTipos');
    }

    // Many to Many
    public function empresas(){
        return $this->belongsToMany('App\FondosRotativos', 'fondorotativos', 'empresa_id', 'fr_id');
    }

    public function proyectos(){
        return $this->belongsToMany('App\FondosRotativos', 'fondorotativos', 'proyecto_id', 'fr_id');
    }
}
