<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyectos extends Model
{
    //Mutators
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value);
    }

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value);
    }

    // One to one ->
    public function hijo(){
        return $this->hasMany('App\Proyectos', 'padre_id');
    }
    
    // One to one <- (inverse)
    public function padre(){
        return $this->belongsTo('App\Proyectos');
    }

    // One to Many ->
    public function solicitudes(){
        return $this->hasMany('App\Solicitudes');
    }

    // One to Many <- (inverse)
    public function empresa(){
        return $this->belongsTo('App\Empresas');
    }

    // Many to Many
    public function usuarios(){
        return $this->belongsToMany('App\User', 'proyectos_users', 'proyecto_id', 'user_id');
    }

    public function fondosrotativos(){
        return $this->belongsToMany('App\FondosRotativos','fondorotativos','fr_id','proyecto_id');
    }
}
