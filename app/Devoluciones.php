<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devoluciones extends Model
{
    // One to Many <- (inverse)
    public function solicitud(){
        return $this->belongsTo('App\Solicitudes');
    }
}
