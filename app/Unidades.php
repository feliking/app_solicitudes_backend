<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidades extends Model
{
    protected $fillable = [
        'descripcion'
    ];

    //Mutators
    public function setCodigoAttribute($value)
    {
        $this->attributes['codigo'] = mb_strtoupper($value);
    }

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function items(){
        return $this->hasMany('App\Items');
    }

    public function itemsRendidos(){
        return $this->hasMany('App\ItemsRendidos');
    }

    public function itemsController(){
        return $this->hasMany('App\ItemsController');
    }
}
