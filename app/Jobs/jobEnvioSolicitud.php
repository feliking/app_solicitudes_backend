<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

// Email
use Mail;
use App\Mail\solicitudEmail;

class jobEnvioSolicitud implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $num_solicitud;
    protected $empresa;
    protected $proyecto;
    protected $referencia;
    protected $correoDestino;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($num_solicitud, $empresa, $proyecto, $referencia, $correoDestino){
        $this->num_solicitud = $num_solicitud;
        $this->empresa = $empresa;
        $this->proyecto = $proyecto;
        $this->referencia = $referencia;
        $this->correoDestino = $correoDestino;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->correoDestino)
            ->send(new solicitudEmail($this->num_solicitud, $this->empresa, $this->proyecto, $this->referencia));
    }
}
