<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    //Mutators
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value);
    }

    // One to Many ->
    public function usuarios(){
        return $this->hasMany('App\Roles');
    }
}
