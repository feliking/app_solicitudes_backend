<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsAf extends Model
{
    // Tabla modelo
    protected $table = 'item_af_datos';

    public function item_rendido()
    {
        return $this->belongsTo('App\ItemsRendiciones', 'item_rendido_id');
    }
}
