<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentosRendicion extends Model
{
    // Tabla modelo
    protected $table = 'documentos_rendicion';

    // One to Many <- (inverse)
    public function solicitud(){
        return $this->belongsTo('App\Solicitudes');
    }
}
