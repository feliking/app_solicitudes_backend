<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObservacionRendicion extends Model
{
    protected $table = 'observaciones_rendicion';

    /* Relationships
     *
     */
    // One to One (inverse)
    public function solicitud() {
        return $this->belongsTo('App\Solicitudes', 'solicitud_id');
    }
}
